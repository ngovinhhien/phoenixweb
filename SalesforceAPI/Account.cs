﻿using SalesforceSharp;
using SalesforceSharp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesforceAPI
{
   public class Account
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Fax { get; set; }
        public string Industry { get; set; }
        public string Phone { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingStreet { get; set; }
        public string BillingCity { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingState { get; set; }
        public string BillingStreet { get; set; }
    }

   public static class AccountDA
   {
       public static List<Account> GetAccount()
       {
           List<Account> recordList = new List<Account>();
           try
           {
               Connect cda = new Connect();
               var client = new SalesforceClient();
               client = cda.ConnectSalesforce();
               var records = client.Query<Account>("SELECT Id,Name,Description,Fax,Industry,Phone,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet FROM Account");

               foreach (var r in records)
               {
                   Account a = new Account();
                   a.Id = r.Id;
                   a.Name = r.Name;
                   a.Description = r.Description;
                   a.Fax = r.Fax;
                   a.Industry = r.Industry;
                   a.Phone = r.Phone;
                   a.ShippingCity = r.ShippingCity;
                   a.ShippingCountry = r.ShippingCountry;
                   a.ShippingState = r.ShippingState;
                   a.ShippingPostalCode = r.ShippingPostalCode;
                   a.ShippingStreet = r.ShippingStreet;
                   a.BillingCity = r.BillingCity;
                   a.BillingCountry = r.BillingCountry;
                   a.BillingPostalCode = r.BillingPostalCode;
                   a.BillingState = r.BillingState;
                   a.BillingStreet = r.BillingStreet;
                   recordList.Add(a);
               }
           }
           catch (SalesforceException ex)
           {

           }
           return recordList;
       }
   }
   public class Connect
   {
       public SalesforceClient ConnectSalesforce()
       {
           var client = new SalesforceClient();
           var authFlow = new UsernamePasswordAuthenticationFlow("3MVG9Y6d_Btp4xp5DDo82HRvipX6ZgGF6MAZDt53gsHH1pKkhnitsfXsbqfgujUp4KO8RPqyRvS_b5KvpJVkf", "1445840785118068527", "support@livetaxiepay.com.au", "Vincent001pJwT91Wl2mGKoNHDynV3HH3Ha");
           try
           {
               client.Authenticate(authFlow);
               var records = client.Query<Account>("SELECT Id,Name,Description,Fax,Industry,Phone,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet FROM Account");

           }
           catch (SalesforceException ex)
           {
             
           }
           return client;
       }
   }
}
