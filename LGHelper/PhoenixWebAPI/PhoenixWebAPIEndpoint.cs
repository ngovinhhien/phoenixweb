﻿namespace LGHelper.PhoenixWebAPI
{
    public static class PhoenixWebAPIEndpoint
    {
        public const string FRAUDMONITORING_BUSINESS_TYPE = "api/fraud-monitoring/business-type";
        public const string FRAUDMONITORING_QUARANTINE_STATUS = "api/fraud-monitoring/quarantine-status";
        public const string FRAUDMONITORING_FRAUD_ALERT = "api/fraud-monitoring/fraud-alert";
        public const string FRAUDMONITORING_TRANSACTION_STATUS = "api/fraud-monitoring/transaction-status";
        public const string FRAUDMONITORING_FRAUD_TRANSACTION = "api/fraud-monitoring/fraud-transaction";
        public const string FRAUDMONITORING_MEMBER = "api/fraud-monitoring/member";
        public const string FRAUDMONITORING_MEMBER_NOTE = "api/fraud-monitoring/member-notes";
        public const string FRAUDMONITORING_MEMBER_TRANSACTIONS = "api/fraud-monitoring/transactions";
        public const string FRAUDMONITORING_MEMBER_OVERVIEW = "api/fraud-monitoring/member-overview";
        public const string FRAUDMONITORING_MEMBER_PAYMENT_HISTORY = "api/fraud-monitoring/member-payment-history";
        public const string FRAUDMONITORING_MEMBER_PAYMENT_DETAILS = "api/fraud-monitoring/member-payment-details";
        public const string FRAUDMONITORING_MEMBER_PENDING_TRANSACTION = "api/fraud-monitoring/member-pending-transaction";
        public const string FRAUDMONITORING_MEMBER_QUARANTINED_TRANSACTION = "api/fraud-monitoring/member-quarantined-transaction";
        public const string FRAUDMONITORING_CHARGEBACK_DATA = "api/fraud-monitoring/chargeback-member-transaction";
        public const string FRAUDMONITORING_CHARGEBACK_TRANSACTION = "api/fraud-monitoring/chargeback";
        public const string FRAUDMONITORING_MEMBER_RAISED_REQUEST = "api/fraud-monitoring/member-raised-request";
        public const string FRAUD_MONITORING_GET_MEMBER_TRANSACTION_PAYMENT_DETAIL = "api/fraud-monitoring/transactions";
        public const string FRAUD_MONITORING_GET_TRANSACTION_NOTES = "api/fraud-monitoring/transaction-notes";
        public const string FRAUD_MONITORING_GET_RELATED_TRANSACTIONS = "api/fraud-monitoring/related-transactions";
        public const string FRAUD_MONITORING_GET_RAISED_REQUEST_TYPES = "api/fraud-monitoring/raise-request-types";
        public const string FRAUD_MONITORING_CANCEL_CHARGEBACK = "api/fraud-monitoring/chargeback/cancel";
        public const string FRAUD_MONITORING_CREATE_REVERSE_CHARGEBACK = "api/fraud-monitoring/chargeback/reverse";
        public const string FRAUDMONITORING_REVERSE_DATA = "api/fraud-monitoring/reverse-member-transaction";
        public const string FRAUDMONITORING_REVERSE_TRANSACTION = "api/fraud-monitoring/reverse";


        public const string PRICINGPLAN_GET_LIST = "api/msf/pricing-plans";
        public const string PRICINGPLAN_LOAD_PRICING_PLAN = "api/msf/pricing-plan";
        public const string PRICINGPLAN_ADD_EDIT_PRICING_PLAN = "api/msf/pricing-plan";
        public const string ACCERTIFY_GET_TRANSACTION = "api/accertify/accertify-transaction";
        public const string ACCERTIFY_GET_SUBBUSINESS_TYPE = "api/accertify/sub-business-type";
        public const string ACCERTIFY_GET_STATUS = "api/accertify/status";
        public const string PAYMENTHISTORY_GET_LIST = "api/driver-card/payment-histories";
        public const string PAYMENTHISTORY_GET_PAYMENT_DETAIL = "api/driver-card/payment-histories";
    }
}