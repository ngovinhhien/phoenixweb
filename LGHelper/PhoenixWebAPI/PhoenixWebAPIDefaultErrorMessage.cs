﻿namespace LGHelper.PhoenixWebAPI
{
    public static class PhoenixWebAPIDefaultErrorMessage
    {
        public const string NotFound = "The page you were looking for cannot be found.";
        public const string MethodNotAllowed = "Request method not allowed.";
        public const string Unauthorized = "Unauthorized access.";
        public const string Forbidden = "You do not have permission to access this page.";
        public const string BadRequest = "Bad request. Please contact IT Support.";
        public const string ServiceUnavailable = "Service Temporarily Unavailable.";
        public const string InternalServerError = "Internal Server Error.";
        public const string RequestTimeout = "Time out error. Please refresh the page again.";
        public const string UnknownError = "System error. Please contact IT team to support.";
        public const string SystemError = "System error, please try again. If this error continues to occur, please contact IT Support.";
        public const string CanNotDeserializeJsonToObject = "Cannot parse response from server.";
        public const string CanNotGetAccessToken = "Cannot get access token. Please contact IT Support.";
    }
}