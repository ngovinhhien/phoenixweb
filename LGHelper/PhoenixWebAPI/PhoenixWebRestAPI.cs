﻿using Newtonsoft.Json;
using Phoenix_BusLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Caching;

namespace LGHelper.PhoenixWebAPI
{
    public class PhoenixWebRestAPI
    {
        private readonly RestClient httpClient;

        /// <summary>
        /// Default timeout (million seconds)
        /// </summary>
        private const int DEFAULT_TIMEOUT_REQUEST = 60000;

        public PhoenixWebRestAPI()
        {
            httpClient = new RestClient();
        }

        public ResultModel<T> Get<T>(string requestUrl, int timeout = DEFAULT_TIMEOUT_REQUEST)
        {
            return SendData<T>(Method.GET, requestUrl, timeout);
        }

        public ResultModel<T> Post<T>(string requestUrl, object content, int timeout = DEFAULT_TIMEOUT_REQUEST)
        {
            return SendData<T>(Method.POST, requestUrl, timeout, content);
        }

        public ResultModel<T> Put<T>(string requestUrl, object content, int timeout = DEFAULT_TIMEOUT_REQUEST)
        {
            return SendData<T>(Method.PUT, requestUrl, timeout, content);
        }

        private ResultModel<T> SendData<T>(Method method, string requestUrl, int timeout, object content = null)
        {
            var resultModel = new ResultModel<T>();

            try
            {
                var request = new RestRequest(method);

                var accessToken = GetAccessToken();

                if (string.IsNullOrEmpty(accessToken))
                {
                    resultModel.Message = PhoenixWebAPIDefaultErrorMessage.CanNotGetAccessToken;

                    return resultModel;
                }

                request.AddHeader("Authorization", $"Bearer {GetAccessToken()}");
                request.AddHeader("Content-Type", "application/json");

                httpClient.BaseUrl = new Uri($"{Caching.GetSystemConfigurationByKey(PhoenixAPISystemConfigurationKeyEnum.PhoenixAPIBaseURL.ToString())}{requestUrl}");
                httpClient.Timeout = timeout;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                if (content != null)
                {
                    request.AddJsonBody(content);
                }

                var res = httpClient.Execute(request);

                switch ((int)res.StatusCode)
                {
                    case (int)HttpStatusCode.NotFound:
                        if (string.IsNullOrEmpty(res.Content))
                        {
                            resultModel.Message = PhoenixWebAPIDefaultErrorMessage.NotFound;
                        }
                        else
                        {
                            resultModel = JsonConvert.DeserializeObject<ResultModel<T>>(res.Content);
                            resultModel.IsSuccess = true;
                        }

                        break;

                    case (int)HttpStatusCode.MethodNotAllowed:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.MethodNotAllowed;
                        break;

                    case (int)HttpStatusCode.Unauthorized:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.Unauthorized;
                        break;

                    case (int)HttpStatusCode.Forbidden:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.Forbidden;
                        break;

                    case (int)HttpStatusCode.BadRequest:
                        if (string.IsNullOrEmpty(res.Content))
                        {
                            resultModel.Message = PhoenixWebAPIDefaultErrorMessage.BadRequest;
                        }
                        else
                        {
                            resultModel = JsonConvert.DeserializeObject<ResultModel<T>>(res.Content);
                        }

                        break;

                    case 422: //Unprocessable Entity
                        resultModel = JsonConvert.DeserializeObject<ResultModel<T>>(res.Content);
                        break;

                    case (int)HttpStatusCode.OK:
                        resultModel = JsonConvert.DeserializeObject<ResultModel<T>>(res.Content);
                        resultModel.IsSuccess = true;
                        break;

                    case 0: //ResponseStatus is error: network is down, failed DNS lookup, unable remote to server, request timeout, ...
                        resultModel.Message = res.ErrorMessage;
                        if (res.ResponseStatus == ResponseStatus.TimedOut)
                        {
                            resultModel.Message = PhoenixWebAPIDefaultErrorMessage.RequestTimeout;
                        }
                        break;

                    case (int)HttpStatusCode.GatewayTimeout:
                    case (int)HttpStatusCode.RequestTimeout: //RequestTimeout
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.RequestTimeout;
                        break;

                    case (int)HttpStatusCode.InternalServerError:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.InternalServerError;
                        break;

                    case (int)HttpStatusCode.ServiceUnavailable:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.ServiceUnavailable;
                        break;

                    default:
                        resultModel.Message = PhoenixWebAPIDefaultErrorMessage.UnknownError;
                        break;
                }
            }
            catch (Exception e)
            {
                resultModel.Message = PhoenixWebAPIDefaultErrorMessage.SystemError;

                if (e.Source == "Newtonsoft.Json")
                {
                    resultModel.Message = PhoenixWebAPIDefaultErrorMessage.CanNotDeserializeJsonToObject;
                }
            }
            return resultModel;
        }

        private string GetAccessToken()
        {
            string accessToken = string.Empty;

            if (MemoryCache.Default.Get("Auth0AccessToken") != null)
            {
                accessToken = MemoryCache.Default.Get("Auth0AccessToken").ToString();

                DateTime.TryParse(MemoryCache.Default.Get("Auth0AccessTokenExpiryDate").ToString(), out DateTime expiryDate);

                if (expiryDate.Subtract(DateTime.Now).Seconds > 0)
                {
                    return accessToken;
                }
            }

            var result = GetAccessTokenFromAuth0();
            accessToken = result.access_token;

            MemoryCache.Default.Set("Auth0AccessToken", result.access_token, null);
            MemoryCache.Default.Set("Auth0AccessTokenExpiryDate", DateTime.Now.AddSeconds(result.expires_in - 60), null);

            return accessToken;
        }

        private Auth0TokenModel GetAccessTokenFromAuth0()
        {
            var client = new RestClient(Caching.GetSystemConfigurationByKey(PhoenixAPISystemConfigurationKeyEnum.PhoenixAPI_Auth0_URL_GetAccessToken.ToString()));
            var request = new RestRequest(Method.POST);

            var strContent = new
            {
                client_id = Caching.GetSystemConfigurationByKey(PhoenixAPISystemConfigurationKeyEnum.PhoenixAPI_Auth0_Client_ID.ToString()),
                client_secret = Caching.GetSystemConfigurationByKey(PhoenixAPISystemConfigurationKeyEnum.PhoenixAPI_Auth0_Client_Secret.ToString()),
                audience = Caching.GetSystemConfigurationByKey(PhoenixAPISystemConfigurationKeyEnum.PhoenixAPI_Auth0_Audience.ToString()),
                grant_type = "client_credentials"
            };

            request.AddJsonBody(strContent);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<Auth0TokenModel>(response.Content);
            }

            return new Auth0TokenModel();
        }
    }

    public class Auth0TokenModel
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
    }

    public class ResultModel<T>
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class DataSearchResponse<T>
    {
        public IList<T> rows { get; set; }
        public int total { get; set; }
        public int page { get; set; }
        public int records { get; set; }
    }

    public class DataSearchResponseWithExtensionObject<T, T1> : DataSearchResponse<T>
    {
        public T1 extensionObject { get; set; }
    }

    public enum PhoenixAPISystemConfigurationKeyEnum
    {
        PhoenixAPIBaseURL,
        PhoenixAPI_Auth0_Client_ID,
        PhoenixAPI_Auth0_Client_Secret,
        PhoenixAPI_Auth0_Audience,
        PhoenixAPI_Auth0_URL_GetAccessToken
    }
}