﻿using Amazon.S3;
using System;
using System.IO;
using Amazon.S3.Transfer;
using Amazon;
using Amazon.S3.Model;
using log4net;
using RestSharp.Extensions;

namespace LGHelper
{
    public class S3
    {
        private static ILog logger = LogManager.GetLogger("S3");
        /// <summary>
        /// Upload file from PhoenixWeb to S3, if debug mode, the real file won't upload to S3 and only uploaded file name is response.
        /// </summary>
        /// <param name="fileToUpload">File stream</param>
        /// <param name="originalFilename">File name</param>
        /// <param name="s3BucketName">S3 bucket name</param>
        /// <returns></returns>
        public static string UploadFile(Stream fileToUpload, string originalFilename, string s3BucketName)
        {
            try
            {
                logger.Info("Start upload file -- UploadFile with stream");
                var uploadedName = $"{originalFilename}.{Guid.NewGuid().ToString().Replace("-", "")}";
#if DEBUG
                logger.Info("In Debug mode");
                return uploadedName;
#endif
                //Build S3 config
                logger.Info("Build s3 configuration");
                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast2);
                var fileTransferUtility = new TransferUtility(s3Client);
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest();
                logger.Info("Finished build s3 configuration");
                // Specify advanced settings. Upload data from a type of System.IO.Stream.
                fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = s3BucketName,
                    InputStream = fileToUpload,
                    StorageClass = S3StorageClass.Standard,
                    Key = uploadedName
                };
                logger.Info($"Upload file to S3 -- start -- s3BucketName = {s3BucketName}, uploadedName = {uploadedName}");
                fileTransferUtility.UploadAsync(fileTransferUtilityRequest).Wait();
                logger.Info("Upload file to S3 -- End");

                return uploadedName;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                logger.Info("End upload file");
            }
        }

        public static string UploadFile(string fileContent, string originalFilename, string s3BucketName)
        {
            try
            {
                logger.Info("Start upload file -- UploadFile with FileContent");
                var uploadedName = $"{originalFilename}.{Guid.NewGuid().ToString().Replace("-", "")}";
#if DEBUG
                logger.Info("In Debug mode");
                return uploadedName;
#endif
                //Build S3 config
                logger.Info("Build s3 configuration");
                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast2);
                var fileTransferUtility = new TransferUtility(s3Client);
                var fileTransferUtilityRequest = new TransferUtilityUploadRequest();
                logger.Info("Finished build s3 configuration");
                // Specify advanced settings. Upload data from a type of System.IO.Stream.
                fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = s3BucketName,
                    InputStream = GenerateStreamFromString(fileContent),
                    StorageClass = S3StorageClass.Standard,
                    Key = uploadedName
                };

                logger.Info($"Upload file to S3 -- start -- s3BucketName = {s3BucketName}, uploadedName = {uploadedName}");
                fileTransferUtility.UploadAsync(fileTransferUtilityRequest).Wait();
                logger.Info("Upload file to S3 -- End");
                return uploadedName;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
            finally
            {
                logger.Info("End upload file");
            }
        }


        public static string GetFileFromS3AndResponseToString(string fileName, string s3BucketName)
        {
            string responseBody = "";
            try
            {
                logger.Info($"Start to get data from S3 of file {fileName} and bucketname = {s3BucketName}");
                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast2);
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = s3BucketName,
                    Key = fileName
                };
                using (GetObjectResponse response = s3Client.GetObject(request))
                using (Stream responseStream = response.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    responseBody = reader.ReadToEnd(); // Now you process the response body.
                }
            }
            catch (AmazonS3Exception e)
            {
                logger.FatalFormat("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                logger.FatalFormat("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            finally
            {
                logger.Info($"Finished getting data from S3 of file {fileName}");
            }
            return responseBody;
        }

        public static byte[] GetFileFromS3AndResponseToByte(string fileName, string s3BucketName)
        {
            try
            {
                logger.Info($"Start to get data from S3 of file {fileName}");
                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast2);
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = s3BucketName,
                    Key = fileName
                };
                using (GetObjectResponse response = s3Client.GetObject(request))
                {
                    logger.Info($"Finished getting data from S3 of file {fileName}");
                    return response.ResponseStream.ReadAsBytes();
                }
            }
            catch (AmazonS3Exception e)
            {
                logger.FatalFormat("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                logger.FatalFormat("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            return null;
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
