﻿using Phoenix_BusLog;

namespace LGHelper
{
    public abstract class RestAPI
    {
        protected string _baseURL = string.Empty;
        protected string _clientId = string.Empty;
        protected string _clientSecret = string.Empty;
        public void CreateAPI(string url, string clientId, string clientSecret)
        {
            _clientId = Caching.GetSystemConfigurationByKey(clientId);
            _clientSecret = Caching.GetSystemConfigurationByKey(clientSecret);
            _baseURL = Caching.GetSystemConfigurationByKey(url);
        }
    }
}
