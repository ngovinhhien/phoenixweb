﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Core;

namespace LGHelper.Logger
{
    public static class Logger
    {
        public static ILog GetLogger(string className)
        {
            return LogManager.GetLogger(className);
        }
    }
}
