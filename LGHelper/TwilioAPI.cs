﻿using log4net;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Web.Mvc;

namespace LGHelper
{
    /// <summary>
    /// This is class to handling the Twilio integration
    /// </summary>
    public class TwilioAPI
    {
        /// <summary>
        /// This is base url for twilio api.
        /// </summary>
        string _baseURL = string.Empty;

        /// <summary>
        /// This is authorization key from twilio
        /// </summary>
        string _authyKey = string.Empty;

        /// <summary>
        /// This is country code
        /// </summary>
        string _countryCode = string.Empty;

        /// <summary>
        /// This is type of sending sms.
        /// </summary>
        string _via = string.Empty;

        /// <summary>
        /// This is code length when sending sms verification code
        /// </summary>
        string _codeLength = string.Empty;

        /// <summary>
        /// Log4net instance.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger("TwilioAPI");

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="model">SMSVerificationModel</param>
        public TwilioAPI(SMSModel model)
        {
            _baseURL = model.BaseURL;
            _authyKey = model.AuthyKey;
            _countryCode = model.CountryCode;
            _via = model.Via;
            _codeLength = model.CodeLength;
        }

        /// <summary>
        /// Dynamic method to handle execute request and sending to twilio, after that received the response and mapping to generic type
        /// </summary>
        /// <typeparam name="T">Expected object Type</typeparam>
        /// <param name="request">RestRequest</param>
        /// <returns>Expected object Type</returns>
        public T Execute<T>(RestRequest request) where T : new()
        {
            logger.Info($"Start Execute Request {_baseURL}{request.Resource}");
            var client = new RestClient();
            client.BaseUrl = new Uri(_baseURL);
            var response = client.Execute<T>(request);
            logger.Info($"Response Content: {response.Content}");
            logger.Info($"End Execute Request {_baseURL}{request.Resource}");
            return response.Data;
        }

        /// <summary>
        /// Send SMS Verification Code function
        /// </summary>
        /// <param name="phoneNumber">This is phone number, which need to be verified. The phone number must start with 04 and length is 10</param>
        public void SendSMSVerificationCode(string phoneNumber, bool isEnableTwilio)
        {
            //Validate phone number is correct or not.
            phoneNumber = phoneNumber?.Trim();
            ValidatePhone(phoneNumber);
            if(isEnableTwilio)
            {
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("X-Authy-API-Key", _authyKey);
                var parameter = $"via={_via}&phone_number={phoneNumber}&country_code={_countryCode}&code_length={_codeLength}";
                request.AddParameter("", parameter, ParameterType.RequestBody);

                var response = this.Execute<TwilioAPIResponse>(request);

                if (response == null || !response.Success)
                {
                    throw new Exception(response?.Message ?? "System Error");
                }
            }
        }

        /// <summary>
        /// Validate SMS verification code
        /// </summary>
        /// <param name="phoneNumber">This is phone number, which received the sms verification code. The phone number must start with 04 and length is 10</param>
        /// <param name="verificationCode">This is verification code, sent by Twilio</param>
        public void ValidateSMSVerificationCode(string phoneNumber, string verificationCode, bool isEnableTwilio)
        {
            //Validate phone number is correct or not.
            phoneNumber = phoneNumber?.Trim();
            ValidatePhone(phoneNumber);
            if(isEnableTwilio)
            {
                var request = new RestRequest(Method.GET);
                request.AddHeader("X-Authy-API-Key", _authyKey);

                request.AddParameter("country_code", _countryCode);
                request.AddParameter("phone_number", phoneNumber);
                request.AddParameter("verification_code", verificationCode);

                var response = this.Execute<TwilioAPIResponse>(request);

                if (response == null || !response.Success)
                {
                    throw new Exception(response?.Message ?? "System Error");
                }
            }
        }

        private void ValidatePhone(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber) ||
                (phoneNumber.Length < 10 || phoneNumber.Length > 11) ||
                (phoneNumber.Length == 10 && phoneNumber.Substring(0, 2) != "04") ||
                (phoneNumber.Length == 11 && phoneNumber.Substring(0, 3) != "614" ))
            {
                throw new Exception("The phone number not correct, please input another one.");
            }
        }
    }

    

    /// <summary>
    /// This is SMS model contain all information of sms verification
    /// </summary>
    public class SMSModel
    {
        /// <summary>
        /// Base URL, you can get from web.config
        /// </summary>
        public string BaseURL { get; set; }

        /// <summary>
        /// Send sms via type, you can get from web.config
        /// </summary>
        public string Via { get; set; }

        /// <summary>
        /// Twilio country code, you can get from web.config
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Twilio code length, you can get from web.config
        /// </summary>
        public string CodeLength { get; set; }

        /// <summary>
        /// Twilio authorization key, you can get from web.config
        /// </summary>
        public string AuthyKey { get; set; }
    }

    /// <summary>
    /// Twilio responsed object
    /// </summary>
    public class TwilioAPIResponse
    {
        /// <summary>
        /// Success or not
        /// </summary>
        [DeserializeAs(Name = "success")]
        public bool Success { get; set; }

        /// <summary>
        /// Carrier
        /// </summary>
        [DeserializeAs(Name = "carrier")]
        public string Carrier { get; set; }

        /// <summary>
        /// IsCellPhone
        /// </summary>
        [DeserializeAs(Name = "is_cellphone")]
        public bool IsCellPhone { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DeserializeAs(Name = "message")]
        public string Message { get; set; }
    }
}