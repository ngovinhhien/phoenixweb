﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using RestSharp.Serialization.Json;
using System;

namespace LGHelper
{
    public class MarketoAPI
    {
        private readonly ILog logger = Logger.Logger.GetLogger("MarketoAPI");

        /// <summary>
        /// This is base url of marketo api.
        /// </summary>
        private string _baseURL;
        private string _clientId;
        private string _grantType;
        private string _clientSecretKey;
        private bool _enableSendingDataToMarketoFunction;

        public MarketoAPI(string grantType, string clientId, string clientSecretKey, string baseURL, bool enableSendingDataToMarketoFunction)
        {
            _grantType = grantType;
            _clientId = clientId;
            _clientSecretKey = clientSecretKey;
            _baseURL = baseURL;
            _enableSendingDataToMarketoFunction = enableSendingDataToMarketoFunction;
        }

        public void Execute(RestRequest request)
        {
            if (_enableSendingDataToMarketoFunction)
            {
                var client = new RestClient();
                client.BaseUrl = new Uri(_baseURL);
                string bearerToken = GetBearerToken();
                client.DefaultParameters.Clear();
                client.Authenticator = new MarketoBearerAuthenticator(bearerToken);

                var response = client.Execute(request);
                if (!response.IsSuccessful)
                {
                    logger.Error(response.Content);
                    var error = JsonConvert.DeserializeObject<MarketoError>(response.Content);
                    throw new Exception(error.ErrorDescription);
                }
            }
            else
            {
                logger.Info("Not sending data to Marketo because the flag enableSendingDataToMarketoFunction is false");
            }
        }

        private string GetBearerToken()
        {
            var client = new RestClient(_baseURL);
            var request = new RestRequest($"/identity/oauth/token?grant_type={_grantType}&client_id={_clientId}&client_secret={_clientSecretKey}", Method.GET);
            var response = client.Execute(request);

            JsonDeserializer dz = new JsonDeserializer();

            MarketoToken res = dz.Deserialize<MarketoToken>(response);

            if (res != null && !String.IsNullOrEmpty(res.AccessToken))
            {
                return res.AccessToken;
            }
            return null;
        }
    }

    class MarketoBearerAuthenticator : IAuthenticator
    {
        string bearerToken = "";
        public MarketoBearerAuthenticator(string _bearerToken)
        {
            this.bearerToken = "Bearer " + _bearerToken;
        }
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            client.AddDefaultHeader("Authorization", bearerToken);
        }
    }

    class MarketoToken
    {
        [DeserializeAs(Name = "access_token")]
        public string AccessToken { get; set; }
        [DeserializeAs(Name = "token_type")]
        public string TokenType { get; set; }
    }

    class MarketoError
    {
        [DeserializeAs(Name = "error")]
        public string Error { get; set; }
        [DeserializeAs(Name = "error_description")]
        public string ErrorDescription { get; set; }
    }
}