﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LGHelper
{
    public interface iRestAPI
    {
        IRestResponse Execute(RestRequest request);
    }
}
