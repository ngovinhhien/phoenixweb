﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using RestSharp.Serialization.Json;
using System;
using System.Runtime.Caching;

namespace LGHelper
{
    public class Auth0ManagementAPI
    {
        ILog log = LogManager.GetLogger("Auth0ManagementAPI");

        private string BaseURL;

        private readonly string _auth0ClientId;
        private readonly string _auth0ClientSecretKey;
        private readonly string _auth0Audience;
        private readonly string _auth0BaseURL;

        public Auth0ManagementAPI(string auth0ClientId, string auth0ClientSecretKey, string auth0Audience, string auth0BaseURL, string baseURL)
        {
            _auth0ClientId = auth0ClientId;
            _auth0ClientSecretKey = auth0ClientSecretKey;
            _auth0Audience = auth0Audience;
            _auth0BaseURL = auth0BaseURL;
            BaseURL = baseURL;
        }

        /// <summary>
        /// Execute the request with response object as defined
        /// </summary>
        /// <typeparam name="T">Response Type</typeparam>
        /// <param name="request">Auth0 Restsharp Request</param>
        /// <returns>Response Type as object after convert from Auth0 </returns>
        public T Execute<T>(RestRequest request) where T : new()
        {
            log.Info($"Start Execute Request {BaseURL}{request.Resource}");
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseURL);
            string bearerToken = GetBearerToken();
            client.DefaultParameters.Clear();
            client.Authenticator = new Auth0BearerAuthenticator(bearerToken);

            var response = client.Execute<T>(request);
            if (!response.IsSuccessful)
            {
                log.Error($"Error occurred while execute request {JsonConvert.SerializeObject(request)}.\nThe response is : \n {JsonConvert.SerializeObject(response)}");
                log.Fatal($"Error occurred while execute request {response.Content}");
                var error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                if (error == null || string.IsNullOrEmpty(error.Description))
                {
                    throw new Exception("Error occurred while execute request");
                }
                throw new Exception(error.Description);
            }
            log.Info($"End Execute Request {BaseURL}{request.Resource}");
            return response.Data;
        }

        /// <summary>
        /// Execute Auth0 request without response type
        /// </summary>
        /// <param name="request">Auth0 Restsharp Request</param>
        /// <returns>The result of request</returns>
        public bool Execute(RestRequest request)
        {
            log.Info($"Start Execute Request {BaseURL}{request.Resource}");
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseURL);
            string bearerToken = GetBearerToken();
            client.Authenticator = new Auth0BearerAuthenticator(bearerToken);
            var response = client.Execute(request);
            if (!response.IsSuccessful)
            {
                log.Error($"Error occurred while execute request {JsonConvert.SerializeObject(request)}.\nThe response is : \n {JsonConvert.SerializeObject(response)}");
                log.Fatal($"Error occurred while execute request {response.Content}");
                var error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                if (error == null || string.IsNullOrEmpty(error.Description))
                {
                    throw new Exception("Error occurred while execute request");
                }
                throw new Exception(error.Description);
            }
            log.Info($"End Execute Request {BaseURL}{request.Resource}");
            return response.IsSuccessful;
        }

        /// <summary>
        /// Auth0 Bearer authentication 
        /// </summary>
        protected class Auth0BearerAuthenticator : IAuthenticator
        {
            string bearerToken = "";
            public Auth0BearerAuthenticator(string _bearerToken)
            {
                this.bearerToken = "Bearer " + _bearerToken;
            }
            public void Authenticate(IRestClient client, IRestRequest request)
            {
                client.AddDefaultHeader("Authorization", bearerToken);
            }
        }

        /// <summary>
        /// Get Auth0 Bearer Token
        /// </summary>
        /// <returns></returns>
        private string GetBearerToken()
        {
            var fuseAuthenticationAccessToken = MemoryCache.Default.Get("Auth0ManagementAPIAccessToken");
            if (fuseAuthenticationAccessToken != null)
            {
                var accessToken = fuseAuthenticationAccessToken.ToString();

                DateTime.TryParse(MemoryCache.Default.Get("Auth0ManagementAPIAccessTokenExpiryDate")?.ToString(), out DateTime expiryDate);

                if (expiryDate.Subtract(DateTime.Now).Seconds > 0)
                {
                    return accessToken;
                }
            }

            var client = new RestClient(_auth0BaseURL);
            var request = new RestRequest("oauth/token", Method.POST);
            var strContent = new
            {
                client_id = _auth0ClientId,
                client_secret = _auth0ClientSecretKey,
                audience = _auth0Audience,
                grant_type = "client_credentials"
            };

            request.AddJsonBody(strContent);
            var response = client.Execute(request);

            JsonDeserializer dz = new JsonDeserializer();

            Auth0BearerTokenResponse res = dz.Deserialize<Auth0BearerTokenResponse>(response);

            if (res != null && !string.IsNullOrEmpty(res.AccessToken))
            {
                MemoryCache.Default.Set("Auth0ManagementAPIAccessToken", res.AccessToken, null);
                MemoryCache.Default.Set("Auth0ManagementAPIAccessTokenExpiryDate", DateTime.Now.AddSeconds(res.ExpiresIn - 60), null);
                return res.AccessToken;
            }
            return null;
        }

        /// <summary>
        /// This is object as mapping response from Auth0
        /// </summary>
        private class Auth0BearerTokenResponse
        {
            [DeserializeAs(Name = "access_token")]
            public string AccessToken { get; set; }
            [DeserializeAs(Name = "scope")]
            public string Scope { get; set; }
            [DeserializeAs(Name = "expires_in")]
            public int ExpiresIn { get; set; }
            [DeserializeAs(Name = "token_type")]
            public string TokenType { get; set; }

        }
    }

    public abstract class NewUserModel
    {

        [DeserializeAs(Name = "username")]
        public string UserName { get; set; }
        [DeserializeAs(Name = "email")]
        public string Email { get; set; }
        [DeserializeAs(Name = "user_metadata")]
        public UserMetaData Metadata { get; set; }
    }

    public class NewUserResponse : NewUserModel
    {
        [DeserializeAs(Name = "user_id")]
        public string UserID { get; set; }
    }

    public class UserMetaData
    {
        [DeserializeAs(Name = "member_key")]
        public string MemberKey { get; set; }
    }

    public class ErrorResponse
    {
        [DeserializeAs(Name = "name")]
        public string Name { get; set; }
        [DeserializeAs(Name = "code")]
        public string Code { get; set; }
        [DeserializeAs(Name = "description")]
        public string Description { get; set; }
        [DeserializeAs(Name = "statusCode")]
        public string StatusCode { get; set; }
    }
}