//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusinessCardType
    {
        public BusinessCardType()
        {
            this.MemberSplitPayments = new HashSet<MemberSplitPayment>();
        }
    
        public int BusinessCardTypeKey { get; set; }
        public Nullable<int> Paymethod_key { get; set; }
        public Nullable<int> VehicleType_key { get; set; }
    
        public virtual PayMethod PayMethod { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        public virtual ICollection<MemberSplitPayment> MemberSplitPayments { get; set; }
    }
}
