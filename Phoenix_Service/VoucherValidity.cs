//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class VoucherValidity
    {
        public VoucherValidity()
        {
            this.Vouchers = new HashSet<Voucher>();
        }
    
        public int VoucherValidity_key { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<Voucher> Vouchers { get; set; }
    }
}
