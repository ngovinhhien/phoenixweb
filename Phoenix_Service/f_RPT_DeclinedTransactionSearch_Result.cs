//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class f_RPT_DeclinedTransactionSearch_Result
    {
        public long id { get; set; }
        public string batchid { get; set; }
        public string batchstatus { get; set; }
        public string transid1 { get; set; }
        public string merchantid { get; set; }
        public string merchantname { get; set; }
        public long driverid { get; set; }
        public System.DateTime loaded { get; set; }
        public System.DateTime startshift { get; set; }
        public System.DateTime endshift { get; set; }
        public string batchnumber { get; set; }
        public string taxiid { get; set; }
        public string terminalid { get; set; }
        public string transtype { get; set; }
        public string trans_status { get; set; }
        public string cardname { get; set; }
        public string transpan { get; set; }
        public System.DateTime transstart { get; set; }
        public System.DateTime transend { get; set; }
        public float transfare { get; set; }
        public float transextras { get; set; }
        public float transtip { get; set; }
        public float transtotalamount { get; set; }
        public int transauthorisationnumber { get; set; }
        public int transpassengers { get; set; }
        public float transtarrif { get; set; }
        public float transkilometers { get; set; }
        public string transpickupzone { get; set; }
        public string transdropoffzone { get; set; }
        public string sourcefile { get; set; }
        public Nullable<System.DateTime> recordcreated { get; set; }
        public Nullable<System.DateTime> filedate { get; set; }
        public string HostBatchNumber { get; set; }
        public Nullable<decimal> HostServiceCharge { get; set; }
        public Nullable<decimal> HostGST { get; set; }
        public Nullable<long> HostTransactionHistoryKey { get; set; }
        public string TransactionDescription { get; set; }
        public string UserID { get; set; }
        public string BusinessID { get; set; }
        public string HostTransactionNumber { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string ABN { get; set; }
        public string CardEntryMode { get; set; }
        public string CellTowerMCC { get; set; }
        public string CellTowerMNC { get; set; }
        public string CellTowerID { get; set; }
        public string CellTowerLAC { get; set; }
        public string CardHolderName { get; set; }
        public string CardExpiry { get; set; }
        public Nullable<decimal> AutoTipAmount { get; set; }
        public Nullable<decimal> AutoTipFee { get; set; }
        public Nullable<System.DateTime> TransactionPaymentTime { get; set; }
    }
}
