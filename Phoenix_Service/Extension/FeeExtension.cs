﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Fee;

namespace Phoenix_Service.Extension
{
    public static class FeeExtension
    {
        public static FeeChargeDetailModel ToFeeChargeDetailModel(this FeeChargeDetail feeChargeDetail)
        {
            return new FeeChargeDetailModel()
            {
                MemberKey =  feeChargeDetail.MemberKey,
                Amount = feeChargeDetail.Amount,
                IsActive = feeChargeDetail.IsActive,
                FeeChargeDetailID = feeChargeDetail.ID,
                MinAmount = feeChargeDetail.Fee.MinAmount,
                MaxAmount = feeChargeDetail.Fee.MaxAmount,
                StartDate = feeChargeDetail.FeeChargeStartDate.Value.ToString("dd/MM/yyyy")
            };
        }

    }
}
