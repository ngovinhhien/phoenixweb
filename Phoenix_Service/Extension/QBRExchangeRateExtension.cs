﻿using System;

namespace Phoenix_Service.Extension
{
    public static class QBRExchangeRateExtension
    {
        public static void CopyQbrExchangeRate(this QBRExchangeRate orginalObject, QBRExchangeRate qbrExchangeRate)
        {
            orginalObject.QantasExchangeRate = qbrExchangeRate.QantasExchangeRate;
            orginalObject.Name = qbrExchangeRate.Name;
            orginalObject.CampaignStartDate = qbrExchangeRate.CampaignStartDate;
            orginalObject.CampaignEndDate = qbrExchangeRate.CampaignEndDate != null ? qbrExchangeRate.CampaignEndDate.Value.AddDays(1).AddSeconds(-1) : qbrExchangeRate.CampaignEndDate;
            orginalObject.RegisterStartDate = qbrExchangeRate.RegisterStartDate;
            orginalObject.RegisterEndDate = qbrExchangeRate.RegisterEndDate != null ? qbrExchangeRate.RegisterEndDate.Value.AddDays(1).AddSeconds(-1) : qbrExchangeRate.RegisterEndDate;
            orginalObject.RegisterEffectedDays = qbrExchangeRate.RegisterEffectedDays;
            orginalObject.GroupID = qbrExchangeRate.GroupID;
        }

        public static void CopyGroup(this Group orginalObject, Group group)
        {
            orginalObject.GroupName = group.GroupName;
            orginalObject.IsActive = group.IsActive;
            orginalObject.UpdatedDate = DateTime.Now;
            orginalObject.UpdatedBy = group.UpdatedBy;
        }
    }
}
