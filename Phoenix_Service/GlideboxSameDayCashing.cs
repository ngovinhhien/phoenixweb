//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class GlideboxSameDayCashing
    {
        public int GlideboxSameDayCashingID { get; set; }
        public Nullable<int> DocketKey { get; set; }
        public string TerminalID { get; set; }
        public string BatchTop { get; set; }
        public string BatchBottom { get; set; }
        public string BatchID { get; set; }
        public int MemberKey { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> TotalGlideboxAmount { get; set; }
        public Nullable<decimal> CommissionAmount { get; set; }
        public Nullable<decimal> TotalGlideboxCommission { get; set; }
        public Nullable<bool> IsPercentage { get; set; }
        public Nullable<System.DateTime> CreatedDatetime { get; set; }
        public string CreatedBy { get; set; }
        public string ShortName { get; set; }
    
        public virtual Member Member { get; set; }
    }
}
