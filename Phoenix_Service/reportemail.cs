//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class reportemail
    {
        public int PayRun_key { get; set; }
        public int Member_Key { get; set; }
        public string PayRunId { get; set; }
        public string MemberId { get; set; }
        public string PayRunType { get; set; }
        public string Email { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailComment { get; set; }
        public string ReportFormat { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int reportemail_id { get; set; }
    }
}
