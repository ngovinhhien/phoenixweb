//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class FloatDemomination
    {
        public FloatDemomination()
        {
            this.FloatTransactionDenoes = new HashSet<FloatTransactionDeno>();
        }
    
        public long FloatDenominationKey { get; set; }
        public string Description { get; set; }
        public decimal Conversion { get; set; }
    
        public virtual ICollection<FloatTransactionDeno> FloatTransactionDenoes { get; set; }
    }
}
