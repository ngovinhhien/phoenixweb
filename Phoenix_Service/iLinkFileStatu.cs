//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class iLinkFileStatu
    {
        public iLinkFileStatu()
        {
            this.iLinkFiles = new HashSet<iLinkFile>();
        }
    
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public Nullable<int> ListOrder { get; set; }
    
        public virtual ICollection<iLinkFile> iLinkFiles { get; set; }
    }
}
