//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class vCRMItem
    {
        public int CRMItem_Key { get; set; }
        public string Description { get; set; }
        public string CRMPriorityName { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> modified_dttm { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public string ItemTypeName { get; set; }
        public string Comments { get; set; }
        public string StatusName { get; set; }
        public string ActionName { get; set; }
        public string EmployeeName { get; set; }
        public string memberID { get; set; }
        public string TerminalID { get; set; }
        public Nullable<int> employee_key { get; set; }
        public string PhoenixAdminUsername { get; set; }
        public Nullable<int> crmstatus_key { get; set; }
        public Nullable<int> crmaction_key { get; set; }
        public Nullable<int> crmpriority_key { get; set; }
        public Nullable<int> crmitemtype_key { get; set; }
        public Nullable<int> eftterminal_key { get; set; }
        public Nullable<int> member_key { get; set; }
        public Nullable<int> ClosedCRMStatus_key { get; set; }
        public Nullable<int> DefaultCRMPriority_key { get; set; }
        public Nullable<int> InitialCRMAction_key { get; set; }
        public string state { get; set; }
        public Nullable<int> TerminalCount { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactInformation { get; set; }
        public Nullable<int> CRMSourceType_key { get; set; }
        public string SourceName { get; set; }
        public Nullable<int> Company_key { get; set; }
        public string BusinessType { get; set; }
    }
}
