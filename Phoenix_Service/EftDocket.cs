//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class EftDocket
    {
        public EftDocket()
        {
            this.QuarantineComments = new HashSet<QuarantineComment>();
        }
    
        public Nullable<long> BatchId { get; set; }
        public Nullable<System.DateTime> TripStart { get; set; }
        public Nullable<System.DateTime> TripEnd { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<int> Authorisation { get; set; }
        public Nullable<int> Passengers { get; set; }
        public Nullable<float> Tarrif { get; set; }
        public Nullable<float> Distance { get; set; }
        public string PickerZone { get; set; }
        public string DropoffZone { get; set; }
        public string CadmusFilename { get; set; }
        public string Status { get; set; }
        public string BatchNumber { get; set; }
        public Nullable<int> TransactionNumber { get; set; }
        public Nullable<bool> Presented { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public int EftDocket_key { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public Nullable<int> Operator_key { get; set; }
        public Nullable<int> Vehicle_key { get; set; }
        public Nullable<bool> Quarantined { get; set; }
        public Nullable<int> QuarantinedReasonKey { get; set; }
        public Nullable<System.DateTime> QuarantinedDate { get; set; }
        public Nullable<int> MemberReferenceEntryKey { get; set; }
        public Nullable<decimal> LevyCharge { get; set; }
    
        public virtual DocketTable DocketTable { get; set; }
        public virtual EftTerminal EftTerminal { get; set; }
        public virtual Operator Operator { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public virtual ICollection<QuarantineComment> QuarantineComments { get; set; }
    }
}
