//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class WebCashing_GetPendingBatchesDetailByMemberKey_Result
    {
        public string Description { get; set; }
        public string DocketBatchId { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
        public string BatchYear { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<decimal> Others { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> DocketDate { get; set; }
        public decimal PayableFaceValue { get; set; }
        public decimal CommissionRate { get; set; }
        public decimal PayableCommission { get; set; }
        public decimal PayableOthers { get; set; }
        public Nullable<decimal> TotalPayable { get; set; }
        public int Docket_key { get; set; }
        public Nullable<decimal> GlideboxCommission { get; set; }
        public Nullable<decimal> TotalGlideboxCommission { get; set; }
    }
}
