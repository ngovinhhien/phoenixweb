//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class FloatTransactionTypeOffice
    {
        public int OfficeKey { get; set; }
        public long FloatTransactionTypeKey { get; set; }
        public bool Active { get; set; }
    
        public virtual FloatTransactionType FloatTransactionType { get; set; }
        public virtual Office Office { get; set; }
    }
}
