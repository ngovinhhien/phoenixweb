//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class SaleCommissionPayment
    {
        public int SaleCommissionPayment_key { get; set; }
        public int EmployeeCommissionRate_key { get; set; }
        public int TerminalSale_key { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string PaymentReference { get; set; }
        public Nullable<bool> Processed { get; set; }
        public Nullable<System.DateTime> ProcessedDate { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> CalculationMonth { get; set; }
        public Nullable<int> CalculationYear { get; set; }
    
        public virtual EmployeeCommissionRate EmployeeCommissionRate { get; set; }
        public virtual TerminalSale TerminalSale { get; set; }
    }
}
