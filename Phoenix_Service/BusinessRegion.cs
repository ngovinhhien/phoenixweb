//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class BusinessRegion
    {
        public BusinessRegion()
        {
            this.Members = new HashSet<Member>();
        }
    
        public long BusinessRegion_Key { get; set; }
        public string BusinessRegionName { get; set; }
    
        public virtual ICollection<Member> Members { get; set; }
    }
}
