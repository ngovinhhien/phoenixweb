//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class Last28Days
    {
        public Nullable<int> operator_key { get; set; }
        public Nullable<int> Vehicle_key { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public System.DateTime EffectiveDate { get; set; }
        public Nullable<int> NumTrans { get; set; }
        public Nullable<decimal> TotalFare { get; set; }
        public string ST { get; set; }
        public Nullable<int> COMP { get; set; }
    }
}
