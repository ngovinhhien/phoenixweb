//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class PreOrderAction
    {
        public PreOrderAction()
        {
            this.Preorders = new HashSet<Preorder>();
            this.PreOrderActionStates = new HashSet<PreOrderActionState>();
        }
    
        public int PreOrderAction_key { get; set; }
        public Nullable<int> PreOrderStatus_key { get; set; }
        public string PreOrderActionName { get; set; }
    
        public virtual ICollection<Preorder> Preorders { get; set; }
        public virtual PreOrderStatu PreOrderStatu { get; set; }
        public virtual ICollection<PreOrderActionState> PreOrderActionStates { get; set; }
    }
}
