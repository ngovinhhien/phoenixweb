//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class vTransactionDetailsFull
    {
        public string Type { get; set; }
        public Nullable<System.DateTime> ItemDate { get; set; }
        public Nullable<decimal> RateUsed { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> TransactionDetailType { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> Docket_key { get; set; }
        public Nullable<int> Transaction_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public int key { get; set; }
    }
}
