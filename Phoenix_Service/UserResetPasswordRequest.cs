//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserResetPasswordRequest
    {
        public int UserResetPasswordRequestId { get; set; }
        public System.Guid UserId { get; set; }
        public System.Guid ResetPasswordToken { get; set; }
        public System.DateTime ExpiresBy { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string IPAddress { get; set; }
        public bool HasCompleted { get; set; }
        public string LastEmailUsed { get; set; }
    }
}
