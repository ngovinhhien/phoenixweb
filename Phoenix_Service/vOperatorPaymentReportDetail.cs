//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class vOperatorPaymentReportDetail
    {
        public Nullable<int> PayRun_key { get; set; }
        public string MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TerminalId { get; set; }
        public string VehicleId { get; set; }
        public int Docket_key { get; set; }
        public Nullable<long> EftDocketBatchId { get; set; }
        public string PayMethodName { get; set; }
        public Nullable<System.DateTime> DocketDate { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> FareReimbursement { get; set; }
        public Nullable<decimal> Commissions { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
    }
}
