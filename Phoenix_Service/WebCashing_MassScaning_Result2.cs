//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class WebCashing_MassScaning_Result2
    {
        public Nullable<int> Member_key { get; set; }
        public string DocketBatchId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedByUser { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public string BatchID { get; set; }
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public string state { get; set; }
        public string TerminalId { get; set; }
        public Nullable<decimal> BatchTotalPayable { get; set; }
        public string BatchNumber { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public string BatchStatus { get; set; }
        public Nullable<bool> IsCashable { get; set; }
        public string LodgementReferenceNo { get; set; }
        public Nullable<decimal> BatchTotal { get; set; }
        public Nullable<decimal> DocketTotalSummary { get; set; }
        public Nullable<decimal> FaceValueSummary { get; set; }
        public Nullable<decimal> ExtrasSummary { get; set; }
        public Nullable<decimal> CommissionSummary { get; set; }
        public Nullable<decimal> TotalBatchCommission { get; set; }
        public Nullable<decimal> TotalBatchFaceValue { get; set; }
        public Nullable<decimal> TotalBatchExtras { get; set; }
        public string PaidBy { get; set; }
        public Nullable<System.DateTime> PaidDate { get; set; }
        public Nullable<decimal> GrandTotalPaidAmount { get; set; }
        public string PaymentType { get; set; }
        public Nullable<decimal> TipPro { get; set; }
        public Nullable<decimal> TipProSummary { get; set; }
        public decimal TippingFee { get; set; }
        public Nullable<decimal> TippingFeeSummary { get; set; }
        public Nullable<decimal> TotalBatchTippingFee { get; set; }
    }
}
