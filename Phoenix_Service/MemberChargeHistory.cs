//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class MemberChargeHistory
    {
        public int MemberChargeHistory_key { get; set; }
        public Nullable<int> TerminalAllocation_key { get; set; }
        public Nullable<decimal> ChargeAmount { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> MemberChargeType_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<int> Office_key { get; set; }
    
        public virtual DocketTable DocketTable { get; set; }
        public virtual TerminalAllocation TerminalAllocation { get; set; }
        public virtual Office Office { get; set; }
        public virtual MemberChargeType MemberChargeType { get; set; }
        public virtual Member Member { get; set; }
    }
}
