//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeCommissionRate
    {
        public EmployeeCommissionRate()
        {
            this.SaleCommissionPayments = new HashSet<SaleCommissionPayment>();
        }
    
        public int EmployeeCommissionRate_key { get; set; }
        public int SaleCommissionRate_key { get; set; }
        public int Employee_key { get; set; }
        public Nullable<decimal> TransactionTarget { get; set; }
        public Nullable<double> BudgetTarget { get; set; }
        public Nullable<int> WaitingPeriod { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<decimal> CashValue { get; set; }
        public Nullable<double> RateValue { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<decimal> TransactionThreshold { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> StartPeriod { get; set; }
        public Nullable<int> EndPeriod { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual SaleCommissionRate SaleCommissionRate { get; set; }
        public virtual ICollection<SaleCommissionPayment> SaleCommissionPayments { get; set; }
    }
}
