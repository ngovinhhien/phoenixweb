//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesforceDetail
    {
        public int Id { get; set; }
        public string ClientID { get; set; }
        public string Secret { get; set; }
        public string UserName { get; set; }
        public string TokenPassword { get; set; }
    }
}
