//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class TerminalRentalRate
    {
        public TerminalRentalRate()
        {
            this.MemberTerminalRentals = new HashSet<MemberTerminalRental>();
            this.TerminalRentals = new HashSet<TerminalRental>();
            this.TerminalRentalHistories = new HashSet<TerminalRentalHistory>();
            this.TerminalRentalTemplates = new HashSet<TerminalRentalTemplate>();
        }
    
        public int TerminalRentalRate_key { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public decimal TransactionTarget { get; set; }
        public decimal RentAmount { get; set; }
        public Nullable<int> Company_key { get; set; }
        public Nullable<int> WaitingPeriod { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<int> RecurrenceKey { get; set; }
    
        public virtual Company Company { get; set; }
        public virtual ICollection<MemberTerminalRental> MemberTerminalRentals { get; set; }
        public virtual ICollection<TerminalRental> TerminalRentals { get; set; }
        public virtual ICollection<TerminalRentalHistory> TerminalRentalHistories { get; set; }
        public virtual ICollection<TerminalRentalTemplate> TerminalRentalTemplates { get; set; }
    }
}
