//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeeChargedRefund
    {
        public int ID { get; set; }
        public int FeeChargeHistoryID { get; set; }
        public Nullable<System.DateTime> ProcessedDate { get; set; }
        public decimal RefundAmount { get; set; }
        public Nullable<int> DocketKey { get; set; }
        public string TicketNumber { get; set; }
        public string Reason { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual FeeChargeHistory FeeChargeHistory { get; set; }
    }
}
