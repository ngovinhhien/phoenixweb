//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class DriverCard
    {
        public DriverCard()
        {
            this.DriverCardHistories = new HashSet<DriverCardHistory>();
        }
    
        public int ID { get; set; }
        public string AccountIdentifier { get; set; }
        public Nullable<int> StatusID { get; set; }
        public int MemberKey { get; set; }
    
        public virtual DriverCardStatu DriverCardStatu { get; set; }
        public virtual ICollection<DriverCardHistory> DriverCardHistories { get; set; }
    }
}
