//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProfileItemType
    {
        public ProfileItemType()
        {
            this.MemberProfiles = new HashSet<MemberProfile>();
            this.ProfileOptionLookups = new HashSet<ProfileOptionLookup>();
        }
    
        public int ProfileItemTypeId { get; set; }
        public string Question { get; set; }
        public string ReturnType { get; set; }
        public int ProfileItemOrder { get; set; }
        public bool IsDefault { get; set; }
        public int QuestionWidth { get; set; }
        public int ValueWidth { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public byte[] ModifiedByUser { get; set; }
    
        public virtual ICollection<MemberProfile> MemberProfiles { get; set; }
        public virtual ICollection<ProfileOptionLookup> ProfileOptionLookups { get; set; }
    }
}
