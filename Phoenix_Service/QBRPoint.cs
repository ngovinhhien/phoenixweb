//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class QBRPoint
    {
        public int ID { get; set; }
        public int Member_Key { get; set; }
        public Nullable<int> Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public Nullable<int> TotalSMS { get; set; }
        public Nullable<int> TotalPoints { get; set; }
        public Nullable<int> ExchangeRateID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    
        public virtual QBRExchangeRate QBRExchangeRate { get; set; }
    }
}
