//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class vVoucherType
    {
        public int vouchertype_key { get; set; }
        public string vouchername { get; set; }
        public int conversionvalue { get; set; }
        public int expiryperiod { get; set; }
        public int company_key { get; set; }
        public string voucherprefix { get; set; }
        public Nullable<int> nextnumber { get; set; }
        public int office_key { get; set; }
        public string officename { get; set; }
        public string state { get; set; }
        public string companyname { get; set; }
        public Nullable<bool> active { get; set; }
        public Nullable<System.DateTime> validfrom { get; set; }
        public Nullable<System.DateTime> validto { get; set; }
        public string termsconditions { get; set; }
        public int OverrideLimit { get; set; }
        public bool ExternalOrder { get; set; }
        public bool UsedLater { get; set; }
    }
}
