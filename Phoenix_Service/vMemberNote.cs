//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class vMemberNote
    {
        public int member_key { get; set; }
        public Nullable<System.DateTime> MaxNoteDate { get; set; }
        public Nullable<System.DateTime> MinNoteDate { get; set; }
        public Nullable<int> Warning { get; set; }
        public string Notes { get; set; }
    }
}
