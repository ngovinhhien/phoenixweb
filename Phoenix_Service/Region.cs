//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class Region
    {
        public Region()
        {
            this.EmployeeRegions = new HashSet<EmployeeRegion>();
            this.OperationStatusHistories = new HashSet<OperationStatusHistory>();
            this.Postcodes = new HashSet<Postcode>();
        }
    
        public int Region_key { get; set; }
        public string Region1 { get; set; }
        public string State { get; set; }
    
        public virtual ICollection<EmployeeRegion> EmployeeRegions { get; set; }
        public virtual ICollection<OperationStatusHistory> OperationStatusHistories { get; set; }
        public virtual ICollection<Postcode> Postcodes { get; set; }
    }
}
