//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuarantineComment
    {
        public int QuarantineComment_Key { get; set; }
        public Nullable<int> Eftdocket_key { get; set; }
        public string Comment { get; set; }
        public string UpdatedByUser { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
    
        public virtual EftDocket EftDocket { get; set; }
    }
}
