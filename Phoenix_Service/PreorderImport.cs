//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class PreorderImport
    {
        public int PreorderImport_key { get; set; }
        public string TradingName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Phone { get; set; }
        public string TerminalID { get; set; }
        public Nullable<bool> StoreABN { get; set; }
        public Nullable<bool> PrintABN { get; set; }
        public Nullable<bool> StoreAN { get; set; }
        public Nullable<bool> StoreTaxiID { get; set; }
        public Nullable<System.DateTime> SignupdateTime { get; set; }
        public string SalePerson { get; set; }
        public string MemberID { get; set; }
        public string Email { get; set; }
    }
}
