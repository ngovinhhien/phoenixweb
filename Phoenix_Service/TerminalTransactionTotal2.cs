//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class TerminalTransactionTotal2
    {
        public Nullable<int> tranmonth { get; set; }
        public Nullable<int> tranyear { get; set; }
        public string originalTerminalid { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> DeallocatedDate { get; set; }
        public Nullable<bool> CurrentlyAllocated { get; set; }
        public string TerminalId { get; set; }
        public string memberid { get; set; }
        public int Member_key { get; set; }
        public string vehicleid { get; set; }
        public Nullable<decimal> totaltranamount { get; set; }
        public int EftTerminal_key { get; set; }
        public Nullable<int> membercompany_key { get; set; }
        public Nullable<int> vehicletype_key { get; set; }
        public Nullable<int> terminalcompany_key { get; set; }
    }
}
