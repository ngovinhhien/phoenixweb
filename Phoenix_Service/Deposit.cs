//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class Deposit
    {
        public int DepositKey { get; set; }
        public Nullable<decimal> DepositAmount { get; set; }
        public Nullable<int> TerminalAllocation_key { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<bool> Waived { get; set; }
    
        public virtual DocketTable DocketTable { get; set; }
        public virtual TerminalAllocation TerminalAllocation { get; set; }
    }
}
