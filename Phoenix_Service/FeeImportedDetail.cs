//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeeImportedDetail
    {
        public long FeeImportedDetailID { get; set; }
        public int FeeImportedFileID { get; set; }
        public string MemberID { get; set; }
        public int MemberKey { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string CardType { get; set; }
        public string FeeType { get; set; }
        public string Description { get; set; }
        public int NumberOfTxn { get; set; }
        public decimal Rate { get; set; }
        public decimal FeeAmount { get; set; }
        public Nullable<int> MemberChargeHistoryKey { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual Member Member { get; set; }
        public virtual FeeImportedFile FeeImportedFile { get; set; }
    }
}
