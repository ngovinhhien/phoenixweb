//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class SPGetRewardPointsByMemberKey_Result
    {
        public int member_key { get; set; }
        public string memberID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TradingName { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> CurrentDate { get; set; }
        public decimal TotalTrans { get; set; }
        public int TotalPointsUsed { get; set; }
        public Nullable<decimal> TotalPoints { get; set; }
    }
}
