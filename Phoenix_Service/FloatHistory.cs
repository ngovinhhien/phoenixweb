//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    using System.Collections.Generic;
    
    public partial class FloatHistory
    {
        public long FloatKey { get; set; }
        public int Office_key { get; set; }
        public decimal Bank { get; set; }
        public decimal Safe { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public System.Guid UpdatedBy { get; set; }
        public Nullable<System.DateTime> ClosedDate { get; set; }
        public Nullable<System.Guid> ClosedBy { get; set; }
        public Nullable<System.DateTime> VerifiedDate { get; set; }
        public Nullable<System.Guid> VerifiedBy { get; set; }
        public decimal Sales { get; set; }
        public decimal EFT { get; set; }
        public decimal Manual { get; set; }
        public decimal CashAdvance { get; set; }
        public decimal PettyCash { get; set; }
        public decimal Others { get; set; }
        public decimal Adjustments { get; set; }
        public int Float100 { get; set; }
        public int Float50 { get; set; }
        public int Float20 { get; set; }
        public int Float10 { get; set; }
        public int Float5 { get; set; }
        public int Float2 { get; set; }
        public int Float1 { get; set; }
        public int Float50c { get; set; }
        public int Float20c { get; set; }
        public int Float10c { get; set; }
        public int Float5c { get; set; }
    
        public virtual Office Office { get; set; }
    }
}
