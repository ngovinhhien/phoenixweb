﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace Phoenix_Service
{
    public partial class TaxiEpayEntities : DbContext
    {
        public TaxiEpayEntities(int CommandTimeout) : this()
           
        {
            this.SetCommandTimeOut(CommandTimeout);
        }
        public void SetCommandTimeOut(int Timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = Timeout;
        }
    }
}
