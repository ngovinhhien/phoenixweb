//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class Portal_TransactionDetails_GetByMemberKey_Paid_Result
    {
        public string memberid { get; set; }
        public string PayStatus { get; set; }
        public string transtype { get; set; }
        public string TaxiId { get; set; }
        public string Terminalid { get; set; }
        public long Driverid { get; set; }
        public string BatchNumber { get; set; }
        public string DealerName { get; set; }
        public string Operatorname { get; set; }
        public Nullable<System.DateTime> ItemDate { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> ServiceCharge { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> DocketTotal { get; set; }
        public string CardName { get; set; }
        public string CardNumberMasked { get; set; }
        public decimal OperatorCommissionRate { get; set; }
        public decimal DealerCommissionRate { get; set; }
        public decimal OperatorCommission { get; set; }
        public decimal DealerCommission { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public string SourceFile { get; set; }
        public Nullable<System.DateTime> Paydate { get; set; }
    }
}
