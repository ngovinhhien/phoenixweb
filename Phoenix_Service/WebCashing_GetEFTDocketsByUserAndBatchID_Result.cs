//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_Service
{
    using System;
    
    public partial class WebCashing_GetEFTDocketsByUserAndBatchID_Result
    {
        public Nullable<int> Member_key { get; set; }
        public Nullable<System.DateTime> docketdate { get; set; }
        public int docket_key { get; set; }
        public string DocketBatchId { get; set; }
        public string CreatedByUser { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public string state { get; set; }
        public string TerminalId { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> BatchTotalPayable { get; set; }
        public Nullable<decimal> TotalPayable { get; set; }
        public string CardNumberMasked { get; set; }
        public string BatchNumber { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public string BatchStatus { get; set; }
        public Nullable<bool> IsCashable { get; set; }
    }
}
