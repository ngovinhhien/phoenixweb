﻿using LGMail.Implementation;
using LGMail.Interface;

namespace LGMail
{
    public static class LGEmail
    {
        public static IEmail GetEmailProvider(EmailType emailType)
        {
            switch (emailType)
            {
                case EmailType.SparkPost:
                    return new SparkPostEmail();
                default:
                    return null;
            }
        }
    }

    public enum EmailType
    {
        SparkPost,
        AnotherType
    }
}
