﻿namespace LGMail.Interface
{
    public interface IEmail
    {
        void SendEmail(dynamic mailDto);
        void SendEmailWithAttachment(dynamic mailDto);
    }
}
