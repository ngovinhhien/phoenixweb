﻿using LGHelper.Logger;
using LGMail.Interface;
using log4net;
using SparkPost;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace LGMail.Implementation
{
    class SparkPostEmail : IEmail
    {
        private readonly ILog logger = Logger.GetLogger("SparkPostEmail");
        public void SendEmail(dynamic mailDto)
        {
            try
            {
                System.Net.ServicePointManager.
                        SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                if (mailDto == null)
                {
                    logger.Fatal("Can not send email with Mail DTO object is null");
                    return;
                }

                var sparkPostAPIKey = mailDto.SparkPostAPIKey;
                var transmission = new Transmission();
                var properties = (IDictionary<string, object>) mailDto;
                if (!properties.ContainsKey("TemplateID"))
                {
                    throw new Exception("Template ID not available when sending email with SparkPost");
                }
                transmission.Content.TemplateId = mailDto.TemplateID;
                if (!properties.ContainsKey("Recipient"))
                {
                    throw new Exception("Recipient not available when sending email with SparkPost");
                }

                // add recipients who will receive your email
                var lstRecipient = mailDto.Recipient.Split(';');
                foreach (var rec in lstRecipient)
                {
                    var recipient = new Recipient
                    {
                        Address = new Address { Email = rec }

                    };
                    transmission.Recipients.Add(recipient);
                }

                if (properties.ContainsKey("CustomMetaData"))
                {
                    transmission.SubstitutionData = mailDto.CustomMetaData;
                }

                // create a new API client using your API key
                var client = new Client(sparkPostAPIKey);

                // if you do not understand async/await, use the sync sending mode:
                client.CustomSettings.SendingMode = SendingModes.Sync;
                var response = client.Transmissions.Send(transmission).Result;
                if (string.IsNullOrEmpty(response.Id))
                {
                    logger.Fatal($"Can not sending email to {mailDto.Recipient}. Please check the response status code from SparkPost {response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                logger.Fatal($"Can not sending email", e);
            }
            
        }

        public void SendEmailWithAttachment(dynamic mailDto)
        {
            try
            {
                if (mailDto == null)
                {
                    logger.Fatal("Can not send email with Mail DTO object is null");
                    return;
                }

                var sparkPostAPIKey = mailDto.SparkPostAPIKey;
                var transmission = new Transmission();
                var properties = (IDictionary<string, object>)mailDto;
                if (!properties.ContainsKey("TemplateID"))
                {
                    throw new Exception("Template ID not available when sending email with SparkPost");
                }
                if (!properties.ContainsKey("Recipient"))
                {
                    throw new Exception("Recipient not available when sending email with SparkPost");
                }

                // add recipients who will receive your email
                var lstRecipient = mailDto.Recipient.Split(';');
                foreach (var rec in lstRecipient)
                {
                    var recipient = new Recipient
                    {
                        Address = new Address { Email = rec }

                    };
                    transmission.Recipients.Add(recipient);
                }

                if (properties.ContainsKey("CustomMetaData"))
                {
                    transmission.SubstitutionData = mailDto.CustomMetaData;
                }

                if (properties.ContainsKey("Attachment"))
                {
                    var lstAttachment = new List<Attachment>();
                    lstAttachment.Add(mailDto.Attachment);
                    transmission.Content.Attachments = lstAttachment;
                }

                
                // create a new API client using your API key
                var client = new Client(sparkPostAPIKey);
                client.CustomSettings.SendingMode = SendingModes.Sync;

                //retrieve template html and set Content.Html
                Task<RetrieveTemplateResponse> templateResponse = client.Templates.Retrieve(mailDto.TemplateID);
                var templateContent = templateResponse.Result.TemplateContent;
                transmission.Content.Html = templateContent.Html;
                transmission.Content.From.Email = templateContent.From.Email;
                transmission.Content.From.Name = templateContent.From.Name;
                transmission.Content.Subject = templateContent.Subject;

                var response = client.Transmissions.Send(transmission).Result;
                if (string.IsNullOrEmpty(response.Id))
                {
                    logger.Fatal($"Can not sending email to {mailDto.Recipient}. Please check the response status code from SparkPost {response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                logger.Fatal($"Can not sending email", e);
            }

        }
    }
}
