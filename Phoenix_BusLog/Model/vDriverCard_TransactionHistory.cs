﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Model
{
    public class vDriverCard_TransactionHistory
    {
        public DateTime TransactionDate { get; set; }
        public decimal SentAmount { get; set; }
        public string PaymentStatus { get; set; }
        public int Transaction_key { get; set; }
        public string LodgementReferenceNo { get; set; }
        public decimal TotalAmount { get; set; }
        public string MemberId { get; set; }
        public int? OxygenTransactionID { get; set; }
    }
}
