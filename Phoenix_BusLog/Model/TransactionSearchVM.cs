using System;

namespace Phoenix_BusLog.Model
{
    public class TransactionSearchVM
    {
        public string DocketStatus { get; set; }
        public string paytype { get; set; }
        public DateTime? PayDate { get; set; }
        public string PaidTo { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
        public string Merchantid { get; set; }
        public string Taxi { get; set; }
        public long DriverId { get; set; }
        public DateTime StartShift { get; set; }
        public DateTime EndShift { get; set; }
        public string Status { get; set; }
        public string Authorisation { get; set; }
        public string CardType { get; set; }
        public string MaskedCardNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float Fare { get; set; }
        public float CashOut { get; set; }
        public float Extras { get; set; }
        public decimal ServiceFee { get; set; }
        public float TotalAmount { get; set; }
        public string Pickup { get; set; }
        public string DropOff { get; set; }
        public string SourceFile { get; set; }
        public string OfficeName { get; set; }
        public string Filedate { get; set; }
        public string ProcessedBy { get; set; }
        public long TransactionID { get; set; }
        public int IsApproved { get; set; }
        public string CardEntryMode { get; set; }
        public decimal? GlideboxAmount { get; set; }
        public decimal? GlideboxCommission { get; set; }
        public string BusinessTypeName { get; set; }
        public string AmexMID { get; set; }
        public DateTime TransactionDateTime { get; set; }
    }
}
