﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Model
{
    public class GlideboxCategoriesModel
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string ShortName { get; set; }
        public decimal Price { get; set; }
        public decimal CommissionAmount { get; set; }
        public int OrderCategory { get; set; }
    }
}
