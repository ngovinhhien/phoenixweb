﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Model
{
    public class vDriverCard_LoadPendingTransaction
    {
        public int? Transaction_key { get; set; }
        public string LodgementReferenceNo { get; set; }
        public int MemberKey { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string MemberId { get; set; }
        public int DriverCardID { get; set; }
        public string AccountIdentifier { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TransferredAmount { get; set; }
        public decimal? RemainingAmount { get; set; }
    }
}
