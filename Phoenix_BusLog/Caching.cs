﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Phoenix_BusLog
{
    public class Caching
    {
        private static ILog _logger = LogManager.GetLogger("Caching");

        public static void ReloadMemoryCache(TaxiEpayEntities entities = null)
        {
            try
            {
                _logger.Info("Start reload memory cache");
                //Clear all cache inside before reload
                foreach (var cacheKey in MemoryCache.Default.Select(n => n.Key))
                {
                    MemoryCache.Default.Remove(cacheKey);
                }

                var systemIDStr = System.Configuration.ConfigurationManager.AppSettings["SystemID"];
                var systemID = 0;
                if (int.TryParse(systemIDStr, out systemID))
                {

                    if (entities == null)
                    {
                        _logger.Info("Reload from new Entities context");
                        //Trying reload all items again.
                        using (var taxientities = new TaxiEpayEntities())
                        {
                            var systemConfiguration = taxientities.SystemConfigurations
                                .Where(n => n.SystemID == systemID).ToList().Distinct()
                                .ToDictionary(n => n.ConfigurationKey, n => n.ConfigurationValue);

                            foreach (var config in systemConfiguration)
                            {
                                if (!MemoryCache.Default.Contains(config.Key))
                                {
                                    MemoryCache.Default.Set(config.Key, config.Value, null);
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.Info("Reload from passing Entities context");
                        var systemConfiguration = entities.SystemConfigurations
                            .Where(n => n.SystemID == systemID).ToList().Distinct()
                            .ToDictionary(n => n.ConfigurationKey, n => n.ConfigurationValue);

                        foreach (var config in systemConfiguration)
                        {
                            if (!MemoryCache.Default.Contains(config.Key))
                            {
                                MemoryCache.Default.Set(config.Key, config.Value, null);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                while (e != null)
                {
                    _logger.Error(e.Message);
                    _logger.Error(e.StackTrace);
                    e = e.InnerException;
                }
                throw new Exception("Can not loading the system configuration");
            }
        }

        /// <summary>
        /// Get system configuration from memory cache. Also trying to reload one time if don't see configuration key before throw exception
        /// </summary>
        /// <param name="key">Configuration Key</param>
        /// <returns>Configuration value as string</returns>
        public static string GetSystemConfigurationByKey(string key, TaxiEpayEntities entities = null)
        {
            try
            {
                if (MemoryCache.Default.Contains(key))
                {
                    return MemoryCache.Default[key] as string;
                }

                //In case configuration key not available in memory cache, trying to reload cache again.
                ReloadMemoryCache(entities);

                //Trying to finding the latest one. If still doesn't finding that, throw exception.
                if (MemoryCache.Default.Contains(key))
                {
                    return MemoryCache.Default[key] as string;
                }
                throw new Exception($"The configuration key = {key} not available");
            }
            catch (Exception e)
            {
                while (e != null)
                {
                    _logger.Error(e.Message);
                    _logger.Error(e.StackTrace);
                    e = e.InnerException;
                }
                throw new Exception("Can not GetSystemConfigurationByKey");
            }

        }
    }
}
