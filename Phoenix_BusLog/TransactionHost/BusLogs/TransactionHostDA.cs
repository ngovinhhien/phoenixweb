﻿using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using PhoenixObjects.TransactionHost;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using System.Data.SqlClient;
using System.Data;
using Phoenix_TransactionHostUI;

namespace Phoenix_BusLog.TransactionHost.BusLogs
{
    public class TransactionHostDA : IDisposable
    {
        private TransactionHostEntities _context;
        public TransactionHostDA()
        {
            _context = new TransactionHostEntities();
        }
        public void AddNewSettlementLog(SettlementLog settlementLog, int logType)
        {
            settlementLog.LogType = logType;
            settlementLog.LogDateTime = DateTime.Now;
            settlementLog.UpdateDateTime = DateTime.Now;
            _context.SettlementLogs.Add(settlementLog);
        }
        public void AddNewSettlementAmount(SettlementAmount settlementAmount)
        {
            _context.SettlementAmounts.Add(settlementAmount);
        }


        public SettlementLog FromUnReconciledBatchesToSettlementLog(UnReconciledBatchesModel unReconciledBatches)
        {
            SettlementLog settlementLog = new SettlementLog();
            settlementLog.Batchnumber = unReconciledBatches.BatchNumber;
            settlementLog.TerminalID = unReconciledBatches.TerminalID;
            settlementLog.MerchantID = unReconciledBatches.MerchantID;
            settlementLog.TerminalSerialNo = unReconciledBatches.SerialNumber;
            return settlementLog;
        }

        public SettlementAmount FromUnReconciledBatchesToSettlementAmount(UnReconciledBatchesModel unReconciledBatches)
        {
            SettlementAmount settlementAmount = new SettlementAmount();
            settlementAmount.Batchnumber = unReconciledBatches.BatchNumber;
            settlementAmount.TerminalID = unReconciledBatches.TerminalID;
            settlementAmount.MerchantID = unReconciledBatches.MerchantID;
            settlementAmount.DebitNumber = unReconciledBatches.DebitNumber;
            settlementAmount.DebitAmount = unReconciledBatches.DebitAmount;
            settlementAmount.CreditAmount = unReconciledBatches.CreditAmount;
            settlementAmount.CreditNumber = unReconciledBatches.CreditNumber;
            settlementAmount.DebitReversalNumber = unReconciledBatches.DebitReversalNumber;
            settlementAmount.DebitReversalAmount = unReconciledBatches.DebitReversalAmount;
            settlementAmount.CreditReversalNumber = unReconciledBatches.CreditReversalNumber;
            settlementAmount.CreditReversalAmount = unReconciledBatches.CreditReversalAmount;
            settlementAmount.SettlementNetAmount = unReconciledBatches.SettlementNetAmount;
            return settlementAmount;
        }

        public SettlementHistory GetSettlementHistoryBy(string terminalID, string merchantID, string batchNumber)
        {

            return (from record in _context.SettlementHistories
                    where record.TerminalID == terminalID
                    && record.MerchantID == merchantID
                    && record.BatchNumber == batchNumber
                    select record).FirstOrDefault();
        }
        public void GetUnReconciledBatchesWithPaging(GridModel<SearchUnReconciledBatches, List<UnReconciledBatchesModel>> paging)
        {

            List<SqlParameter> sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@RowIDFrom", paging.PageIndexFrom));
            sqlParameters.Add(new SqlParameter("@RowIDTo", paging.PageIndexTo));
            sqlParameters.Add(new SqlParameter("@TerminalID", paging.Request.TerminalID ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@SerialNumber", paging.Request.SerialNumber ?? string.Empty));
            var totalItemsParam = new SqlParameter
            {
                ParameterName = "@TotalItems",
                Value = 0,
                Direction = ParameterDirection.Output
            };

            sqlParameters.Add(totalItemsParam);

            paging.Response = _context.Database.SqlQuery<UnReconciledBatchesModel>("EXEC GetUnReconciledBatches @TerminalID, @SerialNumber, @RowIDFrom, @RowIDTo, @TotalItems OUT", sqlParameters.ToArray()).ToList();

            int total = 0;
            int.TryParse(totalItemsParam.Value.ToString(), out total);

            paging.TotalItems = total;
        }


        public void GetUnReconciledTransactionsWithPaging(GridModel<SearchUnReconciledTransactions, List<UnReconciledTransactionsModel>> paging)
        {

            List<SqlParameter> sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@RowIDFrom", paging.PageIndexFrom));
            sqlParameters.Add(new SqlParameter("@RowIDTo", paging.PageIndexTo));
            sqlParameters.Add(new SqlParameter("@TerminalID", paging.Request.TerminalID ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@BatchNumber", paging.Request.BatchNumber ?? string.Empty));
            var totalItemsParam = new SqlParameter
            {
                ParameterName = "@TotalItems",
                Value = 0,
                Direction = ParameterDirection.Output
            };

            sqlParameters.Add(totalItemsParam);

            paging.Response = _context.Database.SqlQuery<UnReconciledTransactionsModel>("EXEC GetUnreconciledTransactions @TerminalID, @BatchNumber, @RowIDFrom, @RowIDTo, @TotalItems OUT", sqlParameters.ToArray()).ToList();

            int total = 0;
            int.TryParse(totalItemsParam.Value.ToString(), out total);

            paging.TotalItems = total;

        }
        public int Call_SP_ToAddSettlementData(string terminalID, string merchantID, string batchNumber, decimal batchTotal)
        {
            return _context.ManualConsolidateSettlementData(terminalID, merchantID, batchNumber, batchTotal);
        }

        public int Call_SP_PushBatchtoPhoenixes(int settlementHistoryKey, bool setLoaded)
        {
            return _context.RPT_ExtractTransactionsDirect(setLoaded, settlementHistoryKey);
        }


        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }
        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0 ? true : false;
        }
    }
}
