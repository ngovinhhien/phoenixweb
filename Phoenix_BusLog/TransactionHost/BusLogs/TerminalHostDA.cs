﻿using Phoenix_BusLog.TransactionHost.Model;
using Phoenix_TransactionHostUI;
using PhoenixObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.BusLogs
{
    public class TerminalHostDA : IDisposable
    {
        public List<TerminalModel> Terminal_ByID(string terminalID)
        {
            List<TerminalModel> TerminalModel = new List<TerminalModel>();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var terminals = from t in con.Terminals
                                where t.TerminalID.Equals(terminalID)
                                select t;
                if (terminals != null)
                {
                    foreach (var t in terminals)
                    {

                        var term = (from t2 in con.Terminals
                                    where t2.TerminalKey.Equals(t.TerminalKey)
                                    select t2).FirstOrDefault();


                        var terminaldefault = (from tr in con.View_GetTerminalConfigUI
                                               where tr.TerminalKey.Equals(t.TerminalKey)
                                               select tr).SingleOrDefault();
                        if (terminaldefault != null)
                        {
                            if (terminaldefault.GPRSReconnectTimer == null)
                            {
                                terminaldefault.GPRSReconnectTimer = 30;
                            }

                            // Terminal terminal = new Terminal();
                            term.InputTimeoutSec = terminaldefault.InputTimeoutsec;
                            term.BacklightSec = terminaldefault.BacklightSec;
                            term.SleepModeMin = terminaldefault.SleepModeMin;
                            term.ScreenBrightness = terminaldefault.ScreenBrightness;
                            // terminal.Sound = (bool)terminaldefault.Sound.Value;
                            //   terminal.RemeberMe = terminaldefault.RemeberMe.Value;
                            term.ABNSetting = terminaldefault.ABNSetting;
                            term.MinimumCharge = terminaldefault.MinimumCharge;
                            term.MaximumCharge = terminaldefault.MaximumCharge;
                            term.EOSTime = terminaldefault.EOSTime.ToString();
                            term.LoginMode = terminaldefault.LoginMode;
                            term.UserID = t.UserID;
                            term.BusinessID = t.BusinessID;
                            term.ABN = t.ABN;
                            term.NetworkTableStatus = terminaldefault.NetworkTableStatus;
                            //  terminal. = terminaldefault.Prefix;
                            //  terminal.l = terminaldefault.LiveBusinessUnit;
                            //  terminal.ter = terminaldefault.TermConfigVer;
                            term.GPRSReconnectTimer = Convert.ToInt16(terminaldefault.GPRSReconnectTimer.Value);
                            term.AppRespCode = terminaldefault.ApprovedResponseCode;
                            term.AppSignRespCode = terminaldefault.ApprovedSignatureResponseCode;
                            term.IsPreTranPing = terminaldefault.IsPreTranPing;
                            term.TranAdviceSendMode = terminaldefault.TranAdviceSendMode;
                            term.DefaultServiceFee = terminaldefault.DefaultServiceFee;
                            term.ServiceFeeMode = terminaldefault.ServiceFeeMode;
                            term.CustomerPaymentType = terminaldefault.CustomerPaymentType;
                            term.EnableNetworkReceiptPrint = terminaldefault.EnableNetworkReceiptPrint;
                            term.EnableMerchantLogoPrint = terminaldefault.EnableMerchantLogoPrint;
                            term.SignatureVerificationTimeout = Convert.ToInt16(terminaldefault.SignatureVerificationTimeout);
                            term.AutoLogonHour = terminaldefault.AutoLogonHour;
                            term.AutoLogonMode = terminaldefault.AutoLogonMode;
                            term.EnableRefundTran = terminaldefault.EnableRefundTran;
                            term.EnableAuthTran = terminaldefault.EnableAuthTran;
                            term.EnableTipTran = terminaldefault.EnableTipTran;
                            term.EnableCashOutTran = terminaldefault.EnableCashOutTran;
                            term.MerchantPassword = terminaldefault.MerchantPassword;
                            term.BankLogonRespCode = terminaldefault.BankLogonRespCode;
                            term.DefaultBackgroundColour = terminaldefault.DefaultBackgroundColour;
                            term.EnableMOTO = terminaldefault.EnableMOTO;
                            term.TerminalKey = t.TerminalKey;
                            term.RebootHours = terminaldefault.RebootHours;
                            term.EnableAutoTip = terminaldefault.EnableAutoTip;
                            term.EnableMOTORefund = terminaldefault.EnableMOTORefund;
                            term.AutoEOSMode = terminaldefault.AutoEOSMode;
                            term.EnableDualAPNMode = terminaldefault.EnableDualAPNMode;

                            term.EnableAutoTipTwo = terminaldefault.EnableAutoTipTwo;

                            TerminalModel TerminalC = new TerminalModel();


                            TerminalC = ConvertToClient(term);
                            TerminalC.IsSetEOS = terminaldefault.IsSetEOS ?? false;// update value of SetEOS from view

                            TerminalC.IsExistCategories =
                                con.TerminalCategories.Any(x => x.TerminalID == term.TerminalID && x.IsActive);

                            TerminalC.MemberKey = GetMemberKeyAllocatedInTerminal(term.TerminalID);

                            TerminalC.IsAllowAssignCategory = true;
                            if (TerminalC.MemberKey != 0)
                            {
                                TerminalC.IsAllowAssignCategory = IsAllowAccessToGlidebox(TerminalC.MemberKey);
                            }

                            TerminalModel.Add(TerminalC);
                        }
                    }
                }
            }
            return TerminalModel;
        }

        public List<TerminalModel> Terminal_ByKey(Int32 Key)
        {
            List<TerminalModel> TerminalModel = new List<TerminalModel>();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var terminals = from t in con.Terminals
                                where t.TerminalKey.Equals(Key)
                                select t;
                if (terminals != null)
                {
                    foreach (var t in terminals)
                    {

                        var term = (from t2 in con.Terminals
                                    where t2.TerminalKey.Equals(t.TerminalKey)
                                    select t2).FirstOrDefault();


                        var terminaldefault = (from tr in con.View_GetTerminalConfigUI
                                               where tr.TerminalKey.Equals(t.TerminalKey)
                                               select tr).SingleOrDefault();
                        if (terminaldefault != null)
                        {
                            if (terminaldefault.GPRSReconnectTimer == null)
                            {
                                terminaldefault.GPRSReconnectTimer = 30;
                            }

                            // Terminal terminal = new Terminal();
                            term.InputTimeoutSec = terminaldefault.InputTimeoutsec;
                            term.BacklightSec = terminaldefault.BacklightSec;
                            term.SleepModeMin = terminaldefault.SleepModeMin;
                            term.ScreenBrightness = terminaldefault.ScreenBrightness;
                            // terminal.Sound = (bool)terminaldefault.Sound.Value;
                            //   terminal.RemeberMe = terminaldefault.RemeberMe.Value;
                            term.ABNSetting = terminaldefault.ABNSetting;
                            term.MinimumCharge = terminaldefault.MinimumCharge;
                            term.MaximumCharge = terminaldefault.MaximumCharge;
                            term.EOSTime = terminaldefault.EOSTime.ToString();
                            term.LoginMode = terminaldefault.LoginMode;
                            term.UserID = t.UserID;
                            term.BusinessID = t.BusinessID;
                            term.ABN = t.ABN;
                            term.NetworkTableStatus = terminaldefault.NetworkTableStatus;
                            //  terminal. = terminaldefault.Prefix;
                            //  terminal.l = terminaldefault.LiveBusinessUnit;
                            //  terminal.ter = terminaldefault.TermConfigVer;
                            term.GPRSReconnectTimer = Convert.ToInt16(terminaldefault.GPRSReconnectTimer.Value);
                            term.AppRespCode = terminaldefault.ApprovedResponseCode;
                            term.AppSignRespCode = terminaldefault.ApprovedSignatureResponseCode;
                            term.IsPreTranPing = terminaldefault.IsPreTranPing;
                            term.TranAdviceSendMode = terminaldefault.TranAdviceSendMode;
                            term.DefaultServiceFee = terminaldefault.DefaultServiceFee;
                            term.ServiceFeeMode = terminaldefault.ServiceFeeMode;
                            term.CustomerPaymentType = terminaldefault.CustomerPaymentType;
                            term.EnableNetworkReceiptPrint = terminaldefault.EnableNetworkReceiptPrint;
                            term.EnableMerchantLogoPrint = terminaldefault.EnableMerchantLogoPrint;
                            term.SignatureVerificationTimeout = Convert.ToInt16(terminaldefault.SignatureVerificationTimeout);
                            term.AutoLogonHour = terminaldefault.AutoLogonHour;
                            term.AutoLogonMode = terminaldefault.AutoLogonMode;
                            term.EnableRefundTran = terminaldefault.EnableRefundTran;
                            term.EnableAuthTran = terminaldefault.EnableAuthTran;
                            term.EnableTipTran = terminaldefault.EnableTipTran;
                            term.EnableCashOutTran = terminaldefault.EnableCashOutTran;
                            term.MerchantPassword = terminaldefault.MerchantPassword;
                            term.BankLogonRespCode = terminaldefault.BankLogonRespCode;
                            term.DefaultBackgroundColour = terminaldefault.DefaultBackgroundColour;
                            term.EnableMOTO = terminaldefault.EnableMOTO;
                            term.TerminalKey = t.TerminalKey;
                            term.RebootHours = terminaldefault.RebootHours;
                            term.EnableAutoTip = terminaldefault.EnableAutoTip;
                            term.EnableMOTORefund = terminaldefault.EnableMOTORefund;
                            term.AutoEOSMode = terminaldefault.AutoEOSMode;
                            term.EnableDualAPNMode = terminaldefault.EnableDualAPNMode;
                            term.EnableAutoTipTwo = terminaldefault.EnableAutoTipTwo;

                            // VAA changes
                            term.EnableReceiptReference = terminaldefault.EnableReceiptReference ?? false;
                            term.EnableQantasReward = terminaldefault.EnableQantasReward ?? false;
                            term.EnableQantasPointsRedemption = terminaldefault.EnableQantasPointsRedemption ?? false;
                            term.EnableBusinessLogo = terminaldefault.EnableBusinessLogo ?? false;
                            term.EnablePaperRollOrder = terminaldefault.EnablePaperRollOrder ?? false;
                            term.EnableSupportCallBack = terminaldefault.EnableSupportCallBack ?? false;
                            term.RefundTransMax = terminaldefault.RefundTransMax;
                            term.RefundCumulativeMax = terminaldefault.RefundCumulativeMax;

                            term.AddLevyChargeToAmount = terminaldefault.AddLevyChargeToAmount ?? false;
                            term.LevyLabel = terminaldefault.LevyLabel;
                            term.LevyCharge = terminaldefault.LevyCharge;



                            term.CustomFooter = terminaldefault.CustomFooter;
                            term.CustomFooterHtml = terminaldefault.CustomFooterHtml;

                            term.IdleTimer = terminaldefault.IdleTimer;
                            term.IdleTransactions = terminaldefault.IdleTransactions;

                            int receiptKey =
                                term.ReceiptKey.HasValue ? term.ReceiptKey.Value
                                : con.Apps.Where(where => where.AppKey == term.AppKey).FirstOrDefault().ReceiptKey;

                            Receipt receipt =
                                con.Receipts.Where(where => where.ReceiptKey == receiptKey).FirstOrDefault();


                            //  term.ClearOverrideSettings = terminaldefault.cle
                            TerminalModel TerminalC = new TerminalModel();
                            TerminalC = ConvertToClient(term, receipt);

                            TerminalC.MemberKey = GetMemberKeyAllocatedInTerminal(term.TerminalID);

                            TerminalC.IsAllowAssignCategory = true;
                            if (TerminalC.MemberKey != 0)
                            {
                                TerminalC.IsAllowAssignCategory = IsAllowAccessToGlidebox(TerminalC.MemberKey);
                            }

                            TerminalModel.Add(TerminalC);

                        }

                    }
                }
            }
            return TerminalModel;
        }

        public List<TerminalModel> Terminal_BySerial(string sNo)
        {
            List<TerminalModel> TerminalModel = new List<TerminalModel>();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var terminals = from t in con.Terminals
                                where t.SerialNumber == sNo
                                select t;
                if (terminals != null)
                {
                    foreach (var t in terminals)
                    {

                        var term = (from t2 in con.Terminals
                                    where t2.TerminalKey.Equals(t.TerminalKey)
                                    select t2).FirstOrDefault();


                        var terminaldefault = (from tr in con.View_GetTerminalConfigUI
                                               where tr.TerminalKey.Equals(t.TerminalKey)
                                               select tr).SingleOrDefault();
                        if (terminaldefault != null)
                        {
                            if (terminaldefault.GPRSReconnectTimer == null)
                            {
                                terminaldefault.GPRSReconnectTimer = 30;
                            }

                            // Terminal terminal = new Terminal();
                            term.InputTimeoutSec = terminaldefault.InputTimeoutsec;
                            term.BacklightSec = terminaldefault.BacklightSec;
                            term.SleepModeMin = terminaldefault.SleepModeMin;
                            term.ScreenBrightness = terminaldefault.ScreenBrightness;
                            // terminal.Sound = (bool)terminaldefault.Sound.Value;
                            //   terminal.RemeberMe = terminaldefault.RemeberMe.Value;
                            term.ABNSetting = terminaldefault.ABNSetting;
                            term.MinimumCharge = terminaldefault.MinimumCharge;
                            term.MaximumCharge = terminaldefault.MaximumCharge;
                            term.EOSTime = terminaldefault.EOSTime.ToString();
                            term.LoginMode = terminaldefault.LoginMode;
                            term.UserID = t.UserID;
                            term.BusinessID = t.BusinessID;
                            term.ABN = t.ABN;
                            term.NetworkTableStatus = terminaldefault.NetworkTableStatus;
                            //  terminal. = terminaldefault.Prefix;
                            //  terminal.l = terminaldefault.LiveBusinessUnit;
                            //  terminal.ter = terminaldefault.TermConfigVer;
                            term.GPRSReconnectTimer = Convert.ToInt16(terminaldefault.GPRSReconnectTimer.Value);
                            term.AppRespCode = terminaldefault.ApprovedResponseCode;
                            term.AppSignRespCode = terminaldefault.ApprovedSignatureResponseCode;
                            term.IsPreTranPing = terminaldefault.IsPreTranPing;
                            term.TranAdviceSendMode = terminaldefault.TranAdviceSendMode;
                            term.DefaultServiceFee = terminaldefault.DefaultServiceFee;
                            term.ServiceFeeMode = terminaldefault.ServiceFeeMode;
                            term.CustomerPaymentType = terminaldefault.CustomerPaymentType;
                            term.EnableNetworkReceiptPrint = terminaldefault.EnableNetworkReceiptPrint;
                            term.EnableMerchantLogoPrint = terminaldefault.EnableMerchantLogoPrint;
                            term.SignatureVerificationTimeout = Convert.ToInt16(terminaldefault.SignatureVerificationTimeout);
                            term.AutoLogonHour = terminaldefault.AutoLogonHour;
                            term.AutoLogonMode = terminaldefault.AutoLogonMode;
                            term.EnableRefundTran = terminaldefault.EnableRefundTran;
                            term.EnableAuthTran = terminaldefault.EnableAuthTran;
                            term.EnableTipTran = terminaldefault.EnableTipTran;
                            term.EnableCashOutTran = terminaldefault.EnableCashOutTran;
                            term.MerchantPassword = terminaldefault.MerchantPassword;
                            term.BankLogonRespCode = terminaldefault.BankLogonRespCode;
                            term.DefaultBackgroundColour = terminaldefault.DefaultBackgroundColour;
                            term.EnableMOTO = terminaldefault.EnableMOTO;
                            term.TerminalKey = t.TerminalKey;
                            term.RebootHours = terminaldefault.RebootHours;
                            term.EnableAutoTip = terminaldefault.EnableAutoTip;
                            term.EnableMOTORefund = terminaldefault.EnableMOTORefund;
                            term.AutoEOSMode = terminaldefault.AutoEOSMode;
                            term.EnableDualAPNMode = terminaldefault.EnableDualAPNMode;

                            term.EnableAutoTipTwo = terminaldefault.EnableAutoTipTwo;

                            TerminalModel TerminalC = new TerminalModel();
                            TerminalC = ConvertToClient(term);

                            TerminalC.IsSetEOS = terminaldefault.IsSetEOS ?? false;// update value of SetEOS from view

                            TerminalC.IsExistCategories =
                                con.TerminalCategories.Any(x => x.TerminalID == term.TerminalID && x.IsActive);

                            TerminalC.MemberKey = GetMemberKeyAllocatedInTerminal(term.TerminalID);

                            TerminalC.IsAllowAssignCategory = true;
                            if (TerminalC.MemberKey != 0)
                            {
                                TerminalC.IsAllowAssignCategory = IsAllowAccessToGlidebox(TerminalC.MemberKey);
                            }

                            TerminalModel.Add(TerminalC);
                        }

                    }
                }
            }
            return TerminalModel;
        }

        public int GetMemberKeyAllocatedInTerminal(string terminalId)
        {
            using (var entities = new Phoenix_Service.TaxiEpayEntities())
            {
                var memberKey = (from m in entities.vTerminalAllocations
                                 where m.TerminalId == terminalId
                                       && m.Member_key != null && m.IsCurrent
                                 select m.Member_key).FirstOrDefault();

                return memberKey ?? 0;
            }
        }

        public bool IsAllowAccessToGlidebox(int memberKey)
        {
            using (var entities = new Phoenix_Service.TaxiEpayEntities())
            {
                var conpanyKey = entities.Members
                    .First(x => x.Member_key == memberKey).Company_key;

                var glideBoxCompany = Caching
                    .GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                        .BusinessTypeEnabledGlideBox.ToString()).Split(',').ToList();
                if (glideBoxCompany.Contains(conpanyKey.ToString()))
                {
                    return true;
                }

                return false;
            }
        }

        private TerminalModel ConvertToClient(Terminal Terminal, Receipt receipt = null)
        {

            TerminalModel terminal = new TerminalModel();
            terminal.TerminalKey = Terminal.TerminalKey;
            terminal.MerchantKey = Terminal.MerchantKey;
            terminal.TerminalGroupKey = Terminal.TerminalGroupKey;
            terminal.AppKey = Terminal.AppKey;
            terminal.ReceiptLogoKey = Terminal.ReceiptLogoKey;
            terminal.ScreenLogoKey = Terminal.ScreenLogoKey;
            terminal.PromptKey = Terminal.PromptKey;
            terminal.TerminalConfigKey = Terminal.TerminalConfigKey;
            terminal.TerminalBINRangeKey = Terminal.TerminalBINRangeKey;
            terminal.TerminalNetworkKey = Terminal.TerminalNetworkKey;
            terminal.StateKey = Terminal.StateKey;
            terminal.ReceiptKey = Terminal.ReceiptKey;
            terminal.TerminalTypeKey = Terminal.TerminalTypeKey;
            terminal.TNNetworkKey = Terminal.TNNetworkKey;

            terminal.TerminalID = Terminal.TerminalID;
            terminal.SerialNumber = Terminal.SerialNumber;
            terminal.VAAVersion = Terminal.VAAVersion;
            terminal.OSVersion = Terminal.OSVersion;
            terminal.FSVersionNO = Terminal.FSVersionNO;
            terminal.DiscountApplied = Terminal.DiscountApplied;
            terminal.UserID = Terminal.UserID;
            terminal.BusinessID = Terminal.BusinessID;
            terminal.ABN = Terminal.ABN;

            terminal.VersionNo = Terminal.VersionNo;
            terminal.InputTimeoutSec = Terminal.InputTimeoutSec;
            terminal.BacklightSec = Terminal.BacklightSec;
            terminal.SleepModeMin = Terminal.SleepModeMin;
            terminal.ScreenBrightness = Terminal.ScreenBrightness;
            terminal.Sound = Terminal.Sound;
            terminal.RemeberMe = Terminal.RemeberMe;
            terminal.EnableAutoTipTwo = terminal.EnableAutoTipTwo;
            terminal.EnableZip = Terminal.EnableZip == true;
            terminal.RecordingMode = Terminal.RecordingMode;
            terminal.UploadTimer = Terminal.UploadTimer;
            terminal.EnableGlidebox = Terminal.EnableGlidebox;
            terminal.CardNotPresentTransactionLimit = Terminal.CardNotPresentTransactionLimit.GetValueOrDefault();
            terminal.CardPresentTransactionLimit = Terminal.CardPresentTransactionLimit.GetValueOrDefault();

            string p = string.Empty;
            if (Terminal.ABNSetting != null)
            {
                string o = Terminal.ABNSetting.Trim();
                int d = 0;
                try
                {
                    d = Convert.ToInt32(o);
                }
                catch
                {
                }
                p = String.Format("{0,0:N2}", d / 100.0);
                terminal.ABNSettingB = Convert.ToDecimal(p);
            }

            terminal.ABNSetting = p;// Terminal.ABNSetting;
            terminal.MinimumCharge = Terminal.MinimumCharge;
            terminal.MaximumCharge = Terminal.MaximumCharge;
            terminal.EOSTime = Terminal.EOSTime;
            terminal.LoginMode = Terminal.LoginMode;
            terminal.UserIdConfig = Terminal.UserIdConfig;
            terminal.BusinessIdConfig = Terminal.BusinessIdConfig;
            terminal.ABNConfig = Terminal.ABNConfig;
            terminal.FullVersionNo = Terminal.TerminalKey + "." + Terminal.VersionNo;
            terminal.GPRSReconnectTimer = Terminal.GPRSReconnectTimer;
            terminal.NetworkTableStatus = Terminal.NetworkTableStatus;
            terminal.AppSignRespCode = Terminal.AppSignRespCode;
            terminal.AppRespCode = Terminal.AppRespCode;

            terminal.Inactive = Terminal.Inactive;
            terminal.CreatedDate = Terminal.CreatedDate;
            terminal.CreatedBy = Terminal.CreatedBy;
            terminal.UpdatedDate = Terminal.UpdatedDate;
            terminal.UpdatedBy = Terminal.UpdatedBy;
            terminal.CreatedByUser = Terminal.CreatedByUser;
            terminal.UpdatedByUser = Terminal.UpdatedByUser;
            terminal.IsPreTranPing = Terminal.IsPreTranPing;
            terminal.TranAdviceSendMode = Terminal.TranAdviceSendMode;
            terminal.ConnectToTMS = Terminal.ConnectToTMS;
            terminal.SimSerialNo = Terminal.SimSerialNo;
            terminal.SimMobileNumber = Terminal.SimMobileNumber;
            terminal.IMEI = Terminal.IMEI;
            terminal.PTID = Terminal.PTID;

            terminal.DefaultServiceFee = Terminal.DefaultServiceFee;
            terminal.ServiceFeeMode = Terminal.ServiceFeeMode;
            terminal.CustomerPaymentType = Terminal.CustomerPaymentType;
            terminal.EnableNetworkReceiptPrint = Terminal.EnableNetworkReceiptPrint;
            terminal.EnableMerchantLogoPrint = Terminal.EnableMerchantLogoPrint;
            terminal.SignatureVerificationTimeout = Terminal.SignatureVerificationTimeout;
            terminal.AutoLogonHour = Terminal.AutoLogonHour;
            terminal.AutoLogonMode = Terminal.AutoLogonMode;
            terminal.EnableRefundTran = Terminal.EnableRefundTran;
            terminal.EnableAuthTran = Terminal.EnableAuthTran;
            terminal.EnableTipTran = Terminal.EnableTipTran;
            terminal.EnableCashOutTran = Terminal.EnableCashOutTran;
            terminal.MerchantPassword = Terminal.MerchantPassword;
            terminal.BankLogonRespCode = Terminal.BankLogonRespCode;

            terminal.MainRegionKey = Terminal.MainRegionKey;
            terminal.TipTrickKey = Terminal.TipTrickKey;
            terminal.TerminalLocationKey = Terminal.TerminalLocationKey;
            terminal.EnableMOTO = Terminal.EnableMOTO;
            terminal.DefaultBackgroundColour = Terminal.DefaultBackgroundColour;
            terminal.AutoTipNumOne = Terminal.AutoTipNumOne;
            terminal.AutoTipNumTwo = Terminal.AutoTipNumTwo;
            terminal.AutoTipNumThree = Terminal.AutoTipNumThree;
            terminal.AutoTipNumFour = Terminal.AutoTipNumFour;

            terminal.AutoTipIsPercentOne = Terminal.AutoTipIsPercentOne;
            terminal.AutoTipIsPercentTwo = Terminal.AutoTipIsPercentTwo;
            terminal.AutoTipIsPercentThree = Terminal.AutoTipIsPercentThree;
            terminal.AutoTipIsPercentFour = Terminal.AutoTipIsPercentFour;

            terminal.AutoTipButtonOneID = Terminal.AutoTipButtonOneID;
            terminal.AutoTipButtonTwoID = Terminal.AutoTipButtonTwoID;
            terminal.AutoTipButtonThreeID = Terminal.AutoTipButtonThreeID;
            terminal.AutoTipButtonFourID = Terminal.AutoTipButtonFourID;
            terminal.AutoTipDefaultButtonID = Terminal.AutoTipDefaultButtonID;
            terminal.ClearOverrideSettings = Terminal.ClearOverrideSettings;


            if (Terminal.BusinessTypeKey != null)
            {
                terminal.BusinessTypeKey = Terminal.BusinessTypeKey.Value;
            }
            if (Terminal.RebootHours != null)
            {
                terminal.RebootHours = Terminal.RebootHours.Value;
            }
            if (Terminal.EnableAutoTip == null)
            {
                terminal.EnableAutoTip = true;
            }
            else
            {
                terminal.EnableAutoTip = Terminal.EnableAutoTip.Value;
            }


            if (Terminal.EnableAutoTipTwo == null)
            {
                terminal.EnableAutoTipTwo = true;
            }
            else
            {
                terminal.EnableAutoTipTwo = Terminal.EnableAutoTipTwo.Value;
            }


            if (Terminal.EnableMOTORefund == null)
            {
                terminal.EnableMOTORefund = true;
            }
            else
            {
                terminal.EnableMOTORefund = Terminal.EnableMOTORefund.Value;
            }

            if (Terminal.AutoEOSMode != null)
            {
                terminal.AutoEOSMode = Terminal.AutoEOSMode.Value;
            }
            if (Terminal.EnableDualAPNMode != null)
            {
                terminal.EnableDualAPNMode = Terminal.EnableDualAPNMode.Value;
            }

            terminal.EnableReceiptReference = Terminal.EnableReceiptReference ?? false;
            terminal.EnableQantasReward = Terminal.EnableQantasReward ?? false;
            terminal.EnableQantasPointsRedemption = Terminal.EnableQantasPointsRedemption ?? false;
            terminal.EnableBusinessLogo = Terminal.EnableBusinessLogo ?? false;
            terminal.EnablePaperRollOrder = Terminal.EnablePaperRollOrder ?? false;
            terminal.EnableSupportCallBack = Terminal.EnableSupportCallBack ?? false;
            terminal.RefundTransMax = Terminal.RefundTransMax;
            terminal.RefundCumulativeMax = Terminal.RefundCumulativeMax;


            terminal.LevyCharge = Convert.ToDecimal(Terminal.LevyCharge.HasValue ? Terminal.LevyCharge.Value / 100.00 : 0);
            terminal.AddLevyChargeToAmount = Terminal.AddLevyChargeToAmount ?? false;
            terminal.LevyLabel = Terminal.LevyLabel;

            terminal.CustomFooter = Terminal.CustomFooter;
            terminal.CustomFooterHtml = Terminal.CustomFooterHtml;

            terminal.EnablePrintCustomerReceiptFirst = false;
            terminal.EnablePrintSettlement = true;
            terminal.PrintReceiptMode = 0;

            if (receipt != null)
            {
                terminal.TerminalHeaderFooter = new ReceiptModel()
                {
                    HeaderLineOne = receipt.HeaderLineOne,
                    HeaderLineTwo = receipt.HeaderLineTwo,
                    HeaderLineThree = receipt.HeaderLineThree,
                    HeaderLineFour = receipt.HeaderLineFour,

                    FooterLineOne = receipt.FooterLineOne,
                    FooterLineTwo = receipt.FooterLineTwo,
                    FooterLineThree = receipt.FooterLineThree,
                    FooterLineFour = receipt.FooterLineFour,



                };
                terminal.EnablePrintCustomerReceiptFirst = receipt.EnablePrintCustomerReceiptFirst;
                terminal.EnablePrintSettlement = !receipt.EnablePrintSettlement;
                terminal.PrintReceiptMode = receipt.PrintReceiptMode;
            }

            terminal.IdleTimer = Terminal.IdleTimer.GetValueOrDefault(0);
            terminal.IdleTransactions = Terminal.IdleTransactions.GetValueOrDefault(0);

            return terminal;
        }
        public Terminal ConvertToTerminalServer(TerminalModel cTerminal)
        {
            //
            Terminal sTerminal = new Terminal();
            sTerminal.TerminalKey = cTerminal.TerminalKey;
            sTerminal.MerchantKey = cTerminal.MerchantKey;

            sTerminal.TerminalGroupKey = cTerminal.TerminalGroupKey;
            sTerminal.AppKey = cTerminal.AppKey;

            sTerminal.ReceiptLogoKey = cTerminal.ReceiptLogoKey;
            sTerminal.ScreenLogoKey = cTerminal.ScreenLogoKey;
            sTerminal.PromptKey = cTerminal.PromptKey;

            sTerminal.TerminalConfigKey = cTerminal.TerminalConfigKey;

            sTerminal.TerminalBINRangeKey = cTerminal.TerminalBINRangeKey;
            sTerminal.TerminalNetworkKey = cTerminal.TerminalNetworkKey;

            sTerminal.StateKey = cTerminal.StateKey;
            sTerminal.ReceiptKey = cTerminal.ReceiptKey;
            sTerminal.TerminalTypeKey = cTerminal.TerminalTypeKey;
            sTerminal.TNNetworkKey = cTerminal.TNNetworkKey;

            sTerminal.VersionNo = cTerminal.VersionNo;
            sTerminal.InputTimeoutSec = cTerminal.InputTimeoutSec;
            sTerminal.BacklightSec = cTerminal.BacklightSec;
            sTerminal.SleepModeMin = cTerminal.SleepModeMin;
            sTerminal.ScreenBrightness = cTerminal.ScreenBrightness;
            sTerminal.Sound = cTerminal.Sound;
            sTerminal.RemeberMe = cTerminal.RemeberMe;
            sTerminal.ABNSetting = cTerminal.ABNSetting;
            sTerminal.MinimumCharge = cTerminal.MinimumCharge;
            sTerminal.MaximumCharge = cTerminal.MaximumCharge;
            sTerminal.EOSTime = cTerminal.EOSTime;
            sTerminal.LoginMode = cTerminal.LoginMode;
            sTerminal.UserIdConfig = cTerminal.UserIdConfig;
            sTerminal.BusinessIdConfig = cTerminal.BusinessIdConfig;
            sTerminal.ABNConfig = cTerminal.ABNConfig;
            sTerminal.GPRSReconnectTimer = cTerminal.GPRSReconnectTimer;
            sTerminal.NetworkTableStatus = cTerminal.NetworkTableStatus;
            sTerminal.Inactive = cTerminal.Inactive;
            sTerminal.UserID = cTerminal.UserID;
            sTerminal.BusinessID = cTerminal.BusinessID;
            sTerminal.ABN = cTerminal.ABN;

            sTerminal.TerminalID = cTerminal.TerminalID;
            sTerminal.SerialNumber = cTerminal.SerialNumber;
            sTerminal.VAAVersion = cTerminal.VAAVersion;
            sTerminal.OSVersion = cTerminal.OSVersion;
            sTerminal.FSVersionNO = cTerminal.FSVersionNO;
            sTerminal.DiscountApplied = cTerminal.DiscountApplied;
            sTerminal.AppRespCode = cTerminal.AppRespCode;
            sTerminal.AppSignRespCode = cTerminal.AppSignRespCode;
            sTerminal.IsPreTranPing = cTerminal.IsPreTranPing;
            sTerminal.TranAdviceSendMode = cTerminal.TranAdviceSendMode;
            sTerminal.ConnectToTMS = cTerminal.ConnectToTMS;
            sTerminal.SimSerialNo = cTerminal.SimSerialNo;
            sTerminal.SimMobileNumber = cTerminal.SimMobileNumber;
            sTerminal.IMEI = cTerminal.IMEI;
            sTerminal.PTID = cTerminal.PTID;
            sTerminal.DefaultBackgroundColour = cTerminal.DefaultBackgroundColour;
            sTerminal.EnableMOTO = cTerminal.EnableMOTO;

            sTerminal.DefaultServiceFee = cTerminal.DefaultServiceFee;
            sTerminal.ServiceFeeMode = cTerminal.ServiceFeeMode;
            sTerminal.CustomerPaymentType = cTerminal.CustomerPaymentType;
            sTerminal.EnableNetworkReceiptPrint = cTerminal.EnableNetworkReceiptPrint;
            sTerminal.EnableMerchantLogoPrint = cTerminal.EnableMerchantLogoPrint;
            sTerminal.SignatureVerificationTimeout = cTerminal.SignatureVerificationTimeout;
            sTerminal.AutoLogonHour = cTerminal.AutoLogonHour;
            sTerminal.AutoLogonMode = cTerminal.AutoLogonMode;
            sTerminal.EnableRefundTran = cTerminal.EnableRefundTran;
            sTerminal.EnableAuthTran = cTerminal.EnableAuthTran;
            sTerminal.EnableTipTran = cTerminal.EnableTipTran;
            sTerminal.EnableCashOutTran = cTerminal.EnableCashOutTran;
            sTerminal.MerchantPassword = cTerminal.MerchantPassword;
            sTerminal.BankLogonRespCode = cTerminal.BankLogonRespCode;
            sTerminal.MainRegionKey = cTerminal.MainRegionKey;
            sTerminal.TipTrickKey = cTerminal.TipTrickKey;
            sTerminal.TerminalLocationKey = cTerminal.TerminalLocationKey;
            sTerminal.BusinessTypeKey = cTerminal.BusinessTypeKey;
            sTerminal.RebootHours = cTerminal.RebootHours;
            sTerminal.EnableAutoTip = cTerminal.EnableAutoTip;
            sTerminal.EnableMOTORefund = cTerminal.EnableMOTORefund;
            sTerminal.AutoEOSMode = cTerminal.AutoEOSMode;
            sTerminal.EnableDualAPNMode = cTerminal.EnableDualAPNMode;
            sTerminal.ClearOverrideSettings = cTerminal.ClearOverrideSettings;

            return sTerminal;
        }

        public void DisableTerminal(TransactionHostEntities dbContext, string terminalId, int terminalKey)
        {
            var terminalList = dbContext.Terminals.Where(x => x.TerminalID == terminalId && x.TerminalKey != terminalKey).ToList();
            terminalList.ForEach(m => m.Inactive = true);
        }

        public List<View_LoginMode> GetView_LoginModeList()
        {
            List<View_LoginMode> loginMode = new List<View_LoginMode>();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from lm in con.View_LoginMode
                         orderby lm.Code ascending
                         select lm).ToList();

                if (t != null)
                {
                    loginMode = t;
                }
            }
            return loginMode;
        }
        public List<State> GetStateList()
        {
            List<State> states = new List<State>();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from lm in con.States
                         orderby lm.ShortDescription ascending
                         select lm).ToList();

                if (t != null)
                {
                    states = t;

                    states.Add(new State());
                }
            }
            return states;
        }
        public List<TerminalNetwork> GetTerminalNetworksList(int key)
        {
            List<TerminalNetwork> terminalNetworks = new List<TerminalNetwork>();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var t = from o in con.TerminalNetworks
                        where o.StateKey == key
                        select o;

                if (t != null)
                {
                    terminalNetworks = t.ToList();
                }
            }
            terminalNetworks.Add(new TerminalNetwork());

            return terminalNetworks;
        }
        public TerminalNetwork GetTerminalNetworks(int Default)
        {
            TerminalNetwork terminalNetwork = new TerminalNetwork();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from o in con.TerminalNetworks
                         where o.TerminalNetworkKey == Default
                         select o).FirstOrDefault();

                if (t != null)
                {
                    terminalNetwork = t;
                }
            }

            return terminalNetwork;
        }

        public List<TerminalNetworkDetail> GetTerminalNetworkDetailList(int key)
        {
            List<TerminalNetworkDetail> terminalNetworkDetails = new List<TerminalNetworkDetail>();// LiveGroup.BusLog.GetEntity<TerminalNetworkDetail>().ServerMany(Key).ToList();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var t = (from tnd in con.TerminalNetworkDetails
                         where tnd.TerminalNetworkKey.Equals(key)
                         select tnd).ToList();
                if (t != null)
                {
                    terminalNetworkDetails = t.ToList();
                }


            }
            terminalNetworkDetails.Add(new TerminalNetworkDetail());
            return terminalNetworkDetails;
        }
        public TerminalNetworkDetail GetTerminalNetworkDetails(int Default)
        {
            TerminalNetworkDetail terminalNetworkDetail = new TerminalNetworkDetail();// LiveGroup.BusLog.GetEntity<TerminalNetworkDetail>().Server(Default);
            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from tnd in con.TerminalNetworkDetails
                         where tnd.TNNetworkKey.Equals(Default)
                         select tnd).SingleOrDefault();

                if (t != null)
                {
                    terminalNetworkDetail = t;
                }
            }

            return terminalNetworkDetail;
        }

        public void AddBatchReconRequest(BatchReconRequestModel request)
        {
            using (TransactionHostEntities context = new TransactionHostEntities())
            {
                context.BatchReconRequests.Add(new BatchReconRequest()
                {
                    Batchnumber = request.Batchnumber,
                    CreatedByDateTime = DateTime.Now,
                    CreatedByUser = request.CreatedByUser,
                    MerchantID = request.MerchantID,
                    TerminalID = request.TerminalID,
                });

                context.SaveChanges();
            }
        }

        public void UpdateTerminal(string terminalID, bool? enableMOTO, bool? enableRefundTran)
        {
            using (TransactionHostEntities context = new TransactionHostEntities())
            {
                var term = context.Terminals.FirstOrDefault(n => n.TerminalID == terminalID);
                if (term != null)
                {
                    term.EnableMOTO = enableMOTO;
                    term.EnableRefundTran = enableRefundTran;
                    context.SaveChanges();
                }
            }
        }

        public void CreateTerminalAssignHistory(TransactionHostEntities tranHostDbContext, TerminalModel newTerminal, Terminal originTerminal, string userName, string sourceName)
        {
            //log history when user disable glidebox
            if (!newTerminal.EnableGlidebox && originTerminal.EnableGlidebox)
            {
                var terminalHistory = new TerminalHistory()
                {
                    TerminalID = originTerminal.TerminalID,
                    ChangedDatetime = DateTime.Now,
                    ChangedBy = userName,
                    Source = sourceName,
                    EnableGlidebox = false,
                    Reason = newTerminal.ReasonDisableGlidebox
                };

                tranHostDbContext.TerminalHistories.Add(terminalHistory);
                return;
            }

            //log history when user enable glidebox
            if (newTerminal.EnableGlidebox && !originTerminal.EnableGlidebox)
            {
                var terminalHistory = new TerminalHistory()
                {
                    TerminalID = originTerminal.TerminalID,
                    ChangedDatetime = DateTime.Now,
                    ChangedBy = userName,
                    Source = sourceName,
                    EnableGlidebox = true,
                };

                tranHostDbContext.TerminalHistories.Add(terminalHistory);
                return;
            }
        }

        private uint DJBHash(string str)
        {
            uint hash = 5381;
            uint i = 0;

            for (i = 0; i < str.Length; i++)
            {
                hash = ((hash << 5) + hash) + ((byte)str[(int)i]);
            }
            return hash;
        }

        public string GenerateTerminalPassword(string terminalId)
        {
            //1. Get the timestamp in YYMMDD format. We don't want minutes or seconds.
            string timestamp = DateTime.Now.ToString("yyMMdd");

            //2. Use a hashing function on the timestamp. I would say we use DJB hashing function. Link & code snippet below.
            uint hashtimestamp = DJBHash(timestamp);

            //3. Use the PTID of the terminal and get it's hash as well
            var tidHash = DJBHash(terminalId);

            //4. Multiply the two
            uint mul = hashtimestamp * tidHash;
            string convertString = mul.ToString();

            //5. Take the last 6 digits of the number as the password
            string password = convertString.Substring(convertString.Length - 6);
            return password;
        }
        public void Dispose()
        {

        }
    }
}
