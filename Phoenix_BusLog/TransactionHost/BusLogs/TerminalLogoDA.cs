﻿using Phoenix_BusLog.TransactionHost.Model;
using Phoenix_TransactionHostUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.BusLogs
{
    public class TerminalLogoDA
    {

        public TerminalLogoModel GetLogo(int TerminalKey)
        {
            TerminalLogoModel model = new TerminalLogoModel();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var term = (from t in con.Terminals
                            where t.TerminalKey == TerminalKey
                            select t).FirstOrDefault();

                if (term != null)
                {

                    model.TerminalID = term.TerminalID;
                    model.TerminalKey = term.TerminalKey;
                    model.ReceiptLogoKey = term.ReceiptLogoKey;
                    model.ScreenLogoKey = term.ScreenLogoKey;
                    model.FooterLogoKey = term.FooterLogoKey;

                    if (model.ReceiptLogoKey != null)
                    {
                        var reciept = (from t in con.ReceiptLogoes
                                       where t.ReceiptLogoKey == term.ReceiptLogoKey
                                       select t).FirstOrDefault();
                        if (reciept != null)
                        {
                            model.ReceiptLogoModel = new ReceiptLogoModel();
                            model.ReceiptLogoModel.VersionNo = reciept.VersionNo;
                            model.ReceiptLogoModel.LogoImage = reciept.LogoImage;
                        }

                    }
                    if (model.ScreenLogoKey != null)
                    {
                        var screen = (from t in con.ScreenLogoes
                                      where t.ScreenLogoKey == model.ScreenLogoKey
                                      select t).FirstOrDefault();
                        if (screen != null)
                        {
                            model.ScreenLogoModel = new ScreenLogoModel();
                            model.ScreenLogoModel.ScreenLogoKey = screen.ScreenLogoKey;
                            model.ScreenLogoModel.LogoImage = screen.LogoImage;
                            model.ScreenLogoModel.Description = screen.Description;
                            model.ScreenLogoModel.VersionNo = screen.VersionNo;
                        }
                    }
                    if (model.FooterLogoKey != null)
                    {
                        var footerLogo = (from f in con.FooterLogoes
                                          where f.FooterLogoKey == model.FooterLogoKey
                                          select f).FirstOrDefault();
                        if (footerLogo != null)
                        {
                            model.FooterLogoModel = new FooterLogoModel();
                            model.FooterLogoModel.FooterLogoKey = footerLogo.FooterLogoKey;
                            model.FooterLogoModel.LogoImage = footerLogo.LogoImage;
                            model.FooterLogoModel.Description = footerLogo.Description;
                            model.FooterLogoModel.VersionNo = footerLogo.VersionNo;
                        }
                    }
                }
            }
            return model;
        }
        public byte[] GetPhotoScreenDA(TerminalLogoModel model)
        {
            byte[] photo = null;
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                string mid = (from m in con.Merchants
                              where m.MerchantKey == model.Terminal.MerchantKey
                              select m.MerchantID).FirstOrDefault();
                var screen = con.GetScreenLogo(model.Terminal.TerminalID, mid).FirstOrDefault();
                photo = screen.LogoImage;
            }
            return photo;
        }

        //public byte[] GetPhotoRecieptDA(TerminalLogoModel model)
        //{
        //    byte[] photo = null;
        //    using (TransactionHostEntities con = new TransactionHostEntities())
        //    {
        //        string mid = (from m in con.Merchants
        //                      where m.MerchantKey == model.Terminal.MerchantKey
        //                      select m.MerchantID).FirstOrDefault();
        //        var screen = con.GetPrinterLogo(model.Terminal.TerminalID, mid).FirstOrDefault();
        //        photo = screen.LogoImage;
        //    }
        //    return photo;
        //}

        public bool GetPhotoRecieptDA(TerminalLogoModel model)
        {
            bool photo = false;
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                //string mid = (from m in con.Merchants
                //              where m.MerchantKey == model.Terminal.MerchantKey
                //              select m.MerchantID).FirstOrDefault();
                //var screen = con.GetPrinterLogo(model.Terminal.TerminalID, mid).FirstOrDefault();
                var screen = (from c in con.Terminals
                              where c.TerminalKey == model.Terminal.TerminalKey
                              select c.ReceiptLogoKey).FirstOrDefault();

                if (screen != null)
                {
                    photo = true;
                }
            }
            return photo;
        }

        public bool GetFooterLogo(TerminalLogoModel model)
        {
            bool photo = false;
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var logo = (from c in con.Terminals
                              where c.TerminalKey == model.Terminal.TerminalKey
                              select c.FooterLogoKey).FirstOrDefault();

                if (logo != null)
                {
                    photo = true;
                }
            }
            return photo;
        }


        public void EnableAutoTip(int TerminalKey, bool Enable)
        {
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var term = (from t in con.Terminals
                            where t.TerminalKey == TerminalKey
                            select t).FirstOrDefault();

                if (term != null)
                {
                    term.EnableAutoTip = Enable;
                    term.VersionNo = term.VersionNo + 1;
                    con.SaveChanges();
                }
            }
        }

        public void EnableAutoTipTwo(int TerminalKey, bool Enable)
        {
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var term = (from t in con.Terminals
                            where t.TerminalKey == TerminalKey
                            select t).FirstOrDefault();

                if (term != null)
                {
                    term.EnableAutoTipTwo = Enable;
                    term.VersionNo = term.VersionNo + 1;
                    con.SaveChanges();
                }
            }
        }

        public PromptModel GetTerminalPrompt(int TerminalKey)
        {
            PromptModel prom = new PromptModel();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var term = (from t in con.Terminals
                            where t.TerminalKey == TerminalKey
                            select t).FirstOrDefault();

                if (term != null)
                {
                    var mid = (from m in con.Merchants
                               where m.MerchantKey == term.MerchantKey
                               select m.MerchantID).FirstOrDefault();

                    var pp = con.GetDisplayTexts(term.TerminalID, mid).FirstOrDefault();


                    if (pp != null)
                    {

                        prom.UserId = pp.UserIDPrompt;
                        prom.BusinessId = pp.BusinessIDPrompt;
                        prom.AmountOne = pp.Amount1;
                        prom.AmountTwo = pp.Amount2;
                        prom.AmountThree = pp.Amount3;
                        prom.AmountFour = pp.Amount4;
                        prom.AmountFive = pp.Amount5;
                        prom.AmountSix = pp.Amount6;
                        prom.AmountSeven = pp.Amount7;
                        prom.AmountEight = pp.Amount7;
                        prom.AmountNine = pp.Amount8;
                        prom.LocationOnePrompt = pp.LocationOnePrompt;
                        prom.LocationTwoPrompt = pp.LocationTwoPrompt;
                        prom.DisableLocationOne = pp.DisableLocationOne;
                        prom.DisableLocationTwo = pp.DisableLocationTwo;
                        prom.EOSButtonText = pp.EOSButtonText;
                        prom.EOSApprovedText = pp.EOSApprovedText;
                        prom.EOSConfirmText = pp.EOSConfirmText;
                        prom.EOSPriorText = pp.EOSPriorText;
                        prom.EOSNotFoundText = pp.EOSNotFoundText;
                        prom.EOSBatchOpenText = pp.EOSBatchOpenText;
                        prom.AutoTipDisplayOption = pp.AutoTipDisplayOption;
                        if (pp.EnableAutoTipAmountDisplay == 1)
                        {
                            prom.EnableAutoTipAmountDisplay = true;
                        }
                        else
                        {
                            prom.EnableAutoTipAmountDisplay = false;
                        }
                        if (pp.EnableAutoTipTotalDisplay == 1)
                        {
                            prom.EnableAutoTipTotalDisplay = true;
                        }
                        else
                        {
                            prom.EnableAutoTipTotalDisplay = false;
                        }
                        prom.AutoTipTitle = pp.AutoTipTitle;
                        prom.AutoTipPriorAmountLabel = pp.AutoTipPriorAmountLabel;
                        if (pp.AutoTipEnterConfirmation == 1)
                        {
                            prom.AutoTipEnterConfirmation = true;
                        }
                        else
                        {
                            prom.AutoTipEnterConfirmation = false;
                        }

                        prom.AutoTipEOSReportOption = pp.AutoTipEOSReportOption;
                        prom.AutoTipButtonOneFeeCents = pp.AutoTipButtonOneFeeCents;
                        prom.AutoTipButtonOneFeePercentage = pp.AutoTipButtonOneFeePercentage;
                        prom.AutoTipButtonTwoFeeCents = pp.AutoTipButtonTwoFeeCents;
                        prom.AutoTipButtonTwoFeePercentage = pp.AutoTipButtonTwoFeePercentage;
                        prom.AutoTipButtonThreeFeeCents = pp.AutoTipButtonThreeFeeCents;
                        prom.AutoTipButtonThreeFeePercentage = pp.AutoTipButtonThreeFeePercentage;
                        prom.AutoTipButtonFourFeeCents = pp.AutoTipButtonFourFeeCents;
                        prom.AutoTipButtonFourFeePercentage = pp.AutoTipButtonFourFeePercentage;
                        prom.AutoTipManualEntryFeeCents = pp.AutoTipManualEntryFeeCents;
                        prom.AutoTipManualEntryFeePercentage = pp.AutoTipManualEntryFeePercentage;
                        prom.AutoTipAmountLabel = pp.AutoTipAmountLabel;
                        prom.AutoTipEOSReportMessage = pp.AutoTipEOSReportMessage;
                        if (pp.AutoTipFeeIncluded == 1)
                        {
                            prom.AutoTipFeeIncluded = true;
                        }
                        else
                        {
                            prom.AutoTipFeeIncluded = false;
                        }

                        prom.AutoTipNumOne = pp.AutoTipNumOne;
                        prom.AutoTipNumTwo = pp.AutoTipNumTwo;
                        prom.AutoTipNumThree = pp.AutoTipNumThree;
                        prom.AutoTipNumFour = pp.AutoTipNumFour;

                        if (pp.AutoTipIsPercentOne == 1)
                        {
                            prom.AutoTipIsPercentOne = true;
                        }
                        else
                        {
                            prom.AutoTipIsPercentOne = false;
                        }


                        if (pp.AutoTipIsPercentTwo == 1)
                        {
                            prom.AutoTipIsPercentTwo = true;
                        }
                        else
                        {
                            prom.AutoTipIsPercentTwo = false;
                        }

                        if (pp.AutoTipIsPercentThree == 1)
                        {
                            prom.AutoTipIsPercentThree = true;
                        }
                        else
                        {
                            prom.AutoTipIsPercentThree = false;
                        }

                        if (pp.AutoTipIsPercentFour == 1)
                        {
                            prom.AutoTipIsPercentFour = true;
                        }
                        else
                        {
                            prom.AutoTipIsPercentFour = false;
                        }



                        prom.AutoTipButtonOneID = pp.AutoTipButtonOneID;
                        prom.AutoTipButtonTwoID = pp.AutoTipButtonTwoID;
                        prom.AutoTipButtonThreeID = pp.AutoTipButtonThreeID;
                        prom.AutoTipButtonFourID = pp.AutoTipButtonFourID;
                        prom.AutoTipDefaultButtonID = pp.AutoTipDefaultButtonID;


                        //Jag
                        prom.AutoTipTwoDisplayOption = pp.AutoTipTwoDisplayOption;
                        if (pp.EnableAutoTipTwoAmountDisplay == 1)
                        {
                            prom.EnableAutoTipTwoAmountDisplay = true;
                        }
                        else
                        {
                            prom.EnableAutoTipTwoAmountDisplay = false;
                        }
                        if (pp.EnableAutoTipTwoTotalDisplay == 1)
                        {
                            prom.EnableAutoTipTwoTotalDisplay = true;
                        }
                        else
                        {
                            prom.EnableAutoTipTwoTotalDisplay = false;
                        }
                        prom.AutoTipTwoTitle = pp.AutoTipTwoTitle;
                        prom.AutoTipTwoPriorAmountLabel = pp.AutoTipTwoPriorAmountLabel;
                        if (pp.AutoTipTwoEnterConfirmation == 1)
                        {
                            prom.AutoTipTwoEnterConfirmation = true;
                        }
                        else
                        {
                            prom.AutoTipTwoEnterConfirmation = false;
                        }

                        prom.AutoTipTwoEOSReportOption = pp.AutoTipTwoEOSReportOption;
                        prom.AutoTipTwoButtonOneFeeCents = pp.AutoTipTwoButtonOneFeeCents;
                        prom.AutoTipTwoButtonOneFeePercentage = pp.AutoTipTwoButtonOneFeePercentage;
                        prom.AutoTipTwoButtonTwoFeeCents = pp.AutoTipTwoButtonTwoFeeCents;
                        prom.AutoTipTwoButtonTwoFeePercentage = pp.AutoTipTwoButtonTwoFeePercentage;
                        prom.AutoTipTwoButtonThreeFeeCents = pp.AutoTipTwoButtonThreeFeeCents;
                        prom.AutoTipTwoButtonThreeFeePercentage = pp.AutoTipTwoButtonThreeFeePercentage;
                        prom.AutoTipTwoButtonFourFeeCents = pp.AutoTipTwoButtonFourFeeCents;
                        prom.AutoTipTwoButtonFourFeePercentage = pp.AutoTipTwoButtonFourFeePercentage;
                        prom.AutoTipTwoManualEntryFeeCents = pp.AutoTipTwoManualEntryFeeCents;
                        prom.AutoTipTwoManualEntryFeePercentage = pp.AutoTipTwoManualEntryFeePercentage;
                        prom.AutoTipTwoAmountLabel = pp.AutoTipTwoAmountLabel;
                        prom.AutoTipTwoEOSReportMessage = pp.AutoTipTwoEOSReportMessage;
                        if (pp.AutoTipTwoFeeIncluded == 1)
                        {
                            prom.AutoTipTwoFeeIncluded = true;
                        }
                        else
                        {
                            prom.AutoTipTwoFeeIncluded = false;
                        }

                        prom.AutoTipTwoNumOne = pp.AutoTipTwoNumOne;
                        prom.AutoTipTwoNumTwo = pp.AutoTipTwoNumTwo;
                        prom.AutoTipTwoNumThree = pp.AutoTipTwoNumThree;
                        prom.AutoTipTwoNumFour = pp.AutoTipTwoNumFour;

                        if (pp.AutoTipTwoIsPercentOne == 1)
                        {
                            prom.AutoTipTwoIsPercentOne = true;
                        }
                        else
                        {
                            prom.AutoTipTwoIsPercentOne = false;
                        }


                        if (pp.AutoTipTwoIsPercentTwo == 1)
                        {
                            prom.AutoTipTwoIsPercentTwo = true;
                        }
                        else
                        {
                            prom.AutoTipTwoIsPercentTwo = false;
                        }

                        if (pp.AutoTipTwoIsPercentThree == 1)
                        {
                            prom.AutoTipTwoIsPercentThree = true;
                        }
                        else
                        {
                            prom.AutoTipTwoIsPercentThree = false;
                        }

                        if (pp.AutoTipTwoIsPercentFour == 1)
                        {
                            prom.AutoTipTwoIsPercentFour = true;
                        }
                        else
                        {
                            prom.AutoTipTwoIsPercentFour = false;
                        }



                        prom.AutoTipTwoButtonOneID = pp.AutoTipTwoButtonOneID;
                        prom.AutoTipTwoButtonTwoID = pp.AutoTipTwoButtonTwoID;
                        prom.AutoTipTwoButtonThreeID = pp.AutoTipTwoButtonThreeID;
                        prom.AutoTipTwoButtonFourID = pp.AutoTipTwoButtonFourID;
                        prom.AutoTipTwoDefaultButtonID = pp.AutoTipTwoDefaultButtonID;



                        //Screen Text
                        prom.AutoTipScreenText = pp.AutoTipScreenText;
                        prom.AutoTipTwoScreenText = pp.AutoTipTwoScreenText;                        
                    }
                }
            }
            return prom;
        }



        public void SaveLogo(TerminalLogoModel model)
        {
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var term = (from t in con.Terminals
                            where t.TerminalKey == model.TerminalKey
                            select t).FirstOrDefault();

                if (term != null)
                {
                    if (model.ScreenLogoModel != null)
                    {
                        using (TransactionHostEntities con1 = new TransactionHostEntities())
                        {
                            if (term.ScreenLogoKey != null && term.ScreenLogoKey != 0)
                            {
                                var screen = (from t in con1.ScreenLogoes
                                              where t.ScreenLogoKey == term.ScreenLogoKey
                                              select t).FirstOrDefault();

                                screen.VersionNo = screen.VersionNo + 1;
                                screen.LogoImage = model.ScreenLogoModel.LogoImage;
                                screen.UpdatedBy = model.UpdatedBy;
                                screen.UpdatedDate = model.UpdatedDate;
                                screen.Description = model.ScreenLogoModel.Description;
                                model.ScreenLogoKey = screen.ScreenLogoKey;
                                con1.SaveChanges();
                            }
                            else
                            {
                                ScreenLogo sLogo = new ScreenLogo();
                                sLogo.VersionNo = 1;
                                sLogo.LogoImage = model.ScreenLogoModel.LogoImage;
                                sLogo.UpdatedBy = model.UpdatedBy;
                                sLogo.UpdatedDate = model.UpdatedDate;
                                sLogo.CreatedBy = model.UpdatedBy;
                                sLogo.CreatedDate = model.UpdatedDate;
                                sLogo.Description = model.ScreenLogoModel.Description;

                                con1.ScreenLogoes.Add(sLogo);
                                con1.SaveChanges();
                                model.ScreenLogoKey = sLogo.ScreenLogoKey;
                            }

                        }

                    }

                    if (model.ReceiptLogoModel != null)
                    {
                        using (TransactionHostEntities con2 = new TransactionHostEntities())
                        {
                            if (term.ReceiptLogoKey != null && term.ReceiptLogoKey != 0)
                            {

                                var reciept = (from t in con2.ReceiptLogoes
                                               where t.ReceiptLogoKey == term.ReceiptLogoKey
                                               select t).FirstOrDefault();

                                reciept.VersionNo = reciept.VersionNo + 1;
                                reciept.LogoImage = model.ReceiptLogoModel.LogoImage;
                                reciept.UpdatedBy = model.UpdatedBy;
                                reciept.UpdatedDate = model.UpdatedDate;
                                reciept.Description = model.ReceiptLogoModel.Description;
                                model.ReceiptLogoKey = reciept.ReceiptLogoKey;
                                con2.SaveChanges();
                            }
                            else
                            {
                                ReceiptLogo sLogo = new ReceiptLogo();
                                sLogo.VersionNo = 1;
                                sLogo.LogoImage = model.ReceiptLogoModel.LogoImage;
                                sLogo.UpdatedBy = model.UpdatedBy;
                                sLogo.UpdatedDate = model.UpdatedDate;
                                sLogo.CreatedBy = model.UpdatedBy;
                                sLogo.CreatedDate = model.UpdatedDate;
                                sLogo.Description = model.ReceiptLogoModel.Description;

                                con2.ReceiptLogoes.Add(sLogo);
                                con2.SaveChanges();
                                model.ReceiptLogoKey = sLogo.ReceiptLogoKey;
                            }
                        }
                    }

                    if (model.FooterLogoModel != null)
                    {
                        using (TransactionHostEntities con3 = new TransactionHostEntities())
                        {
                            if (term.FooterLogoKey != null && term.FooterLogoKey != 0)
                            {

                                var footer = (from t in con3.FooterLogoes
                                               where t.FooterLogoKey == term.FooterLogoKey
                                               select t).FirstOrDefault();

                                footer.VersionNo = footer.VersionNo + 1;
                                footer.LogoImage = model.FooterLogoModel.LogoImage;
                                footer.UpdatedBy = model.UpdatedBy;
                                footer.UpdatedDate = model.UpdatedDate;
                                footer.Description = model.FooterLogoModel.Description;
                                model.FooterLogoKey = footer.FooterLogoKey;
                                con3.SaveChanges();
                            }
                            else
                            {
                                FooterLogo fLogo = new FooterLogo();
                                fLogo.VersionNo = 1;
                                fLogo.LogoImage = model.FooterLogoModel.LogoImage;
                                fLogo.UpdatedBy = model.UpdatedBy;
                                fLogo.UpdatedDate = model.UpdatedDate;
                                fLogo.CreatedBy = model.UpdatedBy;
                                fLogo.CreatedDate = model.UpdatedDate;
                                fLogo.Description = model.FooterLogoModel.Description;

                                con3.FooterLogoes.Add(fLogo);
                                con3.SaveChanges();
                                model.FooterLogoKey = fLogo.FooterLogoKey;
                            }
                        }
                    }



                    if (model.ScreenLogoKey != null)
                    {
                        term.ScreenLogoKey = model.ScreenLogoKey;
                    }
                    if (model.ReceiptLogoKey != null)
                    {
                        term.ReceiptLogoKey = model.ReceiptLogoKey;
                    }
                    if (model.FooterLogoKey != null)
                    {
                        term.FooterLogoKey = model.FooterLogoKey;
                    }
                    term.VersionNo = term.VersionNo + 1;
                    term.UpdatedBy = model.UpdatedBy;
                    term.UpdatedDate = DateTime.Now;

                    con.SaveChanges();
                }
            }
        }
    }
}
