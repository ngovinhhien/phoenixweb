﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_BusLog.TransactionHost.Model;
using Phoenix_TransactionHostUI;
using System.Data.Entity;
namespace Phoenix_BusLog.TransactionHost.BusLogs
{
    public class OnlineOrderDA
    {
        public int AddOnlineOrder(OnlineOrderModel _Order)
        {
            List<OnlineOrderModel> _Orders = new List<OnlineOrderModel>();
            if (_Order != null)
            {
                using (TransactionHostEntities entities = new TransactionHostEntities())
                {
                    OnlineOrder order = new OnlineOrder();
                    order.TerminalID = _Order.TerminalID;
                    order.MemberID = _Order.MemberID;
                    order.OrderNumber = _Order.OrderNumber;
                    order.CustomerName = _Order.CustomerName;
                    order.CustomerContact = _Order.CustomerContact;
                    if (_Order.OrderDueDateTime > DateTime.Now.AddDays(-1))
                        order.OrderDueDateTime = _Order.OrderDueDateTime;
                    order.OrderTotal = _Order.OrderTotal;
                    order.OrderContent = _Order.OrderContent;
                    if (_Order.ExpectedDueDateTime> DateTime.Now.AddDays(-1))
                        order.ExpectedDueDateTime = _Order.ExpectedDueDateTime;
                    order.DeliveryAddress = _Order.DeliveryAddress;
                    order.DeclinedReason = _Order.DeclinedReason;
                    order.CreatedByUser = _Order.CreatedByUser;
                    order.CreatedDateTime = _Order.CreatedDateTime;

                    OnlineOrder neworder = entities.OnlineOrders.Add(order);
                    entities.SaveChanges();
                    if (neworder != null)
                    { 
                        return neworder.OnlineOrderKey;
                    }
                    else
                    {

                        throw new System.Data.Entity.Core.EntityException("Failed to add Online Order");
                    }
                }
            }
            {
                throw new SystemException("Order is null.");
            }

        }
    }
}
