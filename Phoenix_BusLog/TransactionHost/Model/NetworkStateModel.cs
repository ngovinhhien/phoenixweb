﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class NetworkStateModel : ClientModel
    {
        public int NetworkStateKey { get; set; }
        public int NetworkKey { get; set; }
        public int StateKey { get; set; }

        public virtual NetworkModel Network { get; set; }
        public virtual StateModel State { get; set; }
    }
}
