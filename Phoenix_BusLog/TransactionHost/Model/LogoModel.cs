﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class LogoModel
    {
        public int TerminalKey { get; set; }
        public TerminalModel Terminal { get; set; }
    }
}
