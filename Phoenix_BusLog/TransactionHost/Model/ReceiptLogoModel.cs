﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class ReceiptLogoModel : ClientModel
    {
        public int ReceiptLogoKey { get; set; }
        public byte[] LogoImage { get; set; }
        public int VersionNo { get; set; }
        public string Description { get; set; }
    }
}
