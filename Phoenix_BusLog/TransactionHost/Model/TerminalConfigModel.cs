﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class TerminalConfigModel : ClientModel
    {
        public int TerminalConfigKey { get; set; }
        public int VersionNo { get; set; }
        public string TerminalConfigVersionNo { get; set; }
        public Nullable<int> InputTimeoutSec { get; set; }
        public Nullable<int> BacklightSec { get; set; }
        public Nullable<int> SleepModeMin { get; set; }
        public Nullable<int> ScreenBrightness { get; set; }
        public Nullable<bool> Sound { get; set; }
        public Nullable<bool> RemeberMe { get; set; }
        public string ABNSetting { get; set; }
        public Nullable<decimal> MinimumCharge { get; set; }
        public Nullable<decimal> MaximumCharge { get; set; }
        public string EOSTime { get; set; }
        public string LoginMode { get; set; }
        public string UserId { get; set; }
        public string BusinessId { get; set; }
        public string ABN { get; set; }
        public Nullable<bool> NetworkTableStatus { get; set; }
        public Nullable<int> TNNetworkKey { get; set; }

    }
}
