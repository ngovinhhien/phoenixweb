﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class QantasPointUploadModel
    {
        public string Description { get; set; }
        public int Point { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string MemberId { get; set; }
        public DateTime EntryDateTime { get; set; }
        public string CreatedByUser { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedByUser { get; set; }

    }
}