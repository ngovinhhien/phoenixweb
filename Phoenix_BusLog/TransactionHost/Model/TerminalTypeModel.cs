﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class TerminalTypeModel : ClientModel
    {
        public int TerminalTypeKey { get; set; }

        [Required]
        [DisplayName("Model")]
        public string Model { get; set; }
        public Nullable<int> VendorKey { get; set; }


    }
}
