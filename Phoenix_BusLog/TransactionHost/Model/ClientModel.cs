﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
   public class ClientModel
    {
       protected ClientModel()
       {
           //this.UpdatedDate = DateTime.Now;
       }
        public string FullVersionNo { get; set; }

        [Display(Name = "Created On")]
        public System.DateTime CreatedDate { get; set; }
        public System.Guid CreatedBy { get; set; }
        [Display(Name = "Last Updated On")]
        public System.DateTime UpdatedDate { get; set; }
        public System.Guid UpdatedBy { get; set; }
        [Display(Name = "Created By")]
        public String CreatedByUser { get; set; }
        [Display(Name = "Last Updated By")]
        public String UpdatedByUser { get; set; }

        public Nullable<bool> Inactive { get; set; }
    }
}
