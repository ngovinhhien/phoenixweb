﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{  
       public class BINRangeModel : ClientModel
        {
            public int BINRangeKey { get; set; }
            public string CardName { get; set; }
            public string PANLow { get; set; }
            public string PANHigh { get; set; }
            public int Length { get; set; }
        }
    }

