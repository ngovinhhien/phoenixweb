﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class MerchantModel : ClientModel
    {
        public int MerchantKey { get; set; }
        public Nullable<int> MerchantGroupKey { get; set; }
        public string MerchantName { get; set; }
        public string MerchantID { get; set; }
    }
}
