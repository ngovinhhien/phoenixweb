﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class OnlineOrderModel:ClientModel
    {
        public OnlineOrderModel():base()
        {
            this.CreatedDateTime = DateTime.Now;
        }
        public int OnlineOrderKey { get; set; }
        public string MemberID { get; set; }
        public string TerminalID { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }
        public DateTime OrderDueDateTime { get; set; }
        public decimal OrderTotal { get; set; }
        public string OrderContent { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime ExpectedDueDateTime { get; set; }
        public string DeclinedReason { get;set;}
        public DateTime TerminalReceiveDateTime { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
