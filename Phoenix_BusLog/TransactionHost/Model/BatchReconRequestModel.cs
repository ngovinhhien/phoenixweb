﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class BatchReconRequestModel
    {
        public int BatchReconRequestKey { get; set; }
        public string TerminalID { get; set; }
        public string MerchantID { get; set; }
        public string Batchnumber { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedByDateTime { get; set; }
        public Nullable<System.DateTime> RequestProcessedDate { get; set; }
    }
}
