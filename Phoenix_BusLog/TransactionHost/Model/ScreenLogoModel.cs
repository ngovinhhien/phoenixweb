﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class ScreenLogoModel : ClientModel
    {
        public int ScreenLogoKey { get; set; }
        public byte[] LogoImage { get; set; }
        public int VersionNo { get; set; }
        public string Description { get; set; }
    }
}
