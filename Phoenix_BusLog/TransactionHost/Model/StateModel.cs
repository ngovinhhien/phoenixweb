﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class StateModel : ClientModel
    {

        public int StateKey { get; set; }

        [Required]
        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        [Required]
        [DisplayName("Long Description")]
        public string LongDescription { get; set; }

        //public Nullable<NetworkStateModel> NetworkStates { get; set; }
        //public Nullable<TerminalModel> Terminals { get; set; }

    }
}
