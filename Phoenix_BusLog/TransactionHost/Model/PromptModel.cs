﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class PromptModel : ClientModel
    {
        public int PromptKey { get; set; }
        public string UserId { get; set; }
        public string BusinessId { get; set; }
        public string AmountOne { get; set; }
        public string AmountTwo { get; set; }
        public string AmountThree { get; set; }
        public string AmountFour { get; set; }
        public string AmountFive { get; set; }
        public string AmountSix { get; set; }
        public string AmountSeven { get; set; }
        public string AmountEight { get; set; }
        public string AmountNine { get; set; }
        public int VersionNo { get; set; }
        public Nullable<bool> Inactive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public System.Guid UpdatedBy { get; set; }
        public string LocationOnePrompt { get; set; }
        public string LocationTwoPrompt { get; set; }
        public Nullable<bool> DisableLocationOne { get; set; }
        public Nullable<bool> DisableLocationTwo { get; set; }
        public string EOSButtonText { get; set; }
        public string EOSApprovedText { get; set; }
        public string EOSConfirmText { get; set; }
        public string EOSPriorText { get; set; }
        public string EOSNotFoundText { get; set; }
        public string EOSBatchOpenText { get; set; }
        public Nullable<int> AutoTipNumOne { get; set; }
        public Nullable<int> AutoTipNumTwo { get; set; }
        public Nullable<int> AutoTipNumThree { get; set; }
        public Nullable<int> AutoTipNumFour { get; set; }
        public Nullable<bool> AutoTipIsPercentOne { get; set; }
        public Nullable<bool> AutoTipIsPercentTwo { get; set; }
        public Nullable<bool> AutoTipIsPercentThree { get; set; }
        public Nullable<bool> AutoTipIsPercentFour { get; set; }
        public string AutoTipDisplayOption { get; set; }
        public Nullable<bool> EnableAutoTipAmountDisplay { get; set; }
        public Nullable<bool> EnableAutoTipTotalDisplay { get; set; }
        public string AutoTipTitle { get; set; }
        public string AutoTipPriorAmountLabel { get; set; }
        public Nullable<bool> AutoTipEnterConfirmation { get; set; }
        public string AutoTipEOSReportOption { get; set; }
        public Nullable<int> AutoTipButtonOneFeeCents { get; set; }
        public Nullable<int> AutoTipButtonOneFeePercentage { get; set; }
        public Nullable<int> AutoTipButtonTwoFeeCents { get; set; }
        public Nullable<int> AutoTipButtonTwoFeePercentage { get; set; }
        public Nullable<int> AutoTipButtonThreeFeeCents { get; set; }
        public Nullable<int> AutoTipButtonThreeFeePercentage { get; set; }
        public Nullable<int> AutoTipButtonFourFeeCents { get; set; }
        public Nullable<int> AutoTipButtonFourFeePercentage { get; set; }
        public Nullable<int> AutoTipManualEntryFeeCents { get; set; }
        public Nullable<int> AutoTipManualEntryFeePercentage { get; set; }
        public string AutoTipAmountLabel { get; set; }
        public string AutoTipEOSReportMessage { get; set; }
        public Nullable<bool> AutoTipFeeIncluded { get; set; }
        public Nullable<int> AutoTipButtonOneID { get; set; }
        public Nullable<int> AutoTipButtonTwoID { get; set; }
        public Nullable<int> AutoTipButtonThreeID { get; set; }
        public Nullable<int> AutoTipButtonFourID { get; set; }
        public Nullable<int> AutoTipDefaultButtonID { get; set; }
        public string AutoTipScreenText { get; set; }






        public Nullable<int> AutoTipTwoNumOne { get; set; }
        public Nullable<int> AutoTipTwoNumTwo { get; set; }
        public Nullable<int> AutoTipTwoNumThree { get; set; }
        public Nullable<int> AutoTipTwoNumFour { get; set; }
        public Nullable<bool> AutoTipTwoIsPercentOne { get; set; }
        public Nullable<bool> AutoTipTwoIsPercentTwo { get; set; }
        public Nullable<bool> AutoTipTwoIsPercentThree { get; set; }
        public Nullable<bool> AutoTipTwoIsPercentFour { get; set; }
        public string AutoTipTwoDisplayOption { get; set; }
        public Nullable<bool> EnableAutoTipTwoAmountDisplay { get; set; }
        public Nullable<bool> EnableAutoTipTwoTotalDisplay { get; set; }
        public string AutoTipTwoTitle { get; set; }
        public string AutoTipTwoPriorAmountLabel { get; set; }
        public Nullable<bool> AutoTipTwoEnterConfirmation { get; set; }
        public string AutoTipTwoEOSReportOption { get; set; }
        public Nullable<int> AutoTipTwoButtonOneFeeCents { get; set; }
        public Nullable<int> AutoTipTwoButtonOneFeePercentage { get; set; }
        public Nullable<int> AutoTipTwoButtonTwoFeeCents { get; set; }
        public Nullable<int> AutoTipTwoButtonTwoFeePercentage { get; set; }
        public Nullable<int> AutoTipTwoButtonThreeFeeCents { get; set; }
        public Nullable<int> AutoTipTwoButtonThreeFeePercentage { get; set; }
        public Nullable<int> AutoTipTwoButtonFourFeeCents { get; set; }
        public Nullable<int> AutoTipTwoButtonFourFeePercentage { get; set; }
        public Nullable<int> AutoTipTwoManualEntryFeeCents { get; set; }
        public Nullable<int> AutoTipTwoManualEntryFeePercentage { get; set; }
        public string AutoTipTwoAmountLabel { get; set; }
        public string AutoTipTwoEOSReportMessage { get; set; }
        public Nullable<bool> AutoTipTwoFeeIncluded { get; set; }
        public Nullable<int> AutoTipTwoButtonOneID { get; set; }
        public Nullable<int> AutoTipTwoButtonTwoID { get; set; }
        public Nullable<int> AutoTipTwoButtonThreeID { get; set; }
        public Nullable<int> AutoTipTwoButtonFourID { get; set; }
        public Nullable<int> AutoTipTwoDefaultButtonID { get; set; }
        public string AutoTipTwoScreenText { get; set; }

    }
}
