﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class TerminalBINRangeModel : ClientModel
    {

        public int TerminalBINRangeKey { get; set; }
        public string Description { get; set; }
    }
}
