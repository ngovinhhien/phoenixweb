﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class ReceiptModel : ClientModel
    {

        public int ReceiptKey { get; set; }
        public int VersionNo { get; set; }


        [Display(Name = "Header Line One")]
        [StringLength(128)]
        [Required(ErrorMessage = "Please add some text to {0}")]
        public string HeaderLineOne { get; set; }
        [Display(Name = "Header Line Two")]
        [StringLength(128)]
        public string HeaderLineTwo { get; set; }

        [Display(Name = "Header Line Three")]
        [StringLength(128)]
        public string HeaderLineThree { get; set; }
        [Display(Name = "Header Line Four")]
        [StringLength(128)]
        public string HeaderLineFour { get; set; }

        private string _footerLineOne = String.Empty;

        [Display(Name = "Footer Line One")]
        [StringLength(128)]        
        public string FooterLineOne { get { return _footerLineOne ?? String.Empty; } set { _footerLineOne = value; } }
        [Display(Name = "Footer Line Two")]
        [StringLength(128)]
        public string FooterLineTwo { get; set; }
        [Display(Name = "Footer Line Three")]
        [StringLength(128)]
        public string FooterLineThree { get; set; }
        [Display(Name = "Footer Line Four")]
        [StringLength(128)]
        public string FooterLineFour { get; set; }

        
    }
}
