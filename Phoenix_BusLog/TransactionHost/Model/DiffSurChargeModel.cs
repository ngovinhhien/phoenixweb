﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class DiffSurChargeModel
    {

        public DiffSurChargeModel()
        {
            OptionTwo = new DiffChargeOptionTwoModel();
        }

        public int TerminalKey { get; set; }
        public TerminalModel Terminal { get; set; }

        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public string Type3 { get; set; }
        public string Type4 { get; set; }
        public string EnableAutoTip { get; set; }


        public decimal AutoTip1 { get; set; }
        public decimal AutoTip2 { get; set; }
        public decimal AutoTip3 { get; set; }
        public decimal AutoTip4 { get; set; }

        public bool NoDefault { get; set; }
        public bool AutoTip1Default { get; set; }
        public bool AutoTip2Default { get; set; }
        public bool AutoTip3Default { get; set; }
        public bool AutoTip4Default { get; set; }

        public string AutoTip1s { get; set; }
        public string AutoTip2s { get; set; }
        public string AutoTip3s { get; set; }
        public string AutoTip4s { get; set; }
        public string AutoTip1ss { get; set; }
        public string AutoTip2ss { get; set; }
        public string AutoTip3ss { get; set; }
        public string AutoTip4ss { get; set; }

        public string AutoCardType1 { get; set; }
        public string AutoCardType2 { get; set; }
        public string AutoCardType3 { get; set; }
        public string AutoCardType4 { get; set; }

        public string AutoTipDisplayOption { get; set; }
        public string EnableAutoTipAmountDisplay { get; set; }
        public string EnablrAutoTipTotalDisplay { get; set; }
        public string AutoTipTitle { get; set; }
        public string AutoTipPriorAmountLabel { get; set; }
        public string AutoTipEnterConfirmation { get; set; }
        public string AutoTipEOSReportOption { get; set; }
        public string AutoTipAountLabel { get; set; }
        public string AutoTipEOSReportMessgae { get; set; }
        public string AutoTipFeeIncluded { get; set; }
        public Nullable<int> AutoTipDefaultButtonID { get; set; }
        public string AutoTipScreenText { get; set; }
        public string TerminalID { get { if (this.Terminal != null) return this.Terminal.TerminalID; else return ""; } }

        public bool MakeDefaultTip1Option { get; set;}
        public bool MakeDefaultTip2Option { get; set; }
        public bool MakeDefaultTip3Option { get; set; }
        public bool MakeDefaultTip4Option { get; set; }

        public DiffChargeOptionTwoModel OptionTwo { get; set; }
    }

    public class DiffChargeOptionTwoModel
    {
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public string Type3 { get; set; }
        public string Type4 { get; set; }
        public string EnableAutoTip { get; set; }


        public decimal AutoTip1 { get; set; }
        public decimal AutoTip2 { get; set; }
        public decimal AutoTip3 { get; set; }
        public decimal AutoTip4 { get; set; }

        public bool NoDefault { get; set; }
        public bool AutoTip1Default { get; set; }
        public bool AutoTip2Default { get; set; }
        public bool AutoTip3Default { get; set; }
        public bool AutoTip4Default { get; set; }

        public string AutoTip1s { get; set; }
        public string AutoTip2s { get; set; }
        public string AutoTip3s { get; set; }
        public string AutoTip4s { get; set; }
        public string AutoTip1ss { get; set; }
        public string AutoTip2ss { get; set; }
        public string AutoTip3ss { get; set; }
        public string AutoTip4ss { get; set; }

        public string AutoCardType1 { get; set; }
        public string AutoCardType2 { get; set; }
        public string AutoCardType3 { get; set; }
        public string AutoCardType4 { get; set; }

        public string AutoTipDisplayOption { get; set; }
        public string EnableAutoTipAmountDisplay { get; set; }
        public string EnablrAutoTipTotalDisplay { get; set; }
        public string AutoTipTitle { get; set; }
        public string AutoTipPriorAmountLabel { get; set; }
        public string AutoTipEnterConfirmation { get; set; }
        public string AutoTipEOSReportOption { get; set; }
        public string AutoTipAountLabel { get; set; }
        public string AutoTipEOSReportMessgae { get; set; }
        public string AutoTipFeeIncluded { get; set; }
        public Nullable<int> AutoTipDefaultButtonID { get; set; }
        public string AutoTipScreenText { get; set; }

        public bool MakeDefaultTip1Option { get; set; }
        public bool MakeDefaultTip2Option { get; set; }
        public bool MakeDefaultTip3Option { get; set; }
        public bool MakeDefaultTip4Option { get; set; }
    }
}
