﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class TerminalModel : ClientModel
    {


        public TerminalModel ()
        {
            TerminalHeaderFooter = new ReceiptModel();
        }
        public int TerminalKey { get; set; }
        [Display(Name = "Merchant")]
        public Nullable<int> MerchantKey { get; set; }
        [Display(Name = "Terminal Group")]
        public Nullable<int> TerminalGroupKey { get; set; }
        [Display(Name = "App")]
        public Nullable<int> AppKey { get; set; }
        [Display(Name = "Receipt Logo")]
        public Nullable<int> ReceiptLogoKey { get; set; }
        [Display(Name = "Screen Logo")]
        public Nullable<int> ScreenLogoKey { get; set; }
        [Display(Name = "Prompt")]
        public Nullable<int> PromptKey { get; set; }
        public Nullable<int> TerminalConfigKey { get; set; }
        [Display(Name = "BIN Range")]
        public Nullable<int> TerminalBINRangeKey { get; set; }
        [Display(Name = "Terminal Network")]
        public Nullable<int> TerminalNetworkKey { get; set; }
        [Display(Name = "State")]
        public Nullable<int> StateKey { get; set; }
        [Display(Name = "Receipt")]
        public Nullable<int> ReceiptKey { get; set; }
        [Display(Name = "Network")]
        public Nullable<int> TNNetworkKey { get; set; }
        [Display(Name = "Terminal Type/Model")]
        public int TerminalTypeKey { get; set; }

        [Display(Name = "Terminal ID")]
        public string TerminalID { get; set; }


        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }
        [Display(Name = "User ID")]
        public string UserID { get; set; }
        [Display(Name = "Taxi/Buiness ID")]
        public string BusinessID { get; set; }
        [Display(Name = "ABN")]
        public string ABN { get; set; }
        [Display(Name = "VAA Version")]
        public string VAAVersion { get; set; }
        [Display(Name = "OS Version")]
        public string OSVersion { get; set; }
        [Display(Name = "FS Version")]
        public string FSVersionNO { get; set; }
        [Display(Name = "Discount Applied")]
        public Nullable<bool> DiscountApplied { get; set; }

        public int VersionNo { get; set; }
        [Display(Name = "Config Version Number")]
        public string FullVersionNo { get; set; }
        [Display(Name = "Input Timeout (sec)")]
        public Nullable<int> InputTimeoutSec { get; set; }
        [Display(Name = "Backlight (sec)")]
        public Nullable<int> BacklightSec { get; set; }
        [Display(Name = "Sleep Mode (min)")]
        public Nullable<int> SleepModeMin { get; set; }
        [Display(Name = "Brightness")]
        [Range(0, 100)]
        public Nullable<int> ScreenBrightness { get; set; }
        [Display(Name = "Sound")]
        public Nullable<bool> Sound { get; set; }

        public bool SoundB { get; set; }

        [Display(Name = "Remember Me")]
        public Nullable<bool> RemeberMe { get; set; }
        [Display(Name = "Network Table Status")]
        public Nullable<bool> NetworkTableStatus { get; set; }

        public bool NetworkTableStatusB { get; set; }

        [Display(Name = "ABN Setting")]
        public string ABNSetting { get; set; }

        [Display(Name = "ABN Setting")]
        public decimal ABNSettingB { get; set; }
        [Display(Name = "Minimum Charge")]
        public Nullable<decimal> MinimumCharge { get; set; }
        [Display(Name = "Maximum Charge")]
        public Nullable<decimal> MaximumCharge { get; set; }
        [Display(Name = "End Of Shift Time")]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
        public string EOSTime { get; set; }

        [Display(Name = "Absorb Fee")]
        public string EOSTimeDecimal { get; set; }

        [Display(Name = "Absorb Service fee")]
        public bool IsDefaultServiceFeeZero { get; set; }

        [Display(Name = "Manual EOS")]
        public bool IsManualEOS { get; set; }

        [Display(Name = "Login Mode")]
        public string LoginMode { get; set; }
        [Display(Name = "User ID Login Mode")]
        public string UserIdConfig { get; set; }
        [Display(Name = "Taxi/Business ID Login Mode")]
        public string BusinessIdConfig { get; set; }
        [Display(Name = "ABN Input Mode")]
        public string ABNConfig { get; set; }
        [Display(Name = "GPRS Reconnect Timer")]
        public Nullable<short> GPRSReconnectTimer { get; set; }
        [Display(Name = "App Response Codes")]
        public string AppRespCode { get; set; }
        [Display(Name = "App Signature Response Codes")]
        public string AppSignRespCode { get; set; }
        [Display(Name = "Pre Transaction Ping")]
        public Nullable<bool> IsPreTranPing { get; set; }

        [Display(Name = "Enable Zip")]
        public bool EnableZip { get; set; }

        public bool IsPreTranPingB { get; set; }

        [Display(Name = "Transaction Advice Mode")]
        public Nullable<short> TranAdviceSendMode { get; set; }
        [Display(Name = "Connect To TMS")]
        public Nullable<bool> ConnectToTMS { get; set; }


        [Display(Name = "SIM Serial Number")]
        public string SimSerialNo { get; set; }

        [Display(Name = "SIM Mobile Number")]
        public string SimMobileNumber { get; set; }

        [Display(Name = "IMEI")]
        public string IMEI { get; set; }

        [Display(Name = "PTID")]
        public string PTID { get; set; }

        [Display(Name = "Default Service Fee")]
        public int? DefaultServiceFee { get; set; }

        [Display(Name = "Default Service Fee")]
        public string DefaultServiceFeeDecimal { get; set; }

        [Display(Name = "Service Fee Mode")]
        public string ServiceFeeMode { get; set; }

        [Display(Name = "Customer Payment Type")]
        public string CustomerPaymentType { get; set; }

        [Display(Name = "Print Network on Receipt")]
        public byte? EnableNetworkReceiptPrint { get; set; }

        public bool EnableNetworkReceiptPrintB { get; set; }

        [Display(Name = "Print Logo on Merchant Receipt")]
        public byte? EnableMerchantLogoPrint { get; set; }
        public bool EnableMerchantLogoPrintB { get; set; }

        [Display(Name = "Signature Verification Timeout")]
        public short? SignatureVerificationTimeout { get; set; }

        [Display(Name = "Auto Logon Hour")]
        public byte? AutoLogonHour { get; set; }

        [Display(Name = "Auto Logon Mode")]
        public byte? AutoLogonMode { get; set; }

        [Display(Name = "Enable Refund Tran")]
        public bool? EnableRefundTran { get; set; }
        public bool EnableRefundTranB { get; set; }

        [Display(Name = "Enable Auth Tran")]
        public bool? EnableAuthTran { get; set; }
        public bool EnableAuthTranB { get; set; }

        [Display(Name = "Enable Trip Tran")]
        public bool? EnableTipTran { get; set; }
        public bool EnableTipTranB { get; set; }

        [Display(Name = "Enable Cash Out Tran")]
        public bool? EnableCashOutTran { get; set; }
        public bool EnableCashOutTranB { get; set; }

        [Display(Name = "Merchant Password")]
        public string MerchantPassword { get; set; }

        [Display(Name = "Bank Logon Resp Code")]
        public string BankLogonRespCode { get; set; }

        [Display(Name = "Default Background Colour")]
        public string DefaultBackgroundColour { get; set; }

        [Display(Name = "Enable MOTO")]
        public bool? EnableMOTO { get; set; }
        public bool EnableMOTOB { get; set; }

        [Display(Name = "Main Region")]
        public int? MainRegionKey { get; set; }

        [Display(Name = "Tip Trick")]
        public int? TipTrickKey { get; set; }

        [Display(Name = "Terminal Location")]
        public int? TerminalLocationKey { get; set; }




        [Display(Name = "Clear Override Settings")]
        public bool? ClearOverrideSettings { get; set; }
        public bool ClearOverrideSettingsB { get; set; }


        [Display(Name = "BusinessType Key")]
        public int BusinessTypeKey { get; set; }
        [Display(Name = "Reboot Hours")]
        public byte RebootHours { get; set; }
        [Display(Name = "Enable Auto Tip")]
        public bool EnableAutoTip { get; set; }

        [Display(Name = "Enable MOTO Refund")]
        public bool EnableMOTORefund { get; set; }
        [Display(Name = "Auto EOS Mode")]
        public byte AutoEOSMode { get; set; }
        [Display(Name = "Enable Dual APNMode")]
        public bool EnableDualAPNMode { get; set; }
        public Nullable<int> AutoTipNumOne { get; set; }
        public Nullable<int> AutoTipNumTwo { get; set; }
        public Nullable<int> AutoTipNumThree { get; set; }
        public Nullable<int> AutoTipNumFour { get; set; }
        public Nullable<bool> AutoTipIsPercentOne { get; set; }
        public Nullable<bool> AutoTipIsPercentTwo { get; set; }
        public Nullable<bool> AutoTipIsPercentThree { get; set; }
        public Nullable<bool> AutoTipIsPercentFour { get; set; }
        public Nullable<int> AutoTipButtonOneID { get; set; }
        public Nullable<int> AutoTipButtonTwoID { get; set; }
        public Nullable<int> AutoTipButtonThreeID { get; set; }
        public Nullable<int> AutoTipButtonFourID { get; set; }
        public Nullable<int> AutoTipDefaultButtonID { get; set; }
        public string AutoTipScreenText { get; set; }
        [Display(Name = "Enable Auto Tip")]
        public bool EnableAutoTipTwo { get; set; }




        [Display(Name = "Enable Receipt Reference")]
        public bool EnableReceiptReference { get; set; }

        [Display(Name = "Enable Qantas Earn Points")]
        public bool EnableQantasReward { get; set; }
        [Display(Name = "Enable Qantas Spend Points")]
        public bool EnableQantasPointsRedemption { get; set; }
        [Display(Name = "Enable Business Logo")]
        public bool EnableBusinessLogo { get; set; }
        [Display(Name = "Enable Paper RollOrder")]
        public bool EnablePaperRollOrder { get; set; }
        [Display(Name = "Enable Support CallBack")]
        public bool EnableSupportCallBack { get; set; }
        [Display(Name = "Refund Limit Per Transaction ($)")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Please add a numbers")]
        public int? RefundTransMax { get; set; }
        [Display(Name = "Refund Limit Per Batch/Day ($)")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Please add a numbers")]
        [Range(0,20000, ErrorMessage ="Accept value from 0 to $20,000")]
        public int? RefundCumulativeMax { get; set; }


        [Display(Name = "Levy Charge")]
        [RegularExpression(@"^[0-9]([.,][0-9]{0,3})?$", ErrorMessage = "Please add numbers")]
        [Range(0, 99.99, ErrorMessage = "Please select between {0} and {1}")]
        public decimal LevyCharge { get; set; }

        [Display(Name = "Levy Label")]
        [StringLength(25)]
        public string LevyLabel { get; set; }
        [Display(Name = "Add Levy Charge To Amount")]
        public bool AddLevyChargeToAmount { get; set; }

        [Display(Name = "Enable Levy Charge")]
        public bool EnableLevyCharge { get { return LevyCharge > 0; } }


        [Display(Name="Custom Footer")]
        [StringLength(4000)]      
        [AllowHtml]
        public string CustomFooterHtml { get; set; }

        [Display(Name = "Custom Footer")]
        [StringLength(400)]
        [AllowHtml]
        public string CustomFooter { get; set; }


        public ReceiptModel TerminalHeaderFooter { get; set; }

        //public virtual AppModel App { get; set; }
        //public virtual MerchantModel Merchant { get; set; }
        //public virtual PromptModel Prompt { get; set; }
        //public virtual ReceiptModel Receipt { get; set; }
        //public virtual ReceiptLogoModel ReceiptLogo { get; set; }
        //public virtual ScreenLogoModel ScreenLogo { get; set; }
        //public virtual StateModel State { get; set; }
        //public virtual TerminalBINRangeModel TerminalBINRange { get; set; }
        //public virtual TerminalConfigModel TerminalConfig { get; set; }
        //public virtual TerminalNetworkModel TerminalNetwork { get; set; }
        //public virtual TerminalTypeModel TerminalType { get; set; }


        public int MemberKey { get; set; }

        [Display(Name ="Idle Timer (min)")]
        [Range(0,99, ErrorMessage ="The value must be in range 0 to 99")]
        public int IdleTimer { get; set; }
        [Display(Name = "Idle Transactions")]
        [Range(0, 99, ErrorMessage = "The value must be in range 0 to 99")]
        public int IdleTransactions { get; set; }

        public bool IsSetEOS { get; set; }

        [Display(Name = "Recording Mode")]
        public int RecordingMode { get; set; }

        [Display(Name = "Upload Timer (minutes)")]
        [Range(1, 99, ErrorMessage = "Please enter a value from 1 to 99.")]
        public int UploadTimer { get; set; }

        [Display(Name = "Enable Glidebox Sales (only for Taxi group)")]
        public bool EnableGlidebox { get; set; }

        [StringLength(400)]
        public string ReasonDisableGlidebox { get; set; }

        public bool IsExistCategories { get; set; }
        public bool IsAllowAssignCategory { get; set; }

        [Display(Name = "Merchant Receipt Printing Mode")]
        public int PrintReceiptMode { get; set; }

        [Display(Name = "Print Customer Receipt First")]
        public bool EnablePrintCustomerReceiptFirst { get; set; }

        [Display(Name = "Print Settlement/EOS Report")]
        public bool EnablePrintSettlement { get; set; }

        public int PaymentType { get; set; }

        [Display(Name = "Card Present Transaction Limit ($)")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal CardPresentTransactionLimit { get; set; }

        [Display(Name = "Card Not Present Transaction Limit ($)")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal CardNotPresentTransactionLimit { get; set; }
    }

    public class SwapTerminalModel : TerminalModel
    {
        [Required]
        [Display(Name = "New Serial Number")]
        public string SwapSerialNumber { get; set; }
    }

    public class UploadTerminalModel : TerminalModel
    {
        [Required]
        [Display(Name = "Source File")]
        public HttpPostedFileBase File { get; set; }
    }

}
