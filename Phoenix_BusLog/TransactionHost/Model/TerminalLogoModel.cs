﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class TerminalLogoModel : ClientModel
    {
        public int TerminalKey { get; set; }
        public Nullable<int> ReceiptLogoKey { get; set; }
        public Nullable<int> ScreenLogoKey { get; set; }
        public Nullable<int> FooterLogoKey { get; set; }
        public string TerminalID { get; set; }
        public TerminalModel Terminal { get; set; }
        public bool HasPhoto { get; set; }
        public bool HasFooterLogoPhoto { get; set; }
        public ReceiptLogoModel ReceiptLogoModel { get; set; }
        public ScreenLogoModel ScreenLogoModel { get; set; }
        public FooterLogoModel FooterLogoModel { get; set; }

    }
}
