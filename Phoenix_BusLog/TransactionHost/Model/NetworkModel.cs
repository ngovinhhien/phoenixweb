﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHost.Model
{
    public class NetworkModel : ClientModel
    {
        public int NetworkKey { get; set; }
        public string Name { get; set; }
        public int StateKey { get; set; }
        public string Prefix { get; set; }
    }
}
