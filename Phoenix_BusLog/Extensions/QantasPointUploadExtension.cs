﻿using System;
using Phoenix_BusLog.TransactionHost.Model;
using Phoenix_Service;

namespace Phoenix_BusLog.Extension
{
    public static class QantasPointUploadExtension
    {
        public static void CopyMemberReferenceEntry(this MemberReferenceEntry orginalObject, QantasPointUploadModel model)
        {
            orginalObject.Description = model.Description;
            orginalObject.MonthPeriod = model.Month;
            orginalObject.Points = model.Point;
            orginalObject.YearPeriod = model.Year;
            orginalObject.EntryDateTime = model.EntryDateTime;
            orginalObject.CreatedByUser = model.CreatedByUser;
            orginalObject.CreatedDateTime = DateTime.Now;
            orginalObject.UpdatedByUser = model.UpdatedByUser;
            orginalObject.UpdatedDateTime = DateTime.Now;
        }
    }
}
