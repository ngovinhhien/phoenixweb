﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog
{
   public class TEnum
    {
       public enum App
       {
           LPAYAPI,
           PHOENIX_WEB,
        
       }

       public enum TerminalStatus
       {
           STOCK = 0,
           RETURNED = 1,
           LIVE = 2,
           DAMAGED = 3,
           TERMINATED =4,
           UNKNOWN =7,
       }

       public enum MyWebChangeRequestStatus
       {
           PENDING = 0,
           APPROVED = 1,
           DISPATCHED = 2,
          
       }
       public enum LogStatus
       {
           ERROR = 0,
           INFO = 1,      

       }
    }
    public class TerminalStatus
    {        
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
