﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class CustomerTokenDA
    {
       public string GetToken( string Tokenkey)
       {
           string token = string.Empty;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var tok = (from t in context.CustomerTokens
                          where t.Token == Tokenkey
                          select t).FirstOrDefault();
               if(tok != null)
               {
                   token = tok.Token;
               }
           }
           return token;
       }
    }
}
