﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class SuburbDA
    {
        public List<SuburbModel> GetSuburb(int postcodemasterid)
        {
            List<SuburbModel> subList = new List<SuburbModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.Suburbs
                           where m.PostCodeMaster_key == postcodemasterid
                           select m;

                if (addr != null)
                {
                    foreach (var s in addr)
                    {
                        SuburbModel sub = new SuburbModel();

                        sub.SuburbKey = s.Suburb_key;
                        sub.PostCodeMaster_key = s.PostCodeMaster_key.Value;
                        sub.SuburbName = s.Suburb1;

                        subList.Add(sub);
                    }
                }
            }
            return subList;
        }

        public SuburbModel GetSuburbModel(int subkey)
        {
            SuburbModel sub= new SuburbModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Suburbs
                           where m.Suburb_key == subkey
                           select m).FirstOrDefault();

                if (addr != null)
                {
                    sub.SuburbKey = addr.Suburb_key;
                    sub.PostCodeMaster_key = addr.PostCodeMaster_key.Value;
                    sub.SuburbName = addr.Suburb1;
                    PostCodeDA pda = new PostCodeDA();
                    PostCodeModel pc = new PostCodeModel();
                    pc = pda.GetPostCodeModel(addr.PostCodeMaster_key.Value);
                    StateDA st = new StateDA();
                    sub.State = st.GetStateModel(pc.State_key);
                    sub.PostCode = pc.PostCode;
                    CountryDA cda = new CountryDA();
                    sub.Country = cda.Get(sub.State.Countrykey);
                }
            }
            return sub;
        }
        public int GetSuburbkey(int postcodekey, string subu)
        {
            int key =0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Suburbs
                            where m.PostCodeMaster_key == postcodekey && m.Suburb1 == subu
                            select m).FirstOrDefault();

                if (addr != null)
                {
                   key = addr.Suburb_key;
                }
            }
            return key;
        }
        public bool DoesSuburbExist(string suburb)
        {
            bool exit = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sub = suburb.ToUpper();
                var addr = (from m in context.Suburbs
                            where m.Suburb1 == sub
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    exit = true;
                }
            }
            return exit;
        }
    }
}
