﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Search;
using System.Data.Entity.Validation;
using PhoenixObjects.Financial;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Member;
using Phoenix_Service;

namespace Phoenix_BusLog.Data
{
    public class MemberPhotoIDDA
    {
        public PhotoIDModel GetPhotoID(int PhotoIDKey)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                PhotoID photo = (from p in entity.PhotoIDs
                                 where p.PhotoIDKey == PhotoIDKey
                                 select p).FirstOrDefault();

                if (photo != null)
                {
                    PhotoIDModel photoid = new PhoenixObjects.Member.PhotoIDModel(photo);
                    return photoid;
                }
                else
                    return null;

            }
        }

        public PhotoIDModel GetCurrentPhotoID(int _memberKey)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                int mkey = _memberKey;
                PhotoID photo = (from p in entity.PhotoIDs
                                 join m in entity.Members on p.PhotoIDKey equals m.PhotoIDKey
                                 where m.Member_key == mkey
                                 select p).FirstOrDefault();




                if (photo != null)
                {
                    PhotoIDModel photoid = new PhoenixObjects.Member.PhotoIDModel(photo);
                    return photoid;
                }
                else
                    return null;

            }
        }

        public void AddPhotoID(TaxiEpayEntities entity, PhotoIDModel photo)
        {
            //Add new photoID
            PhotoID photoid = new PhotoID();
            photoid.member_key = photo.member_key;
            photoid.UpdatedByUser = photo.UpdatedByUser;
            photoid.CreatedByUser = photo.CreatedByUser;
            photoid.PhotoID1 = photo.PhotoID;
            photoid.PhotoIDExpiry = photo.PhotoIDExpiry;
            photoid.UpdatedDate = photo.UpdatedDate;
            photoid.CreatedDate = photo.CreatedDate;
            entity.PhotoIDs.Add(photoid);
            entity.SaveChanges();

            //Update the default photoID to the new one
            MemberDA memda = new MemberDA();
            memda.UpdateDefaultPhotoID(entity, photoid.member_key.GetValueOrDefault(0), photoid);
        }


    }
}
