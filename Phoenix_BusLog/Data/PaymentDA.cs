﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class PaymentDA
    {
        public string PayAllPendingsByMember(string MemberId)
        {
            string LodgementReferenceNo = string.Empty;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var logno = context.f_Docket_PayAllPendingsByMember(MemberId).FirstOrDefault();
                if(logno != null)
                {
                    LodgementReferenceNo = logno.LodgementReferenceNo;
                }
            }
            return LodgementReferenceNo;
        }

        public decimal RequestTotalPendingAmountByMember(string MemberId)
        {
            decimal TotalAmount = 0M;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var logno = context.f_Docket_GetAllPendingsByMember(MemberId).FirstOrDefault();
                if (logno != null)
                {
                    TotalAmount = logno.GetValueOrDefault(0);
                }
            }
            return TotalAmount;
        }
        public decimal RequestTotalPendingAmountGlideDriver(string _MemberID)
        {
            decimal TotalAmount = 0M;
            try
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var logno = context.f_Docket_GetAllPendingsByMember(_MemberID).FirstOrDefault();
                    if (logno != null)
                    {
                        TotalAmount = logno.GetValueOrDefault(0);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TotalAmount;
        }

        public IEnumerable<PaymentType> GetAllPaymentTypes()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                return context.PaymentTypes.ToList();
            }
        }
    }
}
