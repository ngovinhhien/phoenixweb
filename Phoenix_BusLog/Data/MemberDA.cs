﻿using Newtonsoft.Json;
using Phoenix_Service;
using Phoenix_TransactionHostUI;
using PhoenixObjects.CommissionRate;
using PhoenixObjects.Common;
using PhoenixObjects.Enums;
using PhoenixObjects.Financial;
using PhoenixObjects.Member;
using PhoenixObjects.Member.CommissionRate;
using PhoenixObjects.Member.Search;
using PhoenixObjects.MyWeb;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PhoenixObjects.Enums;

namespace Phoenix_BusLog.Data
{
    public class MemberDA
    {
        /// <summary>
        /// Get commission rate base on member id
        /// </summary>
        /// <param name="memberID">string as member id</param>
        /// <returns>MemberModel</returns>
        public MemberModel GetComissionRateBaseOnMemberId(string memberID)
        {

            MemberModel member = new MemberModel();
            using (var context = new TaxiEpayEntities())
            {
                var mem = context.Members
                    .Include(n => n.Company)
                    .FirstOrDefault(n => n.MemberId == memberID);
                if (mem == null)
                {
                    return member;
                }

                member.CompanyKey = mem.Company_key;
                member.Member_key = mem.Member_key;
                member.BusinesstypeName = mem.Company.Name;
            }

            DefaultRateModel defaultRateModel = new DefaultRateModel();
            CommissionRateDA cda = new CommissionRateDA();
            VehicleTypeDA vda0 = new VehicleTypeDA();
            int vkey0 = vda0.GetVehicleTypeKey(member.CompanyKey);
            defaultRateModel = cda.GetMemberCommissionRate(vkey0, member.Member_key, member.CompanyKey);
            member.CRDebitRate = defaultRateModel.Debit;
            member.CRDebitRateKey = defaultRateModel.Debit_Commision_Key;
            member.CRVisaRate = defaultRateModel.Visa;
            member.CRVisaRateKey = defaultRateModel.Visa_Commision_Key;
            member.CRMasterCardRate = defaultRateModel.Master;
            member.CRMasterCardRateKey = defaultRateModel.Master_Commision_Key;
            member.CRDinersRate = defaultRateModel.Diners;
            member.CRDinersRateKey = defaultRateModel.Diners_Commision_Key;
            member.CRAmexRate = defaultRateModel.Amex;
            member.CRAmexRateKey = defaultRateModel.Amex_Commision_Key;
            member.CRDebitRatePercentage = defaultRateModel.DebitPercentage;
            member.CRDebitRatePercentageKey = defaultRateModel.DebitPercentage_Commision_Key;
            member.DebitIsFixed = defaultRateModel.DebitIsFixed;

            member.CRUnionPayRate = defaultRateModel.UnionPay;
            member.CRUnionPayRateKey = defaultRateModel.Amex_Commision_Key;
            member.CRZipPayRate = defaultRateModel.Zip;
            member.CRZipPayRateKey = defaultRateModel.Zip_Commision_Key;
            member.CRQantasRate = defaultRateModel.Qantas;
            member.CRQantasRateKey = defaultRateModel.Qantas_Commision_Key;
            member.CRAlipayRate = defaultRateModel.Alipay;
            member.CRAlipayRateKey = defaultRateModel.Alipay_Commision_Key;
            member.CRQantasRatePercentage = defaultRateModel.QantasPercentage;
            member.CRQantasRatePercentageKey = defaultRateModel.QantasPercentage_Commision_Key;
            member.QantasIsFixed = defaultRateModel.QantasIsFixed;

            return member;
        }

        public MemberModel GetComissionRateForMemberWithSubBusiness(string memberID, int businessType,
            int subBusinessType)
        {

            MemberModel member = new MemberModel();
            using (var context = new TaxiEpayEntities())
            {
                var mem = context.Members
                    .Include(n => n.Company)
                    .FirstOrDefault(n => n.MemberId == memberID);
                if (mem == null)
                {
                    return member;
                }

                member.CompanyKey = mem.Company_key;
                member.Member_key = mem.Member_key;
                member.BusinesstypeName = mem.Company.Name;
            }

            DefaultRateModel defaultRateModel = new DefaultRateModel();
            CommissionRateDA cda = new CommissionRateDA();
            VehicleTypeDA vda0 = new VehicleTypeDA();
            int vkey0 = vda0.GetVehicleTypeKey(member.CompanyKey);
            defaultRateModel = cda.GetMemberCommissionRate(subBusinessType, member.Member_key, member.CompanyKey);
            member.CRDebitRate = defaultRateModel.Debit;
            member.CRDebitRateKey = defaultRateModel.Debit_Commision_Key;
            member.CRVisaRate = defaultRateModel.Visa;
            member.CRVisaRateKey = defaultRateModel.Visa_Commision_Key;
            member.CRMasterCardRate = defaultRateModel.Master;
            member.CRMasterCardRateKey = defaultRateModel.Master_Commision_Key;
            member.CRDinersRate = defaultRateModel.Diners;
            member.CRDinersRateKey = defaultRateModel.Diners_Commision_Key;
            member.CRAmexRate = defaultRateModel.Amex;
            member.CRAmexRateKey = defaultRateModel.Amex_Commision_Key;
            member.CRDebitRatePercentage = defaultRateModel.DebitPercentage;
            member.CRDebitRatePercentageKey = defaultRateModel.DebitPercentage_Commision_Key;
            member.DebitIsFixed = defaultRateModel.DebitIsFixed;

            member.CRUnionPayRate = defaultRateModel.UnionPay;
            member.CRUnionPayRateKey = defaultRateModel.Amex_Commision_Key;
            member.CRZipPayRate = defaultRateModel.Zip;
            member.CRZipPayRateKey = defaultRateModel.Zip_Commision_Key;
            member.CRQantasRate = defaultRateModel.Qantas;
            member.CRQantasRateKey = defaultRateModel.Qantas_Commision_Key;
            member.CRAlipayRate = defaultRateModel.Alipay;
            member.CRAlipayRateKey = defaultRateModel.Alipay_Commision_Key;
            member.CRQantasRatePercentage = defaultRateModel.QantasPercentage;
            member.CRQantasRatePercentageKey = defaultRateModel.QantasPercentage_Commision_Key;
            member.QantasIsFixed = defaultRateModel.QantasIsFixed;

            return member;
        }

        public string GetBankAccountAndPurposeTypeOfMember(int memberKey)
        {
            var res = String.Empty;
            using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
            {
                res = taxiEpayDbContext.Database
                    .SqlQuery<string>($"EXEC GetBankAccountAndPurposeTypeOfMember {memberKey}").FirstOrDefault();
            }

            return res;
        }

        public bool IsOperatorMember(int memberKey, string payrunId)
        {
            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                var isDealer = taxiEpayDbContext.Dealers.Any(x => x.Dealer_key == memberKey);
                if (!isDealer)
                {
                    return true; //The member is not Dealer
                }

                var isOperator = taxiEpayDbContext.Operators.Any(x => x.Operator_key == memberKey);
                if (!isOperator)
                {
                    return false; //The member is not Operator
                }

                if (payrunId.StartsWith("D", StringComparison.InvariantCultureIgnoreCase) ||
                    string.IsNullOrEmpty(payrunId))
                {
                    return false; //The member is Dealer when PayRunID has refix by "D"
                }

                return true;
            }
        }

        public List<object> GetPurposeListForMember()
        {
            var objects = new List<object>();
            using (var entities = new TaxiEpayEntities())
            {
                var items = entities.PurposeTypes.ToList();
                foreach (var item in items)
                {
                    var obj = new
                    {
                        item.PurposeTypeID,
                        item.PurposeTypeName,
                        Checked = 0
                    };
                    objects.Add(obj);
                }
            }

            return objects;
        }

        public void UpdateVersionForAllTerminalAllocated(int memberKey, bool forcedUpdateEnablePrintSettlement = false)
        {
            try
            {
                TerminalAllocationDA tda = new TerminalAllocationDA();
                var allVehicleTerminalAllocation =
                    tda.GetAllVehicleTerminalAllocationList(memberKey).Where(n => n.IsAllocated);

                if (!allVehicleTerminalAllocation.Any())
                {
                    return;
                }

                using (TransactionHostEntities transactionhostentities = new TransactionHostEntities())
                {
                    foreach (var vehicleTerminalAllocation in allVehicleTerminalAllocation)
                    {
                        var terminal = transactionhostentities.Terminals.FirstOrDefault(n =>
                            n.TerminalKey == vehicleTerminalAllocation.TerminalKey);

                        if (terminal == null)
                        {
                            continue;
                        }

                        if (forcedUpdateEnablePrintSettlement)
                        {
                            var receiptKey =
                                terminal.ReceiptKey.HasValue
                                    ? terminal.ReceiptKey.Value
                                    : transactionhostentities.Apps
                                        .FirstOrDefault(where => where.AppKey == terminal.AppKey)?.ReceiptKey;

                            if (receiptKey != null)
                            {
                                Receipt receipt =
                                    transactionhostentities.Receipts.FirstOrDefault(where =>
                                        where.ReceiptKey == receiptKey);

                                transactionhostentities.usp_UpdateReceiptData(
                                    receipt.FooterLineOne,
                                    receipt.FooterLineTwo,
                                    receipt.FooterLineThree,
                                    receipt.FooterLineFour,
                                    receipt.HeaderLineOne,
                                    receipt.HeaderLineTwo,
                                    receipt.HeaderLineThree,
                                    receipt.HeaderLineFour,
                                    receipt.EnablePrintCustomerReceiptFirst,
                                    false,
                                    receipt.PrintReceiptMode,
                                    terminal.TerminalID);
                            }
                        }
                    }

                    transactionhostentities.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public MemberModel GetBasicMemberInfo(int memberKey)
        {
            MemberModel member = new MemberModel();
            PostCodeDA cda1ee = new PostCodeDA();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = context.Members.FirstOrDefault(n => n.Member_key == memberKey);
                if (mem != null)
                {
                    MemberAddressDA mda = new MemberAddressDA();

                    member.MemberId = mem.MemberId;
                    member.Member_key = mem.Member_key;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    member.MCC = mem.MCC;

                    if (mem.PrimaryAddressKey != null)
                    {
                        member.PrimaryAddressKey = mem.PrimaryAddressKey.Value;
                        member.PrimaryAddress = mda.Get(mem.PrimaryAddressKey.Value);
                        if (member.PrimaryAddress.Suburb.PostCode != null &&
                            member.PrimaryAddress.Suburb.State.StateName != null &&
                            member.PrimaryAddress.Suburb.SuburbName != null)
                        {
                            member.PrimaryAddress.Suburb.PostCode =
                                cda1ee.GetStatePostCodeLocalitySingle(member.PrimaryAddress.Suburb.PostCode,
                                    member.PrimaryAddress.Suburb.State.StateName,
                                    member.PrimaryAddress.Suburb.SuburbName);
                        }

                        member.State = member.PrimaryAddress.Suburb.State.StateName;
                    }
                    else
                    {
                        member.PrimaryAddress = new MemberAddressModel();
                        member.PrimaryAddress.Suburb = new SuburbModel();
                        member.PrimaryAddressKey = 0;
                        member.PrimaryAddress.StreetName = mem.Address;
                        member.PrimaryAddress.StreetNumber = string.Empty;
                        if (mem.Postcode != null && mem.State != null && mem.City != null)
                        {
                            member.PrimaryAddress.Suburb.PostCode =
                                cda1ee.GetStatePostCodeLocalitySingle(mem.Postcode.Trim(), mem.State.Trim(),
                                    mem.City.Trim());
                        }

                        member.State = mem.State;
                        if (member.PrimaryAddress.Suburb.PostCode == string.Empty)
                        {
                            member.PrimaryAddressDesc =
                                "Address found in database(issues loading address) - " + mem.Address + ", " +
                                mem.City + ", " + mem.State + ", " + mem.Postcode;
                        }

                    }

                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.Mobile = String.IsNullOrEmpty(mem.Mobile) ? "" : mem.Mobile.Replace(" ", "");
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;
                    member.DriverLicenseNumber = mem.DriverLicenseNumber;
                    member.PhotoID = mem.PhotoID;
                    member.PhotoIDExpiry = mem.PhotoIDExpiry;
                    member.Address = mem.Address;
                    member.City = mem.City;
                    member.State = mem.State;
                    member.Postcode = mem.Postcode;
                    member.Active = mem.Active.GetValueOrDefault();

                    member.CompanyKey = mem.Company_key;

                    member.IsDealer = false;

                    member.MerchantID = mem.MerchantID;

                    CompanyDA cda1 = new CompanyDA();
                    var com = cda1.Get(member.CompanyKey);
                    member.BusinesstypeName = com.Name;

                    member.IsBankCustomer = false;
                    member.PaymentType = "Cash";


                    member.DOB = mem.DOB;
                }
            }

            return member;
        }

        public MemberModel GetMemeber(int memberKey)
        {
            MemberModel member = new MemberModel();
            PostCodeDA cda1ee = new PostCodeDA();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = context.Database.SqlQuery<MemberModel>($"SELECT * FROM vMemberInfo WHERE Member_key = {memberKey}").FirstOrDefault();
                if (mem != null)
                {
                    MemberAddressDA mda = new MemberAddressDA();
                    //member.MemberAddresses = mda.GetMemberAddress(memberKey);
                    //member.Addresses = mda.GetAddressListModelList(memberKey);
                    //member.AddrForExistingMember = mda.ForExistingAddrListModel(memberKey);

                    //list of allocated terminals
                    TerminalAllocationDA tda = new TerminalAllocationDA();
                    member.TerminalAllocationList = tda.GetTerminalAllocationList(memberKey);
                    member.AllVehicleTerminalAllocation = tda.GetAllVehicleTerminalAllocationList(memberKey);
                    MemberTerminalRentalDA mtrda = new MemberTerminalRentalDA();
                    member.MemberCurrentTerminalRentalList = mtrda.GetKeyByMemberKey(memberKey)
                        .OrderBy(n => n.TransactionTarget).ToList();

                    member.EnableAFForPhysicalTerminalAllocatedToMerchant =
                        tda.CheckMerchantNotHasVirtualTerminalAllocated(memberKey);

                    MemberRentalDA tda1 = new MemberRentalDA();
                    member.MemberRentalModelList = tda1.GetMemberRentalList(memberKey)
                        .OrderBy(n => n.MemberRental_key).ToList();

                    MyWebDA mwda = new MyWebDA();
                    member.MyWebUserProfile = mwda.GetLoginProfile(memberKey);
                    if (member.MyWebUserProfile.UserIdSelected.GetValueOrDefault() != 0)
                    {
                        List<int> memberKeys =
                            mwda.GetMemberKeysByUserID(member.MyWebUserProfile.UserIdSelected.Value, memberKey);
                        member.MerchantList = context.Members.Where(a => memberKeys.Contains(a.Member_key))
                            .Select(a => new MerchantModel
                            {
                                UserId = member.MyWebUserProfile.UserIdSelected.Value,
                                MemberId = a.MemberId,
                                MemberKey = a.Member_key,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                TradingName = a.TradingName,
                                Email = a.Email,
                                Mobile = a.Mobile
                            }).ToList();
                    }

                    member.MerchantWebsite = GetMerchantWebsiteURL(context, memberKey);

                    DriverDA dra = new DriverDA();
                    string driverCertificate = dra.GetDriverCertificateByMemberkey(memberKey);
                    member.DriverCertificateId = driverCertificate;
                    member.DriverAuth = driverCertificate;

                    TransactionDA trda = new TransactionDA();
                    var trans = trda.GetLastTransactionByMemberkey(memberKey);
                    member.LastCashedAmt = trans.TotalAmount;
                    member.LastCashedDateTime = trans.TransactionDate;


                    AuthorisedMemberDA auth = new AuthorisedMemberDA();
                    List<AuthorisedMemberModel> authList = auth.GetAllByMemberKey(memberKey);
                    member.AuthorisedMemberList = authList;


                    //ReportBuildUtilityDA rbuda = new ReportBuildUtilityDA();
                    //member.WeeklyActivityList = rbuda.GetWeeklyReportList(memberKey);
                    //member.MonthlyActivityList = rbuda.GetMonthlyReportList(memberKey);


                    AccountDA mdacc = new AccountDA();
                    FeeDA fda = new FeeDA();
                    member.BankAccounts = mdacc.Get(memberKey);
                    member.Fees = fda.Get(memberKey);
                    member.ChargeFeeType = fda.GetFees().Select(n => new FeeTypeModel
                    {
                        ID = n.ID,
                        Name = n.Name,
                        MinAmount = n.MinAmount,
                        MaxAmount = n.MaxAmount
                    }).ToList();
                    member.MemberId = mem.MemberId;
                    member.Member_key = mem.Member_key;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    member.MCC = mem.MCC;
                    member.ExtendedSettlement = mem.ExtendedSettlement;
                    member.EOMMSFPayment = mem.EOMMSFPayment;
                    member.AmexMerchantID = mem.AmexMerchantID;
                    member.SubBusinessTypeKey = mem.SubBusinessTypeKey;
                    member.IndirectAmexSettlement = mem.IndirectAmexSettlement;
                    member.IsFeeFreeMember = mem.IsFeeFreeMember;

                    var averageTransaction = mem.AverageTransaction ?? 0;
                    var degree = mem.AverageTransaction == null
                        ? "0000"
                        : averageTransaction.ToString().Split('.')[1];
                    var result = degree == "0000"
                        ? Math.Round(averageTransaction, 0)
                        : Math.Round(averageTransaction, 2);
                    member.AverageTransaction = result.ToString().Length > 10 ? Math.Round(result, 1) : result;

                    var averageTurnover = mem.AverageTurnover ?? 0;
                    degree = mem.AverageTurnover == null ? "0000" : averageTurnover.ToString().Split('.')[1];
                    result = degree == "0000" ? Math.Round(averageTurnover, 0) : Math.Round(averageTurnover, 2);
                    member.AverageTurnover = result.ToString().Length > 10 ? Math.Round(result, 1) : result;

                    if (mem.PrimaryAddressKey != -1)
                    {
                        member.PrimaryAddressKey = mem.PrimaryAddressKey;
                        member.PrimaryAddress = mda.Get(mem.PrimaryAddressKey);
                        if (member.PrimaryAddress.Suburb.PostCode != null &&
                            member.PrimaryAddress.Suburb.State.StateName != null &&
                            member.PrimaryAddress.Suburb.SuburbName != null)
                        {
                            member.PrimaryAddress.Suburb.PostCode =
                                cda1ee.GetStatePostCodeLocalitySingle(member.PrimaryAddress.Suburb.PostCode,
                                    member.PrimaryAddress.Suburb.State.StateName,
                                    member.PrimaryAddress.Suburb.SuburbName);
                        }

                        member.State = member.PrimaryAddress.Suburb.State.StateName;
                    }
                    else
                    {
                        member.PrimaryAddress = new MemberAddressModel();
                        member.PrimaryAddress.Suburb = new SuburbModel();
                        member.PrimaryAddressKey = 0;
                        member.PrimaryAddress.StreetName = mem.Address;
                        member.PrimaryAddress.StreetNumber = string.Empty;
                        if (mem.Postcode != null && mem.State != null && mem.City != null)
                        {
                            member.PrimaryAddress.Suburb.PostCode =
                                cda1ee.GetStatePostCodeLocalitySingle(mem.Postcode.Trim(), mem.State.Trim(),
                                    mem.City.Trim());
                        }

                        member.State = mem.State;
                        if (member.PrimaryAddress.Suburb.PostCode == string.Empty)
                        {
                            member.PrimaryAddressDesc =
                                "Address found in database(issues loading address) - " + mem.Address + ", " +
                                mem.City + ", " + mem.State + ", " + mem.Postcode;
                        }

                    }

                    if (mem.MailingAddressKey != -1)
                    {
                        member.MailingAddressKey = mem.MailingAddressKey;
                        member.MailingAddress = mda.Get(mem.MailingAddressKey);
                        member.MailingAddress.Suburb.PostCode = cda1ee.GetStatePostCodeLocalitySingle(
                            member.MailingAddress.Suburb.PostCode, member.MailingAddress.Suburb.State.StateName,
                            member.MailingAddress.Suburb.SuburbName);
                    }
                    else
                    {
                        member.MailingAddressKey = 0;
                        member.MailingAddress = new MemberAddressModel();
                        member.MailingAddress.Suburb = new SuburbModel();
                    }

                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.Mobile = String.IsNullOrEmpty(mem.Mobile) ? "" : mem.Mobile.Replace(" ", "");
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;
                    member.DriverLicenseNumber = mem.DriverLicenseNumber;
                    member.PhotoID = mem.PhotoID;
                    member.PhotoIDExpiry = mem.PhotoIDExpiry;
                    member.Address = mem.Address;
                    member.City = mem.City;
                    member.State = mem.State;
                    member.Postcode = mem.Postcode;
                    member.Active = mem.Active;

                    //PHW-7
                    using (MemberReferenceDA da = new MemberReferenceDA())
                    {
                        member.QBRNumberHistory = da.Get(mem.Member_key);
                        member.QBRNumber = member.QBRNumberHistory != null && member.QBRNumberHistory.Count() > 0
                            ? member.QBRNumberHistory.Last()
                            : String.Empty;
                    }


                    if (mem.ReceiveMonthlyActivityReport != null)
                    {
                        member.ReceiveMonthlyActivityReportB = mem.ReceiveMonthlyActivityReport.Value;
                    }
                    else
                    {
                        member.ReceiveMonthlyActivityReportB = true;
                    }

                    if (mem.ReceivePaymentReport != null)
                    {
                        member.ReceivePaymentReportB = mem.ReceivePaymentReport.Value;
                    }
                    else
                    {
                        member.ReceivePaymentReportB = true;
                    }

                    if (mem.ReceiveWeeklyActivityReport != null)
                    {
                        member.ReceiveWeeklyActivityReportB = mem.ReceiveWeeklyActivityReport.Value;
                    }
                    else
                    {
                        member.ReceiveWeeklyActivityReportB = true;
                    }

                    member.CompanyKey = mem.CompanyKey;


                    List<MemberCommissionRateModel> allRates = new List<MemberCommissionRateModel>();
                    CommissionRateDA comRateDA = new CommissionRateDA();
                    VehicleTypeDA vda0 = new VehicleTypeDA();

                    var vehicles = vda0.Get(member.CompanyKey);
                    int number = 0;
                    foreach (var v in vehicles)
                    {
                        var comRate = GetAllRates(comRateDA, v.VehicleType_key, member.Member_key, member.CompanyKey, number);

                        if (comRate != null)
                            allRates.Add(comRate);
                    }

                    member.IsDealer = false;
                    member.MerchantID = mem.MerchantID;

                    //PHW-378
                    CompanyDA cda1 = new CompanyDA();
                    var com = cda1.Get(member.CompanyKey);
                    member.BusinesstypeName = com.Name;

                    // member.IsOperator = mem.IsOperator;

                    //member.IsDealer = mem.IsDealer;
                    member.IsBankCustomer = false;
                    member.PaymentType = "Cash";

                    OperatorDA opl = new OperatorDA();
                    member.Operator = opl.Get(memberKey);
                    if (member.Operator.Operator_key == 0)
                    {
                        member.Operator = null;
                    }

                    if (member.Operator != null)
                    {
                        member.OperatorKey = member.Operator.Operator_key;
                        var paymentType = member.Operator.PaymentType;
                        switch (paymentType)
                        {
                            case 1:
                                member.IsBankCustomer = true;
                                member.PaymentType = "Bank";
                                break;
                            case 2:
                                break;
                            case 3:
                                member.IsBankCustomer = false;
                                member.PaymentType = "Card";
                                member.PaymentTypeId = 3;
                                break;
                        }

                        DealerDA dal = new DealerDA();
                        if (member.Operator.Dealer_key != null)
                        {
                            member.Dealer = dal.Get(member.Operator.Dealer_key.Value);
                            member.DealerKey = member.Dealer.Dealer_key;
                            member.Dealer_keyString = dal.GetDealerEdit(member.Dealer.Dealer_key);
                            member.IsDealer = true;
                        }
                    }

                    member.DOB = mem.DOB;

                    MemberPhotoIDDA memphotoDA = new MemberPhotoIDDA();
                    member.CurrentPhotoID = memphotoDA.GetCurrentPhotoID(member.Member_key);

                    ZipMerchantAPIDA zipMerchantAPIDA = new ZipMerchantAPIDA();
                    var zipMerchantAPI = zipMerchantAPIDA.GetByMemberKey(mem.Member_key);
                    member.ZipMerchantAPIModel = zipMerchantAPI != null
                        ? zipMerchantAPIDA.FromZipMerchantAPI(zipMerchantAPI)
                        : new ZipMerchantAPIModel { IsActive = false };
                    member.IsDisableQBR = mem.IsDisableQBR;

                    member.CommissionRates = allRates;
                    member.Rebates = allRates.Any(r => r.CRDebitRebateForPurchaseWithCashOut != null || r.CRDebitRebateForCashAdvance != null) ? new List<MemberRebateModel>()
                        {
                            new MemberRebateModel()
                            {
                                CRDebitRebateForPurchaseWithCashOut = allRates.FirstOrDefault(r => r.CRDebitRebateForPurchaseWithCashOut != null)?.CRDebitRebateForPurchaseWithCashOut,
                                CRDebitRebateForCashAdvance = allRates.FirstOrDefault(r => r.CRDebitRebateForCashAdvance != null)?.CRDebitRebateForCashAdvance,
                            }
                        }
                        :
                        new List<MemberRebateModel>();

                    member.RebatesDtoAsJson = JsonConvert.SerializeObject(member.Rebates);
                    #region Driver Card
                    DriverCardDA driverCardDA = new DriverCardDA();
                    var cards = driverCardDA.GetByMemberKey(memberKey);
                    if (cards.Count > 0)
                    {
                        member.DriverCardDtoAsJson = JsonConvert.SerializeObject(cards);
                    }
                    #endregion
                    member.EnableSplitPaymentReport = GetEnableSplitPaymentReport(memberKey);
                }
            }

            return member;
        }

        private MemberCommissionRateModel GetAllRates(CommissionRateDA cda, int vehicleTypeKey, int memberKey, int companyKey, int number)
        {
            var defaultRateModel = cda.GetMemberCommissionRate(vehicleTypeKey, memberKey, companyKey);
            if (!IsRateEmpty(defaultRateModel))
            {
                return new MemberCommissionRateModel
                {
                    Number = ++number,
                    StartDate = DateTime.Now.AddDays(1),
                    BusinessType = defaultRateModel.BusinessType,
                    SubBusinessType = defaultRateModel.SubBusinessType,
                    BusinessTypeID = companyKey,
                    SubBusinessTypeID = vehicleTypeKey,
                    CRAlipayRate = defaultRateModel.Alipay,
                    CRAmexRate = defaultRateModel.Amex,
                    CRDebitRate = defaultRateModel.Debit,
                    CRDinersRate = defaultRateModel.Diners,
                    CRMasterCardRate = defaultRateModel.Master,
                    CRMasterCardDebitRate = defaultRateModel.MasterDebit,
                    CRQantasRate = defaultRateModel.Qantas,
                    CRUnionPayRate = defaultRateModel.UnionPay,
                    CRVisaRate = defaultRateModel.Visa,
                    CRVisaDebitRate = defaultRateModel.VisaDebit,
                    CRZipPayRate = defaultRateModel.Zip,
                    CRJCBRate = defaultRateModel.JCB,
                    CRDebitRatePercentage = defaultRateModel.DebitPercentage,
                    CRQantasRatePercentage = defaultRateModel.QantasPercentage,
                    IsDebitFixed = defaultRateModel.DebitIsFixed,
                    IsQantasFixed = defaultRateModel.QantasIsFixed,

                    CRDebitTranFee = defaultRateModel.DebitTranFee,
                    CRVisaTranFee = defaultRateModel.VisaTranFee,
                    CRVisaDebitTranFee = defaultRateModel.VisaDebitTranFee,
                    CRMasterCardTranFee = defaultRateModel.MasterTranFee,
                    CRMasterCardDebitTranFee = defaultRateModel.MasterDebitTranFee,
                    CRAmexTranFee = defaultRateModel.AmexTranFee,
                    CRDinersTranFee = defaultRateModel.DinersTranFee,
                    CRUnionPayTranFee = defaultRateModel.UnionPayTranFee,
                    CRZipPayTranFee = defaultRateModel.ZipTranFee,
                    CRQantasTranFee = defaultRateModel.QantasTranFee,
                    CRAlipayTranFee = defaultRateModel.AlipayTranFee,
                    CRJCBTranFee = defaultRateModel.JCBTranFee,

                    CRDebitRebateForPurchaseWithCashOut = defaultRateModel.DebitRebateForPurchaseWithCashOut,
                    CRDebitRebateForCashAdvance = defaultRateModel.DebitRebateForCashAdvance
                };
            }

            return null;
        }

        public string GetMerchantWebsiteURL(TaxiEpayEntities taxiepay, int memberKey)
        {
            if (taxiepay != null)
            {
                return taxiepay.Database
                    .SqlQuery<string>($"SELECT ISNULL(MerchantWebsite,'') AS MerchantWebsite FROM Member WHERE Member_Key = {memberKey}")
                    .FirstOrDefault();
            }

            using (var taxiEpay = new TaxiEpayEntities())
            {
                return taxiEpay.Database
                    .SqlQuery<string>($"SELECT ISNULL(MerchantWebsite,'') AS MerchantWebsite FROM Member WHERE Member_Key = {memberKey}")
                    .FirstOrDefault();
            }
        }

        public bool GetEnableTransactionListOnEOMStatement(TaxiEpayEntities taxiepay, int memberKey)
        {
            if (taxiepay != null)
            {
                return taxiepay.Database
                    .SqlQuery<bool>($"SELECT ISNULL(EnableTransactionListOnMonthlyReport,0) AS EnableTransactionListOnMonthlyReport FROM Member WHERE Member_Key = {memberKey}")
                    .FirstOrDefault();
            }

            using (var taxiEpay = new TaxiEpayEntities())
            {
                return taxiEpay.Database
                    .SqlQuery<bool>($"SELECT ISNULL(EnableTransactionListOnMonthlyReport,0) AS EnableTransactionListOnMonthlyReport FROM Member WHERE Member_Key = {memberKey}")
                    .FirstOrDefault();
            }
        }

        public bool GetEnableSplitPaymentReport(int memberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                return context.Database.SqlQuery<bool>(
                        $"SELECT ISNULL(EnableSplitPaymentReport,0) AS EnableSplitPaymentReport FROM Member WHERE Member_key = {memberKey} ")
                    .FirstOrDefault();
            }
        }

        public string UpdateEnableSplitPaymentReport(TaxiEpayEntities context, int memberKey, bool enabled, string modifiedBy, int companyKey)
        {
            var enableSplitPaymentReport = GetEnableSplitPaymentReport(memberKey);
            var allowCompanyKey = Caching.GetSystemConfigurationByKey("EnableSplitPaymentReport");
            var message = "[EnableSplitPaymentReport] ";
            if (!allowCompanyKey.Split(',').Contains(companyKey.ToString()))
            {
                enabled = false;
                message += "[AutoUpdateByCompanyKeyChanged]";
            }
            if (enableSplitPaymentReport != enabled)
            {
                context.Database.ExecuteSqlCommand(
                    $" UPDATE Member " +
                    $" SET EnableSplitPaymentReport = {(enabled ? 1 : 0)}" +
                    $" WHERE Member_key = {memberKey} ");

                message +=
                    $" EnableSplitPaymentReport has been updated from {(enableSplitPaymentReport ? "checked" : "unchecked")} to {(enabled ? "checked" : "unchecked")} by {modifiedBy}";
                return message;
            }

            return string.Empty;
        }


        private bool IsRateEmpty(DefaultRateModel model)
        {
            if (model.Alipay == null &&
                model.Amex == null &&
                model.Debit == null &&
                model.Diners == null &&
                model.Master == null &&
                model.Qantas == null &&
                model.UnionPay == null &&
                model.Visa == null &&
                model.Zip == null)
            {
                return true;
            }

            return false;
        }

        public List<AllVehicleTerminalAllocationModel> GetAllVehicleTerminalAllocationModelBaseOnMemberKey(
            int memberKey)
        {
            TerminalAllocationDA tda = new TerminalAllocationDA();
            return tda.GetAllVehicleTerminalAllocationList(memberKey);
        }

        public IEnumerable<MemberReferenceModel> GetMemberReference(int memberKey)
        {
            List<MemberReferenceModel> memberReferenceModel = new List<MemberReferenceModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                //Object to Object Mapping?
                var memberReferences = context.MemberReferences.Where(where => where.member_key == memberKey);
                foreach (var memberReference in memberReferences)
                {
                    memberReferenceModel.Add(
                        new MemberReferenceModel()
                        {
                            CreatedByUser = memberReference.CreatedByUser,
                            CreatedDate = memberReference.CreatedDate,
                            MemberReferenceKey = memberReference.MemberReferenceKey,
                            MemberReferenceNumber = memberReference.MemberReferenceNumber,
                            MemberReferenceSourceKey = memberReference.MemberReferenceSourceKey,
                            member_key = memberReference.member_key,
                        });

                }

            }

            return memberReferenceModel;
        }


        public IEnumerable<MemberReferenceModel> GetQbrHistory(int memberKey)
        {
            List<MemberReferenceModel> memberReferenceModel = new List<MemberReferenceModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                //Object to Object Mapping?
                var memberReferences = context.MemberReferences.Where(where => where.member_key == memberKey);
                foreach (var memberReference in memberReferences)
                {
                    memberReferenceModel.Add(
                        new MemberReferenceModel()
                        {
                            CreatedByUser = memberReference.CreatedByUser,
                            CreatedDate = memberReference.CreatedDate,
                            MemberReferenceKey = memberReference.MemberReferenceKey,
                            MemberReferenceNumber = memberReference.MemberReferenceNumber,
                            MemberReferenceSourceKey = memberReference.MemberReferenceSourceKey,
                            member_key = memberReference.member_key,
                        });

                }

            }

            return memberReferenceModel;
        }

        public MemberModelSimple GetMemeberSimple(int memberKey)
        {
            MemberModelSimple member = new MemberModelSimple();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == memberKey
                           select m).FirstOrDefault();
                if (mem != null)
                {

                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.MemberId = mem.MemberId;
                    member.Member_key = mem.Member_key;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    member.Mobile = String.IsNullOrEmpty(mem.Mobile) ? "" : mem.Mobile.Replace(" ", "");
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;
                    member.DriverLicenseNumber = mem.DriverLicenseNumber;

                    member.Active = mem.Active.GetValueOrDefault();


                    member.CompanyKey = mem.Company_key;

                    member.IsDealer = false;

                    CompanyDA cda1 = new CompanyDA();
                    var com = cda1.Get(member.CompanyKey);
                    member.BusinesstypeName = com.Name;

                    // member.IsOperator = mem.IsOperator;

                    //member.IsDealer = mem.IsDealer;
                    member.IsBankCustomer = false;

                    member.DOB = mem.DOB;

                }
            }

            return member;
        }

        public bool IsCashCustomer(int memberKey)
        {
            bool IsBank = true;
            OperatorDA opl = new OperatorDA();
            var ops = opl.Get(memberKey);
            if (ops.PayByEFT.Value == true && ops.Operator_key != 0)
            {
                IsBank = false;
            }

            return IsBank;
        }

        public string GetMemeberMobile(int id)
        {
            string mobile = "";
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == id
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    mobile = mem.Mobile;
                }
            }

            return mobile;
        }

        public int GetMemeberIdByMobileEmail(string Mobile, string Email, string FirstName = "",
            string LastName = "")
        {
            int memberkey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Mobile == Mobile && m.Email == Email && m.Active == true &&
                                 (m.FirstName == FirstName || FirstName == "") &&
                                 (m.LastName == LastName || LastName == "")
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    memberkey = mem.Member_key;
                }
            }

            return memberkey;
        }

        public List<MemberModel> GetMembersByMobileEmail(string Mobile, string Email)
        {
            List<MemberModel> MemberList = new List<MemberModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Active == true && (m.Mobile == Mobile || m.Email == Email)
                           select m);
                foreach (var m in mem)
                {
                    MemberModel newmem = this.GetMemeberLowVersionForCashing(m.Member_key);
                    if (newmem != null)
                        MemberList.Add(newmem);
                }
            }

            return MemberList;
        }

        public MemberModel GetMemeberLowVersion(int id)
        {
            MemberModel member = new MemberModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == id
                           select m).FirstOrDefault();
                if (mem != null)
                {

                    // member.Addresses = mem.Addresses;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.Mobile = mem.Mobile;
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;
                    member.DriverLicenseNumber = mem.DriverLicenseNumber;
                    member.PhotoID = mem.PhotoID;
                    member.PhotoIDExpiry = mem.PhotoIDExpiry;
                    member.Address = mem.Address;
                    member.City = mem.City;
                    member.State = mem.State;
                    member.Postcode = mem.Postcode;
                    member.MemberId = mem.MemberId;
                    member.Member_key = mem.Member_key;

                    member.CompanyKey = mem.Company_key;

                    member.IsDealer = false;

                    CompanyDA cda1 = new CompanyDA();
                    var com = cda1.Get(member.CompanyKey);
                    member.BusinesstypeName = com.Name;


                    member.DOB = mem.DOB;
                }
            }

            DriverDA dra = new DriverDA();
            member.DriverCertificateId = dra.GetDriverCertificateByMemberkey(id);

            TransactionDA trda = new TransactionDA();
            var trans = trda.GetLastTransactionByMemberkey(id);
            member.LastCashedAmt = trans.TotalAmount;
            member.LastCashedDateTime = trans.TransactionDate;


            DefaultRateModel defaultRateModel = new DefaultRateModel();
            CommissionRateDA cda = new CommissionRateDA();
            VehicleTypeDA vda0 = new VehicleTypeDA();
            int vkey0 = vda0.GetVehicleTypeKey(member.CompanyKey);
            defaultRateModel = cda.GetMemberCommissionRate(vkey0, member.Member_key, member.CompanyKey);
            member.CRDebitRate = defaultRateModel.Debit;
            member.CRVisaRate = defaultRateModel.Visa;
            member.CRMasterCardRate = defaultRateModel.Master;
            member.CRDinersRate = defaultRateModel.Diners;
            member.CRAmexRate = defaultRateModel.Amex;
            member.CRDebitRatePercentage = defaultRateModel.DebitPercentage;

            return member;
        }


        public MemberModel GetMemeberLowVersionForCashing(int id)
        {
            MemberModel member = new MemberModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == id
                           select m).FirstOrDefault();
                if (mem != null)
                {

                    // member.Addresses = mem.Addresses;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.Mobile = mem.Mobile;
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;
                    member.DriverLicenseNumber = mem.DriverLicenseNumber;
                    member.PhotoID = mem.PhotoID;
                    member.PhotoIDExpiry = mem.PhotoIDExpiry;
                    member.Address = mem.Address;
                    member.City = mem.City;
                    member.State = mem.State;
                    member.Postcode = mem.Postcode;
                    member.MemberId = mem.MemberId;
                    member.Member_key = mem.Member_key;

                    member.CompanyKey = mem.Company_key;

                    member.IsDealer = false;

                    CompanyDA cda1 = new CompanyDA();
                    var com = cda1.Get(member.CompanyKey);
                    member.BusinesstypeName = com.Name;


                    member.DOB = mem.DOB;
                }
            }

            DriverDA dra = new DriverDA();
            member.DriverCertificateId = dra.GetDriverCertificateByMemberkey(id);



            return member;
        }


        public MemberModelAPI GetMemeberForAPI(int memberKey)
        {
            MemberModelAPI member = new MemberModelAPI();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == memberKey && m.Active == true
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    MemberAddressDA mda = new MemberAddressDA();
                    //member.MemberAddresses = mda.GetMemberAddress(memberKey);

                    AccountDA mdacc = new AccountDA();
                    var bk = mdacc.Get(memberKey).Where(r => r.Weight > 0).FirstOrDefault();
                    if (bk != null)
                    {
                        member.AccountName = bk.AccountName;
                        member.AccountNumber = bk.AccountNumber;
                        member.BSB = bk.BSB;
                    }

                    // member.MemberId = mem.MemberId;
                    member.ReferenceId = mem.MemberId;
                    member.FirstName = mem.FirstName;
                    member.LastName = mem.LastName;
                    if (mem.PrimaryAddressKey != null)
                    {
                        var pr = mda.Get(mem.PrimaryAddressKey.Value);
                        member.PrimaryAddress = new MemberAddressModelAPI();
                        member.PrimaryAddress.AddressType = pr.AddressType;
                        member.PrimaryAddress.StreetName = pr.StreetName;
                        member.PrimaryAddress.StreetNumber = pr.StreetNumber;
                        member.PrimaryAddress.Suburb = pr.Suburb.SuburbName;
                        member.PrimaryAddress.State = pr.Suburb.State.StateName;
                        member.PrimaryAddress.PostCode = pr.Suburb.PostCode;

                    }

                    if (mem.MailingAddressKey != null)
                    {
                        var ml = mda.Get(mem.MailingAddressKey.Value);
                        member.MailingAddress = new MemberAddressModelAPI();
                        member.MailingAddress.AddressType = ml.AddressType;
                        member.MailingAddress.StreetName = ml.StreetName;
                        member.MailingAddress.StreetNumber = ml.StreetNumber;
                        member.MailingAddress.Suburb = ml.Suburb.SuburbName;
                        member.MailingAddress.State = ml.Suburb.State.StateName;
                        member.MailingAddress.PostCode = ml.Suburb.PostCode;
                    }

                    member.Telephone = mem.Telephone;
                    member.Fax = mem.Fax;
                    member.Mobile = mem.Mobile;
                    member.Email = mem.Email;
                    member.TradingName = mem.TradingName;
                    member.ABN = mem.ABN;
                    member.ACN = mem.ACN;



                    DefaultRateModel defaultRateModel = new DefaultRateModel();
                    CommissionRateDA cda = new CommissionRateDA();
                    VehicleTypeDA vda0 = new VehicleTypeDA();
                    int vkey0 = vda0.GetVehicleTypeKey(mem.Company_key);
                    defaultRateModel = cda.GetMemberCommissionRate(vkey0, mem.Member_key, mem.Company_key);
                    member.DebitRate = defaultRateModel.Debit;
                    member.VisaRate = defaultRateModel.Visa;
                    member.MasterCardRate = defaultRateModel.Master;
                    member.DinersRate = defaultRateModel.Diners;
                    member.AmexRate = defaultRateModel.Amex;
                    if (mem.DOB != null)
                    {
                        string dd = mem.DOB.Value.Day.ToString("00");
                        string mm = mem.DOB.Value.Month.ToString("00");
                        string yy = mem.DOB.Value.Year.ToString();
                        string dobstring = dd + mm + yy;
                        member.DOB = dobstring;
                    }

                    if (defaultRateModel.DebitIsFixed)
                        member.IsDebitRatePercent = false;
                    else
                        member.IsDebitRatePercent = true;
                    member.DriverLicense = mem.DriverLicenseNumber;
                    if (mem.Driver != null && !string.IsNullOrEmpty(mem.Driver.DriverCertificateId))
                        member.DriverAuthorityNumber = mem.Driver.DriverCertificateId;

                }
            }

            return member;
        }


        public bool DoesMemberEmailMobileExists(string Email, string Mobile)
        {
            //bool member = false;
            int memberkey = 0;
            memberkey = GetMemeberIdByMobileEmail(Mobile, Email);
            return memberkey > 0;
            //using (TaxiEpayEntities context = new TaxiEpayEntities())
            //{
            //    var mem = (from m in context.Members
            //               where m.FirstName == Firstname && m.LastName == Lastname && m.Email == Email && m.TradingName == Tradingname && m.Active == true
            //               select m).FirstOrDefault();
            //    if (mem != null)
            //    {
            //        member = true;
            //    }
            //}
            //return member;
        }

        public bool DoesMemberExists(string Firstname, string Lastname, string Email, string Tradingname)
        {
            bool member = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.FirstName == Firstname && m.LastName == Lastname && m.Email == Email &&
                                 m.TradingName == Tradingname && m.Active == true
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    member = true;
                }
            }

            return member;
        }

        public MemberBalanceModel GetMemberBalance(int Member_Key)
        {
            MemberBalanceModel MemberBalance = new MemberBalanceModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                byte[] photo = null;
                var mem = context.ATMLoadMemberInfo(Member_Key).FirstOrDefault();
                using (TaxiEpayEntities context1 = new TaxiEpayEntities())
                {
                    var mem1 = (from m in context1.Members
                                where m.Member_key == Member_Key
                                select m).FirstOrDefault();
                    if (mem1 != null)
                    {
                        if (mem1.PhotoID != null)
                        {
                            photo = mem1.PhotoID;
                        }
                    }
                }

                if (mem != null)
                {
                    MemberBalance.Balance = mem.Balance;
                    MemberBalance.FirstName = mem.FirstName;
                    MemberBalance.LastName = mem.LastName;
                    MemberBalance.Member_key = mem.Member_key;
                    MemberBalance.MemberID = mem.MemberID;
                    MemberBalance.TradingName = mem.TradingName;
                    MemberBalance.photo = photo;
                }

                if (mem == null)
                {
                    using (TaxiEpayEntities context1 = new TaxiEpayEntities())
                    {
                        var mem1 = (from m in context1.Members
                                    where m.Member_key == Member_Key
                                    select m).FirstOrDefault();
                        if (mem1 != null)
                        {

                            MemberBalance.Balance = 0.0M;
                            MemberBalance.FirstName = mem1.FirstName;
                            MemberBalance.LastName = mem1.LastName;
                            MemberBalance.Member_key = mem1.Member_key;
                            MemberBalance.MemberID = mem1.MemberId;
                            MemberBalance.TradingName = mem1.TradingName;
                            MemberBalance.photo = mem1.PhotoID;
                        }
                    }

                }
            }

            return MemberBalance;
        }


        //public List<MemberModel> GetMemberGeneralSearch(string SearchKey)
        //{         
        //    List<MemberModel> model = new List<MemberModel>();
        //    using (TaxiEpayEntities context = new TaxiEpayEntities())
        //    {
        //        var mem = from m in context.Members
        //                   where m.MemberId.Contains(SearchKey) || m.FirstName.Contains(SearchKey) || m.LastName.Contains(SearchKey)
        //                   || m.Telephone.Contains(SearchKey) || m.Mobile.Contains(SearchKey) || m.State.Contains(SearchKey) || m.Email.Contains(SearchKey)
        //                   || m.City.Contains(SearchKey)
        //                   select m;
        //        if (mem != null)
        //        {
        //            foreach(var m in mem)
        //            {
        //                MemberModel m1 = new MemberModel();
        //                m1 = GetMemeberLowVersion(m.Member_key);
        //                model.Add(m1);
        //            }                  
        //        }
        //    }
        //    return model;
        //}

        public List<MemberModelSimple> GetMemberGeneralSearch(string SearchKey)
        {
            List<MemberModelSimple> model = new List<MemberModelSimple>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                model = (from mem in context.Members
                         join d in context.DriverCards on mem.Member_key equals d.MemberKey into md
                         from driver in md.DefaultIfEmpty()
                         join com in context.Companies on mem.Company_key equals com.Company_key
                         where mem.MemberId.Contains(SearchKey) ||
                               mem.FirstName.Contains(SearchKey) ||
                               mem.LastName.Contains(SearchKey) ||
                               mem.Telephone.Contains(SearchKey) ||
                               mem.Mobile.Contains(SearchKey) ||
                               mem.State.Contains(SearchKey) ||
                               mem.Email.Contains(SearchKey) ||
                               mem.City.Contains(SearchKey) ||
                               driver.AccountIdentifier == SearchKey
                         select new MemberModelSimple
                         {
                             MemberId = mem.MemberId,
                             TradingName = mem.TradingName,
                             FirstName = mem.FirstName,
                             LastName = mem.LastName,
                             Email = mem.Email,
                             BusinesstypeName = com.Name,
                             Member_key = mem.Member_key,
                             Mobile = mem.Mobile
                         })
                            .Distinct()
                            .ToList();
            }

            return model;
        }

        //public List<MemberModel> GetMemberDriverAuthSearch(string driverCertificateId)
        //{
        //    List<MemberModel> model = new List<MemberModel>();
        //    using (TaxiEpayEntities context = new TaxiEpayEntities())
        //    {
        //        var mem = (from m in context.Drivers
        //                  where m.DriverCertificateId == driverCertificateId
        //                  select m).FirstOrDefault();
        //        if (mem != null)
        //        {                   
        //                MemberModel m1 = new MemberModel();
        //                m1 = GetMemeberLowVersion(mem.Driver_key);
        //                model.Add(m1);                   
        //        }
        //    }
        //    return model;
        //}

        public List<MemberModelSimple> GetMemberDriverAuthSearch(string driverCertificateId)
        {
            List<MemberModelSimple> model = new List<MemberModelSimple>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Drivers
                           where m.DriverCertificateId == driverCertificateId
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    MemberModel m1 = new MemberModel();
                    m1 = GetMemeberLowVersion(mem.Driver_key);
                    model.Add(m1);
                }
            }

            return model;
        }

        public int GetMemberCompanyKey(int Memberkey)
        {
            int Company_key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == Memberkey
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    Company_key = mem.Company_key;
                }
            }

            return Company_key;
        }

        public int GetMemberKey(string MemberId)
        {
            int Member_key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.MemberId == MemberId
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    Member_key = mem.Member_key;
                }
            }

            return Member_key;
        }

        public void UpdateModifiedDate(int memberKey, string username)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = context.Members.FirstOrDefault(n => n.Member_key == memberKey);

                if (mem != null)
                {
                    mem.Modified_dttm = DateTime.Now;
                    mem.ModifiedByUser = username;
                    context.SaveChanges();
                }
            }
        }

        public string GetMemberId(int Member_Key)
        {
            string MemberId = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                //var mem = (from m in context.Members
                //           where m.Member_key == Member_Key
                //           select m).FirstOrDefault();
                //if (mem != null)
                //{
                //    MemberId = mem.MemberId;
                //}

                MemberId = context.Members.FirstOrDefault(x => x.Member_key == Member_Key)?.MemberId ?? string.Empty;
            }

            return MemberId;
        }

        public int GetMemberKeyByTerminalIdByEffecDate(string TerminalId, DateTime dat)
        {
            int Member_key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = context.TerminalAllocation_GetByAllocationDate(dat, TerminalId).FirstOrDefault();

                if (mem != null)
                {
                    Member_key = mem.Member_key.Value;
                }
            }

            return Member_key;
        }

        public int GetMemberKeyByTerminalId(string TerminalId)
        {
            int Member_key = 0;
            TerminalDA tda = new TerminalDA();
            int terminal_key = tda.GetTerminalKey(TerminalId);
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.TerminalAllocations
                           where m.IsCurrent == true && m.EftTerminal_key == terminal_key && m.Member_key != null
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    Member_key = mem.Member_key.Value;
                }
            }

            return Member_key;
        }

        public List<SearchListModel> SearchField(string remove, string add)
        {
            List<string> SearchFieldString = new List<string>();
            SearchFieldString.Add("MemberId");
            SearchFieldString.Add("TerminalId");
            SearchFieldString.Add("Mobile");
            SearchFieldString.Add("BusinessId");
            SearchFieldString.Add("FirstName");
            SearchFieldString.Add("LastName");
            SearchFieldString.Add("Email");
            SearchFieldString.Add("TradingName");
            SearchFieldString.Add("State");
            SearchFieldString.Add("Suburb");


            if (remove != string.Empty)
            {
                SearchFieldString.Remove(remove);
            }

            if (add != string.Empty)
            {
                SearchFieldString.Add(add);
            }

            List<SearchListModel> SearchFieldList = new List<SearchListModel>();
            foreach (var s in SearchFieldString)
            {
                SearchListModel sm = new SearchListModel();
                sm.SearchField = s;
                sm.SearchKey = s;
                SearchFieldList.Add(sm);
            }

            return SearchFieldList;
        }

        public byte[] GetMemberPhoto(int memberkey)
        {
            byte[] photo = null;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == memberkey
                           select m.PhotoID).FirstOrDefault();
                if (mem != null)
                {
                    photo = mem;
                    return photo;
                }
            }

            return photo;
        }

        public byte[] GetMemberPhotoByMemberId(string memberId)
        {
            byte[] photo = null;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.MemberId == memberId
                           select m.PhotoID).FirstOrDefault();
                if (mem != null)
                {
                    photo = mem;
                    return photo;
                }
            }

            return photo;
        }

        public List<MemberModel> GetMemeberList(string id)
        {
            List<MemberModel> memberList = new List<MemberModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = from m in context.Members
                          where m.MemberId == id
                          select m;
                if (mem != null)
                {
                    foreach (var m in mem)
                    {
                        MemberModel member = new MemberModel();
                        member = GetMemeber(m.Member_key);
                        memberList.Add(member);
                    }
                }
            }

            return memberList;
        }

        public bool GetMemeberBySalesforceId(string id)
        {
            bool IsMember = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.SaleforceID == id
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    IsMember = true;

                }
            }

            return IsMember;
        }



        public bool GetMemeberByLiveSMS(int Member_key)
        {
            bool IsMember = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MemberLiveSMS_Exported
                           where m.Member_key == Member_key
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    IsMember = true;
                }
            }

            return IsMember;
        }


        public int CloneMember(MemberModel t)
        {
            int member_key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                //add memeber
                member_key = AddClone(t);

                if (t.Addresses.Count() > 0)
                {
                    //add address
                    MemberAddressDA mda = new MemberAddressDA();
                    foreach (var addr in t.Addresses)
                    {
                        mda.AddClone(addr, member_key);
                    }
                }
                else
                {
                    MemberAddressDA mda = new MemberAddressDA();
                    foreach (var addr in t.AddrForExistingMember)
                    {
                        addr.AddressType = "Primary";
                        mda.AddClone(addr, member_key);
                    }
                }

                //add account
                AccountDA mdac = new AccountDA();
                foreach (var acc in t.BankAccounts)
                {
                    mdac.AddClone(acc, member_key);
                }


                if (t.Operator != null)
                {
                    OperatorDA oda = new OperatorDA();
                    oda.AddClone(t.Operator, member_key);
                }
            }

            return member_key;
        }

        public void UpdateFeeChargeDetail(MemberModel t, string UpdatedUser)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mServer = (from m in context.Members
                               where m.Member_key == t.Member_key
                               select m).FirstOrDefault();
                if (mServer != null)
                {
                    if (!string.IsNullOrEmpty(t.FeeDtoAsJson))
                    {
                        FeeDA fda = new FeeDA();
                        var accountList = JsonConvert.DeserializeObject<List<FeeModel>>(t.FeeDtoAsJson);
                        fda.UpdateFeeDetailToMember(context, accountList, mServer, UpdatedUser);
                    }

                    context.SaveChanges();
                }
            }
        }

        public void Update(MemberModel memberModel, string UpdatedUser, ref List<CardSurchargeInfo> cardSurchargeInfos, bool forceUpdateAllTerminalsBelongToOneVehicleType = false)
        {
            var memberKey = 0;
            var needToIncreaseReceiptVersion = false;
            var needToUpdatePrintSettlement = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var mServer = (from m in context.Members
                               where m.Member_key == memberModel.Member_key
                               select m).FirstOrDefault();

                if (mServer != null)
                {
                    memberKey = mServer.Member_key;
                    mServer.FirstName = memberModel.FirstName;
                    mServer.LastName = memberModel.LastName;
                    mServer.Telephone = memberModel.Telephone;
                    mServer.Fax = memberModel.Fax;
                    mServer.Mobile = memberModel.Mobile;
                    mServer.TradingName = memberModel.TradingName;
                    mServer.ABN = memberModel.ABN;
                    mServer.ACN = memberModel.ACN;
                    mServer.MCC = memberModel.MCC;
                    mServer.DriverLicenseNumber = memberModel.DriverLicenseNumber;
                    mServer.Email = memberModel.Email;
                    mServer.MerchantID = memberModel.MerchantID;
                    mServer.ReceiveMonthlyActivityReport = memberModel.ReceiveMonthlyActivityReport;
                    mServer.ReceivePaymentReport = memberModel.ReceivePaymentReport;
                    mServer.ReceiveWeeklyActivityReport = memberModel.ReceiveWeeklyActivityReport;
                    mServer.Active = memberModel.Active;
                    mServer.Company_key = memberModel.CompanyKey;
                    mServer.IsDisableQBR = memberModel.IsDisableQBR;
                    bool forcedUpdateEcomMSF = memberModel.IsFeeFreeMember && !mServer.IsFeeFree; //Force update eCom MSF in case member has been marked to fee free.
                    bool removedFeeFree = mServer.IsFeeFree && !memberModel.IsFeeFreeMember;
                    mServer.IsFeeFree = memberModel.IsFeeFreeMember;

                    if (removedFeeFree)
                    {
                        var memberTerminalRentalDA = new MemberTerminalRentalDA();
                        int.TryParse(Caching.GetSystemConfigurationByKey("FeefreeRentalTemplateKey"),
                            out int feeFreeRentalTemplateKey);
                        memberTerminalRentalDA.UpdateEndDate(context, memberKey, feeFreeRentalTemplateKey, DateTime.Now.AddDays(-1), UpdatedUser);
                    }

                    MemberHistoryDA historyDA = new MemberHistoryDA();
                    string history = string.Empty;
                    if (mServer.EOMMSFPayment.GetValueOrDefault(false) != memberModel.EOMMSFPayment)
                    {
                        history +=
                            $" [EOM_MSF_Payment] {UpdatedUser} has update EOM MSF Payment from {(mServer.EOMMSFPayment.GetValueOrDefault(false) ? "checked" : "unchecked")} to {(memberModel.EOMMSFPayment ? "checked" : "unchecked")};";
                        mServer.EOMMSFPayment = memberModel.EOMMSFPayment;
                    }

                    if (mServer.ExtendedSettlement.GetValueOrDefault(false) != memberModel.ExtendedSettlement)
                    {
                        history +=
                            $" [Extended_Settlement] {UpdatedUser} has update Extended Settlement from {(mServer.ExtendedSettlement.GetValueOrDefault(false) ? "checked" : "unchecked")} to {(memberModel.ExtendedSettlement ? "checked" : "unchecked")};";
                        mServer.ExtendedSettlement = memberModel.ExtendedSettlement;
                    }

                    if (mServer.IndirectAmexSettlement != memberModel.IndirectAmexSettlement)
                    {
                        history +=
                            $" [Indirect_Amex_Settlement] {UpdatedUser} has update Live Group Amex Settlement from {(mServer.IndirectAmexSettlement ? "checked" : "unchecked")} to {(memberModel.IndirectAmexSettlement ? "checked" : "unchecked")};";
                        mServer.IndirectAmexSettlement = memberModel.IndirectAmexSettlement;
                    }

                    //if (!string.IsNullOrEmpty(history))
                    //{
                    //    historyDA.Add(mServer.Member_key, history.Trim().Trim(';'), UpdatedUser);
                    //}

                    if (!String.IsNullOrEmpty(memberModel.BankAccountDtoAsJson))
                    {
                        AccountDA accountDa = new AccountDA();
                        var accountList =
                            JsonConvert.DeserializeObject<List<BankAccountDTO>>(memberModel.BankAccountDtoAsJson);
                        accountDa.UpdateBankAccountToMember(context, accountList, mServer, UpdatedUser);
                    }

                    var businessTypesAllowRebate = Caching.GetSystemConfigurationByKey("BusinessKeysAllowRebate").Split(';').ToList();
                    var debitRebate = string.IsNullOrEmpty(memberModel.RebatesDtoAsJson) ?
                        null
                        :
                        JsonConvert.DeserializeObject<List<MemberRebateModel>>(memberModel.RebatesDtoAsJson).FirstOrDefault();
                    var bUpdateRebateForAllRate = debitRebate?.IsEdited ?? false;

                    var commissionRates = new List<MemberCommissionRateModel>();
                    var needToUpdateCommissionRates = new List<MemberCommissionRateModel>();
                    var comRateDA = new CommissionRateDA();
                    var eCommerceVehicleTypeKeys = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.eCommerceVehicleTypeKey.ToString()).Split(',');

                    if (!string.IsNullOrEmpty(memberModel.CommissionRatesDtoAsJson))
                    {
                        commissionRates = JsonConvert.DeserializeObject<List<MemberCommissionRateModel>>(memberModel.CommissionRatesDtoAsJson);
                    }

                    needToUpdateCommissionRates = bUpdateRebateForAllRate ? commissionRates : commissionRates.Where(c => c.IsEdited
                        || (forcedUpdateEcomMSF && eCommerceVehicleTypeKeys.Contains(c.SubBusinessTypeID.ToString()))).ToList();

                    foreach (var comRate in needToUpdateCommissionRates)
                    {
                        if (comRate.DebitRateType == "Percentage")
                        {
                            comRate.CRDebitRatePercentage =
                                comRate.CRDebitRate != 0 ? comRate.CRDebitRate / 100 : comRate.CRDebitRate;
                        }

                        comRate.CRMasterCardRate = comRate.CRMasterCardRate != 0
                            ? comRate.CRMasterCardRate / 100
                            : comRate.CRMasterCardRate;
                        comRate.CRMasterCardDebitRate = comRate.CRMasterCardDebitRate != 0
                            ? comRate.CRMasterCardDebitRate / 100
                            : comRate.CRMasterCardDebitRate;
                        comRate.CRVisaRate = comRate.CRVisaRate != 0 ? comRate.CRVisaRate / 100 : comRate.CRVisaRate;
                        comRate.CRVisaDebitRate = comRate.CRVisaDebitRate != 0
                            ? comRate.CRVisaDebitRate / 100
                            : comRate.CRVisaDebitRate;
                        comRate.CRAmexRate = comRate.CRAmexRate != 0 ? comRate.CRAmexRate / 100 : comRate.CRAmexRate;
                        comRate.CRDinersRate =
                            comRate.CRDinersRate != 0 ? comRate.CRDinersRate / 100 : comRate.CRDinersRate;
                        comRate.CRUnionPayRate = comRate.CRUnionPayRate != 0
                            ? comRate.CRUnionPayRate / 100
                            : comRate.CRUnionPayRate;
                        comRate.CRZipPayRate =
                            comRate.CRZipPayRate != 0 ? comRate.CRZipPayRate / 100 : comRate.CRZipPayRate;
                        comRate.CRAlipayRate =
                            comRate.CRAlipayRate != 0 ? comRate.CRAlipayRate / 100 : comRate.CRAlipayRate;
                        comRate.CRJCBRate = comRate.CRJCBRate != 0 ? comRate.CRJCBRate / 100 : comRate.CRJCBRate;

                        if (businessTypesAllowRebate.Contains(comRate.BusinessTypeID.ToString()))
                        {
                            comRate.CRDebitRebateForPurchaseWithCashOut = debitRebate.CRDebitRebateForPurchaseWithCashOut;
                            comRate.CRDebitRebateForCashAdvance = debitRebate.CRDebitRebateForCashAdvance;
                        }

                        if (comRate.QantasRateType == "Percentage")
                        {
                            comRate.CRQantasRatePercentage = comRate.CRQantasRate != 0
                                ? comRate.CRQantasRate / 100
                                : comRate.CRQantasRate;
                        }

                        comRateDA.InsertAllCommssionRateAndTranFee(comRate, memberModel.Member_key, UpdatedUser, comRate.DebitRateType,
                            comRate.StartDate, eCommerceVehicleTypeKeys, ref cardSurchargeInfos, comRate.QantasRateType, memberModel.IsFeeFreeMember);

                        if (forcedUpdateEcomMSF)
                        {
                            var mid = mServer.MemberId;
                            context.Database.ExecuteSqlCommand($"EXEC usp_ConfigSurcharge @memberID = '{mid}', @enableSurcharge = 1, @enableCardTypes = '' ");
                        }
                    }
                    if (!needToUpdateCommissionRates.Any(r => eCommerceVehicleTypeKeys.Contains(r.SubBusinessTypeID.ToString())))
                    {
                        cardSurchargeInfos = new List<CardSurchargeInfo>();
                    }


                    mServer.AverageTurnover = memberModel.AverageTurnover;
                    mServer.AverageTransaction = memberModel.AverageTransaction;

                    if (memberModel.PhotoID != null)
                    {
                        mServer.PhotoID = memberModel.PhotoID;
                    }

                    if (memberModel.PhotoIDExpiry != null)
                    {
                        if (memberModel.PhotoIDExpiry.Value.Year > 1800)
                            mServer.PhotoIDExpiry = memberModel.PhotoIDExpiry.Value;
                    }

                    if (memberModel.DOB != null)
                    {
                        if (memberModel.DOB.Value.Year > 1800)
                            mServer.DOB = memberModel.DOB.Value;
                    }

                    mServer.Modified_dttm = DateTime.Now;
                    string UserName = memberModel.ModifiedByUser;
                    if (memberModel.ModifiedByUser.Length > 20)
                    {
                        UserName = UserName.Substring(0, 18);
                    }

                    mServer.ModifiedByUser = UserName;

                    if (memberModel.IsOperator)
                    {
                        OperatorDA oda = new OperatorDA();
                        OperatorModel oml = new OperatorModel();
                        oml = oda.Get(memberModel.Member_key);
                        // mServer.Operator = new Operator();
                        if (oml != null)
                        {
                            if (memberModel.PaymentTypeId != 3)
                            {
                                oml.AllowCash = !memberModel.IsBankCustomer;

                                //If merchant payment type change from bank to cash or cash to bank
                                if (oml.PayByEFT != memberModel.IsBankCustomer)
                                {
                                    // forced increase terminal version
                                    needToIncreaseReceiptVersion = true;

                                    //if payment type is cashing
                                    if (memberModel.IsBankCustomer == false)
                                    {
                                        //force update print settlement is true.
                                        needToUpdatePrintSettlement = true;
                                    }
                                }
                                
                                oml.PayByEFT = memberModel.IsBankCustomer;
                            }
                            else
                            {
                                oml.AllowCash = false;
                                oml.PayByEFT = false;
                                // forced increase terminal version
                                needToIncreaseReceiptVersion = true;
                                //force update print settlement is true.
                                needToUpdatePrintSettlement = false;
                            }
                            oml.PaymentType = memberModel.PaymentTypeId;
                            if (memberModel.DealerKey > 0)
                            {
                                oml.Dealer_key = memberModel.DealerKey;
                            }

                            oda.Update(oml);
                        }
                    }
                    if (!string.IsNullOrEmpty(memberModel.QBRNumber))
                    {
                        var memberReference =
                            context.MemberReferences.FirstOrDefault(where => where.member_key == memberModel.Member_key);
                        if (memberReference != null && !string.IsNullOrEmpty(memberReference.MemberReferenceNumber))
                        {
                            if (memberReference.MemberReferenceNumber != memberModel.QBRNumber)
                            {
                                memberReference.member_key = memberModel.Member_key;
                                memberReference.CreatedByUser = memberModel.ModifiedByUser;
                                memberReference.CreatedDate = DateTime.Now;
                                memberReference.MemberReferenceNumber = memberModel.QBRNumber;
                                memberReference.MemberReferenceSourceKey = 1;
                            }
                        }
                        else
                        {
                            context.MemberReferences.Add(new MemberReference()
                            {
                                member_key = memberModel.Member_key,
                                CreatedByUser = memberModel.ModifiedByUser,
                                CreatedDate = DateTime.Now,
                                MemberReferenceNumber = memberModel.QBRNumber,
                                MemberReferenceSourceKey = 1,
                            });
                        }
                    }

                    SaveMerchantWebsite(context, memberModel.MerchantWebsite, memberKey);
                    UpdateAmexMerchantID(context, memberModel.Member_key, memberModel.AmexMerchantID);
                    SaveTransactionListFlag(context, memberModel.EnableTransactionListOnEOMReport, memberKey);

                    var companyKeysAllowOnlyOneSubBusToAllTerminals =
                        Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                            .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

                    if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(mServer.Company_key.ToString()))
                    {
                        UpdateBusinessTypeKeyForAllTerminals(context, memberModel.Member_key, memberModel.SubBusinessTypeKey);
                        if (forceUpdateAllTerminalsBelongToOneVehicleType)
                        {
                            //Update all vehicle belong to this merchant to new sub-business type key
                            TerminalAllocationDA tda = new TerminalAllocationDA();
                            var allVehicleTerminalAllocation =
                                tda.GetAllVehicleTerminalAllocationList(memberKey).Where(n => n.IsAllocated).ToList();

                            if (allVehicleTerminalAllocation.Any())
                            {
                                var vehicles = allVehicleTerminalAllocation.Select(n => n.Vehicle_key).ToList();
                                var vehiclesEnt = context.Vehicles.Where(n => vehicles.Contains(n.Vehicle_key));
                                foreach (var vehicle in vehiclesEnt)
                                {
                                    vehicle.VehicleType_key = memberModel.SubBusinessTypeKey;
                                    vehicle.Modified_dttm = DateTime.Now;
                                    vehicle.ModifiedByUser = UpdatedUser;
                                }
                            }
                        }
                    }

                    history += UpdateEnableSplitPaymentReport(context, memberKey, memberModel.EnableSplitPaymentReport, UpdatedUser, mServer.Company_key);
                    if (!string.IsNullOrEmpty(history))
                    {
                        historyDA.Add(memberKey, history, UpdatedUser);
                    }
                    context.SaveChanges();
                }

                if (needToIncreaseReceiptVersion)
                {
                    UpdateVersionForAllTerminalAllocated(mServer.Member_key, needToUpdatePrintSettlement);
                }
            }
        }

        private void UpdateBusinessTypeKeyForAllTerminals(TaxiEpayEntities context, int member_key, int businessTypeKeyForAllTerminals)
        {
            context.Database.ExecuteSqlCommand(
                $"Update Member set BusinessTypeKeyForAllTerminals = {businessTypeKeyForAllTerminals} where Member_key = {member_key}");
        }

        private void UpdateAmexMerchantID(TaxiEpayEntities context, int member_key, string amexMerchantID)
        {
            context.Database.ExecuteSqlCommand(
                $"Update Member set AmexMerchantID = '{amexMerchantID}' where Member_key = {member_key}");
        }

        public void UpdateMemberWelcome(int member_key, string username)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var member = context.Members.FirstOrDefault(n => n.Member_key == member_key);

                if (member != null)
                {
                    member.Welcomed = true;
                    member.Modified_dttm = DateTime.Now;
                    member.ModifiedByUser = username;
                    context.SaveChanges();
                }
            }
        }

        public void SaveMerchantWebsite(TaxiEpayEntities context, string merchantWebsiteURL, int memberkey)
        {
            var currentMemberWebsite = GetMerchantWebsiteURL(context, memberkey);
            if (currentMemberWebsite == merchantWebsiteURL)
            {
                return;
            }

            context.Database.ExecuteSqlCommand(
                $"UPDATE Member SET MerchantWebsite = '{merchantWebsiteURL}' WHERE Member_Key = {memberkey}");
        }

        public void SaveTransactionListFlag(TaxiEpayEntities context, bool EnableTransactionList, int memberkey)
        {
            var currentEnableTransactionList = GetEnableTransactionListOnEOMStatement(context, memberkey);
            if (currentEnableTransactionList == EnableTransactionList)
            {
                return;
            }

            context.Database.ExecuteSqlCommand(
                $"UPDATE Member SET EnableTransactionListOnMonthlyReport = {(EnableTransactionList ? 1 : 0)} WHERE Member_Key = {memberkey}");
        }

        public void ChangeMemberDetails(MemberChangeDetailModel mod)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.Members
                           where m.Member_key == mod.Member_key
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    if (mod.Email != null)
                    {
                        mem.Email = mod.Email;
                    }

                    if (mod.Fax != null)
                    {
                        mem.Fax = mod.Fax;
                    }

                    if (mod.Mobile != null)
                    {
                        mem.Mobile = mod.Mobile;
                    }

                    if (mod.Telephone != null)
                    {
                        mem.Telephone = mod.Telephone;
                    }

                    context.SaveChanges();
                }
            }
        }

        public List<string> GetMemberByTradingName(string Name)
        {
            List<string> modelList = new List<string>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = (from m in context.Members
                            where m.TradingName.StartsWith(Name) || m.FirstName.StartsWith(Name)
                            select m).Take(10);
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        string model = string.Empty;
                        model = n.MemberId + "," + n.TradingName;
                        modelList.Add(model);
                    }
                }
            }

            return modelList;
        }

        public IList<Member> GetMembersSameBusinessByMemberId(string memberId, List<int> availableBusinessTypes,
            int currentMemberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                IList<Member> members = context.Members
                    .Where(m => m.MemberId.Contains(memberId) &&
                                availableBusinessTypes.Contains(m.Company_key) &&
                                m.Member_key != currentMemberKey)
                    .Select(m => m)
                    .ToList();
                return members;
            }
        }

        public List<MemberModelSimple> GetSimpleMembersByMemberId(string memberId, List<int> availableBusinessTypes,
            int currentMemberKey)
        {
            List<MemberModelSimple> result = new List<MemberModelSimple>();
            List<Member> members =
                GetMembersSameBusinessByMemberId(memberId, availableBusinessTypes, currentMemberKey).ToList();
            if (members.Count > 0)
            {
                foreach (Member mem in members)
                {
                    MemberModelSimple memberModel = new MemberModelSimple();
                    memberModel = GetMemeberSimple(mem.Member_key);
                    result.Add(memberModel);
                }
            }

            return result;
        }

        public int AddNew(MemberModel t, string UpdatedUser)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                int id;
                //add Member record
                using (var contexttransaction = context.Database.BeginTransaction())
                {
                    Member mServer = new Member();
                    Random rnd = new Random();
                    int ran = rnd.Next(1, 6000);
                    string sub = "TOG";
                    string memberId = sub + ran.ToString();

                    mServer.MemberId = memberId; //temporary set memberid
                    mServer.FirstName = t.FirstName;
                    mServer.LastName = t.LastName;
                    mServer.Telephone = t.Telephone;
                    mServer.Fax = t.Fax;
                    mServer.Mobile = t.Mobile;
                    mServer.TradingName = t.TradingName;
                    mServer.ABN = t.ABN;
                    mServer.ACN = t.ACN;
                    mServer.MCC = t.MCC;
                    mServer.DriverLicenseNumber = t.DriverLicenseNumber;
                    mServer.Email = t.Email;
                    mServer.SaleforceID = t.SalesforceId;
                    mServer.MerchantID = t.MerchantID;
                    //start change for bug 228 by Vu Nguyen. Enable Receive payment report, weekly report and monthly report by default
                    mServer.ReceiveMonthlyActivityReport = t.ReceivePaymentReport;
                    mServer.ReceiveWeeklyActivityReport = t.ReceiveWeeklyActivityReport;
                    mServer.ReceivePaymentReport = t.ReceivePaymentReport;
                    mServer.AverageTransaction = t.AverageTransaction;
                    mServer.AverageTurnover = t.AverageTurnover;
                    mServer.EOMMSFPayment = t.EOMMSFPayment;
                    mServer.ExtendedSettlement = t.ExtendedSettlement;
                    mServer.EnableSplitPaymentReport = t.EnableSplitPaymentReport;
                    mServer.IndirectAmexSettlement = t.IndirectAmexSettlement;
                    mServer.IsFeeFree = t.IsFeeFreeMember;

                    if (!String.IsNullOrEmpty(t.BankAccountDtoAsJson))
                    {
                        AccountDA accountDa = new AccountDA();
                        var accountList =
                            JsonConvert.DeserializeObject<List<BankAccountDTO>>(t.BankAccountDtoAsJson);
                        accountDa.UpdateBankAccountToMember(context, accountList, mServer, UpdatedUser);
                    }

                    //End change for bug 228 by Vu Nguyen
                    //if (t.PhotoID != null)
                    //{
                    //    mServer.PhotoID = t.PhotoID;
                    //}
                    //if (t.PhotoIDExpiry != null)
                    //{
                    //    if (t.PhotoIDExpiry.Value.Year > 1800)
                    //        mServer.PhotoIDExpiry = t.PhotoIDExpiry.Value;
                    //}
                    mServer.Active = true; // t.Active; 
                    if (t.DOB != null)
                    {
                        if (t.DOB.Value.Year > 1800)
                            mServer.DOB = t.DOB.Value;
                    }

                    //  mServer.Welcomed = t.Welcomed;
                    mServer.Modified_dttm = DateTime.Now;
                    string UserName = t.ModifiedByUser;
                    if (t.ModifiedByUser.Length > 20)
                    {
                        UserName = UserName.Substring(0, 18);
                    }

                    mServer.ModifiedByUser = UserName;
                    mServer.CreatedDate = DateTime.Now;
                    mServer.Company_key = t.CompanyKey;
                    context.Members.Add(mServer);
                    context.SaveChanges();

                    //set memberid = member_key;
                    //context.Members.Where(m => m.Member_key == mServer.Member_key).FirstOrDefault().MemberId = mServer.Member_key.ToString(); 
                    mServer.MemberId = mServer.Member_key.ToString();
                    id = mServer.Member_key;
                    var companyKeysAllowOnlyOneSubBusToAllTerminals =
                        Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                            .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

                    // If in case we only allow only one sub-business type for all terminals
                    if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(mServer.Company_key.ToString()))
                    {
                        UpdateBusinessTypeKeyForAllTerminals(context, id, t.SubBusinessTypeKey);
                    }

                    context.SaveChanges();
                    
                    
                    if (!string.IsNullOrEmpty(t.QBRNumber))
                    {
                        context.MemberReferences.Add(new MemberReference()
                        {
                            member_key = id,
                            CreatedByUser = UserName,
                            CreatedDate = DateTime.Now,
                            MemberReferenceNumber = t.QBRNumber,
                            MemberReferenceSourceKey = 1,
                        });
                        
                        context.SaveChanges();
                    }
                    
                    SaveMerchantWebsite(context, t.MerchantWebsite, id);
                    UpdateAmexMerchantID(context, id, t.AmexMerchantID);
                    SaveTransactionListFlag(context, t.EnableTransactionListOnEOMReport, id);
                    contexttransaction.Commit();

                    if (mServer.Member_key != null && mServer.Member_key > 0)
                    {
                        t.Member_key = mServer.Member_key;
                        if (t.PhotoID != null)
                        {
                            PhotoIDModel pModel = new PhotoIDModel();
                            pModel.member_key = mServer.Member_key;
                            pModel.PhotoID = t.PhotoID;
                            if (t.PhotoIDExpiry != null && t.PhotoIDExpiry.Value.Year > 1800)
                            {
                                pModel.PhotoIDExpiry = t.PhotoIDExpiry.Value;
                            }

                            pModel.UpdatedByUser = t.ModifiedByUser;
                            pModel.UpdatedDate = DateTime.Now;
                            MemberPhotoIDDA mpda = new MemberPhotoIDDA();
                            mpda.AddPhotoID(context, pModel);

                        }
                    }
                }

                var businessTypesAllowRebate = Caching.GetSystemConfigurationByKey("BusinessKeysAllowRebate").Split(';').ToList();
                var debitRebate = string.IsNullOrEmpty(t.RebatesDtoAsJson) ? null : JsonConvert.DeserializeObject<List<MemberRebateModel>>(t.RebatesDtoAsJson).FirstOrDefault();
                var bUpdateRebateForAllRate = debitRebate?.IsEdited ?? false;

                var commissionRates = new List<MemberCommissionRateModel>();
                

                if (!string.IsNullOrEmpty(t.CommissionRatesDtoAsJson))
                {
                    commissionRates = JsonConvert.DeserializeObject<List<MemberCommissionRateModel>>(t.CommissionRatesDtoAsJson);
                }
                var eCommerceVehicleTypeKeys = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.eCommerceVehicleTypeKey.ToString()).Split(',');
                var needToUpdateCommissionRates = bUpdateRebateForAllRate ? commissionRates : commissionRates.Where(c => c.IsEdited == true).ToList();

                foreach (var comRate in needToUpdateCommissionRates)
                {
                    if (comRate.DebitRateType == "Percentage")
                    {
                        comRate.CRDebitRatePercentage =
                            comRate.CRDebitRate != 0 ? comRate.CRDebitRate / 100 : comRate.CRDebitRate;
                    }

                    comRate.CRMasterCardRate = comRate.CRMasterCardRate != 0
                        ? comRate.CRMasterCardRate / 100
                        : comRate.CRMasterCardRate;
                    comRate.CRMasterCardDebitRate = comRate.CRMasterCardDebitRate != 0
                        ? comRate.CRMasterCardDebitRate / 100
                        : comRate.CRMasterCardDebitRate;
                    comRate.CRVisaRate = comRate.CRVisaRate != 0 ? comRate.CRVisaRate / 100 : comRate.CRVisaRate;
                    comRate.CRVisaDebitRate = comRate.CRVisaDebitRate != 0 ? comRate.CRVisaDebitRate / 100 : comRate.CRVisaDebitRate;
                    comRate.CRAmexRate = comRate.CRAmexRate != 0 ? comRate.CRAmexRate / 100 : comRate.CRAmexRate;
                    comRate.CRDinersRate = comRate.CRDinersRate != 0 ? comRate.CRDinersRate / 100 : comRate.CRDinersRate;
                    comRate.CRUnionPayRate = comRate.CRUnionPayRate != 0
                        ? comRate.CRUnionPayRate / 100
                        : comRate.CRUnionPayRate;
                    comRate.CRZipPayRate = comRate.CRZipPayRate != 0 ? comRate.CRZipPayRate / 100 : comRate.CRZipPayRate;
                    comRate.CRAlipayRate = comRate.CRAlipayRate != 0 ? comRate.CRAlipayRate / 100 : comRate.CRAlipayRate;
                    comRate.CRJCBRate = comRate.CRJCBRate != 0 ? comRate.CRJCBRate / 100 : comRate.CRJCBRate;

                    if (businessTypesAllowRebate.Contains(comRate.BusinessTypeID.ToString()))
                    {
                        comRate.CRDebitRebateForPurchaseWithCashOut = debitRebate?.CRDebitRebateForPurchaseWithCashOut;
                        comRate.CRDebitRebateForCashAdvance = debitRebate?.CRDebitRebateForCashAdvance;
                    }

                    if (comRate.QantasRateType == "Percentage")
                    {
                        comRate.CRQantasRatePercentage =
                            comRate.CRQantasRate != 0 ? comRate.CRQantasRate / 100 : comRate.CRQantasRate;
                    }
                    //Insert all MSF rate to database.
                    var comRateDA = new CommissionRateDA();
                    List<CardSurchargeInfo> infos = new List<CardSurchargeInfo>();
                    comRateDA.InsertAllCommssionRateAndTranFee(comRate, t.Member_key, UpdatedUser, comRate.DebitRateType,
                        comRate.StartDate, eCommerceVehicleTypeKeys, ref infos, comRate.QantasRateType, t.IsFeeFreeMember);
                }

                //Add MemberPhoto ID into separate table
                OperatorDA oda = new OperatorDA();
                OperatorModel oml = new OperatorModel();
                if (t.PaymentTypeId != 3)
                {
                    oml.AllowCash = !t.IsBankCustomer;
                    oml.PayByEFT = t.IsBankCustomer;
                }
                oml.PaymentType = t.PaymentTypeId;
                oml.Operator_key = id;

                if (t.DealerKey > 0)
                {
                    oml.Dealer_key = t.DealerKey;
                }

                oml.Modified_dttm = DateTime.Now;
                oda.Add(oml);

                return id;

            }
        }

        public int AddClone(MemberModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                //add Member record
                using (var contexttransaction = context.Database.BeginTransaction())
                {
                    Member mServer = new Member();
                    Random rnd = new Random();
                    int ran = rnd.Next(1, 6000);
                    string sub = "TOG";
                    string memberId = sub + ran.ToString();

                    mServer.MemberId = memberId; //temporary set memberid
                    mServer.FirstName = t.FirstName;
                    mServer.LastName = t.LastName;
                    mServer.Telephone = t.Telephone;
                    mServer.Fax = t.Fax;
                    mServer.Mobile = t.Mobile;
                    mServer.TradingName = t.TradingName;
                    mServer.ABN = t.ABN;
                    mServer.ACN = t.ACN;
                    mServer.Address = t.Address;
                    mServer.City = t.City;
                    mServer.State = t.State;
                    mServer.Postcode = t.Postcode;
                    mServer.DriverLicenseNumber = t.DriverLicenseNumber;
                    mServer.Email = t.Email;
                    mServer.MerchantID = t.MerchantID;
                    if (t.PhotoIDExpiry != null)
                        mServer.PhotoIDExpiry = t.PhotoIDExpiry.Value;
                    mServer.Active = true; // t.Active; 
                    if (t.DOB != null)
                        mServer.DOB = t.DOB.Value;
                    //  mServer.Welcomed = t.Welcomed;
                    mServer.Modified_dttm = DateTime.Now;
                    string UserName = t.ModifiedByUser;
                    if (t.ModifiedByUser.Length > 20)
                    {
                        UserName = UserName.Substring(0, 18);
                    }

                    mServer.ModifiedByUser = UserName;

                    mServer.Company_key = t.CompanyKey;
                    context.Members.Add(mServer);
                    context.SaveChanges();

                    //set memberid = member_key;
                    //context.Members.Where(m => m.Member_key == mServer.Member_key).FirstOrDefault().MemberId = mServer.Member_key.ToString(); 
                    mServer.MemberId = mServer.Member_key.ToString();
                    context.SaveChanges();
                    contexttransaction.Commit();
                    return mServer.Member_key;
                }
            }
        }

        public void UpdateDefaultPhotoID(TaxiEpayEntities entity, int memberKey, PhotoID photoid)
        {
            if (photoid != null && photoid.PhotoIDKey > 0)
            {
                int photoidKey = photoid.PhotoIDKey;
                Member mem = (from m in entity.Members
                              join p in entity.PhotoIDs on m.Member_key equals p.member_key
                              where m.Member_key == memberKey && p.PhotoIDKey == photoidKey
                              select m).FirstOrDefault();

                if (mem != null)
                {
                    //Update default photoIDKey in Member record
                    mem.PhotoIDKey = photoidKey;

                    //Update Current PhotoID in Member record
                    if (photoid.PhotoID1 != null)
                        mem.PhotoID = photoid.PhotoID1;
                    if (photoid.PhotoIDExpiry != null)
                        mem.PhotoIDExpiry = photoid.PhotoIDExpiry;


                    entity.SaveChanges();
                }
            }
        }

        public void UpdateDefaultProfilePicture(int memberKey, int memberprofilekey)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                Member mem = (from m in entity.Members
                              join p in entity.MemberProfilePictures on m.Member_key equals p.member_key
                              where m.Member_key == memberKey && p.MemberProfilePictureKey == memberprofilekey
                              select m).FirstOrDefault();

                if (mem != null)
                {
                    mem.MemberProfilePictureKey = memberprofilekey;
                    entity.SaveChanges();
                }
            }
        }

        public PagedResponseModel<MemberSearchResultVM> SearchMember(MemberSearchCriteriaVM model)
        {
            try
            {
                if (model.CreatedDateFrom != null)
                {
                    model.CreatedDateFrom = model.CreatedDateFrom.Substring(6) + model.CreatedDateFrom.Substring(3, 2) + model.CreatedDateFrom.Substring(0, 2);
                }

                if (model.CreatedDateTo != null)
                {
                    model.CreatedDateTo = model.CreatedDateTo.Substring(6) + model.CreatedDateTo.Substring(3, 2) + model.CreatedDateTo.Substring(0, 2);
                }
                string searchKeyword = model.IsSearchByKeyword ? model.SearchKeyword : "";
                model.IsUseAutoComplete = !model.IsSearchByKeyword ? false : model.IsUseAutoComplete;
                if (model.IsUseAutoComplete)
                {
                    if (searchKeyword.IndexOf(",") > 0)
                    {
                        string[] memberInformations = searchKeyword.Split(',');
                        searchKeyword = memberInformations[0];
                    }
                }

                using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
                {
                    taxiEpay.Database.CommandTimeout = 60;

                    List<SqlParameter> sqlParameters = new List<SqlParameter>();
                    sqlParameters.Add(new SqlParameter("@Keyword", searchKeyword ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@CompanyTypeId", model.CompanyKey ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@PaymentTypeId", model.PaymentKey ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@DealerId", model.DealerKey ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@QBRStatus", model.QBRStatusKey ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@CreatedFrom", model.CreatedDateFrom ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@CreatedTo", model.CreatedDateTo ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@IsActive", model.IsActive ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@IsFromAutoSuggest", model.IsUseAutoComplete));
                    sqlParameters.Add(new SqlParameter("@SortField", model.SortField ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@SortOrder", model.SortOrder ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                    sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));
                    var totalItemsParam = new SqlParameter
                    {
                        ParameterName = "@TotalItems",
                        Value = 0,
                        Direction = ParameterDirection.Output,
                    };
                    sqlParameters.Add(totalItemsParam);
                    var response = taxiEpay.Database.SqlQuery<MemberSearchResultVM>(
                        "EXEC SearchMemberWithCriteria "
                    + "  @Keyword"
                    + ", @CompanyTypeId"
                    + ", @PaymentTypeId"
                    + ", @DealerId"
                    + ", @QBRStatus"
                    + ", @CreatedFrom"
                    + ", @CreatedTo"
                    + ", @IsActive"
                    + ", @IsFromAutoSuggest"
                    + ", @SortField"
                    + ", @SortOrder"
                    + ", @PageSize"
                    + ", @PageIndex"
                    + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();
                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    return new PagedResponseModel<MemberSearchResultVM>
                    {
                        rows = response,
                        page = model.PageIndex,
                        total = (int)Math.Ceiling((float)total / model.PageSize),
                        records = total,
                    };
                }
            }
            catch (Exception e)
            {
                return new PagedResponseModel<MemberSearchResultVM>
                {
                    isError = true,
                };
            }

        }

        public List<string> SPMemberSearch(string Name, bool isActive)
        {
            List<string> modelList = new List<string>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                context.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@searchterm", Name ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@isActive", isActive));
                var members = context.Database.SqlQuery<MemberAutoSearchResult>(
                    "EXEC SPSearchMember "
                + "  @searchterm"
                + ", @isActive", sqlParameters.ToArray()).ToList();
                if (members.Count() > 0)
                {
                    members = members.GroupBy(m => m.MemberID).Select(g => g.First()).Take(10).ToList();
                    foreach (var mem in members)
                    {
                        StringBuilder value = new StringBuilder();
                        value = value.Append(mem.MemberID);
                        value = !string.IsNullOrEmpty(mem.TradingName) ? value.Append(", T/N: " + mem.TradingName) : value;
                        value = !string.IsNullOrEmpty(mem.FullName) ? value.Append(", " + mem.FullName) : value;
                        value = !string.IsNullOrEmpty(mem.QBRNumber) ? value.Append(", " + mem.QBRNumber) : value;
                        value = !string.IsNullOrEmpty(mem.AccountIdentifier) ? value.Append(", " + mem.AccountIdentifier) : value;
                        modelList.Add(value.ToString());
                    }
                }

                return modelList;
            }
        }

    }
}
