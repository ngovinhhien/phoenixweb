﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_BusLog.TransactionHostLog.Model;
using Phoenix_Service;
using Phoenix_Service.Extension;
using PhoenixObjects.Common;
using PhoenixObjects.WebAppLog;
using System.Data;

namespace Phoenix_BusLog.Data
{
    public class iLinkConfigDA
    {
        /// <summary>
        /// This method will return the current iLink Credentials.
        /// </summary>
        /// <returns>The current iLink Credential</returns>
        public iLinkCredential GetiLinkCredentials()
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.iLinkCredentials.Where(i => i.IsActive == true).OrderByDescending(i => i.iLinkExpiryDate).FirstOrDefault();
            }
        }
        /// <summary>
        /// This method will inactivate older iLink Credentials and create a new one.
        /// </summary>
        /// <returns>Update result, success or failed</returns>
        public object UpdateiLinkCredentials(iLinkCredential iLinkCredential)
        {
            using (var entities = new TaxiEpayEntities())
            {
                try
                {
                    var activeRecords = entities.iLinkCredentials.Where(i => i.IsActive == true);
                    foreach(iLinkCredential rec in activeRecords)
                    {
                        rec.IsActive = false;
                    }
                    if(iLinkCredential.iLinkFile == null)
                    {
                        var latestRecord = entities.iLinkCredentials.OrderByDescending(i => i.UpdateDate).FirstOrDefault();
                        iLinkCredential.iLinkFile = latestRecord.iLinkFile;
                        iLinkCredential.iLinkFilename = latestRecord.iLinkFilename;
                    }
                    entities.Entry(iLinkCredential).State = EntityState.Added;
                    entities.SaveChanges();
                    return new
                    {
                        IsSuccess = true,
                    };
                }
                catch (Exception e)
                {
                    return new
                    {
                        IsSuccess = false,
                        Message = "System Error"
                    };
                }
            }
        }

        /// <summary>
        /// This method will return the current Download Automation Config.
        /// </summary>
        /// <returns>The current Download Automation Config</returns>
        public object GetDownloadAutomationConfig()
        {
            using (var entities = new TaxiEpayEntities())
            {
                string[] keys = new string[] { "iLinkAllowedFileExtensions", "iLinkFilenameConvention", "iLinkAutomaticErrorReceivers", "iLinkAutomaticResultReceivers" };
                
                var configs = entities.SystemConfigurations.Where(e => keys.Contains(e.ConfigurationKey)).ToList();
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach(SystemConfiguration sysCon in configs)
                {
                    if (result.ContainsKey(sysCon.ConfigurationKey))
                    {
                        return new
                        {
                            Success = false,
                            Message = "The configuration key " + sysCon.ConfigurationKey + " is duplicated"
                        };
                    }
                    else
                    {
                        result.Add(sysCon.ConfigurationKey, sysCon.ConfigurationValue);
                    }
                }
                return new {
                    Success = true,
                    Message = "",
                    Data = result
                };
            }
        }

        /// <summary>
        /// This method will update the iLink Automation Config.
        /// </summary>
        /// <returns>Update result, success or failed</returns>
        public object UpdateDownloadAutomationConfig(Dictionary<string, string> downloadAutomationConfig, string updatedBy)
        {
            using (var entities = new TaxiEpayEntities())
            {
                try
                {
                    foreach (var item in downloadAutomationConfig)
                    {
                        var sysConfig = entities.SystemConfigurations.Where(s => s.ConfigurationKey == item.Key).FirstOrDefault();
                        if(sysConfig != null)
                        {
                            sysConfig.ConfigurationValue = item.Value;
                            sysConfig.UpdatedBy = updatedBy;
                            sysConfig.UpdatedDate = DateTime.Now;
                        }
                    }
                    entities.SaveChanges();
                    return new
                    {
                        IsSuccess = true,
                    };
                }
                catch (Exception e)
                {
                    return new
                    {
                        IsSuccess = false,
                        Message = "System Error"
                    };
                }
            }
        }
        /// <summary>
        /// This method will return the exclusive "Download iLink file job" log search result base on filter value and paging.
        /// </summary>
        /// <returns>A list of "Download iLink file job" log</returns>
        public void SearchiLinkLog(GridModel<SearchLogModel, List<iLink_SearchLog_Result>> model)
        {
            using (var entities = new WebAppLogEntities())
            {
                try
                {
                    List<SqlParameter> sqlParameters = new List<SqlParameter>();
                    if(model.Request.LoggedDateFrom == null)
                        sqlParameters.Add(new SqlParameter("@DateFrom", DBNull.Value));
                    else
                        sqlParameters.Add(new SqlParameter("@DateFrom", model.Request.LoggedDateFrom.ToString("yyyy-MM-dd") + " 00:00:00.000"));
                    if (model.Request.LoggedDateTo == null)
                        sqlParameters.Add(new SqlParameter("@DateTo", DBNull.Value));
                    else
                        sqlParameters.Add(new SqlParameter("@DateTo", model.Request.LoggedDateTo.ToString("yyyy-MM-dd") + " 23:59:59.000"));
                    if (model.Request.Message == null)
                        sqlParameters.Add(new SqlParameter("@Message", DBNull.Value));
                    else
                        sqlParameters.Add(new SqlParameter("@Message", model.Request.Message));
                    if (model.Request.Level == null)
                        sqlParameters.Add(new SqlParameter("@Level", DBNull.Value));
                    else
                        sqlParameters.Add(new SqlParameter("@Level", model.Request.Level));
                    sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                    sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));
                    var totalItemsParam = new SqlParameter
                    {
                        ParameterName = "@TotalItems",
                        Value = 0,
                        Direction = ParameterDirection.Output
                    };
                    sqlParameters.Add(totalItemsParam);
                    model.Response = entities.Database.SqlQuery<iLink_SearchLog_Result>("EXEC iLink_SearchLog @DateFrom, @DateTo, @Message, @Level, @PageSize, @PageIndex, @TotalItems OUT", sqlParameters.ToArray()).ToList();
                    if (totalItemsParam.Value != null)
                    {
                        model.TotalItems = Convert.ToInt32(totalItemsParam.Value);
                    }
                    model.IsError = false;
                } catch (Exception e)
                {
                    model.Message = e.Message;
                    model.IsError = true;
                }
            }
        }

        public List<iLinkFileStatu> GetiLinkFileStatuses()
        {
            using (var entities = new TaxiEpayEntities())
            {
                var statuses = entities.iLinkFileStatus.ToList();
                return statuses;
            }
        }

        public List<iLinkFileType> GetiLinkFileTypes()
        {
            using (var entities = new TaxiEpayEntities())
            {
                var types = entities.iLinkFileTypes.Where(t => t.IsActive == true).ToList();
                return types;
            }
        }
    }
}
