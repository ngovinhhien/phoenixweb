﻿using Phoenix_Service;
using PhoenixObjects.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class CustomerDA
    {
       public List<CustomerModel> GetCustomerByMemberkey(int Memberkey)
       {
           List<CustomerModel> CustomerList = new List<CustomerModel>(); ;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var tok = from t in context.Customers
                         where t.Member_key == Memberkey
                         select t;
               if (tok != null)
               {
                 
                   foreach(var t in tok)
                   {
                       CustomerModel c = new CustomerModel();
                       c = GetCustomer(t.CustomerKey);
                       CustomerList.Add(c);
                   }                 
               }
           }
           return CustomerList;
       }

       public CustomerModel GetCustomer(int Customerkey)
       {
           CustomerModel cus = null;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var tok = (from t in context.Customers
                          where t.CustomerKey == Customerkey
                          select t).FirstOrDefault();
               if (tok != null)
               {
                   cus = new CustomerModel();
                   cus.CustomerKey = tok.CustomerKey;
                   cus.TradingName = tok.TradingName;
                   cus.Member_key = tok.Member_key;
                   cus.FirstName = tok.FirstName;
                   cus.LastName = tok.LastName;
                   cus.Mobile = tok.Mobile;
                   cus.Telephone = tok.Telephone;
                   cus.PrimaryAddressKey = tok.PrimaryAddressKey;
                   cus.MailingAddressKey = tok.MailingAddressKey;
                   cus.DOB = tok.DOB;
                   cus.DefaultPaymentTokenKey = tok.DefaultPaymentTokenKey;
                   cus.Modified_dttm = tok.Modified_dttm;
                   cus.ModifiedByUser = tok.ModifiedByUser;

                   if(tok.PrimaryAddressKey !=null)
                   {
                       var addr = (from a in context.CustomerAddresses
                                   where a.CustomerAddressKey == tok.PrimaryAddressKey.Value
                                   select a).FirstOrDefault();
                       CustomerAddressModel adds = new CustomerAddressModel();
                       adds.CustomerAddressKey = addr.CustomerAddressKey;
                       adds.StreetNumber = addr.StreetNumber;
                       adds.StreetName = addr.StreetName;
                       adds.Modified_dttm = addr.Modified_dttm;
                       adds.ModifiedByUser = addr.ModifiedByUser;
                       adds.Suburb = addr.Suburb;
                       adds.Postcode = addr.Postcode;
                       adds.State = addr.StreetName;
                       cus.CustomerAddresses.Add(adds);
                   }
                   if (tok.MailingAddressKey != null)
                   {
                       var addr = (from a in context.CustomerAddresses
                                   where a.CustomerAddressKey == tok.MailingAddressKey.Value
                                   select a).FirstOrDefault();
                       CustomerAddressModel adds = new CustomerAddressModel();
                       adds.CustomerAddressKey = addr.CustomerAddressKey;
                       adds.StreetNumber = addr.StreetNumber;
                       adds.StreetName = addr.StreetName;
                       adds.Modified_dttm = addr.Modified_dttm;
                       adds.ModifiedByUser = addr.ModifiedByUser;
                       adds.Suburb = addr.Suburb;
                       adds.Postcode = addr.Postcode;
                       adds.State = addr.StreetName;
                       cus.CustomerAddresses.Add(adds);
                   }
                   if (tok.DefaultPaymentTokenKey != null)
                   {
                       var addr = (from a in context.CustomerTokens
                                   where a.CustomerTokenKey == tok.DefaultPaymentTokenKey.Value
                                   select a).FirstOrDefault();
                       CustomerTokenModel adds = new CustomerTokenModel();
                       adds.CustomerTokenKey = addr.CustomerTokenKey;
                       adds.CustomerKey = addr.CustomerKey;
                       adds.Token = addr.Token;
                       adds.DeactivateDate = addr.DeactivateDate;

                       var acc = (from a in context.CustomerAccounts
                                  where a.CustomerAccountKey == addr.CustomerTokenKey
                                  select a).FirstOrDefault();
                       if (acc != null)
                       {
                           CustomerAccountModel accm = new CustomerAccountModel();
                           accm.CustomerAccountKey = acc.CustomerAccountKey;
                           accm.AccountName = acc.AccountName;
                           accm.AccountNumber = acc.AccountNumber;
                           accm.BSB = acc.BSB;
                           accm.Modified_dttm = acc.Modified_dttm;
                           adds.CustomerAccount = accm;

                       }
                       else
                       {
                           var card = (from a in context.CustomerCards
                                       where a.CustomerCardKey == addr.CustomerTokenKey
                                       select a).FirstOrDefault();

                           CustomerCardModel cardm = new CustomerCardModel();
                           cardm.CustomerCardKey = card.CustomerCardKey;
                           cardm.CardMasked = card.CardMasked;
                           cardm.ExpiryMonth = card.ExpiryMonth;
                           cardm.ExpiryYear = card.ExpiryYear;
                           cardm.CardHolderName = card.CardHolderName;
                           cardm.Modified_dttm = card.Modified_dttm;
                           cardm.ModifiedByUser = card.ModifiedByUser;
                           adds.CustomerCard = cardm;
                       }

                       cus.CustomerTokens.Add(adds);
                   }
               }
           }
           return cus;
       }
    }
}
