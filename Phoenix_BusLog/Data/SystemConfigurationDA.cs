﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix_BusLog.Data
{
    public class SystemConfigurationDA
    {
        public IEnumerable<SystemConfiguration> GetSystemConfigurations(int systemID)
        {
            
            using (var entities = new TaxiEpayEntities())
            {
                var systemConfigurations = entities.SystemConfigurations
                       .Where(n => n.SystemID == systemID).ToList().Distinct();

                return systemConfigurations;
            }
        }
    }
}
