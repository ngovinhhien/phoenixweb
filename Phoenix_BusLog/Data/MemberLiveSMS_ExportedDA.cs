﻿using Phoenix_Service;
using PhoenixObjects.LiveSMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
  public  class MemberLiveSMS_ExportedDA
    {
      public void Add(MemberLiveSMS_ExportedModel t)
      {
          using (TaxiEpayEntities context = new TaxiEpayEntities())
          {
              MemberLiveSMS_Exported aSever = new MemberLiveSMS_Exported();
              aSever.Member_key = t.Member_key;
              aSever.CreatedBy = t.CreatedBy;
              aSever.CreatedDate = t.CreatedDate;           

              context.MemberLiveSMS_Exported.Add(aSever);
              context.SaveChanges();
          }
      }
    }
}
