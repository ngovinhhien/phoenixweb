﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class CompanyDA
   {
        public CompanyModel Get(int company_key)
        {
            CompanyModel company = new CompanyModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var skk = (from si in context.Companies
                         where si.Company_key == company_key
                         select si).FirstOrDefault();
                if (skk != null)
                {

                    company.Company_key = skk.Company_key;
                    company.Name = skk.Name;
                    company.ServiceChargeRate = skk.ServiceChargeRate;
                    company.ABN = skk.ABN;
                    company.ACN = skk.ACN;
                    company.City = skk.City;
                    company.State = skk.State;
                    company.Postcode = skk.Postcode;
                    company.Modified_dttm = skk.Modified_dttm;
                    company.ModifiedByUser = skk.ModifiedByUser;
                    company.Company_key = skk.Company_key;
                    company.MSFApplicable = skk.MSFApplicable;
                }
            }
            return company;
        }

        public List<Company> GetCompanyAssociateWithOrganisationforLoyalty()
        {
            using (var taxiEpay = new TaxiEpayEntities())
            {
                return taxiEpay.Companies
                    .Include(n => n.LoyaltyPromotions)
                    .Where(n => n.LoyaltyPromotions.Any(x => x.OrganisationforLoyaltyID != null)).ToList();
            }
        }
   }
}
