﻿using Phoenix_Service;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class TerminalDA
    {
        public TerminalModel CheckTerminalExists(string termId)
        {
            TerminalModel term = null;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.EftTerminals
                            where m.TerminalId == termId
                            select m).FirstOrDefault();
                if (addr != null)
                {
                    term = new TerminalModel();
                    term.TerminalId = addr.TerminalId;
                    term.TerminalType = addr.TerminalType;
                    term.Active = addr.Active;
                    term.Status = addr.Status;
                    term.PPID = addr.PPID;
                    term.EftTerminal_key = addr.EftTerminal_key;
                    term.SIMSerialNumber = addr.SIMSerialNumber;
                    term.SIMMobileNumber = addr.SIMMobileNumber;
                    term.SoftwareVersion = addr.SoftwareVersion;
                    term.PUK = addr.PUK;
                    term.Configuration = addr.Configuration;
                    term.Company_key = addr.Company_key;
                    term.Modified_dttm = addr.Modified_dttm;
                    term.ModifiedByUser = addr.ModifiedByUser;
                }
            }
            return term;
        }


        public bool IsTerminalAllocatedToThisMember(string termId, int memberKey)
        {
            bool term = false;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var r = (from m in context.vTerminalAllocations
                         where m.TerminalId == termId && m.Member_key == memberKey && m.IsCurrent == true
                         select m).FirstOrDefault();
                if (r != null)
                {
                    term = true;
                }
            }
            return term;
        }

        public int GetDealersOperatorTerminalCount(int dealerkey)
        {
            int opt = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.Dealer_GetOperatorActiveTerminalCount(dealerkey).FirstOrDefault();                          

                if (addr != null)
                {
                    opt = addr.Value;
                }
            }
            return opt;
        }

        public string AnyOneTerminalAllocatedToThisMember(int memberKey)
        {
            string terminalid = string.Empty;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var r = (from m in context.vTerminalAllocations
                         where m.Member_key == memberKey && m.IsCurrent == true
                         select m).FirstOrDefault();
                if(r != null)
                {
                    terminalid = r.TerminalId;
                }
            }
            return terminalid;
        }

        public bool IsTerminalAllocated(string termId)
        {
            bool term = false;
           
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var r = (from m in context.vTerminalAllocations
                         where m.TerminalId == termId && m.Member_key != null && m.IsCurrent == true
                         select m).FirstOrDefault();
                if(r != null)
                {
                    term = true;
                }
            }
            return term;
        }

        public string TerminalBelongToMID(string termId)
        {
            string mid = string.Empty;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var r = (from m in context.vTerminalAllocations
                    where m.TerminalId == termId && m.Member_key != null && m.IsCurrent == true
                    select m).FirstOrDefault();
                if (r != null)
                {
                    mid = r.MemberID;
                }
            }
            return mid;
        }

        public int GetMemberkeyTerminalAllocatedTo(string termId)
        {
            int memberkey = 0;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var r = (from m in context.vTerminalAllocations
                         where m.TerminalId == termId && m.Member_key != null && m.IsCurrent == true
                         select m).FirstOrDefault();
                if (r != null)
                {
                    memberkey = r.Member_key.GetValueOrDefault(0);
                }
            }
            return memberkey;
        }

        public void UpdateTerminalStatus(int termkey, string status, string location)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = (from m in context.EftTerminals
                           where m.EftTerminal_key == termkey
                           select m).FirstOrDefault();
                if (addr != null)
                {
                    addr.Status = status;
                    addr.SoftwareVersion = location;
                }
                context.SaveChanges();
            }
        }

        public int GetTerminalKey(string terminalId)
        {
            int terminalKey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.EftTerminals
                           where m.TerminalId == terminalId
                           select m).FirstOrDefault();
                if (addr != null)
                {
                    terminalKey = addr.EftTerminal_key;
                }
            }

            return terminalKey;
        }

        public string GetTerminalId(int terminal_key)
        {
            string terminalId = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.EftTerminals
                            where m.EftTerminal_key == terminal_key
                            select m).FirstOrDefault();
                if (addr != null)
                {
                    terminalId = addr.TerminalId;
                }
            }

            return terminalId;
        }

        public List<string> GetEftTerminals(string termId)
        {
            List<string> term = new List<string>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = from m in context.vTerminalAllocations
                           where m.TerminalId.StartsWith(termId) && m.Member_key == null && m.IsCurrent == true
                           select m.TerminalId;
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        term.Add(n);
                    }
                }
            }

            return term.Take(5).ToList();
        }

        public List<string> EftTerminalsAutoComplate(string termId)
        {
            List<string> term = new List<string>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.SPSearchAvailableTerminals(termId);
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        term.Add(n);
                    }
                }
            }
            return term.Take(12).ToList();
        }
        public List<VirtualTerminalModel> GetVirtualTerminalByMemberkey(int Memberkey)
        {
            List<VirtualTerminalModel> term = new List<VirtualTerminalModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = context.Terminal_GetCurrentVirtualTIDByMemberkey(Memberkey).ToList();
                         
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        VirtualTerminalModel t = new VirtualTerminalModel();
                        t.Company_key = n.Company_key;
                        t.EffectiveDate = n.EffectiveDate;
                        t.EftTerminal_key = n.EftTerminal_key;
                        t.IsCurrent = n.IsCurrent;
                        t.MerchantID = n.MerchantID;
                        t.Modified_dttm = n.Modified_dttm;
                        t.Name = n.Name;
                        t.TerminalAllocation_key = n.TerminalAllocation_key;
                        t.TerminalId = n.TerminalId;
                        t.TerminalType = n.TerminalType;
                        t.Vehicle_key = n.Vehicle_key;                         
                        term.Add(t);
                    }
                }
            }

            return term;

        }

        public List<string> GetAssignedEftTerminals(string termId)
        {
            List<string> term = new List<string>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = from m in context.vTerminalAllocations
                           where m.TerminalId.StartsWith(termId) && m.Member_key != null && m.IsCurrent == true
                           select m.TerminalId;
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        term.Add(n);
                    }
                }
            }

            return term.Take(5).ToList();
        }
    }
}
