﻿using Phoenix_Service;
using PhoenixObjects.Financial;
using PhoenixObjects.Transaction;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Caching;
using Phoenix_BusLog.Model;
using PhoenixObjects.Enums;
using Phoenix_BusLog.Model;

namespace Phoenix_BusLog.Data
{
    public class TransactionDA : LiveDA
    {
        public TransactionDA() : base()
        {

        }
        public TransactionDA(int CommandTimeOut) : base(CommandTimeOut)
        {
        }

        public bool HasGetChargeBack(int member_key, bool IncludeInsurance)
        {
            bool term = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var r = (from m in context.WebCashing_GetChargeBacks(member_key, IncludeInsurance)
                         select m).ToList();

                if (r.Count() > 0)
                {
                    term = true;
                }
            }
            return term;
        }
        public List<WebCashing_GetChargeBackModel> GetChargeBack(int member_key, bool IncludeInsurance)
        {
            List<WebCashing_GetChargeBackModel> term = new List<WebCashing_GetChargeBackModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rr = from m in context.WebCashing_GetChargeBacks(member_key, IncludeInsurance)
                         select m;

                if (rr != null)
                {
                    foreach (var r in rr)
                    {
                        WebCashing_GetChargeBackModel te = new WebCashing_GetChargeBackModel();
                        te.Description = r.Description;
                        te.docket_key = r.docket_key;
                        te.TotalAmount = r.TotalAmount;
                        term.Add(te);
                    }
                }
            }
            return term;
        }
        public List<WebCashing_GetTerminalInsuranceModel> GetInsuranceChargeBack(int member_key)
        {
            List<WebCashing_GetTerminalInsuranceModel> term = new List<WebCashing_GetTerminalInsuranceModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rr = from m in context.WebCashing_GetTerminalInsurance(member_key)
                         select m;

                if (rr != null)
                {
                    foreach (var r in rr)
                    {
                        WebCashing_GetTerminalInsuranceModel te = new WebCashing_GetTerminalInsuranceModel();
                        te.Description = r.Description;
                        te.docket_key = r.docket_key;
                        te.TotalAmount = r.TotalAmount;
                        term.Add(te);
                    }
                }
            }
            return term;
        }

        public bool HasGetInsuranceChargeBack(int member_key)
        {
            bool term = false;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var r = (from m in context.WebCashing_GetTerminalInsurance(member_key)
                         select m).ToList();

                if (r.Count() > 0)
                {
                    term = true;
                }
            }
            return term;
        }
        public TransactionModel GetLastTransactionByMemberkey(int member_key)
        {
            TransactionModel trans = new TransactionModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var tranlist = from m in context.Transactions
                               where m.Member_key == member_key
                               select m;//.OrderByDescending(n => n.TransactionDate);//.FirstOrDefault();
                if (tranlist != null && tranlist.Count() > 0)
                {
                    Transaction rr = tranlist.OrderByDescending(n => n.TransactionDate).FirstOrDefault();
                    if (rr != null)
                    {
                        trans.TransactionDate = rr.TransactionDate;
                        trans.TotalAmount = rr.TotalAmount;
                        trans.LodgementReferenceNo = rr.LodgementReferenceNo;
                        trans.Status = rr.Status;
                        trans.Modified_dttm = rr.Modified_dttm;
                        trans.Account_key = rr.Account_key;
                        trans.Office_key = rr.Office_key;
                        trans.TransactionType_key = rr.TransactionType_key;
                        trans.PayRun_key = rr.PayRun_key;
                        trans.Member_key = rr.Member_key;
                        trans.Description = rr.Description;
                    }
                }
            }
            return trans;
        }

        public List<TransactionSearch_ResultModel> TransactionSearch(PrintDocketSearchModel model)
        {

            List<TransactionSearch_ResultModel> resultModel = new List<TransactionSearch_ResultModel>();
            List<TransactionSearchVM> r1 = new List<TransactionSearchVM>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                context.SetCommandTimeOut(200);

                if (model.StartDate.HasValue && model.StartDate.Value.Year < 1800)
                {
                    model.StartDate = null;
                }

                if (model.EndDate.HasValue && model.EndDate.Value.Year < 1800)
                {
                    model.EndDate = null;
                }


                var MemberID = string.IsNullOrEmpty(model.MemberID) ? "NULL" : $"'{model.MemberID}'";
                var TerminalID = string.IsNullOrEmpty(model.TerminalID) ? "NULL" : $"'{model.TerminalID}'";
                var TaxiID = string.IsNullOrEmpty(model.TaxiID) ? "NULL" : $"'{model.TaxiID}'";
                var BatchNumber = string.IsNullOrEmpty(model.BatchNumber) ? "NULL" : $"'{model.BatchNumber}'";
                var DocketFrom = model.DocketFrom == null ? "NULL" : $"{(float) model.DocketFrom}";
                var DocketTo = model.DocketTo == null ? "NULL" : $"{(float) model.DocketTo}";
                var CardType = string.IsNullOrEmpty(model.CardType) ? "'All'" : $"'{model.CardType}'";
                var CardMask = string.IsNullOrEmpty(model.CardMask) ? "NULL" : $"N'{model.CardMask}'";
                var ReceiptNo = string.IsNullOrEmpty(model.ReceiptNo) ? "NULL" : $"'{model.ReceiptNo}'";
                var AmexMID = string.IsNullOrEmpty(model.AmexMerchantID) ? "NULL" : $"'{model.AmexMerchantID}'";
                var WestpacMID = string.IsNullOrEmpty(model.WestPacMerchantID)
                    ? "NULL"
                    : $"'{model.WestPacMerchantID}'";
                var startDate = model.StartDate?.ToString("yyyy-MM-dd HH:mm:ss");
                var endDate = model.EndDate?.ToString("yyyy-MM-dd HH:mm:ss");
                var filedateStart = "NULL";
                if (model.FileDateFrom != null)
                {
                    filedateStart = $"'{model.FileDateFrom?.ToString("yyyy-MM-dd HH:mm:ss")}'";
                }

                var filedateEnd = "NULL";
                if (model.FileDateTo != null)
                {
                    filedateEnd = $"'{model.FileDateTo?.ToString("yyyy-MM-dd HH:mm:ss")}'";
                }

                r1 = context.Database.SqlQuery<TransactionSearchVM>(
                        $"[dbo].[_RPT_TransactionSearch] " +
                        $" @startdate = '{startDate}'," +
                        $"@enddate = '{endDate}'," +
                        $"@memberid = {MemberID}," +
                        $"@terminalid = {TerminalID}," +
                        $"@taxiid = {TaxiID}," +
                        $"@BatchNumber = {BatchNumber}," +
                        $"@Fare = NULL," +
                        $"@CardNumber = NULL," +
                        $"@InvRoc = NULL," +
                        $"@CardType = {CardType}," +
                        $"@ApprovedIncluded = '{model.ApproveStatusBool}'," +
                        $"@DeclinedIncluded = '{model.DeclineStatusBool}', " +
                        $"@FullCardNumber = {CardMask}, " +
                        $"@OnlyDisplayGlideboxTransaction = '{model.OnlyDisplayGlideboxTransaction}'," +
                        $"@ReceiptNo = {ReceiptNo}," +
                        $"@CompanyKey = {model.CompanyKey}," +
                        $"@AmexMID = {AmexMID}," +
                        $"@FileDateStart = {filedateStart}," +
                        $"@FileDateEnd = {filedateEnd}," +
                        $"@WestpacMID = {WestpacMID}," +
                        $"@TranTotalAmountFrom = {DocketFrom}," +
                        $"@TranTotalAmountTo = {DocketTo}," +
                        $"@LoadPaidDocketStatus = '{model.DocketPaidStatusChecked}'," +
                        $"@LoadPendingDocketStatus = '{model.DocketPendingStatusChecked}'," +
                        $"@LoadUnknownDocketStatus = '{model.DocketUnknownStatusChecked}'"
                    )
                    .ToList();


                r1.ForEach(r =>
                    resultModel.Add(new TransactionSearch_ResultModel
                    {
                        DocketStatus = r.DocketStatus,
                        paytype = r.paytype,
                        PayDate = r.PayDate.ToString(),
                        PaidTo = r.PaidTo,
                        MemberId = r.MemberId,
                        TradingName = r.TradingName,
                        TerminalID = r.TerminalID,
                        BatchNumber = r.BatchNumber,
                        Merchantid = r.Merchantid,
                        Taxi = r.Taxi,
                        DriverId = r.DriverId.ToString(),
                        StartShift = r.StartShift.ToString(),
                        EndShift = r.EndShift.ToString(),
                        Status = r.Status,
                        Authorisation = r.Authorisation.ToString(),
                        CardType = r.CardType,
                        MaskedCardNum = r.MaskedCardNum,
                        StartDate = r.StartDate.ToString(),
                        EndDate = r.EndDate.ToString(),
                        Fare = r.Fare.ToString(),
                        CashOut = r.CashOut.ToString(),
                        Extras = r.Extras.ToString(),
                        ServiceFee = r.ServiceFee.ToString(),
                        TotalAmount = r.TotalAmount.ToString(),
                        Pickup = r.Pickup,
                        DropOff = r.DropOff,
                        SourceFile = r.SourceFile,
                        OfficeName = r.OfficeName,
                        Filedate = r.Filedate,
                        ProcessedBy = r.ProcessedBy,
                        TransactionID = r.TransactionID.ToString(),
                        IsApproved = r.IsApproved.ToString(),
                        CardEntryMode = r.CardEntryMode,
                        GlideboxAmount = r.GlideboxAmount.GetValueOrDefault(0),
                        GlideboxCommission = r.GlideboxCommission.GetValueOrDefault(0),
                        BusinessTypeName = r.BusinessTypeName,
                        AmexMID = r.AmexMID,
                        TransactionDateTime = r.TransactionDateTime.ToString()
                    }));

                return resultModel;
            }
        }

        public List<TransactionDataObj> GetTransactionData(int Dealerkey)
        {
            List<TransactionDataObj> tObjList = new List<TransactionDataObj>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var data = context.MyWebDashboard_Dealer_Transactions(Dealerkey).ToList();
                if (data != null)
                {
                    foreach (var d in data)
                    {
                        TransactionDataObj tObj = new TransactionDataObj();
                        tObj.Month = d.Month;
                        tObj.MonthName = d.MonthName.Substring(0, 3);
                        tObj.year = d.year;
                        tObj.Amount = d.Amount;
                        tObjList.Add(tObj);
                    }
                }
            }
            return tObjList;
        }
        public List<TransactionModel> GetDealerTransactionPayments(int Dealerkey)
        {
            List<TransactionModel> tObjList = new List<TransactionModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var data = context.Dealer_GetDealerPayments(Dealerkey).ToList();
                if (data != null)
                {
                    foreach (var d in data)
                    {
                        TransactionModel tObj = new TransactionModel();
                        tObj.Account_key = d.Account_key;
                        tObj.Description = d.Description;
                        tObj.LodgementReferenceNo = d.LodgementReferenceNo;
                        tObj.Member_key = d.Member_key;
                        tObj.Modified_dttm = d.Modified_dttm;
                        tObj.ModifiedByUser = d.ModifiedByUser;
                        tObj.Office_key = d.Office_key;
                        tObj.PayRun_key = d.PayRun_key;
                        tObj.Status = d.Status;
                        tObj.TotalAmount = d.TotalAmount;
                        tObj.Transaction_key = d.Transaction_key;
                        tObj.TransactionDate = d.TransactionDate;
                        tObj.TransactionType_key = d.TransactionType_key;

                        tObjList.Add(tObj);
                    }
                }
            }
            return tObjList;
        }

        public List<TransactionModel> GetMemberPaymentHistory(int member_key, DateTime? StartPaymentDate, DateTime? EndPaymentDate, int toplastpayment = 30)
        {
            List<TransactionModel> tObjList = new List<TransactionModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                DateTime startdate = StartPaymentDate.GetValueOrDefault(DateTime.MinValue);
                DateTime enddate = StartPaymentDate.GetValueOrDefault(DateTime.MaxValue);
                var data = (from p in context.Transactions
                            join r in context.PayRuns on p.PayRun_key equals r.PayRun_key into payrungroup
                            from rg in payrungroup.DefaultIfEmpty()
                            where p.Member_key == member_key && p.TransactionDate >= startdate && p.TransactionDate <= enddate
                            select p).OrderByDescending(r => r.TransactionDate).Take(toplastpayment);

                if (data != null)
                {
                    foreach (var d in data)
                    {
                        TransactionModel tObj = new TransactionModel();
                        tObj.Account_key = d.Account_key;
                        tObj.Description = d.Description;
                        tObj.LodgementReferenceNo = d.LodgementReferenceNo;
                        tObj.Member_key = d.Member_key;
                        tObj.Modified_dttm = d.Modified_dttm;
                        tObj.ModifiedByUser = d.ModifiedByUser;
                        tObj.Office_key = d.Office_key;
                        tObj.PayRun_key = d.PayRun_key;
                        tObj.Status = d.Status;
                        tObj.TotalAmount = d.TotalAmount;
                        tObj.Transaction_key = d.Transaction_key;
                        tObj.TransactionDate = d.TransactionDate;
                        tObj.TransactionType_key = d.TransactionType_key;
                        if (d.PayRun != null)
                            tObj.PayrunID = d.PayRun.PayRunId;
                        tObjList.Add(tObj);
                    }
                }
            }
            return tObjList;
        }

        public TransactionModel GetMemberPaymentHistory(string LodgementReferenceNo)
        {
            TransactionModel tObj = new TransactionModel();
            if (!string.IsNullOrEmpty(LodgementReferenceNo))
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {

                    var data = (from p in context.Transactions
                                join r in context.PayRuns on p.PayRun_key equals r.PayRun_key into payrungroup
                                from rg in payrungroup.DefaultIfEmpty()
                                where p.LodgementReferenceNo == LodgementReferenceNo
                                select p).OrderByDescending(r => r.TransactionDate).FirstOrDefault();


                    if (data != null)
                    {

                        //foreach (var d in data)
                        //{

                        tObj.Account_key = data.Account_key;
                        tObj.Description = data.Description;
                        tObj.LodgementReferenceNo = data.LodgementReferenceNo;
                        tObj.Member_key = data.Member_key;
                        tObj.Modified_dttm = data.Modified_dttm;
                        tObj.ModifiedByUser = data.ModifiedByUser;
                        tObj.Office_key = data.Office_key;
                        tObj.PayRun_key = data.PayRun_key;
                        tObj.Status = data.Status;
                        tObj.TotalAmount = data.TotalAmount;
                        tObj.Transaction_key = data.Transaction_key;
                        tObj.TransactionDate = data.TransactionDate;
                        tObj.TransactionType_key = data.TransactionType_key;
                        if (data.PayRun != null)
                            tObj.PayrunID = data.PayRun.PayRunId;
                        //tObjList.Add(tObj);
                        //}
                    }
                }
            }
            return tObj;
        }

        public List<MemberMonthlyReportModel> GetPaymentPeriodHistory(int _member_key, int numberofmonth = 24)
        {
            List<MemberMonthlyReportModel> obj = new List<MemberMonthlyReportModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var data = context.Transactions.Where(r => r.Member_key == _member_key).
                    GroupBy(x => new { x.Member_key, x.TransactionDate.Value.Month, x.TransactionDate.Value.Year }).
                    Select(r => new { r.Key.Member_key, r.Key.Month, r.Key.Year }).OrderByDescending(o => new { o.Year, o.Month }).Take(numberofmonth);
                foreach (var r in data)
                {
                    MemberMonthlyReportModel m = new MemberMonthlyReportModel();
                    m.MemberKey = r.Member_key.GetValueOrDefault(_member_key);
                    m.Month = r.Month;
                    m.Year = r.Year;
                    obj.Add(m);
                }

            }
            return obj;
        }

        public List<MemberMonthlyReportModel> GetTransactionPeriodHistory(int _member_key, int numberofmonth = 24)
        {
            List<MemberMonthlyReportModel> obj = new List<MemberMonthlyReportModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var data = context.EftDockets.Where(r => r.Operator_key == _member_key).
                    GroupBy(x => new { x.Operator_key, x.TripStart.Value.Month, x.TripStart.Value.Year }).
                    Select(r => new { r.Key.Operator_key, r.Key.Month, r.Key.Year }).OrderByDescending(o => new { o.Year, o.Month }).Take(numberofmonth);
                foreach (var r in data)
                {
                    MemberMonthlyReportModel m = new MemberMonthlyReportModel();
                    m.MemberKey = r.Operator_key.GetValueOrDefault(_member_key);
                    m.Month = r.Month;
                    m.Year = r.Year;
                    obj.Add(m);
                }

            }
            return obj;
        }

        public List<MemberFYReportModel> GetFinancialYearReportHistory(int _member_key, int numberofyear = 9)
        {
            List<MemberFYReportModel> obj = new List<MemberFYReportModel>();
            for (int i = DateTime.Now.Year - numberofyear; i <= DateTime.Now.Year; i++)
            {
                MemberFYReportModel m = new MemberFYReportModel();
                m.MemberKey = _member_key;
                m.Year = i - 1;
                m.FinancialPeriod = m.Year + " - " + i;
                obj.Add(m);
            }
            return obj.OrderByDescending(n => n.Year).ToList();
        }

        public List<CadmusinCardType> GetCadmusinCardTypes()
        {
            using (var dbContext = new TaxiEpayEntities())
            {
                return dbContext.CadmusinCardTypes.Where(x => x.IsActive == true).ToList();
            }
        }

        public string GetPaymentTypeViaLodgementRefNo(string LodgementRefNo)
        {
            using (var dbContext = new TaxiEpayEntities())
            {
                var transaction = dbContext.Transactions.FirstOrDefault(x => x.LodgementReferenceNo == LodgementRefNo);
                if (transaction == null)
                {
                    return string.Empty;
                }

                var transactionTypeList = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.BankTransactionTypeList.ToString());
                var isBank = transactionTypeList.Split(';').Contains(transaction.TransactionType_key.ToString());
                if (isBank)
                {
                    return "BANK";
                }

                transactionTypeList = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.CashTransactionTypeList.ToString());
                var isCash = transactionTypeList.Split(';').Contains(transaction.TransactionType_key.ToString());
                if (isCash)
                {
                    return "CASH";
                }

                transactionTypeList = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.CardTransactionTypeList.ToString());
                var isCard = transactionTypeList.Split(';').Contains(transaction.TransactionType_key.ToString());
                if (isCard)
                {
                    return "CARD";
                }
            }
            return string.Empty;
        }
    }
}
