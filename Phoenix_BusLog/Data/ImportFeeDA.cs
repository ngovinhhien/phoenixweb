﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using Phoenix_Service;
using PhoenixObjects.Base;
using PhoenixObjects.Common;
using PhoenixObjects.ImportFee;

namespace Phoenix_BusLog.Data
{
    public class ImportFeeDA
    {
        private readonly ILog _logger = LogManager.GetLogger("ImportFeeDA");
        public bool AddFee(List<InterchangeSchemeFeeModel> lstObj, string orgFileName, int orgFileSize, string storedFileName, string UpdatedUser)
        {
            try
            {
                using (var context = new TaxiEpayEntities())
                {
                    using (var tran = context.Database.BeginTransaction())
                    {
                        var currentDate = DateTime.Now;
                        _logger.Info("Build FeeImportFile");
                        var feeImportedFile = new FeeImportedFile
                        {
                            OriginalFileName = orgFileName,
                            Filesize = orgFileSize,
                            StoredFileName = storedFileName,
                            TotalRecords = lstObj.Count,
                            CreatedBy = UpdatedUser,
                            CreatedDate = currentDate,
                            UpdatedBy = UpdatedUser,
                            UpdatedDate = currentDate
                        };
                        _logger.Info("Finished build FeeImportFile");
                        context.FeeImportedFiles.Add(feeImportedFile);

                        var feeImportedDetails = new List<FeeImportedDetail>();

                        _logger.Info("Build FeeImportedDetail");
                        lstObj.ForEach(o =>
                        {
                            var feeImportedDetail = new FeeImportedDetail
                            {
                                FeeImportedFile = feeImportedFile,
                                MemberID = o.MID,
                                MemberKey = o.MKey,
                                Month = Convert.ToInt32(o.Month),
                                Year = Convert.ToInt32(o.Year),
                                CardType = o.CardType,
                                FeeType = o.FeeType,
                                Description = o.Description,
                                NumberOfTxn = string.IsNullOrWhiteSpace(o.Txns_No) ? 0 : Convert.ToInt32(o.Txns_No),
                                Rate = Convert.ToDecimal(o.Rate),
                                FeeAmount = Convert.ToDecimal(o.FeeValue),
                                UpdatedBy = UpdatedUser,
                                UpdatedDate = currentDate
                            };

                            feeImportedDetails.Add(feeImportedDetail);
                        });
                        _logger.Info("Finished build FeeImportedDetail");

                        context.FeeImportedDetails.AddRange(feeImportedDetails);

                        context.SaveChanges();
                        _logger.Info($"Start execute usp_IS_CreateMemberCharge with FeeImportedFileID = {feeImportedFile.FeeImportedFileID}");
                        context.Database.ExecuteSqlCommand("EXEC [dbo].[usp_IS_CreateMemberCharge] @FeeImportedFileID ",
                            new SqlParameter("FeeImportedFileID", feeImportedFile.FeeImportedFileID));
                        _logger.Info($"End execute usp_IS_CreateMemberCharge with FeeImportedFileID = {feeImportedFile.FeeImportedFileID}");
                        tran.Commit();
                    }
                }
                
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                _logger.Info(Newtonsoft.Json.JsonConvert.SerializeObject(lstObj));
                return false;
            }
        }

        public PagedResponseModel<FeeImportedModel> GetFeesImportList(BasePagingModel model)
        {
            using (var context = new TaxiEpayEntities())
            {
                var sqlParameters = new List<SqlParameter>
                {
                    new SqlParameter("@SortField", model.SortField ?? string.Empty),
                    new SqlParameter("@SortOrder", model.SortOrder ?? string.Empty),
                    new SqlParameter("@PageSize", model.PageSize),
                    new SqlParameter("@PageIndex", model.PageIndex)
                };
                
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output,
                };
                
                sqlParameters.Add(totalItemsParam);
                
                var result = context.Database.SqlQuery<FeeImportedModel>("EXEC FeeImportedFileSearch "
                                                                         + "@SortField"
                                                                         + ", @SortOrder"
                                                                         + ", @PageSize"
                                                                         + ", @PageIndex"
                                                                         + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();
                
                int.TryParse(totalItemsParam.Value.ToString(), out var totalItems);
                
                return new PagedResponseModel<FeeImportedModel>
                {
                    rows = result,
                    page = model.PageIndex,
                    total = (int)Math.Ceiling((float)totalItems / model.PageSize),
                    records = totalItems,
                };
            }
        }
    }
}