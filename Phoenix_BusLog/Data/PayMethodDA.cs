﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class PayMethodDA
    {
       public int GetPayMethod_key(string payMethodName)
       {
           int PayMethod_key = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = (from si in context.PayMethods
                         where si.Name == payMethodName
                         select si.PayMethod_key).FirstOrDefault();

               PayMethod_key = sk;
           }
           return PayMethod_key;
       }
    }
}
