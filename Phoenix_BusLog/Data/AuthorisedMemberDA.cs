﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class AuthorisedMemberDA
    {
        public List<AuthorisedMemberModel> GetAllByMemberKey(int MemberKey)
        {
            List<AuthorisedMemberModel> model = new List<AuthorisedMemberModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var aSever = from a in context.AuthorisedMembers
                             join o in context.Members on a.Member_AuthorisedMember_key equals o.Member_key
                             where a.Member_key == MemberKey
                             select new {a.AuthorisedMember_key, a.Member_AuthorisedMember_key, a.Member_key,o.FirstName,o.LastName,o.TradingName,o.MemberId, o.PhotoID };

                if (aSever != null)
                {
                    foreach (var a in aSever)
                    {
                        AuthorisedMemberModel m = new AuthorisedMemberModel();
                        m.AuthorisedMember_key = a.AuthorisedMember_key;
                        m.Member_AuthorisedMember_key = a.Member_AuthorisedMember_key;
                        m.Member_key = a.Member_key;
                        m.FName = a.FirstName;
                        m.LastName =a.LastName;
                        m.MemberId = a.MemberId;
                        m.TradingName = a.TradingName;
                        m.HasPhoto = false;
                        if(a.PhotoID != null)
                        {
                            m.HasPhoto = true;
                        }
                        model.Add(m);
                    }
                }
                return model;
            }
        }


        public void AddAuthorizedMember(int MemberKey, int AuthorizedMemberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var aSever = (from a in context.AuthorisedMembers
                              where a.Member_key == MemberKey && a.Member_AuthorisedMember_key == AuthorizedMemberKey
                              select a).FirstOrDefault();

                if (aSever == null)
                {
                    AuthorisedMember am = new AuthorisedMember();
                    am.Member_AuthorisedMember_key = AuthorizedMemberKey;
                    am.Member_key = MemberKey;
                    context.AuthorisedMembers.Add(am);
                    context.SaveChanges();
                }

            }
        }

        public void RemoveAuthorizedMember(int MemberKey, int AuthorizedMemberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var aSever = (from a in context.AuthorisedMembers
                              where a.Member_key == MemberKey && a.Member_AuthorisedMember_key == AuthorizedMemberKey
                              select a).FirstOrDefault();

                if (aSever != null)
                {
                    context.AuthorisedMembers.Remove(aSever);
                    context.SaveChanges();
                }
               
            }
        }

    }
}
