﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public abstract class LiveDA
    {

        public LiveDA()
        {
            this._CommandTimeout = 60;
        }

        public LiveDA(int _CommandTimeOut)
        {
            this._CommandTimeout = _CommandTimeOut;
        }
        private int _CommandTimeout = 600;

        /// <summary>
        /// Time out in second when execute data query. Only applicable to the DA class that implement it
        /// </summary>
        public int CommandTimeOut
        {
            set { this._CommandTimeout = value; }
            get { return this._CommandTimeout; }
        }
    }
}
