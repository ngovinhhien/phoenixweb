﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class MemberRentalDA
    {
        public int AddNew(MemberRentalModel t)
        {

            List<MemberRentalModel> MemberCurrentMemberRentalList = GetMemberRentalList(t.Member_key);
            if (MemberCurrentMemberRentalList != null)
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var lastDayOfLastMonth = firstDayOfMonth.AddDays(-1);
                foreach (var mCurrent in MemberCurrentMemberRentalList)
                {
                    UpdateEndDate(mCurrent.MemberRental_key, lastDayOfLastMonth, t.ModifiedByUser);
                }
            }
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                MemberRental mServer = new MemberRental();
                mServer.Member_key = t.Member_key;
                mServer.RentFree = t.RentFree;
                mServer.StartDate = t.StartDate;
                mServer.EndDate = t.EndDate;
                mServer.ModifiedByUser = t.ModifiedByUser;
                mServer.Modified_dttm = t.Modified_dttm;
                mServer.RentalAmount = t.RentalAmount;

                context.MemberRentals.Add(mServer);
                context.SaveChanges();
                return mServer.MemberRental_key;
            }
        }

        public void UpdateEndDate(int MemberRental_key, DateTime EndDate, string username)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MemberRentals
                           where m.MemberRental_key == MemberRental_key
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    mem.EndDate = EndDate;
                    mem.ModifiedByUser = username;
                    mem.Modified_dttm = DateTime.Now;
                }
                context.SaveChanges();
            }
        }

        public void Edit(MemberRentalModel m1)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.MemberRentals
                           where m.MemberRental_key == m1.MemberRental_key
                           select m).FirstOrDefault();

                if (addr != null)
                {
                    addr.EndDate = m1.EndDate;
                    addr.Modified_dttm = m1.Modified_dttm;
                    addr.ModifiedByUser = m1.ModifiedByUser;
                    context.SaveChanges();
                  
                }
            }
        }

        public MemberRentalModel GetMemberRental(int id)
        {
            MemberRentalModel memberRental = new MemberRentalModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.MemberRentals
                           where m.Member_key == id
                           select m;

                if (addr != null)
                {

                    MemberDA mda = new MemberDA();
                    memberRental.Member = mda.GetMemeber(id);
                }
            }
            return memberRental;
        
        }
        public MemberRentalModel GetMemberRentalByPK(int MemberRental_key)
        {
            MemberRentalModel memberRental = new MemberRentalModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.MemberRentals
                           where m.MemberRental_key == MemberRental_key
                           select m).FirstOrDefault();

                if (addr != null)
                {
                    memberRental.MemberRental_key = addr.MemberRental_key;
                    MemberDA mda = new MemberDA();
                    memberRental.Member = mda.GetMemeber(addr.Member_key);
                }
            }
            return memberRental;

        }

        public List<MemberRentalModel> GetMemberRentalList(int id)
        {
            List<MemberRentalModel> MemberRentalList = new List<MemberRentalModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.MemberRentals
                           where m.Member_key == id && m.RentFree ==true && m.StartDate < DateTime.Now && (m.EndDate > DateTime.Now || m.EndDate == null)
                           select m;
                if (addr != null)
                {
                    foreach (var t in addr)
                    {
                        MemberRentalModel mServer = new MemberRentalModel();
                        mServer.MemberRental_key = t.MemberRental_key;
                        mServer.Member_key = t.Member_key;
                        mServer.RentFree = t.RentFree;
                        mServer.StartDate = t.StartDate;
                        mServer.EndDate = t.EndDate;
                        // mServer.IndividualTerminalCalculation = t.Fax;
                        // mServer.TransactionTargetPerTerminal = t.Mobile;
                        mServer.ModifiedByUser = t.ModifiedByUser;
                        mServer.Modified_dttm = t.Modified_dttm;
                        mServer.RentalAmount = t.RentalAmount;
                        mServer.RentalTurnoverRangeKey = t.RentalTurnoverRangeKey;
                        MemberRentalList.Add(mServer);
                    }
                }
            }
            return MemberRentalList;
        }
    }

}
