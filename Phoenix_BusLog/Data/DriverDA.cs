﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class DriverDA
    {
       public string GetDriverCertificateByMemberkey(int Memberkey)
       {
           string DriverCert = string.Empty;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var t = (from m in context.Drivers
                        where m.Driver_key == Memberkey
                        select m).FirstOrDefault();

               if (t != null)
               {
                   DriverCert = t.DriverCertificateId;
               }
           }
           return DriverCert;
       }

        public Driver GetDriverByMemberKey(int MemberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var driver = (from d in context.Drivers
                         where d.Driver_key == MemberKey
                         select d).FirstOrDefault();
                return driver;
            }
        }

       public void Add(DriverModel driverModel)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               try
               {
                   Driver dr = new Driver();
                   dr.Driver_key = driverModel.Driver_key;
                   dr.DriverCertificateId = driverModel.DriverCertificateId;
                   dr.ModifiedByUser = driverModel.ModifiedByUser;
                   dr.Modified_dttm = driverModel.Modified_dttm;
                   context.Drivers.Add(dr);
                   context.SaveChanges();
               }
               catch(Exception e)
               {
                   throw new Exception(e.Message);
               }

           }
       }

        public void Update(DriverModel driverModel)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var driver = context.Drivers.FirstOrDefault(d => d.Driver_key == driverModel.Driver_key);
                if (driver != null)
                {
                    try
                    {
                        driver.DriverCertificateId = driverModel.DriverCertificateId;
                        driver.Modified_dttm = driverModel.Modified_dttm;
                        driver.ModifiedByUser = driverModel.ModifiedByUser;
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                    
                }
            }
        }
    }
}
