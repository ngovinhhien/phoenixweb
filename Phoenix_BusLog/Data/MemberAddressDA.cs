﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
namespace Phoenix_BusLog.Data
{
    public class MemberAddressDA
    {
        public List<MerchantAddress> GetMemberAddress(int memberId)
        {
            List<MerchantAddress> addressList = new List<MerchantAddress>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.Database.SqlQuery<MerchantAddress>($"EXEC dbo.usp_getMemberAddress @memberKey = {memberId}").ToList();
                if (addr != null)
                {
                    addressList.AddRange(addr.Select(a => new MerchantAddress() {State = a.State, Postcode = a.Postcode}));
                }
            }
            return addressList;
        }

        public string checkAddress(int id)
        {
            string atype = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Members
                            where m.PrimaryAddressKey == id
                            select m).FirstOrDefault();
                if (addr != null)
                {
                    atype = AddressTypeEnum.Primary.ToString();
                }
                else
                {
                    var addr1 = (from m in context.Members
                                where m.MailingAddressKey == id
                                select m).FirstOrDefault();
                    if (addr1 != null)
                    {
                        atype = AddressTypeEnum.Mailing.ToString();
                    }
                    else {

                        atype = "";
                    }
                }
            }
         
            return atype;
        }

        public bool CheckIfAddressExists(string State, string PostCode, string Suburb)
        {
            bool Exits = false;
            
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    StateDA st = new StateDA();
                    MemberAddress memberAddress = new MemberAddress();
                    int statekey = st.GetStateKey(State);
                    var postcodemasterkey = (from p in context.PostCodeMasters
                                             where p.State_key == statekey && p.PostCode == PostCode
                                             select p.PostCodeMaster_key).FirstOrDefault();
               
                    var subkey = (from s in context.Suburbs
                                  where s.PostCodeMaster_key == postcodemasterkey && s.Suburb1 == Suburb
                                  select s.Suburb_key).FirstOrDefault();
                    if (subkey != null && subkey !=0)
                    {
                        Exits = true;
                    }                   
                }
                return Exits;
            }

        public void Add(AddressListModel maddr, int MemberKey, bool? IsModify = false)
        {
            int addresskey = 0;
            try
            {
                bool exists = this.CheckIfAddressExists(maddr.State, maddr.PostCode, maddr.Suburb);
                if (exists == false)
                {
                    throw new MemberAddException(String.Format("The combination of {0} address State, PostCode and Suburb does not exist", maddr.AddressType));
                }


                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    StateDA st = new StateDA();
                    MemberAddress memberAddress = new MemberAddress();
                    int statekey = st.GetStateKey(maddr.State);
                    var postcodemasterkey = (from p in context.PostCodeMasters
                                             where p.State_key == statekey && p.PostCode == maddr.PostCode
                                             select p.PostCodeMaster_key).FirstOrDefault();
                    //#if DEBUG
                    //                    postcodemasterkey = 3;
                    //                    maddr.Suburb = "Bonnyrig";
                    //#endif
                    var subkey = (from s in context.Suburbs
                                  where s.PostCodeMaster_key == postcodemasterkey && s.Suburb1 == maddr.Suburb
                                  select s.Suburb_key).FirstOrDefault();
                    if (IsModify == true)
                    {
                        var ma = (from m in context.MemberAddresses
                                  where m.MemberAddressKey == maddr.Addresskey
                                  select m).FirstOrDefault();
                        if (ma != null)
                        {
                            ma.StreetNumber = maddr.StreetNumber;
                            ma.StreetName = maddr.StreetName;
                            ma.Suburb_key = subkey;
                            addresskey = maddr.Addresskey;
                        }
                    }
                    else
                    {
                        memberAddress.Member_key = MemberKey;
                        memberAddress.StreetName = maddr.StreetName;
                        memberAddress.StreetNumber = maddr.StreetNumber;
                        memberAddress.Suburb_key = subkey;
                        context.MemberAddresses.Add(memberAddress);
                    }
                    context.SaveChanges();
                    if (IsModify == false)
                    {
                        addresskey = memberAddress.MemberAddressKey;


                        string addressstring = maddr.StreetNumber + " " + maddr.StreetName;

                        AddAnyway(MemberKey, maddr.AddressType, addresskey, addressstring, maddr.Suburb, maddr.PostCode, maddr.State);

                        if (maddr.AddressType == "Primary" || maddr.AddressType == "Mailing")
                        {
                            AddPrimaryMailingAddress(MemberKey, maddr.AddressType, addresskey, addressstring, maddr.Suburb, maddr.PostCode, maddr.State);
                        }
                    }
                    
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddClone(AddressListModel maddr, int id)
        {

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                StateDA st = new StateDA();
                int statekey = st.GetStateKey(maddr.State);
                var postcodemasterkey = (from p in context.PostCodeMasters
                                         where p.State_key == statekey && p.PostCode == maddr.PostCode
                                         select p.PostCodeMaster_key).FirstOrDefault();
                var subkey = (from s in context.Suburbs
                              where s.PostCodeMaster_key == postcodemasterkey && s.Suburb1 == maddr.Suburb
                              select s.Suburb_key).FirstOrDefault();

                MemberAddress memberAddress = new MemberAddress();

                memberAddress.Member_key = id;
                memberAddress.StreetName = maddr.StreetName;
                memberAddress.StreetNumber = maddr.StreetNumber;
                memberAddress.Suburb_key = subkey;


                context.MemberAddresses.Add(memberAddress);
                context.SaveChanges();

                string addressstring = maddr.StreetNumber + " " + maddr.StreetName;

                AddAnyway(maddr.Member.Member_key, maddr.AddressType, memberAddress.MemberAddressKey, addressstring, maddr.Suburb, maddr.PostCode, maddr.State);

                if (maddr.AddressType == "Primary" || maddr.AddressType == "Mailing")
                {
                    AddPrimaryMailingAddress(maddr.Member.Member_key, maddr.AddressType, memberAddress.MemberAddressKey, addressstring, maddr.Suburb, maddr.PostCode, maddr.State);
                }
            }

        }
        private void AddAnyway(int id, string aType, int aKey, string address, string city, string postcode, string state)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                try
                {
                    var addr = (from m in context.Members
                                where m.Member_key == id
                                select m).FirstOrDefault();

                    if (addr != null)
                    {
                        if (aType == "Mailing" && addr.Address != null)
                        {

                        }
                        else
                        {
                            addr.Address = address;
                            addr.City = city;
                            addr.Postcode = postcode;
                            addr.State = state;
                            context.SaveChanges();
                        }
                       
                    }
                }
                catch(Exception ex)
                {
                    throw ex;
                    //throw new Exception(ex.Message);
                }
            }
        }
     


        private void AddPrimaryMailingAddress(int id, string aType, int aKey, string address, string city, string postcode, string state)
        {
            if (aType == "Primary")
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var addr = (from m in context.Members
                                where m.Member_key == id
                                select m).FirstOrDefault();

                    if (addr != null)
                    {
                        addr.PrimaryAddressKey = aKey;
                        addr.Address = address;
                        addr.City = city;
                        addr.Postcode = postcode;
                        addr.State = state;

                        if (state != string.Empty)
                        {
                            var Isp = (addr.MemberId.Substring(0, 1)).ToUpper();

                            if (Isp != "N" && Isp != "Q" && Isp != "W" && Isp != "S" && Isp != "V" && Isp != "A" && Isp != "T")
                            {
                                if (addr.CreatedDate.GetValueOrDefault(DateTime.MinValue) > DateTime.Today)
                                {
                                    if (state.ToUpper() == "NT")
                                    {
                                        var memId = "T" + addr.MemberId;
                                        addr.MemberId = memId;
                                    }
                                    else
                                    {
                                        var s = (state.Substring(0, 1)).ToUpper();
                                        var memId = s + addr.MemberId;
                                        addr.MemberId = memId;
                                    }
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                }
            }
            if (aType == "Mailing")
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var addr = (from m in context.Members
                                where m.Member_key == id
                                select m).FirstOrDefault();

                    if (addr != null)
                    {
                        addr.MailingAddressKey = aKey;
                        context.SaveChanges();
                    }
                }
            }
        
        }


        public MemberAddressModel Get(int id)
        {
            MemberAddressModel address = new MemberAddressModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.MemberAddresses
                            where m.MemberAddressKey == id
                            select m).FirstOrDefault();

                if (addr != null)
                {
                   
                    address.AddressKey = addr.MemberAddressKey;
                    address.StreetName = addr.StreetName;
                    address.StreetNumber = addr.StreetNumber;
                    address.suburbKey = addr.Suburb_key.Value;
                    address.AddressType = checkAddress(addr.MemberAddressKey);
                    SuburbDA sda = new SuburbDA();
                    address.Suburb = sda.GetSuburbModel(addr.Suburb_key.Value);
                  

                }
            }
            return address;
        }

     
        public AddressListModel GetAddressListModel(int id)
        {
            AddressListModel address = new AddressListModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.MemberAddresses
                            where m.Member_key == id
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    MemberDA mda = new MemberDA();
                    MemberModel model = new MemberModel();
                    model =  mda.GetMemeber(id);
                    address.Member = model;
                }
            }
            return address;
        }
        public List<AddressListModel> ForExistingAddrListModel(int id)
        {
            List<AddressListModel> address = new List<AddressListModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Members
                            where m.Member_key == id
                            select m).FirstOrDefault();

                if (addr != null && addr.Address != null && addr.Address != string.Empty)
                {

                    AddressListModel adds = new AddressListModel();

                    MemberModel model = new MemberModel();
                    //model = mda.GetMemeber(id);
                    adds.Member = model;
                    adds.Member.Member_key = id;
                    try
                    {
                        string[] address1 = addr.Address.Split(null);

                        adds.StreetNumber = address1[0];
                        adds.StreetName = address1[1];
                    }
                    catch
                    { }
                    adds.Suburb = addr.City;

                    adds.PostCode = addr.Postcode;
                    adds.State = addr.State;


                    StateDA sda = new StateDA();
                    int skey = sda.GetStateKey(addr.State);
                    StateModel smo = new StateModel();
                    smo = sda.GetStateModel(skey);

                    PostCodeDA pcda = new PostCodeDA();
                    PostCodeModel pmol = pcda.GetPostCodeModel(smo.StateKey, addr.Postcode);

                    SuburbDA suda = new SuburbDA();
                    int subkey = suda.GetSuburbkey(pmol.PostCodeMaster_key, addr.City);
                    adds.Suburb_key = subkey;
                    CountryDA cda = new CountryDA();
                    CountryModel cmo = new CountryModel();
                    cmo = cda.Get(smo.Countrykey);
                    adds.Country = cmo.CountryName;

                    address.Add(adds);

                }
            }
            return address;
        }

        public List<AddressListModel> GetAddressListModelList(int id)
        {
            List<AddressListModel> address = new List<AddressListModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.MemberAddresses
                            where m.Member_key == id
                            select m;

                if (addr != null)
                {
                    foreach (var a in addr)
                    {
                         AddressListModel adds = new AddressListModel();
                        //MemberDA mda = new MemberDA();
                        MemberModel model = new MemberModel();
                        //model = mda.GetMemeber(id);
                        adds.Member = model;
                        adds.Member.Member_key = id;
                        MemberAddressModel ma = new MemberAddressModel();
                        ma =  Get(a.MemberAddressKey);

                        adds.StreetNumber = ma.StreetNumber;
                        adds.StreetName = ma.StreetName;
                        adds.Suburb = ma.Suburb.SuburbName;
                        adds.Suburb_key = ma.suburbKey;
                        adds.PostCode = ma.Suburb.PostCode;
                        adds.State = ma.Suburb.State.StateName;
                        adds.Country = ma.Suburb.Country.CountryName;
                    
                        address.Add(adds);
                    }
                }
            }
            return address;
        }

    }
}
