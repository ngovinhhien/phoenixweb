﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.Member;

namespace Phoenix_BusLog.Data
{
    public class ZipMerchantAPIDA
    {
        public void AddNew(ZipMerchantAPI zipMerchantAPI, string userName)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                zipMerchantAPI.CreatedByUser = userName;
                zipMerchantAPI.CreatedDateTime = DateTime.Now;
                context.ZipMerchantAPIs.Add(zipMerchantAPI);


                var historyNoteObj = new MemberHistory()
                {
                    Member_key = zipMerchantAPI.MemberKey == null ? 0 : zipMerchantAPI.MemberKey.Value,
                    History = $"[ZIP_API_KEY] {userName} has add new Zip API Key and Location ID [{zipMerchantAPI.ZipAPIKey} - {zipMerchantAPI.ZipLocationID}] for merchant key = {zipMerchantAPI.MemberKey}",
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now,
                };
                context.MemberHistories.Add(historyNoteObj);

                context.SaveChanges();
            }
        }


        public void Update(ZipMerchantAPI zipMerchantAPI, string userName, ZipMerchantAPI orgZipMerchantApi)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                zipMerchantAPI.UpdatedByUser = userName;
                zipMerchantAPI.UpdatedDateTime = DateTime.Now;
                context.Entry(zipMerchantAPI).State = EntityState.Modified;

                var historyNoteObj = new MemberHistory()
                {
                    Member_key = zipMerchantAPI.MemberKey == null ? 0 : zipMerchantAPI.MemberKey.Value,
                    History = $"[ZIP_API_KEY] {userName} has update Zip API Key and Location ID from [{orgZipMerchantApi.ZipAPIKey} - {orgZipMerchantApi.ZipLocationID}] with Active is {orgZipMerchantApi.InActive} to [{zipMerchantAPI.ZipAPIKey} - {zipMerchantAPI.ZipLocationID}] with Active is {zipMerchantAPI.InActive} ",
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now,
                };
                context.MemberHistories.Add(historyNoteObj);

                context.SaveChanges();
            }
        }
        public void Delete(int? memberKey, string userName)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var zipMerchantAPI = context.ZipMerchantAPIs.FirstOrDefault(x => x.MemberKey == memberKey);
                context.ZipMerchantAPIs.Remove(zipMerchantAPI);
                var historyNoteObj = new MemberHistory()
                {
                    Member_key = zipMerchantAPI.MemberKey == null ? 0 : zipMerchantAPI.MemberKey.Value,
                    History = $"[ZIP_API_KEY] {userName} has delete Zip API Key and Location ID [{zipMerchantAPI.ZipAPIKey} - {zipMerchantAPI.ZipLocationID}] of merchant key {memberKey}",
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now,
                };
                context.MemberHistories.Add(historyNoteObj);
                context.SaveChanges();
            }
        }

        public ZipMerchantAPI GetByMemberKey(int memberKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                return context.ZipMerchantAPIs.FirstOrDefault(x => x.MemberKey == memberKey);
            }

        }

        public ZipMerchantAPI FromZipMerchantAPIModel(ZipMerchantAPIModel zipMerchantAPIModel, ZipMerchantAPI zipMerchantAPI)
        {
            zipMerchantAPI.InActive = (bool)!zipMerchantAPIModel.IsActive;
            zipMerchantAPI.ZipLocationID = zipMerchantAPIModel.LocationID.Trim();
            zipMerchantAPI.MemberKey = zipMerchantAPIModel.MemberKey;
            zipMerchantAPI.ZipAPIKey = zipMerchantAPIModel.ZipAPIKey.Trim();
            return zipMerchantAPI;
        }
        public ZipMerchantAPIModel FromZipMerchantAPI(ZipMerchantAPI zipMerchantAPI)
        {
            ZipMerchantAPIModel model = new ZipMerchantAPIModel();
            model.IsActive = (bool)(!zipMerchantAPI.InActive ?? false);
            model.LocationID = zipMerchantAPI.ZipLocationID;
            model.MemberKey = zipMerchantAPI.MemberKey;
            model.ZipAPIKey = zipMerchantAPI.ZipAPIKey;
            return model;
        }

    }
}
