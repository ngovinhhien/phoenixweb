﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Financial;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using PhoenixObjects.Terminal;

namespace Phoenix_BusLog.Data
{
    public class TerminalAllocationDA
    {
        public int AddNew(TerminalAllocationModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                TerminalAllocation mServer = new TerminalAllocation();
                mServer.EftTerminal_key = t.EftTerminal_key;
                mServer.Vehicle_key = t.Vehicle_key;
                mServer.Member_key = t.Member_key;
                mServer.EffectiveDate = t.EffectiveDate;
                mServer.ModifiedByUser = t.ModifiedByUser;
                mServer.IsCurrent = t.IsCurrent;
                mServer.PendingSwapOut = t.PendingSwapOut;
                mServer.SwappedTerminalAllocation_key = t.SwappedTerminalAllocation_key;
                mServer.OriginalTerminalAllocation_key = t.OriginalTerminalAllocation_key;
                mServer.DeallocatedDate = t.DeallocatedDate;
                mServer.DeliveryRequired = t.DeliveryRequired;
                mServer.ServiceFee = t.ServiceFee;
                mServer.AllowMOTO = t.AllowMOTO;
                mServer.AllowRefund = t.AllowRefund;

                context.TerminalAllocations.Add(mServer);
                context.SaveChanges();
                AddOriginalKey(mServer.TerminalAllocation_key);
                return mServer.TerminalAllocation_key;
            }

        }
        private void AddOriginalKey(int TA_key)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var ta = (from t in context.TerminalAllocations
                          where t.TerminalAllocation_key == TA_key
                          select t).FirstOrDefault();
                if (ta != null)
                {

                    ta.OriginalTerminalAllocation_key = TA_key;
                    context.SaveChanges();
                }
            }
        }

        public void DisableGlideboxFollowBusinessType(int terminalKey, string userName, string source)
        {

                var tda = new TerminalDA();
                var terminalId = tda.GetTerminalId(terminalKey);

                if (!string.IsNullOrEmpty(terminalId))
                {
                    var thda = new TransactionHost.BusLogs.TerminalHostDA();
                    var memberKey = thda.GetMemberKeyAllocatedInTerminal(terminalId);
                    var isAllowGlideBox = memberKey != 0 ? thda.IsAllowAccessToGlidebox(memberKey) : true;

                    if (!isAllowGlideBox)
                    {
                        using (var entities = new Phoenix_TransactionHostUI.TransactionHostEntities())
                        {
                            var terminal = entities.Terminals.FirstOrDefault(x => x.TerminalID == terminalId);
                            if (terminal != null && terminal.EnableGlidebox)
                            {
                                //log history when user disable glidebox
                                var terminalHistory = new Phoenix_TransactionHostUI.TerminalHistory()
                                {
                                    TerminalID = terminal.TerminalID,
                                    ChangedDatetime = DateTime.Now,
                                    ChangedBy = userName,
                                    Source = source,
                                    EnableGlidebox = false,
                                };

                                entities.TerminalHistories.Add(terminalHistory);
                                terminal.EnableGlidebox = false;
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            
        }

        public void DeallocateTerminal(int TerminalAllocationKey, string Username, string status, DateTime EffectiveDate, string Location)
        {
            int Originalkey = 0;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    using (TaxiEpayEntities context = new TaxiEpayEntities())
                    {
                        var addr = (from m in context.TerminalAllocations
                                    where m.TerminalAllocation_key == TerminalAllocationKey
                                    select m).FirstOrDefault();

                        if (addr != null)
                        {
                            addr.DeallocatedDate = DateTime.Now;
                            addr.IsCurrent = false;
                            Originalkey = addr.TerminalAllocation_key;

                            TerminalAllocation mServer = new TerminalAllocation();
                            mServer.EftTerminal_key = addr.EftTerminal_key;
                            mServer.Vehicle_key = null;
                            mServer.Member_key = null;
                            mServer.EffectiveDate = EffectiveDate;
                            mServer.ModifiedByUser = Username;
                            mServer.IsCurrent = false;
                            mServer.PendingSwapOut = null;
                            mServer.SwappedTerminalAllocation_key = null;
                            mServer.OriginalTerminalAllocation_key = addr.OriginalTerminalAllocation_key;
                            mServer.DeallocatedDate = null;
                            mServer.DeliveryRequired = null;
                            mServer.ServiceFee = null;
                            mServer.AllowMOTO = null;
                            mServer.AllowMOTO = null;

                            context.TerminalAllocations.Add(mServer);

                            TerminalAllocation mServer1 = new TerminalAllocation();
                            mServer1.EftTerminal_key = null;
                            mServer1.Vehicle_key = addr.Vehicle_key;
                            mServer1.Member_key = addr.Member_key;
                            mServer1.EffectiveDate = EffectiveDate;
                            mServer1.ModifiedByUser = Username;
                            mServer1.IsCurrent = true;
                            mServer1.PendingSwapOut = null;
                            mServer1.SwappedTerminalAllocation_key = null;
                            mServer1.OriginalTerminalAllocation_key = addr.OriginalTerminalAllocation_key;
                            mServer1.DeallocatedDate = null;
                            mServer1.DeliveryRequired = null;
                            mServer1.ServiceFee = null;
                            mServer1.AllowMOTO = null;
                            mServer1.AllowMOTO = null;

                            context.TerminalAllocations.Add(mServer1);

                            var veh = (from m in context.Vehicles
                                       where m.Vehicle_key == addr.Vehicle_key
                                       select m).FirstOrDefault();
                            if (veh != null)
                            {
                                veh.TerminalAllocatedDate = EffectiveDate;
                                veh.Modified_dttm = DateTime.Now;
                                veh.ModifiedByUser = Username;
                                veh.EftTerminal_key = null;
                            }

                            context.SaveChanges();

                            //change the status
                            TerminalDA tda1 = new TerminalDA();
                            tda1.UpdateTerminalStatus(addr.EftTerminal_key.Value, status.ToString(), Location);

                            MemberDA mda = new MemberDA();
                            mda.UpdateModifiedDate(addr.Member_key.GetValueOrDefault(0), Username);


                            tran.Complete();
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

        }


        public void SwapTerminal(int OriginalTerminalAllocationKey, string Username, string status, int SwapEFtTerminalKey, DateTime EffectiveDate, string location)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                DeallocateTerminal(OriginalTerminalAllocationKey, Username, status, EffectiveDate, location);
                var memberKey = 0;
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var addr = (from m in context.TerminalAllocations
                                where m.TerminalAllocation_key == OriginalTerminalAllocationKey
                                select m).FirstOrDefault();
                    memberKey = addr.Member_key.GetValueOrDefault(0);
                    TerminalAllocation mServer1 = new TerminalAllocation();
                    mServer1.EftTerminal_key = SwapEFtTerminalKey;
                    mServer1.Vehicle_key = addr.Vehicle_key;
                    mServer1.Member_key = addr.Member_key;
                    mServer1.EffectiveDate = EffectiveDate;
                    mServer1.ModifiedByUser = Username;
                    mServer1.IsCurrent = true;
                    mServer1.PendingSwapOut = false;
                    mServer1.SwappedTerminalAllocation_key = addr.TerminalAllocation_key;
                    mServer1.OriginalTerminalAllocation_key = addr.OriginalTerminalAllocation_key;
                    mServer1.DeallocatedDate = null;
                    mServer1.DeliveryRequired = null;
                    mServer1.ServiceFee = null;
                    mServer1.AllowMOTO = null;
                    mServer1.AllowMOTO = null;

                    context.TerminalAllocations.Add(mServer1);

                    var veh = (from m in context.Vehicles
                               where m.Vehicle_key == addr.Vehicle_key
                               select m).FirstOrDefault();
                    if (veh != null)
                    {
                        veh.TerminalAllocatedDate = DateTime.Now;
                        veh.Modified_dttm = DateTime.Now;
                        veh.ModifiedByUser = Username;
                        veh.EftTerminal_key = SwapEFtTerminalKey;
                    }
                    context.SaveChanges();
                }
                MemberDA mda = new MemberDA();
                mda.UpdateModifiedDate(memberKey, Username);
                tran.Complete();
            }

        }



        public TerminalAllocationModel GetTerminalAllocation(int member_key)
        {
            TerminalAllocationModel terminalAllocation = new TerminalAllocationModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.TerminalAllocations
                           where m.Member_key == member_key
                           select m;

                if (addr != null)
                {

                    MemberDA mda = new MemberDA();
                    terminalAllocation.Member = mda.GetMemeber(member_key);
                    terminalAllocation.Employee_key = terminalAllocation.Member.Employee_key;
                }
            }
            return terminalAllocation;
        }


        public List<AllVehicleTerminalAllocationModel> GetAllVehicleTerminalAllocationList(int member_key)
        {
            List<AllVehicleTerminalAllocationModel> model = new List<AllVehicleTerminalAllocationModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var terminalAllocations = context.Database.SqlQuery<WebCashing_GetMemberTerminalAllocation_AllVehicleModel>(
                    $"EXEC WebCashing_GetMemberTerminalAllocation_AllVehicles @MemberKey = {member_key}").Where(n => n.TerminalAllocation_key != null && n.TerminalId != "" && n.Vehicle_key != 0);

                if (terminalAllocations != null)
                {
                    foreach (var terminalAllocation in terminalAllocations)
                    {
                        AllVehicleTerminalAllocationModel mo = new AllVehicleTerminalAllocationModel();
                        mo.EffectiveDate = terminalAllocation.EffectiveDate;
                        mo.DeallocatedDate = terminalAllocation.DeallocatedDate;
                        mo.Name = terminalAllocation.Name;
                        mo.OriginalTID = terminalAllocation.OriginalTID;
                        mo.RegisteredState = terminalAllocation.RegisteredState;
                        mo.SwappedTID = terminalAllocation.SwappedTID;
                        mo.TerminalAllocatedDate = terminalAllocation.TerminalAllocatedDate;
                        mo.TerminalAllocation_key = terminalAllocation.TerminalAllocation_key;
                        mo.TerminalId = terminalAllocation.TerminalId;
                        mo.Vehicle_key = terminalAllocation.Vehicle_key;
                        mo.VehicleId = terminalAllocation.VehicleId;
                        mo.VehicleTypeKey = terminalAllocation.VehicleType_key;
                        mo.TerminalKey = terminalAllocation.TerminalKey.HasValue ? terminalAllocation.TerminalKey.Value : 0;
                        mo.Inactive = terminalAllocation.Inactive;
                        mo.AppKey = terminalAllocation.AppKey;
                        mo.ReceiptKey = terminalAllocation.ReceiptKey;
                        mo.IsSetEOS = terminalAllocation.IsSetEOS;
                        mo.EnableSendAPIKey = terminalAllocation.EnableSendAPIKey;
                        mo.IsAllocated = terminalAllocation.IsAllocated;
                        mo.AutoCreatePortalAccount = terminalAllocation.AutoCreatePortalAccount;
                        model.Add(mo);
                    }
                }
            }
            return model;

        }


        public bool CheckMerchantNotHasVirtualTerminalAllocated(int memberKey)
        {
            try
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    return context.vTerminalAllocations.Any(n =>
                        n.IsCurrent && n.EftTerminal_key != null && n.Member_key == memberKey && !n.VirtualTerminal);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<TerminalAllocationModel> GetTerminalAllocationList(int member_key)
        {
            List<TerminalAllocationModel> terminalAllocationList = new List<TerminalAllocationModel>();
            try
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var addr = from m in context.TerminalAllocations
                               where m.Member_key == member_key && m.IsCurrent == true && m.EftTerminal_key != null
                               select m;
                    if (addr != null)
                    {
                        foreach (var t in addr)
                        {
                            TerminalAllocationModel mServer = new TerminalAllocationModel();

                            mServer.TerminalAllocation_key = t.TerminalAllocation_key;
                            mServer.EftTerminal_key = t.EftTerminal_key;
                            mServer.Vehicle_key = t.Vehicle_key;
                            mServer.Member_key = t.Member_key;
                            mServer.EffectiveDate = t.EffectiveDate;
                            mServer.ModifiedByUser = t.ModifiedByUser;
                            mServer.IsCurrent = t.IsCurrent;
                            mServer.PendingSwapOut = t.PendingSwapOut;
                            mServer.SwappedTerminalAllocation_key = t.SwappedTerminalAllocation_key;
                            mServer.OriginalTerminalAllocation_key = t.OriginalTerminalAllocation_key;
                            mServer.DeallocatedDate = t.DeallocatedDate;
                            mServer.DeliveryRequired = t.DeliveryRequired;
                            mServer.ServiceFee = t.ServiceFee;
                            mServer.AllowMOTO = t.AllowMOTO;
                            mServer.OnlineOrderEnabled = t.OnlineOrderEnabled.GetValueOrDefault(false);
                            TerminalDA tda = new TerminalDA();
                            string Tid = tda.GetTerminalId(t.EftTerminal_key.Value);

                            if (Tid != string.Empty)
                            {
                                mServer.EftTerminalId = Tid;
                                mServer.TerminalID = Tid;
                            }
                            VehicleDA vda = new VehicleDA();
                            VehicleModel vmodel = new VehicleModel();
                            vmodel = vda.Get(t.Vehicle_key.Value);
                            if (vmodel != null)
                                mServer.VehicleModel = vmodel;

                            terminalAllocationList.Add(mServer);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return terminalAllocationList = null;
            }
            return terminalAllocationList;
        }


        public List<TerminalAllocationModel> GetTerminalAllocationListLowVersion(int member_key)
        {
            List<TerminalAllocationModel> terminalAllocationList = new List<TerminalAllocationModel>();
            try
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var addr = from m in context.TerminalAllocations
                               where m.Member_key == member_key && m.IsCurrent == true && m.EftTerminal_key != null
                               select m;
                    if (addr != null)
                    {
                        foreach (var t in addr)
                        {
                            TerminalAllocationModel mServer = new TerminalAllocationModel();

                            mServer.TerminalAllocation_key = t.TerminalAllocation_key;
                            mServer.EftTerminal_key = t.EftTerminal_key;
                            mServer.Vehicle_key = t.Vehicle_key;


                            TerminalDA tda = new TerminalDA();
                            string Tid = tda.GetTerminalId(t.EftTerminal_key.Value);
                            if (Tid != string.Empty)
                            {
                                mServer.EftTerminalId = Tid;
                                mServer.TerminalID = Tid;
                            }
                            VehicleDA vda = new VehicleDA();
                            VehicleModel vmodel = new VehicleModel();
                            vmodel = vda.Get(t.Vehicle_key.Value);
                            if (vmodel != null)
                                mServer.VehicleModel = vmodel;

                            terminalAllocationList.Add(mServer);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return terminalAllocationList = null;
            }
            return terminalAllocationList;
        }

        public TerminalAllocationModel GetOnlineOrderEnableTerminalAllocation(int member_key)
        {
            TerminalAllocationModel OnlineOrderEnabledTerminal = null; //new List<TerminalAllocationModel>();
            try
            {
                List<TerminalAllocationModel> allterminalallocations = this.GetTerminalAllocationList(member_key);

                if (allterminalallocations != null && allterminalallocations.Count > 0)
                {
                    OnlineOrderEnabledTerminal = allterminalallocations.OrderBy(r => r.EffectiveDate).FirstOrDefault(r => r.OnlineOrderEnabled == true);
                }

            }
            catch (Exception ex)
            {
                OnlineOrderEnabledTerminal = null;
            }
            return OnlineOrderEnabledTerminal;
        }
        public TerminalAllocationModel GetOnlineOrderEnableTerminalAllocation(string MemberID)
        {
            int memberkey = 0;
            MemberDA memDA = new MemberDA();
            TerminalAllocationModel OnlineOrderEnabledTerminal = null;
            if (!string.IsNullOrEmpty(MemberID))
            {
                memberkey = memDA.GetMemberKey(MemberID);

                OnlineOrderEnabledTerminal = this.GetOnlineOrderEnableTerminalAllocation(memberkey);


            }
            return OnlineOrderEnabledTerminal;
        }

        public List<VehicleTypeModel> GetVehicleTypes(int member_key)
        {
            List<VehicleTypeModel> data = null;
            using (var db = new TaxiEpayEntities())
            {
                var query = from ta in db.TerminalAllocations
                            join ve in db.Vehicles on ta.Vehicle_key equals ve.Vehicle_key
                            join vet in db.VehicleTypes on ve.VehicleType_key equals vet.VehicleType_key
                            where ta.Member_key == member_key && ta.IsCurrent
                            select new VehicleTypeModel
                            {
                                Name = vet.Name,
                                Company_Key = vet.Company_key ?? 0,
                                VehicleType_key = vet.VehicleType_key
                            };
                data = query.Distinct().ToList();
            }
            return data;
        }

    }
}
