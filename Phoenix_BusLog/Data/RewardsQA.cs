﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Rewards;

namespace Phoenix_BusLog.Data
{
    /// <summary>
    /// This class represents Member Refernece Entry to store qantas refernce numbers
    /// </summary>
    public class RewardsDA : IDisposable
    {
        public void Dispose()
        {

        }

        private void CreateMap()
        {

        }



        public IEnumerable<QantasExportFileModel> GetPending()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.QantasExportFiles.Where(where => where.QantasExportFileStatusId == 1 || where.QantasExportFileStatusId == 2);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFile, QantasExportFileModel>();
                    cfg.CreateMap<QantasExportFileApprover, QantasExportFileApproverModel>();
                    cfg.CreateMap<QantasExportFileApproverStatu, QantasExportFileApproverStatusModel>();
                    cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>();
                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var qantasExportFileModel = mapper.Map<IEnumerable<QantasExportFile>, IEnumerable<QantasExportFileModel>>(source);

                return qantasExportFileModel;
            }
        }

        public IEnumerable<QantasExportFileModel> Get()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.QantasExportFiles.OrderByDescending(order => order.ExportDateTime);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFile, QantasExportFileModel>();
                    cfg.CreateMap<QantasExportFileApprover, QantasExportFileApproverModel>();
                    cfg.CreateMap<QantasExportFileApproverStatu, QantasExportFileApproverStatusModel>();
                    cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>();
                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var qantasExportFileModel = mapper.Map<IEnumerable<QantasExportFile>, IEnumerable<QantasExportFileModel>>(source);

                return qantasExportFileModel;
            }
        }

        public IEnumerable<LoyaltyExportFileModel> GetUploadedFiles()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.LoyaltyExportFiles.OrderByDescending(order => order.ExportDateTime);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<LoyaltyExportFile, LoyaltyExportFileModel>()
                        .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore());
                    cfg.CreateMap<LoyaltyExportFileApprover, LoyaltyExportFileApproverModel>()
                        .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());
                    cfg.CreateMap<LoyaltyUploadFileHeader, LoyaltyUploadFileHeaderModel>();
                    cfg.CreateMap<LoyaltyUploadFileLineItem, LoyaltyUploadFileLineItemModel>()
                        .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore());
                    cfg.CreateMap<QantasExportFileApproverStatu, QantasExportFileApproverStatusModel>();
                    cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>();
                    cfg.CreateMap<LoyaltyExportFileLineItem, LoyaltyExportFileLineItemModel>()
                                .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu))
                                .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());

                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var qantasExportFileModel = mapper.Map<IEnumerable<LoyaltyExportFile>, IEnumerable<LoyaltyExportFileModel>>(source);

                return qantasExportFileModel;
            }
        }

        public LoyaltyExportFileModel GetUploadedFile(int id)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.LoyaltyExportFiles.Where(where => where.LoyaltyExportFileId == id).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<LoyaltyExportFile, LoyaltyExportFileModel>()
                        .ForMember(dest => dest.LoyaltyExportFileApprovers, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore()); 
                        
                    cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>()
                        .ForMember(dest => dest.QantasExportFiles, opt => opt.Ignore());
                });

                IMapper mapper = config.CreateMapper();
                var qantasExportFileModel = mapper.Map<LoyaltyExportFile, LoyaltyExportFileModel>(exportFiles);

                return qantasExportFileModel;
            }
        }


        public IEnumerable<QantasFileEntryModel> GetExportFileEntries(int exportfileId, out int count, int skip = 0, int take = 0, bool onlyRejected = false)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.QantasFileEntries.Where(where => where.QantasExportFileKey == exportfileId);
                if (onlyRejected)
                {
                    exportFiles = context.QantasFileEntries.Where(where => where.QantasExportFileKey == exportfileId && 
                                                    where.QantasExportFileEntryStatusId.HasValue
                                                    && where.QantasExportFileEntryStatu.Name.Contains("Reject"));
                }
                else
                {
                    exportFiles = context.QantasFileEntries.Where(where => where.QantasExportFileKey == exportfileId &&  
                                                    where.QantasExportFileEntryStatusId.HasValue
                                                && !where.QantasExportFileEntryStatusId.Value.Equals(5)
                                                && !where.QantasExportFileEntryStatusId.Value.Equals(6)
                                                && !where.QantasExportFileEntryStatusId.Value.Equals(7)
                                                && !where.QantasExportFileEntryStatusId.Value.Equals(12));
                }
                

                context.Database.CommandTimeout = 150;

                count = exportFiles.Count();
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFileEntryStatu, QantasExportFileEntryStatusModel>();
                    cfg.CreateMap<QantasFileEntry, QantasFileEntryModel>()
                                .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu));
                    cfg.CreateMap<Member, MemberModel>().ForMember(x => x.Addresses, opt => opt.Ignore())
                                        .ForMember(x => x.MemberAddresses, opt => opt.Ignore())
                                        .ForMember(x => x.Operator, opt => opt.Ignore())
                                        .ForMember(x => x.MemberReferences, opt => opt.MapFrom(src => src.MemberReferences.FirstOrDefault()));

                    cfg.CreateMap<MemberReferenceEntry, MemberReferenceEntryModel>()
                                .ForMember(x => x.ExportStatus, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().QantasExportFileEntryStatu.Name))
                                .ForMember(x => x.ApprovedRejectAndReason, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().UploadStatus));
                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var qantasExportFileModel = mapper.Map<IEnumerable<QantasFileEntry>, IEnumerable<QantasFileEntryModel>>(
                    source.OrderBy(order => order.MemberReferenceEntryKey)
                    .Skip(skip).Take(take));

                return qantasExportFileModel;
            }
        }
        public IEnumerable<MemberReferenceEntryModel> GetQbrPointHistory(long memberId, out int count, int skip = 0, int take = 0)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.MemberReferenceEntries.Where(where => where.Member_Key == memberId );

                count = exportFiles.Count();

                context.Database.CommandTimeout = 150;
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFileEntryStatu, QantasExportFileEntryStatusModel>();

                    cfg.CreateMap<QantasFileEntry, QantasFileEntryModel>()
                                .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu));

                    cfg.CreateMap<MemberReference, MemberReferenceModel>();

                    cfg.CreateMap<Member, MemberModel>().ForMember(x => x.Addresses, opt => opt.Ignore())
                                            .ForMember(x => x.MemberAddresses, opt => opt.Ignore())
                                            .ForMember(x => x.Operator, opt => opt.Ignore())
                                            .ForMember(x => x.MemberReferences, opt => opt.MapFrom(src => src.MemberReferences.FirstOrDefault()));

                    cfg.CreateMap<MemberReferenceEntry, MemberReferenceEntryModel>()
                                            .ForMember(x => x.ExportStatus, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().QantasExportFileEntryStatu.Name))
                                            .ForMember(x => x.ApprovedRejectAndReason, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().UploadStatus))
                                            .ForMember(x => x.PromotionId, opt => opt.MapFrom(src => src.LoyaltyPromotionID))
                                            ;
                    
                });
                
                IMapper mapper = config.CreateMapper();
                IQueryable<MemberReferenceEntry> source = exportFiles;
                IEnumerable<MemberReferenceEntryModel> qantasExportFileModel = mapper.Map<IEnumerable<MemberReferenceEntry>, IEnumerable<MemberReferenceEntryModel>>(
                    source  
                            .OrderByDescending(order => order.YearPeriod)
                            .ThenByDescending(order => order.MonthPeriod)
                            .ThenByDescending(order => order.EntryDateTime)
                            .Skip(skip).Take(take)
                    );
                List<int> promotionIds = qantasExportFileModel.Select(q => q.PromotionId).ToList();
                var promotionList = context.LoyaltyPromotions.Where(x => promotionIds.Contains(x.ID)).Select(x => new
                {
                    Id = x.ID,
                    Name = x.PromotionName
                }).ToList();

                IEnumerable<MemberReferenceEntryModel> result = 
                             (from qantasModel in qantasExportFileModel
                              join promotion in promotionList on qantasModel.PromotionId equals promotion.Id
                              select new MemberReferenceEntryModel
                              {
                                  MemberReferenceEntryKey = qantasModel.MemberReferenceEntryKey,
                                  EntryDateTime = qantasModel.EntryDateTime,
                                  MonthPeriod = qantasModel.MonthPeriod,
                                  YearPeriod = qantasModel.YearPeriod,
                                  Points = qantasModel.Points,
                                  Description = qantasModel.Description,
                                  MemberReferenceEntryTypeKey = qantasModel.MemberReferenceEntryTypeKey,
                                  Member = qantasModel.Member,
                                  ExportStatus = qantasModel.ExportStatus,
                                  ApprovedRejectAndReason = qantasModel.ApprovedRejectAndReason,
                                  PromotionId = promotion.Id,
                                  PromotionName = promotion.Name
                              });
                return result;
            }
        }

        public IEnumerable<QantasFileEntryModel> GetExportFileEntries(long memberId, int qantasExportFileEntryStatusId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var exportFiles = context.QantasFileEntries.Where(where =>
                                                qantasExportFileEntryStatusId > 0

                                                ?

                                                where.QantasExportFileEntryStatusId.HasValue
                                                && where.QantasExportFileEntryStatusId.Value.Equals(qantasExportFileEntryStatusId)
                                                && where.MemberReferenceEntry.Member_Key == memberId
                                                //&& where.UploadStatus != null
                                                :
                                                where.MemberReferenceEntry.Member_Key == memberId
                                                //&& where.UploadStatus != null
                                                );
                context.Database.CommandTimeout = 150;
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFileEntryStatu, QantasExportFileEntryStatusModel>();
                    cfg.CreateMap<QantasFileEntry, QantasFileEntryModel>()
                                .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu));
                    cfg.CreateMap<Member, MemberModel>().ForMember(x => x.Addresses, opt => opt.Ignore())
                                            .ForMember(x => x.MemberAddresses, opt => opt.Ignore())
                                            .ForMember(x => x.Operator, opt => opt.Ignore())
                                            .ForMember(x => x.MemberReferences, opt => opt.MapFrom(src => src.MemberReferences.FirstOrDefault())); 

                    cfg.CreateMap<MemberReferenceEntry, MemberReferenceEntryModel>()
                                            .ForMember(x => x.ExportStatus, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().QantasExportFileEntryStatu.Name))
                                            .ForMember(x => x.ApprovedRejectAndReason, opt => opt.MapFrom(src => src.QantasFileEntries.FirstOrDefault().UploadStatus));
                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var qantasExportFileModel = mapper.Map<IEnumerable<QantasFileEntry>, IEnumerable<QantasFileEntryModel>>(source);

                return qantasExportFileModel;
            }
        }

        public enum QantasExportFileEntryStatus
        {
            Approve,
            Rejected
        }

        public bool UpdateExportFileEntryStatus(int memberKey, int qantasExportFileKey, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var qantasExportFileEntryStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                                        where.IsDeleted == false
                                                                        && where.IsActive == true
                                                                        && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();
                var memberEnteries = context.MemberReferenceEntries.Where(where => where.Member_Key == memberKey).Select(select => select.MemberReferenceEntryKey).ToArray();
                var fileEnteries = context.QantasFileEntries.Where(where => where.QantasExportFileKey == qantasExportFileKey && where.MemberReferenceEntryKey.HasValue && memberEnteries.Contains(where.MemberReferenceEntryKey.Value));
                foreach (QantasFileEntry fileEntry in fileEnteries)
                {
                    fileEntry.QantasExportFileEntryStatusId = qantasExportFileEntryStatus.QantasExportFileEntryStatusId;
                }
                context.SaveChanges();
                return true;
            }
        }

        public bool UpdateExportFileEntryStatusAll(int qantasExportFileKey, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var qantasExportFileStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();

                var fileEnteries = context.QantasFileEntries.Where(where => where.QantasExportFileKey == qantasExportFileKey ).ToList();

                fileEnteries.ForEach(m => m.QantasExportFileEntryStatusId = qantasExportFileStatus.QantasExportFileEntryStatusId);

                context.SaveChanges();
                return true;
            }
        }

        public bool UpdateExportFileStatus(int qantasExportFileKey, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var qantasExportFileStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();

                var qantasExportStatus =
                    context.QantasExportFileStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "ready to be send to qbr" : "rejected")).FirstOrDefault();


                var exportFile = context.QantasExportFiles.Where(where => where.QantasExportFileKey == qantasExportFileKey).ToList();
                exportFile.ForEach(m => m.QantasExportFileStatusId = qantasExportStatus.QantasExportFileStatusId);

                var fileEnteries = context.QantasFileEntries.Where(where => where.QantasExportFileKey == qantasExportFileKey &&
                                                                     (where.QantasExportFileEntryStatusId == 1 || where.QantasExportFileEntryStatusId == 2)).ToList();

                fileEnteries.ForEach(m => m.QantasExportFileEntryStatusId = qantasExportFileStatus.QantasExportFileEntryStatusId);

                context.SaveChanges();
                return true;
            }
        }


        public IEnumerable<OrganisationforLoyaltyModel> GetOrganisationforLoyalties()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var data = context.OrganisationforLoyalties.Where(where => where.IsDeleted == false || where.IsActive == true);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrganisationforLoyalty, OrganisationforLoyaltyModel>();
                });

                IMapper mapper = config.CreateMapper();
                var modelData = mapper.Map<IEnumerable<OrganisationforLoyalty>, IEnumerable<OrganisationforLoyaltyModel>>(data);

                return modelData;
            }
        }

        public void Save(List<LoyaltyUploadFileLineItemModel> loyaltyUploadFileLineItemModels, string uploadedby, string filename, int loyaltyOrgId, bool forceUpdate = false)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                int lastNumber = context.LoyaltyExportFiles.Count() + 1;

                string partnerId = context.OrganisationforLoyalties.Where(where => where.OrganisationforLoyaltyId == loyaltyOrgId).FirstOrDefault().PartnerId;
                //TODO: What if there is no partner Id?

                filename = partnerId + "P." + lastNumber.ToString().PadLeft(3, '0');

                var loyaltyUploadFileHeaders = context.LoyaltyUploadFileHeaders.Add(new LoyaltyUploadFileHeader()
                {
                    FileName = filename,
                    CreatedBy = uploadedby,
                    CreatedOn = DateTime.Now,
                    OrganisationforLoyaltyId = loyaltyOrgId,
                });

                
                

                var loyaltyExportFile = context.LoyaltyExportFiles.Add(new LoyaltyExportFile()
                {
                    CreatedBy = uploadedby,
                    CreatedOn = DateTime.Now,
                    FileDate = DateTime.Now,
                    //FileName = partnerId +  "." + lastNumber.ToString().PadLeft(lastNumber.ToString().Length + 2, '0'),
                    FileName = filename, //need to add letter P after the partnerID and before sequence number
                    QantasExportFileStatusId = 2, //Pending                    
                    LoyaltyUploadFileHeader = loyaltyUploadFileHeaders,
                });

                foreach (LoyaltyUploadFileLineItemModel model in loyaltyUploadFileLineItemModels)
                {
                    var loyaltyUploadFileLineItem = context.LoyaltyUploadFileLineItems.Add(new LoyaltyUploadFileLineItem()
                    {
                        MemberId = model.MemberId,
                        Points = model.Points,
                        Name = model.Name,
                        LoyaltyNumber = model.LoyaltyNumber,
                        CreatedBy = uploadedby,
                        CreatedOn = DateTime.Now,
                        LoyaltyUploadFileHeader = loyaltyUploadFileHeaders,

                    });

                    context.LoyaltyExportFileLineItems.Add(new LoyaltyExportFileLineItem()
                    {
                        Points = model.Points,
                        LoyaltyNumber = model.LoyaltyNumber,
                        CreatedBy = uploadedby,
                        CreatedOn = DateTime.Now,
                        QantasExportFileEntryStatusId = 2,
                        LoyaltyUploadFileLineItem = loyaltyUploadFileLineItem,
                        LoyaltyExportFile = loyaltyExportFile,
                    });
                }

                var processApproverLists = context.ProcessApproverLists.Where(where => where.Process == "LoyaltyRewards");

                foreach (ProcessApproverList approvers in processApproverLists)
                {
                    context.LoyaltyExportFileApprovers.Add(new LoyaltyExportFileApprover()
                    {
                        LoyaltyExportFile = loyaltyExportFile,
                        ProcessApproverListId = approvers.ProcessApproverListId,
                        QantasExportFileApproverStatusId = 2,
                        CreatedBy = uploadedby,
                        Sequence = approvers.Sequence,
                        CreatedOn = DateTime.Now,
                    });
                }
                context.SaveChanges();
            }
        }

        public IEnumerable<LoyaltyExportFileModel> GetUploadedFiles(int organisationId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.LoyaltyExportFiles.Where(where => where.LoyaltyUploadFileHeader.OrganisationforLoyaltyId == organisationId);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<LoyaltyExportFile, LoyaltyExportFileModel>()
                         .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                         .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore())
                         .ForMember(dest => dest.QantasExportFileStatu, opt => opt.Ignore())
                         .ForMember(dest => dest.LoyaltyExportFileApprovers, opt => opt.Ignore())
                         ;
                    //cfg.CreateMap<LoyaltyExportFileApprover, LoyaltyExportFileApproverModel>()
                    //    .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());
                    //cfg.CreateMap<LoyaltyUploadFileHeader, LoyaltyUploadFileHeaderModel>();
                    //cfg.CreateMap<LoyaltyUploadFileLineItem, LoyaltyUploadFileLineItemModel>()
                    //    .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                    //    .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore());
                    //cfg.CreateMap<QantasExportFileApproverStatu, QantasExportFileApproverStatusModel>();
                    //cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>();
                    //cfg.CreateMap<LoyaltyExportFileLineItem, LoyaltyExportFileLineItemModel>()
                    //            .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu))
                    //            .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());
                });

                IMapper mapper = config.CreateMapper();
                var qantasExportFileModel = mapper.Map<IEnumerable<LoyaltyExportFile>, IEnumerable<LoyaltyExportFileModel>>(exportFiles);

                return qantasExportFileModel;
            }

        }
        public IEnumerable<LoyaltyExportFileModel> GetPendingforLoyalty(int organisationId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var exportFiles = context.LoyaltyExportFiles.Where(where => where.LoyaltyUploadFileHeader.OrganisationforLoyaltyId == organisationId
                                                                            && (where.QantasExportFileStatusId == 1 || where.QantasExportFileStatusId == 2));

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<LoyaltyExportFile, LoyaltyExportFileModel>();
                });
                IMapper mapper = config.CreateMapper();
                var qantasExportFileModel = mapper.Map<IEnumerable<LoyaltyExportFile>, IEnumerable<LoyaltyExportFileModel>>(exportFiles);

                return qantasExportFileModel;
            }
        }

        public IEnumerable<LoyaltyExportFileLineItemModel> GetLoyaltyExportFileEntries(int exportfileId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var exportFiles = context.LoyaltyExportFileLineItems.Where(where => where.LoyaltyExportFileId == exportfileId
                                                //&& where.QantasExportFileEntryStatusId.HasValue
                                                //&& where.QantasExportFileEntryStatusId.Value.Equals(2)
                                                );
                context.Database.CommandTimeout = 150;
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<LoyaltyExportFile, LoyaltyExportFileModel>()
                        .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore());
                    cfg.CreateMap<LoyaltyExportFileApprover, LoyaltyExportFileApproverModel>()
                        .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());
                    cfg.CreateMap<LoyaltyUploadFileHeader, LoyaltyUploadFileHeaderModel>();
                    cfg.CreateMap<LoyaltyUploadFileLineItem, LoyaltyUploadFileLineItemModel>()
                        .ForMember(dest => dest.LoyaltyUploadFileHeader, opt => opt.Ignore())
                        .ForMember(dest => dest.LoyaltyExportFileLineItems, opt => opt.Ignore());
                    cfg.CreateMap<QantasExportFileApproverStatu, QantasExportFileApproverStatusModel>();
                    cfg.CreateMap<QantasExportFileStatu, QantasExportFileStatusModel>();
                    cfg.CreateMap<LoyaltyExportFileLineItem, LoyaltyExportFileLineItemModel>()
                                .ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu))
                                .ForMember(dest => dest.LoyaltyExportFile, opt => opt.Ignore());
                });

                IMapper mapper = config.CreateMapper();
                var source = exportFiles;
                var loyaltyExportFileLineItem = mapper.Map<IEnumerable<LoyaltyExportFileLineItem>, IEnumerable<LoyaltyExportFileLineItemModel>>(source);

                return loyaltyExportFileLineItem;
            }
        }

        public IEnumerable<LoyaltyExportFileLineItemModel> GetLoyaltyExportFileEntries(string memberId, int qantasExportFileEntryStatusId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var exportFiles = context.LoyaltyExportFileLineItems.Where(where =>
                                                qantasExportFileEntryStatusId > 0

                                                ?

                                                where.QantasExportFileEntryStatusId.HasValue
                                                && where.QantasExportFileEntryStatusId.Value.Equals(qantasExportFileEntryStatusId)
                                                && where.LoyaltyUploadFileLineItem.MemberId == memberId
                                                //&& where.UploadStatus != null
                                                :
                                                where.LoyaltyUploadFileLineItem.MemberId == memberId
                                                //&& where.UploadStatus != null
                                                );
                context.Database.CommandTimeout = 150;
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<QantasExportFileEntryStatu, QantasExportFileEntryStatusModel>();
                    cfg.CreateMap<LoyaltyExportFileLineItem, LoyaltyExportFileLineItemModel>();
                    //.ForMember(dest => dest.QantasExportFileEntryStatus, opt => opt.MapFrom(src => src.QantasExportFileEntryStatu));
                });

                IMapper mapper = config.CreateMapper();
                var loyaltyExportFileLineItem = mapper.Map<IEnumerable<LoyaltyExportFileLineItem>, IEnumerable<LoyaltyExportFileLineItemModel>>(exportFiles);

                return loyaltyExportFileLineItem;
            }
        }

        public bool UpdateLoyaltyExportFileEntryStatus(string memberKey, int loyaltyExportFileId, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var loyaltyExportFileEntryStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                                        where.IsDeleted == false
                                                                        && where.IsActive == true
                                                                        && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();

                var memberEnteries = context.LoyaltyUploadFileLineItems.Where(where => where.MemberId == memberKey).Select(select => select.MemberId).ToArray();

                var fileEnteries = context.LoyaltyExportFileLineItems.Where(where => where.LoyaltyExportFileId == loyaltyExportFileId
                            && where.LoyaltyUploadFileLineItemId.HasValue && memberEnteries.Contains(where.LoyaltyUploadFileLineItem.MemberId));


                foreach (LoyaltyExportFileLineItem fileEntry in fileEnteries)
                {
                    fileEntry.QantasExportFileEntryStatusId = loyaltyExportFileEntryStatus.QantasExportFileEntryStatusId;
                }
                context.SaveChanges();
                return true;
            }
        }

        public bool UpdateLoyaltyExportFileEntryStatusAll(int qantasExportFileKey, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var qantasExportFileStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();

                var fileEnteries = context.LoyaltyExportFileLineItems.Where(where => where.LoyaltyExportFileId == qantasExportFileKey).ToList();

                fileEnteries.ForEach(m => m.QantasExportFileEntryStatusId = qantasExportFileStatus.QantasExportFileEntryStatusId);

                context.SaveChanges();
                return true;
            }
        }

        public bool UpdateLoyaltyExportFileStatus(int loyaltyExportFileId, QantasExportFileEntryStatus status)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var qantasExportFileStatus =
                    context.QantasExportFileEntryStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "approved by finance" : "rejected by finance")).FirstOrDefault();

                var qantasExportStatus =
                    context.QantasExportFileStatus.Where(where =>
                                                    where.IsDeleted == false
                                                    && where.IsActive == true
                                                    && where.Name.ToLower() == (status == QantasExportFileEntryStatus.Approve ? "ready to be send to qbr" : "rejected")).FirstOrDefault();


                var exportFile = context.LoyaltyExportFiles.Where(where => where.LoyaltyExportFileId == loyaltyExportFileId && where.QantasExportFileStatusId == 2).ToList();
                exportFile.ForEach(m => m.QantasExportFileStatusId = qantasExportStatus.QantasExportFileStatusId );

                var fileEnteries = context.LoyaltyExportFileLineItems.Where(where => where.LoyaltyExportFileId == loyaltyExportFileId &&
                                                                     where.QantasExportFileEntryStatusId == 1 || where.QantasExportFileEntryStatusId == 2).ToList();

                fileEnteries.ForEach(m => m.QantasExportFileEntryStatusId = qantasExportFileStatus.QantasExportFileEntryStatusId);

                context.SaveChanges();
                return true;
            }
        }

    }
}
