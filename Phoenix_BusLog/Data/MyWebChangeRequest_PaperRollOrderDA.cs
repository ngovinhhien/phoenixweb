﻿using Phoenix_Service;
using PhoenixObjects.MyWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
  public class MyWebChangeRequest_PaperRollOrderDA
    {
      public List<PaperRollRequestModel> GetAllPending(string username)
        {
            List<PaperRollRequestModel> Pending = new List<PaperRollRequestModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                string status = TEnum.MyWebChangeRequestStatus.PENDING.ToString();
                var addr = context.MyWebChangeRequest_PaperRollOrder.Where(n => n.Status == status);
                if (addr.Any())
                {
                    foreach (var v in addr)
                    {
                        var mem = context.Members.FirstOrDefault(n => n.Member_key == v.Member_key);
                        if (mem != null)
                        {
                            PaperRollRequestModel mda = new PaperRollRequestModel();
                            mda.AssignedDate = v.AssignedDate;
                            mda.AssignedTo = v.AssignedTo;
                            mda.CheckedBy = v.CheckedBy;
                            mda.CheckedDate = v.CheckedDate;
                            mda.CompleteBy = v.CompleteBy;
                            mda.CompletedDate = v.CompletedDate;
                            mda.CreateDate = v.CreateDate;
                            mda.CreatedBy = v.CreatedBy;
                            mda.Member_key = v.Member_key;                       
                            mda.RequestID = v.RequestID;
                            mda.Status = v.Status;
                            mda.FirstName = mem.FirstName;
                            mda.LastName = mem.LastName;
                            mda.MemberId = mem.MemberId;
                            mda.TradingName = mem.TradingName;
                            mda.Comments = v.Comments;
                            mda.Street = v.Street;
                            mda.QuantityRequested = v.QuantityRequested;
                            mda.QuantitySent = v.QuantitySent;
                            mda.OrgStreet = mem.Address + " " + mem.City + " " + mem.State + " " + mem.Postcode;
                        
                            Pending.Add(mda);
                        }
                    }
                }
            }
            return Pending;



        }

      public PaperRollRequestModel GetRequest(int RequestID)
        {
            PaperRollRequestModel mda = new PaperRollRequestModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var v = context.MyWebChangeRequest_PaperRollOrder.FirstOrDefault(n => n.RequestID == RequestID); 
                if (v != null)
                {
                    var mem = context.Members.FirstOrDefault(n => n.Member_key == v.Member_key);
                    if (mem != null)
                    {
                        mda.AssignedDate = v.AssignedDate;
                        mda.AssignedTo = v.AssignedTo;
                        mda.CheckedBy = v.CheckedBy;
                        mda.CheckedDate = v.CheckedDate;
                        mda.CompleteBy = v.CompleteBy;
                        mda.CompletedDate = v.CompletedDate;
                        mda.CreateDate = v.CreateDate;
                        mda.CreatedBy = v.CreatedBy;
                        mda.Member_key = v.Member_key;
                        mda.RequestID = v.RequestID;
                        mda.Status = v.Status;
                        mda.FirstName = mem.FirstName;
                        mda.LastName = mem.LastName;
                        mda.MemberId = mem.MemberId;
                        mda.TradingName = mem.TradingName;
                        mda.Comments = v.Comments;
                        mda.Street = v.Street;
                        mda.QuantityRequested = v.QuantityRequested;
                        mda.QuantitySent = v.QuantitySent;
                        mda.OrgStreet = mem.Address + " " + mem.City + " " + mem.State + " " + mem.Postcode;
                    }
                }
            }
            return mda;

        }
      public void UpdateStatusComplete(PaperRollRequestModel mod)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var v = (from m in context.MyWebChangeRequest_PaperRollOrder
                         where m.RequestID == mod.RequestID
                         select m).FirstOrDefault();

                if (v != null)
                {
                    v.CompleteBy = mod.CompleteBy;
                    v.CompletedDate = mod.CompletedDate;
                    v.Status = mod.Status;

                    context.SaveChanges();
                }
            }
        }

    }
}
