﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;
using PhoenixObjects.Enums;

namespace Phoenix_BusLog.Data
{
    public class AccountDA
    {
        public void Add(BankAccountModel t)
        {
            //using (TaxiEpayEntities context = new TaxiEpayEntities())
            //{
            //    Account aSever = new Account();                
            //    aSever.Bsb = t.BSB;
            //    aSever.AccountNumber = t.AccountNumber;
            //    aSever.AccountName = t.AccountName;
            //    aSever.Weight = Convert.ToInt64(t.Weight);
            //    aSever.Member_key = t.Member_Key;
            //    aSever.CreatedDate = DateTime.Now;
            //    aSever.ModifiedByUser = t.UserName;
            //    aspnet_Applications
            //    context.Accounts.Add(aSever);
            //    context.SaveChanges();
            //}

            this.Add(t, "");
        }

        public int Add(BankAccountModel t, string Type)
        {
            int accountkey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Account aSever = new Account();
                aSever.Bsb = t.BSB;
                aSever.AccountNumber = t.AccountNumber;
                aSever.AccountName = t.AccountName;
                aSever.Weight = Convert.ToInt64(t.Weight);
                aSever.Member_key = t.Member_Key;
                aSever.CreatedDate = DateTime.Now;
                aSever.Modified_dttm = DateTime.Now;
                aSever.ModifiedByUser = t.UserName;

                context.Accounts.Add(aSever);
                context.SaveChanges();
                accountkey = aSever.Account_key;
            }

            return accountkey;
        }

        public bool IsExistingBankAccountInfo(string bsb, string accountNumber, int memberKey)
        {
            var res = false;
            using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
            {
                res = taxiEpayDbContext.Accounts.Any(x => x.Bsb == bsb && x.AccountNumber == accountNumber && x.Member_key == memberKey);
            }
            return res;
        }

        public void UpdateBankAccountToMember(TaxiEpayEntities taxiEpayDbContext, List<BankAccountDTO> bankAccountDtoList, Member member, string UpdatedUser)
        {
            var originalBankAccounts = member.Accounts
                .ToList();
            var historyNote = new StringBuilder();

            //var lstBankAccountIDs = bankAccountDtoList.Where(n => n.AccountKey > 0).Select(n => n.AccountKey);
            //var lstBankAccountRemove = originalBankAccounts.Where(n => !lstBankAccountIDs.Contains(n.Account_key));

            //if (lstBankAccountRemove.Any())
            //{
            //    var removeBankAccountNote = string.Empty;
            //    removeBankAccountNote +=
            //        $"Bank account(s) {string.Join(",", lstBankAccountRemove.Select(n => n.Account_key))} has been removed out of Merchant {member.Member_key}|";
            //    historyNote.Append(removeBankAccountNote);
            //}

            //foreach (var account in lstBankAccountRemove)
            //{
            //    account.IsActive = false;
            //}
            var purposeTypeForSettingDefaultWeight = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PurposeTypeIdForSettingWeight.ToString(), taxiEpayDbContext);
            foreach (var account in bankAccountDtoList)
            {
                var note = string.Empty;
                //new account
                if (!originalBankAccounts.Exists(x => x.Account_key == account.AccountKey))
                {
                    var newAccount = new Account
                    {
                        AccountName = account.AccountName,
                        AccountNumber = account.AccountNumber,
                        Bsb = account.BSB,
                        Member_key = member.Member_key,
                        CreatedDate = DateTime.Now,
                        Modified_dttm = DateTime.Now,
                        ModifiedByUser = UpdatedUser,
                        Weight = 0
                    };

                    //add purpose type to new account
                    var purposeList = JsonConvert.DeserializeObject<List<TransactionPurposeDTO>>(account.TransactionPurposeDtoAsJson);
                    var purposeActiveIds = purposeList.Where(x => x.Checked);
                    foreach (var purposeActiveId in purposeActiveIds)
                    {
                        if (purposeActiveId.PurposeTypeID != 0) //check any purpose is chooses
                        {
                            if (purposeActiveId.PurposeTypeID.ToString() == purposeTypeForSettingDefaultWeight)
                            {
                                newAccount.Weight = 100;

                                //Update Original Bank Account has weight 100 to 0
                                var orgBankAccount = originalBankAccounts.FirstOrDefault(n => n.Weight == 100);
                                if (orgBankAccount != null)
                                {
                                    orgBankAccount.Weight = 0;
                                    orgBankAccount.ModifiedByUser = UpdatedUser;
                                    orgBankAccount.Modified_dttm = DateTime.Now;
                                }
                            }

                            var accountPurpose = new AccountPurposeType { Account = newAccount, PurposeTypeID = purposeActiveId.PurposeTypeID };
                            if (!string.IsNullOrEmpty(note))
                            {
                                note += "|";
                            }

                            var memberInformation = member.Member_key == 0 ? member.Email : member.MemberId;
                            note +=
                                $"[New Account has been added to Merchant {memberInformation} with purpose [{purposeActiveId.PurposeTypeID}-{purposeActiveId.PurposeTypeName}]";
                            newAccount.AccountPurposeTypes.Add(accountPurpose);
                        }
                    }

                    member.Accounts.Add(newAccount);
                }
                else // update existing account
                {
                    var purposeList = JsonConvert.DeserializeObject<List<TransactionPurposeDTO>>(account.TransactionPurposeDtoAsJson);
                    foreach (var purpose in purposeList)
                    {
                        var purposeTypeOfOriginAccount = taxiEpayDbContext.AccountPurposeTypes
                            .FirstOrDefault(x => x.AccountKey == account.AccountKey && x.PurposeTypeID == purpose.PurposeTypeID);
                        var purposeActiveId = purpose.Checked ? purpose.PurposeTypeID : 0;

                        //Currently, if purpose type name the same with configuration,
                        //we will set the weight of updated bank account to 100, and another bank account will be set to 0.
                        if (purpose.PurposeTypeID.ToString() == purposeTypeForSettingDefaultWeight && purposeActiveId != 0)
                        {
                            //Update Original Bank Account has weight 100 to 0
                            var orgBankAccount = originalBankAccounts.FirstOrDefault(n => n.Weight == 100);
                            if (orgBankAccount != null)
                            {
                                orgBankAccount.Weight = 0;
                                orgBankAccount.ModifiedByUser = UpdatedUser;
                                orgBankAccount.Modified_dttm = DateTime.Now;
                            }

                            var updatedBankAccount =
                                originalBankAccounts.FirstOrDefault(n => n.Account_key == account.AccountKey);
                            if (updatedBankAccount != null)
                            {
                                updatedBankAccount.Weight = 100;
                                updatedBankAccount.ModifiedByUser = UpdatedUser;
                                updatedBankAccount.Modified_dttm = DateTime.Now;
                            }
                        }

                        if (purposeTypeOfOriginAccount != null) // exist purpose
                        {
                            //check => other check
                            if (purposeTypeOfOriginAccount.PurposeTypeID != purposeActiveId && purposeActiveId != 0)
                            {
                                if (!string.IsNullOrEmpty(note))
                                {
                                    note += "|";
                                }
                                note +=
                                    $"[Account {purposeTypeOfOriginAccount.AccountKey}] changed purpose from [{purposeTypeOfOriginAccount.PurposeTypeID}] to [{purposeActiveId}]";
                                purposeTypeOfOriginAccount.PurposeTypeID = purposeActiveId;
                            }

                            //check => uncheck
                            if (purposeTypeOfOriginAccount.PurposeTypeID != purposeActiveId && purposeActiveId == 0)
                            {

                                var accountPurposeType = taxiEpayDbContext.AccountPurposeTypes.FirstOrDefault(x =>
                                    x.AccountKey == account.AccountKey &&
                                    x.PurposeTypeID == purposeTypeOfOriginAccount.PurposeTypeID);
                                if (!string.IsNullOrEmpty(note))
                                {
                                    note += "|";
                                }

                                note +=
                                    $"[Account {purposeTypeOfOriginAccount.AccountKey}] removed out of purpose [{accountPurposeType.PurposeTypeID}]";
                                taxiEpayDbContext.AccountPurposeTypes.Remove(accountPurposeType);
                            }
                        }
                        else // new purpose check
                        {
                            if (purposeActiveId == 0)
                            {
                                continue;
                            }
                            if (!string.IsNullOrEmpty(note))
                            {
                                note += "|";
                            }
                            note +=
                                $"[Account {account.AccountKey}] added purpose [{purposeActiveId}]";
                            var newAccountPurposeType = new AccountPurposeType
                            {
                                AccountKey = account.AccountKey,
                                PurposeTypeID = purposeActiveId
                            };
                            taxiEpayDbContext.AccountPurposeTypes.Add(newAccountPurposeType);
                        }
                    }
                }
                historyNote.Append(note);
            }

            if (!string.IsNullOrEmpty(historyNote.ToString()))
            {
                var historyNoteObj = new MemberHistory()
                {
                    Member_key = member.Member_key,
                    History = historyNote.ToString(),
                    CreatedBy = UpdatedUser,
                    CreatedDate = DateTime.Now,
                };
                taxiEpayDbContext.MemberHistories.Add(historyNoteObj);
            }
        }

        public void Edit(BankAccountModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var aSever = (from a in context.Accounts
                              where a.Account_key == t.AccountKey
                              select a).FirstOrDefault();

                if (aSever != null)
                {
                    aSever.Bsb = t.BSB;
                    aSever.AccountNumber = aSever.AccountNumber;
                    aSever.AccountName = aSever.AccountName;
                    aSever.Weight = Convert.ToInt64(t.Weight);
                    aSever.Modified_dttm = DateTime.Now;
                    aSever.ModifiedByUser = t.UserName;

                }
                context.SaveChanges();
            }
        }


        public void AddClone(BankAccountModel t, int id)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Account aSever = new Account();
                aSever.Bsb = t.BSB;
                aSever.AccountNumber = t.AccountNumber;
                aSever.AccountName = t.AccountName;
                aSever.Weight = Convert.ToInt64(t.Weight);
                aSever.Member_key = id;
                aSever.CreatedDate = DateTime.Now;

                context.Accounts.Add(aSever);
                context.SaveChanges();
            }
        }

        public BankAccountModel GetAccountBykey(int Accountkey)
        {
            BankAccountModel account = new BankAccountModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var t = (from m in context.Accounts
                         where m.Account_key == Accountkey
                         select m).FirstOrDefault();

                if (t != null)
                {
                    account.AccountKey = t.Account_key;
                    account.BSB = t.Bsb;
                    account.AccountNumber = t.AccountNumber;
                    account.AccountName = t.AccountName;
                    account.Weight = Convert.ToInt32(t.Weight);
                    account.Member_Key = t.Member_key.Value;
                }
            }
            return account;
        }

        public List<BankAccountModel> Get(int id)
        {
            List<BankAccountModel> account = new List<BankAccountModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var acc = from m in context.Accounts
                          where m.Member_key == id
                          select m;

                if (acc.Count() > 0)
                {
                    foreach (var t in acc)
                    {
                        BankAccountModel acct = new BankAccountModel();
                        acct.AccountKey = t.Account_key;
                        acct.BSB = t.Bsb;
                        acct.AccountNumber = t.AccountNumber;
                        acct.AccountName = t.AccountName;
                        acct.Weight = Convert.ToInt32(t.Weight);
                        acct.Member_Key = t.Member_key.Value;
                        // MemberDA mda = new MemberDA();
                        // acct.Member = mda.GetMemeber(id);
                        account.Add(acct);
                    }
                }
            }
            return account;
        }

        public decimal GetMemberAccountTotalWeightNOTforCurrentAcc(int Member_Key, int Account_key)
        {
            decimal totalWeight = 0.0M;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var acc = from m in context.Accounts
                          where m.Member_key == Member_Key && m.Account_key != Account_key
                          select m;

                if (acc.Count() > 0)
                {
                    foreach (var t in acc)
                    {
                        totalWeight = totalWeight + Convert.ToDecimal(t.Weight);
                    }
                }
            }
            return totalWeight;
        }



        public BankAccountModel GetAccountListModel(int id)
        {
            BankAccountModel account = new BankAccountModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.Accounts
                           where m.Member_key == id
                           select m;

                if (addr != null)
                {

                    MemberDA mda = new MemberDA();
                    account.Member = mda.GetMemeber(id);
                }
            }
            return account;
        }

    }
}

