﻿using Phoenix_Service;
using PhoenixObjects.Cashing;
using PhoenixObjects.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class PendingTransactionDetailDA
    {

        //public List<WebCashing_GetPendingBatchesByMemberKey> GetPendingTransaction(int member_key, int docketchecksummaryKey, int? Time = null)
        //{
        //    List<WebCashing_GetPendingBatchesByMemberKey_Result3> addr = new List<WebCashing_GetPendingBatchesByMemberKey_Result3>();
        //    List<WebCashing_GetPendingBatchesByMemberKey> model = new List<WebCashing_GetPendingBatchesByMemberKey>();
        //    using (TaxiEpayEntities context = new TaxiEpayEntities())
        //    {
        //        if (docketchecksummaryKey == 0)
        //        {
        //            addr = context.WebCashing_GetPendingBatchesByMemberKey(member_key, null, Time).ToList();
        //        }
        //        else
        //        {
        //            addr = context.WebCashing_GetPendingBatchesByMemberKey(member_key, docketchecksummaryKey, Time).ToList();
        //        }
        //        if (addr != null)
        //        {
        //            foreach (var a in addr)
        //            {
        //                WebCashing_GetPendingBatchesByMemberKey m = new WebCashing_GetPendingBatchesByMemberKey();
        //                m.DocketBatchId = a.DocketBatchId;
        //                m.TerminalID = a.TerminalID;
        //                m.BatchNumber = a.BatchNumber;
        //                m.BatchYear = a.BatchYear;
        //                m.FaceValue = a.FaceValue;
        //                m.Commission = a.Commission;
        //                m.Others = a.Others;
        //                m.Total = a.Total;
        //                model.Add(m);
        //            }
        //        }
        //    }

        //    return model;
        //}

        public List<WebCashing_GetPendingBatchesHeaderByMemberKey> GetPendingBatchesDetailByMemberKey(int member_key, int docketchecksummaryKey, int? Time = null)
        {
            List<WebCashing_GetPendingBatchesHeaderByMemberKey> modelResult = new List<WebCashing_GetPendingBatchesHeaderByMemberKey>();

            List<WebCashing_GetPendingBatchesDetailByMemberKey_Result> ResultList = new List<WebCashing_GetPendingBatchesDetailByMemberKey_Result>();
            // List<WebCashing_GetPendingBatchesDetailsByMemberKey> modelResult = new List<WebCashing_GetPendingBatchesDetailsByMemberKey>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                if (docketchecksummaryKey == 0)
                {
                    ResultList = context.WebCashing_GetPendingBatchesDetailByMemberKey(member_key, null, Time).ToList();
                }
                else
                {
                    ResultList = context.WebCashing_GetPendingBatchesDetailByMemberKey(member_key, docketchecksummaryKey, Time).ToList();
                }
                if (ResultList.Count() > 0)
                {
                    int count = 0;
                    foreach (var r in ResultList)
                    {
                        WebCashing_GetPendingBatchesHeaderByMemberKey model1 = new WebCashing_GetPendingBatchesHeaderByMemberKey();
                        model1 = (from n in modelResult
                                  where n.DocketBatchId == r.DocketBatchId
                                  select n).FirstOrDefault();

                        if (model1 == null)
                        {
                            WebCashing_GetPendingBatchesHeaderByMemberKey model = new WebCashing_GetPendingBatchesHeaderByMemberKey();
                            model.Description = r.Description;
                            model.DocketBatchId = r.DocketBatchId;
                            model.TerminalID = r.TerminalID;
                            model.BatchNumber = r.BatchNumber;

                            model.BatchYear = r.BatchYear;
                            model.FaceValue = Math.Round(r.FaceValue.Value, 2);

                            model.Commission = Math.Round(r.Commission.Value, 2);
                            model.Others = Math.Round(r.Others.GetValueOrDefault(0), 2);
                            model.Total = Math.Round(r.Total.GetValueOrDefault(0), 2);
                            model.TotalGlideboxCommision = Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);

                            count = count + 1;
                            model.count = count.ToString();
                            WebCashing_GetPendingBatchesDetailsByMemberKey ChildModel = new WebCashing_GetPendingBatchesDetailsByMemberKey();
                            ChildModel.Description = r.Description;
                            ChildModel.DocketBatchId = r.DocketBatchId;
                            ChildModel.TerminalID = r.TerminalID;
                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.BatchYear = r.BatchYear;
                            ChildModel.Docket_key = r.Docket_key;
                            ChildModel.DocketDate = r.DocketDate;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.GetValueOrDefault(0), 2);
                            ChildModel.Commission = Math.Round(r.Commission.GetValueOrDefault(0), 2);
                            ChildModel.Others = Math.Round(r.Others.GetValueOrDefault(0), 2);
                            ChildModel.Total = Math.Round(r.Total.GetValueOrDefault(0), 2);
                            ChildModel.TotalPayable = Math.Round(r.TotalPayable.GetValueOrDefault(0), 2);
                            ChildModel.PayableFaceValue = r.PayableFaceValue;
                            ChildModel.CommissionRate = r.CommissionRate*100;
                            ChildModel.PayableCommission = r.PayableCommission;
                            ChildModel.PayableOthers = r.PayableOthers;
                            ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);

                            model.WebCashing_GetPendingBatchesDetailsByMemberKey.Add(ChildModel);
                            modelResult.Add(model);

                        }
                        else
                        {
                            modelResult.Remove(model1);
                            WebCashing_GetPendingBatchesDetailsByMemberKey ChildModel = new WebCashing_GetPendingBatchesDetailsByMemberKey();
                            ChildModel.Description = r.Description;
                            ChildModel.DocketBatchId = r.DocketBatchId;
                            ChildModel.TerminalID = r.TerminalID;
                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.BatchYear = r.BatchYear;
                            ChildModel.Docket_key = r.Docket_key;
                            ChildModel.DocketDate = r.DocketDate;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.GetValueOrDefault(0), 2);
                            ChildModel.Commission = Math.Round(r.Commission.GetValueOrDefault(0), 2);
                            ChildModel.Others = Math.Round(r.Others.GetValueOrDefault(0), 2);
                            ChildModel.Total = Math.Round(r.Total.GetValueOrDefault(0), 2);
                            ChildModel.TotalPayable = Math.Round(r.TotalPayable.GetValueOrDefault(0), 2);
                            ChildModel.PayableFaceValue = r.PayableFaceValue;
                            ChildModel.CommissionRate = r.CommissionRate*100;
                            ChildModel.PayableCommission = r.PayableCommission;
                            ChildModel.PayableOthers = r.PayableOthers;
                            ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);

                            model1.WebCashing_GetPendingBatchesDetailsByMemberKey.Add(ChildModel);
                            modelResult.Add(model1);
                        }
                    }
                }
                return modelResult;
            }
        }
    }
}
   
