﻿using LiveEncryption;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.MyWeb;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using  System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using CustomerPortalDataAccess.Entities;
//using WebMatrix.WebData;

namespace Phoenix_BusLog.Data
{
    public class MyWebDA
    {
        public MyWebUserProfileModel GetLoginProfile(int Member_key)
        {
            MyWebUserProfileModel model = new MyWebUserProfileModel { IsActive = false };

            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = context.usermemberships.OrderBy(n => n.UserMembershipKey)
                    .FirstOrDefault(n => n.MemberKey == Member_key);

                if (mem != null)
                {
                    var profile = (from m in context.userprofiles
                                   where m.UserId == mem.UserId && m.Inactive == false
                                   select m).FirstOrDefault();
                    if (profile != null)
                    {
                        model.UserId = profile.UserId;
                        model.UserName = profile.UserName;
                        model.EmailAddress = profile.EmailAddress;
                        model.IsActive = !profile.Inactive;
                        model.IsAdmin = profile.IsAdmin;
                        model.UserMembershipKeySelected = profile.UserMembershipKeySelected;
                        model.UserIdSelected = profile.UserIdSelected;
                        model.UserIdCreatedBy = profile.UserIdCreatedBy;
                        model.UserIdUpdatedBy = profile.UserIdUpdatedBy;
                        model.CreatedDate = profile.CreatedDate;
                        model.UpdatedDate = profile.UpdatedDate;
                        model.APIKey = profile.APIKey;
                    }

                }
            }
            return model;
        }

        public List<int> GetMemberKeysByUserID(int userId, int currentMemberKey)
        {
            using (CustomerPortalDataAccess.Entities.LiveGroupPortalEntities context = new CustomerPortalDataAccess.Entities.LiveGroupPortalEntities())
            {
                List<int> memberKeys = context.usermemberships
                    .Where(a => a.UserId == userId &&
                                a.MemberKey != currentMemberKey)
                    .Select(a => a.MemberKey)
                    .ToList();
                return memberKeys;
            }
        }

        public void RemoveMerchant(int MemberKey, int UserId)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                usermembership merchantMembership = context.usermemberships
                                                    .Where(u => u.MemberKey == MemberKey &&
                                                                u.UserId == UserId)
                                                    .FirstOrDefault();
                if (merchantMembership != null)
                {
                    context.usermemberships.Remove(merchantMembership);
                    context.SaveChanges();
                }
            }
        }

        public usermembership AddMerchant(int MemberKey, int UserId, string username)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                usermembership merchant = context.usermemberships
                                                    .Where(u => u.MemberKey == MemberKey &&
                                                                u.UserId == UserId)
                                                    .FirstOrDefault();
                if (merchant == null)
                {
                    usermembership membership = new usermembership();
                    membership.UserId = UserId;
                    membership.UserIdCreatedBy = 1;
                    membership.UserIdUpdatedBy = 1;
                    membership.CreatedDate = DateTime.Now;
                    membership.Inactive = false;
                    membership.IsAdmin = false;
                    membership.MemberKey = MemberKey;
                    membership.UpdatedDate = DateTime.Now;
                    membership.VerificationToken = Guid.NewGuid().ToString();
                    membership.Verified = true;
                    membership.CreatedByPhoenixUser = username;
                    context.usermemberships.Add(membership);
                    context.SaveChanges();
                }
                return merchant;
            }
        }

        public MyWebUserProfileModel GetUserProfile(string Username)
        {
            MyWebUserProfileModel model = null;

            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {

                var profile = (from m in context.userprofiles
                               where m.UserName == Username && m.Inactive == false
                               select m).FirstOrDefault();
                if (profile != null)
                {
                    model = new MyWebUserProfileModel();
                    model.UserId = profile.UserId;
                    model.UserName = profile.UserName;
                    model.EmailAddress = profile.EmailAddress;
                    model.IsActive = !profile.Inactive;
                    model.IsAdmin = profile.IsAdmin;
                    model.UserMembershipKeySelected = profile.UserMembershipKeySelected;
                    model.UserIdSelected = profile.UserIdSelected;
                    model.UserIdCreatedBy = profile.UserIdCreatedBy;
                    model.UserIdUpdatedBy = profile.UserIdUpdatedBy;
                    model.CreatedDate = profile.CreatedDate;
                    model.UpdatedDate = profile.UpdatedDate;
                    model.APIKey = profile.APIKey;
                }


            }
            return model;
        }
        public string GetUsernameByTerminalId(string TerminalId)
        {
            string username = string.Empty;
            MemberDA tda = new MemberDA();
            int Member_key = tda.GetMemberKeyByTerminalId(TerminalId);

            MyWebUserProfileModel pro = GetLoginProfile(Member_key);
            if (pro != null)
            {
                username = pro.UserName;
            }
            return username;
        }

        public MyWebUserProfileModel GetUserProfileByEmail(string email)
        {
            MyWebUserProfileModel model = null;

            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {

                var profile = (from m in context.userprofiles
                               where m.EmailAddress == email && m.Inactive == false
                               select m).FirstOrDefault();
                if (profile != null)
                {
                    model = new MyWebUserProfileModel();
                    model.UserId = profile.UserId;
                    model.UserName = profile.UserName;
                    model.EmailAddress = profile.EmailAddress;
                    model.IsActive = !profile.Inactive;
                    model.IsAdmin = profile.IsAdmin;
                    model.UserMembershipKeySelected = profile.UserMembershipKeySelected;
                    model.UserIdSelected = profile.UserIdSelected;
                    model.UserIdCreatedBy = profile.UserIdCreatedBy;
                    model.UserIdUpdatedBy = profile.UserIdUpdatedBy;
                    model.CreatedDate = profile.CreatedDate;
                    model.UpdatedDate = profile.UpdatedDate;
                    model.APIKey = profile.APIKey;
                }

            }
            return model;
        }
        public MyWebUserProfileModel GetUserProfileById(int userId)
        {
            MyWebUserProfileModel model = null;

            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {

                var profile = (from m in context.userprofiles
                               where m.UserId == userId && m.Inactive == false
                               select m).FirstOrDefault();
                if (profile != null)
                {
                    model = new MyWebUserProfileModel();
                    model.UserId = profile.UserId;
                    model.UserName = profile.UserName;
                    model.EmailAddress = profile.EmailAddress;
                    model.IsActive = !profile.Inactive;
                    model.IsAdmin = profile.IsAdmin;
                    model.UserMembershipKeySelected = profile.UserMembershipKeySelected;
                    model.UserIdSelected = profile.UserIdSelected;
                    model.UserIdCreatedBy = profile.UserIdCreatedBy;
                    model.UserIdUpdatedBy = profile.UserIdUpdatedBy;
                    model.CreatedDate = profile.CreatedDate;
                    model.UpdatedDate = profile.UpdatedDate;
                    model.APIKey = profile.APIKey;
                }

            }
            return model;
        }

        public void SaveGhostMyWebData(int memberKey, Guid phoenixUserId, int myWebUserId)
        {
            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                var existedMyWebGhost = taxiEpayDbContext.MyWebGhosts.FirstOrDefault(x =>
                    x.PhoenixUserIDGhost == phoenixUserId && x.MyWebUserIDGhost == myWebUserId);

                if (existedMyWebGhost == null) // in case this myweb account wasn't ghost before 
                {
                    var myWebGhost = new MyWebGhost()
                    {
                        PhoenixUserIDGhost = phoenixUserId,
                        MyWebUserIDGhost = myWebUserId,
                        GhostDate = DateTime.Now
                    };

                    taxiEpayDbContext.MyWebGhosts.Add(myWebGhost);

                    //log ghost information. Only log in the first times
                    var ghostHistory = new MyWebGhostHistory()
                    {
                        PhoenixUserIDGhost = phoenixUserId,
                        MyWebUserIDGhost = myWebUserId,
                        MemberKey = memberKey,
                        GhostDate = DateTime.Now
                    };

                    taxiEpayDbContext.MyWebGhostHistories.Add(ghostHistory);
                }
                else //in case this myweb account was ghost before 
                {
                    existedMyWebGhost.GhostDate = DateTime.Now;
                }

                taxiEpayDbContext.SaveChanges();
            }
        }

        public MyWebGhost GetMyWebGhostDiffMyWebUserId(Guid phoenixUserId, int diffUserId)
        {
            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                return taxiEpayDbContext.MyWebGhosts.FirstOrDefault(x =>
                    x.PhoenixUserIDGhost == phoenixUserId && x.MyWebUserIDGhost != diffUserId);
            }
        }

        public MyWebGhostHistory GetMyWebGhostHistory(Guid phoenixUserId, int myWebUserId)
        {
            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                return taxiEpayDbContext.MyWebGhostHistories
                    .Include(x => x.Member)
                    .FirstOrDefault(x => x.PhoenixUserIDGhost == phoenixUserId && x.MyWebUserIDGhost == myWebUserId);
            }
        }

        public bool DoesUserNameExists(string Username)
        {
            bool model = false;

            using (CustomerPortalDataAccess.Entities.LiveGroupPortalEntities context = new CustomerPortalDataAccess.Entities.LiveGroupPortalEntities())
            {
                var mem = (from m in context.userprofiles
                           where m.UserName == Username && m.Inactive == false
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    model = true;
                }
            }
            return model;
        }

        public bool DoesUserEmailExists(string Email)
        {
            bool model = false;

            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from m in context.userprofiles
                           where m.EmailAddress == Email && m.Inactive == false
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    model = true;
                }
            }
            return model;
        }
        public string GetAPIkey(string Username)
        {
            string APIKey = string.Empty;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from m in context.userprofiles
                           where m.UserName == Username && m.Inactive == false
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    APIKey = mem.APIKey;
                }
            }
            return APIKey;
        }

        public Guid GetApplicationId(int App_id)
        {
            Guid APIKey = new Guid();
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from m in context.esapps
                           where m.Id == App_id
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    APIKey = Guid.Parse(mem.AppId);
                }
            }
            return APIKey;
        }

        public string GetApplicationPublicKey(int Appid)
        {
            string APIKey = string.Empty;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from m in context.esapps
                           where m.Id == Appid
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    APIKey = mem.SecretKey.Substring(0, 24);
                }
            }
            return APIKey;
        }
        public esAppModel GetesAppModel(int Appid)
        {
            esAppModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from m in context.esapps
                           where m.Id == Appid
                           select m).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppModel();
                    model.AppDescription = mem.AppDescription;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.AppName = mem.AppName;
                    model.AppURL = mem.AppURL;
                    model.ExpiryTime = mem.ExpiryTime;
                    model.Id = mem.Id;
                    model.SecretKey = mem.SecretKey;
                }
            }
            return model;
        }


        public esAppTokenModel GetesAppTokenModel(Guid Appid, Guid token)
        {
            esAppTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esapptokens
                           where a.Token == token.ToString() && a.AppId == Appid.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.CreateDate = mem.CreateDate;
                    model.esAppsID = mem.esAppsID;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                    model.UserId = mem.UserId;
                    model.Username = mem.Username;
                }
            }
            return model;
        }
        public esAppTokenModel GetesAppTokenModelByUssername(Guid Appid, string Username)
        {
            esAppTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esapptokens
                           where a.Username == Username && a.AppId == Appid.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.CreateDate = mem.CreateDate;
                    model.esAppsID = mem.esAppsID;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                    model.UserId = mem.UserId;
                    model.Username = mem.Username;
                }
            }
            return model;
        }
        public esAppTokenModel GetesAppTokenModelByToken(Guid token)
        {
            esAppTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esapptokens
                           where a.Token == token.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.CreateDate = mem.CreateDate;
                    model.esAppsID = mem.esAppsID;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                    model.UserId = mem.UserId;
                    model.Username = mem.Username;
                }
            }
            return model;
        }

        public esAppUserTokenModel GetesAppUserTokenModelByToken(Guid token)
        {
            esAppUserTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esappusertokens
                           where a.Token == token.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppUserTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.Id = mem.Id;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);

                }
            }
            return model;
        }

        /// <summary>
        /// token for app1 to get the usename, Appid for app2, 
        /// </summary>
        /// <param name="Appid"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public esAppTokenModel CkeckIfThisTokenRecordExists(Guid Appid, Guid token)
        {
            esAppTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem1 = (from a in context.esapptokens
                            where a.Token == token.ToString()
                            select a).FirstOrDefault();

                var mem = (from a in context.esapptokens
                           where a.Username == mem1.Username && a.AppId == Appid.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.CreateDate = mem.CreateDate;
                    model.esAppsID = mem.esAppsID;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                    model.UserId = mem.UserId;
                    model.Username = mem.Username;
                }
            }
            return model;
        }


        public bool IsUserEncyptedTokenValid(string Encyptedtoken, string Username, int AppId)
        {
            MyWebUserProfileModel UserPro = GetUserProfile(Username);
            Guid AppKey = GetApplicationId(AppId);
            esAppTokenModel TokenModel = GetesAppTokenModelByUssername(AppKey, Username);
            string TokenStringToEnc = Username + TokenModel.Token;
            string EncyptedtokenToCheck = LiveEncryptionHelper.TripleDESEncypt(TokenStringToEnc, UserPro.APIKey.Substring(0, 24));
            bool IsTokenValid = CheckTokenExpiry(TokenModel.Token.GetValueOrDefault(new Guid()));

            if (Encyptedtoken == EncyptedtokenToCheck && IsTokenValid)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public esAppTokenModel GetesAppTokenModel(Guid Appid, string username)
        {
            esAppTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esapptokens
                           where a.Username == username && a.AppId == Appid.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model = new esAppTokenModel();
                    model.AppTokenId = mem.AppTokenId;
                    model.AppId = Guid.Parse(mem.AppId);
                    model.CreateDate = mem.CreateDate;
                    model.esAppsID = mem.esAppsID;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                    model.UserId = mem.UserId;
                    model.Username = mem.Username;
                }
            }
            return model;
        }
        public esAppUserTokenModel GetesAppUserTokenModel(Guid Token)
        {
            esAppUserTokenModel model = null;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esappusertokens
                           where a.Token == Token.ToString()
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    model.AppTokenId = mem.AppTokenId;
                    model.LastVisit = mem.LastVisit;
                    model.Token = Guid.Parse(mem.Token);
                }
            }
            return model;
        }
        public void UpdateesAppUserTokenModel(esAppUserTokenModel model)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esappusertokens
                           where a.AppTokenId == model.AppTokenId
                           select a).FirstOrDefault();

                if (mem != null)
                {

                    mem.LastVisit = model.LastVisit;
                    mem.Token = model.Token.ToString();
                    context.SaveChanges();
                }
            }

        }
        public void DeleteesAppUserTokenModel(esAppUserTokenModel model)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esappusertokens
                           where a.Id == model.Id
                           select a).FirstOrDefault();

                if (mem != null)
                {
                    context.esappusertokens.Remove(mem);
                    context.SaveChanges();
                }
            }

        }

        public void UpdateesAppTokenModel(esAppTokenModel model)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                var mem = (from a in context.esapptokens
                           where a.AppTokenId == model.AppTokenId
                           select a).FirstOrDefault();

                if (mem != null)
                {

                    mem.LastVisit = model.LastVisit;
                    mem.Token = model.Token?.ToString();
                    context.SaveChanges();
                }
            }

        }
        public int AddAppToken(esAppTokenModel model)
        {
            int key = 0;
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                esapptoken tmodel = new esapptoken();
                tmodel.AppId = model.AppId.ToString();
                tmodel.CreateDate = model.CreateDate;
                tmodel.esAppsID = model.esAppsID;
                tmodel.LastVisit = model.LastVisit;
                tmodel.Token = model.Token.ToString();
                tmodel.UserId = model.UserId;
                tmodel.Username = model.Username;
                context.esapptokens.Add(tmodel);
                context.SaveChanges();
                key = tmodel.AppTokenId;
                return key;
            }
        }

        public void AddAppUserToken(esAppUserTokenModel model)
        {
            using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                esappusertoken tmodel = new esappusertoken();
                tmodel.AppTokenId = model.AppTokenId;
                tmodel.Token = model.Token.ToString();
                tmodel.LastVisit = model.LastVisit;

                context.esappusertokens.Add(tmodel);
                context.SaveChanges();
            }
        }

        public bool CheckAppUserTokenExpiry(Guid Token, double ExpiryTime)
        {
            bool IsValid = false;
            DateTime ExpiryDate = DateTime.Now;
            esAppUserTokenModel tok = GetesAppUserTokenModelByToken(Token);

            if (tok != null)
            {
                if (tok.LastVisit.Value.AddMinutes(ExpiryTime) < ExpiryDate)
                {
                    DeleteesAppUserTokenModel(tok);
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            return IsValid;
        }


        public bool AddNewToken(Guid Token, int AppId)
        {
            bool IsAdded = false;
            MyWebDA myDa = new MyWebDA();
            Guid GAppid = myDa.GetApplicationId(AppId);

            esAppTokenModel apptoken = new esAppTokenModel();
            apptoken = myDa.CkeckIfThisTokenRecordExists(GAppid, Token);

            if (apptoken != null)
            {
                apptoken.Token = Token;
                apptoken.LastVisit = DateTime.Now;
                myDa.UpdateesAppTokenModel(apptoken);
                IsAdded = true;
            }

            return IsAdded;
        }

        public Guid CreateUserToken(string Username, int AppId, int UserId)
        {
            Guid TokenGuid = new Guid();


            esAppModel app = GetesAppModel(AppId);

            Guid GAPIKey = GetApplicationId(Convert.ToInt32(AppId));
            esAppTokenModel userToken = GetesAppTokenModel(GAPIKey, Username);

            if (userToken == null)
            {
                esAppTokenModel asApptok = new esAppTokenModel();
                asApptok.Token = Guid.NewGuid();
                asApptok.AppId = app.AppId;
                asApptok.CreateDate = DateTime.Now;
                asApptok.UserId = UserId;
                asApptok.Username = Username;
                asApptok.LastVisit = DateTime.Now;
                AddAppToken(asApptok);
                TokenGuid = asApptok.Token.Value;
            }
            else
            {
                userToken.Token = Guid.NewGuid();
                userToken.LastVisit = DateTime.Now;
                TokenGuid = userToken.Token.Value;
                UpdateesAppTokenModel(userToken);
            }

            return TokenGuid;
        }
        public Guid CreateUserTokenForGlide(string Username, int AppId, int UserId)
        {
            Guid TokenGuid = new Guid();

            esAppModel app = GetesAppModel(AppId);

            Guid GAPIKey = GetApplicationId(Convert.ToInt32(AppId));
            esAppTokenModel userToken = GetesAppTokenModel(GAPIKey, Username);
            //esAppUserTokenModel userAppToken = GetesAppUserTokenModel(GAPIKey, Username);

            if (userToken == null)
            {
                esAppTokenModel asApptok = new esAppTokenModel();
                asApptok.Token = Guid.NewGuid();
                asApptok.AppId = app.AppId;
                asApptok.CreateDate = DateTime.Now;
                asApptok.UserId = UserId;
                asApptok.Username = Username;
                asApptok.LastVisit = DateTime.Now;
                int AppTokenId = AddAppToken(asApptok);
                TokenGuid = asApptok.Token.Value;

                esAppUserTokenModel asAppUsertok = new esAppUserTokenModel();
                asAppUsertok.Token = asApptok.Token;
                asAppUsertok.AppTokenId = AppTokenId;
                asAppUsertok.LastVisit = DateTime.Now;

                AddAppUserToken(asAppUsertok);
            }
            else
            {
                esAppUserTokenModel asAppUsertok = new esAppUserTokenModel();
                asAppUsertok.Token = Guid.NewGuid();
                asAppUsertok.AppTokenId = userToken.AppTokenId;
                asAppUsertok.LastVisit = DateTime.Now;

                AddAppUserToken(asAppUsertok);
                TokenGuid = asAppUsertok.Token.Value;
            }

            return TokenGuid;
        }


        public bool CheckTokenExpiry(Guid Token)
        {
            bool IsValid = false;
            DateTime ExpiryDate = DateTime.Now;
            esAppTokenModel tok = GetesAppTokenModelByToken(Token);

            if (tok != null)
            {
                if (tok.LastVisit < ExpiryDate)
                {

                    tok.Token = Guid.Empty;
                    UpdateesAppTokenModel(tok);
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            return IsValid;
        }

        public List<MemberModelSimple> GetAvailableSubAccount(List<MemberModelSimple> memberList, int currentUserId)
        {
            using ( LiveGroupPortalEntities context = new LiveGroupPortalEntities())
            {
                List<int> memberKeys = memberList.Select(m => m.Member_key).ToList();
                List<int> unavailableMemberKeys = context.usermemberships
                    .Where(um => memberKeys.Contains(um.MemberKey) && um.UserId == currentUserId)
                    .Select(um => um.MemberKey)
                    .ToList();
                memberList = memberList.Where(m => !unavailableMemberKeys.Contains(m.Member_key)).ToList();
            }
            return memberList;
        }
    }
}
