﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class QantasFileEntryStatusDA
    {
        public List<QantasExportFileEntryStatu> GetStatusesByName(string[] names)
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.QantasExportFileEntryStatus.Where(s => names.Contains(s.Name)).ToList();
            }
        }

        public List<QantasExportFileEntryStatu> GetAllActive()
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.QantasExportFileEntryStatus.Where(s => s.IsActive).ToList();
            }
        }
    }
}
