﻿using Phoenix_Service;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class TerminalSalesDA
    {
       public int AddNew(TerminalSalesModel t)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               TerminalSale mServer = new TerminalSale();
               mServer.TerminalAllocation_key = t.TerminalAllocation_key;
               mServer.Employee_key = t.Employee_key;
               mServer.Modified_dttm = t.Modified_dttm;
               mServer.Active = t.Active;
               //mServer.ModifiedByUser = t.ModifiedByUser;
               mServer.Modified_dttm = t.Modified_dttm;
               mServer.SaleRelationship_key = t.SaleRelationship_key;
               mServer.ModifiedByUser = t.ModifiedByUser;

               context.TerminalSales.Add(mServer);
               context.SaveChanges();
               return mServer.TerminalSale_key;

            }

       }
    }
}
