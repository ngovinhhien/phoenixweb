﻿using Phoenix_Service;
using PhoenixObjects.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class EFTDocketDA
    {
        public void UpdateOperator(DateTime AllocatedDate, string TerminalId, int Operator_key, int Vehical_key, int EftTerminal_key)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = from s in context.CadmusINs
                         where s.terminalid == TerminalId && s.transstart >= AllocatedDate
                         select s;
                if (sk != null)
                {
                    foreach (var k in sk)
                    {
                        var skm = from s in context.EftDockets
                                  where s.EftDocket_key == k.docket_key && s.Operator_key == null && s.Vehicle_key == null
                                  select s;
                    }
                }
            }
        }

        public void AllocateTransactionsToMember(int EftDocket_key, int EftTerminal_key, int Operator_key, int Vehicle_key)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.EftDockets
                            where m.EftDocket_key == EftDocket_key
                            select m).FirstOrDefault();
                if (addr != null)
                {
                    addr.EftTerminal_key = EftTerminal_key;
                    addr.Operator_key = Operator_key;
                    addr.Vehicle_key = Vehicle_key;
                }
                context.SaveChanges();
            }

        }

        /// <summary>
        /// Gets the first unallocated date from a list of unallocated transaction for a terminal
        /// </summary>
        /// <param name="TerminalId">terminalId</param>
        /// <returns>date of the first unallocated transaction</returns>
        public string GetFirstUnallocatedDate(string TerminalId)
        {
            string FirstUnallocated = DateTime.Now.ToString("s");
            DateTime BackDate = DateTime.Now.AddDays(-7);
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.EftDockets
                            join c in context.CadmusINs on m.EftDocket_key equals c.docket_key
                            where c.terminalid == TerminalId && m.Operator_key == null && m.Vehicle_key == null && m.TripStart >=BackDate
                            orderby c.transstart
                            select m).FirstOrDefault();
                if (addr != null)
                {
                    FirstUnallocated = addr.TripStart.Value.ToString("s");
                }
            }
            return FirstUnallocated;
        }

        public List<EFTDocketModel> GetUnallocatedTransactionFromSpecificDate(string TerminalId, DateTime AllocatedDate)
        {
            List<EFTDocketModel> model = new List<EFTDocketModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.EftDockets
                           join c in context.CadmusINs on m.EftDocket_key equals c.docket_key
                           where c.terminalid == TerminalId && c.transstart >= AllocatedDate && m.Operator_key == null && m.Vehicle_key == null
                           orderby c.transstart
                           select m;
                if (addr != null)
                {
                    foreach (var a in addr)
                    {
                        EFTDocketModel DocketModel = new EFTDocketModel();
                        DocketModel.Authorisation = a.Authorisation;
                        DocketModel.Authorisation = a.Authorisation;
                        DocketModel.BatchId = a.BatchId;
                        DocketModel.TripStart = a.TripStart;
                        DocketModel.TripEnd = a.TripEnd;
                        DocketModel.Fare = a.Fare;
                        DocketModel.Extras = a.Extras;
                        DocketModel.Commission = a.Commission;
                        DocketModel.Passengers = a.Passengers;
                        DocketModel.Tarrif = a.Tarrif;
                        DocketModel.Distance = a.Distance;
                        DocketModel.PickerZone = a.PickerZone;
                        DocketModel.DropoffZone = a.DropoffZone;
                        DocketModel.CadmusFilename = a.CadmusFilename;
                        DocketModel.Status = a.Status;
                        DocketModel.BatchNumber = a.BatchNumber;
                        DocketModel.TransactionNumber = a.TransactionNumber;
                        DocketModel.Presented = a.Presented;
                        DocketModel.Modified_dttm = a.Modified_dttm;
                        DocketModel.ModifiedByUser = a.ModifiedByUser;
                        DocketModel.EftDocket_key = a.EftDocket_key;
                        DocketModel.EftTerminal_key = a.EftTerminal_key;
                        DocketModel.Operator_key = a.Operator_key;
                        DocketModel.Quarantined = a.Quarantined;
                        DocketModel.Vehicle_key = a.Vehicle_key;
                        DocketModel.terminalid = TerminalId;
                        model.Add(DocketModel);
                    }
                }

            }

            return model;

        }
    }
}
