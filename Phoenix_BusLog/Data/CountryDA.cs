﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class CountryDA
    {

        public List<CountryModel> Get()
        {
            List<CountryModel> countryList = new List<CountryModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = from si in context.Countries
                         select si;
                if (sk != null)
                {
                    foreach (var skk in sk)
                    {
                        var can = new CountryModel();
                        can.CountryKey = skk.CountryKey;
                        can.CountryName = skk.CountryName;
                        countryList.Add(can);
                    }
                }

            }
            return countryList;
        }
        public CountryModel Get(int id)
        {
            CountryModel country = new CountryModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = from si in context.Countries
                         where si.CountryKey == id
                         select si;
                if (sk != null)
                {
                    foreach (var skk in sk)
                    {
                        country.CountryKey = skk.CountryKey;
                        country.CountryName = skk.CountryName;
                    }
                }
            }
            return country;
        }
        public int GetAustraliaKey()
        {
            int countrykey = 1;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = (from si in context.Countries
                         where si.CountryName == "Australia" || si.CountryName == "AUSTRALIA"
                         select si).FirstOrDefault();
                if (sk != null)
                {
                    countrykey = sk.CountryKey;
                }
            }
            return countrykey;
        }
    }


}

