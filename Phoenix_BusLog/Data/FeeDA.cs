﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Enums;
using PhoenixObjects.Fee;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix_BusLog.Data
{
    public class FeeDA
    {
        public List<Fee> GetFees()
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.Fees.ToList();
            }
        }

        public List<FeeModel> Get(int memberKey)
        {
            List<FeeModel> result = new List<FeeModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var fees = from m in context.FeeChargeDetails
                           where m.MemberKey == memberKey
                           select m;

                if (fees.Count() > 0)
                {
                    foreach (var t in fees)
                    {
                        FeeModel fee = new FeeModel();
                        fee.Name = t.Fee.Name;
                        fee.Amount = t.Amount;
                        fee.ID = t.ID;
                        fee.FeeID = t.FeeID;
                        fee.IsEnable = t.IsActive;
                        fee.Description = t.Fee.Description;
                        fee.FrequencyID = t.FrequencyTypeID;
                        fee.FrequencyName = t.FrequencyType.Name;
                        fee.FeeChargeStartDate = t.FeeChargeStartDate;
                        fee.FeeChargeEndDate = t.FeeChargeEndDate;
                        fee.MemberKey = t.MemberKey;
                        result.Add(fee);
                    }
                }
            }
            return result;
        }

        public void CreateFeeForMember(TaxiEpayEntities dbContext, string user, MemberFeeModel model)
        {
            FeeChargeDetail feeDetail = dbContext.FeeChargeDetails
                            .Where(m => m.MemberKey == model.MemberKey
                                    && m.FeeID == model.FeeTypeID
                                    && m.IsDeleted == false)
                            .SingleOrDefault();

            if (feeDetail != null)
            {
                throw new Exception("This fee has already existed");
            }

            var terminalallocated = dbContext.vTerminalAllocations.Any(n => n.IsCurrent && n.Member_key == model.MemberKey && n.EftTerminal_key != null && !n.VirtualTerminal);
            if (!terminalallocated)
            {
                throw new Exception("The merchant must have at least a terminal allocation before adding this fee.");
            }

            FeeChargeDetail newFeeDetail = new FeeChargeDetail
            {
                FeeID = model.FeeTypeID,
                Amount = model.Amount,
                CreatedDate = DateTime.Now.Date,
                CreatedBy = user,
                IsActive = true,
                MemberKey = model.MemberKey,
                FrequencyTypeID = model.FrequencyID,
                FeeChargeStartDate = DateTime.Now//model.StartDate,

            };

            if (model.EndDate == DateTime.MinValue)
            {
                newFeeDetail.FeeChargeEndDate = null;
            }
            else
            {
                newFeeDetail.FeeChargeEndDate = model.EndDate;
            }

            dbContext.FeeChargeDetails.Add(newFeeDetail);

            var activeHistory = new AccountFeeChangeHistory()
            {
                CreatedBy = user,
                CreatedDate = DateTime.Now,
                IsActive = true,
                Reason = "",
                TicketNumber = "",
            };

            var newAmountHistory = new AccountFeeChangeHistory()
            {
                CreatedBy = user,
                CreatedDate = DateTime.Now,
                Reason = "",
                TicketNumber = "",
                NewAmount = model.Amount,
            };
            newFeeDetail.AccountFeeChangeHistories.Add(activeHistory);
            newFeeDetail.AccountFeeChangeHistories.Add(newAmountHistory);

            dbContext.SaveChanges();
        }

        public void UpdateFeeForMember(TaxiEpayEntities dbContext, string user, MemberFeeModel model)
        {
            FeeChargeDetail feeDetail = dbContext.FeeChargeDetails
                            .Where(m => m.MemberKey == model.MemberKey
                                    && m.FeeID == model.FeeTypeID)
                            .SingleOrDefault();
            if (feeDetail == null)
            {
                throw new Exception("This fee is not existed");
            }
            feeDetail.Amount = model.Amount;
            feeDetail.IsActive = model.IsActive;
            dbContext.SaveChanges();
        }

        public List<MemberFeeChangeHistory> GetFeeChangeHistoryForMember(int memberKey)
        {
            List<MemberFeeChangeHistory> histories = new List<MemberFeeChangeHistory>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                histories = context.AccountFeeChangeHistories
                            .Where(m => m.FeeChargeDetail.MemberKey == memberKey)
                            .Select(m => new MemberFeeChangeHistory
                            {
                                ID = m.ID,
                                ProcessedDate = m.CreatedDate.Value,
                                Reason = m.Reason,
                                Username = m.CreatedBy
                            })
                            .ToList();
            }
            return histories;
        }

        public void UpdateFeeDetailForMultilpleMembers(TaxiEpayEntities taxiEpayDbContext, int feeTypeId, decimal amount, int member_key, string UpdatedUser)
        {
            var memberFees = taxiEpayDbContext.FeeChargeDetails.Where(m => m.MemberKey == member_key).ToList();
            if (!(memberFees.Any(m => m.FeeID == feeTypeId)))
            {
                var feeDetail = new FeeChargeDetail();
                feeDetail.FeeID = feeTypeId;
                feeDetail.Amount = amount;
                feeDetail.IsActive = true;
                feeDetail.CreatedBy = UpdatedUser;
                feeDetail.CreatedDate = DateTime.Now;
                feeDetail.MemberKey = member_key;
                feeDetail.FrequencyTypeID = 1;//feeDto.FrequencyID;
                feeDetail.FeeChargeStartDate = DateTime.Now;
                feeDetail.FeeChargeEndDate = null;
                taxiEpayDbContext.FeeChargeDetails.Add(feeDetail);

                var activeHistory = new AccountFeeChangeHistory()
                {
                    CreatedBy = UpdatedUser,
                    CreatedDate = DateTime.Now,
                    IsActive = true,
                    Reason = "",
                    TicketNumber = "",
                };

                var newAmountHistory = new AccountFeeChangeHistory()
                {
                    CreatedBy = UpdatedUser,
                    CreatedDate = DateTime.Now,
                    Reason = "",
                    TicketNumber = "",
                    NewAmount = amount,
                };

                feeDetail.AccountFeeChangeHistories.Add(activeHistory);
                feeDetail.AccountFeeChangeHistories.Add(newAmountHistory);

                taxiEpayDbContext.SaveChanges();
            }
        }

        public void UpdateFeeDetailToMember(TaxiEpayEntities taxiEpayDbContext, List<FeeModel> feeDtoList, Member member, string UpdatedUser)
        {
            var memberFees = taxiEpayDbContext.FeeChargeDetails.Where(m => m.MemberKey == member.Member_key).ToList();

            foreach (var feeDto in feeDtoList)
            {
                if (memberFees.Any(m => m.ID == feeDto.ID))
                {
                    var fee = memberFees.Where(m => m.ID == feeDto.ID).SingleOrDefault();
                    fee.FeeID = feeDto.FeeID;
                    fee.Amount = feeDto.Amount;
                    fee.IsActive = feeDto.IsEnable;
                    fee.FrequencyTypeID = 1;//feeDto.FrequencyID;
                    fee.FeeChargeStartDate = feeDto.FeeChargeStartDate;
                    fee.FeeChargeEndDate = feeDto.FeeChargeEndDate;
                    taxiEpayDbContext.SaveChanges();
                }
                else
                {
                    var feeDetail = new FeeChargeDetail();
                    feeDetail.FeeID = feeDto.FeeID;
                    feeDetail.Amount = feeDto.Amount;
                    feeDetail.IsActive = feeDto.IsEnable;
                    feeDetail.CreatedBy = UpdatedUser;
                    feeDetail.CreatedDate = DateTime.Now;
                    feeDetail.MemberKey = member.Member_key;
                    feeDetail.FrequencyTypeID = 1;//feeDto.FrequencyID;
                    feeDetail.FeeChargeStartDate = feeDto.FeeChargeStartDate;
                    feeDetail.FeeChargeEndDate = feeDto.FeeChargeEndDate;
                    taxiEpayDbContext.FeeChargeDetails.Add(feeDetail);
                    taxiEpayDbContext.SaveChanges();
                }
            }
        }

        public FeeChargeDetail GetFeeChargeDetailByIDAndMemberID(TaxiEpayEntities taxiEpayDbContext, int feeChargeDetailID,
            int memberKey)
        {
            return taxiEpayDbContext.FeeChargeDetails.FirstOrDefault(n =>
                n.ID == feeChargeDetailID && n.MemberKey == memberKey);
        }

        public void DeactivateFeeChargeDetail(int feeChargeDetailID, string modifiedByUser, string reason, string ticketNumber, decimal oldAmount)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var feeChargeDetail = entities.FeeChargeDetails.FirstOrDefault(n => n.ID == feeChargeDetailID);
                if (feeChargeDetail != null)
                {
                    feeChargeDetail.IsActive = false;

                    var activeHistory = new AccountFeeChangeHistory()
                    {
                        CreatedBy = modifiedByUser,
                        CreatedDate = DateTime.Now,
                        IsActive = false,
                        OldAmount = oldAmount,
                        Reason = reason,
                        TicketNumber = ticketNumber,
                        FeeChargeDetailId = feeChargeDetailID
                    };
                    feeChargeDetail.AccountFeeChangeHistories.Add(activeHistory);
                    entities.SaveChanges();
                }
            }
        }


        public void UpdateFeeChargeDetail(int feeChargeDetailID, string modifiedByUser, string reason, string ticketNumber, decimal newAmount, bool isActive, bool recordEnableHistory = true)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var feeChargeDetail = entities.FeeChargeDetails.FirstOrDefault(n => n.ID == feeChargeDetailID);
                if (feeChargeDetail != null)
                {
                    var oldAmount = feeChargeDetail.Amount;
                    feeChargeDetail.Amount = newAmount;
                    feeChargeDetail.IsActive = isActive;

                    if (recordEnableHistory)
                    {
                        var activeHistory = new AccountFeeChangeHistory()
                        {
                            CreatedBy = modifiedByUser,
                            CreatedDate = DateTime.Now,
                            IsActive = isActive,
                            OldAmount = oldAmount,
                            Reason = reason,
                            TicketNumber = ticketNumber,
                        };
                        feeChargeDetail.AccountFeeChangeHistories.Add(activeHistory);
                    }

                    if (oldAmount != newAmount)
                    {
                        var amountChanged = new AccountFeeChangeHistory()
                        {
                            CreatedBy = modifiedByUser,
                            CreatedDate = DateTime.Now,
                            OldAmount = oldAmount,
                            NewAmount = newAmount,
                            Reason = reason,
                            TicketNumber = ticketNumber,
                            FeeChargeDetailId = feeChargeDetailID
                        };
                        feeChargeDetail.AccountFeeChangeHistories.Add(amountChanged);
                    }

                    entities.SaveChanges();
                }
            }
        }

        public void RefundFeeForMember(TaxiEpayEntities dbContext, string user, MemberRefundFeeModel model)
        {
            DateTime startOfPreviousMonth = DateTime.Now.AddMonths(-1);
            DocketTable docket = new DocketTable
            {
                BatchId = startOfPreviousMonth.ToString("yyyy") +
                          startOfPreviousMonth.ToString("MM") +
                          model.MemberKey +
                          DateTime.Now.ToString("MM") +
                          DateTime.Now.ToString("dd"),
                DocketDate = DateTime.Now.Date,
                DocketPaymentStatus = 0,
                FaceValue = model.Amount,
                DocketTotal = model.Amount,
                Office_key = 51,
                ModifiedByUser = user,
                Modified_dttm = DateTime.Now
            };
            dbContext.DocketTables.Add(docket);
            dbContext.SaveChanges();

            PendingTransactionDetail tran = new PendingTransactionDetail
            {
                ItemDate = DateTime.Now,
                RateUsed = 1,
                Amount = model.Amount,
                TransactionDetailType = 20,
                Description = "Refunded Account Fee",
                ModifiedByUser = user,
                Modified_dttm = DateTime.Now,
                Docket_key = docket.Docket_key,
                Member_key = model.MemberKey
            };

            FeeChargedRefund newRefund = new FeeChargedRefund
            {
                FeeChargeHistoryID = model.FeeChargeHistoryID,
                Reason = model.Reason,
                ProcessedDate = DateTime.Now,
                RefundAmount = model.Amount,
                TicketNumber = model.TicketNumber,
                CreatedDate = DateTime.Now.Date,
                CreatedBy = user,
                UpdatedBy = user,
                UpdatedDate = DateTime.Now.Date,
                DocketKey = docket.Docket_key,
            };

            dbContext.PendingTransactionDetails.Add(tran);
            dbContext.FeeChargedRefunds.Add(newRefund);
            dbContext.SaveChanges();
        }
    }
}
