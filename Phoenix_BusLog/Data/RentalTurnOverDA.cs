﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.Common;

namespace Phoenix_BusLog.Data
{
   public class RentalTurnOverDA
    {
       public List<RentalTurnOverModel> Get()
       {
           List<RentalTurnOverModel> businessTypeList = new List<RentalTurnOverModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = from si in context.RentalTurnoverRanges
                        select si;
               if (sk != null)
               {
                   foreach (var skk in sk)
                   {
                       var can = new RentalTurnOverModel();
                       can.LowAmount = skk.LowAmount;
                       can.HighAmount = skk.HighAmount;
                       can.RentalTurnoverRangeKey = skk.RentalTurnoverRangeKey;
                       can.RangeMsg = string.Format("LiveEftPos rental rate turnover from {0} to {1}", skk.LowAmount, skk.HighAmount);
                       businessTypeList.Add(can);
                   }
               }
           }
           return businessTypeList;
       }
       public int GetTurnOverRangeKey(decimal amt)
       {
           int turnOverRangeKey = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = (from si in context.RentalTurnoverRanges
                         where si.LowAmount <= amt && si.HighAmount >= amt
                         select si).FirstOrDefault();
               if (sk != null)
               {

                   turnOverRangeKey = sk.RentalTurnoverRangeKey;

               }
           }

           return turnOverRangeKey;
       }
       
    }
}
