﻿using Phoenix_Service;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class UserLoginHistoryDA
    {
       public void Add(UserLoginHistoryModel t)
       {
           using (aspnetdbEntities context = new aspnetdbEntities())
           {
               UserLoginHistory aSever = new UserLoginHistory();
               aSever.ApplicationName = t.ApplicationName;
               aSever.AttemptDate = t.AttemptDate;
               aSever.IPAddress = t.IPAddress;
               aSever.UserId = t.UserId;
               aSever.UserName = t.UserName;
               aSever.Status = t.Status;
               aSever.AttempDateTime = t.AttempDateTime;
               context.UserLoginHistories.Add(aSever);
               context.SaveChanges();
           }
       }
    }
}
