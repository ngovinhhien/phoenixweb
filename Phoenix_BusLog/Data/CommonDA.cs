﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class CommonDA
    {
        public int GetDocketYear(string DocketMonth, string DocketDay)
        {
            int year = DateTime.Now.Year;
            int monthToday = DateTime.Now.Month;
            int dayToday = DateTime.Now.Day;
            int DocMonthInt = Convert.ToInt32(DocketMonth);
            int DocDayInt = Convert.ToInt32(DocketDay);
            if (DocMonthInt > monthToday)
            {
                year = DateTime.Now.Year - 1;
            }
            else if (DocMonthInt == monthToday)
            {
                if (DocDayInt > dayToday)
                {
                    year = DateTime.Now.Year - 1;
                }
            }
            return year;
        }

    }
}
