﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class MemberRentalTemplateDA
    {
        public List<MemberRentalTemplateModel> Get()
        {
            List<MemberRentalTemplateModel> MemberRentalTemplateList = new List<MemberRentalTemplateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = from m in context.MemberRentalTemplates
                          select m;
                if (mem != null)
                {
                    foreach (var m in mem)
                    {
                        MemberRentalTemplateModel memberRentalTemplate = new MemberRentalTemplateModel();
                        memberRentalTemplate.MemberRentalTemplateKey = m.MemberRentalTemplateKey;
                        memberRentalTemplate.Description = m.Description;
                        memberRentalTemplate.Comment = m.Comment;
                        memberRentalTemplate.Modified_dttm = m.Modified_dttm;
                        memberRentalTemplate.ModifiedByUser = m.ModifiedByUser;
                        MemberRentalTemplateList.Add(memberRentalTemplate);
                    }
                }
            }
            return MemberRentalTemplateList;
        }

        public List<MemberRentalTemplateModel> GetByCompanyKey(int company_key)
        {
            List<MemberRentalTemplateModel> MemberRentalTemplateList = new List<MemberRentalTemplateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = from m in context.MemberRentalTemplates
                          where m.Company_key == company_key
                          select m;
                if (mem != null && mem.Count()>0)
                {
                    foreach (var m in mem)
                    {
                        MemberRentalTemplateModel memberRentalTemplate = new MemberRentalTemplateModel();
                        memberRentalTemplate.MemberRentalTemplateKey = m.MemberRentalTemplateKey;
                        memberRentalTemplate.Description = m.Description;
                        memberRentalTemplate.Comment = m.Comment;
                        memberRentalTemplate.Modified_dttm = m.Modified_dttm;
                        memberRentalTemplate.ModifiedByUser = m.ModifiedByUser;
                        MemberRentalTemplateList.Add(memberRentalTemplate);
                    }
                }
            }
            return MemberRentalTemplateList;
        }
        public int GetKeyByName(string MemberRentalTemplateDescription)
        {
            int MemberRentalTemplateKey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MemberRentalTemplates
                          where m.Description == MemberRentalTemplateDescription
                          select m).FirstOrDefault();
                if (mem != null)
                {
                    MemberRentalTemplateKey = mem.MemberRentalTemplateKey;
                }
            }
            return MemberRentalTemplateKey;
        }
    }
}
