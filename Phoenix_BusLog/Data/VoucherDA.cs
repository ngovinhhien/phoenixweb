﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Vouchers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class VoucherDA
    {

        public List<VoucherTypeModel> GetVoucherType_ByCompanyOffice(int CompanyKey, int OfficeKey)
        {
            List<VoucherTypeModel> VoucherType_Result = new List<VoucherTypeModel>();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var VoucherType = con.SPGetVoucherTypeByCompany(CompanyKey, OfficeKey).ToList();

                if (VoucherType != null)
                {
                    foreach (var v in VoucherType)
                    {
                        VoucherTypeModel vmodel = new VoucherTypeModel();
                        vmodel.Company_key = v.Company_key;
                        vmodel.ConversionValue = v.ConversionValue;
                        vmodel.ExpiryPeriod = v.ExpiryPeriod;
                        vmodel.nextnumber = v.nextnumber;
                        vmodel.UsedLater = v.UsedLater;
                        vmodel.vouchername = v.vouchername;
                        vmodel.VoucherPrefix = v.VoucherPrefix;
                        vmodel.VoucherType_key = v.VoucherType_key;
                        VoucherType_Result.Add(vmodel);
                    }
                }

            }
            return VoucherType_Result;

        }
        public List<SPGetVoucherByMemberKeyModel> GetAssignedVouchers_ByMember(int Memberkey)
        {
            List<SPGetVoucherByMemberKeyModel> Vouchers_Result = new List<SPGetVoucherByMemberKeyModel>();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var VoucherType = con.SPGetVoucherByMemberKey(Memberkey).ToList();

                if (VoucherType != null)
                {
                    foreach (var v in VoucherType)
                    {
                        SPGetVoucherByMemberKeyModel vmodel = new SPGetVoucherByMemberKeyModel();
                        vmodel.voucher_key = v.voucher_key;
                        vmodel.member_key = v.member_key;
                        vmodel.VoucherTypeName = v.VoucherTypeName;
                        vmodel.CreationDate = v.CreationDate;
                        vmodel.ExpiryDate = v.ExpiryDate;
                        vmodel.LastestScanDate = v.LastestScanDate;
                        vmodel.CashValue = v.CashValue;
                        vmodel.vouchernumber = v.vouchernumber;
                        Vouchers_Result.Add(vmodel);
                    }
                }

            }
            return Vouchers_Result;

        }
        public VoucherTypeModel GetVoucherType(int vouchertypekey)
        {
            VoucherTypeModel vmodel = new VoucherTypeModel();
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var v = (from t in con.VoucherTypes
                         where t.VoucherType_key == vouchertypekey
                         select t).FirstOrDefault();

                if (v != null)
                {
                    vmodel.VoucherType_key = v.VoucherType_key;
                    vmodel.vouchername = v.Name;
                    vmodel.ConversionValue = v.ConversionValue;
                    vmodel.ExpiryPeriod = v.ExpiryPeriod;
                    vmodel.Company_key = v.Company_key;
                    vmodel.VoucherPrefix = v.VoucherPrefix;
                    vmodel.nextnumber = v.nextnumber;
                    if (v.OverrideLimit != null)
                        vmodel.OverrideLimit = v.OverrideLimit;
                    if (v.UsedLater != null)
                        vmodel.UsedLater = v.UsedLater.Value;
                }

            }
            return vmodel;
        }

        public string RedeemVoucher(int vouchertypekey, int memberkey, bool used, string user)
        {
            string voucherNumer = string.Empty;
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var c = con.spVoucherGeneration(memberkey, vouchertypekey, used, user, null).FirstOrDefault();
                voucherNumer = c.ToString();
            }

            return voucherNumer;
        }
        public void UseVoucher(string VoucherNumber, string user)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.SPUseVoucherExtended(VoucherNumber, user, 1, string.Empty);
            }
        }

        public string IsVoucherScannable(string VoucherNumber)
        {
            try
            {
                VoucherModel vm = new VoucherModel();
                vm = GetVoucher(VoucherNumber);
                if (DateTime.Now > vm.expirydate)
                {
                    throw new Exception("Voucher expired");
                }
                if (vm.TerminalAllocation_key != null)
                {
                    if (vm.TerminalAllocation_key > 0)
                    {
                        if (vm.TerminalTransactionTotal.GetValueOrDefault(0) < vm.TerminalAmountTarget.GetValueOrDefault(0))
                        {
                            throw new Exception(String.Format("This terminal has not met transaction target between {2} and {3}. Total Terminal Transaction is ${0}. Target is ${1}.", vm.TerminalTransactionTotal, vm.TerminalAmountTarget,
                                vm.TerminalTransactedStartDate.Value.ToString("dd/MM/yyyy"), vm.TransactionCutOffDate.Value.ToString("dd/MM/yyyy")));
                        }
                    }
                }
                if (vm.ScanCountWithinCurrentPeriod >= vm.UsagePeriodLimit)
                {
                    string UsagePeriodString = GetUsagePeriodString(vm.UsagePeriod.GetValueOrDefault());

                    throw new Exception(String.Format("Invalid Voucher. Usage Limit of this voucher is {0} per {1}. This voucher has been used {2} time(s) a {1}", vm.UsagePeriodLimit, UsagePeriodString, vm.ScanCountWithinCurrentPeriod));
                }
                if (vm.MultipleUse == true)
                {
                    if (vm.ScanCount.GetValueOrDefault(0) >= vm.UsageQuantity.GetValueOrDefault(0))
                    {
                        throw new Exception("Invalid Voucher. It has reached the usage limit or has been used.");
                    }
                }
                else
                {
                    if (vm.ScanCount.GetValueOrDefault() >= 1)
                    {
                        throw new Exception("Invalid Voucher. It has reached the usage limit or has been used.");
                    }
                }

                if (vm.ScanCount == 0)
                {
                    return "ok";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Not ok";
        }

        private string GetUsagePeriodString(int UsagePeriod)
        {
            switch (UsagePeriod)
            {
                case 2:
                    return "Week";
                case 1:
                    return "Day";
                case 3:
                    return "Month";
                case 4:
                    return "Year";
                default:
                    return "";
            }
        }

        public VoucherModel GetVoucher(string VoucherNumber)
        {
            VoucherModel vm = new VoucherModel();
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {

                var ck = con.SPGetVoucher(VoucherNumber).FirstOrDefault(); ;

                if (ck != null)
                {

                    vm.CashValue = ck.CashValue;
                    vm.Comment = ck.Comment;
                    vm.expirydate = ck.expirydate;
                    vm.ExpiryPeriod = ck.ExpiryPeriod;
                    vm.firstname = ck.firstname;
                    vm.LastestScanDate = ck.LastestScanDate;
                    vm.lastname = ck.lastname;
                    vm.Member_key = ck.Member_key;
                    vm.memberid = ck.memberid;
                    vm.MultipleUse = ck.MultipleUse;
                    vm.PointUsed = ck.PointUsed;
                    vm.ScanCount = ck.ScanCount;
                    vm.ScanCountWithinCurrentPeriod = ck.ScanCountWithinCurrentPeriod;
                    vm.TerminalAllocation_key = ck.TerminalAllocation_key;
                    vm.TerminalAmountTarget = ck.TerminalAmountTarget;
                    vm.TerminalTransactedStartDate = ck.TerminalTransactedStartDate;
                    vm.TerminalTransactionTotal = ck.TerminalTransactionTotal;
                    vm.tradingname = ck.tradingname;
                    vm.TransactionAmountTarget = ck.TransactionAmountTarget;
                    vm.TransactionCalculatedPeriod = ck.TransactionCalculatedPeriod;
                    vm.TransactionCutOffDate = ck.TransactionCutOffDate;
                    vm.UsagePeriod = ck.UsagePeriod;
                    vm.UsagePeriodLimit = ck.UsagePeriodLimit;
                    vm.UsageQuantity = ck.UsageQuantity;
                    vm.UseDate = ck.UseDate;
                    vm.Voucher_key = ck.Voucher_key;
                    vm.VoucherNumber = ck.VoucherNumber;
                    vm.VoucherType_key = ck.VoucherType_key;
                    vm.VoucherTypeEndDate = ck.VoucherTypeEndDate;
                    vm.VoucherTypeName = ck.VoucherTypeName;
                    vm.VoucherTypeStartDate = ck.VoucherTypeStartDate;
                    vm.VoucherValidity_Key = ck.VoucherValidity_Key;

                }
            }
            return vm;
        }
    }
}

