﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class MemberAccountEntryDA
    {

       public void Add(MemberAccountEntryModel t)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               MemberAccountEntry mae = new MemberAccountEntry();
               mae.MemberAccountKey = t.MemberAccountKey;
               mae.Member_key = t.Member_key;
               mae.Transaction_key = t.Transaction_key;
               mae.EntryDateTime = t.EntryDateTime;
               mae.Description = t.Description;
               mae.CreditAmount = t.CreditAmount;
               mae.DebitAmount = t.DebitAmount;
               mae.BalanceAmount = t.BalanceAmount;
               mae.ReversedMemberAccountEntryKey = t.ReversedMemberAccountEntryKey;
               mae.Office_key = t.Office_key;
               mae.CreatedByUser = t.CreatedByUser;
               mae.CreatedDateTime = t.CreatedDateTime;
               mae.UpdatedByUser = t.UpdatedByUser;
               mae.UpdatedDateTime = t.UpdatedDateTime;

               context.MemberAccountEntries.Add(mae);
               context.SaveChanges();
           }

       }
    }
}
