﻿using Phoenix_Service;
using PhoenixObjects.MyWeb;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class MyWebChangeRequest_BankDA
    {
       public List<BankChangeRequestModel> GetAllPending(string username)
       {
           List<BankChangeRequestModel> Pending = new List<BankChangeRequestModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               string status = TEnum.MyWebChangeRequestStatus.PENDING.ToString();
               var addr = context.MyWebChangeRequest_Bank.Where(n => n.Status == status);
               if (addr.Any())
               {
                   foreach (var v in addr)
                   {

                       var mem = context.Members.Include(n => n.Accounts).FirstOrDefault(n => n.Member_key == v.Member_key);
                       if (mem != null)
                       {
                           BankChangeRequestModel mda = new BankChangeRequestModel();
                           mda.AssignedDate = v.AssignedDate;
                           mda.AssignedTo = v.AssignedTo;
                           mda.CheckedBy = v.CheckedBy;
                           mda.CheckedDate = v.CheckedDate;
                           mda.CompleteBy = v.CompleteBy;
                           mda.CompletedDate = v.CompletedDate;
                           mda.CreateDate = v.CreateDate;
                           mda.CreatedBy = v.CreatedBy;
                           mda.Member_key = v.Member_key;
                           mda.New_AccountName = v.New_AccountName;
                           mda.New_AccountNumber = v.New_AccountNumber;
                           mda.New_BSB = v.New_BSB;
                           mda.RequestID = v.RequestID;
                           mda.Status = v.Status;
                           mda.FirstName = mem.FirstName;
                           mda.LastName = mem.LastName;
                           mda.MemberId = mem.MemberId;
                           mda.TradingName = mem.TradingName;

                           var account = mem.Accounts.FirstOrDefault();
                           if (account != null)
                           {
                               mda.Old_AccountName = account.AccountName;
                               mda.OLd_AccountNumber = account.AccountNumber;
                               mda.Old_BSB = account.Bsb;
                               mda.Old_Weight = account.Weight.Value;
                               mda.Account_key = account.Account_key;
                           }
                           Pending.Add(mda);
                       }
                   }
               }
           }
           return Pending;
       }

       public BankChangeRequestModel GetRequest(int RequestID)
       {
           BankChangeRequestModel mda = new BankChangeRequestModel();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var v = (from m in context.MyWebChangeRequest_Bank
                        where m.RequestID == RequestID
                        select m).FirstOrDefault();

               if (v != null)
               {
                   var mem = (from m in context.Members
                              where m.Member_key == v.Member_key
                              select m).FirstOrDefault();
                   var acc = (from m in context.Accounts
                              where m.Member_key == v.Member_key
                              select m).FirstOrDefault();

                   if (mem != null)
                   {
                       mda.AssignedDate = v.AssignedDate;
                       mda.AssignedTo = v.AssignedTo;
                       mda.CheckedBy = v.CheckedBy;
                       mda.CheckedDate = v.CheckedDate;
                       mda.CompleteBy = v.CompleteBy;
                       mda.CompletedDate = v.CompletedDate;
                       mda.CreateDate = v.CreateDate;
                       mda.CreatedBy = v.CreatedBy;
                       mda.Member_key = v.Member_key;
                       mda.New_AccountName = v.New_AccountName;
                       mda.New_AccountNumber = v.New_AccountNumber;
                       mda.New_BSB = v.New_BSB;
                   
                       mda.RequestID = v.RequestID;
                       if (v.Image1.Length > 2)
                       {
                           mda.SupportDoc1 = v.Image1;
                       }
                       else
                       {
                           mda.SupportDoc1 = null;
                       }
                       if (v.Image2.Length > 2)
                       {
                           mda.SupportDoc2 = v.Image2;
                       }
                       else
                       {
                           mda.SupportDoc2 = null;
                       }
                       mda.Status = v.Status;
                       mda.FirstName = mem.FirstName;
                       mda.LastName = mem.LastName;
                       mda.MemberId = mem.MemberId;
                       mda.TradingName = mem.TradingName;
                       if (acc != null)
                       {
                           mda.Old_AccountName = acc.AccountName;
                           mda.OLd_AccountNumber = acc.AccountNumber;
                           mda.Old_BSB = acc.Bsb;
                           mda.Old_Weight = acc.Weight.Value;
                           mda.Account_key = acc.Account_key;
                       }
                   }
               }
           }
           return mda;

       }

       public BankChgReqImageModel GetDocument(int RequestId, int WhichOne)
       {
           BankChgReqImageModel model = new BankChgReqImageModel();
           model.SupportDoc = new byte[0];
           // byte[] img = new byte[0];

           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               if (WhichOne == 1)
               {
                   var v = (from m in context.MyWebChangeRequest_Bank
                            where m.RequestID == RequestId
                            select m).FirstOrDefault();

                   if (v != null)
                   {
                       model.SupportDoc = v.Image1;
                       model.Extension = v.Image1Ext;
                   }
               }
               else if (WhichOne == 2)
               {
                   var v = (from m in context.MyWebChangeRequest_Bank
                            where m.RequestID == RequestId
                            select m).FirstOrDefault();

                   if (v != null)
                   {
                       model.SupportDoc = v.Image2;
                       model.Extension = v.Image2Ext;
                   }
               }
           }

           return model;

       }

       public void UpdateStatusComplete(BankChangeRequestModel mod)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var v = (from m in context.MyWebChangeRequest_Bank
                        where m.RequestID == mod.RequestID
                        select m).FirstOrDefault();

               if (v != null)
               {
                   v.CompleteBy = mod.CompleteBy;
                   v.CompletedDate = mod.CompletedDate;
                   v.Status = mod.Status;

                   context.SaveChanges();
               }
           }
       }
    }
}
