﻿using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class TerminalRentalRateDA
    {
      
        public List<TerminalRentalRateModel> GetByMemberRentalTemplateKey(int MemberRentalTemplateKey)
        {
            List<TerminalRentalRateModel> TerminalRentalRateList = new List<TerminalRentalRateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = from m in context.TerminalRentalTemplates
                           where m.MemberRentalTemplateKey == MemberRentalTemplateKey
                           select m;
              
                if (mem != null)
                {
                    foreach (var m in mem)
                    {
                        var rates = (from t in context.TerminalRentalRates
                                     where t.TerminalRentalRate_key == m.TerminalRentalRate_Key
                                   
                                    select t).FirstOrDefault();
                        if (rates != null)
                        {
                                TerminalRentalRateModel model = new TerminalRentalRateModel();
                                model.TerminalRentalRate_key = rates.TerminalRentalRate_key;
                                model.StartDate = rates.StartDate;
                                model.EndDate = rates.EndDate;
                                model.TransactionTarget = rates.TransactionTarget;
                                //putting rates from TerminalRentalTemplates not TerminalRentalRates
                                model.RentAmount = m.RentalAmount.Value;
                                model.Company_key = rates.Company_key;
                                model.WaitingPeriod = rates.WaitingPeriod;
                                model.IsDefault = rates.IsDefault;
                                model.MemberRentalTemplateKey = m.MemberRentalTemplateKey;
                                TerminalRentalRateList.Add(model);
                        }
                    }
                }
            }
            return TerminalRentalRateList.OrderBy(n => n.TerminalRentalRate_key).ToList();
        }
    }
}
