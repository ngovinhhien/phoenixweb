﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Phoenix_BusLog.Data
{
    public class BusinessTypeDA
    {
        public List<BusinessTypeModel> Get(bool isfilter = false)
        {
            List<BusinessTypeModel> businessTypeList = new List<BusinessTypeModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var companies = context.Companies.AsQueryable();
                if (isfilter)
                {
                    var disableBusinessTypeList = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebMemberCreationDisableBusinessType.ToString()).Split(',').ToList();
                    companies = companies.Where(m => !disableBusinessTypeList.Contains(m.Name)).AsQueryable();
                }

                businessTypeList = companies.Select(n => new BusinessTypeModel()
                {
                    BusinessTypeKey = n.Company_key,
                    BusinessTypeName = n.Name,
                    MSFApplicable = n.MSFApplicable ?? false
                }).ToList();
            }

            return businessTypeList;
        }

        public List<Company> GetBusinessTypes()
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.Companies.ToList();
            }
        }

        public List<Company> GetBusinessTypesAvailableForPromotion()
        {
            using (var entities = new TaxiEpayEntities())
            {
                var result = (from c in entities.Companies
                              join p in entities.LoyaltyPromotions on c.Company_key equals p.CompanyKey
                              select c
                             ).Distinct().ToList();
                return result;
            }
        }

    }
}
