﻿using Phoenix_Service.LoginDbOld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class LiveEftpos_dbDA
    {
        public string GetLoginAnswer(string Username)
        {
            string answer = string.Empty;
            using (LiveEftposEntities context = new LiveEftposEntities())
            {
                //var ans = (from a in context.aspnet_Users
                //           where a.UserName == Username
                //           select a).FirstOrDefault();
                var ans = context.aspnet_UsersR.Where(r => r.UserName == Username).FirstOrDefault();

                if (ans != null)
                {
                     answer = (from a in context.aspnet_MembershipR
                                where a.UserId == ans.UserId
                                select a.PasswordAnswer).FirstOrDefault();                 
                }              
            }
            return answer;
        }
    }
}
