﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Member;
using PhoenixObjects.QBR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{

    /// <summary>
    /// This class represents Member Refernece Entry to store qantas refernce numbers
    /// </summary>
    public class MemberReferenceAuditDA : IDisposable
    {
        public void Dispose()
        {

        }

        public PagedResponseModel<MemberQBRPointHistoryResultVM> MemberQBRPointHistory(MemberQBRPointHistoryCriteriaVM model)
        {
            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@MemberKey", model.MemberKey));
                sqlParameters.Add(new SqlParameter("@SortField", model.SortField ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortOrder", model.SortOrder ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output,
                };
                sqlParameters.Add(totalItemsParam);
                try
                {
                    var response = taxiEpay.Database.SqlQuery<MemberQBRPointHistoryResultVM>(
                        "EXEC MemberQBRPointHistory "
                    + "  @MemberKey"
                    + ", @SortField"
                    + ", @SortOrder"
                    + ", @PageSize"
                    + ", @PageIndex"
                    + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();
                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    return new PagedResponseModel<MemberQBRPointHistoryResultVM>
                    {
                        rows = response,
                        page = model.PageIndex,
                        total = (int)Math.Ceiling((float)total / model.PageSize),
                        records = total,
                    };
                }
                catch (Exception e)
                {
                    return new PagedResponseModel<MemberQBRPointHistoryResultVM>
                    {
                        isError = true
                    };
                }
            }
        }
    }
}
