﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class MemberAccountDA
    {
       public int Get(int Member_Key)
       {
           int accountkey = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var acc = (from m in context.MemberAccounts
                         where m.Member_Key == Member_Key
                         select m).FirstOrDefault();

               if (acc != null)
               {
                   accountkey = acc.MemberAccountKey;
                 
               }
           }
           return accountkey;
       }
    }
}
