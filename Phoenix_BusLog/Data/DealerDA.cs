﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Member;
using PhoenixObjects.Common;
using PhoenixObjects.Terminal;
using CustomerPortalDataAccess.Entities;

namespace Phoenix_BusLog.Data
{
    public class DealerDA
    {
        public DealerModel Get(int id)
        {
            DealerModel deal = new DealerModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Dealers
                            where m.Dealer_key == id
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    deal.Modified_dttm = addr.Modified_dttm;
                    deal.ModifiedByUser = addr.ModifiedByUser;
                    deal.Dealer_key = addr.Dealer_key;
                }
            }
            return deal;
        }

        public int GetCompanyKey(int memberkey)
        {
            int comanykey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Members
                            where m.Member_key == memberkey
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    comanykey = addr.Company_key;
                }
            }
            return comanykey;
        }

        public List<DealerModel> GetDealersByCompanykey(int companykey)
        {
            List<DealerModel> deal = new List<DealerModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.Dealers
                           join c in context.Members on m.Dealer_key equals c.Member_key
                           where c.Company_key == companykey
                           select new { Dealer_key = m.Dealer_key, Name = c.TradingName };

                if (addr != null)
                {
                    foreach (var v in addr)
                    {
                        DealerModel dm = new DealerModel();
                        dm.Dealer_key = v.Dealer_key;
                        dm.DealerName = v.Name;
                        deal.Add(dm);
                    }
                }
            }
            return deal;
        }


        public List<PerformanceOperatorModel> GetNoTransactionIn30Days(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_OperatorNoTransactionIn30Days(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = null;
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.TerminalId = m.TerminalId;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetNoTransactionIn7Days(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_OperatorNoTransactionIn7Days(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = null;
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.TerminalId = m.TerminalId;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }


        public List<PerformanceOperatorModel> GetTop5OperatorPerformer_Weekly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorTop5Performers(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum.GetValueOrDefault(0));
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;                      
                        model.Add(md);
                    }
                }
            }
            return model;
        }
        public List<PerformanceOperatorModel> GetLeast5OperatorPerformer_Weekly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorLeast5Performers(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;                       
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetTop5OperatorPerformer_Monthly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorTop5Performers_Monthly(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;

                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetLeast5OperatorPerformer_Monthly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorLeast5Performers_Monthly(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;

                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetLeast5OperatorPerformerByTerminal_Weekly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorLeast5PerformersByTerminal(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetLeast5OperatorPerformerByTerminal_Monthly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorLeast5PerformersByTerminal_Monthly(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetTop5OperatorPerformerByTerminal_Weekly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorTop5PerformersByTerminal(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<PerformanceOperatorModel> GetTop5OperatorPerformerByTerminal_Monthly(int Dealer_key)
        {
            List<PerformanceOperatorModel> model = new List<PerformanceOperatorModel>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mo = context.Dealer_GetOperatorTop5PerformersByTerminal_Monthly(Dealer_key).ToList();
                if (mo != null)
                {
                    foreach (var m in mo)
                    {
                        PerformanceOperatorModel md = new PerformanceOperatorModel();
                        md.AmountSum = Convert.ToDecimal(m.AmountSum);
                        md.MemberId = m.MemberId;
                        md.TradingName = m.TradingName;
                        md.Mobile = m.Mobile;
                        md.Telephone = m.Telephone;
                        md.TerminalId = m.TerminalId;
                        model.Add(md);
                    }
                }
            }
            return model;
        }

        public List<MemberModel> Get()
        {
            List<MemberModel> deal = new List<MemberModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.Dealers
                           select m;

                if (addr != null)
                {
                    foreach (var v in addr)
                    {
                        MemberDA mda = new MemberDA();
                        MemberModel dm = mda.GetMemeber(v.Member.Member_key);
                        deal.Add(dm);
                    }
                }
            }
            return deal;
        }
        public int GetDealerIdByUsername(string Username)
        {
            int dealerid = 0;
            bool HasMem = false;
            int memkey = 0;
            try
            {
                using (LiveGroupPortalEntities context = new LiveGroupPortalEntities())
                {
                    var user = (from m in context.userprofiles
                                where m.UserName == Username
                                select m).FirstOrDefault();

                    if (user != null)
                    {
                        var membership = (from m in context.usermemberships
                                     where m.UserId == user.UserId
                                     select m).FirstOrDefault();
                        if (membership != null)
                        {
                            HasMem = true;
                            memkey = membership.MemberKey;
                        }
                    }
                    if (HasMem == true)
                    {
                        using (TaxiEpayEntities context1 = new TaxiEpayEntities())
                        {
                            var del = (from d in context1.Dealers
                                       where d.Dealer_key == memkey
                                       select d).FirstOrDefault();

                            if (del != null)
                            {
                                dealerid = del.Dealer_key;
                            }
                            else
                            {
                                DealerModel dealer = new DealerModel();
                                dealer.Dealer_key = memkey;
                                dealer.Modified_dttm = DateTime.Now;
                                dealer.ModifiedByUser = Username;
                                DealerDA dda = new DealerDA();
                                dda.Add(dealer);
                                DealerModel deal1 = dda.Get(memkey);
                                dealerid = deal1.Dealer_key;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return dealerid;
        }
        public void Add(DealerModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Phoenix_Service.Dealer deal = new Phoenix_Service.Dealer();
                deal.Modified_dttm = t.Modified_dttm;
                deal.ModifiedByUser = t.ModifiedByUser;
                deal.Dealer_key = t.Dealer_key;

                context.Dealers.Add(deal);
                context.SaveChanges();
            }
        }

        public void AddClone(DealerModel t, int id)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Phoenix_Service.Dealer deal = new Phoenix_Service.Dealer();
                deal.Modified_dttm = t.Modified_dttm;
                deal.ModifiedByUser = t.ModifiedByUser;
                deal.Dealer_key = id;

                context.Dealers.Add(deal);
                context.SaveChanges();
            }
        }

        public string GetDealerEdit(int DealerKey)
        {
            string dealerList = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = (from d in context.Dealers
                            join m in context.Members on d.Dealer_key equals m.Member_key
                            where d.Dealer_key == DealerKey
                            select new { id = d.Dealer_key, FName = m.FirstName, LName = m.LastName, MemberId = m.MemberId, MState = m.State, TradingName = m.TradingName }).Take(20);
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        string name = n.TradingName + " | " + n.id + " | " + n.MState;
                        dealerList = name;
                    }
                }
            }

            return dealerList;
        }

        public List<string> GetDealerAutoComplete(string term)
        {
            List<string> dealerList = new List<string>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = (from d in context.Dealers
                            join m in context.Members on d.Dealer_key equals m.Member_key
                            where m.TradingName.StartsWith(term)
                            select new { id = d.Dealer_key, MState = m.State, TradingName = m.TradingName }).Take(20);
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        string name = n.TradingName + " | " + n.id + " | " + n.MState;
                        dealerList.Add(name);
                    }
                }
            }

            return dealerList;
        }
        public MultiMemberSearchModel GetDealerOperators(int Dealerkey)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            MemberDA mda = new MemberDA();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.Dealer_GetActiveOperatorCount(Dealerkey).ToList();


                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        var mem = mda.GetMemeberLowVersion(n.GetValueOrDefault(0));
                        model.MemberList.Add(mem);
                    }
                }
            }

            return model;
        }

        public List<OperatorTerminalModel> GetDealerOperatorsTerminals(int Dealerkey)
        {
            List<OperatorTerminalModel> model = new List<OperatorTerminalModel>();
            MemberDA mda = new MemberDA();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.Dealer_GetOperatorActiveTerminal(Dealerkey).ToList();
                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        OperatorTerminalModel m = new OperatorTerminalModel();
                        m.EftTerminal_key = n.EftTerminal_key;
                        m.Email = n.Email;
                        m.Member_key = n.Member_key;
                        m.MemberId = n.MemberId;
                        m.TerminalId = n.TerminalId;
                        m.TradingName = n.TradingName;
                        m.LastName = n.LastName;
                        m.FirstName = n.FirstName;
                        m.VehicleId = n.VehicleId;

                        model.Add(m);
                    }

                }
            }

            return model;
        }
    }
}
