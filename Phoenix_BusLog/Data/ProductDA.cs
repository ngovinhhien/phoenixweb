﻿using Phoenix_TransactionHostUI;
using PhoenixObjects.Glidebox;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Phoenix_BusLog.Data
{
    public class ProductDA
    {
        public object GetProducts(int pageSize, int currentPage, int categoryID, string searchText = "", string sortName = "", string sortOrder = "")
        {
            using (var entities = new TransactionHostEntities())
            {
                var items = entities.vw_PHW_Glidebox_Manage_GetProducts.AsQueryable();

                if (!string.IsNullOrEmpty(searchText))
                {
                    items = items.Where(n => n.ProductName.ToLower().Contains(searchText.ToLower()));
                }

                if (categoryID > 0)
                {
                    items = items.Where(n => n.CategoryID == categoryID);
                }

                items = SortProductList(items, sortName, sortOrder);
                int totalRecords = items.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var skip = (currentPage - 1) * pageSize;
                var res = items.Skip(skip).Take(pageSize);

                return new
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
            }
        }

        public IQueryable<vw_PHW_Glidebox_Manage_GetProducts> SortProductList(IQueryable<vw_PHW_Glidebox_Manage_GetProducts> items, string sortName, string sortOrder)
        {
            IQueryable<vw_PHW_Glidebox_Manage_GetProducts> itemsSorter;
            switch (sortName)
            {
                case "ProductName":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ProductName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ProductName);
                    }
                    break;
                case "Price":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Price.Length).ThenBy(n => n.Price);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Price.Length).ThenByDescending(n => n.Price);
                    }
                    break;
                case "Quantity":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Quantity);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Quantity);
                    }
                    break;
                case "CreatedDatetime":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ProductID);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ProductID);
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.ProductID);
                    break;
            }
            return itemsSorter;
        }

        public bool ActivateDeactivateProduct(ActivateDeactivateProductModel model, string changedBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var product = entities.GlideBoxProducts.FirstOrDefault(x => x.ProductID == model.ID);

                if (product == null)
                {
                    return false;
                }

                var productHistory = new GlideBoxProductHistory();

                if (model.IsActivateAction)
                {
                    product.IsActive = true;
                    productHistory.IsActive = true;
                    productHistory.Source = "Activate";
                    if (!string.IsNullOrEmpty(model.Reason?.Trim()))
                    {
                        productHistory.Reason = model.Reason.Trim();
                    }
                }
                else
                {
                    product.IsActive = false;
                    productHistory.IsActive = false;
                    productHistory.Reason = model.Reason.Trim();
                    productHistory.Source = "Deactivate";
                }

                productHistory.ProductID = model.ID;
                productHistory.ChangedDatetime = DateTime.Now;
                productHistory.ChangedBy = changedBy;

                entities.GlideBoxProductHistories.Add(productHistory);
                entities.SaveChanges();
            }

            return true;
        }

        public void SaveProduct(ProductModel model, string createdBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var product = new GlideBoxProduct
                {
                    ProductName = model.ProductName.Trim(),
                    Price = model.Price,
                    Quantity = model.Quantity,
                    ImageContent = model.ImageContent,
                    CategoryID = model.Category,
                    IsActive = true,
                    CreatedDatetime = DateTime.Now,
                    CreatedBy = createdBy
                };

                entities.GlideBoxProducts.Add(product);

                var productHistory = new GlideBoxProductHistory
                {
                    ProductID = product.ProductID,
                    NewProductName = product.ProductName,
                    NewPrice = product.Price,
                    NewQuantity = product.Quantity,
                    NewCategoryID = product.CategoryID,
                    IsActive = true,
                    Source = "Create",
                    ChangedDatetime = DateTime.Now,
                    ChangedBy = createdBy
                };

                if (model.ImageContent != null)
                {
                    productHistory.NewImageContent = model.ImageContent;
                }

                entities.GlideBoxProductHistories.Add(productHistory);
                entities.SaveChanges();
            }
        }

        public bool EditProduct(ProductModel model, string changedBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var product = entities.GlideBoxProducts.FirstOrDefault(x => x.ProductID == model.ProductID);

                if (product == null)
                {
                    return false;
                }

                var isChanged = false;
                var productHistory = new GlideBoxProductHistory();
                if (product.ProductName != model.ProductName)
                {
                    productHistory.OldProductName = product.ProductName;
                    productHistory.NewProductName = model.ProductName;
                    product.ProductName = model.ProductName.Trim();
                    isChanged = true;
                }

                if (product.Price != model.Price)
                {
                    productHistory.OldPrice = product.Price;
                    productHistory.NewPrice = model.Price;
                    product.Price = model.Price;
                    isChanged = true;
                }

                if (product.Quantity != model.Quantity)
                {
                    productHistory.OldQuantity = product.Quantity;
                    productHistory.NewQuantity = model.Quantity;
                    product.Quantity = model.Quantity;
                    isChanged = true;
                }

                if (product.CategoryID != model.Category)
                {
                    productHistory.OldCategoryID = product.CategoryID;
                    productHistory.NewCategoryID = model.Category;
                    product.CategoryID = model.Category;
                    isChanged = true;
                }

                if (model.IsChangedImage)
                {
                    productHistory.OldImageContent = product.ImageContent;
                    productHistory.NewImageContent = model.ImageContent;
                    product.ImageContent = model.ImageContent;
                    isChanged = true;
                }

                if (isChanged)
                {
                    productHistory.ProductID = product.ProductID;
                    productHistory.Source = "Edit";
                    productHistory.ChangedDatetime = DateTime.Now;
                    productHistory.ChangedBy = changedBy;
                    entities.GlideBoxProductHistories.Add(productHistory);

                    entities.SaveChanges();
                } 
            }

            return true;
        }

        public bool IsExistProductName(ProductModel model)
        {
            var productName = model.ProductName.Trim().ToLower();
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                if (model.ProductID == 0)
                {
                    return tranHostDbContext.GlideBoxProducts.Any(x => x.ProductName.Trim().ToLower() == productName);
                }
                else
                {
                    return tranHostDbContext.GlideBoxProducts.Any(x =>
                        x.ProductName.Trim().ToLower() == productName && x.ProductID != model.ProductID);
                }
            }
        }

        public object GetProductByID(int productID)
        {
            using (var entities = new TransactionHostEntities())
            {
                var product = entities.GlideBoxProducts.FirstOrDefault(n => n.ProductID == productID);

                string imageDataURL = "";
                if (product.ImageContent != null)
                {
                    string imageBase64 = Convert.ToBase64String(product.ImageContent);
                    imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64);
                }

                return new
                {
                    ProductID = product.ProductID,
                    ProductName = product.ProductName,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    CategoryID = product.CategoryID,
                    CategoryName = product.GlideBoxCategory.CategoryName,
                    ImageContent = imageDataURL,
                    IsActive = product.IsActive,
                    CreatedBy = product.CreatedBy,
                    CreatedDatetime = product.CreatedDatetime.ToString("dd/MM/yyyy")
                };
            }
        }

        public object SearchProductHistory(int productID, int pageSize, int currentPage, string searchText = "", string sortName = "", string sortOrder = "")
        {
            using (var entities = new TransactionHostEntities())
            {
                var items = entities.usp_PHW_Glidebox_SearchProductHistory(productID, searchText).ToList();

                items = SortProductHistoryList(items, sortName, sortOrder); //handle sorting
                int totalRecords = items.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var skip = (currentPage - 1) * pageSize;
                var res = items.Skip(skip).Take(pageSize);

                return new
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = res
                };
            }
        }

        public List<usp_PHW_Glidebox_SearchProductHistory_Result> SortProductHistoryList(List<usp_PHW_Glidebox_SearchProductHistory_Result> items, string sortName, string sortOrder)
        {
            List<usp_PHW_Glidebox_SearchProductHistory_Result> itemsSorter;
            switch (sortName)
            {
                case "ChangedDatetime":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(x => x.ChangedDatetime).ToList();
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ChangedDatetime).ToList();
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.ChangedDatetime).ToList();
                    break;
            }
            return itemsSorter;
        }
    }
}