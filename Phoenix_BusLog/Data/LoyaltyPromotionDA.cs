﻿using Phoenix_Service;
using PhoenixObjects.QBR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class LoyaltyPromotionDA
    {
        public List<LoyaltyPromotionModel> GetPromotionsByCompanykey(int companykey)
        {
            List<LoyaltyPromotionModel> deal = new List<LoyaltyPromotionModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var promotions =
                            (from p in context.LoyaltyPromotions
                             where p.CompanyKey == companykey
                             select new LoyaltyPromotionModel
                             {
                                 PromotionKey = p.ID,
                                 PromotionName = p.PromotionName
                             })
                            .ToList();
                return promotions;

            }
        }
    }
}
