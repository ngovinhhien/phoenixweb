﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using Phoenix_Service.Extension;

namespace Phoenix_BusLog.Data
{
    public class MerchantGroupDA
    {
        /// <summary>
        /// This method will return group and total merchant associate with that group and paging.
        /// </summary>
        /// <param name="PageSize">Page size</param>
        /// <param name="CurrentPage">jqgrid current page</param>
        /// <param name="searchText">Search text</param>
        /// <returns>The merchant group and total number of merchant</returns>
        public object GetMerchantGroupAndCountTheNumberOfMerchant(int PageSize, int CurrentPage, string searchText = "")
        {
            using (var entities = new TaxiEpayEntities())
            {
                var items = entities.View_TotalGroupAndMerchant.AsQueryable();
                if (!string.IsNullOrEmpty(searchText))
                {
                    items = items.Where(n => n.GroupName.ToLower().Contains(searchText.ToLower()));
                }

                items = items.OrderBy(n => n.GroupName);
                int totalRecords = items.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)PageSize);
                var skip = (CurrentPage - 1) * PageSize;
                var res = items.Skip(skip).Take(PageSize);

                return new
                {
                    total = totalPages,
                    page = CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
            }
        }

        /// <summary>
        /// This method using for searching merchant inside group.
        /// </summary>
        /// <param name="groupID">Group ID</param>
        /// <param name="PageSize">Page size</param>
        /// <param name="CurrentPage">jqgrid current page</param>
        /// <param name="searchText">Search text</param>
        /// <returns>The detail of group, also associate with the list of merchant</returns>
        public object GetMerchantGroupDetail(int groupID, int PageSize, int CurrentPage, string searchText = "")
        {
            using (var entities = new TaxiEpayEntities())
            {
                var item = entities.Groups
                    .Include(n => n.Members)
                    .FirstOrDefault(n => n.GroupID == groupID);

                var merchants = item.Members.AsQueryable();

                if (!string.IsNullOrEmpty(searchText))
                {
                    merchants = merchants.Where(n => (n.FirstName + "," + n.LastName).Contains(searchText)
                                                     || (!string.IsNullOrEmpty(n.Email) && n.Email.Contains(searchText)));
                }

                merchants = merchants.OrderBy(n => n.Member_key);
                int totalRecords = merchants.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)PageSize);
                var skip = (CurrentPage - 1) * PageSize;
                var res = merchants.Skip(skip).Take(PageSize);
                var data = res.Select(n => new
                {
                    MerchantName = n.FirstName + "," + n.LastName,
                    n.Email,
                    n.Member_key
                }).ToList();

                return new
                {
                    total = totalPages,
                    page = CurrentPage,
                    records = totalRecords,
                    rows = data
                };
            }
        }

        public object GetMerchantAutoComplete(string searchText)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var merchantList = entities.Members
                    .Where(x => x.Member_key.ToString().Contains(searchText) || x.Email.Contains(searchText))
                    .OrderBy(n => n.CreatedDate)
                    .Take(30)
                    .Select(x => new
                    {
                        value = x.Member_key,
                        label = x.Member_key.ToString() + "-" + x.FirstName + x.LastName + "-" + x.Email,
                        custom = x.Groups.Select(n=>n.GroupName)
                    }).ToList();

                return merchantList;
            }
        }

        public object AddMerchantToGroup(int groupID, List<string> merchantIds, string user)
        {
            using (var entities = new TaxiEpayEntities())
            {
                try
                {
                    var group = entities.Groups.FirstOrDefault(x => x.GroupID == groupID);

                    if (group == null)
                    {
                        return new
                        {
                            Message = $"Could not find group with ID = {groupID}."
                        };
                    }

                    List<string> addedSuccess = new List<string>();
                    List<string> addedError = new List<string>();

                    var memberList = entities.Members;

                    foreach (var merchantId in merchantIds)
                    {
                        if (string.IsNullOrEmpty(merchantId))
                        {
                            continue;
                        }

                        var member = memberList.FirstOrDefault(x => x.MemberId == merchantId);

                        if (group.Members.Any(x => x.MemberId == merchantId) || member == null)
                        {
                            addedError.Add(merchantId); //This merchant have been belonged to this group or could not find merchantid in Member.
                        }
                        else
                        {
                            group.Members.Add(member);

                            var historyNoteObj = new MemberHistory()
                            {
                                Member_key = member.Member_key,
                                History = $"Merchant [{member.MemberId}] added to group [{group.GroupName}]",
                                CreatedBy = user,
                                CreatedDate = DateTime.Now,
                            };
                            entities.MemberHistories.Add(historyNoteObj);

                            addedSuccess.Add(merchantId);
                        }
                    }

                    entities.SaveChanges();

                    return new
                    {
                        Message = "",
                        AddedSuccess = string.Join("; ", addedSuccess),
                        AddedError = string.Join("; ", addedError)
                    };
                }
                catch (Exception)
                {
                    return new
                    {
                        Message = "System Error."
                    };
                }
            }
        }

        /// <summary>
        /// This method using for remove merchant out of group.
        /// </summary>
        /// <param name="groupID">This is group ID</param>
        /// <param name="merchantKey">This is merchant key</param>
        /// <param name="userName">Update by user</param>
        /// <returns>Success or not</returns>
        public object RemoveMerchantOutOfGroup(int groupID, int merchantKey, string userName)
        {
            using (var entities = new TaxiEpayEntities())
            {
                //QBR Exchange Rate
                var qbr = entities.QBRExchangeRates.FirstOrDefault(n => n.GroupID == groupID);
                if (qbr == null)
                {
                    return new
                    {
                        IsSuccess = false,
                        Message = "The group ID is not available."
                    };
                }

                //var used = entities.QBRPoints.Any(n => n.ExchangeRateID == qbr.ID && n.Member_Key == merchantKey);

                //if (used)
                //{
                //    return new
                //    {
                //        IsSuccess = false,
                //        Message = "This merchant have been used for calcuating QBR point in LiveSMS system. It cann't be removed."
                //    };
                //}

                var group = entities.Groups.FirstOrDefault(n => n.GroupID == groupID);
                var selectedMember = group.Members.FirstOrDefault(n => n.Member_key == merchantKey);
                group.Members.Remove(selectedMember);
                group.UpdatedBy = userName;
                group.UpdatedDate = DateTime.Now;

                var historyNoteObj = new MemberHistory()
                {
                    Member_key = selectedMember.Member_key,
                    History = $"[Merchant_Group]Merchant [{selectedMember.MemberId}] removed out of group [{group.GroupName}]",
                    CreatedBy = userName,
                    CreatedDate = DateTime.Now,
                };
                entities.MemberHistories.Add(historyNoteObj);

                entities.SaveChanges();
                return new
                {
                    IsSuccess = true,
                };

            }
        }

        /// <summary>
        /// This method allow user can delete group. If this group has been used for calculating QBR point, it can't be deleted.
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns>success or not</returns>
        public object DeleteGroup(int groupID)
        {
            using (var entities = new TaxiEpayEntities())
            {
                //QBR Exchange Rate
                var qbr = entities.QBRExchangeRates.FirstOrDefault(n => n.GroupID == groupID);
                if (qbr == null)
                {
                    return new
                    {
                        IsSuccess = false,
                        Message = "The group ID is not available."
                    };
                }

                var used = entities.QBRPoints.Any(n => n.ExchangeRateID == qbr.ID);
                if (used)
                {
                    return new
                    {
                        IsSuccess = false,
                        Message = "This group have been used for calcuating QBR point in LiveSMS system. It cann't be deleted."
                    };
                }

                qbr.GroupID = null;
                qbr.IsActive = false;
                var group = entities.Groups.FirstOrDefault(n => n.GroupID == groupID);
                group.Members.Clear();
                group.IsActive = false;

                entities.SaveChanges();

                return new
                {
                    IsSuccess = true,
                };
            }
        }

        public object AddGroupAndCampaign(Group group, QBRExchangeRate exchangeRate)
        {
            using (var entities = new TaxiEpayEntities())
            {
                using (var dbTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        entities.Entry(group).State = EntityState.Added;
                        entities.SaveChanges();

                        exchangeRate.GroupID = group.GroupID;
                        entities.Entry(exchangeRate).State = EntityState.Added;
                        entities.SaveChanges();
                        dbTransaction.Commit();
                        return new
                        {
                            IsSuccess = true,
                        };
                    }
                    catch (Exception e)
                    {
                        dbTransaction.Rollback();
                        return new
                        {
                            IsSuccess = false,
                            Message = "System Error"
                        };
                    }
                }

            }
        }


        public object EditGroupAndCampaign(Group group, QBRExchangeRate exchangeRate)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var currentGroup = entities.Groups.FirstOrDefault(n => n.GroupID == group.GroupID);
                currentGroup.CopyGroup(group);

                var qbrExchangeRate = entities.QBRExchangeRates.FirstOrDefault(n => n.ID == exchangeRate.ID);
                qbrExchangeRate.CopyQbrExchangeRate(exchangeRate);
                qbrExchangeRate.UpdatedBy = group.UpdatedBy;
                qbrExchangeRate.UpdatedDate = DateTime.Now;

                entities.SaveChanges();
                return new
                {
                    IsSuccess = true,
                };
            }
        }

        public object GetGroupAndExchangeRateInformation(int groupID)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var group = entities.Groups.FirstOrDefault(n => n.GroupID == groupID);
                var exchangeRate = entities.QBRExchangeRates.FirstOrDefault(n => n.GroupID == groupID);
                return new
                {
                    ID = exchangeRate.ID,
                    Name = exchangeRate.Name,
                    QantasExchangeRate = exchangeRate.QantasExchangeRate,
                    Type = exchangeRate.ExchangeRateType.ExchangeRateTypeName,
                    FromDate = exchangeRate.CampaignStartDate.HasValue ? exchangeRate.CampaignStartDate.Value.ToString("dd/MM/yyyy") : string.Empty,
                    ToDate = exchangeRate.CampaignEndDate.HasValue ? exchangeRate.CampaignEndDate.Value.ToString("dd/MM/yyyy") : string.Empty,
                    exchangeRate.ExchangeRateTypeCode,
                    GroupName = group.GroupName,
                    IsUsed = exchangeRate.QBRPoints.Any()
                };
            }
        }
    }
}
