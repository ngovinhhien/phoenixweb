﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class PostCodeDA
    {

        public List<string> GetStatePostCodeLocality(string term)
        { 
         List<string> post = new List <string>();
         using (TaxiEpayEntities context = new TaxiEpayEntities())
         {

             var addr = from m in context.vPostCodes
                        where m.PostCode.StartsWith(term)
                        select m.PostCode;
             if (addr != null)
             {
                 foreach (var n in addr)
                 {
                     post.Add(n);
                 }
             }
         }

         return post;
        }

        public string GetStatePostCodeLocalitySingle(string PostCode,string State, string Suburb)
        {
            string post = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {

                var addr = (from m in context.vPostCodes
                           where m.PostcodeNumber == PostCode && m.StateName == State && m.Suburb==Suburb
                           select m.PostCode).FirstOrDefault();
                if (addr != null)
                {
                    post = addr;                  
                }
            }

            return post;
        }



        public List<PostCodeModel> Get(int pCodeMasterKey)
        {
            List<PostCodeModel> post = new List <PostCodeModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.PostCodeMasters
                           where m.PostCodeMaster_key == pCodeMasterKey
                           select m;

                if (addr != null)
                {
                    foreach(var pr in  addr)
                    {                    
                        PostCodeModel p = new PostCodeModel();
                        p.PostCodeMaster_key = pr.PostCodeMaster_key;
                        p.State_key = pr.State_key.Value;
                        p.PostCodeType_Key = pr.PostcodeType_key.Value;
                        p.PostCode = pr.PostCode;

                        post.Add(p);
                    }
                }
            }
            return post;
        }

        public PostCodeModel GetPostCodeModel(int pCodeMasterKey)
        {
            PostCodeModel p = new PostCodeModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.PostCodeMasters
                           where m.PostCodeMaster_key == pCodeMasterKey
                           select m).FirstOrDefault();

                if (addr != null)
                {
                        p.PostCodeMaster_key = addr.PostCodeMaster_key;
                        p.State_key = addr.State_key.Value;
                        p.PostCodeType_Key = addr.PostcodeType_key.Value;
                        p.PostCode = addr.PostCode;
                   
                }
            }
            return p;
        }

        public PostCodeModel GetPostCodeModel(int statekey, string pcode)
        {
            PostCodeModel p = new PostCodeModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.PostCodeMasters
                            where m.State_key == statekey && m.PostCode == pcode
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    p.PostCodeMaster_key = addr.PostCodeMaster_key;
                    p.State_key = addr.State_key.Value;
                    p.PostCodeType_Key = addr.PostcodeType_key.Value;
                    p.PostCode = addr.PostCode;

                }
            }
            return p;
        }
      
    }
}
