﻿using CustomerPortalDataAccess.Entities;
using Phoenix_Service;
using Phoenix_Service.LoginDbOld;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Phoenix_BusLog.Data
{
   public class LiveGroupPortal_dbDA
    {
       public void AddUserMemebershipToOldDatabase(string Username, string EmailAddress, Guid uId)
       {
           try
           {
               List<Customers> phoenixMembers1 = CustomerByEmailAddress(EmailAddress);

               foreach (Customers phoenixMember in phoenixMembers1)
               {
                   int ckey = (int)phoenixMember.CustomerKey;
                   using (LiveEftposEntities context = new LiveEftposEntities())
                   {
                       Int64 AccountKey = 0;

                       var Account = (from Acc in context.AccountRs
                                      where Acc.PhoenixMemberKey == ckey
                                      select Acc).FirstOrDefault();
                       if (Account == null)
                       {
                           Phoenix_Service.LoginDbOld.AccountR AccountServer = new Phoenix_Service.LoginDbOld.AccountR();

                           AccountServer.PhoenixMemberKey = ckey;
                           AccountServer.AccountCreatedDate = DateTime.Now;
                           AccountServer.UserIdCreatedBy = uId;
                           AccountServer.AccountUpdateddDate = DateTime.Now;
                           AccountServer.UserIdUpdatedBy = uId;
                           context.AccountRs.Add(AccountServer);
                           context.SaveChanges();
                           AccountKey = AccountServer.AccountKey;
                       }
                       else
                       {
                           AccountKey = Account.AccountKey;
                       }
                       var Exists = (from ex in context.AccountUserRs
                                     where ex.Account.AccountKey == AccountKey && ex.aspnet_Users.UserId == uId
                                     select ex).FirstOrDefault();
                       //Add user to account users
                       if (Exists == null)
                       {
                           AccountUserR AccountUserServer = new AccountUserR();

                           AccountUserServer.AccountUserIsTop = true;
                           AccountUserServer.AccountKey = AccountKey;
                           AccountUserServer.UserId = uId;
                           AccountUserServer.AccountUserCreatedDate = DateTime.Now;
                           AccountUserServer.UserIdCreatedBy = uId;
                           AccountUserServer.AccountUserUpdatedDate = DateTime.Now;
                           AccountUserServer.UserIdUpdatedBy = uId;

                           context.AccountUserRs.Add(AccountUserServer);
                           context.SaveChanges();
                       }
                   }
               }
           }
           catch
           {

           }
       }

       public List<Customers> CustomerByEmailAddress(String EmailAddress)
       {
           List<Customers> CustmerList = new List<Customers>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var Customers = from customer in context.EFTPOS_Customer
                               where customer.Email.Equals(EmailAddress)
                               select customer;
               try
               {

                   foreach (EFTPOS_Customer Customer in Customers)
                   {
                       Customers cus = new Customers();
                       cus.CustomerName = Customer.FirstName + " " + Customer.LastName;
                       cus.CustomerKey = Customer.Member_key;
                       cus.CustomerId = Customer.MemberId;
                       cus.CompanyType = Customer.Company_key;
                       cus.CustomerType = (int)Customer.CustomerType;

                       cus.ABN = Customer.ABN;
                       cus.ACN = Customer.ACN;
                       cus.Active = (bool)Customer.Active;
                       cus.Address = Customer.Address;
                       cus.City = Customer.City;
                       cus.DriverLicenseNumber = Customer.DriverLicenseNumber;
                       cus.Email = Customer.Email;
                       cus.Fax = Customer.Fax;
                       cus.Mobile = Customer.Mobile;
                       cus.Postcode = Customer.Postcode;
                       cus.State = Customer.State;
                       cus.Telephone = Customer.Telephone;
                       cus.TradingName = Customer.TradingName;
                       cus.Photo = Customer.PhotoID;

                       CustmerList.Add(cus);
                   }
               }
               catch (Exception Ex)
               {

               }
           }

           return CustmerList;
       }
       public void AddMemberShipNewDatabase(string Username)
       {
           using (LiveGroupPortalEntities cont = new LiveGroupPortalEntities())
           {
               var user = (from n in cont.userprofiles
                           where n.UserName == Username
                           select n).FirstOrDefault();
               if (user != null)
               {                
                   List<Customers> phoenixMembers1 = CustomerByEmailAddress(user.EmailAddress);
                   foreach (Customers phoenixMember in phoenixMembers1)
                   {
                       int ckey = (int)phoenixMember.CustomerKey;

                       var membership = (from n in cont.usermemberships
                                         where n.MemberKey == ckey && n.UserId == user.UserId && n.Inactive == false
                                         select n).FirstOrDefault();
                       if (membership == null)
                       {
                           usermembership memb = new usermembership();
                           memb.UserId = user.UserId;
                           memb.MemberKey = ckey;
                           memb.VerificationToken = Guid.NewGuid().ToString();
                           memb.Verified = true;
                           memb.Inactive = false;
                           memb.IsAdmin = false;
                           memb.UserIdCreatedBy = 1;
                           memb.UserIdUpdatedBy = 1;
                           memb.CreatedDate = DateTime.Now;
                           memb.UpdatedDate = DateTime.Now;
                           cont.usermemberships.Add(memb);

                           cont.SaveChanges();

                           user.UserIdSelected = user.UserId;
                           user.UserMembershipKeySelected = memb.UserMembershipKey;

                           cont.SaveChanges();

                       }
                       else
                       {
                           if (membership.Inactive == true)
                           {
                               membership.Inactive = false;
                               cont.SaveChanges();
                           }

                       }
                   }
               }
           }

       }

    }
}
