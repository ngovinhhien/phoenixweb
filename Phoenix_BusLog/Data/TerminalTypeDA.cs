﻿using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class TerminalTypeDA
    {

        public TerminalTypeModel Get(int id)
        {
            TerminalTypeModel tType = new TerminalTypeModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var t = (from m in context.TerminalTypes
                         where m.TerminalType_key == id
                         select m).FirstOrDefault();

                if (t != null)
                {
                    tType.TerminalType_key = t.TerminalType_key;
                    tType.Name = t.Name;
                    tType.prefix = t.prefix;
                    tType.CostPerTerminal = Convert.ToDecimal(t.CostPerTerminal);
                    tType.CostPerSim = Convert.ToDecimal(t.CostPerSim);
                    tType.AvailableForPreorder = t.AvailableForPreorder;
                    tType.MerchantID = t.MerchantID;
                    tType.VirtualTerminal = t.VirtualTerminal;
                    tType.NextSequence = t.NextSequence;
                    tType.member_key = t.member_key;
                }
            }
            return tType;
        }

        public void Edit(TerminalTypeModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var aSever = (from a in context.TerminalTypes
                              where a.TerminalType_key == t.TerminalType_key
                              select a).FirstOrDefault();

                if (aSever != null)
                {
                    aSever.NextSequence = t.NextSequence;
                }
                context.SaveChanges();
            }
        }

        public TerminalTypeModel GetByNameMerchantPrefix(string Name, string merchantid, string prefix)
        {
            TerminalTypeModel tType = new TerminalTypeModel();
            TerminalType t = new TerminalType();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                if (merchantid == "Null")
                {
                    t = (from m in context.TerminalTypes
                         where m.Name == Name && m.prefix == prefix
                         select m).FirstOrDefault();
                }
                else
                {
                    t = (from m in context.TerminalTypes
                         where m.Name == Name && m.MerchantID == merchantid && m.prefix == prefix
                         select m).FirstOrDefault();
                }

                if (t != null)
                {
                    tType.TerminalType_key = t.TerminalType_key;
                    tType.Name = t.Name;
                    tType.prefix = t.prefix;
                    tType.CostPerTerminal = Convert.ToDecimal(t.CostPerTerminal);
                    tType.CostPerSim = Convert.ToDecimal(t.CostPerSim);
                    tType.AvailableForPreorder = t.AvailableForPreorder;
                    tType.MerchantID = t.MerchantID;
                    tType.VirtualTerminal = t.VirtualTerminal;
                    tType.NextSequence = t.NextSequence;
                    tType.member_key = t.member_key;
                }
            }
            return tType;
        }

        public TerminalTypeModel GetVirtualTerminalTypeByDealerKey(int DealerKey)
        {
            TerminalTypeModel tType = new TerminalTypeModel();
            //TerminalType t = new TerminalType();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var t = (from d in context.Dealers
                         join tt in context.TerminalTypes on d.VirtualTerminalTypeKey equals tt.TerminalType_key
                         where d.Dealer_key == DealerKey
                         select tt).FirstOrDefault();


                //if (merchantid == "Null")
                //{
                //    t = (from m in context.TerminalTypes
                //         join
                //         where m.Name == Name && m.prefix == prefix
                //         select m).FirstOrDefault();
                //}
                //else
                //{
                //    t = (from m in context.TerminalTypes
                //         where m.Name == Name && m.MerchantID == merchantid && m.prefix == prefix
                //         select m).FirstOrDefault();
                //}

                if (t != null)
                {
                    tType.TerminalType_key = t.TerminalType_key;
                    tType.Name = t.Name;
                    tType.prefix = t.prefix;
                    tType.CostPerTerminal = Convert.ToDecimal(t.CostPerTerminal);
                    tType.CostPerSim = Convert.ToDecimal(t.CostPerSim);
                    tType.AvailableForPreorder = t.AvailableForPreorder;
                    tType.MerchantID = t.MerchantID;
                    tType.VirtualTerminal = t.VirtualTerminal;
                    tType.NextSequence = t.NextSequence;
                    tType.member_key = t.member_key;
                }
            }
            return tType;
        }


        public List<TerminalTypeModel> GetVirtualTerminalTypeList()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var datas = context.TerminalTypes.Where(n => n.VirtualTerminal == true).OrderBy(n => n.Name)
                    .ToList();

                return datas.Select(n => new TerminalTypeModel()
                {
                    Name = n.Name,
                    TerminalType_key = n.TerminalType_key
                }).ToList();
            }
        }


    }
}

