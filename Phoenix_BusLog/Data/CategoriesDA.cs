﻿using System;
using System.Collections.Generic;
using System.Linq;
using Phoenix_Service;
using PhoenixObjects.Enums;
using System.Data.Entity;
using Phoenix_BusLog.Model;
using PhoenixObjects.GlideBoxCategory;
using Phoenix_BusLog.TransactionHost.BusLogs;
using Phoenix_TransactionHostUI;

namespace Phoenix_BusLog.Data
{
    public class CategoriesDA
    {

        public List<vw_PHW_Glidebox_Manage_GetCategories> GetAllActiveCategoriesList(string categoryName)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                var categories = tranHostDbContext.vw_PHW_Glidebox_Manage_GetCategories
                    .Where(n => n.IsActive)
                    .AsQueryable();
                if (!string.IsNullOrEmpty(categoryName))
                {
                    categories = categories.Where(x => x.CategoryName.Contains(categoryName));
                }
                return categories.OrderBy(x => x.CategoryName).ToList();
            }
        }

        public Tuple<List<TerminalCategory>, List<TerminalCategoryHistory>> MapCategoryBelongToTerminal(List<string> terminalIdList, List<CategoryOrder> categoryList, out List<string> terminalIdAdded, out List<string> terminalIdInvalid, out List<string> terminalIdExist, out List<string> terminalIdNoSupport, string userName, string sourceName)
        {
            terminalIdAdded = new List<string>();
            terminalIdInvalid = new List<string>();
            terminalIdExist = new List<string>();
            terminalIdNoSupport = new List<string>();
            var terminalCategoryList = new List<TerminalCategory>();
            var terminalCategoryHistoryList = new List<TerminalCategoryHistory>();
            var tda = new TerminalHostDA();

            //get terminalId from DB for check terminal Id is valid from client
            var terminalIds = GetTerminalIdByTerminalIdList(terminalIdList);

            //get TerminalCategory from DB for check terminal Id have category
            var currentCategoryAssignees = GetTerCategoriesByTerminalIds(terminalIdList);

            foreach (var terminalId in terminalIdList)
            {
                var memberKey = tda.GetMemberKeyAllocatedInTerminal(terminalId);
                var isAllowAssignCategory = true;
                if (memberKey != 0)
                {
                    isAllowAssignCategory = tda.IsAllowAccessToGlidebox(memberKey);
                }

                //terminal id is belong to business type, which no support enable glidebox 
                if (!isAllowAssignCategory)
                {
                    terminalIdNoSupport.Add(terminalId);
                    continue;
                }


                //terminal id not found
                if (!terminalIds.Contains(terminalId))
                {
                    terminalIdInvalid.Add(terminalId);
                    continue;
                }

                var isExistCategory = currentCategoryAssignees.Any(x => x.TerminalID == terminalId);

                if (isExistCategory)
                {
                    terminalIdExist.Add(terminalId);
                    continue;
                }

                foreach (var category in categoryList)
                {
                    var newTerminalCategory = new TerminalCategory()
                    {
                        IsActive = true,
                        TerminalID = terminalId,
                        CategoryID = category.CategoryID,
                        AssignedDatetime = DateTime.Now,
                        AssignedBy = userName,
                        OrderCategory = category.Order
                    };

                    //log history when assign category to terminal
                    var newHistory = new TerminalCategoryHistory()
                    {
                        ChangedBy = userName,
                        ChangedDatetime = DateTime.Now,
                        CategoryID = category.CategoryID,
                        IsActive = true,
                        TerminalID = terminalId,
                        Source = sourceName,
                        NewOrder = category.Order
                    };

                    terminalCategoryList.Add(newTerminalCategory);
                    terminalCategoryHistoryList.Add(newHistory);
                }
                terminalIdAdded.Add(terminalId);
            }

            return Tuple.Create(terminalCategoryList, terminalCategoryHistoryList);
        }

        public List<CategoryOrder> GetCategoryByCategoryList(List<CategoryOrder> categoryList)
        {
            var categoryIdList = categoryList.Select(x => x.CategoryID).ToList();
            var categorys = new List<GlideBoxCategory>();
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                categorys = tranHostDbContext.GlideBoxCategories
                    .Where(x => categoryIdList.Contains(x.CategoryID) && x.IsActive)
                    .ToList();
            }

            return categorys.Select(x => new CategoryOrder
            {
                CategoryID = x.CategoryID,
                CategoryName = x.CategoryName,
                Order = categoryList.First(y => y.CategoryID == x.CategoryID).Order
            }).ToList();
        }

        public List<string> GetTerminalIdByTerminalIdList(List<string> terminalIdList)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.Terminals.Where(x => terminalIdList.Contains(x.TerminalID))
                    .Select(x => x.TerminalID)
                    .ToList();
            }
        }

        public List<GetCategoryDetailInTerminal_Result> GetCategoryDetailInTerminal(string terminalId)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.GetCategoryDetailInTerminal(terminalId).ToList();
            }
        }

        public List<TerminalCategory> GetTerCategoriesByTerminalId(string terminalId)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.TerminalCategories.Where(x => x.TerminalID == terminalId).ToList();
            }
        }

        public bool IsExistTerminal(string terminalId)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.Terminals.Any(x => x.TerminalID == terminalId);
            }
        }

        public void UpdateCategoryInterminal(string terminalId, List<CategoryOrder> categoryList, string userName, string sourceName)
        {
            var currentCategoryIdAssigned = GetTerCategoriesByTerminalId(terminalId).Select(x => x.CategoryID);
            var newCategories =
                GetCategoryByCategoryList(categoryList.Where(x => !currentCategoryIdAssigned.Contains(x.CategoryID))
                    .ToList());

            var categoriesNeedToBuildGlideboxItems = new List<TerminalCategory>();

            //add new category to terminal
            var newTerminalCategory = new List<TerminalCategory>();
            var newTerminalCategoryHistory = new List<TerminalCategoryHistory>();
            foreach (var item in newCategories)
            {
                var terminalCategories = new TerminalCategory()
                {
                    CategoryID = item.CategoryID,
                    OrderCategory = item.Order,
                    TerminalID = terminalId,
                    IsActive = true,
                    AssignedBy = userName,
                    AssignedDatetime = DateTime.Now,
                };

                var history = new TerminalCategoryHistory()
                {
                    ChangedBy = userName,
                    ChangedDatetime = DateTime.Now,
                    CategoryID = item.CategoryID,
                    IsActive = true,
                    TerminalID = terminalId,
                    Source = sourceName,
                    NewOrder = item.Order
                };

                newTerminalCategoryHistory.Add(history);
                newTerminalCategory.Add(terminalCategories);
                categoriesNeedToBuildGlideboxItems.Add(terminalCategories);
            }

            using (var tranHostDbContext = new TransactionHostEntities())
            {

                var currentTerminalCategory =
                    tranHostDbContext.TerminalCategories.Where(x => currentCategoryIdAssigned.Contains(x.CategoryID) && x.TerminalID == terminalId);

                foreach (var item in currentTerminalCategory)
                {
                    var updateTerminalCategory = categoryList.FirstOrDefault(x => x.CategoryID == item.CategoryID);
                    if (updateTerminalCategory != null) //case update
                    {
                        //only update if difference
                        if (updateTerminalCategory.Order != item.OrderCategory)
                        {
                            var history = new TerminalCategoryHistory()
                            {
                                ChangedBy = userName,
                                ChangedDatetime = DateTime.Now,
                                CategoryID = item.CategoryID,
                                TerminalID = terminalId,
                                Source = sourceName,
                                NewOrder = updateTerminalCategory.Order,
                                OldOrder = item.OrderCategory
                            };

                            item.OrderCategory = updateTerminalCategory.Order;
                            newTerminalCategoryHistory.Add(history);
                        }
                        categoriesNeedToBuildGlideboxItems.Add(item);
                    }
                    else //case delete
                    {
                        var history = new TerminalCategoryHistory()
                        {
                            ChangedBy = userName,
                            ChangedDatetime = DateTime.Now,
                            CategoryID = item.CategoryID,
                            TerminalID = terminalId,
                            Source = sourceName,
                            IsDelete = true
                        };

                        newTerminalCategoryHistory.Add(history);
                        tranHostDbContext.TerminalCategories.Remove(item);
                    }
                }

                //insert new terminal category if have
                if (newTerminalCategory.Count > 0)
                {
                    tranHostDbContext.TerminalCategories.AddRange(newTerminalCategory);
                }

                //insert new terminal category if have
                if (newTerminalCategoryHistory.Count > 0)
                {
                    tranHostDbContext.TerminalCategoryHistories.AddRange(newTerminalCategoryHistory);
                }

                if (categoriesNeedToBuildGlideboxItems.Count > 0)
                {
                    var glideboxItems = GetGlideboxItemsToDisplayOnTerminal(categoriesNeedToBuildGlideboxItems);

                    var currentGlidebox = tranHostDbContext.Glideboxes.FirstOrDefault(x => x.TerminalID == terminalId);
                    if (currentGlidebox != null)
                    {
                        if (currentGlidebox.GlideboxItem != glideboxItems)
                        {
                            currentGlidebox.GlideboxItem = glideboxItems;
                            currentGlidebox.VersionNo += 1;
                            currentGlidebox.UpdatedDatetime = DateTime.Now;
                            currentGlidebox.UpdatedBy = userName;
                        }
                    }
                    else
                    {
                        var newGlidebox = new Glidebox
                        {
                            TerminalID = terminalId,
                            GlideboxItem = glideboxItems,
                            VersionNo = 1,
                            CreatedDatetime = DateTime.Now,
                            CreatedBy = userName
                        };
                        tranHostDbContext.Glideboxes.Add(newGlidebox);
                    }
                }

                tranHostDbContext.SaveChanges();
            }
        }

        public List<CategoryOrder> GetCategoryInTerminal(string terminalId)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.TerminalCategories.Where(x => x.TerminalID == terminalId).Select(x => new CategoryOrder()
                {
                    CategoryID = x.CategoryID,
                    Order = x.OrderCategory,
                }).ToList();
            }
        }

        public void DeleteCategoryInTerminal(string terminalId, string userName, string sourceName)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                var terminalCategory = tranHostDbContext.TerminalCategories.Where(x => x.TerminalID == terminalId).ToList();
                var historyList = terminalCategory.Select(x => new TerminalCategoryHistory()
                {
                    CategoryID = x.CategoryID,
                    TerminalID = terminalId,
                    Source = sourceName,
                    ChangedBy = userName,
                    ChangedDatetime = DateTime.Now,
                    IsDelete = true
                }).ToList();

                var currentGlideboxItem = tranHostDbContext.Glideboxes.FirstOrDefault(x => x.TerminalID == terminalId);
                if (currentGlideboxItem != null)
                {
                    currentGlideboxItem.GlideboxItem = string.Empty;
                    currentGlideboxItem.VersionNo += 1;
                    currentGlideboxItem.UpdatedDatetime = DateTime.Now;
                    currentGlideboxItem.UpdatedBy = userName;
                }

                tranHostDbContext.TerminalCategoryHistories.AddRange(historyList);
                tranHostDbContext.TerminalCategories.RemoveRange(terminalCategory);

                tranHostDbContext.SaveChanges();
            }
        }

        public List<TerminalCategory> GetTerCategoriesByTerminalIds(List<string> terminalIdList)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.TerminalCategories.Where(x => terminalIdList.Contains(x.TerminalID)).ToList();
            }
        }

        public void SaveTerminalCategory(Tuple<List<TerminalCategory>, List<TerminalCategoryHistory>> dataTuple)
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                //add TerminalCategory
                tranHostDbContext.TerminalCategories.AddRange(dataTuple.Item1);
                //add History
                tranHostDbContext.TerminalCategoryHistories.AddRange(dataTuple.Item2);

                #region Add Glidebox Items
                var terminalIDs = dataTuple.Item1.Select(x => x.TerminalID).Distinct();

                foreach (var terminalID in terminalIDs)
                {
                    var terminalCategories = dataTuple.Item1.Where(x => x.TerminalID == terminalID);
                    var glideboxItems = GetGlideboxItemsToDisplayOnTerminal(terminalCategories.ToList());

                    var currentGlidebox = tranHostDbContext.Glideboxes.FirstOrDefault(x => x.TerminalID == terminalID);

                    if (currentGlidebox != null)
                    {
                        if (currentGlidebox.GlideboxItem != glideboxItems)
                        {
                            currentGlidebox.GlideboxItem = glideboxItems;
                            currentGlidebox.VersionNo += 1;
                            currentGlidebox.UpdatedDatetime = DateTime.Now;
                            currentGlidebox.UpdatedBy = terminalCategories.First().AssignedBy;
                        }
                    }
                    else
                    {
                        var newGlidebox = new Glidebox
                        {
                            TerminalID = terminalID,
                            GlideboxItem = glideboxItems,
                            VersionNo = 1,
                            CreatedDatetime = DateTime.Now,
                            CreatedBy = terminalCategories.First().AssignedBy
                        };
                        tranHostDbContext.Glideboxes.Add(newGlidebox);
                    }                   
                }
                #endregion

                tranHostDbContext.SaveChanges();
            }
        }

        public object GetCategories(int pageSize, int currentPage, string searchText = "", string sortOrder = "", string sortName = "")
        {
            using (var entities = new TransactionHostEntities())
            {
                var items = entities.vw_PHW_Glidebox_Manage_GetCategories.AsQueryable();

                if (!string.IsNullOrEmpty(searchText))
                {
                    items = items.Where(n => n.CategoryName.ToLower().Contains(searchText.ToLower()));
                }

                items = SortCategoryList(items, sortName, sortOrder);//handle sorting
                int totalRecords = items.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var skip = (currentPage - 1) * pageSize;
                var res = items.Skip(skip).Take(pageSize);

                return new
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
            }
        }

        public IQueryable<vw_PHW_Glidebox_Manage_GetCategories> SortCategoryList(IQueryable<vw_PHW_Glidebox_Manage_GetCategories> items, string sortName, string sortOrder)
        {
            IQueryable<vw_PHW_Glidebox_Manage_GetCategories> itemsSorter;
            switch (sortName)
            {
                case "CategoryName":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.CategoryName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.CategoryName);
                    }
                    break;
                case "Price":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Price.Length).ThenBy(n => n.Price);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Price.Length).ThenByDescending(n => n.Price);
                    }
                    break;
                case "TotalProduct":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.TotalProduct);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.TotalProduct);
                    }
                    break;
                case "CreatedDatetime":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.CategoryID);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.CategoryID);
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.CategoryID);
                    break;
            }
            return itemsSorter;
        }

        public void CreateCategory(CategoryModel model, string createdBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var category = new GlideBoxCategory
                {
                    CategoryName = model.CategoryName.Trim(),
                    ShortName = model.ShortName.Trim(),
                    Price = model.PriceCategory,
                    CommissionAmount = model.CommissionAmount,
                    IsActive = true,
                    CreatedDatetime = DateTime.Now,
                    CreatedBy = createdBy
                };

                entities.GlideBoxCategories.Add(category);

                var categoryHistory = new GlideBoxCategoryHistory
                {
                    CategoryID = category.CategoryID,
                    NewCategoryName = category.CategoryName,
                    NewShortName = category.ShortName,
                    NewPrice = category.Price,
                    NewCommissionAmount = category.CommissionAmount,
                    IsActive = true,
                    Source = "Create",
                    ChangedDatetime = DateTime.Now,
                    ChangedBy = createdBy
                };

                entities.GlideBoxCategoryHistories.Add(categoryHistory);
                entities.SaveChanges();
            }
        }

        public bool EditCategory(CategoryModel model, string changedBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var category = entities.GlideBoxCategories.FirstOrDefault(x => x.CategoryID == model.CategoryID);

                if (category == null)
                {
                    return false;
                }

                var isChanged = false;
                var isNeedUpdateGlideboxItem = false;
                var categoryHistory = new GlideBoxCategoryHistory();
                if (category.CategoryName != model.CategoryName)
                {
                    categoryHistory.OldCategoryName = category.CategoryName;
                    categoryHistory.NewCategoryName = model.CategoryName;
                    category.CategoryName = model.CategoryName.Trim();
                    isChanged = true;
                }

                if (category.ShortName != model.ShortName)
                {
                    categoryHistory.OldShortName = category.ShortName;
                    categoryHistory.NewShortName = model.ShortName;
                    category.ShortName = model.ShortName.Trim();
                    isChanged = true;
                    isNeedUpdateGlideboxItem = true;
                }

                if (category.Price != model.PriceCategory)
                {
                    categoryHistory.OldPrice = category.Price;
                    categoryHistory.NewPrice = model.PriceCategory;
                    category.Price = model.PriceCategory;
                    isChanged = true;
                    isNeedUpdateGlideboxItem = true;
                }

                if (category.CommissionAmount != model.CommissionAmount)
                {
                    categoryHistory.OldCommissionAmount = category.CommissionAmount;
                    categoryHistory.NewCommissionAmount = model.CommissionAmount;
                    category.CommissionAmount = model.CommissionAmount;
                    isChanged = true;
                }

                if (isChanged)
                {
                    categoryHistory.CategoryID = category.CategoryID;
                    categoryHistory.Source = "Edit";
                    categoryHistory.ChangedDatetime = DateTime.Now;
                    categoryHistory.ChangedBy = changedBy;
                    entities.GlideBoxCategoryHistories.Add(categoryHistory);

                    if (isNeedUpdateGlideboxItem)
                    {
                        var terminalIDs = entities.TerminalCategories.Where(x => x.CategoryID == category.CategoryID).Select(x => x.TerminalID).ToList();
                        foreach (var terminalID in terminalIDs)
                        {
                            var terminalCategories = entities.TerminalCategories.Where(x => x.TerminalID == terminalID).ToList();

                            string glideboxItems = GetGlideboxItemsToDisplayOnTerminal(terminalCategories, category);

                            var currentGlidebox = entities.Glideboxes.FirstOrDefault(x => x.TerminalID == terminalID);
                            if (currentGlidebox != null)
                            {
                                if (currentGlidebox.GlideboxItem != glideboxItems)
                                {
                                    currentGlidebox.GlideboxItem = glideboxItems;
                                    currentGlidebox.VersionNo += 1;
                                    currentGlidebox.UpdatedDatetime = DateTime.Now;
                                    currentGlidebox.UpdatedBy = changedBy;
                                }
                            }
                        }
                    }                   

                    entities.SaveChanges();
                }
            }

            return true;
        }

        public object GetCategoryByID(int categoryID)
        {
            using (var entities = new TransactionHostEntities())
            {
                var category = entities.GlideBoxCategories.FirstOrDefault(n => n.CategoryID == categoryID);

                return new
                {
                    CategoryID = category.CategoryID,
                    CategoryName = category.CategoryName,
                    ShortName = category.ShortName,
                    Price = category.Price,
                    CommissionAmount = category.CommissionAmount
                };
            }
        }

        public bool IsCurrentCategoryAssignedToTerminals(int categoryID)
        {
            using (var entities = new TransactionHostEntities())
            {
                var terminalCategory = entities.TerminalCategories.FirstOrDefault(n => n.CategoryID == categoryID);

                if (terminalCategory != null)
                {
                    return true;
                }
                return false;
            }
        }

        public bool ActivateDeactivateCategory(ActivateDeactivateCategoryModel model, string changedBy)
        {
            using (var entities = new TransactionHostEntities())
            {
                var category = entities.GlideBoxCategories.FirstOrDefault(x => x.CategoryID == model.ID);

                if (category == null)
                {
                    return false;
                }

                var categoryHistory = new GlideBoxCategoryHistory();

                if (model.IsActivateAction)
                {
                    category.IsActive = true;
                    categoryHistory.IsActive = true;
                    categoryHistory.Source = "Activate";
                    if (!string.IsNullOrEmpty(model.Reason?.Trim()))
                    {
                        categoryHistory.Reason = model.Reason.Trim();
                    }
                }
                else
                {
                    category.IsActive = false;
                    categoryHistory.IsActive = false;
                    categoryHistory.Reason = model.Reason.Trim();
                    categoryHistory.Source = "Deactivate";
                }

                categoryHistory.CategoryID = model.ID;
                categoryHistory.ChangedDatetime = DateTime.Now;
                categoryHistory.ChangedBy = changedBy;

                entities.GlideBoxCategoryHistories.Add(categoryHistory);
                entities.SaveChanges();
            }

            return true;
        }

        public object SearchCategoryHistory(int categoryID, int pageSize, int currentPage, string searchText = "", string sortOrder = "", string sortName = "")
        {
            using (var entities = new TransactionHostEntities())
            {
                var items = entities.usp_PHW_Glidebox_SearchCategoryHistory(categoryID, searchText).ToList();
                items = SortCategoryHistoryList(items, sortName, sortOrder);
                int totalRecords = items.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var skip = (currentPage - 1) * pageSize;
                var res = items.Skip(skip).Take(pageSize);

                return new
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = res
                };
            }
        }

        public List<usp_PHW_Glidebox_SearchCategoryHistory_Result> SortCategoryHistoryList(List<usp_PHW_Glidebox_SearchCategoryHistory_Result> items, string sortName, string sortOrder)
        {
            List<usp_PHW_Glidebox_SearchCategoryHistory_Result> itemsSorter;
            switch (sortName)
            {
                case "ChangedDatetime":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(x => x.ChangedDatetime).ToList();
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ChangedDatetime).ToList();
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.ChangedDatetime).ToList();
                    break;
            }
            return itemsSorter;
        }

        public bool IsExistCategoryName(CategoryModel model)
        {
            var categoryName = model.CategoryName.Trim().ToLower();
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                if (model.IsCreateCategoryAction)
                {
                    return tranHostDbContext.GlideBoxCategories.Any(x => x.CategoryName.Trim().ToLower() == categoryName);
                }
                else
                {
                    return tranHostDbContext.GlideBoxCategories.Any(x =>
                        x.CategoryName.Trim().ToLower() == categoryName && x.CategoryID != model.CategoryID);
                }
            }
        }

        public bool IsExistCategoryCode(CategoryModel model)
        {
            var categoryCode = model.ShortName.Trim().ToLower();
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                if (model.IsCreateCategoryAction)
                {
                    return tranHostDbContext.GlideBoxCategories.Any(x => x.ShortName.Trim().ToLower() == categoryCode);
                }
                else
                {
                    return tranHostDbContext.GlideBoxCategories.Any(x =>
                        x.ShortName.Trim().ToLower() == categoryCode && x.CategoryID != model.CategoryID);
                }
            }
        }

        public int GetTotalCategory()
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.GlideBoxCategories.Count();
            }
        }

        public List<GlideBoxCategory> GetAllCategory()
        {
            using (var entities = new TransactionHostEntities())
            {
                return entities.GlideBoxCategories.ToList();
            }
        }

        public string GetGlideboxItemsToDisplayOnTerminal(List<TerminalCategory> terminalCategories, GlideBoxCategory newGlideBoxCategory = null)
        {
            if (terminalCategories.Count > 0)
            {
                List<string> glideboxComponents = new List<string>();
                using (var entities = new TransactionHostEntities())
                {
                    var categories = terminalCategories.Where(x => x.IsActive == true)
                                                   .OrderBy(x => x.OrderCategory)
                                                   .Join(entities.GlideBoxCategories, tc => tc.CategoryID, c => c.CategoryID, (tc, c) => new GlideboxCategoriesModel
                                                   {
                                                       CategoryID = c.CategoryID,
                                                       ShortName = c.ShortName,
                                                       Price = c.Price,
                                                       OrderCategory = tc.OrderCategory
                                                   }).ToList();


                    for (int i = 1; i < 5; i++)
                    {
                        var item = categories.FirstOrDefault(n=>n.OrderCategory == i);
                        if (item == null)
                        {
                            glideboxComponents.Add("");
                            continue;
                        }
                        if (newGlideBoxCategory != null)
                        {
                            if (item.CategoryID == newGlideBoxCategory.CategoryID)
                            {
                                item.ShortName = newGlideBoxCategory.ShortName;
                                item.Price = newGlideBoxCategory.Price;
                            }
                        }

                        glideboxComponents.Add($"{item.CategoryID},{item.ShortName},{(int)(item.Price * 100)}");
                    }

                    //foreach (var item in categories)
                    //{
                    //    if (newGlideBoxCategory != null)
                    //    {
                    //        if (item.CategoryID == newGlideBoxCategory.CategoryID)
                    //        {
                    //            item.ShortName = newGlideBoxCategory.ShortName;
                    //            item.Price = newGlideBoxCategory.Price;
                    //        }
                    //    }

                    //    glideboxComponents.Add($"{item.CategoryID},{item.ShortName},{(int)(item.Price * 100)}");
                    //}
                }

                if (glideboxComponents.Count > 0)
                {
                    return string.Join("|", glideboxComponents);
                }                
            }

            return string.Empty;
        }

        public List<GlideboxCategoriesCommissionForSameDayCashingModel> GetGlideboxCategoriesCommissionForSameDay()
        {
            using (var tranHostDbContext = new TransactionHostEntities())
            {
                return tranHostDbContext.usp_PHW_WebCashing_GetGlideboxCategoriesCommissionForSameDay().Select(x => new GlideboxCategoriesCommissionForSameDayCashingModel(x.CategoryID, x.ShortName, x.Price, x.CommissionAmount, x.IsPercentage)).ToList();
            }
        }
    }
}