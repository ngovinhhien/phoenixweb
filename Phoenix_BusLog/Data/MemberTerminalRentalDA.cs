﻿using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Phoenix_BusLog.Data
{
    public class MemberTerminalRentalDA
    {
       
        public void Add(List<TerminalRentalRateModel> TerminalRentalRateList, int Member_key, DateTime StartDate, DateTime EndDate, string username)
        {
            //get 
            List<MemberTerminalRentalModel> MemberCurrentTerminalRentalList = GetKeyByMemberKey(Member_key);
            if (MemberCurrentTerminalRentalList != null)
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var lastDayOfLastMonth = firstDayOfMonth.AddDays(-1);
                foreach (var mCurrent in MemberCurrentTerminalRentalList)
                {
                    UpdateEndDate(mCurrent.MemberTerminalRentalKey, lastDayOfLastMonth, username);
                }
            }

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                foreach (var m in TerminalRentalRateList)
                {
                    MemberTerminalRental memberTerminalRental = new MemberTerminalRental();
                    memberTerminalRental.Member_key = Member_key;
                    memberTerminalRental.RentalAmount = m.RentAmount;
                    memberTerminalRental.TerminalRentalRate_Key = m.TerminalRentalRate_key;
                    if (StartDate != DateTime.MinValue)
                    {
                        memberTerminalRental.StartDate = StartDate;
                    }
                    if (EndDate != DateTime.MinValue)
                    {
                        memberTerminalRental.EndDate = EndDate;
                    }
                    memberTerminalRental.Modified_dttm = DateTime.Now;
                    memberTerminalRental.ModifiedByUser = username;
                    memberTerminalRental.MemberRentalTemplateKey = m.MemberRentalTemplateKey;

                    context.MemberTerminalRentals.Add(memberTerminalRental);

                }
                context.SaveChanges();
            }
        }

        public void AddNew(int MemberTerminalRentalKey,DateTime EndDate, string username, decimal RentalAmount)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var lastDayOfLastMonth = firstDayOfMonth.AddDays(-1);
                
                var mem = (from m in context.MemberTerminalRentals
                           where m.MemberTerminalRentalKey == MemberTerminalRentalKey
                           select m).FirstOrDefault();

                UpdateEndDate(MemberTerminalRentalKey, lastDayOfLastMonth, username);
                
                    MemberTerminalRental memberTerminalRental = new MemberTerminalRental();
                    memberTerminalRental.Member_key = mem.Member_key;
                    memberTerminalRental.RentalAmount = RentalAmount;
                    memberTerminalRental.TerminalRentalRate_Key = mem.TerminalRentalRate_Key;
                    if (firstDayOfMonth != DateTime.MinValue)
                    {
                        memberTerminalRental.StartDate = firstDayOfMonth;
                    }
                    if (EndDate != DateTime.MinValue)
                    {
                        memberTerminalRental.EndDate = EndDate;
                    }
                    memberTerminalRental.Modified_dttm = DateTime.Now;
                    memberTerminalRental.ModifiedByUser = username;
                    memberTerminalRental.MemberRentalTemplateKey = mem.MemberRentalTemplateKey;

                    context.MemberTerminalRentals.Add(memberTerminalRental);
                              
                context.SaveChanges();
            }
        }


        public void UpdateEndDate(int MemberTerminalRentalKey, DateTime EndDate, string username)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MemberTerminalRentals
                           where m.MemberTerminalRentalKey == MemberTerminalRentalKey
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    mem.EndDate = EndDate;
                    mem.ModifiedByUser = username;
                    mem.Modified_dttm = DateTime.Now;
                }
                context.SaveChanges();
            }
        }
        public MemberTerminalRentalModel GetMemberTerminalRental(int memberTerminalRental_key)
        {
            MemberTerminalRentalModel m = new MemberTerminalRentalModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rental = (from r in context.MemberTerminalRentals
                              where r.MemberTerminalRentalKey == memberTerminalRental_key
                              select r).FirstOrDefault();

                var rate = (from r in context.TerminalRentalRates
                            where r.TerminalRentalRate_key == rental.TerminalRentalRate_Key
                            select r).FirstOrDefault();

                if (rental != null)
                {
                    m.EndDate = rental.EndDate;
                    m.Member_key = rental.Member_key;
                    m.MemberTerminalRentalKey = rental.MemberTerminalRentalKey;
                    m.Modified_dttm = rental.Modified_dttm;
                    m.ModifiedByUser = rental.ModifiedByUser;
                    m.RentalAmount = rental.RentalAmount;
                    m.StartDate = rental.StartDate;
                    m.TerminalRentalRate_Key = rental.TerminalRentalRate_Key;
                    if(rate != null)
                        m.TransactionTarget = rate.TransactionTarget;
                }
            }
            return m;
        }
        public List<MemberTerminalRentalModel> GetKeyByMemberKey(int Memberkey)
        {
            List<MemberTerminalRentalModel> MemberTerminalRentalList = new List<MemberTerminalRentalModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = from m in context.MemberTerminalRentals
                          where m.Member_key == Memberkey && (m.EndDate == null || m.EndDate > DateTime.Now)
                          select m;
                if (mem != null)
                {
                    foreach (var m in mem)
                    {

                        var rate = (from r in context.TerminalRentalRates
                                  where r.TerminalRentalRate_key == m.TerminalRentalRate_Key
                                  select r).FirstOrDefault();

                        MemberTerminalRentalModel mTerminalRental = new MemberTerminalRentalModel();
                        mTerminalRental.MemberTerminalRentalKey = m.MemberTerminalRentalKey;
                        mTerminalRental.Member_key = m.Member_key;
                        mTerminalRental.RentalAmount = m.RentalAmount;
                        mTerminalRental.StartDate = m.StartDate;
                        mTerminalRental.EndDate = m.EndDate;
                        mTerminalRental.Modified_dttm = m.Modified_dttm;
                        mTerminalRental.ModifiedByUser = m.ModifiedByUser;
                        mTerminalRental.TerminalRentalRate_Key = m.TerminalRentalRate_Key;
                        if (rate != null)
                        mTerminalRental.TransactionTarget = rate.TransactionTarget;
                        MemberTerminalRentalList.Add(mTerminalRental);
                    }
                }
            }
            return MemberTerminalRentalList;
        }
        
        public void UpdateEndDate(TaxiEpayEntities context, int MemberKey, int MemberRentalTemplateKey, DateTime EndDate, string username)
        {
            var terminalRentals = (from m in context.MemberTerminalRentals
                where m.MemberRentalTemplateKey == MemberRentalTemplateKey && m.Member_key == MemberKey
                select m).ToList();
            
            terminalRentals.ForEach(t =>
            {
                t.EndDate = EndDate;
                t.ModifiedByUser = username;
                t.Modified_dttm = DateTime.Now;
            });
        }
    }
}
