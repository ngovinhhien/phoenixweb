﻿using Phoenix_Service;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class EmployeeDA
    {
       public List<string> GetEmployeeAutoComplete(string term)
       {
           List<string> memberList = new List<string>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {

               var addr = (from m in context.Employees
                           where m.FirstName.StartsWith(term) && m.Active == true
                           select m).Take(10);
               if (addr != null)
               {
                   foreach (var n in addr)
                   {
                       string name = n.FirstName + " " + n.LastName + "," + n.Employee_key;
                       memberList.Add(name);
                   }
               }
           }

           return memberList;
       }

       public int GetEmployeeByName(string term)
       {
           int Employee_key = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {

               var addr = (from m in context.Employees
                           where m.FirstName ==term && m.Active == true
                           select m).FirstOrDefault();
               if (addr != null)
               {
                   Employee_key = addr.Employee_key;
               }
           }
           return Employee_key;
       }

       public List<EmployeeModel> Get()
       {
           List<EmployeeModel> EmployeeList = new List<EmployeeModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var addr = from m in context.Employees
                           where m.Active == true
                           select m;
               if (addr != null)
               {
                   foreach (var m in addr)
                   {
                       EmployeeModel emp = new EmployeeModel();
                       emp.Employee_key = m.Employee_key;
                       emp.EmployeeID = m.EmployeeID;
                       emp.StartDate = m.StartDate;
                       emp.EndDate = m.EndDate;
                       emp.Role_key = m.Role_key;
                       emp.Office_key = m.Office_key;
                       emp.Manager_key = m.Manager_key;
                       emp.FirstName = m.FirstName;
                       emp.LastName = m.LastName;
                       emp.Telephone = m.Telephone;
                       emp.Mobile = m.Mobile;
                       emp.Email = m.Email;
                       emp.PhoenixAdminUsername = m.PhoenixAdminUsername;
                       emp.Modified_dttm = m.Modified_dttm;
                       emp.ModifiedByUser = m.ModifiedByUser;

                       EmployeeList.Add(emp);
                   }
               }
           }

           return EmployeeList;
       }

       public int GetEmployeeById(int key)
       {
           int memberkey = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {

               var addr = (from m in context.Employees
                           where m.Employee_key == key && m.Active == true
                           select m).FirstOrDefault();
               if (addr != null)
               {
                   memberkey = addr.Employee_key;
               }
           }

           return memberkey;
       }

    }
}
