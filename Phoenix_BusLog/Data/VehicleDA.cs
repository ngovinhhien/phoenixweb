﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class VehicleDA
    {
        public int AddNew(VehicleModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Vehicle mServer = new Vehicle();
                mServer.VehicleId = t.VehicleId;
                mServer.RegisteredState = t.RegisteredState;
                mServer.TerminalAllocatedDate = t.TerminalAllocatedDate;
                mServer.Modified_dttm = t.Modified_dttm;
                mServer.Operator_key = t.Operator_key;
                mServer.ModifiedByUser = t.ModifiedByUser;
                mServer.Modified_dttm = t.Modified_dttm;
                mServer.VehicleType_key = t.VehicleType_key;
                mServer.CreatedDate = t.CreatedDate;
                mServer.EftTerminal_key = t.EftTerminal_key;

                context.Vehicles.Add(mServer);
                context.SaveChanges();
                return mServer.Vehicle_key;

            }
        }


        public void Update(VehicleModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var ve = (from v in context.Vehicles
                          where v.Vehicle_key == t.Vehicle_key
                          select v).FirstOrDefault();

                if (ve != null)
                {
                    ve.TerminalAllocatedDate = t.TerminalAllocatedDate;
                    ve.Modified_dttm = t.Modified_dttm;
                    ve.ModifiedByUser = t.ModifiedByUser;
                    ve.Modified_dttm = t.Modified_dttm;
                    ve.EftTerminal_key = t.EftTerminal_key;
                }
                context.SaveChanges();

            }
        }


        public VehicleModel Get(int vehicle_key)
        {
            VehicleModel mServer = new VehicleModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var t = (from v in context.Vehicles
                         where v.Vehicle_key == vehicle_key
                         select v).FirstOrDefault();
                if (t != null)
                {
                    mServer.VehicleId = t.VehicleId;
                    mServer.RegisteredState = t.RegisteredState;
                    mServer.TerminalAllocatedDate = t.TerminalAllocatedDate;
                    mServer.Modified_dttm = t.Modified_dttm;
                    mServer.Operator_key = t.Operator_key;
                    mServer.ModifiedByUser = t.ModifiedByUser;
                    mServer.Modified_dttm = t.Modified_dttm;
                    mServer.VehicleType_key = t.VehicleType_key;
                    mServer.CreatedDate = t.CreatedDate;
                    mServer.EftTerminal_key = t.EftTerminal_key;
                    mServer.Vehicle_key = t.Vehicle_key;
                }
            }
            return mServer;
        }
    }
}
