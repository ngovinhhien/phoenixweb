﻿using Phoenix_Service;
using System;
using System.Globalization;
using System.Linq;

namespace Phoenix_BusLog.Data
{
    public class UserResetPasswordHistoryDA
    {
        public object GetUserResetPasswordHistory(string fromDate, string toDate, string searchText, int pageSize, int pageNumber)
        {
            var dateformat = "dd-MM-yyyy";

            var firstDate = DateTime.ParseExact(fromDate.ToString(), dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");
            var endDate = DateTime.ParseExact(toDate.ToString(), dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");

            using (var entities = new aspnetdbEntities())
            {
                var historyList = entities.Database.SqlQuery<usp_GetUserResetPasswordHistory_Result>
                ($"EXEC usp_GetUserResetPasswordHistory @FromDate='{firstDate}', @ToDate='{endDate}', @SearchText='{searchText}'");

                var totalRecords = historyList.Count();
                var totalPages = (int)Math.Ceiling((double)totalRecords / (double)pageSize);

                var data = historyList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                return new
                {
                    total = totalPages,
                    page = pageNumber,
                    records = totalRecords,
                    rows = data
                };
            }
        }
    }
}