﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
namespace Phoenix_BusLog.Data
{
    public class MemberHistoryDA
    {
        public void Add(int memberKey, string history, string username)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var newHistory = new MemberHistory()
                {
                    Member_key = memberKey,
                    History = history,
                    CreatedBy = username,
                    CreatedDate = DateTime.Now,
                };
                context.MemberHistories.Add(newHistory);
                context.SaveChanges();
            }
        }


    }
}
