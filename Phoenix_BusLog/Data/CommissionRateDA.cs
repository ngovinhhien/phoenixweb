﻿using Phoenix_Service;
using PhoenixObjects.Cashing;
using PhoenixObjects.CommissionRate;
using PhoenixObjects.Common;
using PhoenixObjects.Financial;
using PhoenixObjects.Member;
using PhoenixObjects.Member.CommissionRate;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Phoenix_BusLog.Data
{
    public class CommissionRateDA
    {
        public int AddNew(CommissionRateModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                CommissionRate mServer = new CommissionRate();
                mServer.GrossDealerCommissionRate = t.GrossDealerCommissionRate;
                mServer.OperatorCommissionRate = t.OperatorCommissionRate;
                mServer.ManualCommissionRate = t.ManualCommissionRate;
                mServer.StartDate = t.StartDate;
                mServer.EndDate = t.EndDate;
                mServer.Current = t.Current;
                mServer.Modified_dttm = t.Modified_dttm;
                mServer.ModifiedByUser = t.ModifiedByUser;
                mServer.Member_key = t.Member_key;
                mServer.DocketPayMethod_key = t.DocketPayMethod_key;
                mServer.FixAmount = t.FixAmount;
                mServer.CustomCommissionRate = t.CustomCommissionRate;
                mServer.TransactionFee = t.TransactionFee;
                mServer.RebateForPurchaseWithCashout = t.RebateForPurchaseWithCashOut;
                mServer.RebateForCashAdvance = t.RebateForCashAdvance;

                context.CommissionRates.Add(mServer);
                context.SaveChanges();
                return mServer.CommissionRate_key;
            }
        }

        public bool ProcessInsertAllNewCommssionRate(MemberModel model, int member_key, string username, string DebitType, DateTime startdate, string QantasType = "Percentage")
        {
            bool IsAdded = false;
            MerchantAccountDA mdac = new MerchantAccountDA();
            string MerchanId = mdac.GetMerchantId(model.CompanyKey);
            string merchantIdstring = string.Empty;
            merchantIdstring = MerchanId;

            int merchantAccountkey = mdac.GetMerchantAccountKey(merchantIdstring, model.CompanyKey);

            List<CommissionRateModel> comRateList = new List<CommissionRateModel>();
            PayMethodDA pda = new PayMethodDA();
            DocketPayMethodDA cda = new DocketPayMethodDA();
            VehicleTypeDA vda0 = new VehicleTypeDA();
            //master card
            CommissionRateModel comRateMaster = new CommissionRateModel();
            comRateMaster.GrossDealerCommissionRate = model.CRMasterCardRate;// +model.CRDealerRate;
            comRateMaster.OperatorCommissionRate = model.CRMasterCardRate;
            comRateMaster.ManualCommissionRate = model.CRMasterCardRate;// model.CRManualVoucherRate;
            comRateMaster.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateMaster.EndDate = null;
            comRateMaster.Current = true;
            comRateMaster.Member_key = member_key;
            int pKey = pda.GetPayMethod_key("MasterCard");
            int vkey0 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey = cda.GetDocketPayMethod_key(pKey, vkey0, merchantAccountkey);
            comRateMaster.DocketPayMethod_key = dKey;
            comRateList.Add(comRateMaster);

            //Visa card
            CommissionRateModel comRateVisa = new CommissionRateModel();
            comRateVisa.GrossDealerCommissionRate = model.CRVisaRate;// +model.CRDealerRate;
            comRateVisa.OperatorCommissionRate = model.CRVisaRate;
            comRateVisa.ManualCommissionRate = model.CRVisaRate;// model.CRManualVoucherRate;
            comRateVisa.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateVisa.EndDate = null;
            comRateVisa.Current = true;
            comRateVisa.Member_key = member_key;
            int pKey1 = pda.GetPayMethod_key("Visa");
            int vkey1 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey1 = cda.GetDocketPayMethod_key(pKey1, vkey1, merchantAccountkey);
            comRateVisa.DocketPayMethod_key = dKey1;
            comRateList.Add(comRateVisa);

            //Amex card
            CommissionRateModel comRateAmex = new CommissionRateModel();
            comRateAmex.GrossDealerCommissionRate = model.CRAmexRate;// +model.CRDealerRate;
            comRateAmex.OperatorCommissionRate = model.CRAmexRate;
            comRateAmex.ManualCommissionRate = model.CRAmexRate; //model.CRManualVoucherRate;
            comRateAmex.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateAmex.EndDate = null;
            comRateAmex.Current = true;
            comRateAmex.Member_key = member_key;
            int pKey2 = pda.GetPayMethod_key("AMEX");
            int vkey2 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey2 = cda.GetDocketPayMethod_key(pKey2, vkey2, merchantAccountkey);
            comRateAmex.DocketPayMethod_key = dKey2;
            comRateList.Add(comRateAmex);

            //Diners card
            CommissionRateModel comRateDiners = new CommissionRateModel();
            comRateDiners.GrossDealerCommissionRate = model.CRDinersRate;// +model.CRDealerRate;
            comRateDiners.OperatorCommissionRate = model.CRDinersRate;
            comRateDiners.ManualCommissionRate = model.CRDinersRate; //model.CRManualVoucherRate;
            comRateDiners.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateDiners.EndDate = null;
            comRateDiners.Current = true;
            comRateDiners.Member_key = member_key;
            int pKey3 = pda.GetPayMethod_key("Diners");
            int vkey3 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey3 = cda.GetDocketPayMethod_key(pKey3, vkey3, merchantAccountkey);
            comRateDiners.DocketPayMethod_key = dKey3;
            comRateList.Add(comRateDiners);



            //Union Pay
            CommissionRateModel comRateUPI = new CommissionRateModel();
            comRateUPI.GrossDealerCommissionRate = model.CRUnionPayRate;// +model.CRDealerRate;
            comRateUPI.OperatorCommissionRate = model.CRUnionPayRate;
            comRateUPI.ManualCommissionRate = model.CRUnionPayRate; //model.CRManualVoucherRate;
            comRateUPI.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateUPI.EndDate = null;
            comRateUPI.Current = true;
            comRateUPI.Member_key = member_key;
            int pKeyUPI = pda.GetPayMethod_key("UnionPay");
            int vkeyUPI = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKeyUPI = cda.GetDocketPayMethod_key(pKeyUPI, vkeyUPI, merchantAccountkey);
            comRateUPI.DocketPayMethod_key = dKeyUPI;
            comRateList.Add(comRateUPI);



            //Zip
            CommissionRateModel comRateZip = new CommissionRateModel();
            comRateZip.GrossDealerCommissionRate = model.CRZipPayRate;// +model.CRDealerRate;
            comRateZip.OperatorCommissionRate = model.CRZipPayRate;
            comRateZip.ManualCommissionRate = model.CRZipPayRate; //model.CRManualVoucherRate;
            comRateZip.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateZip.EndDate = null;
            comRateZip.Current = true;
            comRateZip.Member_key = member_key;
            int pKeyZIP = pda.GetPayMethod_key("Zip");
            int vkeyZIP = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKeyZIP = cda.GetDocketPayMethod_key(pKeyZIP, vkeyZIP, merchantAccountkey);
            comRateZip.DocketPayMethod_key = dKeyZIP;
            comRateList.Add(comRateZip);


            //Qantas

            CommissionRateModel comRateQantas = new CommissionRateModel();
            if (QantasType == "Percentage")
            {
                comRateQantas.GrossDealerCommissionRate = model.CRQantasRatePercentage;
                comRateQantas.OperatorCommissionRate = model.CRQantasRatePercentage;
                comRateQantas.ManualCommissionRate = model.CRQantasRatePercentage;
            }
            else
            {
                comRateQantas.GrossDealerCommissionRate = model.CRQantasRate;
                comRateQantas.OperatorCommissionRate = model.CRQantasRate;
                comRateQantas.ManualCommissionRate = model.CRQantasRate;
                comRateQantas.FixAmount = true;
            }
            comRateQantas.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateQantas.EndDate = null;
            comRateQantas.Current = true;
            comRateQantas.Member_key = member_key;
            int pKeyQantas = pda.GetPayMethod_key("QantasQFF");
            int vkeyQantas = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKeyQantas = cda.GetDocketPayMethod_key(pKeyQantas, vkeyQantas, merchantAccountkey);
            comRateQantas.DocketPayMethod_key = dKeyQantas;
            comRateList.Add(comRateQantas);


            //Alipay card
            CommissionRateModel comRateAlipay = new CommissionRateModel();
            comRateAlipay.GrossDealerCommissionRate = model.CRAlipayRate;// +model.CRDealerRate;
            comRateAlipay.OperatorCommissionRate = model.CRAlipayRate;
            comRateAlipay.ManualCommissionRate = model.CRAlipayRate; //model.CRManualVoucherRate;
            comRateAlipay.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateAlipay.EndDate = null;
            comRateAlipay.Current = true;
            comRateAlipay.Member_key = member_key;
            int pKeyAlipay = pda.GetPayMethod_key("Alipay");
            int vkeyAlipay = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKeyAlipay = cda.GetDocketPayMethod_key(pKeyAlipay, vkeyAlipay, merchantAccountkey);
            comRateAlipay.DocketPayMethod_key = dKeyAlipay;
            comRateList.Add(comRateAlipay);

            //JCB card
            CommissionRateModel comRateJCB = new CommissionRateModel();
            comRateJCB.GrossDealerCommissionRate = model.CRJCBRate;
            comRateJCB.OperatorCommissionRate = model.CRJCBRate;
            comRateJCB.ManualCommissionRate = model.CRJCBRate;
            comRateJCB.StartDate = startdate;
            comRateJCB.EndDate = null;
            comRateJCB.Current = true;
            comRateJCB.Member_key = member_key;
            int pKeyJCB = pda.GetPayMethod_key("JCB");
            int vkeyJCB = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKeyJCB = cda.GetDocketPayMethod_key(pKeyJCB, vkeyJCB, merchantAccountkey);
            comRateJCB.DocketPayMethod_key = dKeyJCB;
            comRateList.Add(comRateJCB);


            //Debit card
            CommissionRateModel comRateDebit = new CommissionRateModel();

            if (DebitType == "Percentage")
            {
                comRateDebit.GrossDealerCommissionRate = model.CRDebitRatePercentage;// +model.CRDealerRate;
                comRateDebit.OperatorCommissionRate = model.CRDebitRatePercentage;
                comRateDebit.ManualCommissionRate = model.CRDebitRatePercentage;//model.CRManualVoucherRate;
            }
            else
            {
                comRateDebit.GrossDealerCommissionRate = model.CRDebitRate;
                comRateDebit.OperatorCommissionRate = model.CRDebitRate;
                comRateDebit.ManualCommissionRate = model.CRDebitRate;
                comRateDebit.FixAmount = true;
            }

            comRateDebit.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateDebit.EndDate = null;
            comRateDebit.Current = true;
            comRateDebit.Member_key = member_key;
            int pKey4 = pda.GetPayMethod_key("Debit");
            int vkey4 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey4 = cda.GetDocketPayMethod_key(pKey4, vkey4, merchantAccountkey);
            comRateDebit.DocketPayMethod_key = dKey4;
            comRateList.Add(comRateDebit);

            //Eftpos card
            CommissionRateModel comRateEftpos = new CommissionRateModel();
            comRateEftpos.GrossDealerCommissionRate = model.CRDebitRatePercentage;// +model.CRDealerRate;
            if (DebitType == "Percentage")
            {
                comRateEftpos.OperatorCommissionRate = model.CRDebitRatePercentage;
                comRateEftpos.ManualCommissionRate = model.CRDebitRatePercentage; //model.CRManualVoucherRate;
            }
            else
            {
                comRateEftpos.GrossDealerCommissionRate = model.CRDebitRate;
                comRateEftpos.OperatorCommissionRate = model.CRDebitRate;
                comRateEftpos.ManualCommissionRate = model.CRDebitRate;
                comRateDebit.FixAmount = true;
            }

            comRateEftpos.StartDate = startdate;// new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);// DateTime.Now;
            comRateEftpos.EndDate = null;
            comRateEftpos.Current = true;
            comRateEftpos.Member_key = member_key;
            int pKey5 = pda.GetPayMethod_key("Eftpos");
            int vkey5 = vda0.GetVehicleTypeKey(model.CompanyKey);
            int dKey5 = cda.GetDocketPayMethod_key(pKey5, vkey5, merchantAccountkey);
            comRateEftpos.DocketPayMethod_key = dKey5;
            comRateList.Add(comRateEftpos);

            foreach (var t in comRateList)
            {
                if (t.OperatorCommissionRate != null)
                {
                    CommissionRateDA coda = new CommissionRateDA();
                    CommissionRateModel m1 = new CommissionRateModel();
                    m1.GrossDealerCommissionRate = t.GrossDealerCommissionRate;
                    m1.OperatorCommissionRate = t.OperatorCommissionRate;
                    m1.ManualCommissionRate = t.ManualCommissionRate;
                    m1.StartDate = t.StartDate;
                    m1.EndDate = t.EndDate;
                    m1.Current = t.Current;
                    m1.Modified_dttm = DateTime.Now;
                    m1.ModifiedByUser = username;
                    m1.CommissionRate_key = t.CommissionRate_key;
                    m1.Member_key = t.Member_key;
                    m1.DocketPayMethod_key = t.DocketPayMethod_key;
                    m1.FixAmount = t.FixAmount;
                    coda.AddNew(m1);
                }
            }

            IsAdded = true;

            return IsAdded;
        }

        public void UpdateMemberCommissionRate(int key, decimal rateAmt, string user, int Member_key)
        {

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                CommissionRate rate = context.CommissionRates.Where(r => r.CommissionRate_key == key).FirstOrDefault();

                if (rate != null)
                {
                    rate.OperatorCommissionRate = rateAmt;
                    rate.Member_key = Member_key;
                    rate.GrossDealerCommissionRate = rateAmt;
                    rate.ManualCommissionRate = rateAmt;
                    rate.Modified_dttm = DateTime.Now;
                    rate.ModifiedByUser = user;
                }

                context.SaveChanges();
            }
        }

        public void UpdateDebitMemberCommissionRate(int key, decimal rateAmt, string user, int Member_key)
        {
            MemberDA mda = new MemberDA();
            var mo = mda.GetMemeber(Member_key);

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                CommissionRate rate = context.CommissionRates.Where(r => r.CommissionRate_key == key).FirstOrDefault();

                if (rate != null)
                {
                    rate.OperatorCommissionRate = rateAmt;
                    rate.Member_key = Member_key;
                    rate.GrossDealerCommissionRate = rateAmt;
                    rate.ManualCommissionRate = rateAmt;
                    rate.Modified_dttm = DateTime.Now;
                    if (mo.CompanyKey == 18)
                    {
                        rate.FixAmount = true;
                    }
                    rate.ModifiedByUser = user;
                }

                context.SaveChanges();
            }
        }

        public DefaultRateModel GetMemberCommissionRate(int vehicleTypeKey, int member_Key, int Company_key)
        {

            string merchantIdstring = string.Empty;

            var mdac = new MerchantAccountDA();
            string MerchanId = mdac.GetMerchantId(Company_key);
            merchantIdstring = MerchanId;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rates = (from r in context.MemberCommissionRates
                             where r.Member_key == member_Key && r.vehicletype_key == vehicleTypeKey && r.MerchantId == merchantIdstring
                             select r).ToList();

                var tranfees = (from r in context.CommissionRates

                                            join dpm in context.DocketPayMethods on r.DocketPayMethod_key equals dpm.DocketPayMethod_key
                                            join pm in context.PayMethods on dpm.PayMethod_key equals pm.PayMethod_key
                                            join ma in context.MerchantAccounts on dpm.MerchantAccount_key equals ma.MerchantAccount_key
                                            where r.Member_key == member_Key && dpm.VehicleType_key == vehicleTypeKey && ma.MerchantId == merchantIdstring && r.TransactionFee != null && r.Current == true
                                            select new { pm.PayMethod_key, r.TransactionFee }).ToList();
                                            
                var subBusinessName = (from v in context.VehicleTypes where v.VehicleType_key == vehicleTypeKey select v.Name).FirstOrDefault();
                var businessName = (from v in context.Companies where v.Company_key == Company_key select v.Name).FirstOrDefault();
                var d = new DefaultRateModel
                {
                    SubBusinessType = subBusinessName,
                    BusinessType = businessName
                };

                foreach (var r in rates)
                {

                    var tranfee = tranfees.FirstOrDefault(t => t.PayMethod_key == r.PayMethod_key);
                    
                    //29/07/2016 Vu added a field if rate is default 
                    if (r.IsDefaultRate == false)
                    {
                        switch (r.PayMethod_key)
                        {
                            case 1: //"Visa"
                            {
                                // d.Visa = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Visa = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.VisaTranFee = tranfee?.TransactionFee;
                                d.Visa_Commision_Key = r.CommissionRate_key;
                                break;
                            }
                            case 24: //"VISA DEBIT"
                            {
                                // d.Visa = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.VisaDebit = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.VisaDebitTranFee = tranfee?.TransactionFee;
                                d.VisaDebit_Commision_Key = r.CommissionRate_key;
                                break;
                            }
                            case 2: //"MasterCard"
                            {
                                //d.Master = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Master = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.MasterTranFee = tranfee?.TransactionFee;
                                d.Master_Commision_Key = r.CommissionRate_key;
                                break;
                            }
                            case 25: //"MASTERCARD DEBIT"
                            {
                                //d.Master = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.MasterDebit = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.MasterDebitTranFee = tranfee?.TransactionFee;
                                d.MasterDebit_Commision_Key = r.CommissionRate_key;
                                break;

                            }
                            case 3: //"AMEX"
                            {
                                //d.Amex = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Amex = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.AmexTranFee = tranfee?.TransactionFee;
                                d.Amex_Commision_Key = r.CommissionRate_key;
                                break;


                            }
                            case 4: //"Diners"
                            {
                                //d.Diners = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Diners = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.DinersTranFee = tranfee?.TransactionFee;
                                d.Diners_Commision_Key = r.CommissionRate_key;
                                break;


                            }
                            case 17: //"UnionPay"
                            {
                                //d.Diners = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.UnionPay = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.UnionPayTranFee = tranfee?.TransactionFee;
                                d.UnionPay_Commision_Key = r.CommissionRate_key;
                                break;


                            }
                            case 18: //"Zip"
                            {
                                //d.Diners = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Zip = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.ZipTranFee = tranfee?.TransactionFee;
                                d.Zip_Commision_Key = r.CommissionRate_key;
                                break;


                            }
                            case 19: //"Alipay"
                            {
                                //d.Diners = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.Alipay = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.AlipayTranFee = tranfee?.TransactionFee;
                                d.Alipay_Commision_Key = r.CommissionRate_key;
                                break;
                            }
                            case 21: //"JCB"
                            {
                                d.JCB = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.JCBTranFee = tranfee?.TransactionFee;
                                d.JCB_Commision_Key = r.CommissionRate_key;
                                break;
                            }
                            case 20: //"QantasQFF"
                            {

                                d.QantasIsFixed = r.FixAmount.GetValueOrDefault(false);
                                d.Qantas = Math.Round(r.OperatorCommissionRate.Value, 6);
                                d.Qantas_Commision_Key = r.CommissionRate_key;
                                d.QantasPercentage = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.QantasPercentage_Commision_Key = r.CommissionRate_key;
                                d.QantasTranFee = tranfee?.TransactionFee;
                                break;
                            }
                            case 6: //"Debit"
                            {
                                if (r.FixAmount == true)
                                {
                                    d.DebitIsFixed = true;
                                }
                                else
                                {
                                    d.DebitIsFixed = false;
                                }

                                //d.Debit =Math.Abs(Math.Round(r.OperatorCommissionRate.Value, 2));
                                d.Debit = Math.Round(r.OperatorCommissionRate.Value, 6);
                                d.Debit_Commision_Key = r.CommissionRate_key;
                                d.DebitTranFee = tranfee?.TransactionFee;
                                d.DebitRebateForPurchaseWithCashOut = r.RebateForPurchaseWithCashout;
                                d.DebitRebateForCashAdvance = r.RebateForCashAdvance;
                                //}
                                //else
                                //{
                                //d.DebitPercentage = Math.Abs(Math.Round(r.OperatorCommissionRate.Value * 100, 2));
                                d.DebitPercentage = Math.Round(r.OperatorCommissionRate.Value * 100, 4);
                                d.DebitPercentage_Commision_Key = r.CommissionRate_key;
                                //}
                                break;
                            }
                        }
                    }
                }
                return d;
            }

            return null;
        }

        /// <summary>
        /// Gets the Sameday Commission Rates for a member
        /// </summary>
        /// <param name="Member_key">Member key</param>
        /// <param name="Businesstype">Business type</param>
        /// <returns>List of Commission rate by Card type</returns>
        public List<CommissionRate_MemberRateModel> GetCurrentMemberSameDayRate(int Member_key, string BusinessType)
        {
            List<CommissionRate_MemberRateModel> model = new List<CommissionRate_MemberRateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rates = context.WebCashing_GetsamedayCommissionRate(Member_key).Where(n => n.BusinessType == BusinessType).OrderBy(n => n.Name).ToList();
                //#if DEBUG
                //                WebCashing_GetsamedayCommissionRate_Result r = new WebCashing_GetsamedayCommissionRate_Result();
                //                r.BusinessType = "";
                //                r.CommissionRate_key = 1;
                //                r.DocketPayMethod_key = 1;

                //                r.HandlingRate =0.02;
                //                r.ManualCommissionRate = 0.05m;
                //                r.member_key = 23;
                //                r.memberid = "23";
                //                r.MerchantId = "2232";
                //                r.Name = "rr";
                //                r.OperatorCommissionRate = 0.04m;

                //                r.vehicletype_key = 21;

                //#endif

                if (rates != null)
                {
                    foreach (var rate in rates)
                    {

                        CommissionRate_MemberRateModel ComRate = new CommissionRate_MemberRateModel();
                        ComRate.BusinessType = rate.BusinessType;
                        ComRate.CommissionRate_key = rate.CommissionRate_key;

                        ComRate.DocketPayMethod_key = rate.DocketPayMethod_key;
                        ComRate.DocketPayMethod_key1 = rate.DocketPayMethod_key + 1000;
                        ComRate.vehicletype_key = rate.vehicletype_key;
                        ComRate.FlatAmount = rate.FlatAmount;
                        ComRate.HandlingAmount = rate.HandlingAmount;
                        ComRate.HandlingRate = rate.HandlingRate;
                        ComRate.ManualCommissionRate = Math.Round((Convert.ToDecimal(rate.ManualCommissionRate) * 100), 2);
                        ComRate.member_key = rate.member_key;
                        ComRate.memberid = rate.memberid;
                        ComRate.MerchantId = rate.MerchantId;
                        ComRate.Name = rate.Name;
                        ComRate.OperatorCommissionRate = Math.Round((Convert.ToDecimal(rate.OperatorCommissionRate) * 100), 2);
                        ComRate.vehicletype_key = rate.vehicletype_key;
                        ComRate.FaceValue = 0;
                        if (ComRate.FlatAmount == true)
                        {
                            ComRate.HandlingCharge = "$" + Math.Round((Convert.ToDecimal(ComRate.HandlingAmount) * 100), 2);
                        }
                        else
                        {
                            ComRate.HandlingCharge = Math.Round((Convert.ToDecimal(ComRate.HandlingRate) * 100), 2) + "%";
                            if (ComRate.HandlingCharge == "0%")
                            {
                                ComRate.HandlingCharge = "0.00%";
                            }
                        }
                        model.Add(ComRate);
                    }
                }
            }
            return model;
        }


        public List<CommissionRate_MemberRateModel> GetCurrentMemberRate(int Member_key, string Businesstype)
        {
            List<CommissionRate_MemberRateModel> model = new List<CommissionRate_MemberRateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var rates = context.WebCashing_GetManualVoucherCommissionRate(Member_key).Where(n => n.BusinessType == Businesstype).OrderBy(n => n.Name).ToList();

                if (rates != null)
                {
                    foreach (var rate in rates)
                    {

                        CommissionRate_MemberRateModel ComRate = new CommissionRate_MemberRateModel();
                        ComRate.BusinessType = rate.BusinessType;
                        ComRate.CommissionRate_key = rate.CommissionRate_key;

                        ComRate.DocketPayMethod_key = rate.DocketPayMethod_key;
                        ComRate.DocketPayMethod_key1 = rate.DocketPayMethod_key + 1000;
                        ComRate.vehicletype_key = rate.vehicletype_key;
                        ComRate.FlatAmount = rate.FlatAmount;
                        ComRate.HandlingAmount = rate.HandlingAmount;
                        ComRate.HandlingRate = rate.HandlingRate;
                        ComRate.ManualCommissionRate = Math.Round((Convert.ToDecimal(rate.ManualCommissionRate) * 100), 2);
                        ComRate.member_key = rate.member_key;
                        ComRate.memberid = rate.memberid;
                        ComRate.MerchantId = rate.MerchantId;
                        ComRate.Name = rate.Name;
                        ComRate.OperatorCommissionRate = Math.Round((Convert.ToDecimal(rate.OperatorCommissionRate) * 100), 2);
                        ComRate.vehicletype_key = rate.vehicletype_key;
                        ComRate.FaceValue = 0;
                        if (ComRate.FlatAmount == true)
                        {
                            ComRate.HandlingCharge = "$" + Math.Round((Convert.ToDecimal(ComRate.HandlingAmount) * 100), 2);
                        }
                        else
                        {
                            ComRate.HandlingCharge = Math.Round((Convert.ToDecimal(ComRate.HandlingRate) * 100), 2) + "%";
                            if (ComRate.HandlingCharge == "0%")
                            {
                                ComRate.HandlingCharge = "0.00%";
                            }
                        }
                        model.Add(ComRate);
                    }
                }
            }
            return model;
        }


        public ManualCashing GetCurrentMemberBusinessTypeList(int Member_key)
        {
            ManualCashing model = new ManualCashing();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var all = context.WebCashing_GetManualVoucherCommissionRate(Member_key).ToList();
                var rates = context.WebCashing_GetManualVoucherCommissionRate(Member_key).Select(n => n.BusinessType).Distinct().ToList();

                if (rates != null)
                {
                    List<ServiceFeeRadio> sss = new List<ServiceFeeRadio>();
                    foreach (var s in rates)
                    {
                        var servicefee = all.Where(n => n.BusinessType == s).FirstOrDefault().ServiceFee;
                        ServiceFeeRadio ss = new ServiceFeeRadio();
                        ss.BusinessType = s;
                        ss.ServiceFeeRate = servicefee.GetValueOrDefault(0);
                        ss.ServiceFeeRate = Math.Round(ss.ServiceFeeRate * 100, 2);
                        sss.Add(ss);
                        model.ServiceFeeRadioList = sss;
                    }
                }
            }
            return model;
        }


        //public DefaultRateModel GetMemberCommissionRate(int companykey, string merchantAccount, int member_Key)
        //{
        //    string[] CardName = new string[] { "Visa", "MasterCard", "AMEX", "Diners", "Debit" };
        //    DefaultRateModel commissionRate = new DefaultRateModel();
        //    string Visa = string.Empty;
        //    string MasterCard = string.Empty;
        //    string AMEX = string.Empty;
        //    string Diners = string.Empty;

        //    for (int i = 0; i < 5; i++)
        //    {
        //        using (TaxiEpayEntities context = new TaxiEpayEntities())
        //        {
        //            string name = CardName[i];

        //            MerchantAccountDA mda = new MerchantAccountDA();
        //            int merchantAccountkey = mda.GetMerchantAccountKey(merchantAccount,companykey);
        //            PayMethodDA pda = new PayMethodDA();
        //            int pKey = pda.GetPayMethod_key(name);
        //            DocketPayMethodDA cda = new DocketPayMethodDA();
        //            int dKey = cda.GetDocketPayMethod_key(pKey, companykey, merchantAccountkey);

        //            var rate = (from r in context.CommissionRates
        //                        where r.Member_key == member_Key && r.DocketPayMethod_key == dKey
        //                        select r).FirstOrDefault();
        //            if (rate != null)
        //            {
        //                if (name == "Visa")
        //                {
        //                    commissionRate.Visa = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
        //                    commissionRate.Visa_Commision_Key = rate.CommissionRate_key;
        //                }
        //                else if (name == "MasterCard")
        //                {
        //                    commissionRate.Master = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
        //                    commissionRate.Master_Commision_Key = rate.CommissionRate_key;
        //                }
        //                else if (name == "AMEX")
        //                {
        //                    commissionRate.Amex = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
        //                    commissionRate.Amex_Commision_Key = rate.CommissionRate_key;
        //                }
        //                else if (name == "Diners")
        //                {
        //                    commissionRate.Diners = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
        //                    commissionRate.Diners_Commision_Key = rate.CommissionRate_key;
        //                }
        //                else if (name == "Debit")
        //                {
        //                    commissionRate.Debit = rate.OperatorCommissionRate.Value;
        //                    commissionRate.DebitPercentage = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
        //                    commissionRate.Debit_Commision_Key = rate.CommissionRate_key;
        //                    commissionRate.DebitPercentage_Commision_Key = rate.CommissionRate_key;
        //                }
        //                else
        //                { }
        //            }
        //        }
        //    }
        //    return commissionRate;
        //}

        public List<DefaultRateModel> LoadDefaultCommissionRates(int companyKey, List<int> vehicleTypeKeys)
        {
            List<DefaultRateModel> result = new List<DefaultRateModel>();
            foreach (int key in vehicleTypeKeys)
            {
                result.Add(GetDefaultCommissionRate(companyKey, key));
            }
            return result;
        }
        public List<DefaultRateModel> GetAllDefaultCommissionRates(string state, int businessType, List<int> subBusinessTypeKeys)
        {
            List<DefaultRateModel> result = new List<DefaultRateModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                List<SqlParameter> sqlParameters = new List<SqlParameter>
                    {
                        new SqlParameter("@State", state),
                        new SqlParameter("@CompanyKey", businessType)
                    };
                var allRates = context.Database.SqlQuery<CommissionRateModel>("EXEC usp_CommissionRate_GetDefault @State, @CompanyKey", sqlParameters.ToArray()).ToList();
                foreach (var key in subBusinessTypeKeys)
                {
                    var filteredCommissionRates = allRates.Where(r => r.SubBusinessType == key.ToString())
                                        .ToList();
                    DefaultRateModel defaultRate = new DefaultRateModel();
                    defaultRate.Debit = filteredCommissionRates.Where(a => a.CardName == "Debit").Select(a => Math.Round(a.OperatorCommissionRate.Value, 2)).FirstOrDefault();
                    defaultRate.DebitPercentage = filteredCommissionRates.Where(a => a.CardName == "Debit").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Visa = filteredCommissionRates.Where(a => a.CardName == "Visa").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Master = filteredCommissionRates.Where(a => a.CardName == "MasterCard").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Amex = filteredCommissionRates.Where(a => a.CardName == "AMEX").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Diners = filteredCommissionRates.Where(a => a.CardName == "Diners").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.UnionPay = filteredCommissionRates.Where(a => a.CardName == "UnionPay").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Zip = filteredCommissionRates.Where(a => a.CardName == "Zip").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Alipay = filteredCommissionRates.Where(a => a.CardName == "Alipay").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.Qantas = filteredCommissionRates.Where(a => a.CardName == "QantasQFF").Select(a => Math.Round(a.OperatorCommissionRate.Value, 2)).FirstOrDefault();
                    defaultRate.QantasPercentage = filteredCommissionRates.Where(a => a.CardName == "QantasQFF").Select(a => Math.Round(a.OperatorCommissionRate.Value * 100, 2)).FirstOrDefault();
                    defaultRate.BusinessType = businessType.ToString();
                    defaultRate.SubBusinessType = key.ToString();
                    result.Add(defaultRate);
                }
            }
            return result;
        }

        public DefaultRateModel GetDefaultCommissionRate(int companykey, int vehicleTypeKey)
        {
            string[] CardName = new string[] { "Visa", "MasterCard", "AMEX", "Diners", "Debit", "UnionPay", "Zip", "Alipay", "QantasQFF" };
            List<string> Cardnames = CardName.ToList();
            DefaultRateModel commissionRate = new DefaultRateModel();
            commissionRate.BusinessType = companykey.ToString();
            commissionRate.SubBusinessType = vehicleTypeKey.ToString();

            //for (int i = 0; i < 5; i++)
            foreach (string cardname in Cardnames)
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    //string name = CardName[i];
                    string name = cardname;
                    var rate = (from r in context.DefaultCommissionRates
                                where r.Current == true && r.VehicleType_key == vehicleTypeKey && r.CardName == name
                                select r).FirstOrDefault();
                    if (rate != null)
                    {
                        if (name == "Visa")
                        {
                            // commissionRate.Visa = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100,2));
                            commissionRate.Visa = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Visa_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "MasterCard")
                        {
                            //commissionRate.Master = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Master = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Master_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "AMEX")
                        {
                            //commissionRate.Amex = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Amex = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Amex_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "Diners")
                        {
                            //commissionRate.Diners = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Diners = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Diners_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "UnionPay")
                        {
                            //commissionRate.Diners = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.UnionPay = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.UnionPay_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "Zip")
                        {
                            //commissionRate.Diners = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Zip = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Zip_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "Alipay")
                        {
                            //commissionRate.Diners = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Alipay = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Alipay_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "QantasQFF")
                        {
                            //commissionRate.Diners = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.Qantas = Math.Round(rate.OperatorCommissionRate.Value, 2);
                            commissionRate.QantasPercentage = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Qantas_Commision_Key = rate.CommissionRate_key;
                            commissionRate.QantasPercentage_Commision_Key = rate.CommissionRate_key;
                        }
                        else if (name == "Debit")
                        {
                            //commissionRate.Debit = Math.Abs(Math.Round(rate.OperatorCommissionRate.Value, 2));
                            commissionRate.Debit = Math.Round(rate.OperatorCommissionRate.Value, 2);
                            //commissionRate.DebitPercentage =Math.Abs(Math.Round(rate.OperatorCommissionRate.Value * 100, 2));
                            commissionRate.DebitPercentage = Math.Round(rate.OperatorCommissionRate.Value * 100, 2);
                            commissionRate.Debit_Commision_Key = rate.CommissionRate_key;
                            commissionRate.DebitPercentage_Commision_Key = rate.CommissionRate_key;
                        }

                        else
                        { }
                    }
                }
            }
            return commissionRate;
        }

        public List<SurchargeRate> GetSurchargeRatesForMember(int memberKey)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                try
                {
                    List<SqlParameter> sqlParameters = new List<SqlParameter>();
                    sqlParameters.Add(new SqlParameter("@memberKey", memberKey));
                    var response = con.Database.SqlQuery<SurchargeRate>("EXEC usp_Fuse_GetSurchargeRateForMember @memberKey", sqlParameters.ToArray()).ToList();
                    return response;
                }
                catch (Exception e)
                {
                    return new List<SurchargeRate>();
                }
            }
        }



        public bool InsertAllCommssionRateAndTranFee(MemberCommissionRateModel model, int memberKey, string username, string DebitType, DateTime startdate, string[] eCommerceVehicleTypeKeys, ref List<CardSurchargeInfo> cardSurchargeInfos, string QantasType = "Percentage", bool isFeeFreeMember = false)
        {
            var comRateList = new List<CommissionRateModel>();
            var pda = new PayMethodDA();
            var cda = new DocketPayMethodDA();
            var mdac = new MerchantAccountDA();
            var merchanId = mdac.GetMerchantId(model.BusinessTypeID);
            var merchantAccountkey = mdac.GetMerchantAccountKey(merchanId, model.BusinessTypeID);

            var startDate = DateTime.Now;
            var payMethodList = new List<int>() { 6, 1, 24, 25, 2, 3, 4, 17, 18, 20, 19, 21 };
            Dictionary<string, bool> updatedCommissionRateCards = new Dictionary<string, bool>();
            updatedCommissionRateCards.Add("mastercard", false);
            updatedCommissionRateCards.Add("visa", false);
            updatedCommissionRateCards.Add("unionpay", false);
            updatedCommissionRateCards.Add("amex", false);
            updatedCommissionRateCards.Add("jcb", false);


            Dictionary<string, decimal> customizedSurcharges = new Dictionary<string, decimal>();
            List<SurchargeRate> allSurcharges = new List<SurchargeRate>();
            if (eCommerceVehicleTypeKeys.Contains(model.SubBusinessTypeID.ToString()))
            {
                allSurcharges = GetSurchargeRatesForMember(memberKey).ToList();
                if (!isFeeFreeMember)
                {
                    customizedSurcharges = allSurcharges
                        .Where(r => r.SurchargeType == 2)
                        .ToDictionary(r => r.CardType.ToLower(), r => r.CommissionRate);
                }
            }

            foreach (var payMethodKey in payMethodList)
            {
                try
                {
                    var comRate = new CommissionRateModel
                    {
                        StartDate = startDate,
                        EndDate = null,
                        Current = true,
                        Member_key = memberKey
                    };
                    int pKey, dKey;

                    switch (payMethodKey)
                    {
                        case 6://"Debit"
                            if (model.DebitRateType == "Percentage")
                            {
                                comRate.OperatorCommissionRate = model.CRDebitRatePercentage;
                                comRate.ManualCommissionRate = model.CRDebitRatePercentage;
                            }
                            else
                            {
                                comRate.GrossDealerCommissionRate = model.CRDebitRate;
                                comRate.OperatorCommissionRate = model.CRDebitRate;
                                comRate.ManualCommissionRate = model.CRDebitRate;
                                comRate.FixAmount = true;
                            }

                            comRate.TransactionFee = model.CRDebitTranFee;
                            comRate.RebateForPurchaseWithCashOut = model.CRDebitRebateForPurchaseWithCashOut;
                            comRate.RebateForCashAdvance = model.CRDebitRebateForCashAdvance;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            break;
                        case 1://"Visa"
                            comRate.GrossDealerCommissionRate = model.CRVisaRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRVisaRate;
                            comRate.ManualCommissionRate = model.CRVisaRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRVisaTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("visa"))
                            {
                                if (model.GetMaxRate(model.CRVisaRate.Value) >= customizedSurcharges["visa"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["visa"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;

                                    var oldMSF = Math.Abs(allSurcharges.Where(s => s.CardType.ToLower() == "visa").Select(s => s.FullMSF.GetValueOrDefault(0)).SingleOrDefault()) * 100;
                                    if (oldMSF != Math.Abs(model.CRVisaRate.Value) * 100)
                                    {
                                        var newMSF = oldMSF == 0 ? 0 : Math.Abs(model.CRVisaRate.Value) * 100;
                                        var newSurchargeRate = oldMSF == 0 ? "0.00%" : comRate.CustomCommissionRate == null ? "Full MSF" : string.Format("{0:N2}%", comRate.CustomCommissionRate.GetValueOrDefault(0) * 100);
                                        CardSurchargeInfo info = new CardSurchargeInfo
                                        {
                                            CardName = "VISA",
                                            OldMSF = string.Format("{0:N2}%", oldMSF),
                                            NewSurchargeRate = newSurchargeRate,
                                            NewMSF = string.Format("{0:N2}%", newMSF)
                                        };
                                        cardSurchargeInfos.Add(info);
                                        updatedCommissionRateCards["visa"] = true;
                                    }
                                }
                            }
                            break;
                        case 24://"VISA DEBIT"
                            comRate.GrossDealerCommissionRate = model.CRVisaDebitRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRVisaDebitRate;
                            comRate.ManualCommissionRate = model.CRVisaDebitRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRVisaDebitTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            break;
                        case 2://"MasterCard"
                            comRate.GrossDealerCommissionRate = model.CRMasterCardRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRMasterCardRate;
                            comRate.ManualCommissionRate = model.CRMasterCardRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRMasterCardTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;

                            if (customizedSurcharges.ContainsKey("mastercard"))
                            {
                                if (model.GetMaxRate(model.CRMasterCardRate.Value) >= customizedSurcharges["mastercard"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["mastercard"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;
                                    var oldMSF = Math.Abs(allSurcharges.Where(s => s.CardType.ToLower() == "mastercard").Select(s => s.FullMSF.GetValueOrDefault(0)).SingleOrDefault()) * 100;
                                    if (oldMSF != Math.Abs(model.CRMasterCardRate.Value) * 100)
                                    {
                                        var newMSF = oldMSF == 0 ? 0 : Math.Abs(model.CRMasterCardRate.Value) * 100;
                                        var newSurchargeRate = oldMSF == 0 ? "0.00%" : comRate.CustomCommissionRate == null ? "Full MSF" : string.Format("{0:N2}%", comRate.CustomCommissionRate.GetValueOrDefault(0) * 100);
                                        CardSurchargeInfo info = new CardSurchargeInfo
                                        {
                                            CardName = "MASTERCARD",
                                            OldMSF = string.Format("{0:N2}%", oldMSF),
                                            NewSurchargeRate = newSurchargeRate,
                                            NewMSF = string.Format("{0:N2}%", newMSF)
                                        };
                                        cardSurchargeInfos.Add(info);
                                        updatedCommissionRateCards["mastercard"] = true;
                                    }
                                }
                            }
                            break;
                        case 25://"MASTERCARD DEBIT"
                            comRate.GrossDealerCommissionRate = model.CRMasterCardDebitRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRMasterCardDebitRate;
                            comRate.ManualCommissionRate = model.CRMasterCardDebitRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRMasterCardDebitTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            break;
                        case 3://"AMEX"
                            comRate.GrossDealerCommissionRate = model.CRAmexRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRAmexRate;
                            comRate.ManualCommissionRate = model.CRAmexRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRAmexTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("amex"))
                            {
                                if (model.GetMaxRate(model.CRAmexRate.Value) >= customizedSurcharges["amex"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["amex"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;

                                    var oldMSF = Math.Abs(allSurcharges.Where(s => s.CardType.ToLower() == "amex").Select(s => s.FullMSF.GetValueOrDefault(0)).SingleOrDefault()) * 100;
                                    if (oldMSF != Math.Abs(model.CRAmexRate.Value) * 100)
                                    {
                                        var newMSF = oldMSF == 0 ? 0 : Math.Abs(model.CRAmexRate.Value) * 100;
                                        var newSurchargeRate = oldMSF == 0 ? "0.00%" : comRate.CustomCommissionRate == null ? "Full MSF" : string.Format("{0:N2}%", comRate.CustomCommissionRate.GetValueOrDefault(0) * 100);
                                        CardSurchargeInfo info = new CardSurchargeInfo
                                        {
                                            CardName = "AMEX",
                                            OldMSF = string.Format("{0:N2}%", oldMSF),
                                            NewSurchargeRate = newSurchargeRate,
                                            NewMSF = string.Format("{0:N2}%", newMSF)
                                        };
                                        cardSurchargeInfos.Add(info);
                                        updatedCommissionRateCards["amex"] = true;
                                    }
                                }
                            }
                            break;
                        case 4://"Diners"
                            comRate.GrossDealerCommissionRate = model.CRDinersRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRDinersRate;
                            comRate.ManualCommissionRate = model.CRDinersRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRDinersTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("diners"))
                            {
                                if (model.GetMaxRate(model.CRDinersRate.Value) >= customizedSurcharges["diners"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["diners"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;
                                }
                            }
                            break;
                        case 17://"UnionPay"
                            comRate.GrossDealerCommissionRate = model.CRUnionPayRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRUnionPayRate;
                            comRate.ManualCommissionRate = model.CRUnionPayRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRUnionPayTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("unionpay"))
                            {
                                if (model.GetMaxRate(model.CRUnionPayRate.Value) >= customizedSurcharges["unionpay"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["unionpay"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;

                                    var oldMSF = Math.Abs(allSurcharges.Where(s => s.CardType.ToLower() == "unionpay").Select(s => s.FullMSF.GetValueOrDefault(0)).SingleOrDefault()) * 100;
                                    if (oldMSF != Math.Abs(model.CRUnionPayRate.Value) * 100)
                                    {
                                        var newMSF = oldMSF == 0 ? 0 : Math.Abs(model.CRUnionPayRate.Value) * 100;
                                        var newSurchargeRate = oldMSF == 0 ? "0.00%" : comRate.CustomCommissionRate == null ? "Full MSF" : string.Format("{0:N2}%", comRate.CustomCommissionRate.GetValueOrDefault(0) * 100);
                                        CardSurchargeInfo info = new CardSurchargeInfo
                                        {
                                            CardName = "UNIONPAY",
                                            OldMSF = string.Format("{0:N2}%", oldMSF),
                                            NewSurchargeRate = newSurchargeRate,
                                            NewMSF = string.Format("{0:N2}%", newMSF)
                                        };
                                        cardSurchargeInfos.Add(info);
                                        updatedCommissionRateCards["unionpay"] = true;
                                    }

                                }
                            }
                            break;
                        case 18://"Zip"
                            comRate.GrossDealerCommissionRate = model.CRZipPayRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRZipPayRate;
                            comRate.ManualCommissionRate = model.CRZipPayRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRZipPayTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("zip"))
                            {
                                if (model.GetMaxRate(model.CRZipPayRate.Value) >= customizedSurcharges["zip"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["zip"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;
                                }
                            }
                            break;
                        case 20://"QantasQFF"
                            if (model.QantasRateType == "Percentage")
                            {
                                comRate.OperatorCommissionRate = model.CRQantasRatePercentage;
                                comRate.ManualCommissionRate = model.CRQantasRatePercentage;
                            }
                            else
                            {
                                comRate.GrossDealerCommissionRate = model.CRQantasRate;
                                comRate.OperatorCommissionRate = model.CRQantasRate;
                                comRate.ManualCommissionRate = model.CRQantasRate;
                                comRate.FixAmount = true;
                            }

                            comRate.TransactionFee = model.CRQantasTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            break;
                        case 19://"Alipay"
                            comRate.GrossDealerCommissionRate = model.CRAlipayRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRAlipayRate;
                            comRate.ManualCommissionRate = model.CRAlipayRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRAlipayTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("alipay"))
                            {
                                if (model.GetMaxRate(model.CRAlipayRate.Value) >= customizedSurcharges["alipay"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["alipay"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;
                                }
                            }

                            break;
                        case 21://"JCB"
                            comRate.GrossDealerCommissionRate = model.CRJCBRate;// +model.CRDealerRate;
                            comRate.OperatorCommissionRate = model.CRJCBRate;
                            comRate.ManualCommissionRate = model.CRJCBRate;// model.CRManualVoucherRate;
                            comRate.TransactionFee = model.CRJCBTranFee;
                            pKey = payMethodKey;
                            dKey = cda.GetDocketPayMethod_key(pKey, model.SubBusinessTypeID, merchantAccountkey);
                            comRate.DocketPayMethod_key = dKey;
                            if (customizedSurcharges.ContainsKey("jcb"))
                            {
                                if (model.GetMaxRate(model.CRJCBRate.Value) >= customizedSurcharges["jcb"])
                                {
                                    comRate.CustomCommissionRate = customizedSurcharges["jcb"];
                                }
                                else
                                {
                                    comRate.CustomCommissionRate = null;

                                    var oldMSF = Math.Abs(allSurcharges.Where(s => s.CardType.ToLower() == "jcb").Select(s => s.FullMSF.GetValueOrDefault(0)).SingleOrDefault()) * 100;
                                    if (oldMSF != Math.Abs(model.CRJCBRate.Value) * 100)
                                    {
                                        var newMSF = oldMSF == 0 ? 0 : Math.Abs(model.CRJCBRate.Value) * 100;
                                        var newSurchargeRate = oldMSF == 0 ? "0.00%" : comRate.CustomCommissionRate == null ? "Full MSF" : string.Format("{0:N2}%", comRate.CustomCommissionRate.GetValueOrDefault(0) * 100);
                                        CardSurchargeInfo info = new CardSurchargeInfo
                                        {
                                            CardName = "JCB",
                                            OldMSF = string.Format("{0:N2}%", oldMSF),
                                            NewSurchargeRate = newSurchargeRate,
                                            NewMSF = string.Format("{0:N2}%", newMSF)
                                        };
                                        cardSurchargeInfos.Add(info);
                                        updatedCommissionRateCards["jcb"] = true;
                                    }
                                }
                            }
                            break;
                    }

                    comRateList.Add(comRate);
                }
                catch (Exception e)
                {
                    //throw;
                }

                if (updatedCommissionRateCards.Where(c => c.Value == true).Any())
                {
                    var notUpdatedCards = updatedCommissionRateCards.Where(c => c.Value == false).ToDictionary(c => c.Key, c => c.Value);
                    foreach (var card in notUpdatedCards)
                    {
                        var surcharge = allSurcharges.Where(s => s.CardType.ToLower() == card.Key).SingleOrDefault();
                        decimal oldMSF = surcharge.FullMSF.GetValueOrDefault(0);
                        decimal newMSF = 0;

                        switch (card.Key)
                        {
                            case "mastercard":
                                newMSF = model.CRMasterCardRate.HasValue ? model.CRMasterCardRate.Value : 0;
                                break;
                            case "visa":
                                newMSF = model.CRVisaRate.HasValue ? model.CRVisaRate.Value : 0;
                                break;
                            case "unionpay":
                                newMSF = model.CRUnionPayRate.HasValue ? model.CRUnionPayRate.Value : 0;
                                break;
                            case "amex":
                                newMSF = model.CRAmexRate.HasValue ? model.CRAmexRate.Value : 0;
                                break;
                            case "jcb":
                                newMSF = model.CRJCBRate.HasValue ? model.CRJCBRate.Value : 0;
                                break;
                        }
                        string newSurchargeRate = surcharge.CommissionRate <= Math.Abs(model.GetMaxRate(newMSF)) ? string.Format("{0:N2}%", surcharge.CommissionRate * 100) : "Full MSF";
                        if (surcharge.SurchargeType == 0 || surcharge.EnableCardType == false)
                        {
                            newSurchargeRate = "None";
                            if (newMSF == 0)
                            {
                                oldMSF = 0;
                            }
                        }

                        CardSurchargeInfo info = new CardSurchargeInfo
                        {
                            CardName = surcharge.CardType.ToUpper(),
                            OldMSF = string.Format("{0:N2}%", oldMSF * 100),
                            NewSurchargeRate = newSurchargeRate,
                            NewMSF = string.Format("{0:N2}%", Math.Abs(newMSF * 100))
                        };
                        cardSurchargeInfos.Add(info);
                    }
                }
            }

            comRateList.ForEach(t =>
            {
                var m1 = new CommissionRateModel
                {
                    GrossDealerCommissionRate = t.GrossDealerCommissionRate,
                    OperatorCommissionRate = t.OperatorCommissionRate,
                    ManualCommissionRate = t.ManualCommissionRate,
                    StartDate = t.StartDate,
                    EndDate = t.EndDate,
                    Current = t.Current,
                    Modified_dttm = DateTime.Now,
                    ModifiedByUser = username,
                    CommissionRate_key = t.CommissionRate_key,
                    Member_key = t.Member_key,
                    DocketPayMethod_key = t.DocketPayMethod_key,
                    FixAmount = t.FixAmount,
                    CustomCommissionRate = t.CustomCommissionRate,
                    TransactionFee = t.TransactionFee,
                    RebateForPurchaseWithCashOut = t.RebateForPurchaseWithCashOut,
                    RebateForCashAdvance = t.RebateForCashAdvance
                };

                new CommissionRateDA().AddNew(m1);
            });

            return true;
        }
    }
}

