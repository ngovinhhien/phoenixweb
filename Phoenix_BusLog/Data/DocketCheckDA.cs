﻿using log4net;
using Phoenix_Service;
using PhoenixObjects.Cashing;
using PhoenixObjects.GlideBoxCategory;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Phoenix_BusLog.Data
{
    public class DocketCheckDA : LiveBusLogic
    {
        public DocketCheckDA()
        { }
        public DocketCheckDA(ILog loggerfactory)
            : base(loggerfactory)
        {

        }
        public void CancelDocketCheck(int DocketCheckSummary)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.WebCashing_Cancel(DocketCheckSummary);
            }
        }
        public WebCashing_PayDockets_Result PayDocket(int DocketCheckSummary, int Memberkey, Guid uId, string username)
        {
            WebCashing_PayDockets_Result modelResult = new WebCashing_PayDockets_Result();
            UserDA uda = new UserDA();
            int officekey = uda.GetUserOfficeKey(uId);
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.Database.CommandTimeout = 180;
                var model = con.WebCashing_PayDockets(DocketCheckSummary, Memberkey, officekey, username).FirstOrDefault();
                if (model != null)
                {
                    modelResult.LodgementReferenceNo = model.LodgementReferenceNo;
                    modelResult.TotalPaid = model.TotalPaid;
                    modelResult.TransactionKey = model.TransactionKey;
                }
            }
            return modelResult;
        }

        public WebCashing_PayMassScanning_Result PayMassDocket(int DocketCheckSummary, Guid uId, string username)
        {
            WebCashing_PayMassScanning_Result modelResult = new WebCashing_PayMassScanning_Result();
            UserDA uda = new UserDA();
            int officekey = uda.GetUserOfficeKey(uId);
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.Database.CommandTimeout = 180;
                var model = con.WebCashing_PayMassScanning(DocketCheckSummary, officekey, username).FirstOrDefault();
                if (model != null)
                {
                    modelResult.LodgementReferenceNo = model.LodgementReferenceNo;
                    modelResult.TotalPaid = model.TotalPaid;
                    modelResult.TransactionKey = model.TransactionKey;
                }
            }
            return modelResult;
        }
        public WebCashingResult MassDocketCheck(string TopBC, string BottomBC, string username, int DocketCheckSummary, double commissionRate, int officekey, WebCashingResult model1)
        {
            WebCashingResult modelPackage = new WebCashingResult();
            List<WebCashing_MassScanModel> modelResult = new List<WebCashing_MassScanModel>();
            try
            {
                if (TopBC != null && BottomBC != null)
                {
                    MemberDA mda = new MemberDA();
                    decimal PaidTotal = 0.0M;
                    decimal faceValue_N_PaidTotal = 0.0M;
                    decimal faceValueTotal = 0.0M;
                    decimal commissionTotalTotal = 0.0M;
                    decimal extraTotal = 0.0M;
                    decimal GrandTotal = 0.0M;
                    decimal BatchTotal = 0.0M;
                    int DocketCheckSummaryKey = 0;
                    string TradingName = string.Empty;
                    string BatchStatus = string.Empty;
                    string LodgementReferenceNo = string.Empty;
                    int count = 0;
                    int Member_Key = 0;

                    List<WebCashing_MassScaning_Result3> r1 = new List<WebCashing_MassScaning_Result3>();

                    string DocketMonth = BottomBC.Substring(2, 2);
                    string DocketDay = BottomBC.Substring(0, 2);
                    CommonDA com = new CommonDA();
                    int year = com.GetDocketYear(DocketMonth, DocketDay);

                    string tempbatch = TopBC.Substring(0, 8).ToString() + BottomBC.Substring(0, 6).ToString() + year.ToString();
                    using (TaxiEpayEntities con = new TaxiEpayEntities())
                    {
                        if (DocketCheckSummary == 0)
                        {
                            r1 = con.WebCashing_MassScaning(username, tempbatch, null, commissionRate, officekey, true).ToList();
                        }
                        else
                        {
                            r1 = con.WebCashing_MassScaning(username, tempbatch, DocketCheckSummary, commissionRate, officekey, true).ToList();
                        }
                        if (r1.Count() > 0)
                        {
                            foreach (var r in r1)
                            {
                                WebCashing_MassScanModel model = new WebCashing_MassScanModel();
                                if (r.Member_key != null)
                                    model.Member_key = r.Member_key.Value;
                                model.DocketBatchId = r.DocketBatchId;
                                model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                                model.BatchID = r.BatchID;
                                model.memberid = r.memberid;
                                model.TradingName = r.TradingName;
                                model.TerminalId = r.TerminalId;
                                model.BatchTotalPayable = r.BatchTotalPayable;
                                model.BatchNumber = r.BatchNumber;
                                model.PaymentStatus = r.PaymentStatus;
                                model.BatchStatus = r.BatchStatus;
                                model.IsCashable = r.IsCashable;
                                model.LodgementReferenceNo = r.LodgementReferenceNo;
                                model.BatchTotal = r.BatchTotal;
                                model.TotalBatchCommission = r.TotalBatchCommission;
                                model.TotalBatchFaceValue = r.TotalBatchFaceValue;
                                model.TotalBatchExtras = r.TotalBatchExtras;
                                model.PaidBy = r.PaidBy;
                                model.PaidDate = r.PaidDate;
                                model.GrandTotalPaidAmount = r.GrandTotalPaidAmount;
                                model.PaymentType = r.PaymentType;


                                faceValueTotal = Math.Round(r.FaceValueSummary.Value, 2);
                                commissionTotalTotal = Math.Round(r.CommissionSummary.Value, 2);
                                extraTotal = Math.Round(r.ExtrasSummary.Value, 2);
                                BatchTotal = Math.Round(r.DocketTotalSummary.Value, 2);
                                GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                                BatchStatus = r.BatchStatus;
                                TradingName = r.TradingName;

                                PaidTotal = PaidTotal + Math.Round(r.GrandTotalPaidAmount.Value, 2);
                                faceValue_N_PaidTotal = faceValue_N_PaidTotal + Math.Round(r.GrandTotalPaidAmount.Value, 2);
                                // PaidTotal = Math.Round(r.PaidAmount.Value, 2);
                                // faceValue_N_PaidTotal = Math.Round(r.PaidAmount.Value, 2) + Math.Round(r.FaceValueSummary.Value, 2);

                                if (r.Member_key != null)
                                    Member_Key = r.Member_key.Value;
                                DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                                LodgementReferenceNo = r.LodgementReferenceNo;
                                if ((r.BatchStatus).ToLower() == "valid")
                                {
                                    count = count + 1;
                                }

                                modelResult.Add(model);

                                if (r.Member_key == null)
                                {
                                    int member_key = 0;
                                    string TerminalId = TopBC.Substring(0, 8);
                                    int day = Convert.ToInt32(BottomBC.Substring(0, 2));
                                    int month = Convert.ToInt32(BottomBC.Substring(2, 2));
                                    DateTime Date = new DateTime(DateTime.Now.Year, month, day);
                                    var rr = (from x in modelResult
                                              select x).FirstOrDefault();
                                    if (rr.Member_key == null)
                                    {
                                        member_key = mda.GetMemberKeyByTerminalIdByEffecDate(rr.TerminalId, Date);
                                        Member_Key = member_key;
                                    }
                                    MemberModel mobj = new MemberModel();
                                    mobj = mda.GetMemeberLowVersionForCashing(Member_Key);
                                    TradingName = mobj.TradingName;
                                }
                            }
                            modelPackage.FaceValueTotal = faceValueTotal;
                            modelPackage.CommissionTotal = commissionTotalTotal;
                            modelPackage.ExtraTotal = extraTotal;
                            modelPackage.Count = count;
                            modelPackage.GrandTotal = GrandTotal;
                            modelPackage.BatchTotal = BatchTotal;
                            modelPackage.WebCashing_MassScanModelList = modelResult;
                            modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                            modelPackage.Member_key = Member_Key;
                            modelPackage.LodgementReferenceNo = LodgementReferenceNo;
                            modelPackage.TradingName = TradingName;
                            modelPackage.BatchStatus = BatchStatus;
                            modelPackage.GrandPaidTotal = PaidTotal;
                            modelPackage.GrandfaceValue_N_PaidTotal = faceValue_N_PaidTotal + GrandTotal;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                model1.BatchStatus = ex.Message;
                return model1;
            }
            return modelPackage;
        }
        public WebCashingResult DocketCheck(string TerminalID, string BatchNumber, string username, int DocketCheckSummary, Guid uId, string ScanType, string IsRemove, string BatchID, double commissionRate, int officekey, bool GenerateAccordin, int Year, int? AuthorisedMemberkey = null, bool? IsAuthorize = false, bool? IsNoReciept = false, int? Docketkey = null)
        {
            WebCashingResult modelPackage = new WebCashingResult();
            List<WebCashing_GetEFTDocketsByBatchID> modelResult = new List<WebCashing_GetEFTDocketsByBatchID>();

            if (!string.IsNullOrEmpty(TerminalID) && !string.IsNullOrEmpty(BatchNumber))
            {
                MemberDA mda = new MemberDA();
                decimal faceValueTotal = 0.0M;
                decimal commissionTotalTotal = 0.0M;
                decimal extraTotal = 0.0M;
                decimal GrandTotal = 0.0M;
                decimal BatchTotal = 0.0M;
                decimal HandlingTotal = 0.0M;
                decimal TipProTotal = 0.0M;
                decimal TipProFeeTotal = 0.0M;
                int DocketCheckSummaryKey = 0;
                string LodgementReferenceNo = string.Empty;
                int count = 0;
                int Member_Key = 0;
                decimal LevyChargeSummary = 0.0M;
                decimal totalGlideboxCommission = 0.00M;

                List<WebCashing_GetEFTDocketsByBatchID1_Result1> ResultList = new List<WebCashing_GetEFTDocketsByBatchID1_Result1>();

                if (IsRemove == "True")
                {
                    this.LogInfo(string.Format("Remove Batch"));
                    using (TaxiEpayEntities con = new TaxiEpayEntities())
                    {
                        List<WebCashing_RemoveEFTDocketsByBatchID1_Result> RemovedList = new List<WebCashing_RemoveEFTDocketsByBatchID1_Result>();
                        con.Database.CommandTimeout = 180;
                        if (Docketkey != null)
                        {
                            this.LogInfo(string.Format("Remove Docketkey {0} from docketchecksummary {1}", Docketkey.Value, DocketCheckSummary));
                            var RemovedList1 = con.WebCashing_RemoveDocket(Docketkey.Value, DocketCheckSummary).ToList();
                            RemovedList = ConvertWebCashing_RemoveDocket(RemovedList1);
                        }
                        else
                        {
                            RemovedList = con.WebCashing_RemoveEFTDocketsByBatchID(BatchID, DocketCheckSummary).ToList();
                        }
                        //var RemovedList = con.WebCashing_RemoveEFTDocketsByBatchID(BatchID, DocketCheckSummary).ToList();
                        DocketCheckSummaryKey = RemovedList.FirstOrDefault().DocketCheckSummaryKey;
                        foreach (var r in RemovedList)
                        {
                            WebCashing_GetEFTDocketsByBatchID1_Result1 model = new WebCashing_GetEFTDocketsByBatchID1_Result1();
                            model.BatchNumber = r.BatchNumber;
                            model.BatchID = r.BatchID;
                            model.BatchStatus = r.BatchStatus;
                            model.BatchTotalPayable = r.BatchTotalPayable;
                            model.CardNumberMasked = r.CardNumberMasked;
                            model.CreatedByUser = r.CreatedByUser;
                            model.docket_key = r.docket_key;
                            model.DocketBatchId = r.DocketBatchId;
                            model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            model.docketdate = r.docketdate;
                            model.IsCashable = r.IsCashable;
                            model.Member_key = r.Member_key;
                            model.memberid = r.memberid;
                            model.PaidCommission = r.PaidCommission;
                            model.PaidTransactionValue = r.PaidTransactionValue;
                            model.PaymentStatus = r.PaymentStatus;
                            model.state = r.state;
                            model.TerminalId = r.TerminalId;
                            model.TotalPaid = r.TotalPaid;
                            model.TotalPayable = r.TotalPayable;
                            model.TradingName = r.TradingName;
                            if (r.GrandTotal != null)
                                model.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            model.PendingCommision = r.PendingCommision;
                            model.PendingTransactionValue = r.PendingTransactionValue;
                            if (r.FaceValue != null)
                                model.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            if (r.TotalCommission != null)
                                model.TotalCommission = Math.Round(r.TotalCommission.Value, 2);
                            if (r.Extras != null)
                                model.Extras = Math.Round(r.Extras.Value, 2);
                            if (r.DocketTotal != null)
                                model.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            model.LodgementReferenceNo = r.LodgementReferenceNo;
                            if (r.BatchTotal != null)
                                model.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            if (r.DocketTotalSummary != null)
                                model.DocketTotalSummary = Math.Round(r.DocketTotalSummary.Value, 2);
                            if (r.FaceValueSummary != null)
                                model.FaceValueSummary = Math.Round(r.FaceValueSummary.Value, 2);
                            if (r.ExtrasSummary != null)
                                model.ExtrasSummary = Math.Round(r.ExtrasSummary.Value, 2);
                            if (r.CommissionSummary != null)
                                model.CommissionSummary = Math.Round(r.CommissionSummary.Value, 2);
                            if (r.TotalBatchFaceValue != null)
                                model.TotalBatchCommission = Math.Round(r.TotalBatchFaceValue.Value, 2);
                            if (r.TotalBatchFaceValue != null)
                                model.TotalBatchFaceValue = Math.Round(r.TotalBatchFaceValue.Value, 2);
                            if (r.TotalBatchExtras != null)
                                model.TotalBatchExtras = Math.Round(r.TotalBatchExtras.Value, 2);
                            model.CardType = r.CardType;
                            if (r.handlingFee != null)
                                model.handlingFee = Math.Round(r.handlingFee, 2);
                            if (r.TotalBatchHandlingFee != null)
                                model.TotalBatchHandlingFee = Math.Round(r.TotalBatchHandlingFee.Value, 2);
                            if (r.HandlingFeeSummary != null)
                                model.HandlingFeeSummary = Math.Round(r.HandlingFeeSummary.Value, 2);
                            model.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                            model.TippingFee = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                            model.TipProSummary = Math.Round(r.TipProSummary.GetValueOrDefault(0), 2);
                            model.TippingFeeSummary = Math.Round(r.TippingFeeSummary.GetValueOrDefault(0), 2);
                            model.TotalBatchChargebacks = r.TotalBatchChargebacks;
                            model.Chargebacks = r.Chargebacks;
                            model.ChargebacksSummary = r.ChargebacksSummary;
                            model.LevyCharge = r.LevyCharge;
                            model.TotalLevyCharge = r.TotalLevyCharge;
                            model.LevyChargeSummary = r.LevyChargeSummary;

                            model.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);
                            model.TotalGlideboxCommission = Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);
                            //model.TotalBatchChargebacks = r.
                            //model.TotalBatchChargebacks = Math.Round(r.)
                            this.LogInfo(string.Format("Add to Result List DocketKey {0} ", r.docket_key));
                            ResultList.Add(model);
                        }
                    }
                    this.LogInfo(string.Format("Finish Removing Batch"));
                }

                if (IsRemove != "True")
                {

                    string tempbatch = GetBatchId(TerminalID, BatchNumber, Year);//TerminalID.Substring(0, 8).ToString() + BatchNumber.Substring(0, 6).ToString() + year.ToString();
                    this.LogInfo(string.Format("Add Batch: {0}", tempbatch));
                    using (TaxiEpayEntities con = new TaxiEpayEntities())
                    {
                        con.Database.CommandTimeout = 180;
                        bool IsMassScan = false;
                        if (ScanType == "MassScan")
                        {
                            IsMassScan = true;
                        }

                        if (IsNoReciept.Value == true) //for No Receipt
                        {
                            if (DocketCheckSummary == 0)  //first scan for no receipt
                            {
                                var ResultListval = con.WebCashing_AddDocketsToCashing(username, Docketkey, null, commissionRate, officekey, IsMassScan, AuthorisedMemberkey).ToList();
                                ResultList = ConvertNoReceptDocket(ResultListval);
                            }
                            else //when there is docket check summary key for no receipt
                            {
                                var ResultListval = con.WebCashing_AddDocketsToCashing(username, Docketkey, DocketCheckSummary, commissionRate, officekey, IsMassScan, AuthorisedMemberkey).ToList();
                                ResultList = ConvertNoReceptDocket(ResultListval);
                            }

                        }
                        else //normal scanning
                        {
                            if (DocketCheckSummary == 0) //first scan
                            {
                                this.LogInfo(string.Format("first scan"));
                                ResultList = con.WebCashing_GetEFTDocketsByBatchID1(username, tempbatch, null, commissionRate, officekey, IsMassScan, TerminalID, BatchNumber, AuthorisedMemberkey).ToList();
                            }
                            else if (IsAuthorize == true) //authorised member
                            {
                                //switching the member to authorised member
                                var ResultListval = con.WebCashing_ChangeAuthorisedMember(DocketCheckSummary, AuthorisedMemberkey).ToList();
                                ResultList = ConvertAuthorised(ResultListval);
                            }
                            else //normal member
                            {
                                this.LogInfo(string.Format("not first scan"));
                                ResultList = con.WebCashing_GetEFTDocketsByBatchID1(username, tempbatch, DocketCheckSummary, commissionRate, officekey, IsMassScan, TerminalID, BatchNumber, AuthorisedMemberkey).ToList();
                            }
                        }
                        if (ResultList.Count > 0)
                        {
                            this.LogInfo(string.Format("get summary key"));
                            DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                            //modelPackage.DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                            modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                        }
                    }
                    this.LogInfo(string.Format("Done Adding Batch: {0}", tempbatch));
                }
                if (GenerateAccordin == true)  //Show transaction detail for each EOS
                {
                    this.LogInfo(string.Format("Generate Accordion"));
                    if (ResultList.Count() > 0)
                    {
                        var dock = ResultList.FirstOrDefault();

                        if (dock != null && dock.Member_key.GetValueOrDefault() > 0)
                        {
                            int docketchecksummarykey = dock.DocketCheckSummaryKey;
                            Member_Key = dock.Member_key.Value;
                            this.LogInfo(string.Format("get member model"));
                            MemberModel membermodel = new MemberModel();
                            membermodel = mda.GetMemeber(Member_Key);
                            this.LogInfo(string.Format("get operator model"));
                            OperatorDA oda1 = new OperatorDA();
                            OperatorModel opt = new OperatorModel();
                            opt = oda1.Get(membermodel.Member_key);
                            foreach (var r in ResultList)
                            {
                                this.LogInfo(string.Format("Generate Accordion Item number: {0}", (ResultList.IndexOf(r) + 1).ToString()));
                                if (r.BatchID == null && r.BatchStatus == null)
                                {
                                    //MemberModel membermodel = new MemberModel();
                                    //// string memberid = ResultList.FirstOrDefault().Member_key;
                                    //Member_Key = r.Member_key.Value;
                                    //membermodel = mda.GetMemeber(Member_Key);

                                    if (Member_Key != r.Member_key.Value)
                                    {
                                        Member_Key = r.Member_key.Value;
                                        membermodel = mda.GetMemeber(Member_Key);
                                        opt = oda1.Get(membermodel.Member_key);
                                    }

                                    WebCashingResult model = new WebCashingResult();
                                    model.ABN = membermodel.ABN;
                                    model.BatchNumber = "";
                                    model.BatchStatus = "";
                                    model.BatchTotal = 0;
                                    model.CommissionTotal = 0;
                                    model.Count = 0;
                                    model.DocketCheckSummaryKey = 0;
                                    model.DocketTotal = 0;
                                    model.ExtraTotal = 0;
                                    model.FaceValueTotal = 0;
                                    model.GrandTotal = 0;
                                    model.TippingFeeTotal = 0;
                                    model.TipProTotal = 0;

                                    model.GlideboxCommission = 0;
                                    model.TotalGlideboxCommission = 0;

                                    model.LodgementReferenceNo = "";
                                    model.Member_key = membermodel.Member_key;
                                    model.memberid = membermodel.MemberId;
                                    model.Mobile = membermodel.Mobile;
                                    model.Name = membermodel.FirstName + " " + membermodel.LastName;
                                    model.Phone = membermodel.Telephone;
                                    model.TradingName = membermodel.TradingName;
                                    model.PhotoID = membermodel.PhotoID;
                                    model.WebCashing_MassScanModelList = null;
                                    model.ResultList = null;

                                    //OperatorDA oda1 = new OperatorDA();
                                    //OperatorModel opt = new OperatorModel();
                                    //opt = oda1.Get(membermodel.Member_key);
                                    if (opt != null && opt.AllowCash.GetValueOrDefault(false))
                                    {
                                        model.BatchStatus = "Cash Customer";
                                    }
                                    else
                                    {
                                        model.BatchStatus = "Bank Customer";
                                    }

                                    if (opt.PaymentType.GetValueOrDefault() == 3)
                                    {
                                        model.BatchStatus = "Card Customer";
                                    }

                                    //MemberNotesDA mdna = new MemberNotesDA();
                                    //IEnumerable<MemberNotesModel> mnm;
                                    //mnm = mdna.GetMemberNotes(membermodel.Member_key).Take(15).ToList();
                                    //model.MemberNotes = mnm;


                                    //model.DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey; //comment this out to prevent we have to go through the result list every time
                                    model.DocketCheckSummaryKey = docketchecksummarykey;


                                    //model.ChargebacksTotal = r.c
                                    modelPackage = model;
                                    continue;
                                }

                                WebCashing_GetEFTDocketsByBatchID model1 = new WebCashing_GetEFTDocketsByBatchID();
                                model1 = (from n in modelResult
                                          where n.BatchID == r.BatchID
                                          select n).FirstOrDefault();

                                if (model1 == null)
                                {
                                    //Summary header
                                    WebCashing_GetEFTDocketsByBatchID model = new WebCashing_GetEFTDocketsByBatchID();
                                    model.BatchStatus = r.BatchStatus;
                                    model.docketdate = r.docketdate;
                                    model.TerminalId = r.TerminalId;
                                    model.BatchNumber = r.BatchNumber;

                                    model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                                    model.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                                    model.LodgementReferenceNo = r.LodgementReferenceNo;
                                    model.PendingCommision = r.PendingCommision;
                                    model.PendingTransactionValue = r.PendingTransactionValue;
                                    model.BatchID = r.BatchID;


                                    model.FaceValue = Math.Round(r.TotalBatchFaceValue.Value, 2);
                                    model.TOtalcommission = Math.Round(r.TotalBatchCommission.GetValueOrDefault(0), 2);
                                    model.Extras = Math.Round(r.TotalBatchExtras.GetValueOrDefault(0), 2);
                                    model.BatchTotal = Math.Round(r.BatchTotal.GetValueOrDefault(0), 2);
                                    model.TotalPayable = Math.Round(r.TotalPayable, 2);
                                    model.TotalBatchHandlingFee = Math.Round(r.TotalBatchHandlingFee.GetValueOrDefault(0), 2);
                                    model.TippingFee = Math.Round(r.TippingFee, 2);
                                    model.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                                    model.TotalLevyCharge = Math.Round(r.TotalLevyCharge.GetValueOrDefault(0), 2);

                                    faceValueTotal = Math.Round(r.FaceValueSummary.GetValueOrDefault(0), 2);
                                    commissionTotalTotal = Math.Round(r.CommissionSummary.GetValueOrDefault(0), 2);
                                    extraTotal = Math.Round(r.ExtrasSummary.GetValueOrDefault(0), 2);
                                    BatchTotal = Math.Round(r.DocketTotalSummary.GetValueOrDefault(0), 2);
                                    GrandTotal = Math.Round(r.GrandTotal.GetValueOrDefault(0), 2);
                                    HandlingTotal = Math.Round(r.HandlingFeeSummary.GetValueOrDefault(0), 2);
                                    TipProTotal = Math.Round(r.TipProSummary.GetValueOrDefault(0), 2);
                                    TipProFeeTotal = Math.Round(r.TippingFeeSummary.GetValueOrDefault(0), 2);
                                    LevyChargeSummary = Math.Round(r.LevyChargeSummary.GetValueOrDefault(0), 2);

                                    model.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);
                                    model.TotalGlideboxCommission = Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);

                                    if (r.Member_key != null && Member_Key != r.Member_key)
                                    {
                                        Member_Key = r.Member_key.GetValueOrDefault(0);
                                        membermodel = mda.GetMemeber(Member_Key);
                                        opt = oda1.Get(membermodel.Member_key);
                                    }
                                    DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                                    LodgementReferenceNo = r.LodgementReferenceNo;
                                    if ((r.BatchStatus).ToLower() == "valid")
                                    {
                                        count = count + 1;
                                        totalGlideboxCommission += Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);
                                    }
                                    if (ScanType != "MassScan")
                                    {
                                        //Transaction details
                                        WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                                        ChildModel.BatchStatus = r.BatchStatus;
                                        ChildModel.BatchNumber = r.BatchNumber;
                                        ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                                        ChildModel.docket_key = r.docket_key;
                                        ChildModel.docketdate = r.docketdate;
                                        ChildModel.PaymentStatus = r.PaymentStatus;
                                        ChildModel.TerminalId = r.TerminalId;
                                        ChildModel.TotalPayable = r.TotalPayable;
                                        ChildModel.GrandTotal = Math.Round(r.GrandTotal.GetValueOrDefault(0), 2);
                                        ChildModel.PendingCommision = r.PendingCommision;
                                        ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                                        ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                                        ChildModel.TOtalcommission = Math.Round(r.TotalCommission.GetValueOrDefault(0), 2);
                                        ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                                        ChildModel.BatchTotal = Math.Round(r.BatchTotal.GetValueOrDefault(0), 2);
                                        ChildModel.DocketTotal = Math.Round(r.DocketTotal.GetValueOrDefault(0), 2);
                                        ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                                        ChildModel.TippingFee = Math.Round(r.TippingFee, 2);
                                        ChildModel.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                                        ChildModel.LevyCharge = Math.Round(r.LevyCharge.GetValueOrDefault(0), 2);
                                        ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);

                                        //  model.TotalPayable = model1.TotalPayable + r.TotalPayable;

                                        model.ResultListChild.Add(ChildModel);

                                        var glideboxCommissionResult = GetGlideboxCommissionSameDayCashing(r.docket_key, r.BatchID);

                                        if (glideboxCommissionResult != null)
                                        {
                                            model.ResultGlideboxCommissionListChild.Add(glideboxCommissionResult);
                                        }
                                    }
                                    else
                                    {
                                        //nothing for mass scanning
                                    }
                                    modelResult.Add(model);

                                }
                                else
                                {
                                    if (ScanType != "MassScan") //normal scanning
                                    {

                                        modelResult.Remove(model1);
                                        WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                                        ChildModel.BatchStatus = r.BatchStatus;
                                        ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                                        ChildModel.BatchNumber = r.BatchNumber;
                                        ChildModel.docketdate = r.docketdate;
                                        ChildModel.docket_key = r.docket_key;
                                        ChildModel.PaymentStatus = r.PaymentStatus;
                                        ChildModel.TerminalId = r.TerminalId;
                                        ChildModel.TotalPayable = r.TotalPayable;
                                        ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                                        ChildModel.PendingCommision = r.PendingCommision;
                                        ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                                        ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                                        ChildModel.TOtalcommission = Math.Round(r.TotalCommission.GetValueOrDefault(0), 2);
                                        ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                                        ChildModel.BatchTotal = Math.Round(r.BatchTotal.GetValueOrDefault(0), 2);
                                        ChildModel.DocketTotal = Math.Round(r.DocketTotal.GetValueOrDefault(0), 2);
                                        ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                                        ChildModel.TippingFee = Math.Round(r.TippingFee, 2);
                                        ChildModel.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                                        ChildModel.LevyCharge = Math.Round(r.LevyCharge.GetValueOrDefault(0), 2);
                                        ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);

                                        model1.TotalPayable = model1.TotalPayable + r.TotalPayable;
                                        model1.GlideboxCommission = model1.GlideboxCommission + r.GlideboxCommission.GetValueOrDefault(0);

                                        model1.ResultListChild.Add(ChildModel);

                                        var glideboxCommissionResult = GetGlideboxCommissionSameDayCashing(r.docket_key, r.BatchID);

                                        if (glideboxCommissionResult != null)
                                        {
                                            model1.ResultGlideboxCommissionListChild.Add(glideboxCommissionResult);
                                        }
                                        modelResult.Add(model1);
                                    }
                                }
                                if (ScanType == "MassScan")
                                {
                                    int member_key = 0;
                                    string TerminalId = TerminalID.Substring(0, 8);
                                    int day = Convert.ToInt32(BatchNumber.Substring(0, 2));
                                    int month = Convert.ToInt32(BatchNumber.Substring(2, 2));
                                    DateTime Date = new DateTime(DateTime.Now.Year, month, day);
                                    var rr = (from x in modelResult
                                              select x).FirstOrDefault();
                                    if (rr.Member_key == null)
                                    {
                                        member_key = mda.GetMemberKeyByTerminalIdByEffecDate(rr.TerminalId, Date);
                                        Member_Key = member_key;
                                    }
                                }

                                if ((r.BatchStatus).ToLower() == "invalid batch")
                                {
                                    if (TerminalID == "Remove")
                                    {
                                        Member_Key = AuthorisedMemberkey.GetValueOrDefault(0);
                                    }
                                    else
                                    {
                                        int member_key = 0;
                                        string TerminalId = TerminalID.Substring(0, 8);
                                        int day = Convert.ToInt32(BatchNumber.Substring(0, 2));
                                        int month = Convert.ToInt32(BatchNumber.Substring(2, 2));

                                        DateTime Date = new DateTime(DateTime.Now.Year, month, day);
                                        if (r.Member_key == null)
                                        {
                                            member_key = mda.GetMemberKeyByTerminalIdByEffecDate(TerminalId, Date);
                                            Member_Key = member_key;
                                        }
                                    }

                                }
                            }
                        }
                        modelPackage.FaceValueTotal = faceValueTotal;
                        modelPackage.CommissionTotal = commissionTotalTotal;
                        modelPackage.ExtraTotal = extraTotal;
                        modelPackage.Count = count;
                        modelPackage.GrandTotal = GrandTotal;
                        modelPackage.BatchTotal = BatchTotal;
                        modelPackage.ResultList = modelResult;
                        modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                        modelPackage.Member_key = Member_Key;
                        modelPackage.LodgementReferenceNo = LodgementReferenceNo;
                        modelPackage.HandlingFeeSummary = HandlingTotal;
                        modelPackage.TipProTotal = TipProTotal;
                        modelPackage.TippingFeeTotal = TipProFeeTotal;
                        modelPackage.LevyChargeSummary = LevyChargeSummary;
                        modelPackage.TotalGlideboxCommission = totalGlideboxCommission;

                        MemberModel mobj = new MemberModel();
                        mobj = mda.GetMemeberLowVersionForCashing(Member_Key);
                        modelPackage.memberid = mobj.MemberId;
                        modelPackage.TradingName = mobj.TradingName;

                        modelPackage.Name = mobj.FirstName + " " + mobj.LastName;
                        modelPackage.Mobile = mobj.Mobile;
                        modelPackage.Phone = mobj.Telephone;
                        modelPackage.PhotoID = mobj.PhotoID;
                        //modelPackage.LastCashedAmt = mobj.LastCashedAmt;
                        //modelPackage.LastCashedDateTime = mobj.LastCashedDateTime;
                        //modelPackage.DriverCertificateId = mobj.DriverCertificateId;
                        //modelPackage.Debit = mobj.CRDebitRate;
                        //modelPackage.Visa = mobj.CRVisaRate.Value;
                        //modelPackage.Master = mobj.CRMasterCardRate.Value;
                        //modelPackage.Diners = mobj.CRDinersRate.Value;
                        //modelPackage.Amex = mobj.CRAmexRate.Value;
                        //modelPackage.DebitPercentage = mobj.CRDebitRatePercentage.Value;
                        modelPackage.Companykey = mobj.CompanyKey;


                        if (mobj.ABN != null)
                            modelPackage.ABN = mobj.ABN;
                        else
                            modelPackage.ABN = mobj.ACN;

                        OperatorDA oda11 = new OperatorDA();
                        OperatorModel opt1 = new OperatorModel();
                        opt1 = oda11.Get(Member_Key);
                        if (opt1.AllowCash == false)
                        {
                            modelPackage.BatchStatus = "Bank Customer";
                        }
                        else
                        {
                            modelPackage.BatchStatus = "Cash Customer";
                        }

                        if (opt1.PaymentType.GetValueOrDefault() == 3)
                        {
                            modelPackage.BatchStatus = "Card Customer";
                        }
                    }
                }
            }
            return modelPackage;
        }

        public string GetBatchId(string TopBarcode, string BottomBarcode, int Year)
        {
            string tempBatchID = string.Empty;
            int monthToday = DateTime.Now.Month;
            string DocketMonth = BottomBarcode.Substring(2, 2);
            string DocketDay = BottomBarcode.Substring(0, 2);
            CommonDA com = new CommonDA();
            int year = com.GetDocketYear(DocketMonth, DocketDay);
            if (Year != 0)
            {
                year = Year;
            }

            return tempBatchID = TopBarcode.Substring(0, 8).ToString() + BottomBarcode.Substring(0, 6).ToString() + year.ToString();
        }

        public WebCashingResult ManualDocketCheck(string username, int DocketCheckSummaryKey, int MemberKey, decimal Amount, int DocketPaymethodKey, double commissionRate, int officekey, bool GenerateAccordin, decimal ServicefeeRate)
        {
            List<WebCashing_AddManualDocketsToCashing_Result1> ResultList = new List<WebCashing_AddManualDocketsToCashing_Result1>();
            WebCashingResult modelPackage = new WebCashingResult();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.Database.CommandTimeout = 180;
                if (DocketCheckSummaryKey == 0)
                {
                    ResultList = con.WebCashing_AddManualDocketsToCashing(username, MemberKey, DocketPaymethodKey, Amount, null, commissionRate, officekey, ServicefeeRate).ToList();
                }
                else
                {
                    ResultList = con.WebCashing_AddManualDocketsToCashing(username, MemberKey, DocketPaymethodKey, Amount, DocketCheckSummaryKey, commissionRate, officekey, ServicefeeRate).ToList();
                }
                if (ResultList != null)
                {
                    modelPackage.DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                }
            }
            if (GenerateAccordin)
            {

                List<WebCashing_GetEFTDocketsByBatchID> modelResult = new List<WebCashing_GetEFTDocketsByBatchID>();
                MemberDA mda = new MemberDA();
                decimal faceValueTotal = 0.0M;
                decimal commissionTotalTotal = 0.0M;
                decimal extraTotal = 0.0M;
                decimal GrandTotal = 0.0M;
                decimal BatchTotal = 0.0M;
                decimal HandlingTotal = 0.0M;

                string LodgementReferenceNo = string.Empty;
                int count = 0;
                int Member_Key = 0;

                if (ResultList.Count() > 0)
                {
                    foreach (var r in ResultList)
                    {
                        WebCashing_GetEFTDocketsByBatchID model1 = new WebCashing_GetEFTDocketsByBatchID();
                        model1 = (from n in modelResult
                                  where n.BatchID == r.BatchID
                                  select n).FirstOrDefault();

                        if (model1 == null)
                        {
                            WebCashing_GetEFTDocketsByBatchID model = new WebCashing_GetEFTDocketsByBatchID();
                            model.BatchStatus = r.BatchStatus;
                            model.docketdate = r.docketdate;
                            model.TerminalId = r.TerminalId;

                            model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            model.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            model.LodgementReferenceNo = r.LodgementReferenceNo;
                            model.PendingCommision = r.PendingCommision;
                            model.PendingTransactionValue = r.PendingTransactionValue;
                            model.BatchID = r.BatchID;
                            model.BatchNumber = r.BatchNumber;

                            model.FaceValue = Math.Round(r.TotalBatchFaceValue.Value, 2);
                            model.TOtalcommission = Math.Round(r.TotalBatchCommission.Value, 2);
                            model.Extras = Math.Round(r.TotalBatchExtras.Value, 2);
                            model.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            model.TotalPayable = Math.Round(r.TotalPayable, 2);
                            model.CardType = r.CardType;
                            model.TotalBatchHandlingFee = Math.Round(r.TotalBatchHandlingFee.Value, 2);

                            faceValueTotal = Math.Round(r.FaceValueSummary.Value, 2);
                            commissionTotalTotal = Math.Round(r.CommissionSummary.Value, 2);
                            extraTotal = Math.Round(r.ExtrasSummary.Value, 2);
                            BatchTotal = Math.Round(r.DocketTotalSummary.Value, 2);
                            GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            HandlingTotal = Math.Round(r.HandlingFeeSummary.Value, 2);

                            if (r.Member_key != null)
                                Member_Key = r.Member_key.Value;
                            DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            LodgementReferenceNo = r.LodgementReferenceNo;
                            if ((r.BatchStatus).ToLower() == "valid")
                            {
                                count = count + 1;
                            }

                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;

                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.PaymentStatus = r.PaymentStatus;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                            model.ResultListChild.Add(ChildModel);

                            modelResult.Add(model);
                        }
                        else
                        {
                            modelResult.Remove(model1);
                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.PaymentStatus = r.PaymentStatus;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);

                            model1.TotalPayable = model1.TotalPayable + r.TotalPayable;

                            model1.ResultListChild.Add(ChildModel);
                            modelResult.Add(model1);
                        }
                    }
                    modelPackage.FaceValueTotal = faceValueTotal;
                    modelPackage.CommissionTotal = commissionTotalTotal;
                    modelPackage.ExtraTotal = extraTotal;
                    modelPackage.Count = count;
                    modelPackage.GrandTotal = GrandTotal;
                    modelPackage.BatchTotal = BatchTotal;
                    modelPackage.ResultList = modelResult;
                    modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                    modelPackage.Member_key = Member_Key;
                    modelPackage.LodgementReferenceNo = LodgementReferenceNo;
                    modelPackage.HandlingFeeSummary = HandlingTotal;
                    MemberModel mobj = new MemberModel();
                    mobj = mda.GetMemeberLowVersionForCashing(Member_Key);
                    modelPackage.memberid = mobj.MemberId;
                    modelPackage.TradingName = mobj.TradingName;

                    modelPackage.Name = mobj.FirstName + " " + mobj.LastName;
                    modelPackage.Mobile = mobj.Mobile;
                    modelPackage.Phone = mobj.Telephone;
                    modelPackage.PhotoID = mobj.PhotoID;
                    modelPackage.LastCashedAmt = mobj.LastCashedAmt;
                    modelPackage.LastCashedDateTime = mobj.LastCashedDateTime;
                    modelPackage.DriverCertificateId = mobj.DriverCertificateId;
                    modelPackage.Companykey = mobj.CompanyKey;
                    if (mobj.ABN != null)
                        modelPackage.ABN = mobj.ABN;
                    else
                        modelPackage.ABN = mobj.ACN;

                    OperatorDA oda1 = new OperatorDA();
                    OperatorModel opt = new OperatorModel();
                    opt = oda1.Get(Member_Key);
                    if (opt.AllowCash == false)
                    {
                        modelPackage.BatchStatus = "Bank Customer";
                    }
                    else
                    {
                        modelPackage.BatchStatus = "Cash Customer";
                    }

                    //RewardPointDA rda = new RewardPointDA();
                    //RewardPointsModel rewardObj = new RewardPointsModel();
                    //rewardObj = rda.Get(Member_Key);
                    //modelPackage.TotalPoints = rewardObj.TotalTrans;
                    //modelPackage.TotalPointsUsed = rewardObj.TotalPointsUsed;
                    //modelPackage.TotalPointsAvailable = rewardObj.TotalPoints;

                    //MemberNotesDA mdna = new MemberNotesDA();
                    //IEnumerable<MemberNotesModel> mnm;
                    //mnm = mdna.GetMemberNotes(Member_Key).Take(15).ToList();
                    //modelPackage.MemberNotes = mnm;

                }
            }

            return modelPackage;
        }

        public WebCashingResult SameDayDocketCheck(string TopBC, string BottomBC, string username, int DocketCheckSummaryKey, int MemberKey, int Quantity, decimal Amount, int DocketPaymethodKey, double commissionRate, int officekey, bool GenerateAccordin, DataTable glideboxCommissionTable)
        {
            WebCashingResult modelPackage = new WebCashingResult();
            List<WebCashing_GetEFTDocketsByBatchID> modelResult = new List<WebCashing_GetEFTDocketsByBatchID>();
            MemberDA mda = new MemberDA();
            decimal faceValueTotal = 0.0M;
            decimal commissionTotalTotal = 0.0M;
            decimal extraTotal = 0.0M;
            decimal GrandTotal = 0.0M;
            decimal BatchTotal = 0.0M;
            decimal HandlingTotal = 0.0M;
            decimal totalGlideboxCommission = 0.00M;

            string LodgementReferenceNo = string.Empty;
            int count = 0;
            int Member_Key = 0;
            int? docketCheckSummaryKey = null;
            List<WebCashing_AddSameDayDockets_Result> ResultList = new List<WebCashing_AddSameDayDockets_Result>();

            int monthToday = DateTime.Now.Month;
            string DocketMonth = BottomBC.Substring(2, 2);
            string DocketDay = BottomBC.Substring(0, 2);

            CommonDA com = new CommonDA();
            int year = com.GetDocketYear(DocketMonth, DocketDay);

            string tempbatch = GetBatchId(TopBC, BottomBC, year); //TopBC.Substring(0, 8).ToString() + BottomBC.Substring(0, 6).ToString() + year.ToString();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.Database.CommandTimeout = 180;
                if (DocketCheckSummaryKey == 0)
                {
                    docketCheckSummaryKey = null;
                }
                else
                {
                    docketCheckSummaryKey = DocketCheckSummaryKey;
                }

                List<SqlParameter> sqlParameters = new List<SqlParameter>
                {
                    new SqlParameter("@user", username),
                    new SqlParameter("@BatchID", tempbatch),
                    new SqlParameter("@BatchTop", TopBC),
                    new SqlParameter("@BatchBottom", BottomBC),
                    new SqlParameter("@memberKey", MemberKey),
                    new SqlParameter("@DocketpaymethodKey", DocketPaymethodKey),
                    new SqlParameter("@amount", Amount),
                    new SqlParameter("@quantity", Quantity),
                    new SqlParameter("@DocketCheckSummary", docketCheckSummaryKey),
                    new SqlParameter("@CashingCenterRate", commissionRate),
                    new SqlParameter("@OfficeKey", officekey),
                    new SqlParameter("@ProcessGlideboxCommission", GenerateAccordin),
                    new SqlParameter("@TvpGlideboxCommission", SqlDbType.Structured)
                    {
                        Value = glideboxCommissionTable,
                        TypeName = "dbo.TVPGlideboxCommissionForSameDayCashing"
                    }
                };

                ResultList = con.Database.SqlQuery<WebCashing_AddSameDayDockets_Result>("EXEC WebCashing_AddSameDayDockets @user, @BatchID, @BatchTop, @BatchBottom, @memberKey, @DocketpaymethodKey, @amount, @quantity, @DocketCheckSummary, @CashingCenterRate, @OfficeKey, @TvpGlideboxCommission, @ProcessGlideboxCommission", sqlParameters.ToArray()).ToList();

                if (ResultList != null && ResultList.Count > 0)
                {
                    DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                    //modelPackage.DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                    modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                }
            }
            if (GenerateAccordin)
            {
                if (ResultList.Count() > 0)
                {
                    foreach (var r in ResultList)
                    {
                        WebCashing_GetEFTDocketsByBatchID model1 = new WebCashing_GetEFTDocketsByBatchID();
                        model1 = (from n in modelResult
                                  where n.BatchID == r.BatchID
                                  select n).FirstOrDefault();

                        if (model1 == null)
                        {
                            WebCashing_GetEFTDocketsByBatchID model = new WebCashing_GetEFTDocketsByBatchID();
                            model.BatchStatus = r.BatchStatus;
                            model.docketdate = r.docketdate;
                            model.TerminalId = r.TerminalId;

                            model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            model.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            model.LodgementReferenceNo = r.LodgementReferenceNo;
                            model.PendingCommision = r.PendingCommision;
                            model.PendingTransactionValue = r.PendingTransactionValue;
                            model.BatchID = r.BatchID;
                            model.BatchNumber = r.BatchNumber;

                            model.FaceValue = Math.Round(r.TotalBatchFaceValue.Value, 2);
                            model.TOtalcommission = Math.Round(r.TotalBatchCommission.Value, 2);
                            model.Extras = Math.Round(r.TotalBatchExtras.Value, 2);
                            model.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            model.TotalPayable = Math.Round(r.TotalPayable.Value, 2);
                            model.CardType = r.CardType;
                            model.TotalBatchHandlingFee = Math.Round(r.TotalBatchHandlingFee.Value, 2);
                            model.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                            model.TippingFee = Math.Round(r.TippingFee, 2);

                            model.LevyCharge = r.LevyCharge;
                            model.TotalLevyCharge = r.TotalLevyCharge;
                            model.LevyChargeSummary = r.LevyChargeSummary;

                            faceValueTotal = Math.Round(r.FaceValueSummary.Value, 2);
                            commissionTotalTotal = Math.Round(r.CommissionSummary.Value, 2);
                            extraTotal = Math.Round(r.ExtrasSummary.Value, 2);
                            BatchTotal = Math.Round(r.DocketTotalSummary.Value, 2);
                            GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            HandlingTotal = Math.Round(r.HandlingFeeSummary.Value, 2);

                            model.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);
                            model.TotalGlideboxCommission = Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);

                            if (r.Member_key != null)
                                Member_Key = r.Member_key.Value;
                            DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            LodgementReferenceNo = r.LodgementReferenceNo;
                            if ((r.BatchStatus).ToLower() == "valid")
                            {
                                count = count + 1;
                                totalGlideboxCommission += Math.Round(r.TotalGlideboxCommission.GetValueOrDefault(0), 2);
                            }

                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.PaymentStatus = string.IsNullOrEmpty(r.PaymentStatus) ? "Pending" : r.PaymentStatus;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                            ChildModel.TippingFee = Math.Round(r.TippingFee, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                            ChildModel.LevyCharge = r.LevyCharge;
                            ChildModel.TotalLevyCharge = r.TotalLevyCharge;
                            ChildModel.LevyChargeSummary = r.LevyChargeSummary;
                            ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);
                            model.ResultListChild.Add(ChildModel);

                            var glideboxCommissionResult = GetGlideboxCommissionSameDayCashing(r.docket_key, r.BatchID);

                            if (glideboxCommissionResult != null)
                            {
                                model.ResultGlideboxCommissionListChild.Add(glideboxCommissionResult);
                            }

                            modelResult.Add(model);
                        }
                        else
                        {
                            modelResult.Remove(model1);
                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                            ChildModel.BatchNumber = r.BatchNumber;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.PaymentStatus = string.IsNullOrEmpty(r.PaymentStatus) ? "Pending" : r.PaymentStatus;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.TipPro = Math.Round(r.TipPro.GetValueOrDefault(0), 2);
                            ChildModel.TippingFee = Math.Round(r.TippingFee, 2);
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                            ChildModel.LevyCharge = r.LevyCharge;
                            ChildModel.TotalLevyCharge = r.TotalLevyCharge;
                            ChildModel.LevyChargeSummary = r.LevyChargeSummary;
                            ChildModel.GlideboxCommission = Math.Round(r.GlideboxCommission.GetValueOrDefault(0), 2);
                            model1.TotalPayable = model1.TotalPayable + r.TotalPayable;

                            model1.GlideboxCommission = model1.GlideboxCommission + r.GlideboxCommission.GetValueOrDefault(0);
                            model1.ResultListChild.Add(ChildModel);

                            var glideboxCommissionResult = GetGlideboxCommissionSameDayCashing(r.docket_key, r.BatchID);

                            if (glideboxCommissionResult != null)
                            {
                                model1.ResultGlideboxCommissionListChild.Add(glideboxCommissionResult);
                            }

                            modelResult.Add(model1);
                        }
                    }
                    modelPackage.FaceValueTotal = faceValueTotal;
                    modelPackage.CommissionTotal = commissionTotalTotal;
                    modelPackage.ExtraTotal = extraTotal;
                    modelPackage.Count = count;
                    modelPackage.GrandTotal = GrandTotal;
                    modelPackage.BatchTotal = BatchTotal;
                    modelPackage.ResultList = modelResult;
                    modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                    modelPackage.Member_key = Member_Key;
                    modelPackage.LodgementReferenceNo = LodgementReferenceNo;
                    modelPackage.HandlingFeeSummary = HandlingTotal;
                    modelPackage.TotalGlideboxCommission = totalGlideboxCommission;

                    MemberModel mobj = new MemberModel();
                    mobj = mda.GetMemeberLowVersionForCashing(Member_Key);
                    modelPackage.memberid = mobj.MemberId;
                    modelPackage.TradingName = mobj.TradingName;

                    modelPackage.Name = mobj.FirstName + " " + mobj.LastName;
                    modelPackage.Mobile = mobj.Mobile;
                    modelPackage.Phone = mobj.Telephone;
                    modelPackage.PhotoID = mobj.PhotoID;
                    //modelPackage.LastCashedAmt = mobj.LastCashedAmt;
                    //modelPackage.LastCashedDateTime = mobj.LastCashedDateTime;
                    //modelPackage.DriverCertificateId = mobj.DriverCertificateId;

                    //modelPackage.Debit = mobj.CRDebitRate;
                    //modelPackage.Visa = mobj.CRVisaRate.Value;
                    //modelPackage.Master = mobj.CRMasterCardRate.Value;
                    //modelPackage.Diners = mobj.CRDinersRate.Value;
                    //modelPackage.Amex = mobj.CRAmexRate.Value;
                    //modelPackage.DebitPercentage = mobj.CRDebitRatePercentage.Value;
                    modelPackage.Companykey = mobj.CompanyKey;

                    if (mobj.ABN != null)
                        modelPackage.ABN = mobj.ABN;
                    else
                        modelPackage.ABN = mobj.ACN;

                    OperatorDA oda1 = new OperatorDA();
                    OperatorModel opt = new OperatorModel();
                    opt = oda1.Get(Member_Key);
                    if (opt.AllowCash == false)
                    {
                        modelPackage.BatchStatus = "Bank Customer";
                    }
                    else
                    {
                        modelPackage.BatchStatus = "Cash Customer";
                    }

                    //RewardPointDA rda = new RewardPointDA();
                    //RewardPointsModel rewardObj = new RewardPointsModel();
                    //rewardObj = rda.Get(Member_Key);
                    //modelPackage.TotalPoints = rewardObj.TotalTrans;
                    //modelPackage.TotalPointsUsed = rewardObj.TotalPointsUsed;
                    //modelPackage.TotalPointsAvailable = rewardObj.TotalPoints;

                    //MemberNotesDA mdna = new MemberNotesDA();
                    //IEnumerable<MemberNotesModel> mnm;
                    //mnm = mdna.GetMemberNotes(Member_Key).Take(15).ToList();
                    //modelPackage.MemberNotes = mnm;

                }
            }

            modelPackage.TipProTotal = Math.Round(modelPackage.ResultList.Sum(n => n.TipPro.GetValueOrDefault(0)), 2);
            modelPackage.TippingFeeTotal = Math.Round(modelPackage.ResultList.Sum(n => n.TippingFee.GetValueOrDefault(0)), 2);

            return modelPackage;
        }

        public WebCashingResult ChargeBackDocketCheck(string description, int docket_key, string username, int DocketCheckSummaryKey, int MemberKey, bool GenerateAccordin)
        {
            WebCashingResult modelPackage = new WebCashingResult();
            List<WebCashing_GetEFTDocketsByBatchID> modelResult = new List<WebCashing_GetEFTDocketsByBatchID>();
            MemberDA mda = new MemberDA();
            decimal faceValueTotal = 0.0M;
            decimal commissionTotalTotal = 0.0M;
            decimal extraTotal = 0.0M;
            decimal GrandTotal = 0.0M;
            decimal BatchTotal = 0.0M;
            decimal HandlingTotal = 0.0M;

            string LodgementReferenceNo = string.Empty;
            int count = 0;
            int Member_Key = 0;
            List<WebCashing_AddChargeBack_Result> ResultList = new List<WebCashing_AddChargeBack_Result>();

            //int monthToday = DateTime.Now.Month;
            //string DocketMonth = BottomBC.Substring(2, 2);
            //int year = DateTime.Now.Year;
            //if ((monthToday == 1 || monthToday == 2) && (DocketMonth == "12" || DocketMonth == "11" || DocketMonth == "10" || DocketMonth == "09" || DocketMonth == "08" || DocketMonth == "07"))
            //{
            //    year = DateTime.Now.Year - 1;
            //}
            //string tempbatch = TopBC.Substring(0, 8).ToString() + BottomBC.Substring(0, 6).ToString() + year.ToString();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.Database.CommandTimeout = 180;
                if (DocketCheckSummaryKey == 0)
                {
                    ResultList = con.WebCashing_AddChargeBack(docket_key, MemberKey, null, username, description).ToList();
                }
                else
                {
                    ResultList = con.WebCashing_AddChargeBack(docket_key, MemberKey, DocketCheckSummaryKey, username, description).ToList();
                }
                if (ResultList != null && ResultList.Count > 0)
                {
                    modelPackage.DocketCheckSummaryKey = ResultList.FirstOrDefault().DocketCheckSummaryKey;
                }
            }
            if (GenerateAccordin)
            {
                if (ResultList.Count() > 0)
                {
                    foreach (var r in ResultList)
                    {
                        WebCashing_GetEFTDocketsByBatchID model1 = new WebCashing_GetEFTDocketsByBatchID();
                        model1 = (from n in modelResult
                                  where n.BatchID == r.BatchID
                                  select n).FirstOrDefault();

                        if (model1 == null)
                        {
                            WebCashing_GetEFTDocketsByBatchID model = new WebCashing_GetEFTDocketsByBatchID();
                            model.BatchStatus = r.BatchStatus;
                            model.docketdate = r.docketdate;
                            model.TerminalId = r.TerminalId;

                            model.DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            model.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            model.LodgementReferenceNo = r.LodgementReferenceNo;
                            model.PendingCommision = r.PendingCommision;
                            model.PendingTransactionValue = r.PendingTransactionValue;
                            model.BatchID = r.BatchID;

                            model.FaceValue = Math.Round(r.TotalBatchFaceValue.Value, 2);
                            model.TOtalcommission = Math.Round(r.TotalBatchCommission.Value, 2);
                            model.Extras = Math.Round(r.TotalBatchExtras.Value, 2);
                            model.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            model.TotalPayable = Math.Round(r.TotalPayable, 2);
                            model.CardType = r.CardType;
                            model.TotalBatchHandlingFee = Math.Round(r.TotalBatchHandlingFee.Value, 2);

                            model.LevyCharge = r.LevyCharge;
                            model.TotalLevyCharge = r.TotalLevyCharge;
                            model.LevyChargeSummary = r.LevyChargeSummary;


                            faceValueTotal = Math.Round(r.FaceValueSummary.Value, 2);
                            commissionTotalTotal = Math.Round(r.CommissionSummary.Value, 2);
                            extraTotal = Math.Round(r.ExtrasSummary.Value, 2);
                            BatchTotal = Math.Round(r.DocketTotalSummary.Value, 2);
                            GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            HandlingTotal = Math.Round(r.HandlingFeeSummary.Value, 2);



                            if (r.Member_key != null)
                                Member_Key = r.Member_key.Value;
                            DocketCheckSummaryKey = r.DocketCheckSummaryKey;
                            LodgementReferenceNo = r.LodgementReferenceNo;
                            if ((r.BatchStatus).ToLower() == "valid")
                            {
                                count = count + 1;
                            }

                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.PaymentStatus = r.PaymentStatus;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                            ChildModel.LevyCharge = r.LevyCharge;
                            ChildModel.TotalLevyCharge = r.TotalLevyCharge;
                            ChildModel.LevyChargeSummary = r.LevyChargeSummary;

                            model.ResultListChild.Add(ChildModel);

                            modelResult.Add(model);
                        }
                        else
                        {
                            modelResult.Remove(model1);
                            WebCashing_GetEFTDocketByBatchIDChild ChildModel = new WebCashing_GetEFTDocketByBatchIDChild();
                            ChildModel.BatchStatus = r.BatchStatus;
                            ChildModel.BatchTotalPayable = r.BatchTotalPayable;
                            ChildModel.docketdate = r.docketdate;
                            ChildModel.docket_key = r.docket_key;
                            ChildModel.PaymentStatus = r.PaymentStatus;
                            ChildModel.TerminalId = r.TerminalId;
                            ChildModel.TotalPayable = r.TotalPayable;
                            ChildModel.GrandTotal = Math.Round(r.GrandTotal.Value, 2);
                            ChildModel.PendingCommision = r.PendingCommision;
                            ChildModel.PendingTransactionValue = r.PendingTransactionValue;
                            ChildModel.FaceValue = Math.Round(r.FaceValue.Value, 2);
                            ChildModel.TOtalcommission = Math.Round(r.TotalCommission.Value, 2);
                            ChildModel.Extras = Math.Round(r.Extras.Value, 2);
                            ChildModel.BatchTotal = Math.Round(r.BatchTotal.Value, 2);
                            ChildModel.DocketTotal = Math.Round(r.DocketTotal.Value, 2);
                            ChildModel.handlingFee = Math.Round(r.handlingFee, 2);
                            ChildModel.LevyCharge = r.LevyCharge;
                            ChildModel.TotalLevyCharge = r.TotalLevyCharge;
                            ChildModel.LevyChargeSummary = r.LevyChargeSummary;

                            model1.TotalPayable = model1.TotalPayable + r.TotalPayable;

                            model1.ResultListChild.Add(ChildModel);
                            modelResult.Add(model1);

                        }
                    }
                    modelPackage.FaceValueTotal = faceValueTotal;
                    modelPackage.CommissionTotal = commissionTotalTotal;
                    modelPackage.ExtraTotal = extraTotal;
                    modelPackage.Count = count;
                    modelPackage.GrandTotal = GrandTotal;
                    modelPackage.BatchTotal = BatchTotal;
                    modelPackage.ResultList = modelResult;
                    modelPackage.DocketCheckSummaryKey = DocketCheckSummaryKey;
                    modelPackage.Member_key = Member_Key;
                    modelPackage.LodgementReferenceNo = LodgementReferenceNo;
                    modelPackage.HandlingFeeSummary = HandlingTotal;
                    MemberModel mobj = new MemberModel();
                    mobj = mda.GetMemeberLowVersionForCashing(Member_Key);
                    modelPackage.memberid = mobj.MemberId;
                    modelPackage.TradingName = mobj.TradingName;

                    modelPackage.Name = mobj.FirstName + " " + mobj.LastName;
                    modelPackage.Mobile = mobj.Mobile;
                    modelPackage.Phone = mobj.Telephone;
                    modelPackage.PhotoID = mobj.PhotoID;
                    modelPackage.Companykey = mobj.CompanyKey;
                    if (mobj.ABN != null)
                        modelPackage.ABN = mobj.ABN;
                    else
                        modelPackage.ABN = mobj.ACN;

                    OperatorDA oda1 = new OperatorDA();
                    OperatorModel opt = new OperatorModel();
                    opt = oda1.Get(Member_Key);
                    if (opt.AllowCash == false)
                    {
                        modelPackage.BatchStatus = "Bank Customer";
                    }
                    else
                    {
                        modelPackage.BatchStatus = "Cash Customer";
                    }

                    //RewardPointDA rda = new RewardPointDA();
                    //RewardPointsModel rewardObj = new RewardPointsModel();
                    //rewardObj = rda.Get(Member_Key);
                    //modelPackage.TotalPoints = rewardObj.TotalTrans;
                    //modelPackage.TotalPointsUsed = rewardObj.TotalPointsUsed;
                    //modelPackage.TotalPointsAvailable = rewardObj.TotalPoints;

                    //MemberNotesDA mdna = new MemberNotesDA();
                    //IEnumerable<MemberNotesModel> mnm;
                    //mnm = mdna.GetMemberNotes(Member_Key).Take(4).ToList();
                    //modelPackage.MemberNotes = mnm;

                }
            }

            return modelPackage;
        }

        private List<WebCashing_GetEFTDocketsByBatchID1_Result1> ConvertAuthorised(List<WebCashing_ChangeAuthorisedMember_Result> authModel)
        {
            List<WebCashing_GetEFTDocketsByBatchID1_Result1> model = new List<WebCashing_GetEFTDocketsByBatchID1_Result1>();
            foreach (var a in authModel)
            {
                WebCashing_GetEFTDocketsByBatchID1_Result1 m = new WebCashing_GetEFTDocketsByBatchID1_Result1();
                m.BatchID = a.BatchID;
                m.Member_key = a.Member_key;
                m.docketdate = a.docketdate;
                m.docket_key = a.docket_key;
                m.DocketBatchId = a.DocketBatchId;
                m.CreatedByUser = a.CreatedByUser;
                m.DocketCheckSummaryKey = a.DocketCheckSummaryKey;
                m.memberid = a.memberid;
                m.TradingName = a.TradingName;
                m.state = a.state;
                m.TerminalId = a.TerminalId;
                m.FaceValue = a.FaceValue;
                m.Extras = a.Extras;
                m.DocketTotal = a.DocketTotal;
                m.TotalCommission = a.TotalCommission;
                m.TotalPaid = a.TotalPaid;
                m.BatchTotalPayable = a.BatchTotalPayable;
                m.TotalPayable = a.TotalPayable.GetValueOrDefault(0);//.Value;
                m.CardNumberMasked = a.CardNumberMasked;
                m.BatchNumber = a.BatchNumber;
                m.PaymentStatus = a.PaymentStatus;
                m.GrandTotal = a.GrandTotal;
                m.BatchStatus = a.BatchStatus;
                m.IsCashable = a.IsCashable;
                m.PendingTransactionValue = a.PendingTransactionValue;
                m.PendingCommision = a.PendingCommision;
                m.PaidTransactionValue = a.PaidTransactionValue;
                m.PaidCommission = a.PaidCommission;
                m.LodgementReferenceNo = a.LodgementReferenceNo;
                m.BatchTotal = a.BatchTotal;
                m.DocketTotalSummary = a.DocketTotalSummary;
                m.FaceValueSummary = a.FaceValueSummary;
                m.ExtrasSummary = a.ExtrasSummary;
                m.CommissionSummary = a.CommissionSummary;
                m.TotalBatchCommission = a.TotalBatchCommission;
                m.TotalBatchFaceValue = a.TotalBatchFaceValue;
                m.TotalBatchExtras = a.TotalBatchExtras;
                m.CardType = a.CardType;
                m.handlingFee = a.handlingFee;
                m.TotalBatchHandlingFee = a.TotalBatchHandlingFee;
                m.HandlingFeeSummary = a.HandlingFeeSummary;
                m.TotalBatchChargebacks = a.TotalBatchChargebacks;
                m.Chargebacks = a.Chargebacks;
                m.ChargebacksSummary = a.ChargebacksSummary;
                m.LevyCharge = a.LevyCharge;
                m.TotalLevyCharge = a.TotalLevyCharge;
                m.LevyChargeSummary = a.LevyChargeSummary;
                model.Add(m);
            }
            return model;
        }
        private List<WebCashing_RemoveEFTDocketsByBatchID1_Result> ConvertWebCashing_RemoveDocket(List<WebCashing_RemoveDocket_Result> docModel)
        {


            List<WebCashing_RemoveEFTDocketsByBatchID1_Result> model = new List<WebCashing_RemoveEFTDocketsByBatchID1_Result>();
            foreach (var a in docModel)
            {
                this.LogInfo(String.Format("Convert Web Cashing DocketKey:{0}", a.docket_key));
                WebCashing_RemoveEFTDocketsByBatchID1_Result m = new WebCashing_RemoveEFTDocketsByBatchID1_Result();
                m.BatchID = a.BatchID;
                m.Member_key = a.Member_key;
                m.docketdate = a.docketdate;
                m.docket_key = a.docket_key;
                m.DocketBatchId = a.DocketBatchId;
                m.CreatedByUser = a.CreatedByUser;
                m.DocketCheckSummaryKey = a.DocketCheckSummaryKey;
                m.memberid = a.memberid;
                m.TradingName = a.TradingName;
                m.state = a.state;
                m.TerminalId = a.TerminalId;
                m.FaceValue = a.FaceValue;
                m.Extras = a.Extras;
                m.DocketTotal = a.DocketTotal;
                m.TotalCommission = a.TotalCommission;
                m.TotalPaid = a.TotalPaid;
                m.BatchTotalPayable = a.BatchTotalPayable;
                m.TotalPayable = a.TotalPayable;
                m.CardNumberMasked = a.CardNumberMasked;
                m.BatchNumber = a.BatchNumber;
                m.PaymentStatus = a.PaymentStatus;
                m.GrandTotal = a.GrandTotal;
                m.BatchStatus = a.BatchStatus;
                m.IsCashable = a.IsCashable;
                m.PendingTransactionValue = a.PendingTransactionValue;
                m.PendingCommision = a.PendingCommision;
                m.PaidTransactionValue = a.PaidTransactionValue;
                m.PaidCommission = a.PaidCommission;
                m.LodgementReferenceNo = a.LodgementReferenceNo;
                m.BatchTotal = a.BatchTotal;
                m.DocketTotalSummary = a.DocketTotalSummary;
                m.FaceValueSummary = a.FaceValueSummary;
                m.ExtrasSummary = a.ExtrasSummary;
                m.CommissionSummary = a.CommissionSummary;
                m.TotalBatchCommission = a.TotalBatchCommission;
                m.TotalBatchFaceValue = a.TotalBatchFaceValue;
                m.TotalBatchExtras = a.TotalBatchExtras;
                m.CardType = a.CardType;
                m.handlingFee = a.handlingFee;
                m.TotalBatchHandlingFee = a.TotalBatchHandlingFee;
                m.HandlingFeeSummary = a.HandlingFeeSummary;
                m.TotalBatchChargebacks = a.TotalBatchChargebacks;
                m.Chargebacks = a.Chargebacks;
                m.ChargebacksSummary = a.ChargebacksSummary;


                m.LevyCharge = a.LevyCharge;
                m.TotalLevyCharge = a.TotalLevyCharge;
                m.LevyChargeSummary = a.LevyChargeSummary;

                m.GlideboxCommission = a.GlideboxCommission;
                m.TotalGlideboxCommission = a.TotalGlideboxCommission;
                //m.ChargebacksSummary = a.ChargebacksSummary;

                model.Add(m);

            }
            return model;
        }
        private List<WebCashing_GetEFTDocketsByBatchID1_Result1> ConvertNoReceptDocket(List<WebCashing_AddDocketsToCashing_Result> docModel)
        {
            List<WebCashing_GetEFTDocketsByBatchID1_Result1> model = new List<WebCashing_GetEFTDocketsByBatchID1_Result1>();
            foreach (var a in docModel)
            {
                WebCashing_GetEFTDocketsByBatchID1_Result1 m = new WebCashing_GetEFTDocketsByBatchID1_Result1();
                m.BatchID = a.BatchID;
                m.Member_key = a.Member_key;
                m.docketdate = a.docketdate;
                m.docket_key = a.docket_key;
                m.DocketBatchId = a.DocketBatchId;
                m.CreatedByUser = a.CreatedByUser;
                m.DocketCheckSummaryKey = a.DocketCheckSummaryKey;
                m.memberid = a.memberid;
                m.TradingName = a.TradingName;
                m.state = a.state;
                m.TerminalId = a.TerminalId;
                m.FaceValue = a.FaceValue;
                m.Extras = a.Extras;
                m.DocketTotal = a.DocketTotal;
                m.TotalCommission = a.TotalCommission;
                m.TotalPaid = a.TotalPaid;
                m.BatchTotalPayable = a.BatchTotalPayable;
                m.TotalPayable = a.TotalPayable.GetValueOrDefault(0);
                m.CardNumberMasked = a.CardNumberMasked;
                m.BatchNumber = a.BatchNumber;
                m.PaymentStatus = a.PaymentStatus;
                m.GrandTotal = a.GrandTotal;
                m.BatchStatus = a.BatchStatus;
                m.IsCashable = a.IsCashable;
                m.PendingTransactionValue = a.PendingTransactionValue;
                m.PendingCommision = a.PendingCommision;
                m.PaidTransactionValue = a.PaidTransactionValue;
                m.PaidCommission = a.PaidCommission;
                m.LodgementReferenceNo = a.LodgementReferenceNo;
                m.BatchTotal = a.BatchTotal;
                m.DocketTotalSummary = a.DocketTotalSummary;
                m.FaceValueSummary = a.FaceValueSummary;
                m.ExtrasSummary = a.ExtrasSummary;
                m.CommissionSummary = a.CommissionSummary;
                m.TotalBatchCommission = a.TotalBatchCommission;
                m.TotalBatchFaceValue = a.TotalBatchFaceValue;
                m.TotalBatchExtras = a.TotalBatchExtras;
                m.CardType = a.CardType;
                m.handlingFee = a.handlingFee;
                m.TotalBatchHandlingFee = a.TotalBatchHandlingFee;
                m.HandlingFeeSummary = a.HandlingFeeSummary;
                m.TotalBatchChargebacks = a.TotalBatchChargebacks;
                m.Chargebacks = a.Chargebacks;
                m.ChargebacksSummary = a.ChargebacksSummary;
                m.LevyCharge = a.LevyCharge;
                m.TotalLevyCharge = a.TotalLevyCharge;
                m.LevyChargeSummary = a.LevyChargeSummary;
                m.GlideboxCommission = a.GlideboxCommission;
                m.TotalGlideboxCommission = a.TotalGlideboxCommission;
                model.Add(m);
            }
            return model;
        }

        public void SaveNoReceiptLogs(WebCashingResult modelList, List<WebCashing_GetPendingBatchesHeaderByMemberKey> noReceiptModels, string currentUsername)
        {
            if (noReceiptModels == null || modelList == null)
            {
                return;
            }

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var receiptsInDocket = modelList.ResultList.Select(r => new
                {
                    batchId = r.BatchID,
                    batchNumber = r.BatchNumber,
                    terminalId = r.TerminalId,
                    memberId = r.memberid
                }).ToList();
                var noReceiptBatchIds = noReceiptModels.Select(model => model.DocketBatchId).ToList();

                List<CashLog> cashLogs = new List<CashLog>();
                foreach (var receipt in receiptsInDocket)
                {
                    if (noReceiptBatchIds.Contains(receipt.batchId))
                    {

                        CashLog log = new CashLog();
                        log.CreatedUser = currentUsername;
                        log.CreatedDate = DateTime.Now;
                        log.MemberId = modelList.memberid;
                        log.TerminalId = receipt.terminalId;
                        log.BatchNumber = receipt.batchNumber;
                        cashLogs.Add(log);
                    }
                }
                con.CashLogs.AddRange(cashLogs);
                con.SaveChanges();
            }
        }

        public GlideboxCategoriesCommissionForSameDayCashingModel GetGlideboxCommissionSameDayCashing(int docketKey, string batchID)
        {
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                var glidebox = entities.GlideboxSameDayCashings.FirstOrDefault(x => x.DocketKey == docketKey && x.BatchID == batchID);

                if (glidebox != null)
                {
                    var glideboxCommissionResult = new GlideboxCategoriesCommissionForSameDayCashingModel
                    {
                        CategoryID = glidebox.CategoryID.Value,
                        ShortName = glidebox.ShortName,
                        Quantity = glidebox.Quantity,
                        CommissionAmount = glidebox.CommissionAmount.GetValueOrDefault(0),
                        TotalCommissionAmount = glidebox.TotalGlideboxCommission.GetValueOrDefault(0)
                    };

                    return glideboxCommissionResult;
                }
            }

            return null;
        }
    }
}


