﻿using Phoenix_Service;
using PhoenixObjects.LiveSMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class SMSMessageDA
    {
       public int AddSMSMessageOut(SMSOutModel t)
       {
           int key = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               SMSMessageOut aSever = new SMSMessageOut();
               aSever.SMSMessageOut_key = t.SMSMessageOut_key;
               aSever.MessageTo = t.MessageTo;
               aSever.MessageFrom = t.MessageFrom;
               aSever.MessageText = t.MessageText;
               aSever.MessageType = t.MessageType;
               aSever.Gateway = t.Gateway;
               aSever.UserId = t.UserId;
               aSever.UserInfo = t.UserInfo;
               aSever.Scheduled = t.Scheduled;
               aSever.IsSent = t.IsSent;
               aSever.IsRead = t.IsRead;
               aSever.Modified_dttm = t.Modified_dttm;
               aSever.ModifiedByUser = t.ModifiedByUser;
               aSever.member_key = t.member_key;           

               context.SMSMessageOuts.Add(aSever);
               context.SaveChanges();
               key = aSever.SMSMessageOut_key;
           }
           return key;
       }
    }
}

      