﻿using Phoenix_Service;
using PhoenixObjects.Customer;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class BsbListDA
    {

        public List<BSBListModel> Get()
        {
            List<BSBListModel> account = new List<BSBListModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var acc = from m in context.BSBLists                       
                          select m;

                if (acc.Count() > 0)
                {
                    foreach (var t in acc)
                    {
                        BSBListModel acct = new BSBListModel();
                        acct.BSBListKey = t.BSBListKey;
                        acct.BSB = t.BSB;
                        acct.Bank = t.Bank;
                        acct.BranchName = t.BranchName;
                        acct.Address = t.Address;
                        acct.Suburb = t.Suburb;
                        acct.Postcode = t.Postcode;
                        acct.State = t.State;
                        acct.Location = t.Location;
                      
                        account.Add(acct);
                    }
                }
            }
            return account;
        }
        public List<string> GetSerach(string Name)
        {
            List<string> modelList = new List<string>();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr =    (from m in context.BSBLists   
                              where m.BSB.StartsWith(Name)
                          select m).Take(10).ToList();

                if (addr != null)
                {
                    foreach (var n in addr)
                    {
                        string model = string.Empty;
                        model = n.BSB + " -" + n.Bank + ", " + n.BranchName + ", " + n.State;
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        public  bool IsBSBValid(string Bsb)
        {
            bool Valid = false;

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = from m in context.BSBLists
                           where m.BSB == Bsb
                           select m;

                if (addr != null)
                {
                    Valid = true;
                }
            }
            return Valid;
        }
    }
}

