﻿using Phoenix_Service;
using PhoenixObjects.Member.DriverCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class DriverCardStatusDA
    {
        public DriverCardStatu GetIssuedStatus()
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var status = (from dc in con.DriverCardStatus
                              where dc.Name == "ISSUED"
                              select dc)
                          .SingleOrDefault();
                return status;
            }
        }

        public List<DriverCardStatu> GetAllCardStatus()
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var list = con.DriverCardStatus.Where(s => s.Name != "ISSUED" && s.Name != "CLOSED").ToList();
                return list;
            }
        }

        public DriverCardStatu GetStatus(int statusID)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var status = con.DriverCardStatus.Where(s => s.ID == statusID).FirstOrDefault();
                return status;
            }
        }

        public DriverCardStatu GetStatus(string statusNameOrCode)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                if (statusNameOrCode.Length > 1)
                {
                    return con.DriverCardStatus.Where(s => s.Name == statusNameOrCode).FirstOrDefault();
                }
                else
                {
                    return con.DriverCardStatus.Where(s => s.StatusCode == statusNameOrCode).FirstOrDefault();
                }
            }
        }
    }
}
