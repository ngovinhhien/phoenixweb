﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Search;
using System.Data.Entity.Validation;
using PhoenixObjects.Financial;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Member;
using Phoenix_Service;

namespace Phoenix_BusLog.Data
{
    public class MemberProfilePictureDA
    {
        public MemberProfilePictureModel GetMemberProfilePicture(int MemberProfilePictureKey)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                MemberProfilePicture photo = (from p in entity.MemberProfilePictures
                                              where p.MemberProfilePictureKey == MemberProfilePictureKey
                                              select p).FirstOrDefault();

                if (photo != null)
                {
                    MemberProfilePictureModel photoid = new PhoenixObjects.Member.MemberProfilePictureModel(photo);
                    return photoid;
                }
                else
                    return null;

            }
        }

        public MemberProfilePictureModel GetCurrentMemberProfilePicture(int _memberKey)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                int mkey = _memberKey;
                MemberProfilePicture photo = (from p in entity.MemberProfilePictures
                                              join m in entity.Members on p.MemberProfilePictureKey equals m.MemberProfilePictureKey
                                              where m.Member_key == mkey
                                              select p).FirstOrDefault();
                if (photo != null)
                {
                    MemberProfilePictureModel photoid = new PhoenixObjects.Member.MemberProfilePictureModel(photo);
                    return photoid;
                }
                else
                    return null;

            }
        }

        public void AddMemberProfilePicture(MemberProfilePictureModel profilepicture)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                //Add new photoID
                MemberProfilePicture memberprofile = new MemberProfilePicture();
                memberprofile.member_key = profilepicture.member_key;
                memberprofile.UpdatedByUser = profilepicture.UpdatedByUser;
                memberprofile.CreatedByUser = profilepicture.CreatedByUser;
                memberprofile.ProfilePicture = profilepicture.ProfilePicture;
                memberprofile.UpdatedDate = profilepicture.UpdatedDate;
                memberprofile.CreatedDate = profilepicture.CreatedDate;
                entity.MemberProfilePictures.Add(memberprofile);
                entity.SaveChanges();

                //Update the default photoID to the new one
                MemberDA memda = new MemberDA();
                memda.UpdateDefaultProfilePicture(memberprofile.member_key.GetValueOrDefault(0), memberprofile.MemberProfilePictureKey);
            }
        }
    }
}
