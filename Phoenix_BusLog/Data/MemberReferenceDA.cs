﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{

    /// <summary>
    /// This class represents Member Refernece to store qantas refernce numbers
    /// </summary>
    /// <seealso cref="MemberDA"/>
    public class MemberReferenceDA: IDisposable
    {
        public void Add(MemberReferenceModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                MemberReference memberReference = new MemberReference();
                memberReference.MemberReferenceNumber = t.MemberReferenceNumber;
                memberReference.member_key = t.member_key;
                context.MemberReferences.Add(memberReference);
                context.SaveChanges();
            };
        }

        public void Dispose()
        {
            
        }

        public void Edit(MemberReferenceModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var memberReference = (from a in context.MemberReferences
                              where a.member_key == t.member_key
                              select a).FirstOrDefault();

                if (memberReference != null)
                {
                    memberReference.MemberReferenceNumber = t.MemberReferenceNumber;
                    memberReference.member_key = t.member_key;                    
                }
                context.SaveChanges();
            }
        }
        
        public string[] Get(int memberKey)
        {            
            string[] referencesNumbers = null;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                referencesNumbers = (from m in context.MemberReferences
                         where m.member_key == memberKey
                         select m.MemberReferenceNumber).ToArray();                
            }
            return referencesNumbers;
        }

        public List<MemberReferenceModel> GetList(int memberKey)
        {
            List<MemberReferenceModel> memberReferenceModels = new List<MemberReferenceModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var acc = from m in context.MemberReferences
                          where m.member_key == memberKey
                          select m;

                if (acc.Count() > 0)
                {
                    foreach (var t in acc)
                    {
                        MemberReferenceModel memberReferenceModel = new MemberReferenceModel();
                        memberReferenceModel.member_key = t.member_key;
                        memberReferenceModel.MemberReferenceNumber = t.MemberReferenceNumber;
                        memberReferenceModels.Add(memberReferenceModel);
                    }
                }
            }
            return memberReferenceModels;
        }
    }
}
