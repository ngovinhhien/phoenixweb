﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class MemberNotesDA
    {
       public void Add(MemberNotesModel t)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               Phoenix_Service.MemberNote notes = new Phoenix_Service.MemberNote();
               notes.NoteDate = t.NoteDate;
               notes.Warning = t.Warning;
               notes.Note = t.Note;
               notes.Modified_dttm = t.Modified_dttm;
               notes.ModifiedByUser = t.ModifiedByUser;
               notes.Member_key = t.Member_key;

               context.MemberNotes.Add(notes);
               context.SaveChanges();
           }
       }

       public void ChangeNoteWarningStatus(int MemberNotekey, bool type)
       {
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var notes = (from n in context.MemberNotes
                            where n.MemberNote_key == MemberNotekey
                            select n).FirstOrDefault();
               if(notes != null)
               {
                   if (type == false)
                   {
                       notes.Warning = false;
                   }
                   else
                   {
                       notes.Warning = true;
                   }
                   context.SaveChanges();
               }             
           }
       }
    

       public List<MemberNotesModel> GetMemberNotes(int Member_key)
       {
           List<MemberNotesModel> model = new List<MemberNotesModel>();
        
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = (from si in context.MemberNotes
                        where si.Member_key == Member_key && si.Warning == true
                        orderby si.NoteDate descending
                        select si).Take(10);
               
               if (sk != null)
               {
                   foreach (var skk in sk)
                   {
                       MemberNotesModel MemberNotes = new MemberNotesModel();
                       MemberNotes.MemberNote_key = skk.MemberNote_key;
                       MemberNotes.NoteDate =  skk.NoteDate;
                       MemberNotes.Warning = skk.Warning;
                       MemberNotes.Note = skk.Note;
                       MemberNotes.Member_key = skk.Member_key;
                       model.Add(MemberNotes);
                   }
               }
           }
           return model.OrderByDescending(n => n.NoteDate).ToList();
       }

       public bool DoesMemberHaveWarning(int Member_key)
       {
           bool model = false;

           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = (from si in context.MemberNotes
                         where si.Member_key == Member_key && si.Warning == true
                         orderby si.NoteDate descending
                         select si).ToList();

            
               if (sk.Count > 0)
               {
                   model = true;
               }
             
           }
           return model;
       }
       public List<MemberNotesModel> GetAllMemberNotes(int Member_key)
       {
           List<MemberNotesModel> model = new List<MemberNotesModel>();

           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {            

               var sk1 = from si in context.MemberNotes
                          where si.Member_key == Member_key
                          orderby si.NoteDate descending
                          select si;
               if (sk1 != null)
               {
                   foreach (var skk in sk1)
                   {
                       MemberNotesModel MemberNotes = new MemberNotesModel();
                       MemberNotes.MemberNote_key = skk.MemberNote_key;
                       MemberNotes.NoteDate = skk.NoteDate;
                       MemberNotes.Warning = skk.Warning;
                       MemberNotes.Note = skk.Note;
                       MemberNotes.Member_key = skk.Member_key;
                       MemberNotes.ModifiedByUser = skk.ModifiedByUser;
                       model.Add(MemberNotes);
                   }
               }             
           }
           return model.OrderByDescending(n => n.NoteDate).ToList();
       }
   }
}
