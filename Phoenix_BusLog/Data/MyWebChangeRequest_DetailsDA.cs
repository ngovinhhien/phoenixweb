﻿using Phoenix_Service;
using PhoenixObjects.MyWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class MyWebChangeRequest_DetailsDA
    {
       public List<DetailChangeRequestModel> GetAllPending(string username)
       {
           List<DetailChangeRequestModel> Pending = new List<DetailChangeRequestModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               string status = TEnum.MyWebChangeRequestStatus.PENDING.ToString();
               var addr = context.MyWebChangeRequest_Details.Where(n => n.Status == status);
               if (addr.Any())
               {
                   foreach (var v in addr)
                   {

                       var mem = context.Members.FirstOrDefault(n => n.Member_key == v.Member_key);
                       if (mem != null)
                       {
                           DetailChangeRequestModel mda = new DetailChangeRequestModel();
                           mda.AssignedDate = v.AssignedDate;
                           mda.AssignedTo = v.AssignedTo;
                           mda.CheckedBy = v.CheckedBy;
                           mda.CheckedDate = v.CheckedDate;
                           mda.CompleteBy = v.CompleteBy;
                           mda.CompletedDate = v.CompletedDate;
                           mda.CreateDate = v.CreateDate;
                           mda.CreatedBy = v.CreatedBy;
                           mda.Member_key = v.Member_key;
                           mda.New_Email = v.New_Email;
                           mda.New_Fax = v.New_Fax;
                           mda.New_Mobile = v.New_Mobile;
                           mda.New_Phone = v.New_Phone;
                           mda.New_PostCode = v.New_PostCode;
                           mda.New_State = v.New_State;
                           mda.New_Street = v.New_Street;
                           mda.New_StreetNo = v.New_StreetNo;
                           mda.New_Suburb = v.New_Suburb;
                           mda.RequestID = v.RequestID;                           
                           mda.Status = v.Status;
                           mda.FirstName = mem.FirstName;
                           mda.LastName = mem.LastName;
                           mda.MemberId = mem.MemberId;
                           mda.TradingName = mem.TradingName;
                           mda.Old_Email = mem.Email;
                           mda.Old_Fax = mem.Fax;
                           mda.Old_Mobile = mem.Mobile;
                           mda.Old_Phone = mem.Telephone;
                           mda.Old_PostCode = Convert.ToInt32(mem.Postcode);
                           mda.Old_State = mem.State;
                           mda.Old_Street = mem.Address;
                           mda.Old_Suburb = mem.City;
                           Pending.Add(mda);
                       }
                   }
               }
           }
           return Pending;

       }

       public DetailChangeRequestModel GetRequest(int RequestID)
       {
           DetailChangeRequestModel mda = new DetailChangeRequestModel();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var v = (from m in context.MyWebChangeRequest_Details
                          where m.RequestID == RequestID
                          select m).FirstOrDefault();

               if (v != null)
               {       
                     var mem = (from m in context.Members
                                  where m.Member_key == v.Member_key
                                  select m).FirstOrDefault();

                     if (mem != null)
                     {
                         mda.AssignedDate = v.AssignedDate;
                         mda.AssignedTo = v.AssignedTo;
                         mda.CheckedBy = v.CheckedBy;
                         mda.CheckedDate = v.CheckedDate;
                         mda.CompleteBy = v.CompleteBy;
                         mda.CompletedDate = v.CompletedDate;
                         mda.CreateDate = v.CreateDate;
                         mda.CreatedBy = v.CreatedBy;
                         mda.Member_key = v.Member_key;
                         mda.New_Email = v.New_Email;
                         mda.New_Fax = v.New_Fax;
                         mda.New_Mobile = v.New_Mobile;
                         mda.New_Phone = v.New_Phone;
                         mda.New_PostCode = v.New_PostCode;
                         mda.New_State = v.New_State;
                         mda.New_Street =v.New_Street;
                         mda.New_StreetNo = v.New_StreetNo;
                         mda.New_Suburb = v.New_Suburb;
                         mda.RequestID = v.RequestID;
                         if (v.Image1.Length > 2)
                         {
                             mda.SupportDoc1 = v.Image1;
                         }
                         else
                         {
                             mda.SupportDoc1 = null;
                         }
                         if (v.Image2.Length > 2)
                         {
                             mda.SupportDoc2 = v.Image2;
                         }
                         else
                         {
                             mda.SupportDoc2 = null;
                         }
                         mda.Status = v.Status;
                         mda.FirstName = mem.FirstName;
                         mda.LastName = mem.LastName;
                         mda.MemberId = mem.MemberId;
                         mda.TradingName = mem.TradingName;
                         mda.Old_Email = mem.Email;
                         mda.Old_Fax = mem.Fax;
                         mda.Old_Mobile = mem.Mobile;
                         mda.Old_Phone = mem.Telephone;
                         mda.Old_PostCode = Convert.ToInt32(mem.Postcode);
                         mda.Old_State = mem.State;
                         mda.Old_Street = mem.Address;
                         mda.Old_Suburb = mem.City;
                     }
               }
           }
           return mda;

       }
       public void UpdateStatusComplete(DetailChangeRequestModel mod)
       {         
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var v = (from m in context.MyWebChangeRequest_Details
                        where m.RequestID == mod.RequestID
                        select m).FirstOrDefault();

               if (v != null)
               {
                   v.CompleteBy = mod.CompleteBy;
                   v.CompletedDate = mod.CompletedDate;
                   v.Status = mod.Status;

                   context.SaveChanges();
               }
           }
       }

       public DetailChgReqImageModel GetDocument(int RequestId, int WhichOne)
       {
           DetailChgReqImageModel model = new DetailChgReqImageModel();
           model.SupportDoc = new byte[0];
           // byte[] img = new byte[0];
            
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               if (WhichOne == 1)
               {
                   var v = (from m in context.MyWebChangeRequest_Details
                            where m.RequestID == RequestId
                            select m).FirstOrDefault();

                   if (v != null)
                   {
                       model.SupportDoc = v.Image1;
                       model.Extension = v.Image1Ext;
                   }
               }
               else  if (WhichOne == 2)
               {
                   var v = (from m in context.MyWebChangeRequest_Details
                            where m.RequestID == RequestId
                            select m).FirstOrDefault();

                   if (v != null)
                   {
                       model.SupportDoc = v.Image2;
                       model.Extension = v.Image2Ext;
                   }
               }
           }

           return model;

       }
    }
}
