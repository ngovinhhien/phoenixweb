﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class RewardPointDA
    {
        public RewardPointsModel Get(int MemberKey)
        {
            RewardPointsModel model = new RewardPointsModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                try
                {
                    var sk = context.SPGetRewardPointsByMemberKey(MemberKey).FirstOrDefault();
                    if (sk != null)
                    {
                        model.member_key = sk.member_key;
                        model.memberID = sk.memberID;
                        model.FirstName = sk.FirstName;
                        model.LastName = sk.LastName;
                        model.TradingName = sk.TradingName;
                        model.Email = sk.Email;
                        model.CurrentDate = sk.CurrentDate;
                        model.TotalTrans = sk.TotalTrans;
                        model.TotalPointsUsed = sk.TotalPointsUsed;
                        model.TotalPoints = sk.TotalPoints;
                    }
                }
                catch
                {

                }
            }
            return model;
        }


    }
}
