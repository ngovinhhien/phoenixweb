﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class MerchantAccountDA
    {

        public int GetMerchantAccountKey(string merchantId,int companykey)
        {
            int MerchantAccount_Key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MerchantAccounts
                           where m.MerchantId == merchantId && m.Company_key == companykey
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    MerchantAccount_Key = mem.MerchantAccount_key;
                }
            }

            return MerchantAccount_Key;
        }
        public string GetMerchantId(int companykey)
        {
            string MerchantId = string.Empty;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var mem = (from m in context.MerchantAccounts
                           where m.Company_key == companykey
                           select m).FirstOrDefault();
                if (mem != null)
                {
                    MerchantId = mem.MerchantId;
                }
            }

            return MerchantId;
        }
        public GetWBCPayWayUserNamePassword_Result GetWBCPayWayCredential()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                 GetWBCPayWayUserNamePassword_Result info = context.GetWBCPayWayUserNamePassword().FirstOrDefault();
                 return info;
            }
        }

    }
}
