﻿using Phoenix_Service;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Phoenix_BusLog.Data
{
    public class PermissionsDA
    {
        public bool HasPermission(IPrincipal UserName, string PermissionName)
        {
            bool hasPermission = false;

            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var pp = (from p in context.Permissions
                          where p.PermissionName == PermissionName
                          select p).FirstOrDefault();

                if (pp != null)
                {
                    var sk = (from si in context.RolePermissions
                              where si.PermissionId == pp.PermissionId
                              select si).ToList();
                    if(sk != null)
                    {
                        foreach (var s in sk)
                        {
                            if (UserName.IsInRole(s.RoleName))
                            {
                                hasPermission = true;
                                break;
                            }
                        }
                    }                  
                }
            }
            return hasPermission;
        }

        public void AddPermissionToRole(Guid RoleId, int PermissionId)
        {
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var RoleName = (from si in context.aspnet_Roles
                                where si.RoleId == RoleId
                                select si.RoleName).FirstOrDefault();

                var HasAlready = (from si in context.RolePermissions
                                  where si.PermissionId == PermissionId && si.RoleName == RoleName
                                select si).FirstOrDefault();

                if(HasAlready == null)
                {
                    RolePermission rp = new RolePermission();
                    rp.RoleName = RoleName;
                    rp.RoleId = RoleId;
                    rp.PermissionId = PermissionId;
                    context.RolePermissions.Add(rp);
                    context.SaveChanges();
                }
            }
        }

        public List<string> GetPermission(string UserName)
        {
            List<string> PermissionList = new List<string>();
            var roles = Roles.GetRolesForUser(UserName);
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                foreach(var r in roles)
                {
                    var sk = (from si in context.RolePermissions
                              where si.RoleName == r
                              select si).ToList();
                    if (sk != null)
                    {
                        foreach (var s in sk)
                        {
                            var pp = (from p in context.Permissions
                                      where p.PermissionId == s.PermissionId
                                      select p).FirstOrDefault();
                            if (pp != null)
                            {                                
                                    PermissionList.Add(pp.PermissionId.ToString());                               
                            }
                        }
                    }
                }             
            }
            return PermissionList;
        }

        public List<PermissionModel> GetAllPermissionList()
        {
            List<PermissionModel> PermissionList = new List<PermissionModel>();          
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
              
                    var sk = (from si in context.Permissions                             
                              select si).ToList();
                    if (sk != null)
                    {
                        foreach (var s in sk)
                        {
                            PermissionModel per = new PermissionModel();
                            per.PermissionId = s.PermissionId;
                            per.PermissionName = s.PermissionName;
                            PermissionList.Add(per);
                        }
                    }               
            }
            return PermissionList;
        }


        public List<PermissionModel> GetAllPermissionListByRole(Guid RoleId)
        {
            List<PermissionModel> PermissionList = new List<PermissionModel>();
            using (aspnetdbEntities context = new aspnetdbEntities())
            {

                var sk = (from si in context.RolePermissions
                          join s in context.Permissions on si.PermissionId equals s.PermissionId
                          where si.RoleId == RoleId
                          select new {si.PermissionId, s.PermissionName}).ToList();
                if (sk != null)
                {
                    foreach (var sa in sk)
                    {
                        PermissionModel per = new PermissionModel();
                        per.PermissionId = sa.PermissionId.GetValueOrDefault(0);
                        per.PermissionName = sa.PermissionName;
                        
                        PermissionList.Add(per);
                    }
                }
            }
            return PermissionList;
        }

        public List<PermissionModel> GetAllPermissionNotRole(Guid RoleId)
        {
            List<PermissionModel> InRole = GetAllPermissionListByRole(RoleId);
            List<PermissionModel> AllPer = GetAllPermissionList();

            foreach(var p in InRole)
            {
                var item = AllPer.SingleOrDefault(x => x.PermissionId == p.PermissionId);
                if(item != null)
                {
                    AllPer.Remove(item);
                }
               
            }

            List<PermissionModel> theRest = AllPer.ToList();

            return theRest;
        }

        public List<PermissionModel> GetAllPermissionListByUser(IPrincipal Username)
        {
            List<PermissionModel> PermissionList = new List<PermissionModel>();
            using (aspnetdbEntities context = new aspnetdbEntities())
            {

                var sk = (from si in context.Permissions
                          select si).ToList();
                if (sk != null)
                {
                    foreach (var s in sk)
                    {
                       
                        PermissionModel per = new PermissionModel();
                        per.PermissionId = s.PermissionId;
                        per.PermissionName = s.PermissionName;
                        //var hasper = HasPermission(Username, s.PermissionName);
                        //per.IsCheck = hasper;
                        PermissionList.Add(per);
                    }
                }
            }




            return PermissionList;
        }
    }
}
