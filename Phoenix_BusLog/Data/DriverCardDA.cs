﻿using log4net;
using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.DriverCard;
using PhoenixObjects.Member.DriverCard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_BusLog.Model;

namespace Phoenix_BusLog.Data
{
    public class DriverCardDA
    {
        private static readonly ILog logger = LogManager.GetLogger("DriverCardProcess");
        public List<MemberDriverCardModel> GetByMemberKey(int memberKey)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var cards = (from dc in con.DriverCards
                             join st in con.DriverCardStatus on dc.StatusID equals st.ID
                             where dc.MemberKey == memberKey
                             select new MemberDriverCardModel
                             {
                                 Id = dc.ID,
                                 AccountIdentifier = dc.AccountIdentifier,
                                 MemberKey = dc.MemberKey,
                                 StatusID = (int)dc.StatusID,
                                 StatusCode = st.StatusCode,
                                 Status = st.Name + " - " + st.Description,
                             })
                          .ToList();
                return cards;
            }
        }

        public DriverCard GetByAccessIdentifier(string accessIdentifier)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var card = (from dc in con.DriverCards
                            where dc.AccountIdentifier == accessIdentifier
                            select dc)
                          .SingleOrDefault();
                return card;
            }
        }

        public bool IsExistedCard(string accessIdentifier)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var isExisted = con.DriverCards.Any(x => x.AccountIdentifier == accessIdentifier);
                return isExisted;
            }
        }

        public int Register(string accountIdentifier, int statusId, int memberkey)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                DriverCard card = new DriverCard();
                card.AccountIdentifier = accountIdentifier;
                card.StatusID = statusId;
                card.MemberKey = memberkey;
                con.DriverCards.Add(card);
                con.SaveChanges();

                logger.Info($"{accountIdentifier} - Register - {memberkey} - {DateTime.Now.ToString("dd/MM/yyyy")}");

                return card.ID;
            }
        }

        public DriverCard GetInUseCard(string accountIdentifier, int memberKey)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var card = (from dc in con.DriverCards
                            join st in con.DriverCardStatus on dc.StatusID equals st.ID
                            where dc.MemberKey == memberKey && st.Name != "CLOSED" && dc.AccountIdentifier == accountIdentifier
                            select dc)
                          .SingleOrDefault();
                return card;
            }
        }

        public void UpdateCardStatus(string accountIdentifier, int statusID)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var card = con.DriverCards.Where(c => c.AccountIdentifier == accountIdentifier).SingleOrDefault();
                if (card != null)
                {
                    card.StatusID = statusID;
                    con.SaveChanges();
                }
            }
        }
        public void UpdateCardStatus(string accountIdentifier, string statusCode)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var card = con.DriverCards.Where(c => c.AccountIdentifier == accountIdentifier).SingleOrDefault();
                if (card != null)
                {
                    card.StatusID = con.DriverCardStatus.Where(s => s.StatusCode == statusCode).First().ID;
                    con.SaveChanges();
                }
            }
        }

        public PagedDriverCardHistoryModel GetHistoriesByIdPaging(int cardId, int currentPage, int pageSize)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var allHistories = (from dc in con.DriverCardHistories
                                    where dc.DriverCardId == cardId
                                    select dc);
                var showedHistories = allHistories.OrderByDescending(h => h.Date).Skip(pageSize * (currentPage - 1)).Take(pageSize);
                int total = allHistories.Count();
                int totalPages = (int)Math.Ceiling((float)total / (float)pageSize);
                PagedDriverCardHistoryModel model = new PagedDriverCardHistoryModel();
                model.page = currentPage;
                model.records = total;
                model.total = totalPages;
                model.rows = showedHistories.Select(h => new DriverCardHistoryModel
                {
                    ActionBy = h.ActionBy,
                    ActionDescription = h.ActionDescription,
                    Date = h.Date,
                    DriverCardId = h.DriverCardId,
                    MemberKey = h.MemberKey,
                    Note = h.Note
                }).ToList();
                return model;
            }
        }

        public string GetCardStatusByIdentifier(string identifier)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                string status = (from c in con.DriverCards
                    join s in con.DriverCardStatus on c.StatusID equals s.ID
                    where c.AccountIdentifier == identifier
                    select s.Name).SingleOrDefault();
                return status;
            }
        }

        public void GetOxygenTransaction(ref GridModel<OxygenTransactionSearchVM, List<OxygenTransactionVM>> model)
        {
            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@TransactionDateFrom", model.Request.TransactionDateFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TransactionDateTo", model.Request.TransactionDateTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@CreatedDateFrom", model.Request.CreatedDateFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@CreatedDateTo", model.Request.CreatedDateTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@OxygenTransRefNo", model.Request.OxygenTransRefNo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@MemberID", model.Request.MemberID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@LodgementReferenceNo", model.Request.LodgementReferenceNo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AccountIdentifier", model.Request.AccountIdentifier ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TransactionType", model.Request.TransactionType ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AmountFrom", model.Request.AmountFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AmountTo", model.Request.AmountTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@StatusID", model.Request.StatusID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortField", model.Request.SortField ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortOrder", model.Request.SortOrder ?? string.Empty));

                sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));

                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output
                };
                sqlParameters.Add(totalItemsParam);
                try
                {
                    var response = taxiEpay.Database.SqlQuery<OxygenTransactionVM>("EXEC GetOxygenTransactions "
                    + "  @TransactionDateFrom"
                    + ", @TransactionDateTo"
                    + ", @CreatedDateFrom"
                    + ", @CreatedDateTo"
                    + ", @OxygenTransRefNo"
                    + ", @MemberID"
                    + ", @LodgementReferenceNo"
                    + ", @AccountIdentifier"
                    + ", @TransactionType"
                    + ", @AmountFrom"
                    + ", @AmountTo"
                    + ", @StatusID"
                    + ", @SortField"
                    + ", @SortOrder"
                    + ", @PageSize"
                    + ", @PageIndex"
                    + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();

                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    model.TotalItems = total;
                    model.Response = response;
                }
                catch (Exception e)
                {
                    model.IsError = true;
                    model.Message = e.Message;
                }
            }
        }

        public List<StatusModel> GetAllOxygenTransactionStatus() {
            using (TaxiEpayEntities con = new TaxiEpayEntities()) {
                try
                {
                    var response = con.Database.SqlQuery<StatusModel>("EXEC GetAllOxygenTransactionStatus ").ToList();
                    return response;
                }catch(Exception e)
                {
                    return null;
                }
            }
        }

        public string ApproveTimeoutTransaction(OxygenAPIResponseModel response, int id, string updatedBy)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                try
                {
                    List<SqlParameter> sqlParameters = new List<SqlParameter>();
                    sqlParameters.Add(new SqlParameter("@TransRefID", response.transRefId ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@ID", id));
                    sqlParameters.Add(new SqlParameter("@ResponseDescription", response.responseDescription ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@UpdatedBy", updatedBy));
                    var errorParam = new SqlParameter
                    {
                        ParameterName = "@Error",
                        SqlDbType = SqlDbType.VarChar,
                        Size = -1,
                        Direction = ParameterDirection.Output
                    };
                    sqlParameters.Add(errorParam);

                    con.Database.ExecuteSqlCommand("EXEC ApproveTimeoutOxygenTransaction "
                        + "  @TransRefID"
                        + ", @ID"
                        + ", @ResponseDescription"
                        + ", @UpdatedBy"
                        + ", @Error OUT", sqlParameters.ToArray());
                    if (errorParam.Value != DBNull.Value)
                    {
                        return errorParam.Value.ToString();
                    }
                    else
                    {
                        return "Error executing sql script";
                    }
                }catch(Exception ex)
                {
                    return ex.Message;
                }
            }
        }


        public List<vDriverCard_LoadPendingTransaction> LoadPendingPayment(int MemberKey)
        {
            using (var context = new TaxiEpayEntities())
            {
                var data = context.Database.SqlQuery<vDriverCard_LoadPendingTransaction>(
                    $"SELECT * FROM vDriverCard_LoadTransaction WHERE MemberKey = {MemberKey}").ToList();
                return data;
            }
        }

        public List<vDriverCard_TransactionHistory> LoadTransactionHistory(int transactionKey)
        {
            using (var context = new TaxiEpayEntities())
            {
                var data = context.Database.SqlQuery<vDriverCard_TransactionHistory>(
                    $"SELECT * FROM vDriverCard_TransactionHistory WHERE Transaction_key = {transactionKey}").OrderByDescending(n=>n.TransactionDate).ToList();
                return data;
            }
        }
    }
}
