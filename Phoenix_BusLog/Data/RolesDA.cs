﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Administrator;
using Phoenix_Service;

namespace Phoenix_BusLog.Data
{
   public class RolesDA
    {
       public List<RoleModel> GetRoles()
       {
           List<RoleModel> RolesList = new List<RoleModel>();
           using (aspnetdbEntities context = new aspnetdbEntities())
           {
               var roles = from c in context.aspnet_Roles
                             select c;
               if (roles != null)
               {
                   foreach (var role in roles)
                   {
                       RoleModel r = new RoleModel();
                       r.RoleId = role.RoleId;
                       r.RoleName = role.RoleName;
                      
                       RolesList.Add(r);
                   }
               }
           }
           return RolesList;
       }

   
    }
}
