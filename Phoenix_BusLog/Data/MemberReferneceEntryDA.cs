﻿using Phoenix_Service;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{

    /// <summary>
    /// This class represents Member Refernece Entry to store qantas refernce numbers
    /// </summary>
    public class MemberReferenceEntryDA : IDisposable
    {
        public void Dispose()
        {

        }

        public void Add(MemberReferenceEntryModel memberReferenceEntryModel)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                context.MemberReferenceEntries.Add(new MemberReferenceEntry()
                {
                    CreatedByUser = memberReferenceEntryModel.CreatedByUser,
                    CreatedDateTime = DateTime.Now,
                    Description = memberReferenceEntryModel.Description,
                    EntryDateTime = DateTime.Now,
                    ManualEntry = memberReferenceEntryModel.ManualEntry,
                    Member_Key = memberReferenceEntryModel.Member_Key,
                    MonthPeriod = memberReferenceEntryModel.MonthPeriod,
                    Points = memberReferenceEntryModel.Points,
                    YearPeriod = memberReferenceEntryModel.YearPeriod,
                    MemberReferenceEntryTypeKey = memberReferenceEntryModel.MemberReferenceEntryTypeKey,
                    LoyaltyPromotionID = memberReferenceEntryModel.PromotionId,
                    QBRPointTypeID = memberReferenceEntryModel.QBRPointTypeID
                });
                context.SaveChanges();
            }
        }

        public List<MemberReferenceEntryModel> GetList(int memberKey)
        {
            List<MemberReferenceEntryModel> memberReferenceModels = new List<MemberReferenceEntryModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                memberReferenceModels = context.Database.SqlQuery<MemberReferenceEntryModel>($"EXEC GetMemberReferenceEntriesOfMember @memberKey = {memberKey}").ToList();
            }
            return memberReferenceModels;
        }

        public List<LoyaltyPromotion> GetLoyaltyPromotionList(int memberKey)
        {
            using (var taxiEpaydbContext = new TaxiEpayEntities())
            {
                var companyKey = taxiEpaydbContext.Members.Find(memberKey).Company_key;
                return taxiEpaydbContext.LoyaltyPromotions.Where(x => x.CompanyKey == companyKey).ToList();
            }
        }

        public bool IsExistPromotion(int promotionId)
        {
            using (var taxiEpaydbContext = new TaxiEpayEntities())
            {
                return taxiEpaydbContext.LoyaltyPromotions.Any(x => x.ID == promotionId);
            }
        }

        public List<MemberReferenceEntryTypeModel> GetEntryTypeList()
        {
            List<MemberReferenceEntryTypeModel> memberReferenceEntryTypeModelList = new List<MemberReferenceEntryTypeModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                foreach (MemberReferenceEntryType t in context.MemberReferenceEntryTypes)
                {
                    memberReferenceEntryTypeModelList.Add(new MemberReferenceEntryTypeModel()
                    {
                        Description = t.Description, 
                        MemberReferenceEntryTypeKey = t.MemberReferenceEntryTypeKey,
                    });
                }
            }
            return memberReferenceEntryTypeModelList;
        }
    }
}
