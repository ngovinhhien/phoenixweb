﻿using Phoenix_Service;
using PhoenixObjects.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class ReportBuildUtilityDA
    {
        public List<MonthlyListModel> GetMonthlyReportList(int MemberKey)
        {
            List<MonthlyListModel> model = new List<MonthlyListModel>();
            //DateTime startDate = DateTime.MinValue;
            //DateTime endDate = DateTime.Now;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var startD = (from m in context.EftDockets
                              where m.Operator_key == MemberKey && m.Fare > 0
                              orderby m.TripStart
                              select m.TripStart).FirstOrDefault();

                var endD = (from m in context.EftDockets
                            where m.Operator_key == MemberKey && m.Fare > 0
                            orderby m.TripStart descending
                            select m.TripStart).FirstOrDefault();
                if (startD != null && endD != null)
                {
                    //DateTime createDate = endD.Value;
                    startD = startD.Value;
                    DateTime recEnddate = startD.Value.AddMonths(12);
                    if (recEnddate > endD)
                    {
                        endD = endD.Value;
                    }
                    else
                    {
                        endD = recEnddate;
                    }   

                    //if (endD.Value.Month.ToString() == DateTime.Now.Month.ToString() && endD.Value.Year.ToString() == DateTime.Now.Year.ToString())
                    //{
                    //    endDate = endDate.AddMonths(-1);
                    //    createDate = new DateTime(endDate.Year, endDate.Month,
                    //                DateTime.DaysInMonth(endDate.Year, endDate.Month));

                    //}
                    while (startD <= endD)
                    {
                        int todayMonth = DateTime.Now.Month;
                        int todayYear = DateTime.Now.Year;

                        MonthlyListModel MonthList = new MonthlyListModel();
                        MonthList.Month = startD.Value.Month.ToString();
                        MonthList.day = "4";
                        MonthList.Year = startD.Value.Year.ToString();
                        MonthList.TranDate = new DateTime(startD.Value.Year, startD.Value.Month, 4);
                        MonthList.Description = " Monthly Statement for - " + startD.Value.Month.ToString() + "/" + startD.Value.Year.ToString();

                        model.Add(MonthList);
                        startD = startD.Value.AddMonths(1);
                    }
                    model = model.OrderByDescending(n => n.TranDate).Take(12).ToList();
                }
            }
            return model;
        }

        public List<WeeklyListModel> GetWeeklyReportList(int MemberKey)
        {
            List<WeeklyListModel> model = new List<WeeklyListModel>();           
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.Now;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var startD = (from m in context.EftDockets
                              where m.Operator_key == MemberKey && m.Fare > 0
                              orderby m.TripStart
                              select m.TripStart).FirstOrDefault();

                var endD = (from m in context.EftDockets
                            where m.Operator_key == MemberKey && m.Fare > 0
                            orderby m.TripStart descending
                            select m.TripStart).FirstOrDefault();
                if (startD != null && endD != null)
                {
                    DateTime createDate = endDate;
                    startDate = startD.Value;

                    DateTime recEnddate = startDate.AddMonths(4);
                    if (recEnddate > endDate)
                    {
                        endDate = endD.Value;
                    }
                    else
                    {
                        endDate = recEnddate;
                    }                  

                    if (endD.Value.Month.ToString() == DateTime.Now.Month.ToString() && endD.Value.Year.ToString() == DateTime.Now.Year.ToString())
                    {
                        endDate = endDate.AddMonths(-1);
                        createDate = new DateTime(endDate.Year, endDate.Month,
                                    DateTime.DaysInMonth(endDate.Year, endDate.Month));

                    }
                    while (startDate <= createDate)
                    {
                        int todayMonth = DateTime.Now.Month;
                        int todayYear = DateTime.Now.Year;

                        if (startDate.DayOfWeek == DayOfWeek.Monday)
                        {
                            WeeklyListModel WeekList = new WeeklyListModel();
                            WeekList.day = startDate.Day.ToString();
                            WeekList.Month = startDate.Month.ToString();
                            WeekList.Year = startDate.Year.ToString();
                            WeekList.TranDate = startDate;
                            WeekList.Description = "Weekly Statement for - " + startDate.Day.ToString() + "/" + startDate.Month.ToString() + "/" + startDate.Year.ToString();

                            model.Add(WeekList);
                        }                       
                        startDate = startDate.AddDays(1);
                    }
                    model = model.OrderByDescending(n => n.TranDate).Take(12).ToList();
                }
            }
            return model;
        }
    }
}
