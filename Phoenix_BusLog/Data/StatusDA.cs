﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class StatusDA
    {

        public List<StatusModel> GetAll()
        {
            List<StatusModel> listOfStatus = new List<StatusModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                listOfStatus = context.Status.Select(m => new StatusModel {
                    ID = m.ID,
                    Name = m.Name,
                    ListOrder = m.ListOrder,
                    Description = m.Description
                }).ToList();
            }
            return listOfStatus;
        }
    }
}
