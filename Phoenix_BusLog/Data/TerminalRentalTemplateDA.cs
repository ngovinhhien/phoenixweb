﻿using Phoenix_Service;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class TerminalRentalTemplateDA
    {
       public List<TerminalRentalTemplateModel> GetByMemberRentalTemplateKey(int MemberRentalTemplateKey)
       {
           List<TerminalRentalTemplateModel> TerminalRentalTemplateList = new List<TerminalRentalTemplateModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var mem = from m in context.TerminalRentalTemplates
                         where m.MemberRentalTemplateKey == MemberRentalTemplateKey
                         select m;
               if (mem != null)
               {
                   foreach (var m in mem)
                   {
                       TerminalRentalTemplateModel terminalRentalTemplate = new TerminalRentalTemplateModel();
                       terminalRentalTemplate.TerminalRentalRate_Key = m.TerminalRentalRate_Key;
                       terminalRentalTemplate.MemberRentalTemplateKey = m.MemberRentalTemplateKey;
                        terminalRentalTemplate.TerminalRentalRate_Key = m.TerminalRentalRate_Key;
                       terminalRentalTemplate.RentalAmount = m.RentalAmount;
                       terminalRentalTemplate.Modified_dttm = m.Modified_dttm;
                       terminalRentalTemplate.ModifiedByUser = m.ModifiedByUser;
                       TerminalRentalTemplateList.Add(terminalRentalTemplate);
                   }
               }
           }
           return TerminalRentalTemplateList;
       }
    }
}
