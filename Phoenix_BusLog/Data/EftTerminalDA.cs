﻿using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class EftTerminalDA
    {

        public EftTerminalModel Get(int id)
        {
            EftTerminalModel tType = new EftTerminalModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var t = (from m in context.EftTerminals
                         where m.EftTerminal_key == id
                         select m).FirstOrDefault();

                if (t != null)
                {
                    tType.TerminalId = t.TerminalId;
                    tType.TerminalType = t.TerminalType;
                    tType.Active = t.Active;
                    tType.Status = t.Status;
                    tType.PPID = t.PPID;
                    tType.SIMSerialNumber = t.SIMSerialNumber;
                    tType.SIMMobileNumber = t.SIMMobileNumber;
                    tType.SoftwareVersion = t.SoftwareVersion;
                    tType.PUK = t.PUK;
                    tType.Configuration = t.Configuration;
                    tType.Modified_dttm = t.Modified_dttm;
                    tType.ModifiedByUser = t.ModifiedByUser;
                    tType.Company_key = t.Company_key;
                    tType.EftTerminal_key = t.EftTerminal_key;
                }
            }
            return tType;
        }

        public void Edit(EftTerminalModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var tType = (from a in context.EftTerminals
                             where a.EftTerminal_key == t.EftTerminal_key
                             select a).FirstOrDefault();

                if (tType != null)
                {
                    tType.TerminalId = t.TerminalId;
                    tType.TerminalType = t.TerminalType;
                    tType.Active = t.Active;
                    tType.Status = t.Status;
                    tType.PPID = t.PPID;
                    tType.SIMSerialNumber = t.SIMSerialNumber;
                    tType.SIMMobileNumber = t.SIMMobileNumber;
                    tType.SoftwareVersion = t.SoftwareVersion;
                    tType.PUK = t.PUK;
                    tType.Configuration = t.Configuration;
                    tType.Modified_dttm = t.Modified_dttm;
                    tType.ModifiedByUser = t.ModifiedByUser;
                    tType.Company_key = t.Company_key;

                }
                context.SaveChanges();
            }
        }

        public int AddNew(EftTerminalModel t)
        {
            int EftTerminal_key = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                EftTerminal tType = new EftTerminal();
                tType.TerminalId = t.TerminalId;
                tType.TerminalType = t.TerminalType;
                tType.Active = t.Active;
                tType.Status = t.Status;
                tType.PPID = t.PPID;
                tType.SIMSerialNumber = t.SIMSerialNumber;
                tType.SIMMobileNumber = t.SIMMobileNumber;
                tType.SoftwareVersion = t.SoftwareVersion;
                tType.PUK = t.PUK;
                tType.Configuration = t.Configuration;
                tType.Modified_dttm = t.Modified_dttm;
                tType.ModifiedByUser = t.ModifiedByUser;
                tType.Company_key = t.Company_key;

                context.EftTerminals.Add(tType);
                context.SaveChanges();
                EftTerminal_key = tType.EftTerminal_key;
            }
            return EftTerminal_key;
        }

        public void AddListOfTerminal(List<EftTerminal> eftTerminals)
        {
            using (var entities = new TaxiEpayEntities())
            {
                entities.EftTerminals.AddRange(eftTerminals);
                entities.SaveChanges();
            }
        }
    }
}

