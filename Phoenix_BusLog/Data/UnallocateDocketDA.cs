﻿using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.Docket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Phoenix_BusLog.Data
{
    public class UnallocateDocketDA : IDisposable
    {
        private TaxiEpayEntities _context;
        public UnallocateDocketDA()
        {
            _context = new TaxiEpayEntities();
        }

        public void GetUnllocateDockets(GridModel<SearchUnallocatedDocketModel, List<UnallocatedDocketModel>> paging)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@DocketBatchId", paging.Request.DocketBatchId ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@TerminalId", paging.Request.TerminalId ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@BatchNumber", paging.Request.BatchNumber ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@RowIDFrom", paging.PageIndexFrom));
            sqlParameters.Add(new SqlParameter("@RowIDTo", paging.PageIndexTo));

            var totalItemsParam = new SqlParameter
            {
                ParameterName = "@TotalItems",
                Value = 0,
                Direction = ParameterDirection.Output
            };

            sqlParameters.Add(totalItemsParam);

            paging.Response = _context.Database.SqlQuery<UnallocatedDocketModel>("EXEC Docket_GetListUnAllocateDockets @DocketBatchId,@TerminalId,@BatchNumber, @RowIDFrom, @RowIDTo, @TotalItems OUT", sqlParameters.ToArray()).ToList();

            int total = 0;
            int.TryParse(totalItemsParam.Value.ToString(), out total);

            paging.TotalItems = total;
        }
        public void GetTerminalAllocations(GridModel<SearchUnallocatedDocketModel, List<TerminalAllcationModel>> paging)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@TerminalId", paging.Request.TerminalId ?? string.Empty));
            sqlParameters.Add(new SqlParameter("@RowIDFrom", paging.PageIndexFrom));
            sqlParameters.Add(new SqlParameter("@RowIDTo", paging.PageIndexTo));

            var totalItemsParam = new SqlParameter
            {
                ParameterName = "@TotalItems",
                Value = 0,
                Direction = ParameterDirection.Output
            };

            sqlParameters.Add(totalItemsParam);

            paging.Response = _context.Database.SqlQuery<TerminalAllcationModel>("EXEC GetTerminalAllcations @TerminalId, @RowIDFrom, @RowIDTo, @TotalItems OUT", sqlParameters.ToArray()).ToList();

            int total = 0;
            int.TryParse(totalItemsParam.Value.ToString(), out total);

            paging.TotalItems = total;
        }
        public string AllocateDocket(AllocateDocketModel model)
        {
            var terminalAllocation = _context.TerminalAllocations.FirstOrDefault(x => x.TerminalAllocation_key == model.TerminalAllocationKey);
            if (terminalAllocation == null)
            {
                return string.Format("Terminal {0} has not been allocatedto any members yet!", terminalAllocation.EftTerminal.TerminalId);
            }
            else if (terminalAllocation.Member_key.GetValueOrDefault(0) == 0 || terminalAllocation.Vehicle_key.GetValueOrDefault(0) == 0)
            {
                return string.Format("Terminal {0} has not been allocated on {1} yet!", terminalAllocation.EftTerminal.TerminalId, terminalAllocation.EffectiveDate.ToString("dd/MM/yyyy"));
            }
            else
            {
                //Check Effective date must be after any Deallocated date
                var checkDeallocatedDate = _context.TerminalAllocations.FirstOrDefault(x => x.TerminalAllocation_key != terminalAllocation.TerminalAllocation_key && x.EftTerminal.TerminalId == terminalAllocation.EftTerminal.TerminalId && !x.IsCurrent && x.DeallocatedDate != null && x.DeallocatedDate > model.EffectiveDate);
                if (checkDeallocatedDate != null)
                {
                    return string.Format("The Effective date {0} must be after The Deallocated date ({1})", model.EffectiveDate.ToString("dd/MM/yyyy HH:mm"), checkDeallocatedDate.DeallocatedDate.Value.ToString("dd/MM/yyyy HH:mm"));
                }
            }

            terminalAllocation.EffectiveDate = model.EffectiveDate;
            _context.SaveChanges();
            return string.Empty;
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }
    }
}
