﻿using Phoenix_Service;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class EftTerminalStatusDA
    {
       public List<EftTerminalStatusModel> GetAll()
       {
           List<EftTerminalStatusModel> model = new List<EftTerminalStatusModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var tt = from m in context.EftTerminalStatus                      
                        select m;

               if (tt != null)
               {
                   foreach(var t in tt)
                   {
                       EftTerminalStatusModel tType = new EftTerminalStatusModel();
                       tType.Id = t.Id;
                       tType.Status = t.Status;
                       model.Add(tType);
                   }
                 
               }
           }
           return model;
       }
    }
}
