﻿using Phoenix_Service;
using PhoenixObjects.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace Phoenix_BusLog.Data
{
    public class CadmusINDA
    {
        public long Add(CadmusINModel response)
        {
            long CadmusId = 0;

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var cad = (from c in con.CadmusINs
                           where c.terminalid == response.terminalid && c.transpan == response.transpan && c.transstart == response.transstart && c.transtotalamount == response.transtotalamount
                           select c).FirstOrDefault();

                if (cad == null)
                {
                    CadmusIN docket = new CadmusIN();
                    docket.batchid = response.batchid;
                    docket.batchnumber = response.batchnumber;
                    docket.batchstatus = response.batchstatus;
                    docket.cardname = response.cardname;
                    docket.driverid = 0;
                    docket.endshift = response.endshift;
                    docket.filedate = response.filedate;
                    docket.loaded = response.loaded;
                    docket.merchantid = response.merchantid;
                    docket.merchantname = response.merchantname;
                    docket.orig_cardname = response.orig_cardname;
                    docket.recordcreated = DateTime.Now;
                    docket.sourcefile = response.sourcefile;
                    docket.startshift = response.startshift;
                    docket.taxiid = response.taxiid;
                    docket.terminalid = response.terminalid;
                    docket.trans_status = response.trans_status;
                    docket.transauthorisationnumber = response.transauthorisationnumber;
                    docket.transdropoffzone = response.transdropoffzone;
                    docket.transend = response.transend;
                    docket.transstart = response.transstart;
                    docket.transextras = response.transextras;
                    docket.transfare = response.transfare;
                    docket.transid1 = response.transid1;
                    docket.transkilometers = response.transkilometers;
                    docket.transpan = response.transpan;
                    docket.transpassengers = response.transpassengers;
                    docket.transpickupzone = response.transpickupzone;
                    docket.transdropoffzone = response.transdropoffzone;
                    docket.transtarrif = response.transtarrif;
                    docket.transtip = response.transtip;
                    docket.transtotalamount = response.transtotalamount;
                    docket.transtype = response.transtype;
                    docket.HostBatchNumber = response.HostBatchNumber;
                    docket.HostTransactionNumber = response.HostTransactionNumber;
                    docket.HostServiceCharge = response.HostServiceCharge;
                    docket.HostGST = response.HostGST;
                    docket.TransactionDescription = response.TransactionDescription;
                    docket.BusinessID = response.BusinessID;
                    docket.UserID = response.UserID;
                    docket.ResponseCode = response.ResponseCode;
                    docket.ResponseText = response.ResponseText;
                    docket.ABN = response.ABN;
                    docket.CardHolderName = response.CardHolderName;
                    docket.CardExpiry = response.CardExpiry;
                    docket.ExternalTransactionId = response.ExternalTransactionId;
                    docket.TransactionPaymentTime = response.TransactionPaymentTime;
                    con.CadmusINs.Add(docket);
                    con.SaveChanges();
                    CadmusId = docket.id;

                    if (CadmusId>0)
                    {
                        BackgroundWorker bgw = new BackgroundWorker();
                        bgw.DoWork += bgw_DoWork;
                        bgw.RunWorkerAsync(CadmusId);
                    }
                    

                }
                return CadmusId;
            }
        }

        public long AddDeclinedTran(CadmusINModel response)
        {
            long CadmusId = 0;

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var cad = (from c in con.CadmusDeclineds
                           where c.terminalid == response.terminalid && c.transpan == response.transpan && c.transstart == response.transstart && c.transtotalamount == response.transtotalamount
                           select c).FirstOrDefault();

                if (cad == null)
                {
                    CadmusDeclined docket = new CadmusDeclined();
                    docket.batchid = response.batchid;
                    docket.batchnumber = response.batchnumber;
                    docket.batchstatus = response.batchstatus;
                    docket.cardname = response.cardname;
                    docket.driverid = 0;
                    docket.endshift = response.endshift;
                    docket.filedate = response.filedate;
                    docket.loaded = response.loaded;
                    docket.merchantid = response.merchantid;
                    docket.merchantname = response.merchantname;
                    docket.recordcreated = DateTime.Now;
                    docket.sourcefile = response.sourcefile;
                    docket.startshift = response.startshift;
                    docket.taxiid = response.taxiid;
                    docket.terminalid = response.terminalid;
                    docket.trans_status = response.trans_status;
                    docket.transauthorisationnumber = response.transauthorisationnumber;
                    docket.transdropoffzone = response.transdropoffzone;
                    docket.transend = response.transend;
                    docket.transstart = response.transstart;
                    docket.transextras = response.transextras;
                    docket.transfare = response.transfare;
                    docket.transid1 = response.transid1;
                    docket.transkilometers = response.transkilometers;
                    docket.transpan = response.transpan;
                    docket.transpassengers = response.transpassengers;
                    docket.transpickupzone = response.transpickupzone;
                    docket.transdropoffzone = response.transdropoffzone;
                    docket.transtarrif = response.transtarrif;
                    docket.transtip = response.transtip;
                    docket.transtotalamount = response.transtotalamount;
                    docket.transtype = response.transtype;
                    docket.HostBatchNumber = response.HostBatchNumber;
                    docket.HostTransactionNumber = response.HostTransactionNumber;
                    docket.HostServiceCharge = response.HostServiceCharge;
                    docket.HostGST = response.HostGST;
                    docket.TransactionDescription = response.TransactionDescription;
                    docket.BusinessID = response.BusinessID;
                    docket.UserID = response.UserID;
                    docket.ResponseCode = response.ResponseCode;
                    docket.ResponseText = response.ResponseText;
                    docket.ABN = response.ABN;
                    docket.CardHolderName = response.CardHolderName;
                    docket.CardExpiry = response.CardExpiry;
                    docket.TransactionPaymentTime = response.TransactionPaymentTime;
                    con.CadmusDeclineds.Add(docket);
                    con.SaveChanges();
                    CadmusId = docket.id;


                }
                return CadmusId;
            }
        }

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            long? CadmusId = (long)e.Argument;
            if (CadmusId.GetValueOrDefault(0) > 0)
            {
                using (TaxiEpayEntities con = new TaxiEpayEntities())
                {
                    var Docketkey = con.f_Docket_ImportDocketFromTranHost(CadmusId, null);
                }
            }
            
        }

        public string GetLastTransactionNumber(string TerminalId, DateTime StartDate)
        {
            string TranNumber = "";
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {

                var result = con.GetNextVirtualTranNumber(TerminalId).FirstOrDefault();
                if (result != null)
                {
                    TranNumber = result;
                }
                //string dd = DateTime.Now.Day.ToString();
                //string mm = DateTime.Now.Month.ToString();
                //string yyyy = DateTime.Now.Year.ToString();
                //string xx = "00";

                //var dt = con.f_Docket_GetLastTransactionNumber(TerminalId, StartDate);
                //if (dt != null)
                //{
                //    int ddt = Convert.ToInt32(dt) + 1;
                //    num = dd + mm + yyyy + xx + ddt.ToString("0000");
                //}
                //else
                //{
                //    int ddt = 0;
                //    num = dd + mm + yyyy + xx + ddt.ToString("0000");
                //}
            }
            return TranNumber;
        }
    }
}
