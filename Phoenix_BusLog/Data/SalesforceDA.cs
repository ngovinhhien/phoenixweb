﻿using Phoenix_Service;
using PhoenixObjects.Salesforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class SalesforceDA
    {

       public void AddCredentials(SalesforceDetailModel credentials)
       {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var cred = (from c in context.SalesforceDetails
                            select c).FirstOrDefault();
                if (cred != null)
                {
                    cred.ClientID = credentials.ClientID;
                    cred.Secret = credentials.Secret;
                    cred.UserName = credentials.UserName;
                    cred.TokenPassword = credentials.TokenPassword;

                    context.SaveChanges();
                }
            }
       }

       public SalesforceDetailModel GetCredentials()
       {
           SalesforceDetailModel credentials = new SalesforceDetailModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var cred = (from c in context.SalesforceDetails
                            select c).FirstOrDefault();
                if (cred != null)
                {
                    credentials.ClientID = cred.ClientID;
                    credentials.Secret = cred.Secret;
                    credentials.UserName = cred.UserName;
                    credentials.TokenPassword = cred.TokenPassword;
                }

            }
           return credentials;

       }
    }
}
