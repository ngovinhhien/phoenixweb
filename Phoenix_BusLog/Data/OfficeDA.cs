﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;

namespace Phoenix_BusLog.Data
{
   public class OfficeDA
    {
       public double GetOfficeCommission(int officekey)
       {
           double commissionRate = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var comm = (from c in context.Offices
                          where c.Office_key == officekey
                        select c).FirstOrDefault();
               if (comm.CommissionRate != null)
               {
                   try
                   {
                       commissionRate = Convert.ToDouble(comm.CommissionRate.Value);
                   }
                   catch { }
               }
           }
           return commissionRate;
       }

       public List<OfficeModel> GetOffices()
       {
           List<OfficeModel> officeList = new List<OfficeModel>();
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var offices = from c in context.Offices
                            select c;
               if (offices != null)
               {
                   foreach(var office in offices)
                   {
                       OfficeModel o = new OfficeModel();
                       o.Name = office.Name;
                       o.Office_key = office.Office_key;
                       officeList.Add(o);
                   }
               }
           }
           return officeList;
       }

       public OfficeModel GetOffice(int officekey)
       {
         OfficeModel o = new OfficeModel();
         using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var offices = (from c in context.Offices
                             where c.Office_key == officekey
                              select c).FirstOrDefault(); ;
               if (offices != null)
               {
                       o.Name = offices.Name;
                       o.Address = offices.Address;
                       o.City = offices.City;
                       o.State = offices.State;
                       o.Postcode = offices.Postcode;
                       o.Telephone = offices.Telephone;
                       o.Fax = offices.Fax;
                       o.Email = offices.Email;
                       o.ContactPerson = offices.ContactPerson;
                       o.Modified_dttm = offices.Modified_dttm;
                       o.ModifiedByUser = offices.ModifiedByUser;
                       o.Company_key = offices.Company_key;
                       o.CommissionRate = offices.CommissionRate;
                       o.OfficeOpeningTime = offices.OfficeOpeningTime;
                       o.OfficeClosingTime = offices.OfficeClosingTime;
                       o.Office_key = offices.Office_key;
               }
           }
           return o;
       }
    }
}
