﻿using Phoenix_Service;
using PhoenixObjects.Member.DriverCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class DriverCardHistoryDA
    {
        public static void SaveHistory(DriverCardHistoryModel model)
        {
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                con.DriverCardHistories.Add(new DriverCardHistory
                {
                    ActionBy = model.ActionBy,
                    ActionDescription = model.ActionDescription,
                    Date = model.Date,
                    Note = model.Note,
                    DriverCardId = model.DriverCardId,
                    MemberKey = model.MemberKey
                });

                con.SaveChanges();
            }
        }
    }
}
