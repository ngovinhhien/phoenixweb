﻿using System.Collections.Generic;
using System.Linq;
using Phoenix_Service;
using PhoenixObjects.PricingPlan;

namespace Phoenix_BusLog.Data
{
    public class PricingPlanDA
    {
        public List<PricingPlanQueryModel> GetPricingPlan(int subBusinessId)
        {
            using (var taxiEpayEntities = new TaxiEpayEntities())
            {
                var sqlQuery = "SELECT p.MSFPLanID, p.MSFPLanName, PC.PayMethod_Key as PayMethodKey, PM.Name AS PayMethodName, PC.IsRateFixed, PC.Rate, PC.TransactionFee"
                                + " FROM MSFPLan P"
                                + " INNER JOIN dbo.MSFPlanConfiguration PC ON PC.MSFPLanID = P.MSFPLanID AND PC.IsActive = 1"
                                + " INNER JOIN dbo.MSFPLanVehicleType PV ON PV.MSFPLanID = P.MSFPLanID AND PV.IsActive = 1"
                                + " INNER JOIN dbo.PayMethod PM ON PM.PayMethod_key = PC.PayMethod_Key"
                                + $" WHERE P.IsActive = 1 AND PV.VehicleType_Key = {subBusinessId} ";
                return taxiEpayEntities.Database.SqlQuery<PricingPlanQueryModel>(sqlQuery).ToList();
            }
        }
    }
}