﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Member;
using PhoenixObjects.Common;
namespace Phoenix_BusLog.Data
{
    public class OperatorDA 
    {
        public OperatorModel Get(int id)
        {
            OperatorModel opt = new OperatorModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Operators
                            where m.Operator_key == id
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    opt.Bond = addr.Bond;
                    opt.BondPaidDate = addr.BondPaidDate;
                    opt.BondReceiptNumber = addr.BondReceiptNumber;
                    opt.TaxiRegistrationNo = addr.TaxiRegistrationNo;
                    opt.AllowCash = addr.AllowCash;
                    opt.PayByEFT = addr.PayByEFT;
                    opt.PayRunGroupId = addr.PayRunGroupId;
                    opt.Dealer_key = addr.Dealer_key;
                    opt.Operator_key = addr.Operator_key;
                    opt.PaymentType = addr.PaymentType;
                }
            }
            return opt;
        }


        public OperatorModel GetByDealerKey(int dealerkey, int operatorkey)
        {
            OperatorModel opt = new OperatorModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = (from m in context.Operators
                            where m.Operator_key == operatorkey && m.Dealer_key == dealerkey
                            select m).FirstOrDefault();

                if (addr != null)
                {
                    opt.Bond = addr.Bond;
                    opt.BondPaidDate = addr.BondPaidDate;
                    opt.BondReceiptNumber = addr.BondReceiptNumber;
                    opt.TaxiRegistrationNo = addr.TaxiRegistrationNo;
                    opt.AllowCash = addr.AllowCash;
                    opt.PayByEFT = addr.PayByEFT;
                    opt.PayRunGroupId = addr.PayRunGroupId;
                    opt.Dealer_key = addr.Dealer_key;
                    opt.Operator_key = addr.Operator_key;

                }
            }
            return opt;
        }
        public int GetOperatorCountDealerKey(int dealerkey)
        {
            int opt = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var addr = context.Dealer_GetActiveOperatorCount(dealerkey).Count();

                if (addr != null)
                {
                  opt =addr;
                }
            }
            return opt;
        }

        public void Add(OperatorModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                 Operator opt = new Operator();
                opt.Bond = t.Bond;
                opt.BondPaidDate = t.BondPaidDate;
                opt.BondReceiptNumber = t.BondReceiptNumber;
                opt.TaxiRegistrationNo = t.TaxiRegistrationNo;
                opt.AllowCash = t.AllowCash;
                opt.PayByEFT = t.PayByEFT;
                opt.PayRunGroupId = t.PayRunGroupId;
                opt.Operator_key = t.Operator_key;
                opt.Dealer_key = t.Dealer_key;
                opt.PaymentType = t.PaymentType;
             
                context.Operators.Add(opt);
                context.SaveChanges();
            }
          
        }

        public void Update(OperatorModel t)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var op = (from m in context.Operators
                            where m.Operator_key == t.Operator_key
                            select m).FirstOrDefault();

                if (op != null)
                {
                    op.Bond = t.Bond;
                    op.BondPaidDate = t.BondPaidDate;
                    op.BondReceiptNumber = t.BondReceiptNumber;
                    op.TaxiRegistrationNo = t.TaxiRegistrationNo;
                    op.AllowCash = t.AllowCash;
                    op.PayByEFT = t.PayByEFT;
                    op.PayRunGroupId = t.PayRunGroupId;
                    op.Dealer_key = t.Dealer_key;
                    op.PaymentType = t.PaymentType;

                    context.SaveChanges();
                }
            }
        }

        public void AddClone(OperatorModel t, int id)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                Operator opt = new Operator();
                opt.Bond = t.Bond;
                opt.BondPaidDate = t.BondPaidDate;
                opt.BondReceiptNumber = t.BondReceiptNumber;
                opt.TaxiRegistrationNo = t.TaxiRegistrationNo;
                opt.AllowCash = t.AllowCash;
                opt.PayByEFT = t.PayByEFT;
                opt.PayRunGroupId = t.PayRunGroupId;
                opt.Operator_key = id;
                opt.Dealer_key = t.Dealer_key;

                context.Operators.Add(opt);
                context.SaveChanges();
            }

        }
      
    }
}
