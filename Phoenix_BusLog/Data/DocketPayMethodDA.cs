﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class DocketPayMethodDA
    {
       public int GetDocketPayMethod_key(int payMethodKey, int vehicleTypeKey, int merchantAccountKey)
       {
           int DocketPayMethod_key = 0;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var sk = (from si in context.DocketPayMethods
                         where si.PayMethod_key == payMethodKey && si.VehicleType_key == vehicleTypeKey && si.MerchantAccount_key == merchantAccountKey
                         select si.DocketPayMethod_key).FirstOrDefault();

               DocketPayMethod_key = sk;
               if(DocketPayMethod_key == 0)
               {
                   throw new Exception("Docket pay method key not found");
               }
           }
           return DocketPayMethod_key;
       }
    }
}
