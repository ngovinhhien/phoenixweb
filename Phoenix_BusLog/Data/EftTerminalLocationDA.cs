﻿using Phoenix_Service;
using PhoenixObjects.Terminal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Exceptions;
using PhoenixObjects.Member;

namespace Phoenix_BusLog.Data
{
    public class EftTerminalLocationDA
    {
        public List<EftTerminalLocationModel> GetAll()
        {
            List<EftTerminalLocationModel> model = new List<EftTerminalLocationModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var tt = from m in context.EftTerminalLocations
                         select m;

                if (tt != null)
                {
                    foreach (var t in tt)
                    {
                        EftTerminalLocationModel tType = new EftTerminalLocationModel();
                        tType.Id = t.Id;
                        tType.Location = t.Location;
                        model.Add(tType);
                    }
                }
            }
            return model;
        }

        public void UpdateTerminal(AllVehicleTerminalAllocationModel model)
        {
            using (var entities = new TaxiEpayEntities())
            {
                var vehicleType = entities.VehicleTypes
                    .Where(v => v.VehicleType_key == model.VehicleTypeKey)
                    .Select(v => new {
                        maximum = v.MaximumTerminalAllocation,
                        name = v.Name
                    })
                    .FirstOrDefault();
                if (!entities.VehicleTypes.Any(n => n.VehicleType_key == model.VehicleTypeKey))
                {
                    throw new ObjectNotFoundException($"The vehicle with type '{model.VehicleTypeKey}' not available.");
                }
                var terminalAllocation = GetCurrenTerminalAllocationByVehicleKey(model.Vehicle_key, entities);

                if (terminalAllocation == null)
                {
                    throw new ObjectNotFoundException($"Terminal allocation not found.");
                }

                if (terminalAllocation.Vehicle == null)
                {
                    throw new ObjectNotFoundException($"The Vehicle doesn't available.");
                }

                var effectedDate = model.EffectiveDate;
                if (effectedDate.GetValueOrDefault(terminalAllocation.EffectiveDate) !=
                    terminalAllocation.EffectiveDate)
                {
                    if (terminalAllocation.EftTerminal_key != null && terminalAllocation.IsCurrent)
                    {

                        //get all terminal allocation with terminal key
                        var allocationList = entities.TerminalAllocations
                            .Where(x => x.EftTerminal_key == terminalAllocation.EftTerminal_key && x.DeallocatedDate != null)
                            .ToList();

                        var nearlyAllocation = allocationList
                            .Where(x => !x.IsCurrent && x.DeallocatedDate < terminalAllocation.EffectiveDate)
                            .OrderByDescending(x => x.DeallocatedDate)
                            .FirstOrDefault();

                        if (nearlyAllocation != null && model.EffectiveDate < nearlyAllocation.DeallocatedDate)
                        {
                            throw new ApplicationException(
                                $"The Effective date {model.EffectiveDate?.ToString("dd/MM/yyyy HH:mm:ss")} " +
                                $"must be larger than {nearlyAllocation.DeallocatedDate?.ToString("dd/MM/yyyy HH:mm:ss")}");
                        }

                    }
                    else
                    {
                        //Deallocate
                        var originalTerminalAllocation = entities.TerminalAllocations.FirstOrDefault(x =>
                            x.TerminalAllocation_key == terminalAllocation.OriginalTerminalAllocation_key);
                        var eftterminalKey = originalTerminalAllocation.EftTerminal_key;

                        //get all terminal allocation with terminal key
                        var allocationList = entities.TerminalAllocations
                            .Where(x => x.EftTerminal_key == eftterminalKey && x.DeallocatedDate != null &&
                                        x.TerminalAllocation_key != terminalAllocation.OriginalTerminalAllocation_key)
                            .ToList();

                        var nearlyAllocation = allocationList
                            .Where(x => !x.IsCurrent && x.DeallocatedDate < terminalAllocation.EffectiveDate)
                            .OrderByDescending(x => x.DeallocatedDate)
                            .FirstOrDefault();

                        if ((nearlyAllocation != null && model.EffectiveDate < nearlyAllocation.DeallocatedDate)
                            || model.EffectiveDate > originalTerminalAllocation.DeallocatedDate)
                        {
                            var msg = string.Empty;
                            if (nearlyAllocation == null)
                            {
                                msg = $"The Effective date {model.EffectiveDate?.ToString("dd/MM/yyyy HH:mm:ss")} " +
                                      $"must be smaller than {originalTerminalAllocation.DeallocatedDate?.ToString("dd/MM/yyyy HH:mm:ss")}";
                            }
                            else
                            {
                                msg =
                                    $"The Effective date {model.EffectiveDate?.ToString("dd/MM/yyyy HH:mm:ss")} must be " +
                                    $"between {nearlyAllocation.DeallocatedDate?.ToString("dd/MM/yyyy HH:mm:ss")} " +
                                    $"and {originalTerminalAllocation.DeallocatedDate?.ToString("dd/MM/yyyy HH:mm:ss")}";
                            }
                            throw new ApplicationException(msg);
                        }
                    }

                    terminalAllocation.EffectiveDate = effectedDate.Value;
                    terminalAllocation.Vehicle.TerminalAllocatedDate = effectedDate.Value;
                }

                terminalAllocation.Vehicle.VehicleId = model.VehicleId;
                terminalAllocation.Vehicle.RegisteredState = model.RegisteredState;
                terminalAllocation.Vehicle.VehicleType_key = model.VehicleTypeKey;

                entities.SaveChanges();
            }
        }

        public int GetTotalTerminalAllocationByVehicleTypeKey(int vehicleTypeKey, int memberKey, TaxiEpayEntities entities = null)
        {
            if (entities == null)
            {
                using (entities = new TaxiEpayEntities())
                {
                    var totalQuery = 
                        (from v in entities.Vehicles
                         join ta in entities.TerminalAllocations on v.Vehicle_key equals ta.Vehicle_key
                         where v.Operator_key == memberKey
                            && v.VehicleType_key == vehicleTypeKey
                            && ta.IsCurrent
                            && ta.EftTerminal_key.HasValue
                         select ta.Vehicle_key);

                    var total = totalQuery.Count();
                    return total;
                }
            }
            else
            {
                var totalQuery =
                        (from v in entities.Vehicles
                         join ta in entities.TerminalAllocations on v.Vehicle_key equals ta.Vehicle_key
                         where v.Operator_key == memberKey
                            && v.VehicleType_key == vehicleTypeKey
                            && ta.IsCurrent
                            && ta.EftTerminal_key.HasValue
                         select ta.Vehicle_key);

                var total = totalQuery.Count();
                return total;
            }
        }

        public TerminalAllocation GetCurrenTerminalAllocationByVehicleKey(int vehicleKey, TaxiEpayEntities entities = null)
        {
            if (entities == null)
            {
                using (entities = new TaxiEpayEntities())
                {
                    var terminalAllocation =
                        entities.TerminalAllocations
                            .Include(n => n.Vehicle)
                            .FirstOrDefault(x => x.Vehicle_key == vehicleKey && x.IsCurrent);

                    return terminalAllocation;
                }
            }
            else
            {
                var terminalAllocation =
                    entities.TerminalAllocations
                        .Include(n => n.Vehicle)
                        .FirstOrDefault(x => x.Vehicle_key == vehicleKey && x.IsCurrent);

                return terminalAllocation;
            }

        }
    }
}
