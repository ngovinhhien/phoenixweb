﻿using Phoenix_Service;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class FreshDeskCredDA
    {
       public FreshDeskCredModel GetUserFreshDeskCred(Guid UserId)
       {
           FreshDeskCredModel model = new FreshDeskCredModel();
           using (aspnetdbEntities context = new aspnetdbEntities())
           {
               var cred = (from k in context.UserFreshDeskCreds
                          where k.UserId == UserId
                          select k).FirstOrDefault();
               if (cred != null)
               {
                   model.Email = cred.Email;
                   model.FD_key = cred.FD_key;
                   model.UserId = cred.UserId;
                   model.Username = cred.Username;
               }
           }
           return model;
       }
       public bool UpdateUserFreshDeskCred(Guid UserId,string Username, string Email)
       {
           FreshDeskCredModel model = new FreshDeskCredModel();
           using (aspnetdbEntities context = new aspnetdbEntities())
           {
               var cred = (from k in context.UserFreshDeskCreds
                           where k.UserId == UserId
                           select k).FirstOrDefault();
               if (cred != null)
               {
                   cred.Email = Email;
                   cred.Username = Username;
                   context.SaveChanges();
                   return true;
               }
              
           }
           return false;

       }
    }
}
