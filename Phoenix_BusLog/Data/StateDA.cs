﻿using Phoenix_Service;
using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
    public class StateDA 
    {

        public List<StateModel> GetState(int s)
        {
            List<StateModel> stateList = new List<StateModel>();
            StateModel state = new StateModel();
            state = GetStateModel(s);
            stateList.Add(state);
            return stateList;
        }


        public List<StateModel> GetStateAustralia()
        {
            List<StateModel> stateList = new List<StateModel>();
            CountryDA cda = new CountryDA();
            int ckey = cda.GetAustraliaKey();

            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = from si in context.States
                         where si.Countrykey == ckey
                          select si;
                if (sk != null)
                {
                    foreach (var s in sk)
                    {
                        StateModel state = new StateModel();
                        state.StateKey = s.State_key;
                        state.StateName = s.StateName;
                        state.FullName = s.StateFullName;

                        stateList.Add(state);
                    }
                }
            }
            return stateList;
        }

      
        public StateModel GetStateModel(int state_key)
        {
            StateModel state = new StateModel();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = (from si in context.States
                          where si.State_key == state_key
                          select si).FirstOrDefault();
                if (sk != null)
                {
                    state.StateKey = sk.State_key;
                    state.StateName = sk.StateName;
                    if (sk.Countrykey.Value == null)
                    {
                        state.Countrykey = 1;
                    }
                    else
                    {
                        state.Countrykey = sk.Countrykey.Value;
                    }
                }
            }
            return state;
        }

        public int GetStateKey(string stateName)
        {
            int statekey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = (from si in context.States
                          where si.StateName == stateName
                         select si.State_key).FirstOrDefault();
               
                    statekey = sk;
            }
            return statekey;
        }

      
        public StateModel Get(int stateKey)
        {
            StateModel state = new StateModel();
            state = GetStateModel(stateKey);
             return state;
        }
    }
}
