﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;

namespace Phoenix_BusLog.Data
{
    public class PayrunDA
    {
        public bool BelongToPayrunGroup(string PayrunID, string SubBusinessType)
        {
            using (var taxiEpay = new TaxiEpayEntities())
            {
                var listPayrunTypes = taxiEpay.Database.SqlQuery<string>($"EXEC usp_CheckPayrunType @PayrunID = '{PayrunID}'");
                if (listPayrunTypes.Count() != 1)
                {
                    return false;
                }

                var subBusinessType = listPayrunTypes.ToList().First();
                if (subBusinessType == SubBusinessType)
                {
                    return true;
                }

                return false;
            }
        }
    }
}
