﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.Common;

namespace Phoenix_BusLog.Data
{
    public class VehicleTypeDA
    {
        public List<VehicleTypeModel> Get()
        {
            List<VehicleTypeModel> businessTypeList = new List<VehicleTypeModel>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = from si in context.VehicleTypes
                         select si;
                if (sk != null)
                {
                    foreach (var skk in sk)
                    {
                        var can = new VehicleTypeModel();
                        can.VehicleType_key = skk.VehicleType_key;
                        can.Name = skk.Name;

                        businessTypeList.Add(can);
                    }
                }

            }
            return businessTypeList;
        }

        public List<VehicleTypeModel> Get(int companyKey)
        {
            List<VehicleTypeModel> businessTypeList = new List<VehicleTypeModel>();

            if (companyKey == 0)
            {
                businessTypeList = Get();
            }
            else
            {
                using (TaxiEpayEntities context = new TaxiEpayEntities())
                {
                    var data = context.VehicleTypes.Where(x => x.Company_key == companyKey).Select(x => new VehicleTypeModel
                    {
                        Name = x.Name,
                        VehicleType_key = x.VehicleType_key
                    }).ToList();

                    if (data != null && data.Count > 0)
                    {
                        businessTypeList.AddRange(data);
                    }
                }
            }
            return businessTypeList;
        }

        public int GetVehicleTypeKey(int Companykey)
        {
            int businessTypekey = 0;
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = (from si in context.VehicleTypes
                          where si.Company_key == Companykey
                          select si).FirstOrDefault();
                if (sk != null)
                {
                    businessTypekey = sk.VehicleType_key;
                }
            }
            return businessTypekey;
        }


        public string GetVehicleTypeByTerminal(string TerminalId)
        {

            string VehicleTypeName = string.Empty;
            TerminalDA tda = new TerminalDA();
            int ter_key = tda.GetTerminalKey(TerminalId);
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var sk = (from si in context.TerminalAllocations
                          where si.EftTerminal_key == ter_key && si.IsCurrent == true
                          select si).FirstOrDefault();
                if (sk != null)
                {
                    var veh = (from si in context.Vehicles
                               where si.Vehicle_key == sk.Vehicle_key.Value
                               select si).FirstOrDefault();

                    if (veh != null)
                    {
                        var vehkey = (from si in context.VehicleTypes
                                      where si.VehicleType_key == veh.VehicleType_key
                                      select si).FirstOrDefault();

                        if (vehkey != null)
                        {
                            VehicleTypeName = vehkey.Name;
                        }
                    }

                }
            }
            return VehicleTypeName;
        }

        public int GetMaximumAllocationByKey(int vehicleTypeKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var maximum = (from si in context.VehicleTypes
                          where si.VehicleType_key == vehicleTypeKey
                          select si.MaximumTerminalAllocation).FirstOrDefault();
                return maximum;
            }
        }

        public string GetNameByKey(int vehicleTypeKey)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var name = (from si in context.VehicleTypes
                               where si.VehicleType_key == vehicleTypeKey
                               select si.Name).FirstOrDefault();
                return name;
            }
        }

        public List<VehicleTypeModel> GetAllVehiclesForAllBusiness(List<int> businessKeys)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var vehicles = (from si in context.VehicleTypes
                            where businessKeys.Contains(si.Company_key.Value)
                            select new VehicleTypeModel {
                                Company_Key = si.Company_key.Value,
                                Name = si.Name,
                                VehicleType_key = si.VehicleType_key
                            }).ToList();
                return vehicles;
            }
        }

        public bool GetAllowAutoCreatePortalAccountByKey(int vehicleTypeKey)
        {
            try
            {
                using (var context = new TaxiEpayEntities())
                {
                    var result = context.Database.SqlQuery<bool>($"SELECT ISNULL(AutoCreatePortalAccount,0) AS AutoCreatePortalAccount FROM VehicleType WHERE VehicleType_Key={vehicleTypeKey}")
                        .FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            
        }
    }
}
