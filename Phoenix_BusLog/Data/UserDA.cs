﻿using PhoenixObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoenix_Service;
using PhoenixObjects.UserAuthentication;
using PhoenixObjects.Administrator;
using Phoenix_Service.LoginDbOld;
using PhoenixObjects.User;
using AutoMapper;
using System.Net;
using CustomerPortalDataAccess.Entities;

namespace Phoenix_BusLog.Data
{
    public class UserDA : IDisposable
    {
        public UserResetPasswordRequestModel GetResetPasswordRequest(Guid userid)
        {
            UserResetPasswordRequestModel userResetPasswordRequestModel = new UserResetPasswordRequestModel();
            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                var user = (from n in cont.UserResetPasswordRequests
                            where n.UserId == userid && n.IsActive == true
                            select n).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserResetPasswordRequest, UserResetPasswordRequestModel>();
                });

                IMapper mapper = config.CreateMapper();
                userResetPasswordRequestModel = mapper.Map<UserResetPasswordRequest, UserResetPasswordRequestModel>(user);
            }
            return userResetPasswordRequestModel;
        }

        public void SaveResetPasswordHistory(string userName, string sourceName, string createdBy)
        {
            using (var dbContext = new aspnetdbEntities())
            {
                var history = new UserResetPasswordHistory
                {
                    UserName = userName,
                    Source = sourceName,
                    CreatedByUser = createdBy,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    ModifiedByUser = createdBy
                };

                dbContext.UserResetPasswordHistories.Add(history);
                dbContext.SaveChanges();
            }
        }

        public bool IsValidResetPasswordRequest(Guid token)
        {
            using (TaxiEpayEntities taxiEpayDBContext = new TaxiEpayEntities())
            {
                return taxiEpayDBContext.UserResetPasswordRequests.Any(x =>
                    x.ResetPasswordToken == token && x.IsActive == true && x.HasCompleted == false);
            }
        }

        public IEnumerable<UserResetPasswordRequestModel> GetResetPasswordRequests()
        {
            IEnumerable<UserResetPasswordRequestModel> userResetPasswordRequestModel = null;
            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                var user = cont.UserResetPasswordRequests.Where(where => where.HasCompleted == false && where.IsActive == true);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserResetPasswordRequest, UserResetPasswordRequestModel>();
                });

                IMapper mapper = config.CreateMapper();
                userResetPasswordRequestModel = mapper.Map<IEnumerable<UserResetPasswordRequest>, IEnumerable<UserResetPasswordRequestModel>>(user);
            }
            return userResetPasswordRequestModel;
        }

        public UserResetPasswordRequestModel SaveResetPasswordRequest(Guid userid, string email, string requestedBy, string createdFromHost)
        {
            UserResetPasswordRequestModel userResetPasswordRequestModel = new UserResetPasswordRequestModel();

            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                //Mark inactive existing requests, if exists
                UserResetPasswordRequest existingUserResetPasswordRequest = cont
                            .UserResetPasswordRequests.Where(where=> where.IsActive && where.UserId == userid).FirstOrDefault();
                if (existingUserResetPasswordRequest != null) { existingUserResetPasswordRequest.IsActive = false; }

                UserResetPasswordRequest userResetPasswordRequest = new UserResetPasswordRequest()
                {
                    CreatedBy = requestedBy,
                    ExpiresBy = DateTime.Now.AddDays(1),
                    CreatedOn = DateTime.Now,
                    IsActive = true,
                    IsDeleted = false,
                    ResetPasswordToken = Guid.NewGuid(),
                    LastEmailUsed = email,
                    UserId = userid,
                    IPAddress = createdFromHost,
                };
                cont.UserResetPasswordRequests.Add(userResetPasswordRequest);
                cont.SaveChanges();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserResetPasswordRequest, UserResetPasswordRequestModel>();
                });

                IMapper mapper = config.CreateMapper();
                userResetPasswordRequestModel = mapper.Map<UserResetPasswordRequest, UserResetPasswordRequestModel>(userResetPasswordRequest);
            }           
            return userResetPasswordRequestModel;
        }

        public void MarkComplete(Guid userid, Guid token)
        {
            UserResetPasswordRequestModel userResetPasswordRequestModel = new UserResetPasswordRequestModel();

            //Mark inactive, if exists
            UserResetPasswordRequestModel existingUserResetPasswordRequestModel = GetResetPasswordRequest(userid);
            if (existingUserResetPasswordRequestModel != null) existingUserResetPasswordRequestModel.IsActive = false;

            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                var user = (from n in cont.UserResetPasswordRequests
                            where n.UserId == userid && n.ResetPasswordToken == token && n.IsActive == true && !n.HasCompleted
                            select n).FirstOrDefault();

                if (user != null)
                {
                    user.HasCompleted = true;
                    user.IsActive = false;
                    cont.SaveChanges();
                }
            }
        }
        public void DeactivateResetPasswordToken(Guid userid, Guid token)
        {
            UserResetPasswordRequestModel userResetPasswordRequestModel = new UserResetPasswordRequestModel();

            //Mark inactive, if exists
            UserResetPasswordRequestModel existingUserResetPasswordRequestModel = GetResetPasswordRequest(userid);
            if (existingUserResetPasswordRequestModel != null) existingUserResetPasswordRequestModel.IsActive = false;

            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                var user = (from n in cont.UserResetPasswordRequests
                            where n.UserId == userid && n.ResetPasswordToken == token && n.IsActive == true && !n.HasCompleted
                            select n).FirstOrDefault();

                user.IsActive = false;
                cont.SaveChanges();
            }
        }


        public UserFreshDeskAuthModel Get(Guid userid)
        {
            UserFreshDeskAuthModel userFreshDeskAuthModel = new UserFreshDeskAuthModel();
            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                var user = (from n in cont.UserFreshDeskAuths
                            where n.UserId == userid && n.IsActive == true
                            select n).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserFreshDeskAuth, UserFreshDeskAuthModel>();
                });

                IMapper mapper = config.CreateMapper();
                userFreshDeskAuthModel = mapper.Map<UserFreshDeskAuth, UserFreshDeskAuthModel>(user);
            }
            return userFreshDeskAuthModel;
        }

        public void Save(UserFreshDeskAuthModel userFreshDeskAuthModel)
        {

            using (TaxiEpayEntities cont = new TaxiEpayEntities())
            {
                UserFreshDeskAuth userFreshDeskAuth = cont.UserFreshDeskAuths.Where(where => where.IsDeleted == false && where.IsActive == true && where.UserId.Equals(userFreshDeskAuthModel.UserId)).FirstOrDefault();
                if (userFreshDeskAuth == null)
                {
                    cont.UserFreshDeskAuths.Add(new UserFreshDeskAuth()
                    {
                        IsActive = true,
                        IsDeleted = false,
                        UserId = userFreshDeskAuthModel.UserId,
                        FreshDeskAPIKey = userFreshDeskAuthModel.FreshDeskAPIKey,
                        CreatedBy = "System",
                        CreatedOn = DateTime.Now,
                        Password = "",
                    });

                }
                else
                {
                    userFreshDeskAuth.FreshDeskAPIKey = userFreshDeskAuthModel.FreshDeskAPIKey;
                    userFreshDeskAuth.UpdatedBy = "System";
                    userFreshDeskAuth.UpdatedOn = DateTime.Now;
                }
                cont.SaveChanges();
            }
            
        }

        public int GetUserOfficeKey(Guid UserId)
        {
            int officekey = GetOfficeKey(UserId);
            return officekey;
        }
        public string GetUserOfficeKeyState(int Officekey)
        {
            OfficeDA oda = new OfficeDA();
            OfficeModel office = new OfficeModel();
            office = oda.GetOffice(Officekey);

            return office.State;
        }
        public void AddUserToRole(Guid UserId, Guid RoleId)
        {
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var user = (from u in context.aspnet_Users
                            where u.UserId == UserId
                            select u).FirstOrDefault();

                var role = (from r in context.aspnet_Roles
                            where r.RoleId == RoleId
                            select r).FirstOrDefault();

                if (user != null && role != null)
                {
                    var key = context.aspnet_UsersInRoles_AddUsersToRoles("Phoenix", user.UserName, role.RoleName, DateTime.Now);
                    context.SaveChanges();
                }
            }
        }

        public int IsUserInRoles(string userName, string Role)
        {
            int role1 = 0;
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var role = context.aspnet_UsersInRoles_FindUsersInRole("Phoenix", "Administrator", userName);

                // var role = context.aspnet_UsersInRoles_IsUserInRole("Phoenix", userName, Role);
                //role1 = role;
            }
            return role1;
        }

        public List<UserProfileModel> GetUsersByRole(Guid RoleId)
        {
            List<UserProfileModel> userProfile = new List<UserProfileModel>();
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var role = (from r in context.aspnet_Roles
                            where r.RoleId == RoleId
                            select r.RoleName).FirstOrDefault();

                var users = context.aspnet_UsersInRoles_GetUsersInRoles("Phoenix", role).ToList();

                if (users != null)
                {
                    foreach (var name in users)
                    {
                        var us = (from r in context.aspnet_Users
                                  where r.UserName == name
                                  select r.UserId).FirstOrDefault();
                        if (us != null)
                        {
                            UserProfileModel uss = new UserProfileModel();
                            uss.UserId = us;
                            uss.UserName = name;

                            userProfile.Add(uss);
                        }
                    }
                }
            }
            return userProfile;
        }







        public List<UserProfileModel> GetUsers()
        {
            List<UserProfileModel> userProfile = new List<UserProfileModel>();
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var users = from u in context.aspnet_Users
                            select u;

                foreach (var u in users)
                {
                    UserProfileModel uProfile = new UserProfileModel();
                    uProfile.UserId = u.UserId;
                    uProfile.UserName = u.UserName;
                    var role = u.aspnet_Roles.ToList();
                    uProfile.Roles = new List<RoleModel>();
                    foreach (var r in role)
                    {
                        RoleModel rm = new RoleModel();
                        rm.RoleId = r.RoleId;
                        rm.RoleName = r.RoleName;
                        uProfile.Roles.Add(rm);

                    }

                    userProfile.Add(uProfile);
                }
            }
            return userProfile;
        }
        //public UserProfileModel GetUserProfile(Guid id)
        //{
        //    UserProfileModel userProfile = new UserProfileModel();
        //    using (aspnetdbEntities context = new aspnetdbEntities())
        //    {
        //        var users = from u in context.aspnet_Users
        //                    where u.UserId == id
        //                    select u;

        //        foreach (var u in users)
        //        {
        //            UserProfileModel uProfile = new UserProfileModel();
        //            uProfile.UserId = u.UserId;
        //            uProfile.UserName = u.UserName;
        //            var role = u.aspnet_Roles.ToList();
        //            uProfile.Roles = new List<RoleModel>();
        //            foreach (var r in role)
        //            {
        //                RoleModel rm = new RoleModel();
        //                rm.RoleId = r.RoleId;
        //                rm.RoleName = r.RoleName;
        //                uProfile.Roles.Add(rm);
        //            }
        //        }
        //    }
        //    return userProfile;
        //}



        private int GetOfficeKey(Guid id)
        {
            int o_key = 0;

            using (aspnetdbEntities context = new aspnetdbEntities())
            {

                var profile = (from u in context.aspnet_Profile
                               where u.UserId == id
                               select u).FirstOrDefault();
                if (profile != null)
                {
                    string[] count = profile.PropertyNames.Split(':');
                    string okey = string.Empty;

                    if (count[0] == "OfficeKey")
                    {
                        int st = Convert.ToInt32(count[2]);
                        int en = Convert.ToInt32(count[3]);
                        okey = profile.PropertyValuesString.Substring(st, en);
                    }
                    else if (count[4] == "OfficeKey")
                    {
                        int st = Convert.ToInt32(count[6]);
                        int en = Convert.ToInt32(count[7]);
                        okey = profile.PropertyValuesString.Substring(st, en);
                    }

                    o_key = Convert.ToInt32(okey);
                }
            }


            return o_key;
        }

        public UserProfileModel GetUsers(Guid id)
        {
            UserProfileModel uProfile = new UserProfileModel();
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var users = (from u in context.aspnet_Users
                             where u.UserId == id
                             select u).FirstOrDefault();

                var profile = (from u in context.aspnet_Profile
                               where u.UserId == id
                               select u).FirstOrDefault();

                if (users != null)
                {
                    uProfile.UserId = users.UserId;
                    uProfile.UserName = users.UserName;
                    uProfile.Dealerkey = profile.Dealerkey;
                    uProfile.LastActivityDate = users.LastActivityDate;
                    //uProfile.SetPropertyValue("LastActivityDate") = 
                    List<string> roles = context.aspnet_UsersInRoles_GetRolesForUser("Phoenix", uProfile.UserName).ToList();
                    if (roles != null)
                    {
                        uProfile.UsersRoleList = roles;
                    }
                    if (profile != null)
                    {
                        using (TaxiEpayEntities con = new TaxiEpayEntities())
                        {
                            int o_key = GetOfficeKey(id);
                            var office = (from u in con.Offices
                                          where u.Office_key == o_key
                                          select u).FirstOrDefault();

                            if (office != null)
                            {
                                if (office.OfficeType_key != null)
                                    uProfile.OfficeType_Key = office.OfficeType_key.Value;
                                if (office.Name != null)
                                    uProfile.Office = office.Name;

                            }
                        }
                    }
                }
            }
            return uProfile;
        }

        public void AddUserOffice(Guid UserId, int OfficeKey)
        {
            using (aspnetdbEntities context = new aspnetdbEntities())
            {
                var key = (from k in context.aspnet_Profile
                           where k.UserId == UserId
                           select k).FirstOrDefault();
                if (key == null)
                {
                    OfficeDA oda = new OfficeDA();
                    OfficeModel office = oda.GetOffice(OfficeKey);
                    if (office != null)
                    {
                        aspnet_Profile userpro = new aspnet_Profile();
                        userpro.UserId = UserId;
                        userpro.PropertyValuesString = office.OfficeType_key.ToString();
                        if (office.OfficeType_key > 1)
                        {
                            userpro.PropertyNames = "OfficeKey:S:0:2:";
                        }
                        else
                        {
                            userpro.PropertyNames = "OfficeKey:S:0:1:";
                        }
                        byte[] Tempb = (from b in context.aspnet_Profile
                                        select b.PropertyValuesBinary).FirstOrDefault();

                        userpro.PropertyValuesBinary = Tempb;
                        userpro.LastUpdatedDate = DateTime.Now;
                        context.aspnet_Profile.Add(userpro);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("office not found");
                    }
                }

            }

        }


        public void AddMemberSystemAccess(string Username, int userId, Guid AppId)
        {
            using (LiveGroupPortalEntities cont = new LiveGroupPortalEntities())
            {
                var user = (from n in cont.lpayMember_mapping
                            where n.UserName == Username && n.AppId == AppId.ToString() && n.UserId == userId
                            select n).FirstOrDefault();

                if (user == null)
                {
                    lpayMember_mapping lpaym = new lpayMember_mapping();
                    lpaym.AppId = AppId.ToString();
                    lpaym.UserId = userId;
                    lpaym.UserName = Username;

                    cont.lpayMember_mapping.Add(lpaym);
                    cont.SaveChanges();
                }
            }
            AddMemberSystemAccessAddToken(Username, userId, AppId);
        }

        public void AddMemberSystemAccessAddToken(string Username, int userId, Guid AppId)
        {
            using (LiveGroupPortalEntities cont = new LiveGroupPortalEntities())
            {
                var user = (from n in cont.esapptokens
                            where n.Username == Username && n.AppId == AppId.ToString() && n.UserId == userId
                            select n).FirstOrDefault();

                if (user == null)
                {
                    esapptoken lpaym = new esapptoken();

                    lpaym.UserId = userId;
                    lpaym.Username = Username;
                    lpaym.CreateDate = DateTime.Now;
                    lpaym.AppId = AppId.ToString();

                    cont.esapptokens.Add(lpaym);
                    cont.SaveChanges();
                }
            }

        }



        public void AddMemberShipNewDatabase(string Username)
        {
            using (LiveGroupPortalEntities cont = new LiveGroupPortalEntities())
            {
                var user = (from n in cont.userprofiles
                            where n.UserName == Username
                            select n).FirstOrDefault();

                if (user != null)
                {
                    List<Customers> phoenixMembers1 = CustomerByEmailAddress(user.EmailAddress);

                    if (phoenixMembers1.Count == 0)
                    {
                        Customers cus = new Customers();
                        cus.CustomerKey = 0;
                        phoenixMembers1.Add(cus);
                    }
                    foreach (Customers phoenixMember in phoenixMembers1)
                    {
                        int ckey = (int)phoenixMember.CustomerKey;
                        var membership = (from n in cont.usermemberships
                                          where n.MemberKey == ckey && n.UserId == user.UserId && n.Inactive == false
                                          select n).FirstOrDefault();
                        if (membership == null)
                        {
                            usermembership memb = new usermembership();
                            memb.UserId = user.UserId;
                            memb.MemberKey = ckey;
                            memb.VerificationToken = Guid.NewGuid().ToString();
                            memb.Verified = true;
                            memb.Inactive = false;
                            memb.IsAdmin = false;
                            memb.UserIdCreatedBy = 1;
                            memb.UserIdUpdatedBy = 1;
                            memb.CreatedDate = DateTime.Now;
                            memb.UpdatedDate = DateTime.Now;
                            cont.usermemberships.Add(memb);
                            user.UserIdSelected = user.UserId;
                            user.UserMembershipKeySelected = memb.UserMembershipKey;
                            cont.SaveChanges();

                            using (LiveGroupPortalEntities contt = new LiveGroupPortalEntities())
                            {
                                var usert = (from n in contt.userprofiles
                                             where n.UserName == Username
                                             select n).FirstOrDefault();

                                usert.UserMembershipKeySelected = memb.UserMembershipKey;
                                contt.SaveChanges();
                            }
                        }
                        else
                        {
                            if (membership.Inactive == true)
                            {
                                membership.Inactive = false;
                                cont.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
        private List<Customers> CustomerByEmailAddress(String EmailAddress)
        {
            List<Customers> CustmerList = new List<Customers>();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var Customers = from customer in context.EFTPOS_Customer
                                where customer.Email.Equals(EmailAddress)
                                select customer;
                try
                {

                    foreach (EFTPOS_Customer Customer in Customers)
                    {
                        Customers cus = new Customers();
                        cus.CustomerName = Customer.FirstName + " " + Customer.LastName;
                        cus.CustomerKey = Customer.Member_key;
                        cus.CustomerId = Customer.MemberId;
                        cus.CompanyType = Customer.Company_key;
                        cus.CustomerType = (int)Customer.CustomerType;

                        cus.ABN = Customer.ABN;
                        cus.ACN = Customer.ACN;
                        cus.Active = (bool)Customer.Active;
                        cus.Address = Customer.Address;
                        cus.City = Customer.City;
                        cus.DriverLicenseNumber = Customer.DriverLicenseNumber;
                        cus.Email = Customer.Email;
                        cus.Fax = Customer.Fax;
                        cus.Mobile = Customer.Mobile;
                        cus.Postcode = Customer.Postcode;
                        cus.State = Customer.State;
                        cus.Telephone = Customer.Telephone;
                        cus.TradingName = Customer.TradingName;
                        cus.Photo = Customer.PhotoID;

                        CustmerList.Add(cus);
                    }
                }
                catch (Exception Ex)
                {

                }
            }

            return CustmerList;
        }



        public void AddUserMemebershipToOldDatabase(string Username, string EmailAddress, Guid uId)
        {
            List<Customers> phoenixMembers1 = CustomerByEmailAddress(EmailAddress);

            foreach (Customers phoenixMember in phoenixMembers1)
            {

                int ckey = (int)phoenixMember.CustomerKey;
                // var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider1"];
                using (LiveEftposEntities context = new LiveEftposEntities())
                {


                    Int64 AccountKey = 0;

                    var Account = (from Acc in context.AccountRs
                                   where Acc.PhoenixMemberKey == ckey
                                   select Acc).FirstOrDefault();
                    if (Account == null)
                    {
                        AccountR AccountServer = new AccountR();

                        AccountServer.PhoenixMemberKey = ckey;
                        AccountServer.AccountCreatedDate = DateTime.Now;
                        AccountServer.UserIdCreatedBy = uId;
                        AccountServer.AccountUpdateddDate = DateTime.Now;
                        AccountServer.UserIdUpdatedBy = uId;
                        context.AccountRs.Add(AccountServer);
                        context.SaveChanges();
                        AccountKey = AccountServer.AccountKey;
                    }
                    else
                    {
                        AccountKey = Account.AccountKey;
                    }
                    var Exists = (from ex in context.AccountUserRs
                                  where ex.Account.AccountKey == AccountKey && ex.aspnet_Users.UserId == uId
                                  select ex).FirstOrDefault();
                    //Add user to account users
                    if (Exists == null)
                    {
                        AccountUserR AccountUserServer = new AccountUserR();

                        AccountUserServer.AccountUserIsTop = true;
                        AccountUserServer.AccountKey = AccountKey;
                        AccountUserServer.UserId = uId;
                        AccountUserServer.AccountUserCreatedDate = DateTime.Now;
                        AccountUserServer.UserIdCreatedBy = uId;
                        AccountUserServer.AccountUserUpdatedDate = DateTime.Now;
                        AccountUserServer.UserIdUpdatedBy = uId;

                        context.AccountUserRs.Add(AccountUserServer);
                        context.SaveChanges();
                    }
                }
            }
        }

        public void Dispose()
        {
            //TOO: Implement
        }
    }
}
