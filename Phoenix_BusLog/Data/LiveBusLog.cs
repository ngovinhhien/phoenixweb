using log4net;

namespace Phoenix_BusLog.Data
{

    public abstract class LiveBusLogic
    {
        public LiveBusLogic() { }
        protected ILog logger;
        public LiveBusLogic(ILog _loggerFactory)
        {
            logger = _loggerFactory;
        }

        protected void LogInfo(string info)
        {
            if (this.logger != null)
                this.logger.Info(info);
        }

        protected void LogError(string error)
        {
            if (this.logger != null)
                this.logger.Error(error);
        }

        protected void LogWarning(string warning)
        {
            if (this.logger != null)
                this.logger.Warn(warning);
        }

    }
}
