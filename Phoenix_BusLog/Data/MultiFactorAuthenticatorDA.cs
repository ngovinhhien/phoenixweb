﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Rewards;
using PhoenixObjects.UserAuthentication;

namespace Phoenix_BusLog.Data
{
    /// <summary>
    /// This class represents Member Refernece Entry to store qantas refernce numbers
    /// </summary>
    public class MultiFactorAuthenticatorDA : IDisposable
    {
        public void Dispose()
        {

        }

        private void CreateMap()
        {

        }

        public MFATypeModel GetActiveMultiFactor()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var source = context.MFATypes.Where(where => where.IsActive == true && where.IsDefault == true).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<MFAType, MFATypeModel>();
                });

                IMapper mapper = config.CreateMapper();
                var mfaTypeModel = mapper.Map<MFAType, MFATypeModel>(source);

                return mfaTypeModel;
            }
        }

        public IEnumerable<UserMFAConfigurationListModel> GetUserMFAConfigurationList()
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var source = context.udv_UserMFAConfigurationList;

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<udv_UserMFAConfigurationList, UserMFAConfigurationListModel>();
                });

                IMapper mapper = config.CreateMapper();
                var mfaTypeModel = mapper.Map<IEnumerable<udv_UserMFAConfigurationList>, IEnumerable<UserMFAConfigurationListModel>>(source);

                return mfaTypeModel;
            }
        }

        public UserMFAConfigurationModel GetUser(Guid userId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var source = context.UserMFAConfigurations.Where(where => where.UserId == userId).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<MFAType, MFATypeModel>();
                    cfg.CreateMap<UserMFAConfiguration, UserMFAConfigurationModel>();
                });

                IMapper mapper = config.CreateMapper();
                var userMFAConfigurations = mapper.Map<UserMFAConfiguration, UserMFAConfigurationModel>(source);

                return userMFAConfigurations;
            }
        }

        public UserMFASetupModel GetUserMFASetup(Guid userId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var source = context.UserMFASetups.Where(where => where.UserId == userId && where.IsActive == true).FirstOrDefault();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserMFASetup, UserMFASetupModel>();
                });

                IMapper mapper = config.CreateMapper();
                var userMFAConfigurations = mapper.Map<UserMFASetup, UserMFASetupModel>(source);

                return userMFAConfigurations;
            }
        }

        public void ToggleUserMFA(Guid userId, bool activate, string createdBy)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                if (!activate)
                {
                    var userMFASetups = context.UserMFASetups.Where(where => where.UserId == userId && where.IsActive == true).FirstOrDefault();
                    if (userMFASetups != null) { userMFASetups.IsActive = false; userMFASetups.IsDeleted = true; }

                    var userMFAConfigurations = context.UserMFAConfigurations.Where(where => where.UserId == userId).FirstOrDefault();
                    context.UserMFAConfigurations.Remove(userMFAConfigurations);

                }
                else
                {
                    context.UserMFAConfigurations.Add(new UserMFAConfiguration()
                    {
                        CreatedBy = createdBy,
                        CreatedOn = DateTime.Now,
                        HasCompleted = false,
                        IsActive = true,
                        IsDeleted = false,
                        IsEnabled = true,
                        MFATypeId = context.MFATypes.Where(where => where.IsActive == true && where.IsDefault == true).FirstOrDefault().MFATypeId,
                        UserId = userId,
                    });
                }
                context.SaveChanges();
            }
        }

        public bool UpsertUserMFASetup(Guid userId, int mfaTypeId, string setupField1, string setupField2, string setupField3)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {            
                var source = context.UserMFASetups.Where(where => where.UserId == userId && where.IsActive == true).FirstOrDefault();

                if (source != null)
                {
                    source.IsActive = false;
                }

                context.UserMFASetups.Add(new UserMFASetup()
                {
                    CreatedBy = "PhoenixWeb",
                    MFATypeId = mfaTypeId,
                    SetupField1 = setupField1,
                    SetupField2 = setupField2,
                    SetupField3 = setupField3,
                    UserId = userId,
                    CreatedOn = DateTime.Now,
                    IsActive = true,

                });
                context.SaveChanges();
                return true;
            }
        }

        public bool SetUserMFASetupComplete(Guid userId)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var source = context.UserMFASetups.Where(where => where.UserId == userId && where.IsActive == true).FirstOrDefault();
                if (source != null)
                {
                    source.IsActive = false;
                    source.UpdatedBy = "PhoenixWeb";
                    source.UpdatedOn = DateTime.Now;                    
                }

                var userMFAConfigurationModel = context.UserMFAConfigurations.Where(where => where.UserId == userId && where.IsActive == true && !where.HasCompleted).FirstOrDefault();
                if (source != null)
                {
                    userMFAConfigurationModel.HasCompleted = true;
                    userMFAConfigurationModel.UpdatedBy = "PhoenixWeb";
                    userMFAConfigurationModel.UpdatedOn = DateTime.Now;
                }
                context.SaveChanges();
                return true;
            }
        }        
    }
}
