﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using EFBulkInsert;
using Phoenix_BusLog.Extension;
using Phoenix_Service;
using Phoenix_Service.Extension;
using Phoenix_BusLog.TransactionHost.Model;
using PhoenixObjects.Common;
using PhoenixObjects.QBR;
using System.Data;

namespace Phoenix_BusLog.Data
{
    public class QantasProcessingDA
    {
        public const string SaleAndFinRejectedStatus = "SF_REJECTED";
        public const string SaleAndFinApprovalStatus = "SF_APPROVAL";
        public const string ManagerRejectedStatus = "MNG_REJECTED";
        public const string ManagerApprovedStatus = "MNG_APPROVAL";
        public const string ManagerApprovedTheRejectedRecordsStatus = "MNG_APPROVAL_REJECTED_RECORDS";

        public const string qantasExportWattingStatus = "Waiting for send to Qantas";
        public const string qantasExportRejectedStatus = "Rejected";

        public const string qantasExportFileEntryWattingStatus = "Waiting for send to Qantas";
        public const string qantasExportFileEntryRejectedStatus = "Rejected by Sales";
        private readonly Dictionary<string, int> QantasExportFileEntryStatusCodes = new Dictionary<string, int>();
        private readonly Dictionary<string, int> QantasExportFileStatusCodes = new Dictionary<string, int>();

        public QantasProcessingDA()
        {
            using (var entities = new TaxiEpayEntities())
            {
                QantasExportFileEntryStatusCodes =
                    entities.QantasExportFileEntryStatus.ToDictionary(n => n.Name,
                        n => n.QantasExportFileEntryStatusId);


                QantasExportFileStatusCodes =
                    entities.QantasExportFileStatus.ToDictionary(n => n.Name,
                        n => n.QantasExportFileStatusId);
            }
        }

        public void PrepareEntryDataToSendQBR(TaxiEpayEntities taxiEpayDbContext, IList<MemberReferenceEntryForManagerApproval> entryForManagerApproval, string currentUser)
        {
            var latestNumber = 0;
            var sequenceNumberWithOrgLoyalty = new Dictionary<int, int>();
            //group "entry for approval" by loyalty Id 
            var loyaltyIdList = entryForManagerApproval.Select(x => x.LoyaltyPromotionID).Distinct().ToList();
            foreach (var loyaltyId in loyaltyIdList)
            {
                int orgLoyaltyId = taxiEpayDbContext.LoyaltyPromotions.Find(loyaltyId).OrganisationforLoyaltyID ?? 0;
                if (sequenceNumberWithOrgLoyalty.Any(x => x.Key == orgLoyaltyId)) //calculate sequence number in case loyalty same organization loyalty but sequence number was calculated before
                {
                    latestNumber = sequenceNumberWithOrgLoyalty[orgLoyaltyId];
                }
                else
                {
                    //get latest file name was sent to Qantas before
                    var latestQantasFileDate =
                        taxiEpayDbContext.QantasExportFiles.Where(x => x.OrganisationforLoyaltyID == orgLoyaltyId).Max(x => x.FileDate);
                    var latestQantasNameList = taxiEpayDbContext.QantasExportFiles
                        .Where(x => x.FileDate == latestQantasFileDate && x.OrganisationforLoyaltyID == orgLoyaltyId).Select(x => x.FileName);
                    if (latestQantasNameList.Any())
                    {
                        var max = 0;
                        foreach (var fileName in latestQantasNameList)
                        {
                            //get next number of "Qantas Export File" base on latest name
                            foreach (var name in fileName?.Split('.'))
                            {
                                if (int.TryParse(name, out var number))
                                {
                                    if (number > max)
                                    {
                                        max = number;
                                    }
                                }
                            }
                        }

                        latestNumber = max;
                    }
                    else
                    {
                        latestNumber = 0;
                    }
                }

                //get identity number for file name from 0 to 999
                var newNumber = (latestNumber + 1) > 999 ? 1 : (latestNumber + 1);
                latestNumber = newNumber;
                sequenceNumberWithOrgLoyalty.Add(orgLoyaltyId, latestNumber);

                var entryStatusWithLoyaltyList = entryForManagerApproval.Where(x => x.LoyaltyPromotionID == loyaltyId
                                                                                    && x.StatusCode != ManagerApprovedStatus
                                                                                    && x.StatusCode != ManagerApprovedTheRejectedRecordsStatus)
                    .Select(x => x.StatusCode).Distinct().ToList();
                //group status of every loyalty
                foreach (var entryStatus in entryStatusWithLoyaltyList)
                {
                    //////////////////////////
                    ////add Qantas Export File
                    var qantasExportFile = new QantasExportFile()
                    {
                        FileDate = DateTime.Now.Date,
                        IsDeleted = false,
                        IsActive = true,
                    };


                    //get OrganisationforLoyalty base on loyalty Id
                    var organisationforLoyalty = taxiEpayDbContext.OrganisationforLoyalties.First(x =>
                        x.OrganisationforLoyaltyId == orgLoyaltyId);
                    //get refix of Qantas export file name. If Entry was rejected by Sale then add "R" before file name
                    var isApproved = entryStatus == SaleAndFinApprovalStatus;
                    var prefixName = isApproved ? "" : "R";
                    //formula of new name
                    var newQantasExportName = prefixName + organisationforLoyalty.PartnerId + "P." + newNumber.ToString("000");

                    //get QantasExportFileStatus base on status of sale/fin
                    //var statusId = (entryStatus == SaleAndFinApprovalStatus)
                    //    ? taxiEpayDbContext.QantasExportFileStatus
                    //        .FirstOrDefault(x => x.Name == qantasExportWattingStatus).QantasExportFileStatusId
                    //    : taxiEpayDbContext.QantasExportFileStatus
                    //        .FirstOrDefault(x => x.Name == qantasExportRejectedStatus).QantasExportFileStatusId;


                    qantasExportFile.QantasExportFileStatusId = isApproved
                        ? QantasExportFileStatusCodes[qantasExportWattingStatus]
                        : QantasExportFileStatusCodes[qantasExportRejectedStatus];
                    qantasExportFile.LoyaltyPromotionID = loyaltyId;
                    qantasExportFile.OrganisationforLoyaltyID = organisationforLoyalty.OrganisationforLoyaltyId;
                    qantasExportFile.FileName = newQantasExportName;

                    taxiEpayDbContext.QantasExportFiles.Add(qantasExportFile);

                    //filter and group "entry for approval" base on every loyalty and status 
                    foreach (var item in entryForManagerApproval.Where(x => x.LoyaltyPromotionID == loyaltyId && x.StatusCode == entryStatus))
                    {
                        item.StatusCode = prefixName != "R"
                            ? ManagerApprovedStatus
                            : ManagerApprovedTheRejectedRecordsStatus;
                        var isApprovedEntry = item.StatusCode == SaleAndFinApprovalStatus;
                        item.ModifiedBy = currentUser;
                        item.ModifiedDate = DateTime.Now;
                        //////////////////////
                        //Add Qantas File Entry
                        var qantasFileEntry = new QantasFileEntry()
                        {
                            Points = item.Point,
                            UploadStatus = "",
                            QBRNumber = item.QBRNumber,
                            IsDeleted = false
                        };
                        //get "export file entry" status base on "entry for approval" status
                        //var exportFileEntryStatusId = item.StatusCode == SaleAndFinApprovalStatus
                        //    ? taxiEpayDbContext.QantasExportFileEntryStatus
                        //        .FirstOrDefault(x => x.Name == qantasExportFileEntryWattingStatus)
                        //        .QantasExportFileEntryStatusId
                        //    : taxiEpayDbContext.QantasExportFileEntryStatus
                        //        .FirstOrDefault(x => x.Name == qantasExportFileEntryRejectedStatus)
                        //        .QantasExportFileEntryStatusId;


                        qantasFileEntry.QantasExportFileEntryStatusId = isApprovedEntry
                            ? QantasExportFileEntryStatusCodes[qantasExportFileEntryWattingStatus]
                            : QantasExportFileEntryStatusCodes[qantasExportFileEntryRejectedStatus];

                        // In case that MemberReferenceEntrySummary has been available before.
                        if (item.MemberReferenceEntrySummary != null)
                        {
                            item.MemberReferenceEntrySummary.MonthPeriod = item.SendMonth;
                            item.MemberReferenceEntrySummary.YearPeriod = item.SendYear;
                            qantasFileEntry.MemberReferenceEntrySummary = item.MemberReferenceEntrySummary;
                        }
                        else
                        {
                            ////////////////////////////////////////
                            //Add Member Reference Entry Summnary
                            var entrySummary = new MemberReferenceEntrySummary()
                            {
                                Points = item.Point,
                                MonthPeriod = item.SendMonth,
                                YearPeriod = item.SendYear,
                                Member_key = item.MemberKey,
                                LoyaltyPromotionID = loyaltyId
                            };
                            //link Entry Summary with Entry for Approval
                            item.MemberReferenceEntrySummary = entrySummary;

                            //link entry summary with member reference entry
                            foreach (var memrefEntry in item.MemberReferenceEntries)
                            {
                                memrefEntry.MemberReferenceEntrySummary = entrySummary;
                            }
                            ///////////////////////////////////////////////

                            taxiEpayDbContext.MemberReferenceEntrySummaries.Add(entrySummary);

                            //link qantas file entry with entry summary
                            qantasFileEntry.MemberReferenceEntrySummary = entrySummary;
                        }

                        /////////////////////////
                        //link qantas export file
                        qantasFileEntry.QantasExportFile = qantasExportFile;
                        /////////////////////////

                        taxiEpayDbContext.QantasFileEntries.Add(qantasFileEntry);
                    }
                }
            }
        }

        public string GetFileType(string contentType)
        {
            var type = String.Empty;
            switch (contentType)
            {
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    type = "EXCEL";
                    break;
                case "text/plain":
                    type = "TEXT";
                    break;
                default:
                    type = "";
                    break;
            }

            return type;
        }

        public bool IsHeaderMatch(List<string> templateColumn, List<string> headerRow)
        {
            headerRow = headerRow.Distinct().ToList();
            //check number of header column match
            if (headerRow.Count != templateColumn.Count)
            {
                return false;
            }

            //check value of header column correct
            foreach (var cellValue in headerRow)
            {
                if (!templateColumn.Any(x => x == cellValue))
                {
                    return  false;
                }
            }

            return true;
        }

        public void HandleSaveQantasPoint(TaxiEpayEntities taxiEpayDbContext, List<QantasPointUploadModel> entryList, int loyaltyId)
        {
            var entryTypeKey = taxiEpayDbContext.MemberReferenceEntryTypes.FirstOrDefault(x =>
                x.Description.Equals("Manual Entry", StringComparison.InvariantCulture))?.MemberReferenceEntryTypeKey;

            var lst = new List<MemberReferenceEntry>();
            var lstMemberIdFromClient = entryList.Select(n => n.MemberId);
            var lstMemberId = taxiEpayDbContext.Members.Where(n => lstMemberIdFromClient.Contains(n.MemberId)).ToDictionary(n => n.MemberId, n => n.Member_key);
            foreach (var entry in entryList)
            {
                var newEntry = new MemberReferenceEntry();
                newEntry.CopyMemberReferenceEntry(entry);
                if (lstMemberId.TryGetValue(entry.MemberId, out var memberKey))
                {
                    newEntry.Member_Key = memberKey;
                }
                newEntry.MemberReferenceEntryTypeKey = entryTypeKey;
                newEntry.LoyaltyPromotionID = loyaltyId;
                lst.Add(newEntry);
            }
            taxiEpayDbContext.BulkInsert(lst.AsEnumerable());
        }

        public QantasPointUploadModel CreateEntryBaseOnRowData(Dictionary<string, string> rowValueWithColumnName, string user, out string error, DateTime importDate)
        {
            error = "";
            var entry = new QantasPointUploadModel()
            {
                EntryDateTime = importDate,
                CreatedByUser = user,
                CreatedDateTime = importDate,
                UpdatedDateTime = importDate,
                UpdatedByUser = user,
            };
            try
            {
                foreach (var rowData in rowValueWithColumnName)
                {
                    if (rowData.Key.Equals("Description", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (String.IsNullOrEmpty(rowData.Value))
                        {
                            throw new Exception("Description is empty.");
                        }
                        entry.Description = rowData.Value.Length > 200 ? rowData.Value.Substring(0, 199) : rowData.Value;
                        continue;
                    }
                    if (rowData.Key.Equals("Point", StringComparison.InvariantCultureIgnoreCase))
                    {
                        entry.Point = int.Parse(rowData.Value);
                        continue;
                    }
                    if (rowData.Key.Equals("Month", StringComparison.InvariantCultureIgnoreCase))
                    {
                        entry.Month = int.Parse(rowData.Value);
                        continue;
                    }
                    if (rowData.Key.Equals("Year", StringComparison.InvariantCultureIgnoreCase))
                    {
                        entry.Year = int.Parse(rowData.Value);
                        continue;
                    }
                    if (rowData.Key.Equals("Member Id", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (String.IsNullOrEmpty(rowData.Value) || rowData.Value.Length > 20)
                        {
                            throw new Exception("MemberId is Empty or longer 20 character.");
                        }
                        entry.MemberId = rowData.Value;
                    }
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }

            return entry;
        }

        public string ValidateImportDataValidWithSystem(List<QantasPointUploadModel> entryList)
        {
            StringBuilder errorList = new StringBuilder();
            using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
            {
                //validate memberkey existed in db
                var memberKeyImports = entryList.Select(x => x.MemberId).Distinct().ToList();
                var memberKeyValids = taxiEpayDbContext.Members.Where(x => memberKeyImports.Contains(x.MemberId))
                    .Select(n => n.MemberId)
                    .ToList();
                if (memberKeyImports.Count() != memberKeyValids.Count)
                {
                    memberKeyImports.RemoveAll(n => memberKeyValids.Contains(n));
                    errorList.Append($"MemberKey [{string.Join(",", memberKeyImports.Select(n => n))}] doesn’t exist.");
                }
            }

            return errorList.ToString();
        }

        public int SaveQantasPointEarnedRate(TaxiEpayEntities taxiEpayDbContext, QBRExchangeRate qbrExchangeRate, string user)
        {
            if (qbrExchangeRate.ID > 0) //edit 
            {
                var orginalObject = taxiEpayDbContext.QBRExchangeRates.Find(qbrExchangeRate.ID);
                orginalObject.CopyQbrExchangeRate(qbrExchangeRate);
                orginalObject.UpdatedDate = DateTime.Now;
                orginalObject.UpdatedBy = user;
            }
            else //add
            {
                if (qbrExchangeRate.ExchangeRateTypeCode == "DEFAULT")
                {
                    int? defaultId = GetDefaultEarnedRateId(taxiEpayDbContext);
                    if (defaultId != null)
                    {
                        DeActiveQantasPointEarnedRate(taxiEpayDbContext, (int)defaultId, user);
                    }
                }

                qbrExchangeRate.CreatedBy = user;
                qbrExchangeRate.UpdatedBy = user;
                qbrExchangeRate.IsActive = true;
                qbrExchangeRate.CampaignEndDate = qbrExchangeRate.CampaignEndDate != null ? qbrExchangeRate.CampaignEndDate.Value.AddDays(1).AddSeconds(-1) : qbrExchangeRate.CampaignEndDate;
                qbrExchangeRate.RegisterEndDate = qbrExchangeRate.RegisterEndDate != null ? qbrExchangeRate.RegisterEndDate.Value.AddDays(1).AddSeconds(-1) : qbrExchangeRate.RegisterEndDate;
                taxiEpayDbContext.QBRExchangeRates.Add(qbrExchangeRate);
            }

            taxiEpayDbContext.SaveChanges();

            return qbrExchangeRate.ID;
        }


        public int? GetDefaultEarnedRateId(TaxiEpayEntities taxiEpayDbContext)
        {
            return taxiEpayDbContext.QBRExchangeRates.FirstOrDefault(x => x.ExchangeRateTypeCode == "DEFAULT" && x.IsActive == true)?.ID;
        }

        public void DeActiveQantasPointEarnedRate(TaxiEpayEntities taxiEpayDbContext, int id, string user)
        {
            var qbrExchangeRate = taxiEpayDbContext.QBRExchangeRates.Find(id);

            qbrExchangeRate.IsActive = false;
            qbrExchangeRate.UpdatedBy = user;
            qbrExchangeRate.UpdatedDate = DateTime.Now;

        }

        public List<View_GetQantasExportFile> GetQantasExportFileListForMonthAndYear(int month, int year)
        {
            using (var entities = new TaxiEpayEntities())
            {
                return entities.View_GetQantasExportFile.Where(n => n.MonthPeriod == month && n.YearPeriod == year).ToList();
            }
        }

        public IEnumerable<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> SortDataListForSaleFin(IEnumerable<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> items, string sortName, string sortOrder)
        {
            IEnumerable<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> itemsSorter;
            switch (sortName)
            {
                case "EntryDate":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.EntryDate);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.EntryDate);
                    }
                    break;
                case "Description":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Description);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Description);
                    }
                    break;
                case "Point":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Point);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Point);
                    }
                    break;
                case "Month":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Month);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Month);
                    }
                    break;
                case "Year":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Year);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Year);
                    }
                    break;
                case "MemberId":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.MemberId);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.MemberId);
                    }
                    break;
                case "TradingName":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.TradingName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.TradingName);
                    }
                    break;
                case "QBRNumber":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.QBRNumber);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.QBRNumber);
                    }
                    break;
                case "CreatedBy":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.CreatedBy);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.CreatedBy);
                    }
                    break;
                case "Status":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Status);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Status);
                    }
                    break;
                case "Comment":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Comment);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Comment);
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.Status);
                    break;
            }
            return itemsSorter;
        }

        public IQueryable<View_GetQBRPointForManager> SortDataListForManager(IQueryable<View_GetQBRPointForManager> items, string sortName, string sortOrder)
        {
            IQueryable<View_GetQBRPointForManager> itemsSorter;
            switch (sortName)
            {
                case "EntryDate":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.EntryDate);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.EntryDate);
                    }
                    break;
                case "Description":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Description);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Description);
                    }
                    break;
                case "Point":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Point);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Point);
                    }
                    break;
                case "Month":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Month);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Month);
                    }
                    break;
                case "Year":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Year);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Year);
                    }
                    break;
                case "MemberId":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.MemberId);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.MemberId);
                    }
                    break;
                case "TradingName":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.TradingName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.TradingName);
                    }
                    break;
                case "QBRNumber":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.QBRNumber);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.QBRNumber);
                    }
                    break;
                case "CreatedBy":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.CreatedBy);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.CreatedBy);
                    }
                    break;
                case "Status":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Status);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Status);
                    }
                    break;
                case "Comment":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Comment);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Comment);
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.Status);
                    break;
            }
            return itemsSorter;
        }

        public IQueryable<View_QantasPointFileReviewingDetail> SortDataListForReviewing(IQueryable<View_QantasPointFileReviewingDetail> items, string sortName, string sortOrder)
        {
            IQueryable<View_QantasPointFileReviewingDetail> itemsSorter;
            switch (sortName)
            {
                case "EntryDateTime":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.EntryDateTime);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.EntryDateTime);
                    }
                    break;
                case "Description":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Description);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Description);
                    }
                    break;
                case "Points":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Points);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Points);
                    }
                    break;
                case "MonthPeriod":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.MonthPeriod);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.MonthPeriod);
                    }
                    break;
                case "YearPeriod":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.YearPeriod);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.YearPeriod);
                    }
                    break;
                case "MemberId":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.MemberId);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.MemberId);
                    }
                    break;
                case "TradingName":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.TradingName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.TradingName);
                    }
                    break;
                case "QBRNumber":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.QBRNumber);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.QBRNumber);
                    }
                    break;
                case "Status":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Status);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Status);
                    }
                    break;
                case "Comment":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Comment);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Comment);
                    }
                    break;
                case "ReviewBySale":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ReviewBySale);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ReviewBySale);
                    }
                    break;
                case "ReviewByManager":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ReviewByManager);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ReviewByManager);
                    }
                    break;
                default:
                    itemsSorter = items.OrderBy(n => n.EntryDateTime);
                    break;
            }
            return itemsSorter;
        }

        public IQueryable<QBRExchangeRate> SortDataListForPointEarnedRate(IQueryable<QBRExchangeRate> items, string sortName, string sortOrder)
        {
            IQueryable<QBRExchangeRate> itemsSorter;
            switch (sortName)
            {
                case "Name":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.Name);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.Name);
                    }
                    break;
                case "IsDefault":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ExchangeRateTypeCode);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ExchangeRateTypeCode);
                    }
                    break;
                case "Type":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.ExchangeRateType.ExchangeRateTypeName);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.ExchangeRateType.ExchangeRateTypeName);
                    }
                    break;
                case "QantasExchangeRate":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => n.QantasExchangeRate);
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => n.QantasExchangeRate);
                    }
                    break;
                case "FromDate":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => new { n.CampaignStartDate, n.RegisterStartDate });
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => new { n.CampaignStartDate, n.RegisterStartDate });
                    }
                    break;
                case "ToDate":
                    if (sortOrder == "asc")
                    {
                        itemsSorter = items.OrderBy(n => new { n.CampaignEndDate, n.RegisterEndDate });
                    }
                    else
                    {
                        itemsSorter = items.OrderByDescending(n => new { n.CampaignEndDate, n.RegisterEndDate });
                    }
                    break;
                default:
                    itemsSorter = items.OrderByDescending(n => n.CreatedDate);
                    break;
            }
            return itemsSorter;
        }

        public PagedResponseModel<QBRPointHistoryResultVM> SearchQBRPointHistory(QBRSearchPointHistoryCriteriaVM model)
        {
            if (!string.IsNullOrEmpty(model.SubmittedFrom))
            {
                model.SubmittedFrom = model.SubmittedFrom.Substring(6) + model.SubmittedFrom.Substring(3, 2) + model.SubmittedFrom.Substring(0, 2);
            }
            if (!string.IsNullOrEmpty(model.SubmittedTo))
            {
                model.SubmittedTo = model.SubmittedTo.Substring(6) + model.SubmittedTo.Substring(3, 2) + model.SubmittedTo.Substring(0, 2);
            }
            if (!string.IsNullOrEmpty(model.PeriodFrom))
            {
                model.PeriodFrom = model.PeriodFrom.Substring(3, 4) + model.PeriodFrom.Substring(0, 2);
            }
            if (!string.IsNullOrEmpty(model.PeriodTo))
            {
                model.PeriodTo = model.PeriodTo.Substring(3, 4) + model.PeriodTo.Substring(0, 2);
            }

            if (model.SortField == "Pointstr")
            {
                model.SortField = "Points";
            }

            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@MemberId", model.MemberID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@QBRNumber", model.QBRNumber ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@CompanyTypeId", model.CompanyKey ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@PromotionId", model.PromotionID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@FileName", model.FileName ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@PeriodFrom", model.PeriodFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@PeriodTo", model.PeriodTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SubmittedFrom", model.SubmittedFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SubmittedTo", model.SubmittedTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@StatusId", model.StatusID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortField", model.SortField ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortOrder", model.SortOrder ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output,
                };
                sqlParameters.Add(totalItemsParam);
                try
                {
                    var response = taxiEpay.Database.SqlQuery<QBRPointHistoryResultVM>(
                        "EXEC SearchQBRPointHistoryWithCriteria "
                    + "  @MemberId"
                    + ", @QBRNumber"
                    + ", @CompanyTypeId"
                    + ", @PromotionId"
                    + ", @FileName"
                    + ", @PeriodFrom"
                    + ", @PeriodTo"
                    + ", @SubmittedFrom"
                    + ", @SubmittedTo"
                    + ", @StatusId"
                    + ", @SortField"
                    + ", @SortOrder"
                    + ", @PageSize"
                    + ", @PageIndex"
                    + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();
                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    return new PagedResponseModel<QBRPointHistoryResultVM>
                    {
                        rows = response,
                        page = model.PageIndex,
                        total = (int)Math.Ceiling((float)total / model.PageSize),
                        records = total,
                    };
                }
                catch (Exception e)
                {
                    return new PagedResponseModel<QBRPointHistoryResultVM>
                    {
                        isError = true
                    };
                }
            }
        }

        public List<QBRFileDetailVM> GetDetailEntryByExportFile(int summaryKey)
        {
            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@MemberReferenceEntrySummaryKey", summaryKey));
                try
                {
                    var response = taxiEpay.Database.SqlQuery<QBRFileDetailVM>(
                        "EXEC SearchExportFileDetails @MemberReferenceEntrySummaryKey"
                        , sqlParameters.ToArray()).ToList();
                    return response;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public List<QBRFileHistoryVM> GetHistoryByExportFile(int summaryKey)
        {
            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@MemberReferenceEntrySummaryKey", summaryKey));
                try
                {
                    var response = taxiEpay.Database.SqlQuery<QBRFileHistoryVM>(
                        "EXEC SearchExportFileHistory @MemberReferenceEntrySummaryKey"
                        , sqlParameters.ToArray()).ToList();
                    return response;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
