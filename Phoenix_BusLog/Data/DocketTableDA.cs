﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Data
{
   public class DocketTableDA
    {
       public bool DoesBatchExists(string BatchId)
       {
           bool BatchExists = false;
           using (TaxiEpayEntities context = new TaxiEpayEntities())
           {
               var t = (from m in context.DocketTables
                        where m.BatchId == BatchId
                        select m).FirstOrDefault();

               if (t != null)
               {
                   BatchExists = true;
               }
           }
           return BatchExists;
       }
    }
}
