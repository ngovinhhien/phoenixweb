﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHostLog.Model
{
    public class LoggerTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public bool? SearchableTerminalID { get; set; }
    }
}
