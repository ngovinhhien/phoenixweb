﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHostLog.Model
{
    public class LogModel
    {
        public int Id { get; set; }
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        [Display(Name = "Thread")]
        [MaxLength(255)]
        public string Thread { get; set; }
        [Display(Name = "Level")]
        [MaxLength(50)]
        public string Level { get; set; }
        public string Logger { get; set; }
        [Display(Name ="Message")]
        [MaxLength(4000)]
        public string Message { get; set; }
        [Display(Name = "Exception")]
        [MaxLength(2000)]
        public string Exception { get; set; }

    }
}
