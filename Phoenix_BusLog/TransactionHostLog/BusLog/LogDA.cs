﻿using Phoenix_BusLog.TransactionHostLog.Model;
using Phoenix_Service;
using PhoenixObjects.Common;
using PhoenixObjects.TransactionHostLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.TransactionHostLog.BusLog
{
    public class LogDA : IDisposable
    {
        public List<string> GetLevel()
        {
            var lst = new List<string>();
            using (var db = new TransactionHostLogEntities1())
            {
                lst = db.LogLevels.Select(x => x.Name).ToList();
            }
            return lst;
        }
        public List<LoggerTypeModel> GetLogger()
        {
            var lst = new List<LoggerTypeModel>();
            using (var db = new TransactionHostLogEntities1())
            {
                lst = db.LoggerTypes.Select(x => new LoggerTypeModel
                {
                    Id = x.Id,
                    Key = x.Key,
                    Name = x.Name,
                    SearchableTerminalID = x.SearchableTerminalID
                }).ToList();
            }
            return lst;
        }
        public void GetWithPaging(GridModel<SearchLogModel, List<LogModel>> paging)
        {
            using (var db = new TransactionHostLogEntities1())
            {
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@Id", paging.Request.Id));

                if (paging.Request.Date.HasValue)
                {
                    sqlParameters.Add(new SqlParameter("@Date", paging.Request.Date.Value));
                }
                else
                {
                    sqlParameters.Add(new SqlParameter("@Date", DBNull.Value));
                }
                if (paging.Request.LoggedDateFrom.HasValue)
                {
                    sqlParameters.Add(new SqlParameter("@DateFrom", paging.Request.LoggedDateFrom.Value));
                }
                else
                {
                    sqlParameters.Add(new SqlParameter("@DateFrom", DBNull.Value));
                }
                if (paging.Request.LoggedDateTo.HasValue)
                {
                    sqlParameters.Add(new SqlParameter("@DateTo", paging.Request.LoggedDateTo.Value));
                }
                else
                {
                    sqlParameters.Add(new SqlParameter("@DateTo", DBNull.Value));
                }
                sqlParameters.Add(new SqlParameter("@Thread", paging.Request.Thread ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@Level", paging.Request.Level ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@Logger", paging.Request.Logger ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@Message", paging.Request.Message ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@Exception", paging.Request.Exception ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@RowIDFrom", paging.PageIndexFrom));
                sqlParameters.Add(new SqlParameter("@RowIDTo", paging.PageIndexTo));
                sqlParameters.Add(new SqlParameter("@TerminalID", paging.Request.TerminalID ?? string.Empty));
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output
                };

                sqlParameters.Add(totalItemsParam);

                paging.Response = db.Database.SqlQuery<LogModel>("EXEC SearchLog @Id, @Date, @DateFrom, @DateTo, @Thread, @Level, @Logger, @Message, @Exception, @TerminalID, @RowIDFrom, @RowIDTo, @TotalItems OUT", sqlParameters.ToArray()).ToList();

                int total = 0;
                int.TryParse(totalItemsParam.Value.ToString(), out total);

                paging.TotalItems = total;
            }
        }

        public void Dispose()
        {

        }
    }
}
