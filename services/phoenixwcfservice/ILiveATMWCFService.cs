﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PhoenixWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILiveATMWCFService" in both code and config file together.
    [ServiceContract]
    public interface ILiveATMWCFService
    {
        [OperationContract]
        bool RegisterATMUser(string TerminalID, string CardTrack, string MobileNumber, string PIN, ref string ErrorReason);

        [OperationContract]
        MemberInfo ValidateCardTrack(string CardTrack, string PIN, string TerminalID = "");

        [OperationContract]
        Withdraw GetWithdrawDockets(int memberKey, decimal withdrawAmount);
        [OperationContract]
        void PayBatches(int memberKey, DateTime _transactionDateTime, List<string> Batches, int OfficeKey, string User,  int memberaccountkey);
        //[OperationContract, Obsolete]
        //void RecordWithdraw(int memberKey, int memberaccountKey, decimal amount, string Description, DateTime TransactionDate);

        [OperationContract]
        int RecordWithdraw(int memberKey, int memberaccountKey, decimal amount, string Description, DateTime TransactionDate, int officeKey, string user);
        [OperationContract]
        MemberInfo LoadMember(int member_key);
        [OperationContract]
        MemberAccountBalance GetMemberAccountEntry(int memberKey);
        [OperationContract]
        bool CheckPulse();
        [OperationContract]
        int RecordCashDispensed(int ReceiptID, decimal amount, string Description, DateTime TransactionDate);
       
    }

    [DataContract]
    public class MemberInfo
    {
        [DataMember]
        public string MemberID
        {
            get; 
            set; 
        }
        [DataMember]
        public string Firstname {get;set;}
        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string TradingName
        {
            get;
            set;
        }

        [DataMember]
        public int MemberKey
        {
            get;
            set;
        }

        [DataMember]
        public decimal Balance
        {
            get;
            set;
        }

        [DataMember]
        public int MemberAccountKey
        {
            get;
            set;
        }
    }

    [DataContract]
    public class Dockets
    {
        [DataMember]
        public string TerminalID { get; set; }
        [DataMember]
        public string BatchNumber { get; set; }
        [DataMember]
        public decimal FaceValue { get; set; }
        [DataMember]
        public decimal Commission { get; set; }
        [DataMember]
        public decimal Payable { get; set; }
        [DataMember]
        public string BatchID { get; set; }
            
    }
    [DataContract]
    public class Withdraw
    {
        public Withdraw()
        {
            DocketList = new List<Dockets>();
        }
        [DataMember]
        public string MemberID { get; set; }
        [DataMember]
        public string TradingName { get;set;}
        [DataMember]
        public decimal AccountBalance { get; set; }
        [DataMember]
        public List<Dockets> DocketList { get; set; }
        [DataMember]
        public decimal TotalPayable { get; set; }
    }

    [DataContract]
    public class AccountEntry
    {
        public AccountEntry()
        {
        }
        [DataMember]
        public DateTime? EntryDateTime { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal? DebitAmount { get; set; }
        [DataMember]
        public decimal? CreditAmount { get; set; }
        [DataMember]
        public decimal? Balance { get; set; }
    }

    [DataContract]
    public class MemberAccountBalance
    {
        public MemberAccountBalance()
        {
            this.AccountEntries = new List<AccountEntry>();
            this.Balance = 0;
        }
        [DataMember]
        public decimal Balance { get; set; }

        [DataMember]
        public List<AccountEntry> AccountEntries { get; set; }
    }
}
