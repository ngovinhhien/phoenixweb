﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWCFService.Models
{
    public class VirtualTerminal
    {
        public string TID { get; set; }
        public string TradingName { get; set; }
        public string ABN { get; set; }
        public string MerchantID { get; set; }
        public string MerchantName { get; set; }
        public int MemberKey { get; set; }
        public bool AllowRefund { get; set; }
        public bool AllowMOTO { get; set; }
    }
}