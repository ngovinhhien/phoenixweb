﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWCFService
{
    public abstract class PayWayOrderFactory
    {
        public abstract PayWayResponse ProcessOrder(PayWayOrder order);


    }

    //public class CaptureTransactionProccessor :PayWayOrderFactory
    //{

    //}
}