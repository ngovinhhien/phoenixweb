﻿//#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.ComponentModel;
using log4net.Config;
using Live.Util.TraceListeners;
using System.Diagnostics;
using Qvalent.PayWay;
using System.Net;
using Live.Util.Common;
using System.ServiceModel;
using System.Reflection;
using System.Text;
using PhoenixWCFService.Models;
namespace PhoenixWCFService
{


    [ServiceBehavior(IncludeExceptionDetailInFaults=true)]
    public class PhoenixWCF : IPhoenixWCFService
    {
        
        private static Log4NetTraceListener log4netListener = null;

        string PayWayUsername;
        string PayWayPassword;
        string LiveCadmusMerchant;
        string LiveMerchantName;

        public PhoenixWCF()
        {
            if (log4netListener == null)
            {
                log4netListener = new Log4NetTraceListener("PhoenixWCF_SERVICE");
                // initialise log4net!
                Trace.Listeners.Add(log4netListener);

                //string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                //string folder = Path.GetDirectoryName(exePath);

                //FileInfo info = new FileInfo(Path.Combine(folder, "log4net.config"));
                FileInfo info = new FileInfo(Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "log4net.config"));

                XmlConfigurator.ConfigureAndWatch(info);
                #if DEBUG
                Trace.TraceInformation("Logging Started");
#endif
            }

            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                GetWBCPayWayUserNamePassword_Result info = entities.GetWBCPayWayUserNamePassword().FirstOrDefault();
                if (info != null)
                {
                    PayWayUsername = info.Username;
                    PayWayPassword = info.Pass;
                    LiveCadmusMerchant = info.MerchantID;
                    LiveMerchantName = info.MerchantName;
                }
            }
        }
        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        //private TaxiEpayEntities entities;

        //protected TaxiEpayEntities MyEntities
        //{
        //    get
        //    {
        //        if (entities == null)
        //        {
        //            entities = new TaxiEpayEntities();
        //        }
        //        return entities;
        //    }
        //}

        //public bool SendSMSReceipt(string mobilenumber, ReceiptPictureToSend ReceiptToSend)
        //{

        //    try
        //    {
        //        if (mobilenumber != "")
        //        {
        //            ReceiptImage receiptImageProcessor = new ReceiptImage();
        //            Image receipt = receiptImageProcessor.CreateReceiptImage(ReceiptToSend);
        //            if (receipt != null)
        //            {
        //                MemoryStream stream = new MemoryStream();
        //                receipt.Save(stream, ImageFormat.Jpeg);
        //                byte[] data = stream.ToArray();
        //                ReceiptPicture pic = new ReceiptPicture();
        //                pic.FileName = ReceiptToSend.TerminalID + "." + ReceiptToSend.Transactionnumber + ".jpg";
        //                pic.FileContent = data;
        //                pic.TerminalID = ReceiptToSend.TerminalID;
        //                pic.MerchantID = ReceiptToSend.MerchantID;
        //                pic.TransactionNumber = ReceiptToSend.Transactionnumber;
        //                pic.CreatedDateTime = DateTime.Now;
        //                pic.CreatedBy = "TransactionHost";
        //                pic.MobileNumberToSend = mobilenumber;
        //                System.Diagnostics.Trace.Write("created picture");
        //                MyEntities.ReceiptPictures.AddObject(pic);
        //                MyEntities.SaveChanges();
        //                System.Diagnostics.Trace.Write("saved picture");
        //                Configuration config = WebConfigurationManager.OpenWebConfiguration(null);
        //                string URL = Settings.Default.ReceiptURL+ pic.ID+" . ";
        //                string message = Settings.Default.SupportMessage;// "For Any Queries Call 1300 883 703";
        //                SMSMessageOut newmessage = new SMSMessageOut();
        //                newmessage.MessageTo = mobilenumber;
        //                newmessage.MessageText = URL + message;
        //                newmessage.Modified_dttm = DateTime.Now;
        //                newmessage.ModifiedByUser = "TransactionHost";
        //                MyEntities.SMSMessageOuts.AddObject(newmessage);
        //                MyEntities.SaveChanges();
        //                return true;
        //            }
        //            else return false;
        //        }
        //        else return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        //System.Diagnostics.Trace.Write(ex.Message);
        //        Trace.TraceError(ex.Message + ". " + ex.InnerException, ex.InnerException, ex.StackTrace);
        //        return false;
        //    }
        //}
        
        /// <summary>
        /// Upload all transactions for the whole batch
        /// </summary>
        /// <param name="Dockets"></param>
        /// <returns></returns>
        public Dictionary<Int64, Int64> UploadTransactions(List<CadmusIN> Dockets)
        {
            Dictionary<Int64, Int64> transactionlist = new Dictionary<long,long>();
            if (Dockets.Count > 0)
            {
                foreach (CadmusIN docket in Dockets)
                {
                    try
                    {
                        Int64 cadmusinID = 0;
                        cadmusinID = UploadTransaction(docket);
                        transactionlist.Add(docket.HostTransactionHistoryKey.GetValueOrDefault(0), cadmusinID);
                        #if DEBUG
                        Trace.TraceInformation(string.Format("Import transactionhistoryKey: {0} to cadmusinID: {1}", docket.HostTransactionHistoryKey.GetValueOrDefault(0), cadmusinID));
                        #endif
                        if (cadmusinID > 0 && docket.trans_status.Contains("Approved")) //only create dockets for approved transactions
                        {
                            //BackgroundWorker bgw = new BackgroundWorker();
                            //bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
                            //bgw.RunWorkerAsync(cadmusinID);
                            try
                            {
                                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                                {
                                    Docket_ImportDocketFromTranhost_Result result = entities.Docket_ImportDocketFromTranhost(cadmusinID).FirstOrDefault(); //try to create docket and pending transaction
                                }
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError(String.Format("Error creating docket for CadmusinID={0}", cadmusinID));
                                Trace.TraceError(ex.Message + ". " + ex.InnerException, ex.InnerException, ex.StackTrace);
                                
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(String.Format("Error uploading transaction: TerminalID={0}, TransID1={1}, Batch={2}", docket.terminalid, docket.transid1, docket.batchnumber));
                        Trace.TraceError(ex.Message + ". " + ex.InnerException, ex.InnerException, ex.StackTrace);
                        foreach (PropertyInfo pi in docket.GetType().GetProperties())
                        {
                            try
                            {
                                Trace.TraceError(String.Format("Property Name: {0}. Property Value: {1}", pi.Name, pi.GetValue(docket, null).ToString()));
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        continue;
                    }
                }
            }
            return transactionlist;
        }



        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            Int64 cadmusinID = 0;
            cadmusinID = (Int64)e.Argument;
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    Docket_ImportDocketFromTranhost_Result result = entities.Docket_ImportDocketFromTranhost(cadmusinID).FirstOrDefault(); //try to create docket and pending transaction
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error creating docket for CadmusinID={0}", cadmusinID));
                Trace.TraceError(ex.Message + ". " + ex.InnerException, ex.InnerException, ex.StackTrace);
            }
        }



        /// <summary>
        /// Check if Cadmusin transaction exist. If yes, return the cadmusin ID, if not, add new record and return the new id
        /// </summary>
        /// <param name="Docket"></param>
        /// <returns></returns>
        long CheckCadmusinExistence(CadmusIN Docket)
        {
            long cadid = 0;
            using (TaxiEpayEntities myEntities = new TaxiEpayEntities())
            {
                System.Data.Objects.ObjectParameter cid = new System.Data.Objects.ObjectParameter("CadmusID", typeof(long));
                myEntities.GetCadmusID(Docket.trans_status.Contains("Approved"), Docket.terminalid, Docket.HostTransactionHistoryKey, Docket.HostBatchNumber, cid);
                try
                {
                    if (cid.Value != System.DBNull.Value)
                    {
                        cadid = Convert.ToInt64(cid.Value);
                    }
                    else
                        cadid = 0;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(string.Format("Error checking existence of TerminalID:{0}, TranhistoryKey:{1}, BatchNumber:{2}. Error Message: {3}", Docket.terminalid, Docket.HostTransactionHistoryKey, Docket.HostBatchNumber, ex.Message));
                    cadid = 0;
                }
            }
            return cadid;
        }
        /// <summary>
        /// Upload individual cadmusin docket
        /// </summary>
        /// <param name="Docket"></param>
        /// <returns></returns>
        public Int64 UploadTransaction(CadmusIN Docket)
        {
            Int64 cadid = 0;
            cadid = CheckCadmusinExistence(Docket);
            if (cadid == 0)
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    if (Docket.trans_status.Contains("Approved")) //add Approved transaction to CadmusIN table
                    {

                        CadmusIN newdocket = new CadmusIN();

                        newdocket = Docket;
                        entities.CadmusINs.AddObject(newdocket);
                        entities.SaveChanges();
                        cadid = newdocket.id;
                        //Docket_ImportDocketFromTranhost_Result result = MyEntities.Docket_ImportDocketFromTranhost(cadid).FirstOrDefault(); //try to create docket and pending transaction
                        //if (result != null)
                        //{
                        //    docketkey = result.DocketKey.GetValueOrDefault(0);
                        //}
                    }
                    else //Add other transaction to CadmusDeclined table.
                    {
                        CadmusDeclined declined = new CadmusDeclined();
                        declined.batchid = Docket.batchid;
                        declined.batchnumber = Docket.batchnumber;
                        declined.batchstatus = Docket.batchstatus;
                        declined.cardname = Docket.cardname;
                        declined.driverid = Docket.driverid;
                        declined.endshift = Docket.endshift;
                        declined.filedate = Docket.filedate;
                        declined.loaded = Docket.loaded;
                        declined.merchantid = Docket.merchantid;
                        declined.merchantname = Docket.merchantname;
                        declined.recordcreated = Docket.recordcreated;
                        declined.sourcefile = Docket.sourcefile;
                        declined.startshift = Docket.startshift;
                        declined.taxiid = Docket.taxiid;
                        declined.terminalid = Docket.terminalid;
                        declined.trans_status = Docket.trans_status;
                        declined.transauthorisationnumber = Docket.transauthorisationnumber;
                        declined.transdropoffzone = Docket.transdropoffzone;
                        declined.transend = Docket.transend;
                        declined.transextras = Docket.transextras;
                        declined.transfare = Docket.transfare;
                        declined.transid1 = Docket.transid1;
                        declined.transkilometers = Docket.transkilometers;
                        declined.transpan = Docket.transpan;
                        declined.transpassengers = Docket.transpassengers;
                        declined.transpickupzone = Docket.transpickupzone;
                        declined.transstart = Docket.transstart;
                        declined.transtarrif = Docket.transtarrif;
                        declined.transtip = Docket.transtip;
                        declined.transtotalamount = Docket.transtotalamount;
                        declined.transtype = Docket.transtype;
                        declined.HostGST = Docket.HostGST;
                        declined.HostServiceCharge = Docket.HostServiceCharge;
                        declined.HostBatchNumber = Docket.HostBatchNumber;
                        declined.HostTransactionHistoryKey = Docket.HostTransactionHistoryKey;
                        declined.TransactionDescription = Docket.TransactionDescription;
                        declined.UserID = Docket.UserID;
                        declined.BusinessID = Docket.BusinessID;
                        declined.HostTransactionNumber = Docket.HostTransactionNumber;
                        declined.ResponseCode = Docket.ResponseCode;
                        declined.ResponseText = Docket.ResponseText;
                        declined.ABN = Docket.ABN;
                        declined.CardEntryMode = Docket.CardEntryMode;
                        declined.CardHolderName = Docket.CardHolderName;
                        declined.CellTowerID = Docket.CellTowerID;
                        declined.CellTowerLAC = Docket.CellTowerLAC;
                        declined.CellTowerMCC = Docket.CellTowerMCC;
                        declined.CellTowerMNC = Docket.CellTowerMNC;
                        entities.CadmusDeclineds.AddObject(declined);
                        entities.SaveChanges();
                        cadid = declined.id;
                    }
                }
            }
            int docketkey = 0;
            

            return cadid;
        }

        public string GetCurrentCustomerPaymentType(string TerminalID)
        {
            string PaymentType = "B";
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    PaymentType = entities.GetCurrentMemberPaymentType(TerminalID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Error getting customer payment type for TerminalID :{0}. ErrorMessage: {1}", TerminalID, ex.Message));
            }
            return PaymentType;

        }

        public bool VerifyPhoenixLogin(string username, string password)
        {
            return true;
        }
        string GetVirtualTerminalID(int MemberKey)
        {
            string TID = "";
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                GetCurrentVirtualTIDByMemberKey_Result result = entities.GetCurrentVirtualTIDByMemberKey(MemberKey).FirstOrDefault();
                if (result != null)
                {
                    TID = result.TerminalID;
                }
            }
            #if DEBUG
            return "TEST"; 
            #endif
            return TID;
        }

        string GetVirtualTerminalID(string LEUsername, ref string TradingName, ref string ABN, ref string MerchantID, ref string MerchantName, ref int MemberKey, ref string APIKey )
        {
            string TID = "";
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                Terminal_GetCurrentVirtualTIDByUsername_Result result = entities.GetCurrentVirtualTIDByUsername(LEUsername).FirstOrDefault();
                if (result != null)
                {
                    TID = result.TerminalId;
                    TradingName = result.TradingName;
                    ABN = result.ABN;
                    MerchantID = result.MerchantID;
                    MerchantName = result.MerchantName;
                    MemberKey = result.PhoenixMemberKey;
                    APIKey = result.APIKey;
                }
            }
//#if DEBUG

//            TID = "99999999";
//            TradingName = "Test Merchant";
//            ABN = "99999999";
//            MerchantID = "TEST";
//            MerchantName = "Test Merchant";
//            MemberKey = 16291;

//            return TID;
//#endif
            return TID;
        }

        VirtualTerminal GetVirtualTerminalID(string LEUsername)
        {
            string TID = "";
            VirtualTerminal terminal = null;
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                Terminal_GetCurrentVirtualTIDByUsername_Result result = entities.GetCurrentVirtualTIDByUsername(LEUsername).FirstOrDefault();
                if (result != null)
                {
                    terminal = new VirtualTerminal();
                    terminal.TID = result.TerminalId;
                    terminal.TradingName = result.TradingName;
                    terminal.ABN = result.ABN;
                    terminal.MerchantID = result.MerchantID;
                    terminal.MerchantName = result.MerchantName;
                    terminal.MemberKey = result.PhoenixMemberKey;
                    terminal.AllowMOTO = result.AllowMOTO;
                    terminal.AllowRefund = result.allowRefund;
                }
            }
#if DEBUG

            terminal = new VirtualTerminal();
            terminal.TID = "99999999";
            terminal.TradingName = "Test Merchant";
            terminal.ABN = "99999999";
            terminal.MerchantID = "TEST";
            terminal.MerchantName = "Test Merchant";
            terminal.MemberKey = 16291;
            terminal.AllowMOTO = true;
            terminal.AllowRefund = true;

            return terminal;
#endif
            return terminal;
        }
        string GetNextVirtualTransactionNumber(string TerminalID)
        {
            string TranNumber = "";
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                GetNextVirtualTranNumber_Result result = entities.GetNextVirtualTranNumber(TerminalID).FirstOrDefault();
                if (result != null)
                {
                    TranNumber = result.NextTranNumber;
                }
            }
            //#if DEBUG
            //return "2510201301033";
            //#endif
            return TranNumber;
        }

        string UpdateReferenceNumber(string username, string ExternalReferenceNumber, string CardMasked, short ExpiryMonth, short ExpiryYear, string CardHolderName )
        {
            string refnumber = "";
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {

                UpdateCustomerReferenceNumber_Result result = null;
                result = entities.UpdateCustomerReferenceNumber(username, ExternalReferenceNumber, CardMasked, ExpiryMonth, ExpiryYear,CardHolderName).FirstOrDefault();
                if (result != null)
                {
                    refnumber = result.CustomerReferenceNumber;
                }
            }

            return refnumber;
        }
        public MOTOTranResponse RefundTransaction(MOTOTran TranToProcess)
        {
            //string terminalID = "";
            //string TradingName = "";
            //string ABN = "";
            //string MerchantID = "";
            //string MerchantName = "";
            decimal orderamount = Math.Abs( TranToProcess.Amount + TranToProcess.Surcharge + TranToProcess.GSTOnSurcharge);
            MOTOTranResponse resp = new MOTOTranResponse();
            resp.Description = TranToProcess.Description;
            resp.Amount = -TranToProcess.Amount;
            resp.Surcharge = -TranToProcess.Surcharge;
            resp.GSTOnSurcharge = -TranToProcess.GSTOnSurcharge;
            resp.UserName = TranToProcess.username;
            VirtualTerminal terminal = null;
            try
            {
                //VirtualTerminal terminal = null;
                
                if (String.IsNullOrEmpty(TranToProcess.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {
                    terminal = GetVirtualTerminalID(TranToProcess.username);
                    //int member_key=0;
                    
                    
                    //terminalID = GetVirtualTerminalID(TranToProcess.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key);
                    
                    if (terminal == null || string.IsNullOrEmpty(terminal.TID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else if (!terminal.AllowRefund)
                    {
                        throw new FunctionNotAvailableException();
                    }
                    else
                    {

                        resp.TradingName = terminal.TradingName;
                        resp.ABN = terminal.ABN;
                        resp.MerchantID = terminal.MerchantID;
                        resp.MerchantName = terminal.MerchantName;
                        resp.TerminalID = terminal.TID;
                    }
                

                string CardMasked = "";

                //with refund card details can be blank, the original card information will be used by Payway. However to record the card number in PHoenix, we force the client to pass the Card Mask.
                if (!String.IsNullOrEmpty(TranToProcess.cardNumber))
                {
                    if (TranToProcess.cardNumber.Length >= 6)
                        CardMasked = TranToProcess.cardNumber.Substring(0, 6) + "******" + TranToProcess.cardNumber.Remove(0, TranToProcess.cardNumber.Length - 4);
                    else
                        CardMasked = TranToProcess.cardNumber + "******";
                    resp.CardMasked = CardMasked;
                }
                else
                    throw new InvalidCardInformationException();
   
                if (orderamount <= 0)
                {
                    throw new InvalidAmountInformationException();
                }

             

                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = "";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);


                string TransactionNumber = GetNextVirtualTransactionNumber(terminal.TID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", terminal.MerchantID);
                requestParameters.Add("order.type", "refund");
                //requestParameters.Add("card.PAN", TranToProcess.cardNumber);
                //requestParameters.Add("card.CVN", TranToProcess.cardVerificationNumber);
                //requestParameters.Add("card.expiryYear", TranToProcess.cardExpiryYear.ToString());
                //requestParameters.Add("card.expiryMonth", TranToProcess.cardExpiryMonth.ToString());
                requestParameters.Add("order.amount", Math.Truncate(orderamount * 100).ToString());
                requestParameters.Add("customer.orderNumber", TransactionNumber);
                requestParameters.Add("card.currency", TranToProcess.card_currency);
                requestParameters.Add("order.ECI", "SSL");
                requestParameters.Add("customer.originalOrderNumber", TranToProcess.OriginalOrderNumber);
                requestParameters.Add("customer.customerReferenceNumber", terminal.TID);
                //requestParameters.Add("card.cardHolderName", TranToProcess.cardholdername);
                resp.OrderNumber = TransactionNumber;
#if DEBUG
                Trace.TraceInformation(string.Format("Refund transaction. OriginalTransactionNumber: {0}. New TransactionNumber: {1}.", TranToProcess.OriginalOrderNumber, TransactionNumber));
#endif
                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);


                string requestText = payWayAPI.FormatRequestParameters(requestParameters);
#if DEBUG
                Trace.TraceInformation(requestText);
#endif

                bool reversalapproved = false;
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                List<string> responseparametersList = new List<string>();
                int retries = 0;
                while (!reversalapproved && retries < 10)
                {
                    if (retries != 0)
                        Thread.Sleep(60000); //wait 60 seconds to retry
                    string responseText = payWayAPI.ProcessCreditCard(requestText);

#if DEBUG
                    Trace.TraceInformation(responseText);
#endif
                    responseparameters = new Dictionary<string, string>();
                    responseparametersList = responseText.Split('&').ToList();

                    foreach (string s in responseparametersList)
                    {
                        string field = s.Substring(0, s.IndexOf('='));
                        string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                        responseparameters.Add(field, value);
                    }
                    if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                    {
                        if (responseparameters["response.summaryCode"] == "2" ||
                            responseparameters["response.responseCode"] == "QE" ||
                            responseparameters["response.responseCode"] == "QI" ||
                            responseparameters["response.responseCode"] == "QX")
                        {
                            reversalapproved = false;
                        }
                        else
                            reversalapproved = true;
                    }
                    retries++;
                }
                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {

                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                    resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                    resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";

                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }

                //Write into phoenix database
                BackgroundWorker bgwInsertTransaction = new BackgroundWorker();
                bgwInsertTransaction.DoWork += new DoWorkEventHandler(bgwInsertTransaction_DoWork);
                bgwInsertTransaction.RunWorkerAsync(resp);
                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Username: {0}. Error message: {1}", TranToProcess.username, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }

            }
            finally
            {

            }
            return resp;
        }
        public MOTOTranResponse ReverseTransaction(MOTOTran TranToProcess)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            decimal orderamount = TranToProcess.Amount + TranToProcess.Surcharge + TranToProcess.GSTOnSurcharge;
            MOTOTranResponse resp = new MOTOTranResponse();
            resp.Description = TranToProcess.Description;
            resp.Amount = TranToProcess.Amount;
            resp.Surcharge = TranToProcess.Surcharge;
            resp.GSTOnSurcharge = TranToProcess.GSTOnSurcharge;

            try
            {
                if (String.IsNullOrEmpty(TranToProcess.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {
                    int member_key = 0;
                    string APIKey = "";
                    terminalID = GetVirtualTerminalID(TranToProcess.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key, ref APIKey);
                    if (string.IsNullOrEmpty(terminalID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {

                        resp.TradingName = TradingName;
                        resp.ABN = ABN;
                        resp.MerchantID = MerchantID;
                        resp.MerchantName = MerchantName;
                        resp.TerminalID = terminalID;
                    }
                }

                string CardMasked = "";
                //if (String.IsNullOrEmpty(TranToProcess.cardNumber))
                //{
                //    throw new InvalidCardInformationException();
                //}
                if (!String.IsNullOrEmpty(TranToProcess.cardNumber))
                {
                    if (TranToProcess.cardNumber.Length >= 6)
                        CardMasked = TranToProcess.cardNumber.Substring(0, 6) + "******" + TranToProcess.cardNumber.Remove(0, TranToProcess.cardNumber.Length - 4);
                    else
                        CardMasked = TranToProcess.cardNumber + "******";
                    resp.CardMasked = CardMasked;
                }
                else
                    throw new InvalidCardInformationException();

                if (String.IsNullOrEmpty(TranToProcess.cardVerificationNumber))
                {
                    throw new InvalidCVNInformationException();
                }

                if (String.IsNullOrEmpty(TranToProcess.card_currency))
                {
                    throw new InvalidCurrencyInformationException();
                }
                if (orderamount <= 0)
                {
                    throw new InvalidAmountInformationException();
                }
                if (TranToProcess.cardExpiryMonth <= 0 || TranToProcess.cardExpiryMonth > 12 || TranToProcess.cardExpiryYear <= 0)
                {
                    throw new InvalidExpiryDateException();
                }
                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                    @"logDirectory=LOG_DIR";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);


                string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", MerchantID);
                requestParameters.Add("order.type", "reversal");
                requestParameters.Add("card.PAN", TranToProcess.cardNumber);
                requestParameters.Add("card.CVN", TranToProcess.cardVerificationNumber);
                requestParameters.Add("card.expiryYear", TranToProcess.cardExpiryYear.ToString());
                requestParameters.Add("card.expiryMonth", TranToProcess.cardExpiryMonth.ToString());
                requestParameters.Add("order.amount", Math.Truncate(orderamount * 100).ToString());
                requestParameters.Add("customer.orderNumber", TransactionNumber);
                requestParameters.Add("card.currency", TranToProcess.card_currency);
                requestParameters.Add("order.ECI", "SSL");
                requestParameters.Add("customer.originalOrderNumber", TranToProcess.OriginalOrderNumber);
                requestParameters.Add("customer.customerReferenceNumber", terminalID);
                requestParameters.Add("card.cardHolderName", TranToProcess.cardholdername);
                #if DEBUG
                Trace.TraceInformation(string.Format("Reverse transaction. OriginalTransactionNumber: {0}. New TransactionNumber: {1}.", TranToProcess.OriginalOrderNumber, TransactionNumber));
#endif
                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                      
                string requestText = payWayAPI.FormatRequestParameters(requestParameters);
                #if DEBUG
                Trace.TraceInformation(requestText);
#endif

                bool reversalapproved = false;
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                List<string> responseparametersList = new List<string>();
                int retries = 0;
                while (!reversalapproved && retries<10)
                {
                    if (retries!=0)
                        Thread.Sleep(60000); //wait 60 seconds to retry
                    string responseText = payWayAPI.ProcessCreditCard(requestText);

                    #if DEBUG
                    Trace.TraceInformation(responseText);
#endif
                    responseparameters = new Dictionary<string, string>();
                    responseparametersList = responseText.Split('&').ToList();
                    
                    foreach (string s in responseparametersList)
                    {
                        string field = s.Substring(0, s.IndexOf('='));
                        string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                        responseparameters.Add(field, value);
                    }
                    if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode") )
                    {
                        if (responseparameters["response.summaryCode"] == "2" ||
                            responseparameters["response.responseCode"] == "QE" ||
                            responseparameters["response.responseCode"] == "QI" ||
                            responseparameters["response.responseCode"] == "QX")
                        {
                            reversalapproved = false;
                        }
                        else
                            reversalapproved = true;
                    }
                    retries++;
                }
                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {

                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                    resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                    resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";

                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }
                
                
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }
                
            }
            finally
            {

            }
            return resp;
        }

        /// <summary>
        /// This method is used to process a MOTO transaction
        /// </summary>
        /// <param name="TranToProcess"></param>
        /// <returns></returns>
        public MOTOTranResponse ProcessTransaction(MOTOTran TranToProcess)
        {
            return PaywayProcessTransaction(TranToProcess, false);
        }

        public MOTOTranResponse ProcessTransactionEncrypted(MOTOTran TranToProcess)
        {
            return PaywayProcessTransaction(TranToProcess, true);
        }
        public MOTOTranResponse PaywayProcessTransaction(MOTOTran TranToProcess, bool encrypted)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            decimal orderamount = TranToProcess.Amount + TranToProcess.Surcharge + TranToProcess.GSTOnSurcharge;
            MOTOTranResponse resp = new MOTOTranResponse();
            resp.Description = TranToProcess.Description;
            resp.Amount = TranToProcess.Amount;
            resp.Surcharge = TranToProcess.Surcharge;
            resp.GSTOnSurcharge = TranToProcess.GSTOnSurcharge;
            resp.CardHolderName = TranToProcess.cardholdername;
            string CardMasked = "";
            resp.UserName = TranToProcess.username;
            int memberkey = 0;
            try
            {
                
                if (String.IsNullOrEmpty(TranToProcess.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {
                    string APIKey = "";

                    terminalID = GetVirtualTerminalID(TranToProcess.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref memberkey, ref APIKey);

                    if (encrypted)
                    {
                        string CardNumber = TranToProcess.cardNumber;
                        if (string.IsNullOrWhiteSpace(APIKey))
                        {
                            throw new MerchantAPIKeyNotFoundException();
                        }
                        CardNumber = LiveEncryption.LiveEncryptionHelper.TripleDESDecryptNoPadding(CardNumber, APIKey);
                        TranToProcess.cardNumber = CardNumber;
                    }

                    if (!String.IsNullOrEmpty(TranToProcess.cardNumber))
                    {
                        if (TranToProcess.cardNumber.Length >= 6)
                            CardMasked = TranToProcess.cardNumber.Substring(0, 6) + "******" + TranToProcess.cardNumber.Remove(0, TranToProcess.cardNumber.Length - 4);
                        else
                            CardMasked = TranToProcess.cardNumber + "******";

                    }
                    else
                        throw new InvalidCardInformationException();
                    resp.CardMasked = CardMasked;


                    if (string.IsNullOrEmpty(terminalID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {

                        resp.TradingName = TradingName;
                        resp.ABN = ABN;
                        resp.MerchantID = MerchantID;
                        resp.MerchantName = MerchantName;
                        resp.TerminalID = terminalID;

                    }
                }


                if (String.IsNullOrEmpty(TranToProcess.cardVerificationNumber))
                {
                    throw new InvalidCVNInformationException();
                }

                if (String.IsNullOrEmpty(TranToProcess.card_currency))
                {
                    throw new InvalidCurrencyInformationException();
                }
                if (orderamount <= 0)
                {
                    throw new InvalidAmountInformationException();
                }
                if (TranToProcess.cardExpiryMonth <= 0 || TranToProcess.cardExpiryMonth > 12 || TranToProcess.cardExpiryYear <= 0)
                {
                    throw new InvalidExpiryDateException();
                }

              

                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                    @"logDirectory=LOG_DIR";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);

                //string certificateFile = "C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0&logDirectory=C:\\Users\\Gali7_000\\Documents\\Logs\\Payway";
                //payWayAPI.Initialise(certificateFile);

                //Hashtable requestParameters = new Hashtable();

                string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", MerchantID);
                requestParameters.Add("order.type", "capture");
                requestParameters.Add("card.PAN", TranToProcess.cardNumber);
                requestParameters.Add("card.CVN", TranToProcess.cardVerificationNumber);
                requestParameters.Add("card.expiryYear", TranToProcess.cardExpiryYear.ToString());
                requestParameters.Add("card.expiryMonth", TranToProcess.cardExpiryMonth.ToString());
                requestParameters.Add("order.amount", Math.Truncate(orderamount * 100).ToString());
                requestParameters.Add("customer.orderNumber", TransactionNumber);
                requestParameters.Add("card.currency", TranToProcess.card_currency);
                requestParameters.Add("order.ECI", "SSL");
                requestParameters.Add("customer.customerReferenceNumber", terminalID);
                requestParameters.Add("card.cardHolderName", TranToProcess.cardholdername);


                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                //Trace.TraceInformation(requestText);

                string responseText = payWayAPI.ProcessCreditCard(requestText);


                //Trace.TraceInformation(responseText);
                List<string> responseparametersList = new List<string>();
                responseparametersList = responseText.Split('&').ToList();
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                foreach (string s in responseparametersList)
                {
                    string field = s.Substring(0, s.IndexOf('='));
                    string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                    responseparameters.Add(field, value);
                }


                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {

                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                    resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                    resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    resp.OrderNumber = TransactionNumber;
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";

                    if (responseparameters["response.summaryCode"] == "2") //if transaction erred, reverse it
                    {

                        BackgroundWorker bgwReverseTransaction = new BackgroundWorker();
                        bgwReverseTransaction.DoWork += new DoWorkEventHandler(bgwReverseTransaction_DoWork);
                        TranToProcess.OriginalOrderNumber = TransactionNumber;
                        //MOTOTranResponse reverseresponse = ReverseTransaction(TranToProcess);
                        bgwReverseTransaction.RunWorkerAsync(TranToProcess);

                        resp.Approved = false;
                        resp.ResponseCode = "91";
                        resp.ResponseText = "Transaction Process Erred. Trasaction is reversed";
                    }

#if DEBUG
                    Trace.TraceInformation("Insert to Phoenix");
#endif
                    ////Write into phoenix database
                    BackgroundWorker bgwInsertTransaction = new BackgroundWorker();
                    bgwInsertTransaction.DoWork += new DoWorkEventHandler(bgwInsertTransaction_DoWork);
                    bgwInsertTransaction.RunWorkerAsync(resp);

                    //if (TranToProcess.SaveCustomer)
                    //{
                    //    //save customer card details

                    //    if (TranToProcess.Customer != null)
                    //    {
                    //        TranToProcess.Customer.MerchantUserName = TranToProcess.username;
                    //        TranToProcess.Customer.MerchantMemberKey = memberkey;
                    //        BackgroundWorker bgwAddCustomer = new BackgroundWorker();
                    //        bgwAddCustomer.DoWork += new DoWorkEventHandler(bgwAddCustomer_DoWork);
                    //        bgwAddCustomer.RunWorkerAsync(TranToProcess.Customer);
                    //    }
                    //}
                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }
                //return resp;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }
                //resp.Approved = false;
                //resp.ResponseCode = "";
                //resp.ResponseText = "Unknown Error";
            }
            finally
            {

            }
            return resp;
        }
        /// <summary>
        /// Process a MOTO transaction with provided Token.
        /// </summary>
        /// <param name="TranToProcess"></param>
        /// <returns></returns>
        public MOTOTranResponse ProcessTransactionWithToken(TokenTran TranToProcess)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            decimal orderamount = TranToProcess.Amount + TranToProcess.Surcharge + TranToProcess.GSTOnSurcharge;
            MOTOTranResponse resp = new MOTOTranResponse();
            resp.Description = TranToProcess.Description;
            resp.Amount = TranToProcess.Amount;
            resp.Surcharge = TranToProcess.Surcharge;
            resp.GSTOnSurcharge = TranToProcess.GSTOnSurcharge;
            resp.Token = TranToProcess.Token;
            //resp.CardHolderName = TranToProcess.cardholdername;
            string CardMasked = "";
            resp.UserName = TranToProcess.username;
            int memberkey = 0;
            try
            {
                bool foundToken = false;
                

                resp.CardMasked = CardMasked;
                if (String.IsNullOrEmpty(TranToProcess.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {

                    string APIKey = "";
                    terminalID = GetVirtualTerminalID(TranToProcess.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref memberkey, ref APIKey);
                    if (string.IsNullOrEmpty(terminalID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {

                        resp.TradingName = TradingName;
                        resp.ABN = ABN;
                        resp.MerchantID = MerchantID;
                        resp.MerchantName = MerchantName;
                        resp.TerminalID = terminalID;

                    }
                }
                using (TaxiEpayEntities entity = new TaxiEpayEntities())
                {
                    CustomerToken token = entity.CustomerTokens.Where(r => r.Token == TranToProcess.Token).FirstOrDefault(); //&& r.Customer.Member_key == memberkey
                    if (token == null)
                    {
                        throw new CustomerTokenNotFoundException();
                    }
                    else
                    {
                        resp.CardMasked = token.CustomerCard.CardMasked;
                        resp.CardHolderName = token.CustomerCard.CardHolderName;
                    }

                }

                //if (String.IsNullOrEmpty(TranToProcess.cardVerificationNumber))
                //{
                //    throw new InvalidCVNInformationException();
                //}

                if (String.IsNullOrEmpty(TranToProcess.card_currency))
                {
                    throw new InvalidCurrencyInformationException();
                }
                if (orderamount <= 0)
                {
                    throw new InvalidAmountInformationException();
                }
                //if (TranToProcess.cardExpiryMonth <= 0 || TranToProcess.cardExpiryMonth > 12 || TranToProcess.cardExpiryYear <= 0)
                //{
                //    throw new InvalidExpiryDateException();
                //}
                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                    @"logDirectory=LOG_DIR";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);

                //string certificateFile = "C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0&logDirectory=C:\\Users\\Gali7_000\\Documents\\Logs\\Payway";
                //payWayAPI.Initialise(certificateFile);

                //Hashtable requestParameters = new Hashtable();

                string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", MerchantID);
                if (TranToProcess.Preauth)
                    requestParameters.Add("order.type", "preauth");
                else
                    requestParameters.Add("order.type", "capture");
                //requestParameters.Add("card.PAN", TranToProcess.cardNumber);
                //requestParameters.Add("card.CVN", TranToProcess.cardVerificationNumber);
                //requestParameters.Add("card.expiryYear", TranToProcess.cardExpiryYear.ToString());
                //requestParameters.Add("card.expiryMonth", TranToProcess.cardExpiryMonth.ToString());
                requestParameters.Add("order.amount", Math.Truncate(orderamount * 100).ToString());
                requestParameters.Add("customer.orderNumber", TransactionNumber);
                requestParameters.Add("card.currency", TranToProcess.card_currency);
                requestParameters.Add("order.ECI", "REC");
                requestParameters.Add("customer.customerReferenceNumber", TranToProcess.Token);
                //requestParameters.Add("card.cardHolderName", TranToProcess.cardholdername);


                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                //Trace.TraceInformation(requestText);

                string responseText = payWayAPI.ProcessCreditCard(requestText);


                //Trace.TraceInformation(responseText);
                List<string> responseparametersList = new List<string>();
                responseparametersList = responseText.Split('&').ToList();
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                foreach (string s in responseparametersList)
                {
                    string field = s.Substring(0, s.IndexOf('='));
                    string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                    responseparameters.Add(field, value);
                }


                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {
                    if (MerchantID == "TEST")
                    {
                        if (TranToProcess.Amount == 15m) //Approved
                        {
                            resp.Approved = true;
                            resp.ResponseCode = "08";
                            resp.ResponseText = "Honour with indentification";
                            resp.ReceiptNumber = "123456";
                            resp.TransactionTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                            resp.CardScheme = "MASTERCARD";
                            resp.CreditGroup = "VI/BC/MC";
                        }
                        else if (TranToProcess.Amount == 20m) //Declined
                        {
                            resp.Approved = false;
                            resp.ResponseCode = "05";
                            resp.ResponseText = "Do not honour";
                            resp.ReceiptNumber = "123456";
                            resp.TransactionTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                            resp.CardScheme = "MASTERCARD";
                            resp.CreditGroup = "VI/BC/MC";
                        }

                    }
                    else
                    {
                        resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                        resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                        resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                        resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                        resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                        resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                        resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    }
                    resp.OrderNumber = TransactionNumber;
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";
                    if (responseparameters["response.summaryCode"] == "2") //if transaction erred, reverse it
                    {

                        BackgroundWorker bgwReverseTransaction = new BackgroundWorker();
                        bgwReverseTransaction.DoWork += new DoWorkEventHandler(bgwReverseTransaction_DoWork);
                        TranToProcess.OriginalOrderNumber = TransactionNumber;
                        //MOTOTranResponse reverseresponse = ReverseTransaction(TranToProcess);
                        bgwReverseTransaction.RunWorkerAsync(TranToProcess);

                        resp.Approved = false;
                        resp.ResponseCode = "91";
                        resp.ResponseText = "Transaction Process Erred. Trasaction is reversed";
                    }

#if DEBUG
                    Trace.TraceInformation("Insert to Phoenix");
#endif
                    //Write into phoenix database
                    BackgroundWorker bgwInsertTransaction = new BackgroundWorker();
                    bgwInsertTransaction.DoWork += new DoWorkEventHandler(bgwInsertTransaction_DoWork);
                    bgwInsertTransaction.RunWorkerAsync(resp);

                    //if (TranToProcess.SaveCustomer)
                    //{
                    //    //save customer card details

                    //    if (TranToProcess.Customer != null)
                    //    {
                    //        TranToProcess.Customer.MerchantUserName = TranToProcess.username;
                    //        TranToProcess.Customer.MerchantMemberKey = memberkey;
                    //        BackgroundWorker bgwAddCustomer = new BackgroundWorker();
                    //        bgwAddCustomer.DoWork += new DoWorkEventHandler(bgwAddCustomer_DoWork);
                    //        bgwAddCustomer.RunWorkerAsync(TranToProcess.Customer);
                    //    }
                    //}
                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }
                //return resp;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }
                //resp.Approved = false;
                //resp.ResponseCode = "";
                //resp.ResponseText = "Unknown Error";
            }
            finally
            {

            }
            return resp;
        }


        

        //private string TripleDESDecrypt(string username, string cipherText)
        //{
        //    string APIKey = "";
        //    using (TaxiEpayEntities entities = new TaxiEpayEntities())
        //    {
               
        //        var user = entities.UserProfiles.Where(r=>r.UserName.ToUpper() == username.ToUpper()).FirstOrDefault();
        //        if (user != null)
        //        {
        //            APIKey = user.APIKey; 
        //        }
        //        else
        //            throw new MerchantNotFoundException();

        //        if (!string.IsNullOrWhiteSpace(APIKey))
        //        {
        //            string plaintext = LiveEncryption.LiveEncryptionHelper.TripleDESDecrypt(cipherText, APIKey);

        //            return plaintext;
        //        }
        //        else
        //            throw new MerchantAPIKeyNotFoundException();
        //    }
            
        //}
        void bgwAddCustomer_DoWork(object sender, DoWorkEventArgs e)
        {
            MerchantCustomer customer = (MerchantCustomer)e.Argument;
            AddCustomer(customer);
        }

        void bgwReverseTransaction_DoWork(object sender, DoWorkEventArgs e)
        {
            MOTOTran resp = (MOTOTran)e.Argument;
            ReverseTransaction(resp);
        }

        void bgwReverseOrignalTransaction_DoWork(object sender, DoWorkEventArgs e)
        {
            ReverseMOTOTran resp = (ReverseMOTOTran)e.Argument;
            ReverseTransaction(resp);
        }

        void bgwInsertTransaction_DoWork(object sender, DoWorkEventArgs e)
        {
            MOTOTranResponse resp = (MOTOTranResponse)e.Argument;
            InsertTransactiontoPhoenix(resp);
        }

        public void InsertTransactiontoPhoenix(MOTOTranResponse response)
        {
            #if DEBUG
            Trace.TraceInformation("Insert to Phoenix");
#endif
            //Write into phoenix database
            //string CardMasked = response.CardMasked;
            //resp.CardMasked = CardMasked;
            try
            {
                
                decimal orderamount = response.Amount + response.Surcharge + response.GSTOnSurcharge;
                CadmusIN docket = new CadmusIN();
                docket.batchid = "";
                docket.batchnumber = response.OrderNumber.Substring(0, 4) + response.OrderNumber.Substring(8, 2);
                docket.batchstatus = "Loaded";
                docket.cardname = response.CardScheme;
                DateTime filedate = DateTime.Today;
                DateTime TransactionDateTime = DateTime.Now;
                if (!string.IsNullOrEmpty(response.TransactionTime))
                {
                    TransactionDateTime = Convert.ToDateTime(response.TransactionTime);
                }
                if (TransactionDateTime.Hour < 18)
                {
                    filedate = TransactionDateTime.Date;
                }
                else
                    filedate = TransactionDateTime.Date.AddDays(1);
                //Int64.TryParse(item.DriverID, out drid);
                string filename = "LiveEFT000000" + filedate.ToString("yyyyMMdd") + "O.csv";
                docket.driverid = 0;
                docket.endshift = TransactionDateTime;
                docket.filedate = filedate;
                docket.loaded = DateTime.Now;
                docket.merchantid = LiveCadmusMerchant;
                docket.merchantname = LiveMerchantName;
                docket.orig_cardname = response.CardScheme;
                docket.recordcreated = DateTime.Now;
                docket.sourcefile = filename;
                docket.startshift = TransactionDateTime;
                docket.taxiid = "";
                docket.terminalid = response.TerminalID;
                string status = response.Approved ? "Approved" : "Declined";
                docket.trans_status = status + " (" + response.ResponseCode + ")";
                int tranauth = 0;
                if (!Int32.TryParse(response.ReceiptNumber.Trim('\''), out tranauth))
                    tranauth = 0;
                docket.transauthorisationnumber = tranauth;
                docket.transdropoffzone = "";
                docket.transend = TransactionDateTime;
                docket.transstart = TransactionDateTime;
                docket.transextras = 0.0f;
                docket.transfare = (float)response.Amount;
                docket.transid1 = response.OrderNumber.Substring(0, 4) + response.OrderNumber.Substring(8, 2) + response.OrderNumber.Substring(10, 3);
                docket.transkilometers = 0;
                docket.transpan = response.CardMasked;
                docket.transpassengers = 0;
                docket.transpickupzone = "Online";
                docket.transdropoffzone = "Online";
                docket.transtarrif = 0;
                docket.transtip = 0;
                docket.transtotalamount = (float)orderamount;
                docket.transtype = "Credit";
                docket.HostBatchNumber = response.OrderNumber.Substring(0, 10);
                docket.HostTransactionNumber = response.OrderNumber;
                docket.HostServiceCharge = response.Surcharge;
                docket.HostGST = response.GSTOnSurcharge;
                //int Descriptionlen = response.Description.Length <= 200 ? response.Description.Length : 200;
                //docket.TransactionDescription = response.Description.Substring(0, Descriptionlen);
                //int BusinessIDLen = response.TradingName.Length <= 200 ? response.TradingName.Length : 200;
                //docket.BusinessID = response.TradingName.Substring(0,BusinessIDLen);
                //int UserIDLen = response.UserName.Length <= 50 ? response.UserName.Length : 50;
                //docket.UserID = response.UserName.Substring(0,UserIDLen);

                docket.TransactionDescription = response.Description;
                docket.BusinessID = response.TradingName;
                docket.UserID = response.UserName;
                
                List<CadmusIN> tranlist = new List<CadmusIN>();
                docket.ResponseCode = response.ResponseCode;
                docket.ResponseText = response.ResponseText;
                docket.ABN = response.ABN;
                docket.CardHolderName = response.CardHolderName;
                tranlist.Add(docket);
                this.UploadTransactions(tranlist);
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", response.MerchantID, ex.Message));
                //resp.Approved = false;
                //resp.ResponseCode = "96";
                //resp.ResponseText = "Internal Error";
            }
        }
      
        public float GetSurcharge(string username, string FirstFourDigits)
        {
            float surcharge = 0.0f;
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {


                    GetMSFByLEUsername_Result result = entities.GetSurchargeByLEUserName(username, FirstFourDigits).FirstOrDefault();
                    if (result != null && !String.IsNullOrEmpty(result.TerminalId))
                    {
                        surcharge = Convert.ToSingle(result.Surcharge.GetValueOrDefault(0.02d));
                    }
                    else
                        throw new MerchantNotFoundException();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error retrieving surcharge for username:{0}. Error message:{1}",username, ex.Message));
                surcharge = 0.0f;
            }
            return surcharge;
        }
        
        public bool SendReceiptViaSMS(string MobileNumber, string merchantid, string ordernumber)
        {
            
            try
            {
                string ReceiptURL = GenerateReceiptURL(merchantid, ordernumber);
                if (!String.IsNullOrEmpty(ReceiptURL))
                {
                    using (TaxiEpayEntities entities = new TaxiEpayEntities())
                    {
                        string message = PhoenixWCFService.Properties.Settings.Default.SupportMessage;// "For Any Queries Call 1300 883 703";
                        SMSMessageOut newmessage = new SMSMessageOut();
                        newmessage.MessageTo = MobileNumber;
                        newmessage.MessageText = ReceiptURL + message;
                        newmessage.Modified_dttm = DateTime.Now;
                        newmessage.ModifiedByUser = "OnlinePayment";
                        entities.SMSMessageOuts.AddObject(newmessage);
                        entities.SaveChanges();
                        return true;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Error sending SMS. TerminalID: {0}. TransactionNumber:{1}. Error message: {2}", merchantid, ordernumber, ex.Message));
                return false;

            }

        }
        public bool SendReceiptViaEmail(string Email, string merchantid, string ordernumber)
        {
            try
            {
                string reportURL = string.Format("{0}&rs:Command=Render&rc:Parameters=false&rc:toolbar=false&rc:format=html4.0&rs:ClearSession=true&TerminalID={1}&TransactionNumber={2}", PhoenixWCFService.Properties.Settings.Default.ReceiptReportURL, merchantid, ordernumber);
                //pic.FileContent = data;
                string URL = "";
                System.Net.WebClient c = new System.Net.WebClient();
                c.Credentials = new NetworkCredential(PhoenixWCFService.Properties.Settings.Default.ReportServerUsername, PhoenixWCFService.Properties.Settings.Default.ReportServerPassword, PhoenixWCFService.Properties.Settings.Default.ReportSeverDomain);
                //c.Credentials = new NetworkCredential("Administrator", "Livepayments123", "10.100.2.16");
                #if DEBUG
                Trace.TraceInformation("download report as HTML");
#endif
                string EmailBody = c.DownloadString(reportURL);
                SMTPMailSender emailsender = new SMTPMailSender();
                emailsender.SMTPServer = PhoenixWCFService.Properties.Settings.Default.SMTPServer;
                emailsender.SMTPPort = PhoenixWCFService.Properties.Settings.Default.SMTPPort;
                emailsender.MailFrom = PhoenixWCFService.Properties.Settings.Default.SMTPEmailFrom;
                emailsender.MailBody = EmailBody;
                emailsender.MailTo = Email;
                //emailsender.MailCC = "reports@livegroup.com.au";
                emailsender.MailSubject = "Online payment Receipt";
                emailsender.SendMail();
                
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error sending receipt via email to {0}. TerminalID: {1}, TransactionNumber: {2}. Error message: {3}", Email, merchantid, ordernumber, ex.Message));
                return false;

            }

        }
        string GenerateRandomReceiptID()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
        public string GenerateReceiptURL(string TerminalID, string TransactionNumber)
        {
            //if (CustomerMobileNumer != "")
            //{
                //Trace.TraceInformation("Send SMS to number: " + CustomerMobileNumer);
                //string reportURL = string.Format("http://10.10.2.20/Reportserver?/HostReports/Receipt&rs:Command=Render&rs:Format=IMAGE&rc:outputformat=JPEG&TerminalID={0}&TransactionNumber={1}",TerminalID, TransactionNumber);
            string URL = "";
            try
            {
                string reportURL = string.Format("{0}&rs:Command=Render&rs:Format=IMAGE&rc:outputformat=JPEG&TerminalID={1}&TransactionNumber={2}", PhoenixWCFService.Properties.Settings.Default.ReceiptReportURL, TerminalID, TransactionNumber);
                //pic.FileContent = data;

                System.Net.WebClient c = new System.Net.WebClient();
                c.Credentials = new NetworkCredential(PhoenixWCFService.Properties.Settings.Default.ReportServerUsername, PhoenixWCFService.Properties.Settings.Default.ReportServerPassword);
                #if DEBUG
                Trace.TraceInformation("download report as image");
#endif
                byte[] data = c.DownloadData(reportURL);
                if (data != null)
                {
                    string RandomID = "";
                    using (TaxiEpayEntities MyEntities = new TaxiEpayEntities())
                    {
                        List<PictureReceiptID> result = new List<PictureReceiptID>();
                        do
                        {
                            RandomID = GenerateRandomReceiptID();
                            result = MyEntities.GetReceiptPictureByReceiptID(RandomID).ToList();
                        }
                        while (result.Count > 0);

                        ReceiptPicture pic = new ReceiptPicture();
                        pic.FileName = RandomID + ".jpg";
                        pic.FileContent = data;
                        pic.TerminalID = TerminalID;
                        //pic.MerchantID = MerchantID;
                        pic.TransactionNumber = TransactionNumber;
                        pic.CreatedDateTime = DateTime.Now;
                        pic.CreatedBy = "TransactionHost";
                        //pic.MobileNumberToSend = CustomerMobileNumer;
                        pic.ReceiptID = RandomID;
                        //System.Diagnostics.Trace.Write("created picture");
                        #if DEBUG
                        Trace.TraceInformation("Save receipt image");
#endif
                        MyEntities.ReceiptPictures.AddObject(pic);
                        MyEntities.SaveChanges();

                        //upload to FTP
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(PhoenixWCFService.Properties.Settings.Default.ReceiptFTPServer + pic.FileName);
                        request.Credentials = new NetworkCredential(PhoenixWCFService.Properties.Settings.Default.ReceiptFTPServerUsername, PhoenixWCFService.Properties.Settings.Default.ReceiptFTPServerPassword);
                        request.Method = WebRequestMethods.Ftp.UploadFile;
                        request.UseBinary = true;
                        request.ContentLength = data.Length;
                        using (Stream reqStream = request.GetRequestStream())
                        {
                            reqStream.Write(data, 0, data.Length);
                            reqStream.Close();
                        }

                        //System.Diagnostics.Trace.Write("saved picture");
                        //Configuration config = WebConfigurationManager.OpenWebConfiguration(null);
                        URL = PhoenixWCFService.Properties.Settings.Default.ReceiptURL + RandomID + " . ";
                        //string message = PhoenixWCFService.Properties.Settings.Default.SupportMessage;// "For Any Queries Call 1300 883 703";
                        //SMSMessageOut newmessage = new SMSMessageOut();
                        //newmessage.MessageTo = CustomerMobileNumer;
                        //newmessage.MessageText = URL + message;
                        //newmessage.Modified_dttm = DateTime.Now;
                        //newmessage.ModifiedByUser = "TransactionHost";
                        //MyEntities.SMSMessageOuts.AddObject(newmessage);
                        //MyEntities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return URL;
            //}
        }
        public TransactionHistory GetTransactionDetail(string username, string ordernumber)
        {
            TransactionHistory tran = new TransactionHistory();
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                try
                {
                    OnlineTransactionDetail item = entities.GetOnlineTransactionDetail(username, ordernumber).FirstOrDefault();
                    if (item != null)
                    {
                        tran.ABN = item.ABN;
                        tran.Amount = Convert.ToDecimal(item.Amount.GetValueOrDefault(0.0f));
                        tran.Approved = item.Status.Contains("Approved");
                        tran.CardMasked = item.PAN;
                        tran.CardScheme = item.CardType;
                        tran.Description = item.TranDescription;
                        tran.GSTOnSurcharge = item.GSTOnSurcharge.GetValueOrDefault(0.0m);
                        tran.MerchantID = item.MerchantID;
                        tran.TerminalID = item.TerminalID;
                        tran.OrderNumber = item.OrderNumber;
                        tran.PaymentDate = item.PaymentDate;
                        tran.ReceiptNumber = item.ReceiptNo.ToString();
                        tran.Surcharge = item.Surcharge.GetValueOrDefault(0.0m);
                        tran.Amount = Convert.ToDecimal(item.Amount.GetValueOrDefault(0.0f));
                        tran.Total = Convert.ToDecimal(item.Total.GetValueOrDefault(0.0f));
                        tran.TradingName = item.TradingName;
                        tran.TransactionTime = item.TranDateTime.GetValueOrDefault(DateTime.MinValue).ToString("dd-MMM-yyyy HH:mm:ss");
                        tran.UserName = item.username;
                        tran.TransactionDateTime = item.TranDateTime.Value;
                        tran.ResponseText = item.ResponseText;
                        tran.MerchantName = item.MerchantName;
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Error getting Transaction Detail. Username: {0}. OrderNumber: {1}. Error message: {2}", username, ordernumber, ex.Message);
                }
            }
            return tran;
        }
        public List<TransactionHistory> GetTransactionHistory(string username)
        {
            List<TransactionHistory> TransactionList = new List<TransactionHistory>();
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    List<OnlineTransactionHistory> tranlist = entities.GetOnlineTransactionByDateRange(username).ToList();
                    foreach (var item in tranlist)
                    {
                        try
                        {
                            TransactionHistory tran = new TransactionHistory();
                            tran.ABN = item.ABN;
                            tran.Amount = Convert.ToDecimal(item.Amount.GetValueOrDefault(0.0f));
                            tran.Approved = item.Status.Contains("Approved");
                            tran.CardMasked = item.PAN;
                            tran.CardScheme = item.CardType;
                            tran.Description = item.TranDescription;
                            tran.GSTOnSurcharge = item.GSTOnSurcharge.GetValueOrDefault(0.0m);
                            tran.MerchantID = item.MerchantID;
                            tran.TerminalID = item.TerminalID;
                            tran.OrderNumber = item.OrderNumber;
                            tran.PaymentDate = item.PaymentDate;
                            tran.ReceiptNumber = item.ReceiptNo.ToString();
                            tran.Surcharge = item.Surcharge.GetValueOrDefault(0.0m);
                            tran.Amount = Convert.ToDecimal(item.Amount.GetValueOrDefault(0.0f));
                            tran.Total = Convert.ToDecimal(item.Total.GetValueOrDefault(0.0f));
                            tran.TradingName = item.TradingName;
                            tran.TransactionTime = item.TranDateTime.GetValueOrDefault(DateTime.MinValue).ToString("dd-MMM-yyyy HH:mm:ss");
                            tran.UserName = item.username;
                            tran.TransactionDateTime = item.TranDateTime.Value;
                            TransactionList.Add(tran);


                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error getting transaction history. Error message: {0}", ex.Message));
            }
            return TransactionList;
        }



       
        public List<TransactionPayment> GetTransactionPayments(string username, DateTime StartDate, DateTime EndDate)
        {
            List<TransactionPayment> tranlist = new List<TransactionPayment>();
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                try
                {
                    foreach(var item in entities.GetOnlineTransactionPayment(username, StartDate, EndDate))
                    {
                        TransactionPayment tran = new TransactionPayment();
                        tran.PaymentDate = item.PaymentDate.Value;
                        tran.PaymentReference = item.ReferenceNumber;
                        tran.TotalPayment = item.TotalAmount.GetValueOrDefault(0);
                        tranlist.Add(tran);
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Error getting Transaction Payment. Username: {0}", username);
                }
            }
            return tranlist;
        }

        public bool SwapTerminal(string oldTerminalID, string newTerminalID, DateTime EffectiveDate)
        {
            try
            {
                if (EffectiveDate < DateTime.Now.AddDays(-30))
                {
                    throw new Exception("The Effective Date is more than 30 days in the past");
                    
                }
                else
                {
                    SwapTerminal_Result result;
                    using (TaxiEpayEntities entities = new TaxiEpayEntities())
                    {
                        result = entities.SwapTerminal(oldTerminalID, newTerminalID, EffectiveDate, "Auto Function").FirstOrDefault();
                        if (result != null)
                        { 
                            if (string.IsNullOrEmpty(result.MemberID))
                            {
                                throw new Exception("TerminalID not valid or member not found");
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            throw new Exception("Swap terminal failed");
                        }
                    }
                }
                //return true;
            }
            catch(Exception ex)
            {
                throw new FaultException(ex.Message);
            }
        }
        public bool ReturnTerminal(string oldTerminalID, DateTime EffectiveDate)
        {
            try
            {
                if (EffectiveDate < DateTime.Now.AddDays(-30))
                {
                    throw new Exception("The Effective Date is more than 30 days in the past");

                }
                else
                {
                    ReturnTerminal_Result result;
                    using (TaxiEpayEntities entities = new TaxiEpayEntities())
                    {
                        result = entities.ReturnTerminal(oldTerminalID,  EffectiveDate, "Auto Function").FirstOrDefault();
                        if (result != null)
                        {
                            if (result.OldTerminalKey.GetValueOrDefault(0) == 0)
                            {
                                throw new Exception("Terminal not found");
                            }
                            else if (result.MemberKey.GetValueOrDefault(0) == 0)
                            {
                                throw new Exception("Terminal was not allocated");
                            }
                            else
                            {
                                return true;

                            }
                        }
                        else
                        {
                            throw new Exception("Return terminal failed: Terminal not found");
                        }
                    }
                }
                //return true;
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public bool NewAllocation(string terminalID, string CustomerMobileNumber, string DriverLicenseTrack1)
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }
        }
        public bool UpdateInformation(string TerminalID, string CustomerMobileNumber, string DriverLicense)
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }
        }
        ///// <summary>
        ///// Register a Credit Card information with Payway
        ///// </summary>
        ///// <param name="accountToRegister"></param>
        ///// <returns></returns>
        //public RegisterAccountResponse RegisterAccount(CrediCardAccount accountToRegister)
        //{
        //    string terminalID = "";
        //    string TradingName = "";
        //    string ABN = "";
        //    string MerchantID = "";
        //    string MerchantName = "";
        //    string CardMasked = "";

        //    RegisterAccountResponse resp = new RegisterAccountResponse() ;
        //    resp.UserName = accountToRegister.username;
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(accountToRegister.cardNumber))
        //        {
        //            if (accountToRegister.cardNumber.Length >= 6)
        //                CardMasked = accountToRegister.cardNumber.Substring(0, 6) + "******" + accountToRegister.cardNumber.Remove(0, accountToRegister.cardNumber.Length - 4);
        //            else
        //                CardMasked = accountToRegister.cardNumber + "******";

                    

        //        }
        //        else
        //            throw new InvalidCardInformationException();


        //        if (String.IsNullOrEmpty(accountToRegister.username))
        //        {
        //            throw new InvalidUsernameException();
        //        }
        //        else
        //        {

        //            int member_key = 0;
        //            terminalID = GetVirtualTerminalID(accountToRegister.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key);
        //            if (string.IsNullOrEmpty(terminalID))
        //            {
        //                throw new MerchantNotFoundException();
        //            }
        //            else
        //            {

 
        //            }
        //        }


             
        //        if (accountToRegister.cardExpiryMonth <= 0 || accountToRegister.cardExpiryMonth > 12 || accountToRegister.cardExpiryYear <= 0)
        //        {
        //            throw new InvalidExpiryDateException();
        //        }
        //        string ReferenceNumber = "";
        //        using (TaxiEpayEntities entity = new TaxiEpayEntities())
        //        { 
        //            bool exist = false;
        //            do
        //            {
        //                ReferenceNumber = GenerateRandomCustomerReferenceNumber(20);
        //                var crn = entity.MemberCRNs.Where(r => r.ExternalCRN == ReferenceNumber).FirstOrDefault();
        //                if (crn != null)
        //                    exist = true;
        //                else exist = false;
        //            }
        //            while (exist);
        //            string s = "";
        //            s = UpdateReferenceNumber(accountToRegister.username, ReferenceNumber, CardMasked, (short)accountToRegister.cardExpiryMonth, (short)accountToRegister.cardExpiryYear, accountToRegister.cardholdername);
        //        }
        //        if (string.IsNullOrEmpty(ReferenceNumber))
        //        {
        //            throw new RegisterAccountException();
        //        }


              
        //        PayWayAPI payWayAPI = new PayWayAPI();

        //        String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
        //                            @"logDirectory=LOG_DIR";

        //        initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
        //        payWayAPI.Initialise(initParams);

           

        //        Dictionary<string, string> requestParameters = new Dictionary<string, string>();
        //        requestParameters.Add("customer.username", PayWayUsername);
        //        requestParameters.Add("customer.password", PayWayPassword);
        //        requestParameters.Add("customer.merchant", MerchantID);
        //        requestParameters.Add("order.type", "registerAccount");
        //        requestParameters.Add("card.PAN", accountToRegister.cardNumber);
        //        requestParameters.Add("card.expiryYear", accountToRegister.cardExpiryYear.ToString());
        //        requestParameters.Add("card.expiryMonth", accountToRegister.cardExpiryMonth.ToString());
        //        //requestParameters.Add("order.ECI", "SSL");
        //        requestParameters.Add("customer.customerReferenceNumber",ReferenceNumber);
        //        requestParameters.Add("card.cardHolderName", accountToRegister.cardholdername);


        //        //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

        //        string requestText = payWayAPI.FormatRequestParameters(requestParameters);

        //        //Trace.TraceInformation(requestText);

        //        string responseText = payWayAPI.ProcessCreditCard(requestText);


        //        //Trace.TraceInformation(responseText);
        //        List<string> responseparametersList = new List<string>();
        //        responseparametersList = responseText.Split('&').ToList();
        //        Dictionary<string, string> responseparameters = new Dictionary<string, string>();
        //        foreach (string s in responseparametersList)
        //        {
        //            string field = s.Substring(0, s.IndexOf('='));
        //            string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
        //            responseparameters.Add(field, value);
        //        }


        //        if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
        //        {

        //            resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
        //            resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
        //            resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
        //            resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
        //            resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";

        //            string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";
        //            resp.CardMasked = CardMasked;

        //            if (resp.Approved)
        //                resp.CardToken = ReferenceNumber;
        //        }
        //        else
        //        {
        //            resp.Approved = false;
        //            resp.ResponseCode = "96";
        //            resp.ResponseText = "Unknown Error from WBC API";
        //        }
        //        //return resp;
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
        //        if (ex is PhoenixExceptions)
        //        {
        //            resp.Approved = false;
        //            resp.ResponseCode = "96";
        //            resp.ResponseText = ex.Message;
        //        }
        //        //resp.Approved = false;
        //        //resp.ResponseCode = "";
        //        //resp.ResponseText = "Unknown Error";
        //    }
        //    finally
        //    {

        //    }
        //    return resp;
        //}



        public RegisterAccountResponse RegisterToken(MOTOTran TokenToAdd)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            string CardMasked = "";

            RegisterAccountResponse resp = new RegisterAccountResponse();

            //MOTOTranResponse tranres = ProcessTransaction(TokenToAdd);
            //try
            //{
                //if (tranres.Approved)
                //{
                    //if (TokenToAdd.SaveCustomer)
                    //{
                        //CardToken card = (CardToken)
            try
            {
             
                string CardNumber = "";
                string APIKey = "";
                //use username to verify if User has virtual terminal or not
                if (String.IsNullOrEmpty(TokenToAdd.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {
                    resp.UserName = (TokenToAdd.username);
                    int member_key = 0;
                    
                    terminalID = GetVirtualTerminalID(TokenToAdd.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key, ref APIKey);
                    if (string.IsNullOrEmpty(terminalID) && member_key>0)
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {
                        //nothing to do
                        TokenToAdd.Customer.MerchantMemberKey = member_key;
                        TokenToAdd.Customer.MerchantUserName = TokenToAdd.username;

                    }
                }

                if (string.IsNullOrWhiteSpace(APIKey))
                {
                    throw new MerchantAPIKeyNotFoundException();
                }

                if (!String.IsNullOrEmpty(TokenToAdd.cardNumber))
                {
                    CardNumber = LiveEncryption.LiveEncryptionHelper.TripleDESDecryptNoPadding(TokenToAdd.cardNumber, APIKey);
                }
                else
                    throw new InvalidCardInformationException();

                //verify Expiry Date
                if (TokenToAdd.cardExpiryMonth <= 0 || TokenToAdd.cardExpiryMonth > 12 || TokenToAdd.cardExpiryYear < (DateTime.Now.Year - 2000))
                {
                    throw new InvalidExpiryDateException();
                }
                string ReferenceNumber = "";
                
                //Add Customer
                int customerKey = 0;
                customerKey = AddCustomer(TokenToAdd.Customer);
                if (customerKey >= 0)
                {
                    //Add CardToken
                    bool newtoken = false;
                    CardToken cardtoken = new CardToken();
                    cardtoken.CardHolderName = TokenToAdd.cardholdername;
                    cardtoken.CardMasked = GetCardMasked(CardNumber); // TokenToAdd.CardMasked;
                    cardtoken.ExpiryMonth = TokenToAdd.cardExpiryMonth;
                    cardtoken.ExpiryYear = TokenToAdd.cardExpiryYear;
                    cardtoken.CardHash = TokenToAdd.HashString;

                    resp.CustomerKey = customerKey;
                    resp.CardMasked = GetCardMasked(CardNumber);// TokenToAdd.CardMasked;
                    ReferenceNumber = AddCardToken(cardtoken, customerKey, terminalID, ref newtoken);
                    resp.CardToken = ReferenceNumber;


                    
                    //CardNumber = TripleDESDecrypt(TokenToAdd.username, TokenToAdd.cardNumber);
                                       
                   

                    TokenToAdd.cardNumber = CardNumber;

                    PayWayAPI payWayAPI = new PayWayAPI();

                    String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                        @"logDirectory=LOG_DIR";

                    initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                    payWayAPI.Initialise(initParams);



                    Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                    requestParameters.Add("customer.username", PayWayUsername);
                    requestParameters.Add("customer.password", PayWayPassword);
                    requestParameters.Add("customer.merchant", MerchantID);
                    requestParameters.Add("order.type", "registerAccount");
                    requestParameters.Add("card.PAN", TokenToAdd.cardNumber);
                    requestParameters.Add("card.expiryYear", TokenToAdd.cardExpiryYear.ToString());
                    requestParameters.Add("card.expiryMonth", TokenToAdd.cardExpiryMonth.ToString());
                    //requestParameters.Add("order.ECI", "SSL");
                    requestParameters.Add("customer.customerReferenceNumber", ReferenceNumber);
                    requestParameters.Add("card.cardHolderName", TokenToAdd.cardholdername);


                    //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                    //prepare for sending request
                    string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                    //Trace.TraceInformation(requestText);


                    //send request and get response back from Payway
                    string responseText = payWayAPI.ProcessCreditCard(requestText);


                    //Trace.TraceInformation(responseText);
                    List<string> responseparametersList = new List<string>();
                    responseparametersList = responseText.Split('&').ToList();
                    Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                    foreach (string s in responseparametersList)
                    {
                        string field = s.Substring(0, s.IndexOf('='));
                        string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                        responseparameters.Add(field, value);
                    }
                    bool approved = false;

                    //if (TokenToAdd.username = "")
                    if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                    {
                        if (MerchantID == "TEST")
                        {
                            approved = true;
                            resp.Approved = true;
                            resp.ResponseCode = "00";
                            resp.ResponseText = "Approved";
                            resp.ReceiptNumber = "123456";
                            resp.TransactionTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                                

                        }
                        else
                        {
                            approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;

                            resp.Approved = approved;
                            resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                            resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                            resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                            resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                        }
                        string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";
                        if (!approved)
                        {
                            resp.CardMasked = "";
                            resp.CardToken = "";
                        }
                        //if (resp.Approved)
                        //    resp.CardToken = ReferenceNumber;

                    }
                    else
                    {
                        resp.Approved = false;
                        resp.ResponseCode = "96";
                        resp.ResponseText = "Unknown Error from WBC API";
                    }

                
                    //if approved, save card token to database and link to the Customer token record.
                    if (!approved && newtoken)
                    {
                        if (!string.IsNullOrEmpty(ReferenceNumber))
                            DeactivateCustomerToken(ReferenceNumber);
                        throw new RegisterAccountException("Failed to register Token");
                    }

                }
                else
                {
                    throw new RegisterAccountException("Failed to register Token");
                }


                //return resp;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }
             
            }
            finally
            {

            }

 
            return resp;
        }


        private string GetCardMasked(string CardNumber)
        {

            string cardMasked = "";
            try
            {
                if (!String.IsNullOrEmpty(CardNumber))
                {
                    if (CardNumber.Length >= 6)
                        cardMasked = CardNumber.Substring(0, 6) + "******" + CardNumber.Remove(0, CardNumber.Length - 4);
                    else
                        cardMasked = CardNumber + "******";
                }

            }
            catch
            {
                throw;
            }
            return cardMasked;
        }
        private void DeactivateCustomerToken(string Token)
        {
            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                //Delete 
                CustomerToken token = entities.CustomerTokens.FirstOrDefault(r=>r.Token == Token);
                if (token != null)
                {
                    token.DeactivateDate = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Generate a random Customer Reference number with specified length
        /// </summary>
        /// <returns></returns>
        private string GenerateRandomCustomerReferenceNumber(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
        /// <summary>
        /// Remove registered Credit Card information. The Card information properties can be blank.
        /// </summary>
        /// <param name="accountToDeregister"></param>
        /// <returns></returns>
        public RegisterAccountResponse DeregisterAccount(DeregisteredAccount accountToDeregister)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            string CardMasked = "";

            DeregisteredAccount _accountToDeregister = new DeregisteredAccount();
            _accountToDeregister.CardToken = accountToDeregister.CardToken;
            _accountToDeregister.username = accountToDeregister.username;

            RegisterAccountResponse resp = new RegisterAccountResponse();
            resp.UserName = _accountToDeregister.username;
            try
            {
                //if (!String.IsNullOrEmpty(accountToRegister.cardNumber))
                //{
                //    if (accountToRegister.cardNumber.Length >= 6)
                //        CardMasked = accountToRegister.cardNumber.Substring(0, 6) + "******" + accountToRegister.cardNumber.Remove(0, accountToRegister.cardNumber.Length - 4);
                //    else
                //        CardMasked = accountToRegister.cardNumber + "******";



                //}
                //else
                //    throw new InvalidCardInformationException();


                if (String.IsNullOrEmpty(_accountToDeregister.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {

                    int member_key = 0;
                    string APIKey = "";
                    terminalID = GetVirtualTerminalID(_accountToDeregister.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key, ref APIKey);
                    if (string.IsNullOrEmpty(terminalID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {

                        //resp.TradingName = TradingName;
                        //resp.ABN = ABN;
                        //resp.MerchantID = MerchantID;
                        //resp.MerchantName = MerchantName;
                        //resp.TerminalID = terminalID;
                    }
                }


                //if (String.IsNullOrEmpty(accountToRegister.cardVerificationNumber))
                //{
                //    throw new InvalidCVNInformationException();
                //}
                //if (accountToRegister.cardExpiryMonth <= 0 || accountToRegister.cardExpiryMonth > 12 || accountToRegister.cardExpiryYear <= 0)
                //{
                //    throw new InvalidExpiryDateException();
                //}
                string ReferenceNumber = "";
                ReferenceNumber = GetCustomerReferenceNumber(_accountToDeregister.username, _accountToDeregister.CardToken);
                if (string.IsNullOrEmpty(ReferenceNumber))
                {
                    throw new RegisterAccountException();
                }


                //if (String.IsNullOrEmpty(accountToRegister.card_currency))
                //{
                //    throw new InvalidCurrencyInformationException();
                //}
                //if (orderamount <= 0)
                //{
                //    throw new InvalidAmountInformationException();
                //}

                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                    @"logDirectory=LOG_DIR";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);

                //string certificateFile = "C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0&logDirectory=C:\\Users\\Gali7_000\\Documents\\Logs\\Payway";
                //payWayAPI.Initialise(certificateFile);

                //Hashtable requestParameters = new Hashtable();

                //string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", MerchantID);
                requestParameters.Add("order.type", _accountToDeregister.GetTranType());
                //requestParameters.Add("card.PAN", accountToDeregister.cardNumber);
                //requestParameters.Add("card.CVN", accountToDeregister.cardVerificationNumber);
                //requestParameters.Add("card.expiryYear", accountToDeregister.cardExpiryYear.ToString());
                //requestParameters.Add("card.expiryMonth", accountToDeregister.cardExpiryMonth.ToString());
                requestParameters.Add("order.ECI", "SSL");
                requestParameters.Add("customer.customerReferenceNumber", ReferenceNumber);
                //requestParameters.Add("card.cardHolderName", accountToDeregister.cardholdername);


                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                string requestText = payWayAPI.FormatRequestParameters(requestParameters);

                //Trace.TraceInformation(requestText);

                string responseText = payWayAPI.ProcessCreditCard(requestText);


                //Trace.TraceInformation(responseText);
                List<string> responseparametersList = new List<string>();
                responseparametersList = responseText.Split('&').ToList();
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                foreach (string s in responseparametersList)
                {
                    string field = s.Substring(0, s.IndexOf('='));
                    string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                    responseparameters.Add(field, value);
                }


                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {

                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                    //resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                    //resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    //resp.OrderNumber = TransactionNumber;
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";
                    //resp.CardMasked = CardMasked;
                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }
                //return resp;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }
                //resp.Approved = false;
                //resp.ResponseCode = "";
                //resp.ResponseText = "Unknown Error";
            }
            finally
            {

            }
            return resp;
        }
        /// <summary>
        /// Get the stored Customer Reference Number
        /// </summary>
        /// <param name="username"></param>
        /// <param name="ExternalCRN"></param>
        /// <returns></returns>
        string GetCustomerReferenceNumber(string username, string ExternalCRN)
        {
            string CRN = "";
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {

                    GetCustomerReferenceNumber_Result result = null;
                    result = entities.GetCustomerReferenceNumber(username, ExternalCRN).FirstOrDefault();
                    if (result != null)
                    {
                        CRN = result.CustomerReferenceNumber;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return CRN;
            
        }
//        /// <summary>
//        /// Process Transaction with Customer Reference Number instead of Card Details
//        /// </summary>
//        /// <param name="TranToProcess"></param>
//        /// <returns></returns>
//        public MOTOTranResponse ProcessCardlessTransaction(CardlessTran TranToProcess)
//        {
//            string terminalID = "";
//            string TradingName = "";
//            string ABN = "";
//            string MerchantID = "";
//            string MerchantName = "";
//            decimal orderamount = TranToProcess.Amount + TranToProcess.Surcharge + TranToProcess.GSTOnSurcharge;
//            MOTOTranResponse resp = new MOTOTranResponse();
//            resp.Description = TranToProcess.Description;
//            resp.Amount = TranToProcess.Amount;
//            resp.Surcharge = TranToProcess.Surcharge;
//            resp.GSTOnSurcharge = TranToProcess.GSTOnSurcharge;
        
//            string CardMasked = "";
//            resp.UserName = TranToProcess.username;
//            try
//            {
//                //if (!String.IsNullOrEmpty(TranToProcess.cardNumber))
//                //{
//                //    if (TranToProcess.cardNumber.Length >= 6)
//                //        CardMasked = TranToProcess.cardNumber.Substring(0, 6) + "******" + TranToProcess.cardNumber.Remove(0, TranToProcess.cardNumber.Length - 4);
//                //    else
//                //        CardMasked = TranToProcess.cardNumber + "******";

//                //}
//                //else
//                //    throw new InvalidCardInformationException();
//                resp.CardMasked = CardMasked;
//                if (String.IsNullOrEmpty(TranToProcess.username))
//                {
//                    throw new InvalidUsernameException();
//                }
//                else
//                {
//                    int member_key = 0;

//                    terminalID = GetVirtualTerminalID(TranToProcess.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key);
//                    if (string.IsNullOrEmpty(terminalID))
//                    {
//                        throw new MerchantNotFoundException();
//                    }
//                    else
//                    {

//                        resp.TradingName = TradingName;
//                        resp.ABN = ABN;
//                        resp.MerchantID = MerchantID;
//                        resp.MerchantName = MerchantName;
//                        resp.TerminalID = terminalID;
//                    }
//                }


//                //if (String.IsNullOrEmpty(TranToProcess.cardVerificationNumber))
//                //{
//                //    throw new InvalidCVNInformationException();
//                //}

//                if (String.IsNullOrEmpty(TranToProcess.card_currency))
//                {
//                    throw new InvalidCurrencyInformationException();
//                }
//                if (orderamount <= 0)
//                {
//                    throw new InvalidAmountInformationException();
//                }
//                //if (TranToProcess.cardExpiryMonth <= 0 || TranToProcess.cardExpiryMonth > 12 || TranToProcess.cardExpiryYear <= 0)
//                //{
//                //    throw new InvalidExpiryDateException();
//                //}
//                PayWayAPI payWayAPI = new PayWayAPI();

//                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
//                                    @"logDirectory=LOG_DIR";

//                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
//                payWayAPI.Initialise(initParams);

//                //string certificateFile = "C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0&logDirectory=C:\\Users\\Gali7_000\\Documents\\Logs\\Payway";
//                //payWayAPI.Initialise(certificateFile);

//                //Hashtable requestParameters = new Hashtable();

//                string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

//                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
//                requestParameters.Add("customer.username", PayWayUsername);
//                requestParameters.Add("customer.password", PayWayPassword);
//                requestParameters.Add("customer.merchant", MerchantID);
//                requestParameters.Add("order.type", "capture");
//                //requestParameters.Add("card.PAN", TranToProcess.cardNumber);
//                //requestParameters.Add("card.CVN", TranToProcess.cardVerificationNumber);
//                //requestParameters.Add("card.expiryYear", TranToProcess.cardExpiryYear.ToString());
//                //requestParameters.Add("card.expiryMonth", TranToProcess.cardExpiryMonth.ToString());
//                requestParameters.Add("order.amount", Math.Truncate(orderamount * 100).ToString());
//                requestParameters.Add("customer.orderNumber", TransactionNumber);
//                requestParameters.Add("card.currency", TranToProcess.card_currency);
//                requestParameters.Add("order.ECI", "SSL");
//                requestParameters.Add("customer.customerReferenceNumber", TranToProcess.CustomerReferenceNumber);
//                //requestParameters.Add("card.cardHolderName", TranToProcess.cardholdername);


//                //string requestText = payWayAPI.FormatRequestParameters(requestParameters);

//                string requestText = payWayAPI.FormatRequestParameters(requestParameters);

//                //Trace.TraceInformation(requestText);

//                string responseText = payWayAPI.ProcessCreditCard(requestText);


//                //Trace.TraceInformation(responseText);
//                List<string> responseparametersList = new List<string>();
//                responseparametersList = responseText.Split('&').ToList();
//                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
//                foreach (string s in responseparametersList)
//                {
//                    string field = s.Substring(0, s.IndexOf('='));
//                    string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
//                    responseparameters.Add(field, value);
//                }


//                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
//                {

//                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
//                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
//                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
//                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
//                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
//                    resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
//                    resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
//                    resp.OrderNumber = TransactionNumber;
//                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";

//                    if (responseparameters["response.summaryCode"] == "2") //if transaction erred, reverse it
//                    {

//                        BackgroundWorker bgwReverseTransaction = new BackgroundWorker();
//                        bgwReverseTransaction.DoWork += new DoWorkEventHandler(bgwReverseOrignalTransaction_DoWork);
//                        ReverseMOTOTran reversetran = new ReverseMOTOTran();
//                        reversetran.OriginalOrderNumber = TransactionNumber;
//                        reversetran.username = TranToProcess.username;
//                        bgwReverseTransaction.RunWorkerAsync(reversetran);

//                        resp.Approved = false;
//                        resp.ResponseCode = "91";
//                        resp.ResponseText = "Transaction Process Erred. Trasaction is reversed";
//                    }

//#if DEBUG
//                    Trace.TraceInformation("Insert to Phoenix");
//#endif
//                    //Write into phoenix database
//                    BackgroundWorker bgwInsertTransaction = new BackgroundWorker();
//                    bgwInsertTransaction.DoWork += new DoWorkEventHandler(bgwInsertTransaction_DoWork);
//                    bgwInsertTransaction.RunWorkerAsync(resp);
//                }
//                else
//                {
//                    resp.Approved = false;
//                    resp.ResponseCode = "96";
//                    resp.ResponseText = "Unknown Error from WBC API";
//                }
//                //return resp;
//            }
//            catch (Exception ex)
//            {
//                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
//                if (ex is PhoenixExceptions)
//                {
//                    resp.Approved = false;
//                    resp.ResponseCode = "96";
//                    resp.ResponseText = ex.Message;
//                }
//                //resp.Approved = false;
//                //resp.ResponseCode = "";
//                //resp.ResponseText = "Unknown Error";
//            }
//            finally
//            {

//            }
//            return resp;
//        }

        private MOTOTranResponse ReverseTransaction(ReverseMOTOTran reversetran)
        {
            string terminalID = "";
            string TradingName = "";
            string ABN = "";
            string MerchantID = "";
            string MerchantName = "";
            MOTOTranResponse resp = new MOTOTranResponse();
            try
            {
                if (String.IsNullOrEmpty(reversetran.username))
                {
                    throw new InvalidUsernameException();
                }
                else
                {
                    int member_key = 0;
                    string APIKey = "";
                    terminalID = GetVirtualTerminalID(reversetran.username, ref TradingName, ref ABN, ref MerchantID, ref MerchantName, ref member_key, ref APIKey );
                    if (string.IsNullOrEmpty(terminalID))
                    {
                        throw new MerchantNotFoundException();
                    }
                    else
                    {

                        resp.TradingName = TradingName;
                        resp.ABN = ABN;
                        resp.MerchantID = MerchantID;
                        resp.MerchantName = MerchantName;
                        resp.TerminalID = terminalID;
                    }
                }

                
                PayWayAPI payWayAPI = new PayWayAPI();

                String initParams = @"certificateFile=C:\\Users\\Gali7_000\\Documents\\LiveGroup\\Resources\\ccapi.q0" + "&" +
                                    @"logDirectory=LOG_DIR";

                initParams = @"certificateFile=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayCertificate + @"&logDirectory=" + PhoenixWCFService.Properties.Settings.Default.WBCPaywayLogDir;
                payWayAPI.Initialise(initParams);


                string TransactionNumber = GetNextVirtualTransactionNumber(terminalID);

                Dictionary<string, string> requestParameters = new Dictionary<string, string>();
                requestParameters.Add("customer.username", PayWayUsername);
                requestParameters.Add("customer.password", PayWayPassword);
                requestParameters.Add("customer.merchant", MerchantID);
                requestParameters.Add("order.type", "reversal");
                
                requestParameters.Add("customer.orderNumber", TransactionNumber);
                requestParameters.Add("order.ECI", "SSL");
                requestParameters.Add("customer.originalOrderNumber", reversetran.OriginalOrderNumber);
#if DEBUG
                Trace.TraceInformation(string.Format("Reverse transaction. OriginalTransactionNumber: {0}. New TransactionNumber: {1}.", reversetran.OriginalOrderNumber, TransactionNumber));
#endif


                string requestText = payWayAPI.FormatRequestParameters(requestParameters);
#if DEBUG
                Trace.TraceInformation(requestText);
#endif

                bool reversalapproved = false;
                Dictionary<string, string> responseparameters = new Dictionary<string, string>();
                List<string> responseparametersList = new List<string>();
                int retries = 0;
                while (!reversalapproved && retries < 10)
                {
                    if (retries != 0)
                        Thread.Sleep(60000); //wait 60 seconds to retry
                    string responseText = payWayAPI.ProcessCreditCard(requestText);

#if DEBUG
                    Trace.TraceInformation(responseText);
#endif
                    responseparameters = new Dictionary<string, string>();
                    responseparametersList = responseText.Split('&').ToList();

                    foreach (string s in responseparametersList)
                    {
                        string field = s.Substring(0, s.IndexOf('='));
                        string value = s.Substring(s.IndexOf('=') + 1, s.Length - (s.IndexOf('=') + 1));
                        responseparameters.Add(field, value);
                    }
                    if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                    {
                        if (responseparameters["response.summaryCode"] == "2" ||
                            responseparameters["response.responseCode"] == "QE" ||
                            responseparameters["response.responseCode"] == "QI" ||
                            responseparameters["response.responseCode"] == "QX")
                        {
                            reversalapproved = false;
                        }
                        else
                            reversalapproved = true;
                    }
                    retries++;
                }
                if (responseparameters.Count > 0 && responseparameters.ContainsKey("response.summaryCode"))
                {

                    resp.Approved = responseparameters.ContainsKey("response.summaryCode") ? responseparameters["response.summaryCode"] == "0" : false;
                    resp.ResponseCode = responseparameters.ContainsKey("response.responseCode") ? responseparameters["response.responseCode"] : "96";
                    resp.ResponseText = responseparameters.ContainsKey("response.text") ? responseparameters["response.text"] : "Unknown";
                    resp.ReceiptNumber = responseparameters.ContainsKey("response.receiptNo") ? responseparameters["response.receiptNo"] : "";
                    resp.TransactionTime = responseparameters.ContainsKey("response.transactionDate") ? responseparameters["response.transactionDate"] : "";
                    resp.CardScheme = responseparameters.ContainsKey("response.cardSchemeName") ? responseparameters["response.cardSchemeName"] : "";
                    resp.CreditGroup = responseparameters.ContainsKey("response.creditGroup") ? responseparameters["response.creditGroup"] : "";
                    string AuthID = responseparameters.ContainsKey("response.authid") ? responseparameters["response.authid"] : "";

                }
                else
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = "Unknown Error from WBC API";
                }


            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error processing WBC online transaction for Virtual TID: {0}. Error message: {1}", terminalID, ex.Message));
                if (ex is PhoenixExceptions)
                {
                    resp.Approved = false;
                    resp.ResponseCode = "96";
                    resp.ResponseText = ex.Message;
                }

            }
            finally
            {

            }
            return resp;
        }
        private int AddCustomer(MerchantCustomer customer)
        {
            int CustomerKey = 0;
            try{
       
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    CustomerKey = 0;
                    Member m = entities.Members.FirstOrDefault(r => r.Member_key == customer.MerchantMemberKey);
                    //check if customer already exist
                    if (!String.IsNullOrEmpty(customer.Email))
                    {
                        Customer c = null;
                        if (m.Customers != null && m.Customers.Count >0)
                            c = m.Customers.FirstOrDefault(r => r.Email == customer.Email);
                        if (c == null)
                        {
                            //customer email doesn't exist. Add new customer and return customerkey
                            Customer nCustomer = new Customer();
                            nCustomer.TradingName = customer.Tradingname;
                            nCustomer.FirstName = customer.FirstName;
                            nCustomer.LastName = customer.LastName;
                            nCustomer.Email = customer.Email;
                            nCustomer.Mobile = customer.Mobile;
                            nCustomer.Telephone = customer.Phone;
                            nCustomer.DOB = customer.DateOfBirth;
                            //add primary address
                            if (customer.PrimaryAddress !=null && customer.PrimaryAddress.StreetName != "" && customer.PrimaryAddress.StreetNumber!="")
                            {
                                nCustomer.CustomerAddress = new CustomerAddress();
                                nCustomer.CustomerAddress.StreetNumber = customer.PrimaryAddress.StreetNumber;
                                nCustomer.CustomerAddress.StreetName = customer.PrimaryAddress.StreetName;
                                nCustomer.CustomerAddress.Postcode = customer.PrimaryAddress.PostCode;
                                if (customer.PrimaryAddress.Suburb!="")
                                {
                                    //try to get matching suburb from database using given Postcode and suburb name
                                    Suburb sub = entities.Suburbs.FirstOrDefault(r => r.PostCodeMaster.PostCode == customer.PrimaryAddress.PostCode && r.Suburb1.ToUpper() == customer.PrimaryAddress.Suburb.ToUpper());
                                    if (sub != null)
                                        nCustomer.CustomerAddress.Suburb = sub;
                                }
                                nCustomer.CustomerAddress.Modified_dttm = DateTime.Now;
                                nCustomer.CustomerAddress.ModifiedByUser = "Auto Process";
                            }
                            //add primary address
                            if (customer.MailingAddress != null && customer.MailingAddress.StreetName != "" && customer.MailingAddress.StreetNumber != "")
                            {
                                nCustomer.CustomerAddress1 = new CustomerAddress();
                                nCustomer.CustomerAddress1.StreetNumber = customer.MailingAddress.StreetNumber;
                                nCustomer.CustomerAddress1.StreetName = customer.MailingAddress.StreetName;
                                nCustomer.CustomerAddress1.Postcode = customer.MailingAddress.PostCode;
                                if (customer.MailingAddress.Suburb != "")
                                {
                                    //try to get matching suburb from database using given Postcode and suburb name
                                    Suburb sub = entities.Suburbs.FirstOrDefault(r => r.PostCodeMaster.PostCode == customer.MailingAddress.PostCode && r.Suburb1.ToUpper() == customer.MailingAddress.Suburb.ToUpper());
                                    if (sub != null)
                                        nCustomer.CustomerAddress1.Suburb = sub;
                                }
                                 nCustomer.CustomerAddress1.Modified_dttm = DateTime.Now;
                                nCustomer.CustomerAddress1.ModifiedByUser = "Auto Process";
                            }
                            m.Customers.Add(nCustomer);
                            entities.SaveChanges();
                            CustomerKey = nCustomer.CustomerKey;
                        }
                        else
                        {
                            //customer email exists. return the exisint Customerkey
                            CustomerKey = c.CustomerKey;

                        }
                    }
                    else
                        throw new CustomerEmailMissingException();
                }
                return CustomerKey;
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                throw ex;
            }
            catch(Exception ex)
            {
                throw ex;
              
            }
            finally
            {
                
            }
        }

        private string CreateToken(string prefix, int length)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                string s = "";
                CustomerToken token = null;
                string tok;
                do{
                    s = prefix+GenerateRandomCustomerReferenceNumber(length);
                    s = s.Substring(0, 20);
                    token = entity.CustomerTokens.FirstOrDefault(r => r.Token == s);
                    //tok = token.Token;
                }
                while( token != null);
                CustomerToken custoken = new CustomerToken();
                custoken.Token = s;
                entity.SaveChanges();
                return s;
            }
        }
        private string AddCardToken(CardToken token, int CustomerKey, string prefix, ref bool newToken)
        {
            string Token = "";

            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                bool exist = false;

                var card = entity.CustomerCards.FirstOrDefault(r => r.CustomerToken.CustomerKey == CustomerKey && !r.CustomerToken.DeactivateDate.HasValue);
                if (card != null)
                {
                    string oldcardtoken = card.CustomerToken.Token;
                    newToken = false;
                    //there is the same card number and expiry month and year in this customer wallet
                    if (card.CardHash == token.CardHash)
                    {
                        if (!string.IsNullOrEmpty(card.CustomerToken.Token) && !string.IsNullOrWhiteSpace(card.CustomerToken.Token))
                        {
                            if (CheckTokenExistenceOnPayway(oldcardtoken))
                            {
                                throw new TokenExistedException();
                            }
                            
                        }
                        //card exists for this customer, return the existing token
                        return card.CustomerToken.Token;
                    }
                    else
                    {
                        //deactivate old card
                        DeactivateCustomerToken(oldcardtoken);
                        var customer = entity.Customers.FirstOrDefault(r => r.CustomerKey == CustomerKey);
                        Token = CreateToken(prefix, 20);
                        CustomerToken custoken = new CustomerToken();
                        custoken.Token = Token;
                        custoken.CustomerCard = new CustomerCard();
                        //custoken.CustomerCard.CardHolderName = token.CardHash;
                        custoken.CustomerCard.CardHolderName = token.CardHolderName;
                        custoken.CustomerCard.CardMasked = token.CardMasked;
                        custoken.CustomerCard.ExpiryMonth = token.ExpiryMonth;
                        custoken.CustomerCard.ExpiryYear = token.ExpiryYear;
                        custoken.CustomerCard.Modified_dttm = DateTime.Now;
                        custoken.CustomerCard.ModifiedByUser = "Auto Process";
                        custoken.CustomerCard.CardHash = token.CardHash;
                        customer.CustomerTokens.Add(custoken);
                        entity.SaveChanges();
                        customer.DefaultPaymentTokenKey = custoken.CustomerTokenKey;
                        entity.SaveChanges();

                        //throw new CreditCardExistInWalletException();
                    }

                }
                else
                {
                    //Customer hasn't got any card in his wallet.
                    newToken = true;
                    var customer = entity.Customers.FirstOrDefault(r => r.CustomerKey == CustomerKey);
                    Token = CreateToken(prefix, 20);
                    CustomerToken custoken = new CustomerToken();
                    custoken.Token = Token;
                    custoken.CustomerCard = new CustomerCard();
                    //custoken.CustomerCard.CardHolderName = token.CardHash;
                    custoken.CustomerCard.CardHolderName = token.CardHolderName;
                    custoken.CustomerCard.CardMasked = token.CardMasked;
                    custoken.CustomerCard.ExpiryMonth = token.ExpiryMonth;
                    custoken.CustomerCard.ExpiryYear = token.ExpiryYear;
                    custoken.CustomerCard.Modified_dttm = DateTime.Now;
                    custoken.CustomerCard.ModifiedByUser = "Auto Process";
                    custoken.CustomerCard.CardHash = token.CardHash;
                    customer.CustomerTokens.Add(custoken);
                    entity.SaveChanges();
                    customer.DefaultPaymentTokenKey = custoken.CustomerTokenKey;
                    entity.SaveChanges();
                }


                return Token;
            }
        }

        private bool CheckTokenExistenceOnPayway(string Token)
        {
            return false;

        }
        
    }
}
