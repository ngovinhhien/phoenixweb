﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PhoenixWCFService
{
    public abstract class PhoenixExceptions : System.Exception, ISerializable
    {
        public PhoenixExceptions()
        {
        }
        public PhoenixExceptions(string message)
            : base(message)
        {
        }
        public PhoenixExceptions(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public PhoenixExceptions(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        
    }

    public class InvalidUsernameException : PhoenixExceptions
    {
        public InvalidUsernameException()
            : base("Invalid Username")
        {
        }
    }

    public class InvalidTransactionInformationException : PhoenixExceptions
    {
        public InvalidTransactionInformationException()
            : base("Invalid Transaction Information")
        {
        }
    }
    public class InvalidCardInformationException : PhoenixExceptions
    {
        public InvalidCardInformationException()
            : base("Invalid Card Information")
        {
        }
    }
    public class CreditCardExistInWalletException : PhoenixExceptions
    {
        public CreditCardExistInWalletException()
            : base("Different Credit Card Exist in Wallet")
        {
        }
    }
    public class InvalidCVNInformationException : PhoenixExceptions
    {
        public InvalidCVNInformationException()
            : base("Invalid CVN Information")
        {
        }
    }
    public class InvalidAmountInformationException : PhoenixExceptions
    {
        public InvalidAmountInformationException()
            : base("Invalid Amount Information")
        {
        }
    }
    public class MerchantNotFoundException : PhoenixExceptions
    {
        public MerchantNotFoundException()
            : base("MerchantID not found")
        {
        }
    }
    public class InvalidCurrencyInformationException : PhoenixExceptions
    {
        public InvalidCurrencyInformationException()
            : base("Invalid Currency Information")
        {
        }
    }

    public class InvalidExpiryDateException : PhoenixExceptions
    {
        public InvalidExpiryDateException()
            : base("Invalid Expiry Date")
        {
        }
    }

    public abstract class TerminalAllocationException : PhoenixExceptions
    {
        public TerminalAllocationException()
        { }
        public TerminalAllocationException(string message)
            : base(message)
        {
        }
        public TerminalAllocationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public TerminalAllocationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
    public class OldTerminalNotFoundException : TerminalAllocationException
    {
        public OldTerminalNotFoundException()
            : base("Old Terminal Not Found")
        {
        }
    }
    public class NewTerminalNotFound : TerminalAllocationException
    {
        public NewTerminalNotFound()
            : base("New Terminal Not Found")
        {
        }
    }
    
    public class TerminalNotAllocatedException : TerminalAllocationException
    {
        public TerminalNotAllocatedException()
            : base("Terminal Is Not Allocated")
        {
        }
    }
    public class CustomerRefNotFound : PhoenixExceptions
    {
        public CustomerRefNotFound(): base("Customer Reference Number Not Found")
        {

        }
    }

    public class RegisterAccountException : PhoenixExceptions
    {
        public RegisterAccountException()
            : base("Error in Registering Account")
        {

        }
        public RegisterAccountException(string message)
            : base(message)
        {
        }
        public RegisterAccountException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public RegisterAccountException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
    public class CustomerEmailMissingException : PhoenixExceptions
    {
        public CustomerEmailMissingException()
            : base("Customer Email Missing")
        {

        }
    }

    public class CustomerTokenNotFoundException : PhoenixExceptions
    {
        public CustomerTokenNotFoundException()
            : base("Customer Token Not Found")
        {

        }
    }

    public class FunctionNotAvailableException : PhoenixExceptions
    {
        public FunctionNotAvailableException()
            : base("Function not available. Please contact Administrator")
        {
        }
    }

    public class TokenExistedException : PhoenixExceptions
    {
        public TokenExistedException()
            : base("System cannot register this card. Card has already been registered.")
        {
        }
    }

    public class MerchantAPIKeyNotFoundException : PhoenixExceptions
    {
        public MerchantAPIKeyNotFoundException()
            : base("Merchant API key not found")
        {
        }
    }
    public class OrderStatusException : PhoenixExceptions
    {
        public OrderStatusException()
            : base("Bad Order Status")
        {
        }
    }
}