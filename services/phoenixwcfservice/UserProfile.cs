//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhoenixWCFService
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserProfile
    {
        public UserProfile()
        {
            this.UserProfile1 = new HashSet<UserProfile>();
        }
    
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string MemberId { get; set; }
        public string TerminalId { get; set; }
        public bool Inactive { get; set; }
        public bool IsAdmin { get; set; }
        public Nullable<int> UserMembershipKeySelected { get; set; }
        public Nullable<int> UserIdSelected { get; set; }
        public Nullable<int> UserIdCreatedBy { get; set; }
        public Nullable<int> UserIdUpdatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string APIKey { get; set; }
    
        public virtual ICollection<UserProfile> UserProfile1 { get; set; }
        public virtual UserProfile UserProfile2 { get; set; }
    }
}
