﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Collections;
using Live.Util.Common;
namespace PhoenixWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPhoenixWCFService
    {

        //[OperationContract]
        //bool SendSMSReceipt(string mobilenumber, ReceiptPictureToSend ReceiptToSend);

        //[OperationContract]
        //Int64 UploadTransaction(CadmusIN Docket);

        [OperationContract]
        Dictionary<Int64, Int64> UploadTransactions(List<CadmusIN> Docket);

        [OperationContract]
        string GetCurrentCustomerPaymentType(string TerminalID);

        [OperationContract]
        bool VerifyPhoenixLogin(string username, string password);

        [OperationContract]
        float GetSurcharge(string username, string FirstFourDigits);
        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
        //[OperationContract]
        //string GetVirtualTerminalID(int MemberKey);

        [OperationContract]
        MOTOTranResponse ProcessTransaction(MOTOTran TranToProcess);

        [OperationContract]
        MOTOTranResponse RefundTransaction(MOTOTran TranToProcess);

        //[OperationContract]
        //void UploadWBCAPITransaction(WBCAPIResponse response);

        [OperationContract]
        bool SendReceiptViaSMS(string MobileNumber, string merchantid, string ordernumber);

        [OperationContract]
        bool SendReceiptViaEmail(string Email, string merchantid, string ordernumber);

        [OperationContract]
        TransactionHistory GetTransactionDetail(string username, string ordernumber);

        [OperationContract]
        List<TransactionHistory> GetTransactionHistory(string username);

        [OperationContract]
        List<TransactionPayment> GetTransactionPayments(string username, DateTime StartDate, DateTime EndDate);

        [OperationContract]
        bool SwapTerminal(string oldTerminalID, string newTerminalID, DateTime EffectiveDate);

        [OperationContract]
        bool ReturnTerminal(string oldTerminalID, DateTime EffectiveDate);
        [OperationContract]
        bool NewAllocation(string terminalID, string CustomerMobileNumber, string DriverLicenseTrack1);
        [OperationContract]
        bool UpdateInformation(string TerminalID, string CustomerMobileNumber, string DriverLicense);

        //[OperationContract]
        //RegisterAccountResponse RegisterToken(MOTOTran accountToRegister);

        //[OperationContract]
        //MOTOTranResponse ProcessCardlessTransaction(CardlessTran TranToProcess);

        [OperationContract]
        MOTOTranResponse ProcessTransactionWithToken(TokenTran TranToProcess);

        [OperationContract]
        RegisterAccountResponse RegisterToken(MOTOTran TokenToAdd);
        [OperationContract]
        MOTOTranResponse ProcessTransactionEncrypted(MOTOTran TranToProcess);

        [OperationContract]
        RegisterAccountResponse DeregisterAccount(DeregisteredAccount accountToDeregister);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    //[DataContract]
    //public class ReceiptPictureToSend
    //{
    //    string _TaxiID;

    //    [DataMember]
    //    public string TaxiID
    //    {
    //        get { return _TaxiID; }
    //        set { _TaxiID = value; }
    //    }
    //    string _DriverID;

    //    [DataMember]
    //    public string DriverID
    //    {
    //        get { return _DriverID; }
    //        set { _DriverID = value; }
    //    }
    //    string _Transactionnumber;

    //    [DataMember]
    //    public string Transactionnumber
    //    {
    //        get { return _Transactionnumber; }
    //        set { _Transactionnumber = value; }
    //    }
    //    string _Pickuplocation;

    //    [DataMember]
    //    public string Pickuplocation
    //    {
    //        get { return _Pickuplocation; }
    //        set { _Pickuplocation = value; }
    //    }
    //    string _DropoffLocation;

    //    [DataMember]
    //    public string DropoffLocation
    //    {
    //        get { return _DropoffLocation; }
    //        set { _DropoffLocation = value; }
    //    }
    //    decimal _Fare;

    //    [DataMember]
    //    public decimal Fare
    //    {
    //        get { return _Fare; }
    //        set { _Fare = value; }
    //    }
    //    decimal _ServiceFee;

    //    Dictionary<string, decimal> _Amounts;
    //    [DataMember]
    //    public Dictionary<string, decimal> Amounts
    //    {
    //        get { return _Amounts; }
    //        set { _Amounts = value; }
    //    }

    //    [DataMember]
    //    public decimal ServiceFee
    //    {
    //        get { return _ServiceFee; }
    //        set { _ServiceFee = value; }
    //    }
    //    decimal _GSTOnServiceFee;

    //    [DataMember]
    //    public decimal GSTOnServiceFee
    //    {
    //        get { return _GSTOnServiceFee; }
    //        set { _GSTOnServiceFee = value; }
    //    }
    //    decimal _Total;

    //    [DataMember]
    //    public decimal Total
    //    {
    //        get { return _Total; }
    //        set { _Total = value; }
    //    }
    //    string _TerminalID;

    //    [DataMember]
    //    public string TerminalID
    //    {
    //        get { return _TerminalID; }
    //        set { _TerminalID = value; }
    //    }
    //    string _MerchantID;

    //    [DataMember]
    //    public string MerchantID
    //    {
    //        get { return _MerchantID; }
    //        set { _MerchantID = value; }
    //    }
    //    string _Batch;

    //    [DataMember]
    //    public string Batch
    //    {
    //        get { return _Batch; }
    //        set { _Batch = value; }
    //    }
    //    string _Inv;

    //    [DataMember]
    //    public string Inv
    //    {
    //        get { return _Inv; }
    //        set { _Inv = value; }
    //    }
    //    string _CardName;

    //    [DataMember]
    //    public string CardName
    //    {
    //        get { return _CardName; }
    //        set { _CardName = value; }
    //    }
    //    string _CardMask;

    //    [DataMember]
    //    public string CardMask
    //    {
    //        get { return _CardMask; }
    //        set { _CardMask = value; }
    //    }
    //    string _ExpiryDate;

    //    [DataMember]
    //    public string ExpiryDate
    //    {
    //        get { return _ExpiryDate; }
    //        set { _ExpiryDate = value; }
    //    }
    //    string _AuthID;

    //    [DataMember]
    //    public string AuthID
    //    {
    //        get { return _AuthID; }
    //        set { _AuthID = value; }
    //    }
    //    string _RRN;

    //    [DataMember]
    //    public string RRN
    //    {
    //        get { return _RRN; }
    //        set { _RRN = value; }
    //    }
    //    string _STAN;

    //    [DataMember]
    //    public string STAN
    //    {
    //        get { return _STAN; }
    //        set { _STAN = value; }
    //    }
    //    string _TranDateTime;

    //    [DataMember]
    //    public string TranDateTime
    //    {
    //        get { return _TranDateTime; }
    //        set { _TranDateTime = value; }
    //    }
    //    string _CardType;

    //    [DataMember]
    //    public string CardType
    //    {
    //        get { return _CardType; }
    //        set { _CardType = value; }
    //    }
    //    decimal _SaleTotal;

    //    [DataMember]
    //    public decimal SaleTotal
    //    {
    //        get { return _SaleTotal; }
    //        set { _SaleTotal = value; }
    //    }




    //}

    //[DataContract]
    //public class WBCAPIResponse
    //{
    //    public int SummaryCode { get; set; }
    //    public string ResponseCode { get; set; }
    //    public string ResponseText { get; set; }
    //    public string SettlementDate { get; set; }
    //    public string ReceiptNo { get; set; }
    //    public string CardSchemeName { get; set; }
    //    public string CreditGroup { get; set; }
    //    public string CVNResponse { get; set; }
    //    public DateTime TransactionDate {get;set;}
    //    public string AuthID { get; set; }
    //}
    [DataContract]
    public enum TranType 
    {
        [StringValue("capture")]
        Capture ,

        [StringValue("refund")]
        Refund,

        [StringValue("query")]
        Query,

        [StringValue("echo")]
        Echo,

        [StringValue("preauth")]
        Preauth,

        [StringValue("captureWithoutAuth")]
        CaptureWithoutAuth,

        [StringValue("reversal")]
        Reversal,

        [StringValue("registerAccount")]
        RegisterAccount,

        [StringValue("deregisterAccount")]
        DeregisterAccount,

        [StringValue("BankAccount")]
        BankAccount
    }

    public interface IPayWayChargeTran
    {
        decimal Amount { get; set; }
        decimal Surcharge { get; set; }

        decimal GSTOnSurcharge { get; set; }

        string Description { get; set; }

        

        string card_currency { get; set; }
    }

    public interface IPayWayCardInfo
    {
        string cardNumber { get; set; }
        int cardExpiryYear { get; set; }
        int cardExpiryMonth { get; set; }
        string cardholdername { get; set; }
    }

    public interface IRefundTransaction
    {
        string OriginalOrderNumber { get; set; }
    }

     [DataContract]
    public abstract class PayWayOrder
    {
         protected TranType TransactionType { get; set; }

         //[DataMember]
         public string GetTranType()
         {
             
             return StringEnum.GetStringValue(this.TransactionType);
              
         }

         [DataMember]
         public string IPAddress
         {
             get;
             set;
         }

         [DataMember]
         public string username { get; set; }

         [DataMember]
         public string OriginalOrderNumber { get; set; }
         [DataMember]
         public string OrderNumber { get; set; }

         [DataMember]
         public decimal Amount { get; set; }

         [DataMember]
         public decimal Surcharge { get; set; }

         [DataMember]
         public decimal GSTOnSurcharge { get; set; }

         [DataMember]
         public string Description { get; set; }

         [DataMember]
         public string card_currency { get; set; }
 
    }

     [DataContract]
     public abstract class PayWayTran : PayWayOrder, IPayWayCardInfo
     {

         [DataMember]
         public string cardNumber { get; set; }

         [DataMember]
         public int cardExpiryYear { get; set; }
         [DataMember]
         public int cardExpiryMonth { get; set; }

         [DataMember]
         public string cardholdername { get; set; }

         public string HashString
         {
             get
             {
                 string hash = "";
                 try
                 {
                     string card = this.cardNumber + cardExpiryMonth.ToString() + cardExpiryYear;
                     hash = LiveEncryption.LiveEncryptionHelper.EncryptPassword(card);
                     return hash;
                 }
                 catch
                 {
                     return hash;
                 }
             }
         }
         //[DataMember]
         //public string OrderNumber { get; set; }

     

         //[DataMember]
         //public string cardVerificationNumber { get; set; }

         //public string CardMasked
         //{
         //    get
         //    {
         //        string cardMasked = "";
         //        try
         //        {
         //            if (!String.IsNullOrEmpty(cardNumber))
         //            {
         //                if (cardNumber.Length >= 6)
         //                    cardMasked = cardNumber.Substring(0, 6) + "******" + cardNumber.Remove(0, cardNumber.Length - 4);
         //                else
         //                    cardMasked = cardNumber + "******";
         //            }
                     
         //        }
         //        catch
         //        {
                     
         //        }
         //        return cardMasked;
         //    }
         //}
         
       

     }

    [DataContract]
     public abstract class Tran : PayWayTran
    {
     
        [DataMember]
        public string cardVerificationNumber { get; set; }

        
    }

    [DataContract]
    public class CrediCardAccount : PayWayTran,IPayWayCardInfo
    {
        public CrediCardAccount()
        {
            this.TransactionType = PhoenixWCFService.TranType.RegisterAccount;
        }
        //[DataMember]
        //public string CustomerReferenceNumber { get; set; }
    }

    [DataContract]
    public class CardlessTran : PayWayOrder, IPayWayChargeTran
    {
        public CardlessTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Capture;
        }
        [DataMember]
        public string CustomerReferenceNumber { get; set; }

        //[DataMember]
        //public decimal Amount { get; set; }
        //[DataMember]
        //public decimal Surcharge { get; set; }

        //[DataMember]
        //public decimal GSTOnSurcharge { get; set; }

        //[DataMember]
        //public string Description { get; set; }

        //[DataMember]
        //public string OriginalOrderNumber { get; set; }

        //[DataMember]
        //public string card_currency { get; set; }
    }

    [DataContract]
    public class TokenTran : PayWayOrder
    {
        public TokenTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Capture;
        }
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public bool Preauth { get; set; }

    }

    [DataContract]
    public class PreAuth : Tran
    {
        public PreAuth()
        {
            this.TransactionType = PhoenixWCFService.TranType.Preauth;
        }
       
    }

    [DataContract]
    public class CaptureWithoutAuth : MOTOTran
    {
        public CaptureWithoutAuth()
        {
            this.TransactionType = PhoenixWCFService.TranType.CaptureWithoutAuth;
        }
        [DataMember]
        public string AuthID { get; set; }
    }
    [DataContract]
    public class MOTOTran : Tran, IPayWayChargeTran
    {
        public MOTOTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Capture;
        }
        
       
        //[DataMember]
        //public decimal orderAmount { get; set; }


        //[DataMember]
        //public decimal Amount { get; set; }
        //[DataMember]
        //public decimal Surcharge { get; set; }

        //[DataMember]
        //public decimal GSTOnSurcharge { get; set; }

        //[DataMember]
        //public string Description { get; set; }

        

        //[DataMember]
        //public string card_currency { get; set; }

        //[DataMember]
        //public bool SaveCustomer { get; set; }

        [DataMember]
        public MerchantCustomer Customer { get; set; }
    }
    [DataContract]
    public class RefundMotoTran : MOTOTran, IRefundTransaction
    {
        public RefundMotoTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Refund;
        }

        [DataMember]
        new public string OriginalOrderNumber { get; set; }
    }
    [DataContract]
    public class EchoTran : PayWayOrder
    {
        public EchoTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Echo;
        }
    }
    [DataContract]
    public class ReverseMOTOTran :PayWayOrder,IRefundTransaction
    {
        public ReverseMOTOTran()
        {
            this.TransactionType = PhoenixWCFService.TranType.Reversal;
        }

        //[DataMember]
        //public string OriginalOrderNumber { get; set; }
    }

    [DataContract]
    public class BankAccountDebit : PayWayOrder
    {
        public BankAccountDebit()
        {
            this.TransactionType = PhoenixWCFService.TranType.BankAccount;
        }

        //[DataMember]
        //public decimal Amount { get; set; }
        //[DataMember]
        //public decimal Surcharge { get; set; }

        //[DataMember]
        //public decimal GSTOnSurcharge { get; set; }

        //[DataMember]
        //public string Description { get; set; }

        //[DataMember]
        //public string OriginalOrderNumber { get; set; }

        //[DataMember]
        //public string card_currency { get; set; }

        [DataMember]
        public bool SaveCustomer { get; set; }

        [DataMember]
        public MerchantCustomer Customer { get; set; }

        [DataMember]
        public AccountToken BankAccount { get; set; }
    }

    [DataContract]
    public abstract class PayWayResponse
    {
        [DataMember]
        public string ResponseCode { get; set; }
        [DataMember]
        public string ResponseText { get; set; }
        [DataMember]
        public string ReceiptNumber { get; set; }
        [DataMember]
        public bool Approved { get; set; }
        [DataMember]
        public string TransactionTime { get; set; }

        [DataMember]
        public string UserName { get; set; }
      
    }
    [DataContract]
    public class MOTOTranResponse :PayWayResponse
    {
    
        //[DataMember]
        //public CVNResponseCode CVNResponse { get; set; }
        [DataMember]
        public string CardScheme { get; set; }
      
        
        [DataMember]
        public string OrderNumber { get; set; }
        [DataMember]
        public string CreditGroup { get; set; }
        [DataMember]
        public string AuthID { get; set; }
        [DataMember]
        public string CardMasked { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public decimal Surcharge { get; set; }

        [DataMember]
        public decimal GSTOnSurcharge { get; set; }

        [DataMember]
        public string TradingName { get; set; }

        [DataMember]
        public string ABN { get; set; }

        [DataMember]
        public string MerchantID { get; set; }
        
        [DataMember]
        public string MerchantName { get; set; }
        [DataMember]
        public string TerminalID { get; set; }
        [DataMember]
        public string CardHolderName { get; set; }

        [DataMember]
        public string Token { get; set; }
    }



    //[DataContract]
    //public enum CVNResponseCode{
    //    Matched,
    //    NotMatched,
    //    NotProcessed,
    //    Suspicious,
    //    Unknown
    //}

    [DataContract]
    public class TransactionHistory : MOTOTranResponse
    {
        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public Decimal Total { get; set; }

        [DataMember]
        public DateTime TransactionDateTime { get; set; }
    }

    [DataContract]
    public class TransactionPayment 
    {
        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public Decimal TotalPayment { get; set; }

        [DataMember]
        public string PaymentReference { get; set; }
    }

    [DataContract]
    public class RegisterAccountResponse : PayWayResponse
    {
        [DataMember]
        public string CardMasked { get; set; }

        [DataMember]
        public string CardToken { get; set; }

        //[DataMember]
        public int CustomerKey { get; set; }
    }

    [DataContract]
    public class DeregisteredAccount : PayWayOrder
    {
        public DeregisteredAccount()
        {
            this.TransactionType = PhoenixWCFService.TranType.DeregisterAccount;
        }
        //[DataMember]
        //public string CustomerReferenceNumber { get; set; }

        //[DataMember]
        //public string CustomerKey { get; set; }

        [DataMember]
        public string CardToken { get; set; }
        
    }

    [DataContract]
    [Serializable]
    public class MerchantCustomerAddress
    {
        [DataMember]
        public string StreetNumber { get; set; }

        [DataMember]
        public string StreetName { get; set; }

        [DataMember]
        public string Suburb { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string PostCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MerchantCustomer
    {
        public MerchantCustomer()
        {
            Wallets = new List<Token>();
            PrimaryAddress = new MerchantCustomerAddress();
            MailingAddress = new MerchantCustomerAddress();
        }

        [DataMember]
        public string Tradingname { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public MerchantCustomerAddress PrimaryAddress { get; set; }
        [DataMember]
        public MerchantCustomerAddress MailingAddress { get; set; }

        [DataMember]
        public List<Token> Wallets { get; set; }

        [DataMember]
        public Token DefaultPaymentToken { get; set; }

        public string MerchantUserName { get; set; }

        public int MerchantMemberKey { get; set; }
    }

    public interface IPaymentToken
    {
        //public string LastFourDigits;
    }
    [DataContract]
    [Serializable]
    public abstract class Token
    {
        [DataMember]
        public int TokenKey { get; set; }

        [DataMember]
        public MerchantCustomer Customer { get; set; }
    }
    [DataContract]
    [Serializable]
    public class CardToken :Token ,IPaymentToken
    {
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string CardMasked{get;set;}
        [DataMember]
        public int ExpiryMonth {get;set;}
        [DataMember]
        public int ExpiryYear {get;set;}
        [DataMember]
        public string LastFourDigits
        {
            get
            {
                return CardMasked.Trim().Substring(CardMasked.Length - 4, 4);
            }
        }
        [DataMember]
         public string FirstSixDigits
        {
            get
            {
                return CardMasked.Trim().Substring(0, 6);
            }
        }
        [DataMember]
        public string CardHolderName { get; set; }

        public string CardHash { get; set; }
    }
    [DataContract]
    [Serializable]
    public class AccountToken: Token,IPaymentToken
    {
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public string LastFourDigits
        {
            get
            {
                return AccountNumber.Trim().Substring(AccountNumber.Length - 4, 4);
            }
        }
    }

    [DataContract]
    public class MerchantService
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }

    [DataContract]
    [Serializable]
    public class Membership
    {
        public MerchantCustomer Customer { get; set; }
        public MerchantService Service { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public BillingCycle BillingCycle { get; set; }
        public decimal PaymentAmount { get; set; }
    }
    
    public abstract class BillingCycle
    {
        public BillingCycleType CycleType { get; set; }
        public int DaysInterval{get;set;}
        public int WeeksInterval{get;set;}
        public int MonthsInterval{get;set;}
        public int DaysOfWeek{get;set;}
        public int DaysOfMonth{get;set;}
    }

    public class OneOffCharge :BillingCycle
    {
        public OneOffCharge()
        {
            this.CycleType = BillingCycleType.OneOff;
        }
    }

    public class DailyCharge : BillingCycle
    {
        public DailyCharge()
        {
            this.CycleType = BillingCycleType.Daily;
            this.DaysInterval = 1;
        }
    }

    public class WeeklyCharge : BillingCycle
    {
        public WeeklyCharge(DayOfWeek DayOfWeekToCharge)
        {
            this.CycleType = BillingCycleType.Weekly;
            this.WeeksInterval = 1;
            this.DaysOfWeek = (int) DayOfWeekToCharge+1;
        }
    }

    [DataContract]
    public enum BillingCycleType
    {
        [StringValue("OneOff")]
        OneOff,

        [StringValue("Daily")]
        Daily,

        [StringValue("Weekly")]
        Weekly,
        [StringValue("Fortnightly")]
        Fortnightly,

        [StringValue("Monthly")]
        Monthly,

        [StringValue("Quarterly")]
        Quarterly,
        [StringValue("HalfYearly")]
        HalfYearly,
        [StringValue("Yearly")]
        Yearly
     

        
    }

  
    //[DataContract]
    //public enum DaysOfWeek
    //{
    //    [StringValue("OneOff")]
    //    OneOff,

    //    [StringValue("Daily")]
    //    Daily,

    //    [StringValue("Weekly")]
    //    Weekly,
    //    [StringValue("Fortnightly")]
    //    Fortnightly,

    //    [StringValue("Monthly")]
    //    Monthly,

    //    [StringValue("Quarterly")]
    //    Quarterly,
    //    [StringValue("HalfYearly")]
    //    HalfYearly,
    //    [StringValue("Yearly")]
    //    Yearly



    //}
}
