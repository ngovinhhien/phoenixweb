﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;

namespace PhoenixWCFService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class LiveATMWCFService : ILiveATMWCFService
    {
        public bool RegisterATMUser(string TerminalID, string CardTrack, string MobileNumber, string PIN, ref string ErrorReason)
        {
            bool success = false;

            //if (ValidateCardTrack(TerminalID, CardTrack))
            //{
            //    success = false;
            //    ErrorReason = "You has registered a different card";
            //}

            using(TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                ATMCheckTerminalID_Result result = entities.ATMCheckTerminalID(TerminalID).FirstOrDefault();
                if(result != null) //existing card registered with this customer
                {
                    if (!string.IsNullOrEmpty(result.CardData))
                    {
                        success = false;
                        ErrorReason = "You has registered a different card";
                    }
                    else
                    {
                        if (result.Mobile == MobileNumber.Replace(" ", "")) //match mobile number then create a new member card entry
                        {
                            Member m = entities.Members.Where(r => r.Member_key == result.member_key).FirstOrDefault();
                            if (m != null)
                            {
                                MemberCard card = new MemberCard();
                                card.Member_key = result.member_key;
                                card.CardData = CardTrack;
                                card.CreatedDateTime = DateTime.Now;
                                card.UpdatedDateTime = DateTime.Now;
                                card.CreatedByUser = "ATM";
                                card.UpdatedByUser = "ATM";
                                card.PIN = PIN;


                                MemberAccount acc = new MemberAccount();
                                m.MemberAccounts.Add(acc);

                                entities.MemberCards.AddObject(card);
                                entities.SaveChanges();
                                success = true;
                            }
                            else
                                success = false;

                        }
                        else //mobile number doesn't match
                        {
                            success = false;
                            ErrorReason = "Your mobile number is different from what is on your member account. Please contact our office";
                        }
                    }
                }
                else //no member with this terminal ID
                {
                    success = false;
                    ErrorReason = "Record not found. Please contact our office";
                }

            }
            return success;
        }

        public MemberInfo ValidateCardTrack(string CardTrack, string PIN, string TerminalID = "")
        {
            MemberInfo mem = null;
            bool success = false;

            using(TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                if (String.IsNullOrEmpty(CardTrack))
                    CardTrack = "";
                ATMCheckCardData_Result result =  entities.ATMCheckCardData(CardTrack, TerminalID).FirstOrDefault();
                if (result != null)
                {
                    success = result.PIN == PIN;
                    if (success)
                    {
                        mem = new MemberInfo();
                        mem.Firstname = result.FirstName;
                        mem.LastName = result.LastName;
                        mem.MemberID = result.MemberID;
                        mem.TradingName = result.TradingName;
                        mem.MemberKey = result.Member_key.GetValueOrDefault(0);
                        mem.Balance = result.Balance;
                        mem.MemberAccountKey = result.MemberAccountKey.GetValueOrDefault(0);
                    }
                }
                else
                    success = false;

            }
            return mem;

        }

        public Withdraw GetWithdrawDockets(int memberKey, decimal withdrawAmount)
        {
            Withdraw withdraw = new Withdraw();
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {

                    List<ATMGetDockets_Result> results = entities.ATMGetDockets(memberKey).ToList();

                    foreach (var item in results)
                    {
                        //if (withdraw.TotalPayable < withdrawAmount)
                        //{
                            withdraw.MemberID = item.MemberId;
                            Dockets doc = new Dockets();
                            doc.TerminalID = item.terminalid;
                            doc.BatchNumber = item.batchnumber;
                            doc.BatchID = item.BatchId;
                            doc.FaceValue = item.FaceValue.GetValueOrDefault(0);
                            doc.Commission = item.Commission.GetValueOrDefault(0);
                            doc.Payable = item.FaceValue.GetValueOrDefault(0) + item.Commission.GetValueOrDefault(0);
                            withdraw.TotalPayable += doc.Payable;
                            withdraw.TradingName = item.TradingName;
                            if (doc.Payable>0)
                                withdraw.DocketList.Add(doc);
                        //}
                        //else
                        //    break;
                    }


                }
            }
            catch(Exception ex)
            {
                
            }
            return withdraw;
        }


        public void PayBatches(int memberKey, DateTime _transactionDateTime, List<string> Batches, int OfficeKey, string User, int memberaccountkey)
        {
            try
            {
                DateTime trandate = new DateTime(_transactionDateTime.Year, _transactionDateTime.Month, _transactionDateTime.Day, _transactionDateTime.Hour, _transactionDateTime.Minute, _transactionDateTime.Second);
                using (TaxiEpayEntities entity = new TaxiEpayEntities())
                {
                    string batches = String.Join(",", Batches.ToArray());
                    entity.ATMPayDocket(batches, trandate, memberKey, User, OfficeKey, memberaccountkey);
                }
                //return 0.00m;
            }
            catch(Exception ex)
            {
                
            }

        }

        public void RecordWithdraw(int memberKey, int memberaccountKey, decimal amount, string Description, DateTime TransactionDate)
        {
            try
            {
                DateTime trandate = new DateTime(TransactionDate.Year, TransactionDate.Month, TransactionDate.Day, TransactionDate.Hour, TransactionDate.Minute, TransactionDate.Second);
               
                using (TaxiEpayEntities entity = new TaxiEpayEntities())
                {
                    entity.ATMRecordWithdraw(memberKey, memberaccountKey, amount, trandate, Description);
                }
                //return 0.00m;
            }
            catch (Exception ex)
            {

            }

        }

        //public int RecordWithdraw(int memberKey, int memberaccountKey, decimal amount, string Description, DateTime TransactionDate)
        //{
        //    int ReceiptID = 0;
        //    try
        //    {
        //        DateTime trandate = new DateTime(TransactionDate.Year, TransactionDate.Month, TransactionDate.Day, TransactionDate.Hour, TransactionDate.Minute, TransactionDate.Second);

        //        using (TaxiEpayEntities entity = new TaxiEpayEntities())
        //        {
        //            var result = entity.ATMRecordTransaction(memberKey, memberaccountKey, trandate, 0, amount, Description).FirstOrDefault();
        //            if (result != null)
        //                ReceiptID = result.GetValueOrDefault(0);

        //        }
        //        //return 0.00m;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return ReceiptID;
        //}

        public int RecordWithdraw(int memberKey, int memberaccountKey, decimal amount, string Description, DateTime TransactionDate, int officeKey, string user)
        {
            int ReceiptID = 0;
            try
            {
                DateTime trandate = new DateTime(TransactionDate.Year, TransactionDate.Month, TransactionDate.Day, TransactionDate.Hour, TransactionDate.Minute, TransactionDate.Second);

                using (TaxiEpayEntities entity = new TaxiEpayEntities())
                {
                    var result = entity.ATMRecordTransaction(memberKey, memberaccountKey, trandate,officeKey, user, 0, amount, Description, null).FirstOrDefault();
                    if (result != null)
                        ReceiptID = result.GetValueOrDefault(0);

                }
                //return 0.00m;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error recording Withdraw. Error message: {0}",  ex.Message));
            }

            return ReceiptID;
        }
        
        public int RecordCashDispensed(int ReceiptID, decimal amount, string Description, DateTime TransactionDate)
        {
            int Receipt = ReceiptID;
            try
            {
                DateTime trandate = new DateTime(TransactionDate.Year, TransactionDate.Month, TransactionDate.Day, TransactionDate.Hour, TransactionDate.Minute, TransactionDate.Second);
                using (TaxiEpayEntities entity = new TaxiEpayEntities())
                {
                    var original = entity.ATMGetTransactionByReceiptID(ReceiptID).FirstOrDefault();
                    if (original != null)
                    {
                        decimal ori_amount = original.DebitAmount.GetValueOrDefault();
                        int memkey = original.Member_key.GetValueOrDefault(0);
                        int memacckey = original.MemberAccountKey.GetValueOrDefault(0);

                        if (amount < ori_amount)
                        {
                            var result = entity.ATMRecordTransaction(memkey, memacckey, trandate, original.OfficeKey, original.CreatedByUser, ori_amount - amount, 0, Description, null).FirstOrDefault();
                            if (result != null)
                                Receipt = result.GetValueOrDefault(0);
                        }
                        else if (amount > ori_amount)
                        {
                            var result = entity.ATMRecordTransaction(memkey, memacckey, trandate, original.OfficeKey, original.CreatedByUser, 0, amount - ori_amount, Description, null).FirstOrDefault();
                            if (result != null)
                                Receipt = result.GetValueOrDefault(0);
                        }
                    }

                }
                //return 0.00m;
            }
            catch (Exception ex)
            {
                Trace.TraceError(String.Format("Error recording Withdraw. Error message: {0}", ex.Message));
            }
            return Receipt;
        }


        public MemberInfo LoadMember(int member_key)
        {
            MemberInfo mem = null;
            bool success = false;

            using (TaxiEpayEntities entities = new TaxiEpayEntities())
            {
                ATMLoadMemberInfo_Result result = entities.ATMLoadMemberInfo(member_key).FirstOrDefault();
                if (result != null)
                {
                    mem = new MemberInfo();
                    mem.Firstname = result.FirstName;
                    mem.LastName = result.LastName;
                    mem.MemberID = result.MemberID;
                    mem.TradingName = result.TradingName;
                    mem.MemberKey = result.Member_key.GetValueOrDefault(0);
                    mem.Balance = result.Balance;
                }
                

            }
            return mem;
        }

        public MemberAccountBalance GetMemberAccountEntry(int memberKey)
        {
            MemberAccountBalance mab = new MemberAccountBalance();
            List<AccountEntry> accountentries = new List<AccountEntry>();
            try
            {
                using (TaxiEpayEntities entities = new TaxiEpayEntities())
                {
                    List<ATMLoadMemberAccountEntry_Result> result = entities.ATMLoadMemberAccountEntry(memberKey).ToList();
                    if (result != null)
                    {
                        mab.Balance = result.OrderByDescending(r=>r.EntryDateTime).FirstOrDefault().balanceAmount.GetValueOrDefault(0.0m);
                        foreach (var item in result)
                        {
                            AccountEntry entry = new AccountEntry();
                            entry.EntryDateTime = item.EntryDateTime;
                            entry.Description = item.Description;
                            entry.CreditAmount = item.CreditAmount;
                            entry.DebitAmount = item.DebitAmount;
                            entry.Balance = item.balanceAmount;

                            mab.AccountEntries.Add(entry);
                        }

                    }
                }
            }
            catch(Exception ex)
            {

            }
            return mab;
        }

        public bool CheckPulse()
        {
            return true;
        }
            
    }


}
