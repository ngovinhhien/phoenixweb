﻿using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using Phoenix_BusLog.Data;
using Phoenix_Service;
using PhoenixObjects.Cashing;
using PhoenixWeb.Site.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PhoenixWeb.Site.ReportingWebService;
using PhoenixObjects.Member;
using PhoenixWeb.Site.Models.ViewModel;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class CashingController : Controller
    {
        //
        // GET: /Cashing/
        public List<ScannedDocketModel> ScannedDocketList = new List<ScannedDocketModel>();

        public ActionResult Index()
        {
            Session["MemberKey"] = 0;
            Session["DocketCheckSummary"] = 0;
            Session["ModelList"] = 0;
            Session["LodgmentNo"] = 0;
            Session["Paid"] = "false";
            Session["Print"] = "false";
            Session["ScannedDocketList"] = null;
            return View();
        }
        public ActionResult GenerateReport(FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.Site.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            DateTime sDate = Convert.ToDateTime(c["StartDate"]);
            DateTime eDate = Convert.ToDateTime(c["EndDate"]);
            UserDA uda = new UserDA();
            string ReportName = "/Cashing Reports/Daily Cashing Summary";

            var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            int officekey = uda.GetUserOfficeKey(uId);
            string state = uda.GetUserOfficeKeyState(officekey);
            IList<PhoenixWeb.Site.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.Site.ReportingWebService.ParameterValue>();

            parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "StartDate", Value = sDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
            parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "EndDate", Value = eDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
            parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "Office", Value = officekey.ToString() });
            parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "State", Value = state });
            parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "User", Value = User.Identity.Name });
            string format = "PDF";
            Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
            return new ReportsResult(output, mimeType, format);
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult Menu()
        {
            return View();
        }
        public class ReportsResult : ActionResult
        {
            public ReportsResult(byte[] data, string mineType, string format)
            {
                this.Data = data;
                this.MineType = mineType;
                this.Format = format;
            }

            public byte[] Data { get; set; }
            public string MineType { get; set; }
            public string Format { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                if (Format == "PDF")
                {
                    MemoryStream ms = new MemoryStream(Data);
                    context.HttpContext.Response.ContentType = "application/pdf";
                    context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
                    context.HttpContext.Response.Buffer = true;
                    ms.WriteTo(context.HttpContext.Response.OutputStream);
                }
                if (Format == "HTML4.0")
                {
                    if (Data == null)
                    {
                        new EmptyResult().ExecuteResult(context);
                        return;
                    }
                    context.HttpContext.Response.ContentType = MineType;

                    using (MemoryStream ms = new MemoryStream(Data))
                    {
                        ms.Position = 0;
                        using (StreamReader sr = new StreamReader(ms))
                        {
                            context.HttpContext.Response.Output.Write(sr.ReadToEnd());
                        }
                    }
                }
            }
        }
        public ActionResult Print()
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                PhoenixWeb.Site.ReportingWebService.Warning[] warnings;
                string[] streamIds;

                string RefNumber = Session["LodgmentNo"].ToString();
                int DocketCheckSummary = (int)Session["DocketCheckSummary"];
                string ReportName = "/Cashing Reports/Operator Cashing Report";

                IList<PhoenixWeb.Site.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.Site.ReportingWebService.ParameterValue>();
                parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "RefNumber", Value = RefNumber });
                parameters.Add(new PhoenixWeb.Site.ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });
                // string format = "HTML4.0";
                string format = "PDF";

                Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                Session["MemberKey"] = 0;
                Session["DocketCheckSummary"] = 0;
                Session["ModelList"] = 0;
                return new FileContentResult(output, mimeType);
              
               // return new ReportsResult(output, mimeType, format);

            }
            catch (Exception ex)
            {
                int DocketCheckSummary = (int)Session["DocketCheckSummary"];
                ViewBag.Error = ex.Message;
                if (DocketCheckSummary != 0)
                {
                    DocketCheckDA dcd = new DocketCheckDA();
                    dcd.CancelDocketCheck(DocketCheckSummary);
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Error = "Error Printing";
            WebCashingResult model1 = new WebCashingResult();
            return View("PayDone", model1);
        }

        public ActionResult ViewMemberNotes(string Id)
        {
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(Id);

            MemberModel mobj = new MemberModel();
            mobj = mda.GetMemeberLowVersion(member_key);

            MemberNotesDA mdna = new MemberNotesDA();
            IEnumerable<MemberNotesModel> mnm;
            mnm = mdna.GetMemberNotes(member_key);
            Session["MemeberKey"] = member_key;
            Session["MemeberId"] = Id;
            MemberNoteVM model = new MemberNoteVM();
            model.MemberId = mobj.MemberId;
            model.FirstName = mobj.FirstName;
            model.LastName = mobj.LastName;
            model.TradingName = mobj.TradingName;
            model.MemberNotes = mnm.ToList();

            return View("MemberNotes", model);
        }

        public ActionResult CreateMemberNotes(FormCollection c, MemberNoteVM m)
        {
            MemberNotesDA mda = new MemberNotesDA();
            MemberNotesModel model = new MemberNotesModel();
            model.NoteDate = DateTime.Now;
            var t = c["IsWarning"];
            if (t == "on")
                model.Warning = true;
            else
                model.Warning = false;
                    
            model.Note = c["Note"].ToString();

            int member_key = (int)Session["MemeberKey"];
            string Id = "0";
            if (member_key != 0)
            {
                Id = Session["MemeberId"].ToString();

                model.Member_key = member_key;
                model.Modified_dttm = DateTime.Now;
                model.ModifiedByUser = User.Identity.Name;

                mda.Add(model);
            }

            return RedirectToAction("ViewMemberNotes", Id);
        }

        public ActionResult GetPhoto(string memberId)
        {
            MemberDA mda = new MemberDA();
            byte[] photo = mda.GetMemberPhotoByMemberId(memberId);
         
            return File(photo, "image/jpeg");
        }


        public ActionResult Withdrawal()
        {
            return View();
        }
        public ActionResult PayWithdrawal(string MemberId, decimal Amount)
        {
            UserDA uda = new UserDA();
            MemberAccountEntryDA maeda = new MemberAccountEntryDA();
            MemberAccountDA mada = new MemberAccountDA();
            MemberAccountEntryModel model = new MemberAccountEntryModel();
            string TerminalId = Session["TopBC"].ToString();
            MemberDA mda = new MemberDA();

            try
            {
                int member_key = mda.GetMemberKey(MemberId);
                decimal bal = mda.GetMemberBalance(member_key).Balance;

                if(bal < Amount )
                {
                    ViewBag.Error = "Amount cannot be greater than balance amount";
                    return View("MemberAccount", TerminalId);
                }
                if (Amount < 5M)
                {
                    ViewBag.Error = "Cannot withdraw less than $5.00";
                    return View("MemberAccount", TerminalId);
                }
                
                var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
               
               
                model.MemberAccountKey = mada.Get(member_key);
                model.Member_key = member_key;
                model.Transaction_key = null;
                model.EntryDateTime = DateTime.Now;
                model.Description = "Widthdrawal over the counter by " + User.Identity.Name;
                model.CreditAmount = 0.0M;
                model.DebitAmount = Amount;
                model.BalanceAmount = 0.0M;
                model.ReversedMemberAccountEntryKey = null;
                model.Office_key = uda.GetUserOfficeKey(uId);
                maeda.Add(model);
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("MemberAccount", TerminalId);
            }
            
            return View("MemberAccount", TerminalId);
        }
        public ActionResult SearchMemberAccount(string TopBC)
        {
            MemberBalanceModel model = new MemberBalanceModel();
            if (TopBC != null)
            {
                string TerminalId = TopBC.Substring(0, 8);
                Session["TopBC"] = TerminalId;
                MemberDA mda = new MemberDA();
                int member_key = mda.GetMemberKeyByTerminalId(TerminalId);
                model = mda.GetMemberBalance(member_key);
               
            }
            return View("MemberAccount", model);
        }

        public ActionResult Search(string TopBC, string BottomBC)
        {
            string paid = Session["Paid"].ToString();
            if (paid == "false")
            {
                WebCashingResult model = new WebCashingResult();
                model.GrandTotal = 0.0M;
                List<WebCashing_GetEFTDocketsByBatchID> result = new List<WebCashing_GetEFTDocketsByBatchID>();
                if (TopBC != null && BottomBC != null)
                {
                    var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;

                    int DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    DocketCheckDA dcd = new DocketCheckDA();
                    MemberDA mda = new MemberDA();
                    //result = dcd.DocketCheck(TopBC, BottomBC, User.Identity.Name, DocketCheckSummary, uId);
                    model.ResultList = result;
                    decimal faceValueTotal = 0.0M;
                    decimal commissionTotalTotal = 0.0M;
                    decimal extraTotal = 0.0M;
                   // model.GrandTotal = 0.0M;
                    int count = 0;
                    int Member_Key = 0;

                    foreach (var n in result)
                    {
                        if (n.BatchStatus != "Valid" && n.BatchStatus != "Paid")
                        {
                            if (result.Count == 1)
                            {

                                string TerminalId = TopBC.Substring(0, 8);
                                int day = Convert.ToInt32(BottomBC.Substring(0, 2));
                                int month = Convert.ToInt32(BottomBC.Substring(2, 2));
                                DateTime Date = new DateTime(DateTime.Now.Year, month, day);
                                Session["TopBC"] = TerminalId;

                                int member_key = mda.GetMemberKeyByTerminalIdByEffecDate(TerminalId, Date);
                                Member_Key = member_key;
                            }
                        }
                     
                        if (n.BatchStatus == "Valid")
                        {
                            faceValueTotal = faceValueTotal + n.FaceValue.Value;
                            commissionTotalTotal = commissionTotalTotal + n.TOtalcommission.Value;
                            extraTotal = extraTotal + n.Extras.Value;
                            count = count + 1;
                            Member_Key = n.Member_key.Value;
                        }
                        if (n.BatchStatus == "Paid")
                        {
                            faceValueTotal = 0.0M;
                            commissionTotalTotal = 0.0M;
                            model.GrandTotal = 0.0M;
                            Member_Key = n.Member_key.Value;
                        }
                        //if (n.BatchStatus != "Valid")
                        //{
                        //    OperatorDA oda = new OperatorDA();
                        //    OperatorModel opt = new OperatorModel();
                        //    opt = oda.Get(Member_Key);
                        //    if (opt.AllowCash == false)
                        //    {
                        //        n.BatchStatus = "Bank Customer";
                        //    }
                        //}

                       // model.BatchStatus = n.BatchStatus;
                    }
                    model.FaceValueTotal = faceValueTotal;
                    model.CommissionTotal = commissionTotalTotal;
                    model.ExtraTotal = extraTotal;
                    model.Count = count;
                    WebCashing_GetEFTDocketsByBatchID GetValidOne = (from x in result
                                                                     where x.BatchStatus == "Valid"
                                                                     select x).FirstOrDefault();

                    MemberModel mobj = new MemberModel();
                    mobj = mda.GetMemeberLowVersion(Member_Key);
                    model.memberid = mobj.MemberId;
                    model.TradingName = mobj.TradingName;

                    model.Name = mobj.FirstName + " " + mobj.LastName;
                    model.Mobile = mobj.Mobile;
                    model.Phone = mobj.Telephone;
                    model.PhotoID = mobj.PhotoID;
                    if (mobj.ABN != null)
                        model.ABN = mobj.ABN;
                    else
                        model.ABN = mobj.ACN;


                    OperatorDA oda = new OperatorDA();
                    OperatorModel opt = new OperatorModel();
                    opt = oda.Get(Member_Key);
                    if (opt.AllowCash == false)
                    {
                        model.BatchStatus = "Bank Customer";
                    }
                    else
                    {
                        model.BatchStatus = "Cash Customer";
                    }



                    RewardPointDA rda = new RewardPointDA();
                    RewardPointsModel rewardObj = new RewardPointsModel();
                    rewardObj = rda.Get(Member_Key);
                    model.TotalPoints = rewardObj.TotalPoints;
                    model.TotalPointsUsed = rewardObj.TotalPointsUsed;

                    MemberNotesDA mdna = new MemberNotesDA();
                    IEnumerable<MemberNotesModel> mnm;
                    mnm = mdna.GetMemberNotes(Member_Key).Take(4).ToList();
                    model.MemberNotes = mnm;

                    if (GetValidOne != null)
                    {
                        model.GrandTotal = GetValidOne.GrandTotal;
                        Session["DocketCheckSummary"] = GetValidOne.DocketCheckSummaryKey;
                        Session["LodgmentNo"] = GetValidOne.LodgementReferenceNo;
                    }
                    else
                    {
                        WebCashing_GetEFTDocketsByBatchID GetValidOne1 = (from x in result
                                                                          select x).FirstOrDefault();
                        model.GrandTotal = GetValidOne1.GrandTotal;
                        Session["DocketCheckSummary"] = GetValidOne1.DocketCheckSummaryKey;
                        Session["LodgmentNo"] = GetValidOne1.LodgementReferenceNo;
                    }
                    if (model.ResultList.First().BatchStatus == "Valid")
                    {
                        Session["DocketCheckSummary"] = GetValidOne.DocketCheckSummaryKey;
                        Session["MemberKey"] = Member_Key;
                        Session["ModelList"] = model;
                    }
                }
                ViewBag.ListError = "";
                return View("List", model);
            }
            else
            {
                WebCashingResult model = new WebCashingResult();
                model = (WebCashingResult)Session["ModelList"];
                ViewBag.ListError = "Please click New Batch button to start scanning new batch";
                return View("List", model);

            }
        }

        public ActionResult PayNow(string BtnType)
        {
            return View("PayNow");
        }

        public ActionResult PayDone(string BtnType)
        {
            int DocketCheckSummary = (int)Session["DocketCheckSummary"];
            if (BtnType == "Pay")
            {
                ViewBag.Msg = "Do you want to pay batch?";
                return View();
            }
            if (BtnType == "Pay1")
            {
                string paid = Session["Paid"].ToString();
                if (paid == "false" && DocketCheckSummary !=0)
                {
                    Session["Print"] = "false";
                    var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                    int memberkey = (int)Session["MemberKey"];
                    string print = Session["Print"].ToString();
                    if (paid == "false" && print == "false" && memberkey != 0)
                    {
                        try
                        {
                            DocketCheckDA dcd = new DocketCheckDA();
                            WebCashing_PayDockets modelPay = new WebCashing_PayDockets();
                            WebCashing_PayDockets_Result modelPay_result = dcd.PayDocket(DocketCheckSummary, memberkey, uId, User.Identity.Name);
                            modelPay.LodgementReferenceNo = modelPay_result.LodgementReferenceNo;
                            modelPay.TotalPaid = modelPay_result.TotalPaid;
                            modelPay.TransactionKey = modelPay_result.TransactionKey;
                            Session["LodgmentNo"] = modelPay.LodgementReferenceNo;
                            Session["DocketCheckSummary"] = 0;
                            Session["MemberKey"] = 0;
                            Session["Paid"] = "True";
                            WebCashingResult model = new WebCashingResult();
                            model = (WebCashingResult)Session["ModelList"];

                            //WebCashingResult model11 = new WebCashingResult();
                            //model = (WebCashingResult)Session["ModelList"];
                            ViewBag.Msg = "Batch Paid, Please print the statement";
                            return View("PayNow");
                            //ViewBag.Msg = "Batch Paid, Please Print Statement";
                            //return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);// View(model);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = ex.Message;
                            if (DocketCheckSummary != 0)
                            {
                                DocketCheckDA dcd = new DocketCheckDA();
                                dcd.CancelDocketCheck(DocketCheckSummary);
                                ViewBag.Msg = "An Error has occurred, Could not pay this Batch";
                                return View("PayError");
                               // ViewBag.Error = ex.Message;
                               // ViewBag.Msg = ex.Message;
                               // return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);// View(model);
                                //WebCashingResult model2 = new WebCashingResult();
                                //return View("PayDone", model2);
                            }
                        }
                    }
                    else
                    {
                       // ViewBag.Msg = "Error!,Cannot Pay Batch";
                        ViewBag.Msg = "An Error has occurred, Could not pay this Batch";
                        return View("PayError");
                       // return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);// View(model);
                        //ViewBag.Error = "Error! Cannot pay";
                        //WebCashingResult model2 = new WebCashingResult();
                        //return View("PayDone", model2);
                    }
                  return View();
                }
            }
            if (BtnType == "Print")
            {
                Session["Print"] = "True";
                return RedirectToAction("Print");
            }
            ViewBag.Msg = "Cannot Pay,Batch already Paid, Please Print Statement";
            return View("PayError");
            //return Json(ViewBag.Msg, JsonRequestBehavior.AllowGet);// View(model);
            //ViewBag.Error = "This batch is already paid";
            //WebCashingResult model1 = new WebCashingResult();
            //return View("PayDone",model1);
        }
      
        //public ActionResult ShowDialog()
        //{
        //    WebCashingResult model = new WebCashingResult();    
        //    model = (WebCashingResult)Session["ModelList"];
        //    return View(model);
        //}
        public ActionResult NewBatch()
        {
            int DocketCheckSummary = 0;
            var CheckSummary = Session["DocketCheckSummary"];
            if (CheckSummary != null)
                DocketCheckSummary = (int)Session["DocketCheckSummary"];

            var pstatus = Session["Paid"];
            string paid= string.Empty;
            if(pstatus != null)
            paid = Session["Paid"].ToString();
            if (paid == "false")
            {
                DocketCheckDA dcd = new DocketCheckDA();
                dcd.CancelDocketCheck(DocketCheckSummary);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
        public ActionResult Cancel()
        {
            int DocketCheckSummary = (int)Session["DocketCheckSummary"];
            string paid = Session["Paid"].ToString();
            if (paid == "false")
            {
                DocketCheckDA dcd = new DocketCheckDA();
                dcd.CancelDocketCheck(DocketCheckSummary);
                return RedirectToAction("Index");
            }
           
            return RedirectToAction("Index");
        }
    }
}

