﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhoenixObjects.Common;
using Phoenix_BusLog.Data;
using PhoenixObjects.Member;
using PhoenixObjects.Administrator;
using PhoenixObjects.UserAuthentication;
using PhoenixObjects.Terminal;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class AjaxAdminController : Controller
    {
        public JsonResult GetCountries()
        {
            CountryDA cda = new CountryDA();

            var countries = cda.Get().ToList();
            countries.Add(new CountryModel
            {
                CountryKey = 0,
                CountryName = "Select Country"
            });
            return Json(countries.OrderBy(c => c.CountryKey), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSearchFields()
        {
            MemberDA mda = new MemberDA();
            var SearchFieldList = mda.SearchField(string.Empty, string.Empty).ToList();
            return Json(SearchFieldList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMemberRentalTemplate(int company_key)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();

            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();
            MemberRentalTemplateList.Add(new MemberRentalTemplateModel
            {
                MemberRentalTemplateKey = 0,
                Description = "Select Member Rental Template"
            });
            return Json(MemberRentalTemplateList.OrderBy(c => c.Description), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTerminalRentalRateList(int MemberRentalTemplateKey)
        {
            Session["RateListToSave"] = null;
            TerminalRentalRateDA cda = new TerminalRentalRateDA();
            var TerminalRentalTemplateList = cda.GetByMemberRentalTemplateKey(MemberRentalTemplateKey).ToList();
            Session["RateListToSave"] = TerminalRentalTemplateList;
            return Json(TerminalRentalTemplateList.OrderBy(c => c.RentAmount), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBusinessType()
        {
            BusinessTypeDA cda = new BusinessTypeDA();

            var businessType = cda.Get().ToList();
            businessType.Add(new BusinessTypeModel
            {
                BusinessTypeKey = 0,
                BusinessTypeName = "Select Business Type"
            });
            return Json(businessType.OrderBy(c => c.BusinessTypeKey), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleType()
        {
            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get().ToList();
           
            return Json(VehicleType.OrderBy(c => c.VehicleType_key), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOffice()
        {
            OfficeDA oda = new OfficeDA();
            var office = oda.GetOffices().ToList();
            office.Add(new OfficeModel
            {
                Name = "Select Office",
                Office_key = 0
            });
            return Json(office.OrderBy(c => c.Office_key), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUsers()
        {
            UserDA oda = new UserDA();
            var users = oda.GetUsers().ToList();
            users.Add(new UserProfileModel
            {
                UserName = "Select Users",
                UserId = Guid.Empty
            });
            return Json(users.OrderBy(c => c.UserName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoles()
        {
            RolesDA rda = new RolesDA();
            var roles = rda.GetRoles().ToList();
            roles.Add(new RoleModel
            {
                RoleName = "Select Role",
                RoleId = Guid.Empty
            });
            return Json(roles.OrderBy(c => c.RoleId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDealers()
        {
            DealerDA cda = new DealerDA();
            var dealer = cda.Get().ToList();
            dealer.Add(new MemberModel
            {
                Member_key = 0,
                TradingName = "Select Dealer"
            });
            return Json(dealer.OrderBy(c => c.Member_key), JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult GetRentalTurnoverRange()
        {
            RentalTurnOverDA cda = new RentalTurnOverDA();
            var RentalTurnoverRange = cda.Get().ToList();
            RentalTurnoverRange.Add(new RentalTurnOverModel
            {
                RentalTurnoverRangeKey = 0,
               RangeMsg = "Please select turnover range"
            });
            return Json(RentalTurnoverRange.OrderBy(c => c.RentalTurnoverRangeKey), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStates()
        {

            StateDA cda = new StateDA();
            var states = cda.GetStateAustralia().ToList();
            //states.Add(new StateModel
            //{
            //    StateKey = 0,
            //    StateName = "Select State"
            //});
            return Json(states.OrderBy(c => c.StateKey), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEFTTerminalId(int termId)
        {
            TerminalDA cda = new TerminalDA();
            string teId = termId.ToString();
            var states = cda.CheckTerminalExists(teId);
            return Json(states.TerminalId, JsonRequestBehavior.AllowGet);
        }
     
    }
}
