﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Phoenix_BusLog.Data;
using PhoenixObjects.UserAuthentication;
using PhoenixObjects.Salesforce;
namespace PhoenixWeb.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministrationController : Controller
    {
        //
        // GET: /Administration/
       
        public ActionResult Index()
        {
            //string username = Session["Username"].ToString();
            var nameT =User.Identity.Name;
            //var r = User.IsInRole("Administrator");
            UserDA uda = new UserDA();
            int HasRole = uda.IsUserInRoles(nameT, "Administrator");

            return RedirectToAction("Menu");
        }
        
        public ActionResult SalesforceCred()
        {
            SalesforceDA sda = new SalesforceDA();
            SalesforceDetailModel cred = new SalesforceDetailModel();
            cred = sda.GetCredentials();
            return View(cred);
        }

        [HttpPost]
        public ActionResult SalesforceCred(SalesforceDetailModel Model)
        {
            SalesforceDA sda = new SalesforceDA();
            sda.AddCredentials(Model);
            return View(Model);
        }
        public ActionResult UserProfile(Guid id)
        {
            UserDA uda = new UserDA();
            UserProfileModel model = uda.GetUsers(id);
           return View(model);
        }
        public ActionResult Menu()
        {
            return View();
        }
        public ActionResult CreateRoles()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateRoles(FormCollection c)
        {
            string RoleName = c["RoleName"].ToString();
            Roles.CreateRole(RoleName);
            return RedirectToAction("Menu");
        }
        public ActionResult AddRoles()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRoles(FormCollection c)
        {
            Guid userid = Guid.Parse(c["UserName"].ToString());
            Guid roleid = Guid.Parse(c["Role"].ToString());

            UserDA uda = new UserDA();
            try
            {
                 ViewBag.Message= "Role Saved";
                uda.AddUserToRole(userid, roleid);
            }
            catch
            {
                ViewBag.Message = "Error";
            }
           
            return View();
        }

      
        public ActionResult Register()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Register(FormCollection c)
        {
            string username = c["UserName"].ToString();
            string email = c["Email"].ToString();
            string password = c["Password"].ToString();
            int officekey = Convert.ToInt32(c["Office"].ToString());
            Guid RoleKey = Guid.Parse(c["Role"].ToString());
             
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(username, password, email, passwordQuestion: null, passwordAnswer: null, isApproved: true, providerUserKey: null, status: out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    Guid uId = (Guid)membershipProvider.GetUser(username, false).ProviderUserKey;
                    UserDA uda = new UserDA();
                    if (officekey != 0)
                    {
                        uda.AddUserOffice(uId, officekey);
                    }
                    if (RoleKey != Guid.Empty)
                    {
                        uda.AddUserToRole(uId,RoleKey);
                    }

                    ViewBag.Success = "User Added Successfully";
                    return View("Success");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                    return View();
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Success");
        }
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}
