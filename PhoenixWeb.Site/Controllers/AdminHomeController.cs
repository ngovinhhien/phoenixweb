﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;


namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class AdminHomeController : Controller
    {
        //
        // GET: /Administrator/AdminHome/
       
        public ActionResult Index()
        {
           
            return View();
        }

        //
        // GET: /Administrator/AdminHome/Details/5

        //
        // GET: /Administrator/AdminHome/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Administrator/AdminHome/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administrator/AdminHome/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Administrator/AdminHome/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administrator/AdminHome/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Administrator/AdminHome/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult FreshDeskLogin()
        {
            const string key = "4fe7df9302aa983ad987ad1a1cbcfb0a";
            const string pathTemplate = "https://livegroup.freshdesk.com/login/sso?name={0}&email={1}&timestamp={2}&hash={3}";

            var username = "Vnguyen";
            var email = "vu.nguyen@livegroup.com.au";

            string timems = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString();
            var hash = GetHash(key, username, email, timems);
            var path = String.Format(pathTemplate, Server.UrlEncode(username), Server.UrlEncode(email), timems, hash);

            return Redirect(path);
        }
        private static string GetHash(string secret, string name, string email, string timems)
        {
            string input = name + email + timems;
            var keybytes = Encoding.Default.GetBytes(secret);
            var inputBytes = Encoding.Default.GetBytes(input);

            var crypto = new HMACMD5(keybytes);
            byte[] hash = crypto.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                string hexValue = b.ToString("X").ToLower(); // Lowercase for compatibility on case-sensitive systems
                sb.Append((hexValue.Length == 1 ? "0" : "") + hexValue);
            }
            return sb.ToString();
        }
    }
}
