﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix_BusLog.Salesforce;
using SalesforceSharp;
using SalesforceSharp.Security;
using SalesforceSharp.Serialization;
using SalesforceSharp.Models;
using PhoenixObjects.Salesforce;
using Phoenix_BusLog.Data;
namespace PhoenixWeb.Controllers
{
     [Authorize(Roles = "Operations,Administrator")]
    public class CaseController : Controller
    {
        //
        // GET: /Case/7515VA

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Menu()
        {
            return View();

        }
        List<SAccount> recordAccountGlobalList = new List<SAccount>();
        List<SAsset> recordAssetGlobalList = new List<SAsset>();
        List<SContact> recordContactGlobalList = new List<SContact>();

        private void SalesForceConnect()
        {
            var client = new SalesforceClient();
            SalesforceDA sda = new SalesforceDA();
            SalesforceDetailModel cred = new SalesforceDetailModel();
            cred = sda.GetCredentials();

            var authFlow = new UsernamePasswordAuthenticationFlow(cred.ClientID, cred.Secret, cred.UserName, cred.TokenPassword);

            var Account = client.Query<SAccount>("SELECT Id,AccountNumber,Name FROM Account");
            var Asset = client.Query<SAsset>("SELECT AccountId, TerminalID__c FROM Asset");
            var Contacts = client.Query<SContact>("SELECT Id,AccountId, FirstName, LastName,Email,MasterRecordId,Name,Birthdate,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet,MobilePhone,Fax,Phone FROM Contact");

            foreach (var r in Account)
            {
                SAccount a = new SAccount();
                a.Id = r.Id;
                a.AccountNumber = r.AccountNumber;
                a.Name = r.Name;
                recordAccountGlobalList.Add(a);
            }
            foreach (var r in Asset)
            {
                SAsset a = new SAsset();
                a.AccountId = r.AccountId;
                a.TerminalID__c = r.TerminalID__c;
                recordAssetGlobalList.Add(a);
            }
            foreach (var r in Contacts)
            {
                SContact a = new SContact();
                a.Id = r.Id;
                a.AccountId = r.AccountId;
                a.FirstName = r.FirstName;
                a.LastName = r.LastName;
                a.Email = r.Email;
                a.MasterRecordId = r.MasterRecordId;
                a.Name = r.Name;
                a.MailingCity = r.MailingCity;
                a.MailingCountry = r.MailingCountry;
                a.MailingPostalCode = r.MailingPostalCode;
                a.MailingState = r.MailingState;
                a.MailingStreet = r.MailingStreet;
                a.MobilePhone = r.MobilePhone;
                a.Phone = r.Phone;
                a.Fax = r.Fax;
                recordContactGlobalList.Add(a);
            }
        }

        public ActionResult SearchCase()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SearchCase(FormCollection c, string submitButton, CaseModel m)
        {
            if (submitButton == "Search")
            {
                string terminalId = c["TerminalID"].ToString();
                string MemberID = c["MemberID"].ToString();


                SalesForceConnect();
                //if (recordAccountGlobalList.Count == 0)

                SContact contract = new SContact();
                SAsset asset = new SAsset();
                SAccount account = new SAccount();
                string assetAccountId = string.Empty;
                List<SAccount> AllNames = new List<SAccount>();

                if (terminalId != null && terminalId != string.Empty)
                {
                    assetAccountId = (from d in recordAssetGlobalList
                                      where d.TerminalID__c == terminalId
                                      select d.AccountId).ToList().FirstOrDefault();
                }
                if (MemberID != null && MemberID != string.Empty)
                {
                    assetAccountId = (from d in recordAccountGlobalList
                                      where d.AccountNumber == MemberID
                                      select d.Id).ToList().FirstOrDefault();
                }

                // string name2 = char.ToUpper(name[0]) + name.Substring(1);

                //AllNames = (from d in recordAccountGlobalList
                //      where d.Name.Contains(name) || d.Name.Contains(name2) || d.Name.StartsWith(name) || d.Name.StartsWith(name2)
                //      select d).ToList();

                CaseModel model = new CaseModel();
                if (assetAccountId != null)
                {

                    account = recordAccountGlobalList.Find(r => r.Id == assetAccountId);
                    contract = recordContactGlobalList.Find(r => r.AccountId == assetAccountId);
                    asset = recordAssetGlobalList.Find(r => r.AccountId == assetAccountId);

                    model.AccountName = account.Name;
                    model.ContactName = contract.FirstName + " " + contract.LastName;
                    model.AccountSalesforceId = account.Id;
                    model.ContactSalesforceId = contract.Id;
                    model.TerminalId = asset.TerminalID__c;
                    model.Phone = contract.Phone;
                    model.Email = contract.Email;
                    Session["SalesforceCase"] = model;
                    return View(model);
                }


            }
            if (submitButton == "SalesforceBtn")
            {
                var client = new SalesforceClient();
                var authFlow = new UsernamePasswordAuthenticationFlow("3MVG9Y6d_Btp4xp5DDo82HRvipX6ZgGF6MAZDt53gsHH1pKkhnitsfXsbqfgujUp4KO8RPqyRvS_b5KvpJVkf", "1445840785118068527", "support@livetaxiepay.com.au", "Vincent002daVVim4tWSWgH1dySvWNq91oN");
                CaseModel cs = new CaseModel();
                cs.AccountSalesforceId = c["SalesforceID"].ToString();
                cs.Subject = c["Subject"].ToString();
                cs.Descrition = c["Description"].ToString();
                cs.Type = c["Type"].ToString();
                cs.Status = c["Status"].ToString();
                cs.Priority = c["Priority"].ToString();
                cs.TerminalId = c["TerminalIDS"].ToString();
                cs.CaseOrigin = c["CaseOrigin"].ToString();
                cs.Escalate = m.EscalateB.ToString();
              
                bool e = m.EscalateB;
                string ContactID = m.ContactSalesforceId;
                string Email = c["Email"].ToString();
                string Phone = c["PhoneNo"].ToString();

                client.Authenticate(authFlow);
                var CaseId = client.Create("Case", new
                 {
                     ACCOUNTID = cs.AccountSalesforceId,
                     SUBJECT = cs.Subject,
                     DESCRIPTION = cs.Descrition,
                     TYPE = cs.Type,
                     STATUS = cs.Status,
                     PRIORITY = cs.Priority,
                     TERMINAL_ID__C = c["TerminalIDS"].ToString(),
                     ORIGIN = cs.CaseOrigin,
                     ISESCALATED = e, //cs.Escalate,
                 });

                if (Email != null && Email != string.Empty) 
                {
                    SContact contract = new SContact();
                    contract = recordContactGlobalList.Find(r => r.Id == ContactID);
                    client.Update("Contact", ContactID, new { Email = Email});
                }
                 if (Phone != null && Phone != string.Empty)
                {
                    SContact contract = new SContact();
                    contract = recordContactGlobalList.Find(r => r.Id == ContactID);
                    client.Update("Contact", ContactID, new { Phone = Phone });
                }

            }
            return View();
        }
    }
}
