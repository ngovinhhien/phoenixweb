﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix_BusLog;
using PhoenixObjects;
using System.IO;
using System.Text.RegularExpressions;
using PhoenixObjects.Member;
using Phoenix_BusLog.Data;
using PhoenixObjects.Common;
using System.Data.Entity.Validation;
using Phoenix_BusLog.Salesforce;
using SalesforceSharp;
using SalesforceSharp.Security;
using SalesforceSharp.Serialization;
using SalesforceSharp.Models;
using PhoenixObjects.Terminal;
using PhoenixObjects.Financial;
using System.Transactions;
using PhoenixObjects.Salesforce;
namespace PhoenixWeb.Controllers
{
    [Authorize(Roles = "Operations,Administrator")]
    public class MemberController : Controller
    {
        //
        // GET: /Administrator/Member/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Menu()
        {
            return View();

        }
      public  List<SAccount> recordAccountGlobalList = new List<SAccount>();
      public List<SContact> recordLeadGlobalList = new List<SContact>();
      public List<SLead> recordOpportGlobalList = new List<SLead>();

      private List<SAccount> SalesForceConnect(string sfName, string type)
      {
          var client = new SalesforceClient();
          //Setup > Personal Setup > My Personal Information > Reset My Security Token.
          SalesforceDA sda = new SalesforceDA();
          SalesforceDetailModel cred = new SalesforceDetailModel();
          cred = sda.GetCredentials();

          var authFlow = new UsernamePasswordAuthenticationFlow(cred.ClientID, cred.Secret, cred.UserName, cred.TokenPassword);
          client.Authenticate(authFlow);
          IList<SAccount> records = null;
          if (type == "1")
          {
              //order is important
              records = client.Query<SAccount>("SELECT Id, AccountNumber,Name,Description,Industry,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,Phone FROM Account where Name like '" + sfName + "%'");
          }
          if (type == "2")
          {
              records = client.Query<SAccount>("SELECT Id, AccountNumber,Name,Description,Industry,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,Phone FROM Account where Phone like '%" + sfName + "%'");
          }
          foreach (var r in records)
          {
              SAccount a = new SAccount();
              a.Id = r.Id;
              a.AccountNumber = r.AccountNumber;
              a.Name = r.Name;
              a.Description = r.Description;
              a.Industry = r.Industry;
              a.ShippingCity = r.ShippingCity;
              a.ShippingCountry = r.ShippingCountry;
              a.ShippingState = r.ShippingState;
              a.ShippingPostalCode = r.ShippingPostalCode;
              a.ShippingStreet = r.ShippingStreet;
              a.Phone = r.Phone;

              recordAccountGlobalList.Add(a);
          }
          Session["recordAccountGlobalList"] = recordAccountGlobalList;
          return recordAccountGlobalList;
      }

      private void SalesForceConnect2(string AccountId)
      {
          var client = new SalesforceClient();
          //Setup > Personal Setup > My Personal Information > Reset My Security Token.
          SalesforceDA sda = new SalesforceDA();
          SalesforceDetailModel cred = new SalesforceDetailModel();
          cred = sda.GetCredentials();

          var authFlow = new UsernamePasswordAuthenticationFlow(cred.ClientID, cred.Secret, cred.UserName, cred.TokenPassword);
          client.Authenticate(authFlow);

          IList<SContact> leadRecords = null;
          IList<SLead> lead = null;
          leadRecords = client.Query<SContact>("SELECT AccountId, FirstName, LastName,Email,MasterRecordId,Name,Birthdate,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet,MobilePhone,Fax,Phone FROM Contact where AccountId = '" + AccountId + "'");
          lead = client.Query<SLead>("SELECT AccountId, Amex_Diners__c,Average_transaction__c,Diners__c,Debit__c,EFTPOS_turnover__c,Terminal_Rental__c,Visa_Mastercard__c,Monthly_Rental__c,Flat_Rate__c FROM Opportunity where AccountId = '" + AccountId + "'");
          foreach (var r in leadRecords)
          {
              SContact a = new SContact();
              a.AccountId = r.AccountId;
              a.FirstName = r.FirstName;
              a.LastName = r.LastName;
              a.Email = r.Email;
              a.MasterRecordId = r.MasterRecordId;
              a.Name = r.Name;
              a.MailingCity = r.MailingCity;
              a.MailingCountry = r.MailingCountry;
              a.MailingPostalCode = r.MailingPostalCode;
              a.MailingState = r.MailingState;
              a.MailingStreet = r.MailingStreet;
              a.MobilePhone = r.MobilePhone;
              a.Phone = r.Phone;
              a.Fax = r.Fax;
              recordLeadGlobalList.Add(a);
          }
          foreach (var r in lead)
          {
              SLead a = new SLead();
              a.AccountId = r.AccountId;
              a.Amex_Diners__c = r.Amex_Diners__c;
              a.Diners__c = r.Diners__c;
              a.Average_transaction__c = r.Average_transaction__c;
              a.Debit__c = r.Debit__c;
              a.EFTPOS_turnover__c = r.EFTPOS_turnover__c;
              //turn over range
              a.Terminal_Rental__c = r.Terminal_Rental__c;
              a.Visa_Mastercard__c = r.Visa_Mastercard__c;
             // a.Sliding_Scale__c = r.Sliding_Scale__c;
              a.Monthly_Rental__c = r.Monthly_Rental__c;
              a.Flat_Rate__c = r.Flat_Rate__c;

              recordOpportGlobalList.Add(a);
          }
      }
      public ActionResult SalesforceAddMemeber(string Id)
      {
          recordAccountGlobalList = (List<SAccount>)Session["recordAccountGlobalList"];
          SAccount ac = recordAccountGlobalList.Find(r => r.Id == Id);

          List<SForceModel> modelSF = new List<SForceModel>();
          SContact ld = new SContact();
          SLead led = new SLead();
          SalesForceConnect2(Id);

          if (ac != null)
          {
              SForceModel sforceModel = new SForceModel();
              ld = recordLeadGlobalList.Find(r => r.AccountId == Id);
              led = recordOpportGlobalList.Find(r => r.AccountId == Id);

              MemberModel mem = new MemberModel();
              mem.SalesforceId = Id;
              mem.CompanyKey = 18;

              if (ld != null)
              {
                  if (ld.FirstName != null)
                      mem.FirstName = ld.FirstName.Substring(0, 1).ToUpper() + ld.FirstName.Substring(1);
                  if (ld.LastName != null)
                      mem.LastName = ld.LastName.Substring(0, 1).ToUpper() + ld.LastName.Substring(1);
                  mem.Email = ld.Email;
                  if (ld.Phone != null)
                  {
                      string preMob = ld.Phone.Substring(0, 2);

                      if (preMob == "04")
                      {
                          mem.Mobile = ld.Phone.Trim();
                      }
                      else
                      {
                         mem.Telephone = ld.Phone.Trim();
                      }

                  }
                  if (ld.MobilePhone != null)
                      mem.Mobile = ld.MobilePhone.Trim();
                  mem.TradingName = ac.Name;
                  mem.Fax = ld.Fax;

                  if (Convert.ToDateTime(ld.Birthdate) != DateTime.MinValue)
                  {
                      mem.DOB = Convert.ToDateTime(ld.Birthdate);
                  }
              }

              if (led != null)
              {
                  if (led.Debit__c != null)
                      mem.CRDebitRate = led.Debit__c.Value;
                  if (led.Debit__c != null)
                      mem.CRDebitRatePercentage = led.Debit__c.Value;
                  if (led.Visa_Mastercard__c != null)
                      mem.CRVisaRate = led.Visa_Mastercard__c.Value;
                  if (led.Visa_Mastercard__c != null)
                      mem.CRMasterCardRate = led.Visa_Mastercard__c.Value;
                  if (led.Amex_Diners__c != null)
                      mem.CRAmexRate = led.Amex_Diners__c.Value;
                  if (led.Diners__c != null)
                      mem.CRDinersRate = led.Diners__c.Value;

                  string mRental = led.Monthly_Rental__c;
                  if (mRental != null)
                  {
                      MemberRentalTemplateDA mrda = new MemberRentalTemplateDA();
                      mem.RentalMemberRentalTemplateKey = mrda.GetKeyByName(mRental);
                  }

                  if (led.Flat_Rate__c != null)
                  {
                      mem.IsFlatRate = led.Flat_Rate__c;
                  }
                  else
                  {
                      mem.IsFlatRate = false;
                  }

                  if (led.Terminal_Rental__c != null)
                      mem.RentalAmountFromSalesforce = led.Terminal_Rental__c;

                  var range = led.EFTPOS_turnover__c;
                  RentalTurnOverDA rda = new RentalTurnOverDA();
                  if (range != null)
                      mem.RentalTurnoverRangeKeyFromSalesforce = rda.GetTurnOverRangeKey(range.Value);

              }
              else
              {

                  //no opportunities found in the salesforce so use default
                  CommissionRateDA cda = new CommissionRateDA();
                  DefaultRateModel drt = new DefaultRateModel();
                  drt = cda.GetDefaultCommissionRate(1, 1);

                  mem.CRDebitRate = drt.Debit;
                  mem.CRDebitRatePercentage = drt.DebitPercentage;
                  mem.CRVisaRate = drt.Visa;
                  mem.CRMasterCardRate = drt.Master;
                  mem.CRAmexRate = drt.Amex;
                  mem.CRDinersRate = drt.Diners;
                  mem.RentalMemberRentalTemplateKey = 7;
              }

              sforceModel.memberModel = mem;
              // sforceModel.
              if (ld != null)
              {
                  if (ld.MailingStreet != null)
                  {
                      string[] str = ld.MailingStreet.Split(null);
                      AddressListModel addrModel = new AddressListModel();
                      addrModel.Member = mem;

                      if (str[0] != null)
                      {
                          addrModel.StreetNumber = str[0];
                      }
                      if (str.Length > 1)
                      {
                          if (str[1] != null)
                          {
                              addrModel.StreetName = str[1];
                          }
                      }
                      sforceModel.addrModel = addrModel;
                  }
              }
              modelSF.Add(sforceModel);
          }

          Session["SalesforceMember"] = modelSF;
          MemberDA mda = new MemberDA();
          Session["AdrList"] = null;
          Session["BAccountList"] = null;
          Session["MemberRentalModelList"] = null;
          Session["TerminalAllocationList"] = null;
          bool IsMember = mda.GetMemeberBySalesforceId(Id);
          if (IsMember == false)
          {
              List<SForceModel> SFmodel = (List<SForceModel>)Session["SalesforceMember"];
              SForceModel sforceModel = new SForceModel();
              MemberModel model = new MemberModel();
              sforceModel = SFmodel.Where(r => r.memberModel.SalesforceId == Id).FirstOrDefault();
              model = sforceModel.memberModel;
              Session["SalesforceMemberAddress"] = sforceModel.addrModel;
              Session["SalesforceMemberModel"] = sforceModel.memberModel;
              Session["SalesforceRentalModel"] = sforceModel.memberRentalModel;
              model.IsBankCustomer = true;
              if (model.DOB != null)
              {
                  string dobDate = model.DOB.Value.ToShortDateString();
                  string[] dobDateArry = dobDate.Split('/');
                  model.DOBstring = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
              }
             // return View("Create", model);
              return RedirectToAction("Create", model);
          }
          else
          {
              ViewBag.Msg = "This member is already in Phoenix";
              return View("Message");
          }
      }

        public ActionResult GetSFName()
        {
            return Json(recordAccountGlobalList.OrderBy(c => c.Name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesforceSearch()
        {
            List<SAccount> model = new List<SAccount>();
            return View(model);
        }

        [HttpPost]
        public ActionResult SalesforceSearch(FormCollection c)
        {
            var name = c["Name"].ToString();
            var type = c["Type"].ToString();
            List<SAccount> model = new List<SAccount>();
            model = SalesForceConnect(name, type);
            return View(model);
        }
     
        public ActionResult AutoCompleteEftTerminalId(string term)
        {
            List<string> m = new List<string>();
            TerminalDA cda = new TerminalDA();
            var model = cda.GetEftTerminals(term).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AutoCompletePostCode(string term)
        {
            List<string> m = new List<string>();
            PostCodeDA cda = new PostCodeDA();
            var model = cda.GetStatePostCodeLocality(term).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteDealer(string term)
        {
            List<string> dealerList = new List<string>();
            DealerDA cda = new DealerDA();
            dealerList = cda.GetDealerAutoComplete(term).ToList();

            return Json(dealerList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string term)
        {
            List<string> memberList = new List<string>();
            EmployeeDA cda = new EmployeeDA();
            memberList = cda.GetEmployeeAutoComplete(term).ToList();

            return Json(memberList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteSearchTypeNumber(string term)
        {
            List<string> m = new List<string>();
            m.Add("MemberId");
            m.Add("TerminalId");
            m.Add("Mobile");
            m.Add("BusinessId");
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteSearchTypeString(string term)
        {
            List<string> m = new List<string>();
            m.Add("First Name");
            m.Add("Last Name");
            m.Add("Email");
            m.Add("Trading Name");
            m.Add("State");
            m.Add("Suburb");
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        private void DrawDropDownConfigAllocateTerminal()
        {
            StateDA sda = new StateDA();
            var states = sda.GetStateAustralia().ToList();
            ViewBag.StateList = states.Select(item => new SelectListItem
            {
                Text = item.StateName,
                Value = item.StateKey.ToString()
            }).ToList();

            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get().ToList();
            ViewBag.VehicleTypeList = VehicleType.Select(item => new SelectListItem
            {

                Text = item.Name,
                Value = item.VehicleType_key.ToString()
            }).ToList();
        }

        private void DrawDropDownConfig(int company_key)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();
           
            ViewBag.MemberRentalTemplateList = MemberRentalTemplateList.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.MemberRentalTemplateKey.ToString()
            }).ToList();

            BusinessTypeDA bda = new BusinessTypeDA();
            var BusinessTypeList = bda.Get().ToList();
            ViewBag.BusinessTypeList = BusinessTypeList.Select(item => new SelectListItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString()
            }).ToList();
        }
       
        public ActionResult Create(MemberModel m_model)
        {
            Session["AdrList"] = null;
            Session["BAccountList"] = null;
            Session["MemberRentalModelList"] = null;
            Session["TerminalAllocationList"] = null;
            Session["RateListToSave"] = null;
            CommissionRateDA cda = new CommissionRateDA();
            DefaultRateModel drt = new DefaultRateModel();
            drt = cda.GetDefaultCommissionRate(1, 1);
            MemberModel model = new MemberModel();
            model = m_model;
            model.IsBankCustomer = true;

            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');

            model.RentalStartDate = DateTime.Now;
           
            MemberModel SFmodel = new MemberModel();
            SFmodel = (MemberModel)Session["SalesforceMemberModel"];
            if (SFmodel != null)
            {
                model.CRDebitRate = SFmodel.CRDebitRate;
                model.CRVisaRate = SFmodel.CRVisaRate;
                model.CRMasterCardRate = SFmodel.CRMasterCardRate;
                model.CRDinersRate = SFmodel.CRDinersRate;
                model.CRAmexRate = SFmodel.CRAmexRate;
                model.CRDebitRatePercentage = SFmodel.CRDebitRatePercentage;
                model.RentalMemberRentalTemplateKey = SFmodel.RentalMemberRentalTemplateKey;
                model.CompanyKey = 18;
                model.RentalAmountFromSalesforce = SFmodel.RentalAmountFromSalesforce;
                bool IsFlatRate = SFmodel.IsFlatRate;
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();
                //this is the case for default case with entered amount in salesforce, set amt for all 5k and over
                if (model.RentalMemberRentalTemplateKey == 7 && model.RentalAmountFromSalesforce != null && IsFlatRate == false)
                {
                    if(model.RentalAmountFromSalesforce >= 0)
                    {
                        foreach(var rate in model.TerminalRentalRateList)
                        {
                            if(rate.TransactionTarget >=5000)
                            {
                                rate.RentAmount = model.RentalAmountFromSalesforce.Value;
                            }
                        }
                    }
                }
                //this is the case for flat rate from salesforce,set amt for all 
                if (model.RentalMemberRentalTemplateKey == 7 && model.RentalAmountFromSalesforce != null && IsFlatRate == true)
                {
                    if (model.RentalAmountFromSalesforce >= 0)
                    {
                        foreach (var rate in model.TerminalRentalRateList)
                        {
                                rate.RentAmount = model.RentalAmountFromSalesforce.Value;
                        }
                    }
                }
            }
            else
            {
                model.CRDebitRate = drt.Debit;
                model.CRVisaRate = drt.Visa;
                model.CRMasterCardRate = drt.Master;
                model.CRDinersRate = drt.Diners;
                model.CRAmexRate = drt.Amex;
                model.CRDebitRatePercentage = drt.DebitPercentage;
                model.TerminalRentalRateList = null;
                model.CompanyKey = 18;
                model.RentalMemberRentalTemplateKey = 7;
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();

            }
            Session["RateListToSave"] = model.TerminalRentalRateList;
            DrawDropDownConfig(model.CompanyKey);
            return View(model);
        }
        //
        // POST: /Administrator/Member/Create

        [HttpPost]
        public ActionResult Create(MemberModel m, FormCollection c, HttpPostedFileBase file)
        {
            MemberModel model = new MemberModel();

            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    string[] dealer = (c["Dealer"].ToString()).Split('|');
                    int count = dealer.Count();
                    if (count > 2)
                    {
                        model.DealerKey = Convert.ToInt32(dealer[1]);
                    }

                    model.FirstName = c["FirstName"].ToString();
                    model.LastName = c["LastName"].ToString();
                    model.Telephone = c["Telephone"].ToString();
                    model.Fax = c["Fax"].ToString();
                    model.Mobile = c["Mobile"].ToString();
                    model.TradingName = c["TradingName"].ToString();
                    model.ABN = c["ABN"].ToString();
                    model.ACN = c["ACN"].ToString();
                    model.DriverLicenseNumber = c["DriverLicenseNumber"].ToString();
                    model.Email = c["Email"].ToString();
                    model.Active = m.Active;
                    model.IsBankCustomer = m.IsBankCustomer;
                    model.CompanyKey = m.CompanyKey;// Convert.ToInt32(c["BusinessType"].ToString());
                    model.Welcomed = m.Welcomed;
                    model.SalesforceId = m.SalesforceId;
                    model.IsOperator = true;
                    model.Modified_dttm = DateTime.Now;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedByUser = User.Identity.Name;
                    model.MerchantID = c["Merchant"].ToString();


                    DateTime PhotoIDExpiry = default(DateTime);
                    DateTime.TryParse(c["PhotoIDExpiry"].ToString(), out PhotoIDExpiry);
                    if (PhotoIDExpiry != null)
                        model.PhotoIDExpiry = PhotoIDExpiry;
                    model.Active = m.Active;
                    DateTime dob = default(DateTime);
                    DateTime.TryParse(c["DOB"].ToString(), out dob);
                    if (dob != null)
                        model.DOB = dob;
                    byte[] photoid = new byte[1];
                    if (file != null)
                    {
                        bool validFile = Regex.IsMatch(file.FileName, "(.*?)\\.(JPG|PNG|JPEG)$");
                        if (file.ContentLength > 0 && validFile)
                        {
                            BinaryReader reader = new BinaryReader(file.InputStream);
                            photoid = reader.ReadBytes((int)file.InputStream.Length);
                        }
                    }
                    model.PhotoID = photoid;

                    MemberDA mem = new MemberDA();
                    int member_key = mem.AddNew(model);

                    //adding address
                    List<CAddress> CAddresses = (List<CAddress>)Session["AdrList"];
                    if (CAddresses != null)
                    {
                        foreach (var cadd in CAddresses)
                        {
                            MemberAddressDA mda = new MemberAddressDA();
                            AddressListModel m1 = new AddressListModel();
                            int id = member_key;
                            MemberDA mda1 = new MemberDA();
                           // MemberModel member = mda1.GetMemeber(id);
                            m1.StreetNumber = cadd.StreetNumber;
                            m1.StreetName = cadd.StreetName;
                            m1.PostCode = cadd.PostCode;
                            m1.State = cadd.State;
                            m1.Suburb = cadd.Suburb;
                            m1.Country = cadd.Country;
                            m1.AddressType = cadd.AddressType;
                          //  m1.Member = member;
                            mda.Add(m1,id);
                        }
                    }

                    //adding account
                    BankAccountModelList m2 = new BankAccountModelList();
                    m2.BAccounttModelList = (List<BankAccountModel>)Session["BAccountList"];
                    if (m2.BAccounttModelList != null)
                    {
                        foreach (var b in m2.BAccounttModelList)
                        {
                            AccountDA mda = new AccountDA();
                            MemberDA mda1 = new MemberDA();
                            //  MemberModel member = mda1.GetMemeber(id);
                            BankAccountModel account = new BankAccountModel();
                            account.AccountName = b.AccountName;
                            account.AccountNumber = b.AccountNumber;
                            account.Weight = b.Weight;
                            account.BSB = b.BSB;
                            account.Member_Key = member_key;
                            //   account.Member = member;
                            mda.Add(account);
                        }
                    }
                    decimal CRDebitRatePercentage = 0;
                    decimal MannualVoucherRate = 0;
                    //add commissionRate

                    if (model.CompanyKey == 1)
                    {
                        CRDebitRatePercentage = 0;
                        Decimal.TryParse(c["DebitRatePercentage"].ToString(), out CRDebitRatePercentage);
                        if (CRDebitRatePercentage != 0)
                            model.CRDebitRatePercentage = CRDebitRatePercentage / 100;

                        MannualVoucherRate = 0;
                        Decimal.TryParse(c["MannualVoucherRate"].ToString(), out MannualVoucherRate);
                        if (MannualVoucherRate != 0)
                            model.CRManualVoucherRate = MannualVoucherRate;

                        decimal MasterCardRate = 0;
                        Decimal.TryParse(c["MasterCardRate"].ToString(), out MasterCardRate);
                        if (MasterCardRate != 0)
                            model.CRMasterCardRate = MasterCardRate / 100;

                        decimal VisaCardRate = 0;
                        Decimal.TryParse(c["VisaCardRate"].ToString(), out VisaCardRate);
                        if (VisaCardRate != 0)
                            model.CRVisaRate = VisaCardRate / 100;

                        decimal AmexCardRate = 0;
                        Decimal.TryParse(c["AmexCardRate"].ToString(), out AmexCardRate);
                        if (AmexCardRate != 0)
                            model.CRAmexRate = AmexCardRate / 100;

                        decimal DinersCardRate = 0;
                        Decimal.TryParse(c["DinersCardRate"].ToString(), out DinersCardRate);
                        if (DinersCardRate != 0)
                            model.CRDinersRate = DinersCardRate / 100;
                    }
                    else
                    {
                        decimal DebitRate = 0;
                        Decimal.TryParse(c["DebitRate"].ToString(), out DebitRate);
                        if (DebitRate != 0)
                            model.CRDebitRate = -DebitRate;

                        decimal MasterCardRate = 0;
                        Decimal.TryParse(c["MasterCardRate"].ToString(), out MasterCardRate);
                        if (MasterCardRate != 0)
                            model.CRMasterCardRate = -(MasterCardRate / 100);

                        decimal VisaCardRate = 0;
                        Decimal.TryParse(c["VisaCardRate"].ToString(), out VisaCardRate);
                        if (VisaCardRate != 0)
                            model.CRVisaRate = -(VisaCardRate / 100);

                        decimal AmexCardRate = 0;
                        Decimal.TryParse(c["AmexCardRate"].ToString(), out AmexCardRate);
                        if (AmexCardRate != 0)
                            model.CRAmexRate = -(AmexCardRate / 100);

                        decimal DinersCardRate = 0;
                        Decimal.TryParse(c["DinersCardRate"].ToString(), out DinersCardRate);
                        if (DinersCardRate != 0)
                            model.CRDinersRate = -(DinersCardRate / 100);
                    }


                    MerchantAccountDA mdac = new MerchantAccountDA();
                    string merchantIdstring = string.Empty;
                    if (model.CompanyKey == 1 || model.CompanyKey == 18)
                    {
                        merchantIdstring = "23166226";
                    }
                    if (model.CompanyKey == 19)
                    {
                        merchantIdstring = "00583567";
                    }

                    int merchantAccountkey = mdac.GetMerchantAccountKey(merchantIdstring, model.CompanyKey);

                    List<CommissionRateModel> comRateList = new List<CommissionRateModel>();
                    PayMethodDA pda = new PayMethodDA();
                    DocketPayMethodDA cda = new DocketPayMethodDA();
                    VehicleTypeDA vda0 = new VehicleTypeDA();
                    //master card
                    CommissionRateModel comRateMaster = new CommissionRateModel();
                    comRateMaster.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    comRateMaster.OperatorCommissionRate = model.CRMasterCardRate;
                    comRateMaster.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateMaster.StartDate = DateTime.Now;
                    comRateMaster.EndDate = null;
                    comRateMaster.Current = true;
                    comRateMaster.Member_key = member_key;
                    int pKey = pda.GetPayMethod_key("MasterCard");
                    int vkey0 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey = cda.GetDocketPayMethod_key(pKey, vkey0, merchantAccountkey);
                    comRateMaster.DocketPayMethod_key = dKey;
                    comRateList.Add(comRateMaster);

                    //Visa card
                    CommissionRateModel comRateVisa = new CommissionRateModel();
                    comRateVisa.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    comRateVisa.OperatorCommissionRate = model.CRVisaRate;
                    comRateVisa.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateVisa.StartDate = DateTime.Now;
                    comRateVisa.EndDate = null;
                    comRateVisa.Current = true;
                    comRateVisa.Member_key = member_key;
                    int pKey1 = pda.GetPayMethod_key("Visa");
                    int vkey1 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey1 = cda.GetDocketPayMethod_key(pKey1, vkey1, merchantAccountkey);
                    comRateVisa.DocketPayMethod_key = dKey1;
                    comRateList.Add(comRateVisa);

                    //Amex card
                    CommissionRateModel comRateAmex = new CommissionRateModel();
                    comRateAmex.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    comRateAmex.OperatorCommissionRate = model.CRAmexRate;
                    comRateAmex.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateAmex.StartDate = DateTime.Now;
                    comRateAmex.EndDate = null;
                    comRateAmex.Current = true;
                    comRateAmex.Member_key = member_key;
                    int pKey2 = pda.GetPayMethod_key("AMEX");
                    int vkey2 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey2 = cda.GetDocketPayMethod_key(pKey2, vkey2, merchantAccountkey);
                    comRateAmex.DocketPayMethod_key = dKey2;
                    comRateList.Add(comRateAmex);

                    //Diners card
                    CommissionRateModel comRateDiners = new CommissionRateModel();
                    comRateDiners.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    comRateDiners.OperatorCommissionRate = model.CRDinersRate;
                    comRateDiners.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateDiners.StartDate = DateTime.Now;
                    comRateDiners.EndDate = null;
                    comRateDiners.Current = true;
                    comRateDiners.Member_key = member_key;
                    int pKey3 = pda.GetPayMethod_key("Diners");
                    int vkey3 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey3 = cda.GetDocketPayMethod_key(pKey3, vkey3, merchantAccountkey);
                    comRateDiners.DocketPayMethod_key = dKey3;
                    comRateList.Add(comRateDiners);

                    //Debit card
                    CommissionRateModel comRateDebit = new CommissionRateModel();
                    comRateDebit.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    if (model.CompanyKey == 1) { comRateDebit.OperatorCommissionRate = model.CRDebitRatePercentage; }
                    if (model.CompanyKey == 18)
                    {
                        comRateDebit.OperatorCommissionRate = model.CRDebitRate;
                        comRateDebit.FixAmount = true;
                    }

                    comRateDebit.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateDebit.StartDate = DateTime.Now;
                    comRateDebit.EndDate = null;
                    comRateDebit.Current = true;
                    comRateDebit.Member_key = member_key;
                    int pKey4 = pda.GetPayMethod_key("Debit");
                    int vkey4 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey4 = cda.GetDocketPayMethod_key(pKey4, vkey4, merchantAccountkey);
                    comRateDebit.DocketPayMethod_key = dKey4;
                    comRateList.Add(comRateDebit);

                    //Eftpos card
                    CommissionRateModel comRateEftpos = new CommissionRateModel();
                    comRateEftpos.GrossDealerCommissionRate = model.CRMasterCardRate + model.CRDealerRate;
                    if (model.CompanyKey == 1) { comRateEftpos.OperatorCommissionRate = model.CRDebitRatePercentage; }
                    if (model.CompanyKey == 18) { comRateEftpos.OperatorCommissionRate = model.CRDebitRate; comRateDebit.FixAmount = true; }

                    comRateEftpos.ManualCommissionRate = model.CRManualVoucherRate;
                    comRateEftpos.StartDate = DateTime.Now;
                    comRateEftpos.EndDate = null;
                    comRateEftpos.Current = true;
                    comRateEftpos.Member_key = member_key;
                    int pKey5 = pda.GetPayMethod_key("Eftpos");
                    int vkey5 = vda0.GetVehicleTypeKey(model.CompanyKey);
                    int dKey5 = cda.GetDocketPayMethod_key(pKey5, vkey5, merchantAccountkey);
                    comRateEftpos.DocketPayMethod_key = dKey5;
                    comRateList.Add(comRateEftpos);

                    foreach (var t in comRateList)
                    {
                        CommissionRateDA coda = new CommissionRateDA();
                        CommissionRateModel m1 = new CommissionRateModel();
                        m1.GrossDealerCommissionRate = t.GrossDealerCommissionRate;
                        m1.OperatorCommissionRate = t.OperatorCommissionRate;
                        m1.ManualCommissionRate = t.ManualCommissionRate;
                        m1.StartDate = t.StartDate;
                        m1.EndDate = t.EndDate;
                        m1.Current = t.Current;
                        m1.Modified_dttm = t.Modified_dttm;
                        m1.ModifiedByUser = t.ModifiedByUser;
                        m1.CommissionRate_key = t.CommissionRate_key;
                        m1.Member_key = t.Member_key;
                        m1.DocketPayMethod_key = t.DocketPayMethod_key;
                        m1.FixAmount = t.FixAmount;
                        coda.AddNew(m1);
                    }

                    DateTime StartDate = DateTime.MinValue;
                    DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
                    if (StartDate != DateTime.MinValue)
                    {
                        StartDate = Convert.ToDateTime(c["StartDate"].ToString());
                    }

                    DateTime endDate = DateTime.MinValue;
                    DateTime.TryParse(c["EndDate"].ToString(), out endDate);
                    if (endDate != DateTime.MinValue)
                    {
                        endDate = Convert.ToDateTime(c["EndDate"].ToString());
                    }
                    //add rental
                    if(m.RentalRentFree != true)
                    {
                        int MemberRentalTemplateKey = m.RentalMemberRentalTemplateKey.Value;// Convert.ToInt32(c["MonthlyRental"].ToString());
                        List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                        //TerminalRentalRateDA trda = new TerminalRentalRateDA();
                        MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
                        mtra.Add(TerminalRentalRateList, member_key, StartDate, endDate, User.Identity.Name);

                    }
                    else
                    {
                        MemberRentalDA mda = new MemberRentalDA();
                        MemberRentalModel m1 = new MemberRentalModel();
                        m1.Member_key = member_key;
                        m1.RentFree = true;
                        if (StartDate != DateTime.MinValue)
                        {
                            m1.StartDate = StartDate;
                        }
                        if (endDate != DateTime.MinValue)
                        {
                            m1.EndDate = endDate;
                        }
                        m1.ModifiedByUser = User.Identity.Name;
                        m1.Modified_dttm = DateTime.Now;
                        m1.RentalAmount = 0;
                        //m1.RentalTurnoverRangeKey = t.RentalTurnoverRangeKey;
                        mda.AddNew(m1);

                    }
                    //add Vehicle and TerminalAllocation
                    List<TerminalAllocationModel> TAListInternal = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                    if (TAListInternal != null)
                    {
                        foreach (var t in TAListInternal)
                        {
                            VehicleDA vda = new VehicleDA();
                            VehicleModel v1 = new VehicleModel();
                            v1.VehicleId = t.VehicleModel.VehicleId;
                            v1.RegisteredState = t.VehicleModel.RegisteredState;
                            v1.TerminalAllocatedDate = t.VehicleModel.TerminalAllocatedDate;
                            v1.Operator_key = member_key;
                            v1.VehicleType_key = t.VehicleModel.VehicleType_key;
                            v1.CreatedDate = DateTime.Now;
                            v1.Modified_dttm = DateTime.Now;
                            v1.ModifiedByUser = User.Identity.Name;
                            v1.EftTerminal_key = t.EftTerminal_key;
                            int veh_Key = vda.AddNew(v1);

                            TerminalAllocationDA mda = new TerminalAllocationDA();
                            TerminalAllocationModel m1 = new TerminalAllocationModel();
                            m1.EftTerminal_key = t.EftTerminal_key;
                            m1.Member_key = member_key;
                            m1.EffectiveDate = t.VehicleModel.TerminalAllocatedDate.Value;
                            m1.IsCurrent = true;
                            m1.ModifiedByUser = User.Identity.Name;
                            m1.DeliveryRequired = t.DeliveryRequired;
                            m1.ServiceFee = t.ServiceFee;
                            m1.AllowMOTO = t.AllowMOTO;
                            m1.AllowRefund = t.AllowRefund;
                            m1.Vehicle_key = veh_Key;
                            int termAlloc_Key = mda.AddNew(m1);

                            //change the status
                            TerminalDA tda = new TerminalDA();
                            tda.UpdateTerminalStatus(t.EftTerminal_key.Value, TEnum.TerminalStatus.LIVE.ToString());

                            if (t.Member_key != 0)
                            {
                                TerminalSalesDA sda = new TerminalSalesDA();
                                TerminalSalesModel s1 = new TerminalSalesModel();
                                s1.TerminalAllocation_key = termAlloc_Key;
                                s1.Employee_key = t.Member_key.Value;// member_key;
                                s1.Active = true;
                                s1.SaleRelationship_key = 1;
                                s1.ModifiedByUser = User.Identity.Name;
                                s1.Modified_dttm = DateTime.Now;

                                sda.AddNew(s1);
                            }
                        }
                    }
                    tran.Complete();
                    return RedirectToAction("Edit", new { member_key = member_key });
                }
                catch (Exception ex)
                {

                    ModelState.AddModelError("error", ex.InnerException);
                    DrawDropDownConfig(model.CompanyKey);
                    return View(model);
                }

            }
        }
        
        
        public ActionResult CreateMemberAddress()
        {
            CAddressList model = new CAddressList();
            model.CAddresses = (List<CAddress>)Session["AdrList"];
            AddressListModel SFmodel = new AddressListModel();
            SFmodel = (AddressListModel)Session["SalesforceMemberAddress"];
            if (SFmodel != null)
            {
                model.StreetNumber = SFmodel.StreetNumber;
                model.StreetName = SFmodel.StreetName;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateMemberAddress(CAddressList m, FormCollection c)
        {
            var c1 = c["PostCode"].ToString();
            string[] pc = c1.Split(',');

            CAddress adr = new CAddress();
            Random rnd = new Random();
            adr.Id = rnd.Next(1, 50);
            adr.StreetNumber = m.StreetNumber;
            adr.StreetName = m.StreetName;
            adr.PostCode = pc[0].Trim();
            adr.State = pc[1].Trim();
            adr.Suburb = pc[2].Trim();
            adr.Country = c["Country"].ToString().Trim();
            adr.AddressType = c["AddressType"].ToString();
            m.StreetNumber = c["StreetNumber"].ToString();
            m.StreetName = c["StreetName"].ToString();

            List<CAddress> madd = new List<CAddress>();
            madd = (List<CAddress>)Session["AdrList"];
            if (madd == null)
            {
                madd = new List<CAddress>();
            }
            madd.Add(adr);

            m.CAddresses = madd.Distinct().ToList<CAddress>();
            Session["AdrList"] = m.CAddresses;

            CAddressList model = new CAddressList();
            model = m;
            return View(model);
        }

        public ActionResult DeleteCreatedAddress(int Id)
        {
            List<CAddress> madd = new List<CAddress>();
            madd = (List<CAddress>)Session["AdrList"];
            if (madd != null)
            {
                var item = (from n in madd
                           where n.Id == Id
                                select n).FirstOrDefault();
                madd.Remove(item);
            }
             CAddressList model = new CAddressList();
            model.CAddresses = madd.Distinct().ToList<CAddress>();

            if (madd.Count() == 0)
                model.CAddresses = null;

            Session["AdrList"] = model.CAddresses;

           // return View(model);
          return  RedirectToAction("CreateMemberAddress");
        }

        

        public ActionResult AddMemberAddress(int id)
        {

            MemberDA mda = new MemberDA();
            MemberModel member = mda.GetMemeber(id);
            Session["Memberkey"] = id;

            AddressListModel model = new AddressListModel();
            AddressListModel SFmodel = new AddressListModel();
            SFmodel = (AddressListModel)Session["SalesforceMemberAddress"];
            if (SFmodel != null)
            {
                model.Member = member;
                model.StreetNumber = SFmodel.StreetNumber;
                model.StreetName = SFmodel.StreetName;
            }
            else
            {
                model.Member = member;
                model.StreetNumber = string.Empty;
                model.StreetName = string.Empty;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddMemberAddress(AddressListModel m, FormCollection c)
        {
            MemberAddressDA mda = new MemberAddressDA();
            int id = (int)Session["Memberkey"];
            MemberDA mda1 = new MemberDA();
           // MemberModel member = mda1.GetMemeber(id);
            var c1 = c["PostCode"].ToString();
            string[] pc = c1.Split(',');
            m.PostCode = pc[0].Trim();
            m.State = pc[1].Trim();
            m.Suburb = pc[2].Trim();
            m.Country = c["Country"].ToString().Trim();
            m.StreetNumber = c["StreetNumber"].ToString();
            m.StreetName = c["StreetName"].ToString();
            m.AddressType = c["AddressType"].ToString();
           // m.Member = member;
            mda.Add(m,id);

            AddressListModel model = new AddressListModel();
            model = mda.GetAddressListModel(m.Member.Member_key);
            return View(model);
        }

        public ActionResult CreateMemberAccount()
        {
            BankAccountModelList model = new BankAccountModelList();
            model.BAccounttModelList = (List<BankAccountModel>)Session["BAccountList"];

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateMemberAccount(BankAccountModelList m, FormCollection c)
        {
            BankAccountModel account = new BankAccountModel();
            BankAccountModelList model = new BankAccountModelList();
            decimal totalWeight = 0;
            Random rnd = new Random();
            account.Id = rnd.Next(1, 50);
            account.AccountName = c["AccountName"].ToString();
            account.AccountNumber = c["AccountNumber"].ToString();
            account.Weight = Convert.ToInt32(c["Weight"].ToString());
            account.BSB = c["BsbNumber"].ToString();
            List<BankAccountModel> BAList = new List<BankAccountModel>();
            BAList = (List<BankAccountModel>)Session["BAccountList"];
            if (BAList == null)
            {
                BAList = new List<BankAccountModel>();
                totalWeight = account.Weight;
            }
            else
            {
                foreach (var BA in BAList)
                {
                    totalWeight = totalWeight + BA.Weight;
                }
                totalWeight = totalWeight + account.Weight;
            }

            if (totalWeight > 100)
            {
                ViewBag.Error = "Weight exceed more than 100%";
                Session["BAccountList"] = BAList;
                model.BAccounttModelList = BAList;
                return View(model);

            }
            BAList.Add(account);
            Session["BAccountList"] = BAList;
            model.BAccounttModelList = BAList;
            return View(model);

        }

        public ActionResult DeleteCreateMemberAccount(int Id)
        {
            BankAccountModel account = new BankAccountModel();
            BankAccountModelList model = new BankAccountModelList();

            List<BankAccountModel> BAList = new List<BankAccountModel>();
            BAList = (List<BankAccountModel>)Session["BAccountList"];
            if (BAList != null)
            {
                var item = (from n in BAList
                            where n.Id == Id
                            select n).FirstOrDefault();
                BAList.Remove(item);
            }
            if (BAList.Count() == 0)
            {
                BAList = null;
            }
            Session["BAccountList"] = BAList;
            model.BAccounttModelList = BAList;

            return RedirectToAction("CreateMemberAccount");
            //  return View(model);

        }


        public ActionResult AddMemberAccount(int id)
        {
            AccountDA mda = new AccountDA();
            BankAccountModel model = new BankAccountModel();
            Session["Memberkey"] = id;
            model = mda.GetAccountListModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddMemberAccount(BankAccountModel m, FormCollection c)
        {
            AccountDA mda = new AccountDA();
            int id = (int)Session["Memberkey"];
            MemberDA mda1 = new MemberDA();
            MemberModel member = mda1.GetMemeber(id);

            BankAccountModel account = new BankAccountModel();
            account.AccountName = c["AccountName"].ToString();
            account.AccountNumber = c["AccountNumber"].ToString();
            account.Weight = Convert.ToInt32(c["Weight"].ToString());
            account.BSB = c["BsbNumber"].ToString();
            account.Member = member;
            decimal totalWeight = 0;
            foreach (var BA in member.BankAccounts)
            {
                totalWeight = totalWeight + BA.Weight;

            }
            totalWeight = totalWeight + account.Weight;
            BankAccountModel model = new BankAccountModel();
            model.AccountName = account.AccountName;
            model.AccountNumber = account.AccountNumber;
            model.BSB = account.BSB;
            if (totalWeight > 100)
            {
                ViewBag.Error = "Weight exceed more than 100%";
                model = mda.GetAccountListModel(id);
                return View(model);
            }
            else
            {
                mda.Add(account);
                model = mda.GetAccountListModel(id);
                return View(model);
            }
        }

        public ActionResult CloneMember(int id)
        {

            MemberDA mda = new MemberDA();
            MemberModel member = mda.GetMemeber(id);
            member.IsOperator = true;
            member.Modified_dttm = DateTime.Now;
            member.ModifiedByUser = User.Identity.Name;
            int member_key = 0;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    member_key = mda.CloneMember(member);
                    tran.Complete();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);
                    return View("Search");
                }
            }

            return RedirectToAction("Edit", new { member_key });
        }

        public ActionResult Details(int member_key)
        {
            MemberDA mda = new MemberDA();
            MemberModel model = new MemberModel();
            model = mda.GetMemeber(member_key);
            if (model.DOB != null)
            {
                string dobDate = model.DOB.Value.ToShortDateString();
                string[] dobDateArry = dobDate.Split('/');
                model.DOBstring = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            }
            if (model.PhotoIDExpiry != null)
            {
                string ExpiryDate = model.PhotoIDExpiry.Value.ToShortDateString();
                string[] ExpiryDateArry = ExpiryDate.Split('/');
                model.PhotoIDExpiryString = ExpiryDateArry[2].PadLeft(4, '0') + "-" + ExpiryDateArry[1].PadLeft(2, '0') + "-" + ExpiryDateArry[0].PadLeft(2, '0');
            }
            return View(model);
        }


        //
        // GET: /Administrator/Member/Edit/5

        public ActionResult Edit(int member_key)
        {
            MemberDA mda = new MemberDA();
            MemberModel model = new MemberModel();
            model = mda.GetMemeber(member_key);
            if (model.DOB != null)
            {
                string dobDate = model.DOB.Value.ToShortDateString();
                string[] dobDateArry = dobDate.Split('/');
                model.DOBstring = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            }
            if (model.PhotoIDExpiry != null)
            {
                string ExpiryDate = model.PhotoIDExpiry.Value.ToShortDateString();
                string[] ExpiryDateArry = ExpiryDate.Split('/');
                model.PhotoIDExpiryString = ExpiryDateArry[2].PadLeft(4, '0') + "-" + ExpiryDateArry[1].PadLeft(2, '0') + "-" + ExpiryDateArry[0].PadLeft(2, '0');
            }
                               

            model.RentalMemberRentalTemplateKey = 7;
            TerminalRentalRateDA trda = new TerminalRentalRateDA();
            model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();
            DrawDropDownConfig(model.CompanyKey);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(MemberModel m, FormCollection c, HttpPostedFileBase file)
        {
            MemberModel model = new MemberModel();

            try
            {
                string[] dealer = (c["Dealer"].ToString()).Split('|');
                int count = dealer.Count();
                if (count > 2)
                {
                    model.DealerKey = Convert.ToInt32(dealer[1]);
                }
                int memkey = 0;
                Int32.TryParse(c["Member_key"], out memkey);
                model.Member_key = memkey;


                MemberDA mda = new MemberDA();
                MemberModel MemberEdit = new MemberModel();
                model = mda.GetMemeber(memkey);

                model.BusinesstypeName = m.BusinesstypeName;
                model.MemberId = c["MemberId"].ToString();
                model.FirstName = c["FirstName"].ToString();
                model.LastName = c["LastName"].ToString();
                model.Telephone = c["Telephone"].ToString();
                model.Fax = c["Fax"].ToString();
                model.Mobile = c["Mobile"].ToString();
                model.TradingName = c["BusinesstypeName"].ToString();
                model.ABN = c["ABN"].ToString();
                model.ACN = c["ACN"].ToString();
                model.DriverLicenseNumber = c["DriverLicenseNumber"].ToString();
                model.Email = c["Email"].ToString();
                model.MerchantID = c["Merchant"].ToString();

                DateTime PhotoIDExpiry;
                DateTime.TryParse(c["PhotoIDExpiry"].ToString(), out PhotoIDExpiry);
                if (PhotoIDExpiry != null)
                    model.PhotoIDExpiry = PhotoIDExpiry;

                model.Active = m.Active;
                model.IsBankCustomer = m.IsBankCustomer;
                // model.CompanyKey = Convert.ToInt32(c["BusinessType"].ToString());
                DateTime dob;
                DateTime.TryParse(c["DOB"].ToString(), out dob);
                if (dob != null)
                    model.DOB = dob;
                //model.DOB = Convert.ToDateTime(c["DOB"].ToString());
                //  model.Welcomed = m.Welcomed;
                model.IsOperator = true;
                model.Modified_dttm = DateTime.Now;
                model.ModifiedByUser = User.Identity.Name;
                model.Address = c["Address"].ToString();
                model.City = c["City"].ToString();
                model.State = c["State"].ToString();
                model.Postcode = c["Postcode"].ToString();

                byte[] photoid = new byte[1];
                if (file != null)
                {
                    bool validFile = Regex.IsMatch(file.FileName, "(.*?)\\.(JPG|jpg|PNG|png|JPEG|jpeg)$");
                    if (file.ContentLength > 0 && validFile)
                    {
                        BinaryReader reader = new BinaryReader(file.InputStream);
                        photoid = reader.ReadBytes((int)file.InputStream.Length);
                        model.PhotoID = photoid;
                    }
                }

                MemberDA mem = new MemberDA();
                mem.Update(model);
                CommissionRateDA comRateMaster = new CommissionRateDA();
                if (model.CompanyKey == 1)
                {
                    //add commissionRate
                    decimal CRDebitRatePercentage = 0;
                    Decimal.TryParse(c["DebitRatePercentage"].ToString(), out CRDebitRatePercentage);
                    if (CRDebitRatePercentage != 0)
                        model.CRDebitRatePercentage = CRDebitRatePercentage / 100;

                    comRateMaster.UpdateMemberCommissionRate(model.CRDebitRateKey, model.CRDebitRatePercentage, User.Identity.Name,model.Member_key);

                    decimal MannualVoucherRate = 0;
                    Decimal.TryParse(c["MannualVoucherRate"].ToString(), out MannualVoucherRate);
                    if (MannualVoucherRate != 0)
                        model.CRManualVoucherRate = MannualVoucherRate / 100;
                    decimal MasterCardRate = 0;
                    Decimal.TryParse(c["MasterCardRate"].ToString(), out MasterCardRate);
                    if (MasterCardRate != 0)
                        model.CRMasterCardRate = MasterCardRate / 100;
                    comRateMaster.UpdateMemberCommissionRate(model.CRMasterCardRateKey, model.CRMasterCardRate, User.Identity.Name, model.Member_key);

                    decimal VisaCardRate = 0;
                    Decimal.TryParse(c["VisaCardRate"].ToString(), out VisaCardRate);
                    if (VisaCardRate != 0)
                        model.CRVisaRate = VisaCardRate / 100;
                    comRateMaster.UpdateMemberCommissionRate(model.CRVisaRateKey, model.CRVisaRate, User.Identity.Name, model.Member_key);

                    decimal AmexCardRate = 0;
                    Decimal.TryParse(c["AmexCardRate"].ToString(), out AmexCardRate);
                    if (AmexCardRate != 0)
                        model.CRAmexRate = AmexCardRate / 100;
                    comRateMaster.UpdateMemberCommissionRate(model.CRAmexRateKey, model.CRAmexRate, User.Identity.Name, model.Member_key);

                    decimal DinersCardRate = 0;
                    Decimal.TryParse(c["DinersCardRate"].ToString(), out DinersCardRate);
                    if (DinersCardRate != 0)
                        model.CRDinersRate = DinersCardRate / 100;
                    comRateMaster.UpdateMemberCommissionRate(model.CRDinersRateKey, model.CRDinersRate, User.Identity.Name, model.Member_key);
                }
                else
                {
                    decimal DebitRate = 0;
                    Decimal.TryParse(c["DebitRate"].ToString(), out DebitRate);
                    if (DebitRate != 0)
                        model.CRDebitRate = -DebitRate;
                    comRateMaster.UpdateMemberCommissionRate(model.CRDebitRateKey, model.CRDebitRate, User.Identity.Name, model.Member_key);

                    decimal MasterCardRate = 0;
                    Decimal.TryParse(c["MasterCardRate"].ToString(), out MasterCardRate);
                    if (MasterCardRate != 0)
                        model.CRMasterCardRate = -(MasterCardRate / 100);
                    comRateMaster.UpdateMemberCommissionRate(model.CRMasterCardRateKey, model.CRMasterCardRate, User.Identity.Name, model.Member_key);

                    decimal VisaCardRate = 0;
                    Decimal.TryParse(c["VisaCardRate"].ToString(), out VisaCardRate);
                    if (VisaCardRate != 0)
                        model.CRVisaRate = -(VisaCardRate / 100);
                    comRateMaster.UpdateMemberCommissionRate(model.CRVisaRateKey, model.CRVisaRate, User.Identity.Name, model.Member_key);

                    decimal AmexCardRate = 0;
                    Decimal.TryParse(c["AmexCardRate"].ToString(), out AmexCardRate);
                    if (AmexCardRate != 0)
                        model.CRAmexRate = -(AmexCardRate / 100);
                    comRateMaster.UpdateMemberCommissionRate(model.CRAmexRateKey, model.CRAmexRate, User.Identity.Name, model.Member_key);

                    decimal DinersCardRate = 0;
                    Decimal.TryParse(c["DinersCardRate"].ToString(), out DinersCardRate);
                    if (DinersCardRate != 0)
                        model.CRDinersRate = -(DinersCardRate / 100);
                    comRateMaster.UpdateMemberCommissionRate(model.CRDinersRateKey, model.CRDinersRate, User.Identity.Name, model.Member_key);
                }



                ////Eftpos card
                //decimal rat1 = 0;
                //if (model.CompanyKey == 1)
                //{
                //    rat1 = model.CRDebitRatePercentage;
                //}
                //if (model.CompanyKey == 18)
                //{
                //    rat1 = model.CRDebitRate;

                //}
                //comRateMaster.UpdateMemberCommissionRate(model.CRDebitRatePercentageKey, rat1);

                //  tran.Complete();
                ModelState.AddModelError("error", "Updated!");
                return RedirectToAction("Edit", new { member_key = memkey });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
                return View(model);
            }
            // }
        }

        public ActionResult AddSearchList(MemberSearchModel m, FormCollection c)
        {
            string key1 = c["SearchKey1label"].ToString();
            string value = c["SearchKey1"].ToString();
            SearchListModel sm = new SearchListModel();
            sm.SearchKey = key1;
            sm.SearchField = value;
            m.SearchList.Add(sm);
            return View("Search", m);
        }

        public ActionResult Search()
        {
            MemberSearchModel model = new MemberSearchModel();
            model = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(FormCollection c, string Command, MemberSearchModel m)
        {
            MemberSearchModel model = new MemberSearchModel();
            if (Command == "Search")
            {
                string MemberId = string.Empty;
                string TerminalId = string.Empty;
                string Mobile = string.Empty;
                string BusinessId = string.Empty;
                string FirstName = string.Empty;
                string LastName = string.Empty;
                string Email = string.Empty;
                string TradingName = string.Empty;
                string State = string.Empty;
                string Suburb = string.Empty;

                List<SearchListModel> smlistsession = new List<SearchListModel>();
                smlistsession = (List<SearchListModel>)Session["searchList"];
                SearchDA sda = new SearchDA();
                if (smlistsession.Find(r => r.SearchKey == "MemberId") != null)
                {
                    MemberId = smlistsession.Find(r => r.SearchKey == "MemberId").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "TerminalId") != null)
                {
                    TerminalId = smlistsession.Find(r => r.SearchKey == "TerminalId").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "Mobile") != null)
                {
                    Mobile = smlistsession.Find(r => r.SearchKey == "Mobile").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "BusinessId") != null)
                {
                    BusinessId = smlistsession.Find(r => r.SearchKey == "BusinessId").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "FirstName") != null)
                {
                    FirstName = smlistsession.Find(r => r.SearchKey == "FirstName").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "LastName") != null)
                {
                    LastName = smlistsession.Find(r => r.SearchKey == "LastName").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "Email") != null)
                {
                    Email = smlistsession.Find(r => r.SearchKey == "Email").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "TradingName") != null)
                {
                    TradingName = smlistsession.Find(r => r.SearchKey == "TradingName").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "State") != null)
                {
                    State = smlistsession.Find(r => r.SearchKey == "State").SearchField.ToString();
                }
                if (smlistsession.Find(r => r.SearchKey == "Suburb") != null)
                {
                    Suburb = smlistsession.Find(r => r.SearchKey == "Suburb").SearchField.ToString();
                }
                List<MemberModel> modl = new List<MemberModel>();

                modl = sda.SearchMember(MemberId, FirstName, LastName, TerminalId, Email, Mobile, TradingName, State, Suburb, string.Empty);
                if (modl.Count > 0)
                {
                    model.Members = modl;
                }
                Session["searchList"] = null;
            }
            if (Command == "AddSearch")
            {
                string key1 = c["SearchKey1label"].ToString();
                //int len = key1.Length - 1;
                //string ke = key1.Substring(9, len);
                string value = c["SearchKey1"].ToString();

                if (key1 == string.Empty)
                {
                    ModelState.AddModelError("error", "Enter Search Key");
                    model.SearchList = (List<SearchListModel>)Session["searchList"];
                    return View(model);
                }

                List<SearchListModel> smlist = new List<SearchListModel>();
                SearchListModel sm = new SearchListModel();
                sm.SearchKey = key1;
                sm.SearchField = value;
                smlist.Add(sm);

                List<SearchListModel> smlistsession = new List<SearchListModel>();
                smlistsession = (List<SearchListModel>)Session["searchList"];
                if (smlistsession != null)
                {
                    foreach (var r in smlistsession)
                    {
                        SearchListModel smsession = new SearchListModel();
                        smsession.SearchKey = r.SearchKey;
                        smsession.SearchField = r.SearchField;
                        smlist.Add(smsession);
                    }
                }

                Session["searchList"] = smlist;
                model.SearchList = smlist;

            }
            return View(model);
        }

        public ActionResult EditMemberRentalAmount(int MemberTerminalRentalKey)
        {
              MemberTerminalRentalModel model = new MemberTerminalRentalModel();

            if(MemberTerminalRentalKey != null || MemberTerminalRentalKey !=0)
            {
                MemberTerminalRentalDA mtr = new MemberTerminalRentalDA();
                model = mtr.GetMemberTerminalRental(MemberTerminalRentalKey);
            }
            return View(model);

        }

        [HttpPost]
        public ActionResult EditMemberRentalAmount(int MemberTerminalRentalKey)
        {
            MemberTerminalRentalModel model = new MemberTerminalRentalModel();

            if (MemberTerminalRentalKey != null || MemberTerminalRentalKey != 0)
            {
                MemberTerminalRentalDA mtr = new MemberTerminalRentalDA();
                model = mtr.GetMemberTerminalRental(MemberTerminalRentalKey);
            }
            return View(model);

        }

        public ActionResult AddMemberRental(int id, int Companykey)
        {
            MemberRentalDA mda = new MemberRentalDA();
            MemberRentalModel model = new MemberRentalModel();
            Session["Memberkey"] = id;
            Session["Companykey"] = Companykey;
            model = mda.GetMemberRental(id);

            MemberTerminalRentalDA mtrda = new MemberTerminalRentalDA();
            model.MemberCurrentTerminalRentalList = mtrda.GetKeyByMemberKey(id);

            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            
            model.RentalMemberRentalTemplateKey = 7;
            TerminalRentalRateDA trda = new TerminalRentalRateDA();
            model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();
            Session["RateListToSave"] = model.TerminalRentalRateList;
            DrawDropDownConfig(Companykey);
            return View(model);
        }
        
        [HttpPost]
        public ActionResult AddMemberRental(MemberRentalModel m, FormCollection c)
        {        
            int member_key = Convert.ToInt32(Session["Memberkey"]);// mda.GetMemberKey(m.Member.MemberId);
            //get 
            MemberTerminalRentalDA mtrda = new MemberTerminalRentalDA();
           List<MemberTerminalRentalModel> MemberCurrentTerminalRentalList = mtrda.GetKeyByMemberKey(member_key);
           if (MemberCurrentTerminalRentalList != null)
           {
               foreach (var mCurrent in MemberCurrentTerminalRentalList)
               {
                   mtrda.UpdateEndDate(mCurrent.MemberTerminalRentalKey,DateTime.Now,User.Identity.Name);
               }
           }
            
            int Companykey = Convert.ToInt32(Session["Companykey"]);
            DateTime StartDate = DateTime.MinValue;
            DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
            if (StartDate != DateTime.MinValue)
            {
                StartDate = Convert.ToDateTime(c["StartDate"].ToString());
            }

            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(c["EndDate"].ToString(), out endDate);
            if (endDate != DateTime.MinValue)
            {
                endDate = Convert.ToDateTime(c["EndDate"].ToString());
            }
            if (m.RentalRentFree != true)
            {
                int MemberRentalTemplateKey = m.RentalMemberRentalTemplateKey.Value;// Convert.ToInt32(c["MonthlyRental"].ToString());
                List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                //TerminalRentalRateDA trda = new TerminalRentalRateDA();
                MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
                mtra.Add(TerminalRentalRateList, member_key, StartDate, endDate, User.Identity.Name);
            }
            else
            {
                MemberRentalDA mrda = new MemberRentalDA();
                MemberRentalModel m1 = new MemberRentalModel();
                m1.Member_key = member_key;
                m1.RentFree = true;
                if (StartDate != DateTime.MinValue)
                {
                    m1.StartDate = StartDate;
                }
                if (endDate != DateTime.MinValue)
                {
                    m1.EndDate = endDate;
                }
                m1.ModifiedByUser = User.Identity.Name;
                m1.Modified_dttm = DateTime.Now;
                m1.RentalAmount = 0;
                //m1.RentalTurnoverRangeKey = t.RentalTurnoverRangeKey;
                mrda.AddNew(m1);
            }
            return RedirectToAction("AddMemberRental", new { member_key, Companykey });
        }

        public ActionResult AddAllocateMemberTerminal(int id)
        {
            TerminalAllocationDA mda = new TerminalAllocationDA();
            TerminalAllocationModel model = new TerminalAllocationModel();
            Session["Memberkey"] = id;
            model = mda.GetTerminalAllocation(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult AddAllocateMemberTerminal(TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            ViewBag.Error = "";
            string Term_key = c["EftTerminal_key"].ToString();
            TerminalModel term = tda.CheckTerminalExists(Term_key);
            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
            string state = c["State"].ToString();
            if (state == "Select State")
            {
                ViewBag.Error = "Select State";
                return View(m);
            }
            int VehicleTypekey = Convert.ToInt32(c["VehicleType"].ToString());
            if (VehicleTypekey == 0)
            {
                ViewBag.Error = "Select Vehicle Type";
                return View(m);
            }
            try
            {
                var c1 = c["SaleReps"].ToString();
                string[] sr = c1.Split(',');
                TerminalAllocationModel account = new TerminalAllocationModel();
                account.VehicleModel = new VehicleModel();
                account.VehicleModel.VehicleId = c["VehicleId"].ToString();// m.VehicleModel.VehicleId;
                account.EftTerminal_key = term.EftTerminal_key;
                account.Member_key = 0; //Convert.ToInt32(sr[1]);
                account.EffectiveDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());
                account.IsCurrent = true;
                account.ModifiedByUser = User.Identity.Name;
                account.DeliveryRequired = m.DeliveryRequired;
                account.ServiceFee = m.ServiceFee;
                account.AllowMOTO = m.AllowMOTO;
                account.AllowRefund = m.AllowRefund;
                account.VehicleModel.RegisteredState = c["State"].ToString();
                account.EftTerminalId = Convert.ToInt32(term.TerminalId);

                account.VehicleModel.TerminalAllocatedDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());

                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];

                account.VehicleModel.VehicleType_key = Convert.ToInt32(c["VehicleType"].ToString());
                account.VehicleModel.CreatedDate = DateTime.Now;

                List<TerminalAllocationModel> TAListInternal = new List<TerminalAllocationModel>();
                TAListInternal = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];

                if (TAListInternal == null)
                {
                    TAListInternal = new List<TerminalAllocationModel>();
                }
                TAListInternal.Add(account);

                Session["TerminalAllocationList"] = TAListInternal;


                List<TerminalAllocationModel> TAListInternal1 = (List<TerminalAllocationModel>)Session["TerminalAllocationDate"];

                //add Vehicle and TerminalAllocation
                if (TAListInternal != null)
                {
                    foreach (var t in TAListInternal)
                    {
                        VehicleDA vda = new VehicleDA();
                        VehicleModel v1 = new VehicleModel();
                        v1.VehicleId = t.VehicleModel.VehicleId;
                        v1.RegisteredState = t.VehicleModel.RegisteredState;
                        v1.TerminalAllocatedDate = t.VehicleModel.TerminalAllocatedDate;
                        //  m1.Modified_dttm = c["BsbNumber"].ToString();
                        v1.Operator_key = m.Member_key;
                        v1.VehicleType_key = t.VehicleModel.VehicleType_key;
                        v1.CreatedDate = DateTime.Now;

                        int veh_Key = vda.AddNew(v1);
                        TerminalAllocationDA mda = new TerminalAllocationDA();
                        TerminalAllocationModel m1 = new TerminalAllocationModel();
                        m1.EftTerminal_key = t.EftTerminal_key;
                        m1.Member_key = m.Member_key;
                        m1.EffectiveDate = t.EffectiveDate;
                        m1.IsCurrent = true;
                        m1.ModifiedByUser = User.Identity.Name;
                        m1.DeliveryRequired = t.DeliveryRequired;
                        m1.ServiceFee = t.ServiceFee;
                        m1.AllowMOTO = t.AllowMOTO;
                        m1.AllowRefund = t.AllowRefund;
                        m1.Vehicle_key = veh_Key;
                        int termAlloc_Key = mda.AddNew(m1);

                        TerminalSalesDA sda = new TerminalSalesDA();
                        TerminalSalesModel s1 = new TerminalSalesModel();
                        s1.TerminalAllocation_key = termAlloc_Key;
                        s1.Employee_key = t.Member_key.Value;
                        s1.Active = true;
                        s1.SaleRelationship_key = 1;
                        s1.ModifiedByUser = User.Identity.Name;
                        s1.Modified_dttm = DateTime.Now;

                        mda.AddNew(m1);
                    }
                }

                TerminalAllocationDA mda1 = new TerminalAllocationDA();
                TerminalAllocationModel model = new TerminalAllocationModel();
                Session["Memberkey"] = m.Member_key;
                model = mda1.GetTerminalAllocation(m.Member_key.Value);
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
        }

        public ActionResult CreateAllocateMemberTerminal()
        {
            DrawDropDownConfigAllocateTerminal();
            TerminalAllocationModel model = new TerminalAllocationModel();
            VehicleModel md = new VehicleModel();
            model.VehicleModel = md;
            model.VehicleModel.VehicleType_key = 19;
            
            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.EffectiveDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');


            if (Session["AdrList"] != null)
            {
                List<CAddress> CAddresses = (List<CAddress>)Session["AdrList"];

                var state = (from s in CAddresses
                             where s.AddressType == "Primary"
                             select s.State).FirstOrDefault();

                StateDA sda = new StateDA();
                var stateId = sda.GetStateKey(state);

                model.State_key = stateId;
            }

            model.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];

            return View(model);
        }
        [HttpPost]
        public ActionResult CreateAllocateMemberTerminal(TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            ViewBag.Error = "";
            int empkey = 0;
            string Term_key = c["EftTerminal_key"].ToString();
            TerminalModel term = tda.CheckTerminalExists(Term_key);
            bool IsTerminalAllocated = tda.IsTerminalAllocated(Term_key);
            
            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
            if (IsTerminalAllocated)
            {
                ViewBag.Error = "Terminal already allocated";
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
            int state = m.State_key.Value;// c["State"].ToString();
            if (state == 0)
            {
                ViewBag.Error = "Select State";
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }

            int VehicleTypekey = m.VehicleModel.VehicleType_key.Value;// Convert.ToInt32(c["VehicleType"].ToString());
            if (VehicleTypekey == 0)
            {
                ViewBag.Error = "Select Vehicle Type";
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
            var c1 = c["SaleReps"].ToString();
            if (c1.Length > 0)
            {
                try
                {
                    string[] employeeArray = c1.Split(',');
                    int memberk = Convert.ToInt32(employeeArray[1]);
                    EmployeeDA eda = new EmployeeDA();
                    empkey = eda.GetEmployeeById(memberk);
                    if (empkey == 0)
                    {
                        ViewBag.Error = "Sales rep does not seem correct";
                        m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                        return View(m);
                    }
                }
                catch
                {
                    ViewBag.Error = "Sales rep does not seem correct";
                    m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                    return View(m);
                }                
            }
            try
            {
                TerminalAllocationModel account = new TerminalAllocationModel();
                account.VehicleModel = new VehicleModel();
                account.VehicleModel.VehicleId = c["VehicleId"].ToString();// m.VehicleModel.VehicleId;
                account.EftTerminal_key = term.EftTerminal_key;
                account.Member_key = empkey;
                account.EffectiveDate =  Convert.ToDateTime(c["TerminalAllocationDate"].ToString());
                account.IsCurrent = true;
                account.ModifiedByUser = User.Identity.Name;
                account.DeliveryRequired = m.DeliveryRequired;
                account.ServiceFee = m.ServiceFee;
                account.AllowMOTO = m.AllowMOTO;
                account.AllowRefund = m.AllowRefund;

                StateDA sda = new StateDA();
                StateModel smodel = new StateModel();
                smodel = sda.Get(m.State_key.Value);

                account.VehicleModel.RegisteredState = smodel.StateName;
                account.EftTerminalId = Convert.ToInt32(term.TerminalId);

                account.VehicleModel.TerminalAllocatedDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());

                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];

                account.VehicleModel.VehicleType_key = m.VehicleModel.VehicleType_key;// Convert.ToInt32(c["VehicleType"].ToString());
                account.VehicleModel.CreatedDate = DateTime.Now;

                List<TerminalAllocationModel> TAListInternal = new List<TerminalAllocationModel>();
                TAListInternal = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];

                if (TAListInternal == null)
                {
                    TAListInternal = new List<TerminalAllocationModel>();
                }
                TAListInternal.Add(account);

                Session["TerminalAllocationList"] = TAListInternal;
                TerminalAllocationModel model = new TerminalAllocationModel();
                model.TerminalAllocationList = TAListInternal;
                DrawDropDownConfigAllocateTerminal();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                m.TerminalAllocationList = (List<TerminalAllocationModel>)Session["TerminalAllocationList"];
                return View(m);
            }
            // return View(model);
        }

        public ActionResult CreateMemberRental()
        {
            MemberRentalModel model = new MemberRentalModel();
            model.MemberRentalModelList = (List<MemberRentalModel>)Session["MemberRentalModelList"];
            MemberRentalModel SFmodel = new MemberRentalModel();
            SFmodel = (MemberRentalModel)Session["SalesforceRentalModel"];
            if (SFmodel != null)
            {
                model.RentalAmount = SFmodel.RentalAmount;
                model.RentalTurnoverRangeKey = SFmodel.RentalTurnoverRangeKey;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateMemberRental(MemberRentalModel m, FormCollection c)
        {

            MemberRentalModel account = new MemberRentalModel();

            account.RentFree = m.RentFree;
            DateTime StartDate = DateTime.MinValue;
            DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
            if (StartDate != DateTime.MinValue)
            {
                account.StartDate = Convert.ToDateTime(c["StartDate"].ToString());
            }
            
            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(c["EndDate"].ToString(), out endDate);
            if (endDate != DateTime.MinValue)
            {
                account.EndDate = Convert.ToDateTime(c["EndDate"].ToString());
            }
            account.ModifiedByUser = User.Identity.Name;
            account.Modified_dttm = DateTime.Now;
            account.RentalAmount = Convert.ToDecimal(c["RentalAmount"].ToString());
            account.RentalTurnoverRangeKey = Convert.ToInt32(c["RentalTurnoverRange"].ToString());
            account.RentalTurnoverRangeName = c["RentalTurnoverRange"].ToString();

            List<MemberRentalModel> TAListInternal = new List<MemberRentalModel>();
            TAListInternal = (List<MemberRentalModel>)Session["MemberRentalModelList"];

            if (TAListInternal == null)
            {
                TAListInternal = new List<MemberRentalModel>();
            }
            TAListInternal.Add(account);

            Session["MemberRentalModelList"] = TAListInternal;
            MemberRentalModel model = new MemberRentalModel();
            model.MemberRentalModelList = TAListInternal;
            return View(model);
        }


        public ActionResult GetPhoto(int memberkey)
        {
            MemberDA mda = new MemberDA();
            byte[] photo = mda.GetMemberPhoto(memberkey);
            if (photo == null)
            {
                try
                {
                    photo = System.IO.File.ReadAllBytes("PhoenixWeb/Images/LIV002_LiveGroupLogo_CMYK.png");
                }
                catch
                { }
            }
            return File(photo, "image/jpeg");
        }
    }
}
