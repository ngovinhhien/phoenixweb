﻿using PhoenixWeb.Site.ReportingWebService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PhoenixWeb.Site.Models
{
    public class Common
    {
        public static void RenderReport(string reportName,string format, IList<PhoenixWeb.Site.ReportingWebService.ParameterValue> parameters, out byte[] output, out string extension, out string mimeType, out string encoding, out Warning[] warnings, out string[] streamIds)
        {
            using (var webServiceProxy = new ReportExecutionServiceSoapClient("ReportExecutionServiceSoap"))
            {
                try
                {
                    // IList<ParameterValue> parameters = new List<ParameterValue>();
                    //string reportName = ConfigurationManager.AppSettings.Get("ReportServerPath") + report;
                    NetworkCredential clientCredentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("ReportDomainAdmin"), ConfigurationManager.AppSettings.Get("ReportDomainPassword"), ConfigurationManager.AppSettings.Get("ReportDomain"));

                    webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                    webServiceProxy.ClientCredentials.Windows.ClientCredential = clientCredentials;
                    
                    // Init Report to execute
                    ServerInfoHeader serverInfoHeader;
                    ExecutionInfo executionInfo;
                    ExecutionHeader executionHeader = webServiceProxy.LoadReport(null, reportName, null, out serverInfoHeader, out executionInfo);
                    // Attach Report Parameters
                    webServiceProxy.SetExecutionParameters(executionHeader, null, parameters.ToArray(), "en-au", out executionInfo);
                    // Render
                  //  webServiceProxy.Render(executionHeader, null, "HTML4.0", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                    webServiceProxy.Render(executionHeader, null, "PDF", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                   
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

        }
    }
}
