﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using log4net;
using System.Reflection;
using log4net.Config;

namespace Live.Util.TraceListeners
{
    /// <summary>
    /// Tracelistener class which translates standard .net trace calls
    /// to log4net calls. This wrapper allows the capture of trace information
    /// without rewriting code to specifically call log4net functions
    /// </summary>
    public class Log4NetTraceListener : TraceListener
    {
        private ILog _logger;
        private TraceEventType lastEventType = TraceEventType.Information;

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetTraceListener" /> class.
        /// </summary>
        /// <param name="logName">Name of the log.</param>
        public Log4NetTraceListener(string logName)
        {
            _logger = LogManager.GetLogger(logName);
        }

        /*
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            switch (eventType)
            {
                case TraceEventType.Warning:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.WarnFormat(format, args);
                    break;

                case TraceEventType.Error:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.ErrorFormat(format, args);
                    break;

                case TraceEventType.Critical:
                    _logger.FatalFormat(format, args);
                    break;

                default:
                case TraceEventType.Information:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.InfoFormat(format, args);
                    break;
            }
        }
        */

        public override void WriteLine(string message)
        {
            switch (lastEventType)
            {
                case TraceEventType.Warning:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.Warn(message);
                    break;

                case TraceEventType.Error:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.Error(message);
                    break;

                case TraceEventType.Critical:
                    _logger.Fatal(message);
                    break;

                default:
                case TraceEventType.Information:
                    //format = format += "\r\nStack Trace: " + GetStackTrace();
                    _logger.Info(message);
                    break;
            }
        }
        
        public override void Write(string message)
        {
            if (message.Contains("Information"))
                lastEventType = TraceEventType.Information;
            else if (message.Contains("Warning"))
                lastEventType = TraceEventType.Warning;
            else if (message.Contains("Error"))
                lastEventType = TraceEventType.Error;
            else
                lastEventType = TraceEventType.Information;
        }

        private string GetStackTrace()
        {
            // TODO: this doesnt seem to work properly. Do we need it?
            StackTrace stackTrace = new StackTrace();
            return stackTrace.ToString();
        }
    }
}
