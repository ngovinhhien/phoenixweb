﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
namespace LiveEncryption
{
    public static class LiveEncryptionHelper
    {
        public static string EncryptPassword(string Password)
        {
            if (!string.IsNullOrEmpty(Password))
            {
                return SHA256Hash(Password);
            }
            else
                throw new System.Security.Cryptography.CryptographicException("Input password is null or empty");
        }
        public static string SHA256Hash(string plaintext)
        {
            if (!string.IsNullOrEmpty(plaintext))
            {
                byte[] pass = System.Text.ASCIIEncoding.UTF8.GetBytes(plaintext);
                pass = new SHA256Managed().ComputeHash(pass);
                return ASCIIEncoding.UTF8.GetString(pass);
            }
            else
                throw new System.Security.Cryptography.CryptographicException("Plaintext string is null or empty");
        }

        public static string TripleDESEncypt(string plaintext, string key)
        {
            if (String.IsNullOrEmpty( key))
            {
                throw new System.Security.Cryptography.CryptographicException("Key cannot be null or empty");
            }
            else if(key.Length!=24)
            {
                throw new CryptographicException("Key length need to be 24 characters");
            }
            else
            {
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(plaintext);



                keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;
                //mode of operation. there are other 4 modes.
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;
                //padding mode(if any extra byte added)

                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                //transform the specified region of bytes array to resultArray
                byte[] resultArray =
                  cTransform.TransformFinalBlock(toEncryptArray, 0,
                  toEncryptArray.Length);
                //Release resources held by TripleDes Encryptor
                tdes.Clear();
                //Return the encrypted data into unreadable string format
                //return ASCIIEncoding.UTF8.GetString(resultArray);
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);

            }
        }


        public static string TripleDESEncyptNoPadding(string plaintext, string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new System.Security.Cryptography.CryptographicException("Key cannot be null or empty");
            }
            else if (key.Length != 24)
            {
                throw new CryptographicException("Key length need to be 24 characters");
            }
            else
            {
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(plaintext);



                keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;
                //mode of operation. there are other 4 modes.
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;
                //padding mode(if any extra byte added)

                tdes.Padding = PaddingMode.Zeros;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                //transform the specified region of bytes array to resultArray
                byte[] resultArray =
                  cTransform.TransformFinalBlock(toEncryptArray, 0,
                  toEncryptArray.Length);
                //Release resources held by TripleDes Encryptor
                tdes.Clear();
                //Return the encrypted data into unreadable string format
                //return ASCIIEncoding.UTF8.GetString(resultArray);
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);

            }
        }


        public static string TripleDESDecrypt(string cipherString, string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new System.Security.Cryptography.CryptographicException("Key cannot be null or empty");
            }
            else if (key.Length != 24)
            {
                throw new CryptographicException("Key length need to be 24 characters");
            }
            else
            {
                byte[] keyArray;
                //get the byte code of the string

                byte[] toEncryptArray = Convert.FromBase64String(cipherString);
       
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;
                //mode of operation. there are other 4 modes. 
                //We choose ECB(Electronic code Book)

                tdes.Mode = CipherMode.ECB;
                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(
                                     toEncryptArray, 0, toEncryptArray.Length);
                //Release resources held by TripleDes Encryptor                
                tdes.Clear();
                //return the Clear decrypted TEXT
                return UTF8Encoding.UTF8.GetString(resultArray);

            }
        }
        public static string TripleDESDecryptNoPadding(string cipherString, string key)
        {
            if (String.IsNullOrEmpty(key))
            {
                throw new System.Security.Cryptography.CryptographicException("Key cannot be null or empty");
            }
            else if (key.Length != 24)
            {
                throw new CryptographicException("Key length need to be 24 characters");
            }
            else
            {
                byte[] keyArray;
                //get the byte code of the string

                byte[] toEncryptArray = Convert.FromBase64String(cipherString);

                keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;
                //mode of operation. there are other 4 modes. 
                //We choose ECB(Electronic code Book)

                tdes.Mode = CipherMode.ECB;
                //padding mode(if any extra byte added)
                tdes.Padding = PaddingMode.Zeros;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(
                                     toEncryptArray, 0, toEncryptArray.Length);
                //Release resources held by TripleDes Encryptor                
                tdes.Clear();
                //return the Clear decrypted TEXT
                return UTF8Encoding.UTF8.GetString(resultArray);

            }
        }
    }
}
