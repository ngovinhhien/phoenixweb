﻿using System.Diagnostics;
using Live.Util.TraceListeners;
using System.IO;
using log4net.Config;

namespace PhoenixLog
{
    public interface IPhoenixLog
    {
        void LogInfo(string info);
        //{

        //    Trace.TraceInformation(String.Format("Log info for Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, info));
        //}
        void LogError(string errormessage);
        //{
        //    Trace.TraceError(String.Format("Error processing Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, errormessage));
        //}
        void LogWarning(string warning);
        //{
        //    Trace.TraceError(String.Format("Warning processing Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, warning));
        //}
    }
    public abstract class PhoenixLogger: IPhoenixLog
    {
        public void LogInfo(string info)
        {
            Trace.TraceInformation(info);
        }
        public void LogError(string errormessage)
        {
            Trace.TraceError(errormessage);
        }
        public void LogWarning(string warning)
        {
            Trace.TraceError(warning);
        }
    }
    


    public class PhoenixLog4Net : PhoenixLogger
    {
        protected static Log4NetTraceListener log4netListener = null;
        
        public PhoenixLog4Net(string Log4NetConfigFilePath="")
        {
           
            if (log4netListener == null)
            {
                log4netListener = new Log4NetTraceListener(System.AppDomain.CurrentDomain.FriendlyName);
                if (!Trace.Listeners.Contains(log4netListener))
                    Trace.Listeners.Add(log4netListener);

                FileInfo info = new FileInfo(Path.Combine(Log4NetConfigFilePath, "log4net.config"));

                XmlConfigurator.ConfigureAndWatch(info);
            }
        }
        
    }
}
