﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace PhoenixLog
{
    public abstract class PhoenixLogFactory
    {
        //protected PhoenixLog logger;
        public abstract PhoenixLogger CreateLogger(string configfile = "");

    }

    public class Log4NetFactory : PhoenixLogFactory
    {
        public override PhoenixLogger CreateLogger(string Log4NetFilePath = "")
        {
            if (String.IsNullOrEmpty(Log4NetFilePath))
                Log4NetFilePath = Directory.GetCurrentDirectory();
            PhoenixLog4Net logger = new PhoenixLog4Net(Log4NetFilePath);
            return logger;
        }
    }
}
