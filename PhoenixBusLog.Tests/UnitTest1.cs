﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phoenix_BusLog.Data;
using System.Linq;

namespace PhoenixBusLog.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Check_Rewards_Penidng_List_Count()
        {
            RewardsDA rewardsDA = new RewardsDA();
            int count = rewardsDA.GetPending().Count();

            Assert.AreEqual(count >= 0, true);
        }
    }
}
