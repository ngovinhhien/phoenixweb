﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Base
{
    public class BaseModel
    {
        public string CurrentModule { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
