﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Base
{
    public class BasePagingModel
    {
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
