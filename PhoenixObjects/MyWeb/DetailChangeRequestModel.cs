﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.MyWeb
{
   public class DetailChangeRequestModel
    {
        public int RequestID { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string New_Street { get; set; }
        public string New_StreetNo { get; set; }
        public string New_Suburb { get; set; }
        public string New_State { get; set; }
        public Nullable<int> New_PostCode { get; set; }
        public string New_Phone { get; set; }
        public string New_Mobile { get; set; }
        public string New_Email { get; set; }
        public string New_Fax { get; set; }

        public string Old_Street { get; set; }
        public string Old_Suburb { get; set; }
        public string Old_State { get; set; }
        public Nullable<int> Old_PostCode { get; set; }
        public string Old_Phone { get; set; }
        public string Old_Mobile { get; set; }
        public string Old_Email { get; set; }
        public string Old_Fax { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public string CompleteBy { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string CheckedBy { get; set; }
        public Nullable<System.DateTime> CheckedDate { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public byte[] SupportDoc1 { get; set; }
        public byte[] SupportDoc2 { get; set; }
    }

   public class DetailChgReqImageModel
   {
       public byte[] SupportDoc { get; set; }
       public string Extension { get; set; }
   }
}
