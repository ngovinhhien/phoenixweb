﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.MyWeb
{
   public class MyWebUserProfileModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string MemberId { get; set; }
        public string TerminalId { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public Nullable<int> UserMembershipKeySelected { get; set; }
        public Nullable<int> UserIdSelected { get; set; }
        public Nullable<int> UserIdCreatedBy { get; set; }
        public Nullable<int> UserIdUpdatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string APIKey { get; set; }
    }
}
