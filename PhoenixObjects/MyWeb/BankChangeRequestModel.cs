﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.MyWeb
{
   public class BankChangeRequestModel
    {
        public int RequestID { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string New_AccountName { get; set; }
        public string New_AccountNumber { get; set; }
        public string New_BSB { get; set; }

        public string Old_AccountName { get; set; }
        public string OLd_AccountNumber { get; set; }
        public string Old_BSB { get; set; }
        public float Old_Weight { get; set; }
        public Nullable<int> Account_key { get; set; }

        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public string CompleteBy { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string CheckedBy { get; set; }
        public Nullable<System.DateTime> CheckedDate { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public byte[] SupportDoc1 { get; set; }
        public byte[] SupportDoc2 { get; set; }
    
    }

   public class BankChgReqImageModel
   {
       public byte[] SupportDoc { get; set; }
       public string Extension { get; set; }
   }
}
