﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.MyWeb
{
    public class MyWebUserMembershipModel
    {
        public int UserMembershipKey { get; set; }
        public int UserId { get; set; }
        public int MemberKey { get; set; }
        public string VerificationToken { get; set; }
        public Nullable<System.DateTime> VerificationTokenExpirationDate { get; set; }
        public bool Verified { get; set; }
        public bool Inactive { get; set; }
        public bool IsAdmin { get; set; }
        public int UserIdCreatedBy { get; set; }
        public int UserIdUpdatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    }
}
