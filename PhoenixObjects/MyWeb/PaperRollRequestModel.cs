﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.MyWeb
{
  public  class PaperRollRequestModel
    {
        public int RequestID { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string StreetNo { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public Nullable<int> PostCode { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public string CompleteBy { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string CheckedBy { get; set; }
        public Nullable<System.DateTime> CheckedDate { get; set; }
        public string Status { get; set; }
        public Nullable<int> QuantityRequested { get; set; }
        public Nullable<int> QuantitySent { get; set; }
        public string Comments { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string OrgStreet { get; set; }
        public string TrackingNo { get; set; }
      
    }
}
