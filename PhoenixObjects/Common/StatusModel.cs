﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class StatusModel
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public int? ListOrder { get; set; }
        public string Description { get; set; }
    }
}
