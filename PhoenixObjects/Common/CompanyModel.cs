﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class CompanyModel
    {
        public string Name { get; set; }
        public Nullable<float> ServiceChargeRate { get; set; }
        public string ABN { get; set; }
        public string ACN { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public int Company_key { get; set; }
        public Nullable<bool> MSFApplicable { get; set; }
    }
}
