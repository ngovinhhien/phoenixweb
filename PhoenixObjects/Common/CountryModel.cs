﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class CountryModel : ClientModel
    {

        public string CountryName { get; set; }

        public int CountryKey { get; set; }
    }
    class CountryModelComparer : IEqualityComparer<CountryModel>
    {
        public bool Equals(CountryModel c1, CountryModel c2)
        {
            return (c1.CountryKey == c2.CountryKey && c1.CountryName == c2.CountryName);
        }

        public int GetHashCode(CountryModel country)
        {
            return country.CountryKey.GetHashCode();
        }
    }

}
