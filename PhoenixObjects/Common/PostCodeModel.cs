﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class CAddressList
    {
        [DisplayName("Street Number")]
        public string StreetNumber { get; set; }
        [DisplayName("Street Name")]
        public string StreetName { get; set; }
        public List<CAddress> CAddresses { get; set; }

    }
    public class CAddress
    {
        public int Id { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Suburb { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }

    }
    public class AddressListModel
    {
        public MemberModel Member { get; set; }
        public MemberAddressModel MemberAddress { get; set; }
        [DisplayName("Street Number")]
        public string StreetNumber { get; set; }
        [DisplayName("Street Name")]
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public int Suburb_key { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public int Addresskey { get; set; }

    }
    public class PostCodeModel
    {
        public string PostCode { get; set; }
        public int State_key { get; set; }
        public int PostCodeMaster_key { get; set; }
        public int PostCodeType_Key { get; set; }
    }

}
