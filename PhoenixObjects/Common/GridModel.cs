﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class GridModel<TRequest, TResponse>
         where TRequest : class
         where TResponse : class
    {
        public GridModel()
        {
            PageIndex = 1;
            PageSize = 10;
            Request = default(TRequest);
            Response = default(TResponse);
        }

        public bool IsError { get; set; }
        public string Message { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageIndexFrom { get { return (PageIndex - 1) * PageSize + 1; } }
        public int PageIndexTo { get { return PageIndex * PageSize; } }
        public TRequest Request { get; set; }
        public TResponse Response { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get { return (int)Math.Ceiling((float)TotalItems / PageSize); } }
    }
}
