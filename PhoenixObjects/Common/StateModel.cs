﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class StateModel
    {

        public string StateName { get; set; }
        public int Countrykey { get; set; }
        public int StateKey { get; set; }
        public string Capital { get; set; }
        public string FullName { get; set; }
    }

    class StateModelComparer : IEqualityComparer<StateModel>
    {
        public bool Equals(StateModel c1, StateModel c2)
        {
            return (c1.StateKey == c2.StateKey && c1.StateName == c2.StateName);
        }

        public int GetHashCode(StateModel state)
        {
            return state.StateKey.GetHashCode();
        }
    }
}
