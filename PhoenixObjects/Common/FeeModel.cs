﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{

    public class FeeModel
    {

        public int ID { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public int MemberKey { get; set; }
        public int? FrequencyID { get; set; }
        public string FrequencyName { get; set; }
        public string Description { get; set; }
        public int FeeID { get; set; }
        public DateTime? FeeChargeStartDate { get; set; }
        public DateTime? FeeChargeEndDate { get; set; }
    }

    public class FeeTypeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
    }
}
