﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
  public class VehicleTypeModel
    {
        public string Name { get; set; }
        public int VehicleType_key { get; set; }
        public int Company_Key { get; set; }
    }
}
