﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
   public class RentalTurnOverModel
    {
        public int RentalTurnoverRangeKey { get; set; }
        public Nullable<int> Company_key { get; set; }
        public Nullable<decimal> LowAmount { get; set; }
        public Nullable<decimal> HighAmount { get; set; }
        public string RangeMsg { get; set; }
    }
}
