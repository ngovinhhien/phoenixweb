﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
   public class OfficeModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> Company_key { get; set; }
        public int Office_key { get; set; }
        public Nullable<decimal> CommissionRate { get; set; }
        public Nullable<System.TimeSpan> OfficeOpeningTime { get; set; }
        public Nullable<System.TimeSpan> OfficeClosingTime { get; set; }
        public Nullable<int> OfficeType_key { get; set; }
       
    }
}
