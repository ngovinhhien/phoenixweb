﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class SuburbModel
    {
        public int SuburbKey { set; get; }
        public string SuburbName { get; set; }
        public string PostCode { get; set; }
        public int PostCodeMaster_key { get; set; }
        public StateModel State { get; set; }
        public CountryModel Country { get; set; }
    }

   
}
