﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class APIModel
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string URL { get; set; }
        public string TellerId { get; set; }
        public string VendorBranchId { get; set; }
        public string VendorBranchName { get; set; }

    }
}
