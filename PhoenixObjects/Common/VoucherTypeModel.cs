﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
   public class VoucherTypeModel
    {
        public int VoucherType_key { get; set; }
        public string vouchername { get; set; }
        public int ConversionValue { get; set; }
        public int ExpiryPeriod { get; set; }
        public int Company_key { get; set; }
        public string VoucherPrefix { get; set; }
        public Nullable<int> nextnumber { get; set; }
        public bool UsedLater { get; set; }

        public string TermsConditions { get; set; }
        public Nullable<int> OverrideLimit { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<decimal> TransactionAmountTarget { get; set; }
        public Nullable<int> TerminalAmountTarget { get; set; }
        public Nullable<int> Period { get; set; }
        public Nullable<decimal> CashValue { get; set; }
        public Nullable<bool> MultipleUse { get; set; }
        public Nullable<int> UsagePeriod { get; set; }
        public Nullable<int> UsagePeriodLimit { get; set; }
        public Nullable<bool> RefereeSharing { get; set; }
        public Nullable<bool> ExternalOrder { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Promotion { get; set; }
        public Nullable<decimal> DiscountPct { get; set; }
        public Nullable<int> MinQty { get; set; }
        public Nullable<int> MaxQty { get; set; }
    }
}


      
      