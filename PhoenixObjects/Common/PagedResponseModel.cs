﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class PagedResponseModel<T>
    {
        public int total { get; set; }
        public int page { get; set; }
        public List<T> rows { get; set; }
        public int records { get; set; }
        public bool isError { get; set; }
        public string errorMessage { get; set; }
    }
}
