﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public class VehicleModel
    {
        public VehicleModel()
        {
            VehicleType = new VehicleTypeModel();
        }
        public int Vehicle_key { get; set; }
        public string VehicleId { get; set; }
        public string RegisteredState { get; set; }
        public string RegisteredStateName { get; set; }
        public Nullable<System.DateTime> TerminalAllocatedDate { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public Nullable<int> Operator_key { get; set; }
        public Nullable<int> VehicleType_key { get; set; }
        public string VehicleType_keyName { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public VehicleTypeModel VehicleType { get; set; }
    }
}
