﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{
    public abstract class ClientModel
    {

        public System.DateTime CreatedDate { get; set; }
        public String CreatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public String UpdatedBy { get; set; }

        public int UserKeyCreatedBy { get; set; }
        public int UserKeyUpdatedBy { get; set; }

        public bool Inactive { get; set; }
    }
}
