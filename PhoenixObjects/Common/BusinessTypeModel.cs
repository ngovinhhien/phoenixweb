﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Common
{

    public class BusinessTypeModel
    {
      
        public int BusinessTypeKey
        {
            get;
            set;
        }


        public string BusinessTypeName
        {
            get;
            set;
        }
        public bool? MSFApplicable
        {
            get;
            set;
        }

        public string MerchantId { get; set; }
    }
}
