﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Search
{
    public class SearchTransactionLogModel
    {
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        public int TerminalKey { get; set; }

        public string CardType { get; set; }
        public string BatchNumber { get; set; }
        public double? MinAmount { get; set; }
        public double? MaxAmount { get; set; }
        public bool ApproveStatusBool { get; set; }
        public bool DeclineStatusBool { get; set; }
        public string ApproveStatus { get; set; }
        public bool OnlyDisplayGlideboxTransaction { get; set; }
    }
}
