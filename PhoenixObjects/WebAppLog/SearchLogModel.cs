using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.WebAppLog
{
    public class SearchLogModel
    {
        public int Id { get; set; }
        [Display(Name = "Logged Date From")]
        public DateTime LoggedDateFrom { get; set; }
        [Display(Name = "Logged Date To")]
        public DateTime LoggedDateTo { get; set; }
        [Display(Name = "Thread")]
        public string Thread { get; set; }
        [Display(Name = "Level")]
        public string Level { get; set; }
        [Display(Name = "Logger")]
        public string Logger { get; set; }
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "Exception")]
        public string Exception { get; set; }
    }
}
