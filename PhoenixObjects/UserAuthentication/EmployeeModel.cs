﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
    public class EmployeeModel
    {
        public int Employee_key { get; set; }
        public string EmployeeID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int Role_key { get; set; }
        public Nullable<int> Office_key { get; set; }
        public Nullable<int> Manager_key { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public string PhoenixAdminUsername { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> CRMCommentType_key { get; set; }

    }
}
