﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
   public class esAppModel
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public string AppDescription { get; set; }
        public string AppURL { get; set; }
        public Nullable<System.Guid> AppId { get; set; }
        public string SecretKey { get; set; }
        public Nullable<decimal> ExpiryTime { get; set; }
    }
}
