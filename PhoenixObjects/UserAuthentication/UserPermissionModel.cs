﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
   public class UserPermissionModel
    {
        public int RoleId { get; set; }
        public List<PermissionModel> PermissionList { get; set; }     

    }
   public class PermissionModel
    {
        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
        //public bool IsCheck { get; set; }    
    }
    
}
