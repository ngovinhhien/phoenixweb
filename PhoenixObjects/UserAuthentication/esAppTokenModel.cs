﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
   public class esAppTokenModel
    {
        public int AppTokenId { get; set; }
        public Nullable<System.Guid> Token { get; set; }
        public string Username { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.Guid> AppId { get; set; }
        public Nullable<System.DateTime> LastVisit { get; set; }
        public Nullable<int> esAppsID { get; set; }
    }
}
