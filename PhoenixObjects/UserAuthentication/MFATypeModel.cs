﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
    public class MFATypeModel
    {
        public int MFATypeId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SecretKey { get; set; }
        public string PublicKey { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDefault { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
