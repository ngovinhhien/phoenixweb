﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
    public class UserResetPasswordRequestModel
    {
        public int UserResetPasswordRequestId { get; set; }
        public System.Guid UserId { get; set; }
        public System.Guid ResetPasswordToken { get; set; }
        public System.DateTime ExpiresBy { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string LastEmailUsed { get; set; }
        public bool IsExpiredToken { get { return ExpiresBy < DateTime.Now; } }
    }
}
