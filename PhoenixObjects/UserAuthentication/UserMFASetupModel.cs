﻿using PhoenixObjects.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
    public partial class UserMFASetupModel : BaseModel
    {
        public int UserMFASetupId { get; set; }
        public System.Guid UserId { get; set; }
        public int MFATypeId { get; set; }
        public string SetupField1 { get; set; }
        public string SetupField2 { get; set; }
        public string SetupField3 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public  bool IsAdminChangePassword { get; set; }
    }
}
