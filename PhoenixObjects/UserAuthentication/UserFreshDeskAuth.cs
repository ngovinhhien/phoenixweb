﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.User
{
    public class UserFreshDeskAuthModel
    {
        public int UserFreshDeskAuthId { get; set; }
        public System.Guid UserId { get; set; }
        public string FreshDeskAPIKey { get; set; }
        public string Password { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
