﻿using PhoenixObjects.Administrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Profile;
using System.Web.Security;

namespace PhoenixObjects.UserAuthentication
{
    public class UserProfileModel : ProfileBase
    {
        //static public UserProfileModel CurrentUser
        //{
        //    get
        //    {
        //        return (UserProfileModel)
        //               (ProfileBase.Create(Membership.GetUser().UserName));
        //    }
        //}
        //public string FullName
        //{
        //    get { return ((string)(base["FullName"])); }
        //    set { base["FullName"] = value; Save(); }
        //}
        public Guid UserId { get; set; }
        public new string UserName { get; set; }
        public List<RoleModel> Roles { get; set; }
        public string Office { get; set; }
        public int OfficeType_Key { get; set; }
        public List<string> UsersRoleList { get; set; }
        public int? Dealerkey { get; set; }
        public new DateTime LastActivityDate { get; set; }
        public new DateTime LastUpdatedDate { get; set; }
        public string FreshDeskAPI { get; set; }
    }

    
    public class Customers
    {
        public int CustomerSearchBy { get; set; }
        public string CustomerName { get; set; }
        public long? CustomerKey { get; set; }
        public string CustomerId { get; set; }
        public int? TerminalKey { get; set; }
        public string TerminalId { get; set; }
        public int CustomerType { get; set; }
        public int? CompanyType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TradingName { get; set; }
        public string ABN { get; set; }
        public string ACN { get; set; }
        public string DriverLicenseNumber { get; set; }
        public bool Active { get; set; }
        public byte[] Photo { get; set; }
    }
}
