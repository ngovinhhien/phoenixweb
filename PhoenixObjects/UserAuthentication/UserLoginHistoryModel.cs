﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
   public class UserLoginHistoryModel
    {
        public int Id { get; set; }
        public string IPAddress { get; set; }
        public Nullable<System.DateTime> AttemptDate { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public string ApplicationName { get; set; }
        public Nullable<System.DateTime> AttempDateTime { get; set; }
    }
}
