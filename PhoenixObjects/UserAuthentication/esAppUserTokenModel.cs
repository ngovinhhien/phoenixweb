﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.UserAuthentication
{
   public class esAppUserTokenModel
    {
        public int Id { get; set; }
        public Nullable<int> AppTokenId { get; set; }
        public Nullable<System.Guid> Token { get; set; }
        public Nullable<System.DateTime> LastVisit { get; set; }
    }
}
