﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;

namespace PhoenixObjects.UserAuthentication
{
    public class PhoenixUser
    {
        public string Username { get; set; }
        public OfficeModel office { get; set; }
        public string Email { get; set; }
        public string UserID { get; set; }
    }
}
