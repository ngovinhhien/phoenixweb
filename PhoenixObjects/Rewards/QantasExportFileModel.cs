﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class QantasExportFileModel
    {
        public QantasExportFileModel()
        {
            this.QantasExportFileApprovers = new HashSet<QantasExportFileApproverModel>();
        }

        public int QantasExportFileKey { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> FileDate { get; set; }
        public Nullable<System.DateTime> ExportDateTime { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> QantasExportFileStatusId { get; set; }

        public virtual QantasExportFileStatusModel QantasExportFileStatus { get; set; }
        public virtual ICollection<QantasExportFileApproverModel> QantasExportFileApprovers { get; set; }
        //public virtual ICollection<QantasFileEntryModel> QantasFileEntries { get; set; }
    }
}
