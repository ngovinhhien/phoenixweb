﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class QantasExportFileStatusModel
    {
        public QantasExportFileStatusModel()
        {
            this.QantasExportFiles = new HashSet<QantasExportFileModel>();
        }

        public int QantasExportFileStatusId { get; set; }
        public string Name { get; set; }
        public Nullable<int> NextStatus { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<QantasExportFileModel> QantasExportFiles { get; set; }
    }
}
