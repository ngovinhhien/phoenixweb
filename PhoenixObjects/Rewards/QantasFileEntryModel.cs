﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class QantasFileEntryModel
    {
        public int QantasFileEntryKey { get; set; }
        public Nullable<int> QantasExportFileKey { get; set; }
        public Nullable<int> MemberReferenceEntryKey { get; set; }
        public Nullable<int> Points { get; set; }
        public string UploadStatus { get; set; }
        public string QBRNumber { get; set; }
        public Nullable<int> QantasExportFileEntryStatusId { get; set; }

        public virtual MemberReferenceEntryModel MemberReferenceEntry { get; set; }
        public virtual QantasExportFileEntryStatusModel QantasExportFileEntryStatus { get; set; }
    }
}
