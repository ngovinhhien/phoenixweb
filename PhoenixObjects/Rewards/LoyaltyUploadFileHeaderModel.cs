﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class LoyaltyUploadFileHeaderModel
    {
        public LoyaltyUploadFileHeaderModel()
        {
            this.LoyaltyUploadFileLineItems = new HashSet<LoyaltyUploadFileLineItemModel>();
            this.LoyaltyExportFiles = new HashSet<LoyaltyExportFileModel>();
        }

        public int LoyaltyUploadFileHeaderId { get; set; }
        public string FileName { get; set; }
        public int OrganisationforLoyaltyId { get; set; }
        public Nullable<bool> IsReadyToProcess { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<LoyaltyUploadFileLineItemModel> LoyaltyUploadFileLineItems { get; set; }
        public virtual ICollection<LoyaltyExportFileModel> LoyaltyExportFiles { get; set; }
    }
}
