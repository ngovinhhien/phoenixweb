﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class LoyaltyUploadFileLineItemModel
    {
        public LoyaltyUploadFileLineItemModel()
        {
            this.LoyaltyExportFileLineItems = new HashSet<LoyaltyExportFileLineItemModel>();
        }

        public int LoyaltyUploadFileLineItemId { get; set; }
        public int LoyaltyUploadFileHeaderId { get; set; }
        public int Points { get; set; }
        public string MemberId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public string LoyaltyNumber { get; set; }

        public virtual LoyaltyUploadFileHeaderModel LoyaltyUploadFileHeader { get; set; }

        public virtual ICollection<LoyaltyExportFileLineItemModel> LoyaltyExportFileLineItems { get; set; }
    }
}
