﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class QantasExportFileEntryStatusModel
    {
        public QantasExportFileEntryStatusModel()
        {
            //this.QantasFileEntries = new HashSet<QantasFileEntryModel>();
        }

        public int QantasExportFileEntryStatusId { get; set; }
        public string Name { get; set; }
        public Nullable<int> NextStatus { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        //public virtual ICollection<QantasFileEntryModel> QantasFileEntries { get; set; }
    }
}
