﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class LoyaltyExportFileModel
    {
        public LoyaltyExportFileModel()
        {
            this.LoyaltyExportFileLineItems = new HashSet<LoyaltyExportFileLineItemModel>();
            this.LoyaltyExportFileApprovers = new HashSet<LoyaltyExportFileApproverModel>();
        }

        public int LoyaltyExportFileId { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> FileDate { get; set; }
        public Nullable<System.DateTime> ExportDateTime { get; set; }
        public Nullable<int> QantasExportFileStatusId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int LoyaltyUploadFileHeaderId { get; set; }

        public virtual LoyaltyUploadFileHeaderModel LoyaltyUploadFileHeader { get; set; }
        public virtual QantasExportFileStatusModel QantasExportFileStatu { get; set; }
        public virtual ICollection<LoyaltyExportFileLineItemModel> LoyaltyExportFileLineItems { get; set; }
        public virtual ICollection<LoyaltyExportFileApproverModel> LoyaltyExportFileApprovers { get; set; }
    }
}
