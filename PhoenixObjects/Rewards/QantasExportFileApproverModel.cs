﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class QantasExportFileApproverModel
    {
        public int QantasExportFileApproverId { get; set; }
        public int ProcessApproverListId { get; set; }
        public int QantasExportFileId { get; set; }
        public int Sequence { get; set; }
        public int QantasExportFileApproverStatusId { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }

        public virtual QantasExportFileModel QantasExportFile { get; set; }
        public virtual QantasExportFileApproverStatusModel QantasExportFileApproverStatus { get; set; }
    }
}
