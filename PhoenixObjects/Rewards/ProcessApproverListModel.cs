﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class ProcessApproverListModel
    {
        public ProcessApproverListModel()
        {
            this.QantasExportFileApprovers = new HashSet<QantasExportFileApproverModel>();
            this.LoyaltyExportFileApprovers = new HashSet<QantasExportFileApproverModel>();
        }

        public int ProcessApproverListId { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public string Email { get; set; }
        public string Process { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<QantasExportFileApproverModel> QantasExportFileApprovers { get; set; }
        public virtual ICollection<QantasExportFileApproverModel> LoyaltyExportFileApprovers { get; set; }
    }
}
