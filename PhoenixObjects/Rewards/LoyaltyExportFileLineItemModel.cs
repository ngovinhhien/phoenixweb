﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class LoyaltyExportFileLineItemModel
    {
        public int LoyaltyExportFileLineItemId { get; set; }
        public Nullable<int> LoyaltyExportFileId { get; set; }
        public Nullable<int> LoyaltyUploadFileLineItemId { get; set; }
        public Nullable<int> Points { get; set; }
        public string UploadStatus { get; set; }
        public string LoyaltyNumber { get; set; }
        public Nullable<int> QantasExportFileEntryStatusId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual LoyaltyExportFileModel LoyaltyExportFile { get; set; }
        public virtual QantasExportFileEntryStatusModel QantasExportFileEntryStatus { get; set; }
        public virtual LoyaltyUploadFileLineItemModel LoyaltyUploadFileLineItem { get; set; }
    }
}
