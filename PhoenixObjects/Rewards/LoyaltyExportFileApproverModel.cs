﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Rewards
{
    public class LoyaltyExportFileApproverModel
    {
        public int LoyaltyExportFileApproverId { get; set; }
        public int ProcessApproverListId { get; set; }
        public int LoyaltyExportFileId { get; set; }
        public int Sequence { get; set; }
        public int QantasExportFileApproverStatusId { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual LoyaltyExportFileModel LoyaltyExportFile { get; set; }
        public virtual ProcessApproverListModel ProcessApproverList { get; set; }
        public virtual QantasExportFileApproverStatusModel QantasExportFileApproverStatu { get; set; }
    }
}
