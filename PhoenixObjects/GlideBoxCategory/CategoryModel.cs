﻿using System.ComponentModel.DataAnnotations;

namespace PhoenixObjects.GlideBoxCategory
{
    public class CategoryModel
    {
        public int CategoryID { get; set; }

        [Display(Name = "Category Name")]
        [Required(ErrorMessage = "Category name is required.")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Category name must be between {2} and {1} characters.")]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "The Category name can only contain letters.")]
        public string CategoryName { get; set; }

        [Display(Name = "Category Code")]
        [Required(ErrorMessage = "Category code is required.")]
        [StringLength(7, MinimumLength = 3, ErrorMessage = "Category code must be between {2} and {1} characters.")]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "The Category code can only contain letters.")]
        public string ShortName { get; set; }

        [Range(2.00, 200.00, ErrorMessage = "Category price is invalid and must be between {1} and {2}.")]
        [Required(ErrorMessage = "Category price is required.")]
        [Display(Name = "Price")]
        [RegularExpression(@"^\d+(.\d{1,2})?$", ErrorMessage = "Category price must have no more than two decimal places.")]
        public decimal PriceCategory { get; set; }

        [Range(0.1, 200.00, ErrorMessage = "Category commission amount is invalid and must be between {1} and {2}.")]
        [Display(Name = "Commission Amount")]
        [Required(ErrorMessage = "Category driver commission amount is required.")]
        [RegularExpression(@"^\d+(.\d{1,2})?$", ErrorMessage = "Category driver commission must have no more than two decimal places.")]
        public decimal CommissionAmount { get; set; }

        public bool IsCreateCategoryAction { get; set; }
    }

    public class ActivateDeactivateCategoryModel
    {
        public int ID { get; set; }

        [CustomValidate]
        public string Reason { get; set; }

        public bool IsActivateAction { get; set; }
    }

    public class CustomValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (ActivateDeactivateCategoryModel)validationContext.ObjectInstance;

            string errorMessage = string.Empty;
            if (string.IsNullOrEmpty(model.Reason?.Trim()))
            {
                if (!model.IsActivateAction)
                {
                    errorMessage = "A reason for de-activating this category is required.";
                }
            }
            else
            {
                if (model.Reason.Length > 1000)
                {
                    if (!model.IsActivateAction)
                    {
                        errorMessage = "Category de-activation reason cannot be greater than 1000 characters.";
                    }
                    else
                    {
                        errorMessage = "Category activation reason cannot be greater than 1000 characters.";
                    }
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return new ValidationResult(errorMessage);
            }

            return ValidationResult.Success;
        }
    }

    public class GlideboxCategoriesCommissionForSameDayCashingModel
    {
        public int CategoryID { get; set; }
        public string ShortName { get; set; }
        public decimal Price { get; set; }
        public decimal CommissionAmount { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalCommissionAmount { get; set; }
        public bool? IsPercentage { get; set; }

        public GlideboxCategoriesCommissionForSameDayCashingModel()
        {
        }

        public GlideboxCategoriesCommissionForSameDayCashingModel(int categoryId, string shortName, decimal price, decimal commissionAmount, bool? isPercentage)
        {
            CategoryID = categoryId;
            ShortName = shortName;
            CommissionAmount = commissionAmount;
            Price = price;
            IsPercentage = isPercentage;
        }
    }
}