﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.GlideBoxCategory
{
    public class BulkAssignCategoryModel
    {
        public string TerminalIds { get; set; }
        public List<CategoryOrder> CategoryOrders { get; set; }
    }

    public class CategoryOrder
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int Order { get; set; }
    }

    public class IndividualAssignCategoryModel
    {
        public string TerminalId { get; set; }
        public List<CategoryOrder> CategoryOrders { get; set; }
    }
}
