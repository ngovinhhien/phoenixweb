﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PhoenixObjects.Exceptions
{
    public abstract class PhoenixException : System.Exception, ISerializable
    {
        public string custommessage = "";
        public PhoenixException()
        {
        }

        public PhoenixException(string message)
            : base(message)
        {
        }
        public PhoenixException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public PhoenixException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
    public class PhoenixGeneralException : PhoenixException
    {
         public PhoenixGeneralException()
        {
        }

        public PhoenixGeneralException(string message)
            : base(message)
        {
        }
        public PhoenixGeneralException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        public PhoenixGeneralException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        public PhoenixGeneralException(Exception ex) :base(ex.Message, ex.InnerException)
        {
            
        }
    }
    public class AddressNotFoundException : PhoenixException
    {

        public AddressNotFoundException()
            : base("Address Not Found. ")
        {
        }
        public AddressNotFoundException(string message)
            : base("Address Not Found. "+message)
        {
        }
        public AddressNotFoundException(string message, Exception innerException)
            : base("Address Not Found. " + message, innerException)
        {
        }
        public AddressNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    public class TerminalNotFound : PhoenixException
    {

        public TerminalNotFound()
            : base("Terminal Not Found. ")
        {
        }
        public TerminalNotFound(string message)
            : base("Terminal Not Found. " + message)
        {
        }
        public TerminalNotFound(string message, Exception innerException)
            : base("Terminal Not Found. " + message, innerException)
        {
        }
        public TerminalNotFound(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    public class InvalidBankAccountException : PhoenixException
    {

        public InvalidBankAccountException()
            : base("InvalidBankAccount. ")
        {
        }
        public InvalidBankAccountException(string message)
            : base("InvalidBankAccount. " + message)
        {
        }
        public InvalidBankAccountException(string message, Exception innerException)
            : base("InvalidBankAccount. " + message, innerException)
        {
        }
        public InvalidBankAccountException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    public class MemberAddException : PhoenixException
    {
        public MemberAddException()
            : base("Invalid Member Address")
        {
        }

        public MemberAddException(string message)
            : base(message)
        {
        }
        public MemberAddException(string message, Exception innerException)
            : base("Invalid Member Address" + message, innerException)
        {
        }
        public MemberAddException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
