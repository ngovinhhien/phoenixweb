﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Payway
{
   public class PaywayCustomer
    {
       public PaywayCustomer()
        {
            schedule = new schedule();
            paymentSetup = new paymentSetup();
           // merchant = new merchant();
            contact = new contact();
            customFields = new customFields();

        }
       public string customerNumber { get; set; }
       public schedule schedule { get; set; }
       public paymentSetup paymentSetup { get; set; }
      // public merchant merchant { get; set; }
       public contact contact { get; set; }
       public customFields customFields { get; set; }
       //public notes notes { get; set; }
    }
    public class paymentSetup
    {
       
        public string singleUseTokenId { get; set; }
        public string merchantId { get; set; }
        public string bankAccountId { get; set; }    
       
       
    }
    //public class creditCard
    //{
    //    public string cardNumber { get; set; }
    //    public string expiryDateMonth { get; set; }
    //    public string expiryDateYear { get; set; }
    //    public string cardScheme { get; set; }
    //    public string cardholderName { get; set; }
    //    public decimal surchargePercentage { get; set; }
    //}

    //public class bankAccount
    //{
     
    //    public string bsb { get; set; }
    //    public string accountNumber { get; set; }
    //    public string accountName { get; set; }
    //    public decimal surchargeAmount { get; set; }
    //    public string yourBankAccount { get; set; }
      
    //}


    //public class merchant
    //{
        
    //    public string merchantId { get; set; }
    //    public string merchantName { get; set; }
    //    public string settlementBsb { get; set; }
    //    public string settlementAccountNumber { get; set; }
    //    public string surchargeBsb { get; set; }
    //    public int surchargeAccountNumber { get; set; }
    //}

    public class contact
    {
        
        public string customerName { get; set; }
        public string emailAddress { get; set; }
        public bool sendEmailReceipts { get; set; }
        public string phoneNumber { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public string cityName { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
    }
    //public class address
    //{
        
    //    public string street1 { get; set; }
    //    public string street2 { get; set; }
    //    public string cityName { get; set; }
    //    public string state { get; set; }
    //    public string postalCode { get; set; }
    //}

    public class customFields
    {
       
        public string customField1 { get; set; }
        public string customField2 { get; set; }
        public string customField3 { get; set; }
        public string customField4 { get; set; }
       
    }

    //public class notes
    //{
    //     public notes()
    //    {
    //       new notes();
    //    }

    //    public string notes { get; set; }
      
    //}

    public class schedule
    {
      
        public string frequency { get; set; }
        public DateTime nextPaymentDate { get; set; }
        public decimal nextPrincipalAmount { get; set; }
        public decimal regularPrincipalAmount { get; set; }
        public int numberOfPaymentsRemaining { get; set; }
        public decimal finalPrincipalAmount { get; set; }
        public Nullable<int> Servicekey { get; set; }
        public string ServiceName { get; set; }
     

    }
}
