﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixObjects.DriverCard
{
    public class OxygenTransactionSearchVM
    {
        [Display(Name = "Transfer Date From")]
        public string TransactionDateFrom { get; set; }

        [Display(Name = "To")]
        public string TransactionDateTo { get; set; }

        [Display(Name = "Created Date From")]
        public string CreatedDateFrom { get; set; }

        [Display(Name = "To")]
        public string CreatedDateTo { get; set; }

        [Display(Name = "Oxygen Ref ID")]
        public string OxygenTransRefNo { get; set; }

        [Display(Name = "Member ID")]
        public string MemberID { get; set; }

        [Display(Name = "Payment Reference No")]
        public string LodgementReferenceNo { get; set; }

        [Display(Name = "Account Identifier")]
        public string AccountIdentifier { get; set; }

        [RegularExpression("([1-9][0-9]*[.][0-9]{1,2})")]
        [Display(Name = "Amount From")]
        public string AmountFrom { get; set; }

        [RegularExpression("([1-9][0-9]*[.][0-9]{1,2})")]
        [Display(Name = "To")]
        public string AmountTo { get; set; }

        [Display(Name = "Status")]
        public string StatusID { get; set; }

        [Display(Name = "Transfer Type")]
        public string TransactionType { get; set; }

        public string IsVoid { get; set; }

        public string SortField { get; set; }

        public string SortOrder { get; set; }
    }
}