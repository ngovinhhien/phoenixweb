﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixObjects.DriverCard
{
    public class OxygenTransactionVM
    {
        public int ID { get; set; }
        public int Transaction_key { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string OxygenTransRefNo { get; set; }
        public string MemberID { get; set; }
        public string AccountIdentifier { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}