﻿using PhoenixObjects.Member.DriverCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.DriverCard
{
    public class OxygenAPIResponseModel
    {
        public string accountIdentifier { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public string cardStatus { get; set; }
        public string cardStatusDescription { get; set; }
        public decimal? availableBalance { get; set; }
        public decimal? postedBalance { get; set; }
        public string transRefId { get; set; }
        public string txnDate { get; set; }
        public string txnTime { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string dateOfBirth { get; set; }
        public string mobilePhone { get; set; }
        public string email { get; set; }
        public string drivingLicenseNumber { get; set; }
        //Only on error
        public string statusCode { get; set; }
        public string description { get; set; }
        public decimal totalAmount { get; set; }
        public string uniqueId { get; set; }

        public MemberDriverCardModel ToMemberDriverCardModel()
        {
            var dob = string.IsNullOrEmpty(dateOfBirth) ? string.Empty : Convert.ToDateTime(dateOfBirth).ToString("dd/MM/yyyy");
            MemberDriverCardModel model = new MemberDriverCardModel();
            model.AccountIdentifier = accountIdentifier;
            model.AvailableBalance = availableBalance;
            model.FirstName = firstName;
            model.LastName = lastName;
            model.DOB = dob;
            model.MobilePhone = mobilePhone;
            model.Email = email;
            model.DrivingLicenseNumber = drivingLicenseNumber;
            model.Address1 = address1;
            model.Address2 = address2;
            model.City = city;
            model.PostalCode = postalCode;
            model.State = state;
            model.StatusCode = cardStatus;

            return model;
        }
    }
}
