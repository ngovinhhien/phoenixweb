﻿using System.ComponentModel.DataAnnotations;

namespace PhoenixObjects.Glidebox
{
    public class ProductModel
    {
        public int ProductID { get; set; }

        [Display(Name = "Product Name")]
        [Required(ErrorMessage = "Product name is required.")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Product name must be between {2} and {1} characters.")]
        public string ProductName { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Product price is required.")]
        [Range(0.01, 1000.00, ErrorMessage = "Product price is invalid and must be between {1} and {2}.")]
        [RegularExpression(@"^\d+(.\d{1,2})?$", ErrorMessage = "Product price must have no more than two decimal places.")]
        public decimal Price { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Quantity is required.")]
        [Range(0, 1000, ErrorMessage = "Quantity is invalid and must be between {1} and {2}.")]
        [RegularExpression("([0-9]+)", ErrorMessage = "The Product quantity field can only contain numbers.")]
        public int Quantity { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }

        [Display(Name = "Category")]
        public int Category { get; set; }

        public byte[] ImageContent { get; set; }

        public bool IsChangedImage { get; set; }
    }

    public class ActivateDeactivateProductModel
    {
        public int ID { get; set; }

        [CustomValidate]
        public string Reason { get; set; }

        public bool IsActivateAction { get; set; }
    }

    public class CustomValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (ActivateDeactivateProductModel)validationContext.ObjectInstance;

            string errorMessage = string.Empty;
            if (string.IsNullOrEmpty(model.Reason?.Trim()))
            {
                if (!model.IsActivateAction)
                {
                    errorMessage = "A reason for de-activating this product is required.";
                }
            }
            else
            {
                if (model.Reason.Length > 1000)
                {
                    if (!model.IsActivateAction)
                    {
                        errorMessage = "Product de-activation reason cannot be greater than 1000 characters.";
                    }
                    else
                    {
                        errorMessage = "Product activation reason cannot be greater than 1000 characters.";
                    }
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return new ValidationResult(errorMessage);
            }

            return ValidationResult.Success;
        }
    }
}