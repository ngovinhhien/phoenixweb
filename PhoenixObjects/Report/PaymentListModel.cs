﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Report
{
  public class PaymentListModel
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public DateTime TranDate { get; set; }
        public string Description { get; set; }
    }
}
