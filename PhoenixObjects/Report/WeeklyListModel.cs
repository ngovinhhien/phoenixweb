﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Report
{
   public class WeeklyListModel
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string day { get; set; }
        public DateTime TranDate { get; set; }
        public string Description { get; set; }
    }
}
