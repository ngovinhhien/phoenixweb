﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Docket
{
    public class SearchUnallocatedDocketModel
    {
        [Display(Name = "Docket Batch ID")]
        [MaxLength(20, ErrorMessage ="The max length of Docket Batch Id is 20")]
        public string DocketBatchId { get; set; }        
        [Display(Name = "Terminal ID")]
        [MaxLength(20, ErrorMessage = "The max length of Terminal Id is 20")]
        public string TerminalId { get; set; }
        [Display(Name = "Batch Number")]
        [MaxLength(20, ErrorMessage = "The max length of Batch Number is 20")]
        public string BatchNumber { get; set; }
    }
}
