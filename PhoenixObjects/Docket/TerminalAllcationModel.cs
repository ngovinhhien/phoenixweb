﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Docket
{
    public class TerminalAllcationModel
    {
        public string TerminalId { get; set; }
        public string MemberId { get; set; }
        public string VehicleId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool IsCurrent { get; set; }
        public string ModifiedByUser { get; set; }
        public DateTime? UnallocatedDate { get; set; }
        public int TerminalAllocation_key { get; set; }
        public string VehicleTypeName { get; set; }
    }
}
