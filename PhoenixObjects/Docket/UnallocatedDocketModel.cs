﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Docket
{
    public class UnallocatedDocketModel
    {
        public int Docket_key { get; set; }
        public string DocketBatchId { get; set; }
        public string TerminalId { get; set; }
        public string VehicleId { get; set; }
        public Nullable<System.DateTime> DocketDate { get; set; }        
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> ServiceCharge { get; set; }
        public Nullable<decimal> Gst { get; set; }
        public Nullable<decimal> DocketTotal { get; set; }        
        public string BatchNumber { get; set; }        
        
        
    }
}
