﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Docket
{
    public class AllocateDocketModel
    {
        public string DocketBatchId { get; set; }
        public int TerminalAllocationKey { get; set; }
        public DateTime EffectiveDate { get; set; }        
    }
}
