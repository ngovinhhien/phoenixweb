﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.LiveSMS
{
   public class SMSOutModel
    {
        public int SMSMessageOut_key { get; set; }
        public string MessageTo { get; set; }
        public string MessageFrom { get; set; }
        public string MessageText { get; set; }
        public string MessageType { get; set; }
        public string Gateway { get; set; }
        public string UserId { get; set; }
        public string UserInfo { get; set; }
        public Nullable<System.DateTime> Scheduled { get; set; }
        public bool IsSent { get; set; }
        public bool IsRead { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> member_key { get; set; }
    }
}
