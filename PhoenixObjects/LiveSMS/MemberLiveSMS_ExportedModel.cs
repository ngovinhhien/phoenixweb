﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.LiveSMS
{
    public class MemberLiveSMS_ExportedModel
    {
        public int Id { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<int> LiveSMSReferenceKey { get; set; }
    }
}
