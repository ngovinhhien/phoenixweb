﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.CommissionRate
{
    public class SurchargeRate
    {
        public int ID { get; set; }
        public string CardType { get; set; }
        public bool EnableCardType { get; set; }
        public int SurchargeType { get; set; }
        public decimal? FullMSF { get; set; }
        public decimal? MaxSurcharge { get; set; }
        public decimal CommissionRate { get; set; }
    }
}
