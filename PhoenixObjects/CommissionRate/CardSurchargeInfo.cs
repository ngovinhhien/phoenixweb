﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.CommissionRate
{
    public class CardSurchargeInfo
    {
        public string CardName { get; set; }
        public string OldMSF { get; set; }
        public string NewMSF { get; set; }
        public string NewSurchargeRate { get; set; }
    }
}
