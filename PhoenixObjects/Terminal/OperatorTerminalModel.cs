﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
  public class OperatorTerminalModel
    {
        public Nullable<int> EftTerminal_key { get; set; }
        public string TerminalId { get; set; }
        public string MemberId { get; set; }
        public int Member_key { get; set; }
        public string TradingName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string VehicleId { get; set; }
    }
}
