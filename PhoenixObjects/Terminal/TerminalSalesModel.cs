﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
  public class TerminalSalesModel
    {

        public int TerminalSale_key { get; set; }
        public int TerminalAllocation_key { get; set; }
        public int Employee_key { get; set; }
        public bool Active { get; set; }
        public int SaleRelationship_key { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
    
    }
}
