﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class TerminalModel
    {

        public string TerminalId { get; set; }
        public Nullable<int> TerminalType { get; set; }
        public bool Active { get; set; }
        public string Status { get; set; }
        public string PPID { get; set; }
        public string SIMSerialNumber { get; set; }
        public string SIMMobileNumber { get; set; }
        public string SoftwareVersion { get; set; }
        public string PUK { get; set; }
        public string Configuration { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public Nullable<int> Company_key { get; set; }
        public int EftTerminal_key { get; set; }
    }
}
