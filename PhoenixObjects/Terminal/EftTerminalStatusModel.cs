﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class EftTerminalStatusModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
