﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class TerminalTypeModel
    {
        public int TerminalType_key { get; set; }
        public string Name { get; set; }
        public string prefix { get; set; }
        public Nullable<decimal> CostPerTerminal { get; set; }
        public Nullable<decimal> CostPerSim { get; set; }
        public Nullable<bool> AvailableForPreorder { get; set; }
        public string MerchantID { get; set; }
        public Nullable<bool> VirtualTerminal { get; set; }
        public Nullable<int> NextSequence { get; set; }
        public Nullable<int> member_key { get; set; }
    }
}
