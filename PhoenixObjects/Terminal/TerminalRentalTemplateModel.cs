﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
    public class TerminalRentalTemplateModel
    {
        public int TerminalRentalTemplateKey { get; set; }
        public Nullable<int> MemberRentalTemplateKey { get; set; }
        public Nullable<decimal> RentalAmount { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> TerminalRentalRate_Key { get; set; }
    }
}
