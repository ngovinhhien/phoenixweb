﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class EftTerminalLocationModel
    {
        public int Id { get; set; }
        public string Location { get; set; }
    }
}
