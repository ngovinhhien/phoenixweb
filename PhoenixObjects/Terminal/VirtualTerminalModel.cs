﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class VirtualTerminalModel
    {
        public int TerminalAllocation_key { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public Nullable<int> Vehicle_key { get; set; }      
        public System.DateTime EffectiveDate { get; set; }
        public bool IsCurrent { get; set; }        
        public string TerminalId { get; set; }
        public Nullable<int> TerminalType { get; set; }    
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser1 { get; set; }       
        public Nullable<int> Company_key { get; set; }   
        public string Name { get; set; }
        public string MerchantID { get; set; }


     
    }
}
