﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
   public class TerminalRentalRateModel
    {
        public int TerminalRentalRate_key { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public decimal TransactionTarget { get; set; }
        public decimal RentAmount { get; set; }
        public Nullable<int> Company_key { get; set; }
        public Nullable<int> WaitingPeriod { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<int> MemberRentalTemplateKey { get; set; }
        public decimal? NewRentAmount { get; set; }
    }
}
