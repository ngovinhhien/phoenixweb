﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Terminal
{
    public class WebCashing_GetMemberTerminalAllocation_AllVehicleModel
    {
        public int Vehicle_key { get; set; }
        public string VehicleId { get; set; }
        public Nullable<System.DateTime> TerminalAllocatedDate { get; set; }
        public string RegisteredState { get; set; }
        public string Name { get; set; }
        public Nullable<int> TerminalAllocation_key { get; set; }
        public string TerminalId { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> DeallocatedDate { get; set; }
        public string OriginalTID { get; set; }
        public string SwappedTID { get; set; }
        public Nullable<int> TerminalKey { get; set; }
        public int VehicleType_key { get; set; }
        public Nullable<bool> Inactive { get; set; }
        public Nullable<int> AppKey { get; set; }
        public Nullable<int> ReceiptKey { get; set; }
        public bool IsSetEOS { get; set; }
        public bool EnableSendAPIKey { get; set; }
        public bool IsAllocated { get; set; }
        public bool AutoCreatePortalAccount { get; set; }
    }
}
