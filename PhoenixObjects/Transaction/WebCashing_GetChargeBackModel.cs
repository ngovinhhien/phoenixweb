﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
   public class WebCashing_GetChargeBackModel
    {
        public string Description { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<int> docket_key { get; set; }
    }
}
