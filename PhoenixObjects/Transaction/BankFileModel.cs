﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class BankFileModel
    {

        public bool IsChecked { get; set;}
        public string MerchantID { get; set; }
        public string TerminalId { get; set; }
        public string SettlementDate { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string STAN { get; set; }
        public string CardIssuer { get; set; }
        public string CardNumber { get; set; }
        public string Account { get; set; }
        public string Expiry { get; set; }
        public string Amount { get; set; }
        public string TransType { get; set; }
        public string Approved { get; set; }
        public string Reversal { get; set; }
    }
}
