﻿using PhoenixObjects.Financial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class MissingTransactionSearchModel
    {
        public MissingTransactionSearchModel()
        {
            MemberID = string.Empty;
            TerminalID = string.Empty;
            TaxiID = string.Empty;
            CardMask = string.Empty;
            CardType = string.Empty;
            ApproveStatus = string.Empty;
            BatchNumber = string.Empty;
            EndDateString = string.Empty;
            StartDateString = string.Empty;
            BankFile = new List<BankFileModel>();
        }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string MemberID { get; set; }
        public string TerminalID { get; set; }
        public string TaxiID { get; set; }
        public string CardMask { get; set; }
        public string CardType { get; set; }
        public string ApproveStatus { get; set; }
        public Boolean ApproveStatusBool { get; set; }
        public Boolean DeclineStatusBool { get; set; }
        public Boolean BothStatusBool { get; set; }
        public string BatchNumber { get; set; }
        public double? DocketFrom { get; set; }
        public double? DocketTo { get; set; }
        public string EndDateString { get; set; }
        public string StartDateString { get; set; }

        public List<TransactionSearchResultModel> Host = new List<TransactionSearchResultModel>();
        public List<TransactionSearch_ResultModel> CadMusIn = new List<TransactionSearch_ResultModel>();
        public List<TransactionSearchResultModel> Bank = new List<TransactionSearchResultModel>();
        public List<BankFileModel> BankFile = new List<BankFileModel>();
    }
}
