﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class TransactionDetailsModel
    {
        public Nullable<decimal> Amount { get; set; }
        public string Description { get; set; }
        public Nullable<int> Docket_key { get; set; }
        public Nullable<System.DateTime> ItemDate { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public Nullable<decimal> RateUsed { get; set; }
        public Nullable<int> TransactionDetailType { get; set; }
        public int TransactionDetail_key { get; set; }
        public Nullable<int> Transaction_key { get; set; }
        public string TerminalId { get; set; }
        public string VehicleId { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public string RateString { get; set; }
    }

    public class oTransactionDetailsModel
    {
        public int Tkey { get; set; }
        public DateTime Date { get; set; }
        public Decimal Amount { get; set; }
        public decimal MSF { get; set; }
        public decimal Total { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string TransType { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Batchno { get; set; }
        public Decimal RateUsed { get; set; }
        public string Status { get; set; }
        public string RateString { get; set; }
        public string TerminalId { get; set; }
        public string PaymentReference { get; set; }
        public string TransactionDescription { get; set; }



    }
}
