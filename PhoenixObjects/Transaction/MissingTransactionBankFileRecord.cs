﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class MissingTransactionBankFileRecordModel
    {
        public int MissingTransactionBankFileRecordId { get; set; }
        public int MissingTransactionBankFileId { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public string SettlementDate { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string STAN { get; set; }
        public string CardIssuer { get; set; }
        public string CardNumber { get; set; }
        public string Account { get; set; }
        public string Expiry { get; set; }
        public Nullable<double> Amount { get; set; }
        public string Trans_Type { get; set; }
        public string Approved { get; set; }
        public string Reversal { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual MissingTransactionBankFileRecordModel MissingTransactionBankFile { get; set; }
    }
}
