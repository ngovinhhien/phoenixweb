﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class MissingTransactionBankFileModel
    {
        public MissingTransactionBankFileModel()
        {
            this.MissingTransactionBankFileRecords = new HashSet<MissingTransactionBankFileRecordModel>();
        }

        public int MissingTransactionBankFileId { get; set; }
        public string FileName { get; set; }
        public bool IsCompleted { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<MissingTransactionBankFileRecordModel> MissingTransactionBankFileRecords { get; set; }
    }
}
