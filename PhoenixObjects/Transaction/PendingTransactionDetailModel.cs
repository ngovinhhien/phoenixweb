﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
   public class PendingTransactionDetailModel
    {
        public Nullable<System.DateTime> ItemDate { get; set; }
        public Nullable<decimal> RateUsed { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> TransactionDetailType { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public Nullable<int> Docket_key { get; set; }
        public int PendingTransactionDetail_key { get; set; }
        public Nullable<int> Member_key { get; set; }
    }
}
