﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
   public class WebCashing_AddChargeBackModel
    {
        public Nullable<int> Member_key { get; set; }
        public Nullable<System.DateTime> docketdate { get; set; }
        public int docket_key { get; set; }
        public string DocketBatchId { get; set; }
        public string CreatedByUser { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public string BatchID { get; set; }
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public string state { get; set; }
        public string TerminalId { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> DocketTotal { get; set; }
        public Nullable<decimal> TotalCommission { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> BatchTotalPayable { get; set; }
        public Nullable<decimal> TotalPayable { get; set; }
        public string CardNumberMasked { get; set; }
        public string BatchNumber { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public string BatchStatus { get; set; }
        public Nullable<bool> IsCashable { get; set; }
        public Nullable<decimal> PendingTransactionValue { get; set; }
        public Nullable<decimal> PendingCommision { get; set; }
        public Nullable<decimal> PaidTransactionValue { get; set; }
        public Nullable<decimal> PaidCommission { get; set; }
        public string LodgementReferenceNo { get; set; }
        public Nullable<decimal> BatchTotal { get; set; }
        public Nullable<decimal> DocketTotalSummary { get; set; }
        public Nullable<decimal> FaceValueSummary { get; set; }
        public Nullable<decimal> ExtrasSummary { get; set; }
        public Nullable<decimal> CommissionSummary { get; set; }
        public Nullable<decimal> TotalBatchCommission { get; set; }
        public Nullable<decimal> TotalBatchFaceValue { get; set; }
        public Nullable<decimal> TotalBatchExtras { get; set; }
        public string CardType { get; set; }
        public decimal handlingFee { get; set; }
        public Nullable<decimal> TotalBatchHandlingFee { get; set; }
        public Nullable<decimal> HandlingFeeSummary { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> LevyCharge { get; set; }
        public Nullable<decimal> TotalLevyCharge { get; set; }
        public Nullable<decimal> LevyChargeSummary { get; set; }
    }
}
