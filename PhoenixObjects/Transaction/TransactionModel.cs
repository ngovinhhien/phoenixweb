﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Transaction
{
    public class TransactionModel
    {

        public Nullable<System.DateTime> TransactionDate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public Nullable<int> Account_key { get; set; }
        public int Transaction_key { get; set; }
        public Nullable<int> Office_key { get; set; }
        public Nullable<int> TransactionType_key { get; set; }
        public Nullable<int> PayRun_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> FailedDate { get; set; }
        public string PayrunID { get; set; }
    }
    public class TransactionDataObj
    {
        public Nullable<int> Month { get; set; }
        public string MonthName { get; set; }
        public Nullable<int> year { get; set; }
        public Nullable<double> Amount { get; set; }
    }
    public class sTransactionDetailsModel
    {
        public int Tkey { get; set; }
        public DateTime Date { get; set; }
        public Decimal Amount { get; set; }
        public decimal MSF { get; set; }
        public decimal Total { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string TransType { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Batchno { get; set; }
        public Decimal RateUsed { get; set; }
        public string Status { get; set; }
        public string RateString { get; set; }
        public string TerminalId { get; set; }
        public string PaymentReference { get; set; }
        public string TransactionDescription { get; set; }
        public long? DriverId { get; set; }
        public string TaxiId { get; set; }
        public System.DateTime EndOfShift { get; set; }

    }
    public class sTransactionParentModel
    {

        public int Tkey { get; set; }
        public Decimal TotalAmount { get; set; }
        public decimal TotalMSF { get; set; }
        public decimal Total { get; set; }
        public string Batchno { get; set; }
        public string TerminalId { get; set; }
        public string Id { get; set; }
        public List<sTransactionDetailsModel> Details = new List<sTransactionDetailsModel>();
    }
    public class TransactionPackage
    {
        public string Date { get; set; }
        public string TerminalId { get; set; }
        public Decimal Amount { get; set; }
        public decimal MSF { get; set; }
        public decimal Total { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string TransType { get; set; }
        public string Status { get; set; }
        public string PaymentReference { get; set; }
        public long? DriverId { get; set; }
        public string TaxiId { get; set; }
        public System.DateTime EndOfShift { get; set; }

    }

    public class PaymentPackage
    {
        public string Date { get; set; }
        public Decimal Amount { get; set; }
        public string RateUsed { get; set; }
        public string Fare { get; set; }
        public string Extras { get; set; }
        public string TerminalId { get; set; }
        public string VehicleId { get; set; }

    }
    public class oPaymentPackage
    {
        public string Date { get; set; }
        public Decimal Amount { get; set; }
        public decimal MSF { get; set; }
        public decimal Total { get; set; }
        public string RateUsed { get; set; }

    }

   
}
