﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.TransactionHost
{
    public class SearchUnReconciledTransactions
    {
        public int Id { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
    }
}
