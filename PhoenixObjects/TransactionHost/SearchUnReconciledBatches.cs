﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.TransactionHost
{
    public class SearchUnReconciledBatches
    {
        public int Id { get; set; }
        [Display(Name = "TerminalID")]
        public string TerminalID { get; set; }
        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }
    }
}
