﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.TransactionHost
{
    public class UnReconciledTransactionsModel
    {
        [Display(Name = "Total Sale Amount")]
        public decimal TotalSaleAmount { get; set; }
        [Display(Name = "Response Code")]
        public string ResponseCode { get; set; }
        [Display(Name = "Start Date Time")]
        public DateTime? StartDateTime { get; set; }
        [Display(Name = "Transaction Number")]
        public string TransactionNumber { get; set; }
        [Display(Name = "Card Name")]
        public string CardName { get; set; }
        [Display(Name = "Card Masked")]
        public string CardMasked { get; set; }
        [Display(Name = "Tran Type")]
        public string TranType { get; set; }
        [Display(Name = "Processing Type Name")]
        public string ProcessingTypeName { get; set; }
    }
}
