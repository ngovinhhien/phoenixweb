﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.TransactionHost;

namespace PhoenixObjects.TransactionHost
{
    public class UnReconciledBatchesModel
    {
        [Display(Name = "Terminal ID")]
        public string TerminalID { get; set; }
        [Display(Name = "Terminal ID")]
        public string MerchantID { get; set; }

        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }
        [Display(Name = "Batch Number")]
        public string BatchNumber { get; set; }
        [Display(Name = "Transaction Count")]
        public int TransactionCount { get; set; }
        [Display(Name = "Batch Total")]
        public decimal BatchTotal { get; set; }
        [Display(Name = "Batch Start")]
        public DateTime? SOSAttempt { get; set; }
        [Display(Name = "Batch End")]
        public DateTime? FirstEOSAttempt { get; set; }
        [Display(Name = "Batch Closing Attempts")]
        public int EOSAttempt { get; set; }

        public int? DebitNumber { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmount { get; set; }
        public int? CreditNumber { get; set; }
        public int? DebitReversalNumber { get; set; }
        public decimal? DebitReversalAmount { get; set; }
        public int? CreditReversalNumber { get; set; }
        public decimal? CreditReversalAmount { get; set; }
        public decimal? SettlementNetAmount
        {
            get
            {
                return DebitAmount.GetValueOrDefault(0) + CreditReversalAmount.GetValueOrDefault(0) - DebitReversalAmount.GetValueOrDefault(0) - CreditAmount.GetValueOrDefault(0);
            }
        }
    }
}
