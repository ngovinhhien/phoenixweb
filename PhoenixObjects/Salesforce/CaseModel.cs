﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Salesforce
{
   public class CaseModel
    {
        public string ContactName { get; set; }
        public string AccountName { get; set; }
        public string AccountSalesforceId { get; set; }
        public string ContactSalesforceId { get; set; }
        public string  TerminalId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Descrition { get; set; }
        public string Subject { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public string CaseOrigin { get; set; }
        public string Escalate { get; set; }
        public bool EscalateB { get; set; }
    }
}
