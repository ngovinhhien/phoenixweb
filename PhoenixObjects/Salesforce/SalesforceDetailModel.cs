﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Salesforce
{
  public class SalesforceDetailModel
    {
            public int Id { get; set; }
            public string ClientID { get; set; }
            public string Secret { get; set; }
            public string UserName { get; set; }
            public string TokenPassword { get; set; }
      
    }
}
