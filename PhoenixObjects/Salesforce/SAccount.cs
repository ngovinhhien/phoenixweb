﻿using PhoenixObjects.Common;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix_BusLog.Salesforce
{
    
    public class SAccount
    {
        public string Id { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Industry { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingStreet { get; set; }
        public string Phone { get; set; }
        public Object CreatedBy { get; set; }
      
    }
    public class SAsset
    {
     
        public string AccountId { get; set; }
        public string TerminalID__c { get; set; }
     
              

    }
    public class SContact
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MasterRecordId { get; set; }
        public string Name { get; set; }
        public string Birthdate { get; set; }
        public string MailingCity { get; set; }
        public string MailingCountry { get; set; }
        public string MailingPostalCode { get; set; }
        public string MailingState { get; set; }
        public string MailingStreet { get; set; }
        public string MobilePhone { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        
    }
    public class SLead
    {
        public string AccountId { get; set; }
        public decimal? Amex_Diners__c { get; set; }
        public decimal? Diners__c { get; set; }
        public decimal? Unionpay__c { get; set; }
        public decimal? Zip__c { get; set; }
        public decimal? Qantas__c { get; set; }
        public decimal? Alipay__c { get; set; }
        public decimal? Average_transaction__c { get; set; }
        public decimal? Debit__c { get; set; }
        public decimal? EFTPOS_turnover__c { get; set; }
        public decimal? Terminal_Rental__c { get; set; }
        public decimal? Visa_Mastercard__c { get; set; }
        //public string Monthly_Rental__c { get; set; }
        public decimal? Monthly_Rent__c { get; set; }
        public string Quoted_by__c { get; set; }        
        public bool Flat_Rate__c { get; set; }
        public string New_Free_Rental__c { get; set; }
        
    }
    public class SForceModel
    {
        public MemberModel memberModel { get; set; }
        public AddressListModel addrModel { get; set; }
        public MemberRentalModel memberRentalModel { get; set; }
    }
}
      