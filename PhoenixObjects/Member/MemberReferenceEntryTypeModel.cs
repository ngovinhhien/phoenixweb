﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class MemberReferenceEntryTypeModel
    {
        public int MemberReferenceEntryTypeKey { get; set; }
        public string Description { get; set; }
    }
}
