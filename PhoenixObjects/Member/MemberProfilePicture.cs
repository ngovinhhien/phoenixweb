﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace PhoenixObjects.Member
{
    
    public class MemberProfilePictureModel
    {
        public MemberProfilePictureModel() 
        {
            this.UpdatedDate = DateTime.Now;
            this.CreatedDate = DateTime.Now;
        }
        public MemberProfilePictureModel(dynamic _picture) : this()
        {
            if (_picture.MemberProfilePictureKey != null && _picture.MemberProfilePictureKey > 0)
                this.MemberProfilePictureKey = _picture.MemberProfilePictureKey;
            if (_picture.ProfilePicture != null)
                this.ProfilePicture = _picture.ProfilePicture;
            if (_picture.memberKey != null && _picture.memberKey > 0)
                this.member_key = _picture.memberKey;
            if (!String.IsNullOrEmpty(_picture.CreatedByUser))
                this.CreatedByUser = _picture.CreatedByUser;
            if (!String.IsNullOrEmpty(_picture.UpdatedByUser))
                this.UpdatedByUser = _picture.UpdatedByUser;
            if (_picture.CreatedDate != null && _picture.CreatedDate is DateTime)
                this.CreatedDate = _picture.CreatedDate;
            if (_picture.UpdatedDate != null && _picture.UpdatedDate is DateTime)
                this.UpdatedDate = _picture.UpdatedDate;
        }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedByUser { get; set; }
        public Nullable<int> member_key { get; set; }
        public int MemberProfilePictureKey { get; set; }
        public byte[] ProfilePicture { get; set; }
        public int MemberKey { get; set; }
    }
}
