﻿using PhoenixObjects.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class ZipMerchantAPIModel
    {
        public int? MemberKey { get; set; }
        [StringLength(50, ErrorMessage = "API key max length is 50 characters")]
        [RequireIf(PropertyTo = nameof(LocationID), ErrorMessage = "Please input Zip API Key")]
        public string ZipAPIKey { get; set; }
        [StringLength(10, ErrorMessage = "The location ID is only 10 digits number")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        [RequireIf(PropertyTo = nameof(ZipAPIKey), ErrorMessage = "Please input Location ID")]
        public string LocationID { get; set; }
        public bool IsActive { get; set; }
    }
}
