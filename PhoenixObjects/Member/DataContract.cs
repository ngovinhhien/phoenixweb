﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhoenixObjects.Common;
using System.Text;
using System.Runtime.Serialization;
using PhoenixObjects.Member;


namespace PhoenixObjects.Member
{
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    
    public class MemberModel2
    {
   
        public int MemberKey
        {
            get;
            set;
        }

        
        public string MemberID
        {
            get;
            set;
        }

        
        public string FirstName
        {
            get;
            set;
        }

        
        public string LastName
        {
            get;
            set;
        }
        
        public string Tradingname
        {
            get;
            set;
        }
        
        public string ABN
        {
            get;
            set;
        }
        
        public string ACN
        {
            get;
            set;
        }
        
        public string DriverLicenseNumber
        {
            get;
            set;
        }
        
        public bool Active
        {
            get;
            set;
        }

        
        public BusinessTypeModel BusinessType
        {
            get;
            set;
        }

        
        public bool IsOperator
        {
            get;
            set;
        }

        
        public bool IsDealer
        {
            get;
            set;
        }

       

        
        public bool IsBankCustomer
        {
            get;
            set;
        }

        
        public List<BankAccountModel> BankAccounts { get; set; }

        
        public bool IsFlatCommissionRate
        {get;set;}
    }

   

   



    


    


    

    


    



}