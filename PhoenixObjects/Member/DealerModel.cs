﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class DealerModel
    {
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public int Dealer_key { get; set; }
        public string DealerName { get; set; }
      
    }
}
