﻿using System;

namespace PhoenixObjects.Member.CommissionRate
{
    public class MemberRebateModel
    {
        public int BusinessTypeID { get; set; }
        public bool IsEdited { get; set; }
        public DateTime StartDate { get; set; }
        public decimal? CRDebitRebateForPurchaseWithCashOut { get; set; }
        public decimal? CRDebitRebateForCashAdvance { get; set; }
    }
}