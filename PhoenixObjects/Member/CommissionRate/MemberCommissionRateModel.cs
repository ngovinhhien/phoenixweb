﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.CommissionRate
{
    public class MemberCommissionRateModel
    {
        public int Number { get; set; }
        public string BusinessType { get; set; }
        public string SubBusinessType { get; set; }
        public int BusinessTypeID { get; set; }
        public int SubBusinessTypeID { get; set; }
        public string DebitRateType { get; set; }
        public string QantasRateType { get; set; }
        public bool IsEdited { get; set; }
        public decimal? CRDebitRate { get; set; }
        public DateTime StartDate { get; set; }
        public Nullable<decimal> CRDebitRatePercentage { get; set; }
        public Nullable<decimal> CRVisaRate { get; set; }
        public Nullable<decimal> CRVisaDebitRate { get; set; }
        public Nullable<decimal> CRMasterCardRate { get; set; }
        public Nullable<decimal> CRMasterCardDebitRate { get; set; }
        public Nullable<decimal> CRAmexRate { get; set; }
        public Nullable<decimal> CRDinersRate { get; set; }
        public Nullable<decimal> CRDealerRate { get; set; }
        public Nullable<decimal> CRManualVoucherRate { get; set; }
        public Nullable<decimal> CRUnionPayRate { get; set; }
        public Nullable<decimal> CRZipPayRate { get; set; }
        public Nullable<decimal> CRQantasRate { get; set; }
        public Nullable<decimal> CRQantasRatePercentage { get; set; }
        public Nullable<decimal> CRAlipayRate { get; set; }
        public Nullable<decimal> CRJCBRate { get; set; }
        public Nullable<decimal> CRDebitRebateForPurchaseWithCashOut { get; set; }
        public Nullable<decimal> CRDebitRebateForCashAdvance { get; set; }
        public bool IsDebitFixed { get; set; }
        public bool IsQantasFixed { get; set; }
        
        public decimal? CRDebitTranFee { get; set; }
        public decimal? CRVisaTranFee { get; set; }
        public decimal? CRVisaDebitTranFee { get; set; }
        public decimal? CRMasterCardTranFee { get; set; }
        public decimal? CRMasterCardDebitTranFee { get; set; }
        public decimal? CRAmexTranFee { get; set; }
        public decimal? CRDinersTranFee { get; set; }
        public decimal? CRUnionPayTranFee { get; set; }
        public decimal? CRZipPayTranFee { get; set; }
        public decimal? CRQantasTranFee { get; set; }
        public decimal? CRAlipayTranFee { get; set; }
        public decimal? CRJCBTranFee { get; set; }

        public decimal GetMaxRate(decimal rate)
        {
            return Math.Floor(Math.Abs(rate) / (1 - Math.Abs(rate)) * 10000) / 10000;
        }

        private string GetRateString(decimal? rate, bool fixable = false)
        {
            if (fixable)
            {
                if (Math.Abs(rate.GetValueOrDefault(0.0m)) > 0.10m && fixable)
                    return String.Format("${0:N4}", rate.GetValueOrDefault(0.0m));
                else
                    return String.Format("{0:N4}%", (rate.GetValueOrDefault(0.0m) * 100.0m));
            }
            else
            {
                return String.Format("{0:N4}%", (rate.GetValueOrDefault(0.0m)));
            }

        }
        public string DebitRateString
        {
            get
            {
                return GetRateString(CRDebitRate, true);

            }
        }

        public string VisaRateString
        {
            get
            {
                return GetRateString(CRVisaRate);
            }
        }

        public string MasterRateString
        {
            get
            {
                return GetRateString(CRMasterCardRate);
            }
        }

        public string AmexRateString
        {
            get
            {
                return GetRateString(CRAmexRate);
            }
        }

        public string DinersRateString
        {
            get
            {
                return GetRateString(CRDinersRate);

            }
        }

        public string UnionPayRateString
        {
            get
            {
                return GetRateString(CRUnionPayRate);

            }
        }

        public string ZipRatesString
        {
            get
            {
                return GetRateString(CRZipPayRate);

            }
        }

        public string QantasRateString
        {
            get
            {
                return GetRateString(CRQantasRate, true);

            }
        }

        public string AlipayRateString
        {
            get
            {
                return GetRateString(CRAlipayRate);

            }
        }
    }
}
