﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.Search
{
    public class MemberAutoSearchResult
    {
        public string TradingName { get; set; }
        public string MemberID { get; set; }
        public string FullName { get; set; }
        public string TerminalId { get; set; }
        public string QBRNumber { get; set; }
        public string AccountIdentifier { get; set; }
    }
}
