﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.Search
{
    public class MemberSearchCriteriaVM
    {
        public string CompanyKey { get; set; }
        public string DealerKey { get; set; }
        public string PaymentKey { get; set; }
        public string QBRStatusKey { get; set; }
        public string SearchKeyword { get; set; }
        public string CreatedDateFrom { get; set; }
        public string CreatedDateTo { get; set; }
        public string IsActive { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public bool IsSearchByKeyword { get; set; }
        public bool IsUseAutoComplete { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
