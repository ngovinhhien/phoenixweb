﻿namespace PhoenixObjects.Member.Search
{
    public class PaymentSearchCriteriaVM
    {
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string FromDate { get; set; }
        public string ToDate {get;set; }
        public string ReferenceNo { get; set; }
        public string MemberKey { get; set; }
    }
}