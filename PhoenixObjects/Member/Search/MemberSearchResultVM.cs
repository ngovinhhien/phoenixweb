﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.Search
{
    public class MemberSearchResultVM
    {
        public string TradingName { get; set; }
        public string MemberId { get; set; }
        public string DealerName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string CompanyName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string PaymentType { get; set; }
        public bool Active { get; set; }
        public string QBRNumber { get; set; }
        public bool IsDisableQBR { get; set; }
        public bool? EOMMSFPayment { get; set; }
        public int MemberKey { get; set; }

    }
}
