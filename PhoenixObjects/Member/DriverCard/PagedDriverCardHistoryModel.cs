﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.DriverCard
{
    public class PagedDriverCardHistoryModel
    {
        public int total { get; set; }
        public int page { get; set; }
        public List<DriverCardHistoryModel> rows { get; set; }
        public int records { get; set; }
    }
}
