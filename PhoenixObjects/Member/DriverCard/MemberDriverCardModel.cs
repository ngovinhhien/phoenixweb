﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.DriverCard
{
    public class MemberDriverCardModel
    {
        public int Id { get; set; }
        public string AccountIdentifier { get; set; }
        public int MemberKey { get; set; }
        public int StatusID { get; set; }
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string DOB { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string DrivingLicenseNumber { get; set; }
        public decimal? AvailableBalance { get; set; }
    }
}
