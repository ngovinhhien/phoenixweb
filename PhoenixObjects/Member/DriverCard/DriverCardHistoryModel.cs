﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member.DriverCard
{
    public class DriverCardHistoryModel
    {
        public string ActionBy { get; set; }
        public string ActionDescription { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
        public int MemberKey { get; set; }
        public int DriverCardId { get; set; }

    }
}
