﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class MemberAccountEntryModel
    {
        public int MemberAccountEntryKey { get; set; }
        public Nullable<int> MemberAccountKey { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<int> Transaction_key { get; set; }
        public Nullable<System.DateTime> EntryDateTime { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> CreditAmount { get; set; }
        public Nullable<decimal> DebitAmount { get; set; }
        public Nullable<decimal> BalanceAmount { get; set; }
      
        public Nullable<int> ReversedMemberAccountEntryKey { get; set; }
        public Nullable<int> Office_key { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string UpdatedByUser { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
    }
}
