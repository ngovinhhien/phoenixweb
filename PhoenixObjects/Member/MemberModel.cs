﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
//using System.Web.Mvc;
using PhoenixObjects.Financial;
using PhoenixObjects.Terminal;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Report;
using PhoenixObjects.Customer;
using PhoenixObjects.Transaction;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using PhoenixObjects.Fee;
using PhoenixObjects.Member.CommissionRate;
using PhoenixObjects.Member.DriverCard;

namespace PhoenixObjects.Member
{
    public class MemberModelSimple
    {
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public string SalesforceId { get; set; }
        public int Member_key { get; set; }
        public string MemberId { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string FullMobile
        {
            get
            {
                if (String.IsNullOrEmpty(this.Mobile))
                {
                    return this.Mobile;
                }
                else
                {
                    if (this.Mobile.StartsWith("+")) //guess the mobile number has countrycode
                    {
                        return this.Mobile;
                    }
                    else if (this.Mobile.StartsWith("04")) //guess the mobile number in format starting 04
                    {
                        //remove the 0 at the front and add +61
                        return "+61" + this.Mobile.Replace(" ", "").Remove(0, 1);
                    }
                    else
                        return this.Mobile;
                }
            }
        }
        public string Email { get; set; }
        [DisplayName("Trading-Name")]
        public string TradingName { get; set; }
        public string ABN { get; set; }
        public string ACN { get; set; }
        public string ReferenceId { get; set; }
        [DisplayName("Drivers LicenseNo.")]
        public string DriverLicenseNumber { get; set; }
        public byte[] PhotoID { get; set; }

        [DisplayName("PhotoID Expiry")]
        //PHW-378
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> PhotoIDExpiry { get; set; }
        public string PhotoIDExpiryString { get; set; }
        public BusinessTypeModel BusinessType { get; set; }
        public bool IsOperator { get; set; }
        public bool IsDealer { get; set; }
        public bool IsBankCustomer { get; set; }
        public bool IsCashCustomer { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DOB { get; set; }
        public string DOBstring { get; set; }
        public bool Welcomed { get; set; }
        public bool Active { get; set; }
        public int CompanyKey { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public string BusinesstypeName { get; set; }
        public string SubBusinesstypeName { get; set; }
        public int DealerKey { get; set; }
        public string Dealer_keyString { get; set; }

        //QBR Number: PHW-7
        public string QBRNumber { get; set; }

        //QBR Number History: PHW-7
        //Make sure it is populated ascending
        public string[] QBRNumberHistory { get; set; }
        public bool IsDisableQBR { get; set; }
        [DisplayName("Website")]
        public string MerchantWebsite { get; set; }
        [DisplayName("Extended Settlement")]
        public bool ExtendedSettlement { get; set; }
        [DisplayName("EOM MSF Payment")]
        public bool EOMMSFPayment { get; set; }
        public string AmexMerchantID { get; set; }
        [DisplayName("Live Group Amex Settlement")]
        public bool IndirectAmexSettlement { get; set; }

        [DisplayName("Enable Split Payment Reports")]
        public bool EnableSplitPaymentReport { get; set; }
        public bool NotAllowSelectSubBusinessType { get; set; }
    }
    public class MemberModel : MemberModelSimple
    {
        public MemberModel()
        {
            this.MemberAddresses = new List<MemberAddressModel>();
            this.Addresses = new List<AddressListModel>();
            this.BankAccounts = new List<BankAccountModel>();
            this.AllVehicleTerminalAllocation = new List<AllVehicleTerminalAllocationModel>();
            this.TerminalAllocationList = new List<TerminalAllocationModel>();
            this.MerchantList = new List<MerchantModel>();
            this.Fees = new List<FeeModel>();
            this.CommissionRates = new List<MemberCommissionRateModel>();

            CompanyTypes = new List<SelectListItem>();
            StatusTypes = new List<SelectListItem>();
        }

        public MemberAddressModel PrimaryAddress { get; set; }
        public MemberAddressModel MailingAddress { get; set; }
        public List<AddressListModel> Addresses { get; set; }
        public List<AddressListModel> AddrForExistingMember { get; set; }
        public List<MemberAddressModel> MemberAddresses { get; set; }
        public List<TerminalAllocationModel> TerminalAllocationList
        {
            get;
            set;
        }
        public List<MemberCommissionRateModel> CommissionRates { get; set; }
        public List<MemberRebateModel> Rebates { get; set; }
        public MemberDriverCardModel DriverCard { get; set; }
        public List<FeeModel> Fees { get; set; }
        public string FeeDtoAsJson { get; set; }
        public List<FeeTypeModel> ChargeFeeType { get; set; }
        public string ChargeFeeTypeDtoAsJson { get; set; }
        public string ChargeHistoryAsJson { get; set; }
        public string ChangeHistoryAsJson { get; set; }
        public string CommissionRatesDtoAsJson { get; set; }
        public string RebatesDtoAsJson { get; set; }
        public string DefaultRatesDtoAsJson { get; set; }
        public string DriverCardDtoAsJson { get; set; }

        //private List<TerminalAllocationModel> _TerminalAllocationList;
        public List<MemberRentalModel> MemberRentalModelList { get; set; }
        [DisplayName("Member")]

        public List<BankAccountModel> BankAccounts { get; set; }
        public string BankAccountDtoAsJson { get; set; }
        public bool IsFlatCommissionRate { get; set; }

        public DealerModel Dealer { get; set; }
        public int OperatorKey { get; set; }
        public OperatorModel Operator { get; set; }
        [DisplayName("Street No")]
        public string PStreetNo { get; set; }
        [DisplayName("Street Name")]
        public string PStreet { get; set; }
        [DisplayName("Post Code")]
        public string PPostCode { get; set; }

        public string MStreetNo { get; set; }
        public string MStreet { get; set; }
        public string MPostCode { get; set; }


        public string StartDate { get; set; }
        [StringLength(50, ErrorMessage = "Address length must be equal or less than 50.")]
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PurposeTypes { get; set; }
        public string Postcode { get; set; }

        [StringLength(50, ErrorMessage = "Address length must be equal or less than 50.")]
        public string MailAddress { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailPostcode { get; set; }

        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only allow input number or decimal with max 2 digit (0.00)")]
        public Nullable<decimal> AverageTransaction { get; set; }

        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only allow input number or decimal with max 2 digit (0.00)")]
        public Nullable<decimal> AverageTurnover { get; set; }
        public string MCC { get; set; }

        public string[] SearchList { get; set; }
        public bool DebitIsFixed { get; set; }
        public bool QantasIsFixed { get; set; }
        public decimal? CRDebitRate { get; set; }
        public decimal? CRDebitRateOrigin { get; set; }
        public int CRDebitRateKey { get; set; }
        public Nullable<decimal> CRDebitRatePercentage { get; set; }
        public int CRDebitRatePercentageKey { get; set; }
        public Nullable<decimal> CRVisaRate { get; set; }
        public Nullable<decimal> CRVisaRateOrigin { get; set; }
        public int CRVisaRateKey { get; set; }
        public Nullable<decimal> CRMasterCardRate { get; set; }
        public Nullable<decimal> CRMasterCardRateOrigin { get; set; }
        public int CRMasterCardRateKey { get; set; }
        public Nullable<decimal> CRAmexRate { get; set; }
        public Nullable<decimal> CRAmexRateOrigin { get; set; }
        public int CRAmexRateKey { get; set; }
        public Nullable<decimal> CRDinersRate { get; set; }
        public Nullable<decimal> CRDinersRateOrigin { get; set; }
        public int CRDinersRateKey { get; set; }
        public Nullable<decimal> CRDealerRate { get; set; }
        public Nullable<decimal> CRDealerRateOrigin { get; set; }
        public int CRDealerRateKey { get; set; }
        public Nullable<decimal> CRManualVoucherRate { get; set; }
        public int CRManualVoucherRateKey { get; set; }
        public Nullable<decimal> CRUnionPayRate { get; set; }
        public Nullable<decimal> CRUnionPayRateOrigin { get; set; }
        public int CRUnionPayRateKey { get; set; }

        public Nullable<decimal> CRZipPayRate { get; set; }
        public Nullable<decimal> CRZipPayRateOrigin { get; set; }
        public int CRZipPayRateKey { get; set; }

        public Nullable<decimal> CRQantasRate { get; set; }
        public Nullable<decimal> CRQantasRateOrigin { get; set; }
        public int CRQantasRateKey { get; set; }

        public Nullable<decimal> CRQantasRatePercentage { get; set; }
        public int CRQantasRatePercentageKey { get; set; }

        public Nullable<decimal> CRAlipayRate { get; set; }
        public Nullable<decimal> CRAlipayRateOrigin { get; set; }
        public int CRAlipayRateKey { get; set; }
        public Nullable<decimal> CRJCBRate { get; set; }
        public DefaultRateModel DefaultRateModel { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string MerchantID { get; set; }
        public DateTime? RentalStartDate { get; set; }
        string _rentalStartDateString;
        public string RentalStartDateString
        {
            get
            {
                if (RentalStartDate != null)
                    _rentalStartDateString = RentalStartDate.Value.ToString("dd-MM-yyyy");//  .ToShortDateString();
                return _rentalStartDateString;

            }
            set
            {

                _rentalStartDateString = value;
            }
        }
        public DateTime? RentalEndDate { get; set; }

        string _rentalEndDateString = "";
        public string RentalEndDateString
        {
            get
            {
                if (RentalEndDate != null)
                    _rentalEndDateString = RentalEndDate.Value.ToString("dd-MM-yyyy"); //.ToShortDateString();
                return _rentalEndDateString;

            }
            set
            {
                _rentalEndDateString = value;
            }
        }
        public bool RentalRentFree { get; set; }
        public int? RentalMemberRentalTemplateKey { get; set; }
        public decimal? RentalAmountFromSalesforce { get; set; }
        public int RentalTurnoverRangeKeyFromSalesforce { get; set; }
        public bool IsFlatRate { get; set; }
        public List<TerminalRentalRateModel> TerminalRentalRateList { get; set; }
        public List<MemberTerminalRentalModel> MemberCurrentTerminalRentalList { get; set; }
        public List<AllVehicleTerminalAllocationModel> AllVehicleTerminalAllocation { get; set; }
        public List<MerchantModel> MerchantList { get; set; }
        public List<MemberFeeChangeHistory> FeeChangeHistories { get; set; }
        public List<MemberFeeChargeHistory> FeeChargeHistories { get; set; }

        public Nullable<int> State_key { get; set; }
        public Nullable<int> Employee_key { get; set; }
        public Nullable<bool> ReceiveWeeklyActivityReport { get; set; }
        public Nullable<bool> ReceivePaymentReport { get; set; }
        public Nullable<bool> ReceiveMonthlyActivityReport { get; set; }
        public bool ReceiveWeeklyActivityReportB { get; set; }
        public bool ReceivePaymentReportB { get; set; }
        public bool ReceiveMonthlyActivityReportB { get; set; }
        public string PaymentType { get; set; }
        public int PaymentTypeId { get; set; }
        public MyWebUserProfileModel MyWebUserProfile { get; set; }
        public List<WeeklyListModel> WeeklyActivityList { get; set; }
        public List<MonthlyListModel> MonthlyActivityList { get; set; }
        public string Username { get; set; }
        public string UsernameEncrypt { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string DriverCertificateId { get; set; }
        public decimal? LastCashedAmt { get; set; }
        public DateTime? LastCashedDateTime { get; set; }
        public int MailingAddressKey { get; set; }
        public string DriverAuth { get; set; }
        public List<SelectListItem> CompanyTypes { get; set; }
        public List<SelectListItem> StatusTypes { get; set; }
        public int PrimaryAddressKey { get; set; }
        public string PrimaryAddressDesc { get; set; }
        public List<AuthorisedMemberModel> AuthorisedMemberList { get; set; }
        public CommissionRateVM SalesforceData { get; set; }
        public int PhotoIDKey { get; set; }
        //private PhotoIDModel _currentPhotoID;
        public PhotoIDModel CurrentPhotoID
        {
            get;
            set;


        }

        public bool EnableAFForPhysicalTerminalAllocatedToMerchant { get; set; }

        public int MemberProfilePictureKey { get; set; }
        //private MemberProfilePicture _MemberProfilePicture;
        public MemberProfilePictureModel MemberProfilePhoto
        {
            get;
            set;

        }
        public List<TransactionModel> PaymentHistory { get; set; }
        public List<MemberMonthlyReportModel> MonthlyReportHistory { get; set; }
        public List<MemberFYReportModel> FinancialYearReportHistory { get; set; }

        public MemberReferenceModel MemberReferences { get; set; }

        public ZipMerchantAPIModel ZipMerchantAPIModel { get; set; }
        public bool AllowResetEmailNotification { get; set; }

        public int SubBusinessTypeKey { get; set; }
        public bool AllowOnlyOneBusinessTypeForAllTerminals { get; set; }
        public bool EnableTransactionListOnEOMReport { get; set; }

        [DisplayName("Fee Free Member")]
        public bool IsFeeFreeMember { get; set; }
    }

    public class Dealer : MemberModel
    {
    }
    public class BankTaxiMember : MemberModel
    {
        public BankTaxiMember()
        {
            this.IsBankCustomer = true;
            this.IsOperator = true;
            this.CompanyKey = 1;
        }
    }
    public class CashTaxiMember : MemberModel
    {
        public CashTaxiMember()
        {
            this.IsBankCustomer = false;
            this.IsOperator = true;
            this.CompanyKey = 1;
        }
    }
    public class BankLEMember : MemberModel
    {
        public BankLEMember()
        {
            this.IsBankCustomer = true;
            this.IsOperator = true;
            this.CompanyKey = 18;
        }
    }

    public class CashLEMember : MemberModel
    {
        public CashLEMember()
        {
            this.IsBankCustomer = false;
            this.IsOperator = true;
            this.CompanyKey = 18;
        }
    }
    public class Customers
    {
        public int CustomerSearchBy { get; set; }
        public string CustomerName { get; set; }
        public long? CustomerKey { get; set; }
        public string CustomerId { get; set; }
        public int? TerminalKey { get; set; }
        public string TerminalId { get; set; }
        public int CustomerType { get; set; }
        public int? CompanyType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TradingName { get; set; }
        public string ABN { get; set; }
        public string ACN { get; set; }
        public string DriverLicenseNumber { get; set; }
        public bool Active { get; set; }
        public byte[] Photo { get; set; }
    }



    public class CustomerListModelAPI
    {
        public List<CustomerModel> CustomerList { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
    }


    public class MemberAddressModelAPI
    {
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string AddressType { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }

    }


    public class MemberModelAPI
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MemberAddressModelAPI PrimaryAddress { get; set; }
        public MemberAddressModelAPI MailingAddress { get; set; }

        public string ReferenceId { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TradingName { get; set; }
        public string ABN { get; set; }
        public string ACN { get; set; }

        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }

        public string DOB { get; set; }

        public Nullable<decimal> DebitRate { get; set; }
        public Nullable<decimal> VisaRate { get; set; }
        public Nullable<decimal> MasterCardRate { get; set; }
        public Nullable<decimal> AmexRate { get; set; }
        public Nullable<decimal> DinersRate { get; set; }

        public Nullable<decimal> UnionPayRate { get; set; }
        public Nullable<decimal> ZipPayRate { get; set; }
        public Nullable<decimal> QantaRate { get; set; }
        public Nullable<decimal> AlipayRate { get; set; }



        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string MerchantID { get; set; }


        public string ResponseText { get; set; }

        public bool IsDebitRatePercent { get; set; }
        public string DriverLicense { get; set; }
        public string DriverAuthorityNumber { get; set; }
    }





}
