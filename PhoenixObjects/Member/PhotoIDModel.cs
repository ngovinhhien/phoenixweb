﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace PhoenixObjects.Member
{
    public class PhotoIDModel
    {
        public PhotoIDModel() {
            this.CreatedDate = this.UpdatedDate = DateTime.Now;
            //this.CreatedByUser = this.UpdatedByUser = "Auto Process";
        }
        public PhotoIDModel(dynamic _photoid) :this()
        {
            if (_photoid.PhotoIDKey != null && _photoid.PhotoIDKey > 0)
                this.PhotoIDKey = _photoid.PhotoIDKey;
            if (_photoid.PhotoID1 != null)
                this.PhotoID = _photoid.PhotoID1;
            if (_photoid.PhotoIDExpiry != null && _photoid.PhotoIDExpiry is DateTime)
                this.PhotoIDExpiry = _photoid.PhotoIDExpiry;
            if (!String.IsNullOrEmpty(_photoid.CreatedByUser))
                this.CreatedByUser = _photoid.CreatedByUser;
            if (!String.IsNullOrEmpty(_photoid.UpdatedByUser))
                this.UpdatedByUser = _photoid.UpdatedByUser;
            if (_photoid.CreatedDate != null && _photoid.CreatedDate is DateTime)
                this.CreatedDate = _photoid.CreatedDate;
            if (_photoid.UpdatedDate != null && _photoid.UpdatedDate is DateTime)
                this.UpdatedDate = _photoid.UpdatedDate;


        }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedByUser { get; set; }
        public Nullable<int> member_key { get; set; }
        public int PhotoIDKey { get; set; }
        public byte[] PhotoID { get; set; }
        [DisplayName("PhotoID Expiry")]
        public Nullable<System.DateTime> PhotoIDExpiry { get; set; }

       
    }
}
