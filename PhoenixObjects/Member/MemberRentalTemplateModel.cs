﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class MemberRentalTemplateModel
    {
        public int MemberRentalTemplateKey { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
    }
}
