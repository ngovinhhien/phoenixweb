﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public enum PaymentType
    {

        Bank = 1,
        Cash = 0
    }
    public enum AddressTypeEnum
    {

        Primary = 1,
        Mailing = 0
    }
}
