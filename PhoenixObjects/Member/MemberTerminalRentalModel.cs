﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class MemberTerminalRentalModel
    {
        public int MemberTerminalRentalKey { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<decimal> RentalAmount { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> TerminalRentalRate_Key { get; set; }
        public decimal? TransactionTarget { get; set; }
       

       
    }
}
