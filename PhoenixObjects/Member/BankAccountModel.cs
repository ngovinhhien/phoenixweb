﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{

    public class BankAccountModelList
    {
        public string BSB { get; set; }
       public List<BankAccountModel> BAccounttModelList { get; set; }
    }

    public class BankAccountModel
    {
        public int Id { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public int Weight { get; set; }
        public int Member_Key { get; set; }
        public MemberModel Member { get; set; }
        public string UserName { get; set; }

        int Accountkey;

        public int AccountKey { get; set; }
    }

    public class BankAccountDTO
    {
        public int AccountKey { get; set; }
        public string  AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string BSB { get; set; }
        public string TransactionPurposeDtoAsJson { get; set; }
    }

    public class TransactionPurposeDTO
    {
        public int PurposeTypeID { get; set; }
        public string PurposeTypeName { get; set; }
        public bool Checked { get; set; }

    }





    class BankAccountModelComparer : IEqualityComparer<BankAccountModel>
    {
        public bool Equals(BankAccountModel c1, BankAccountModel c2)
        {
            return (c1.BSB == c2.BSB && c1.AccountNumber == c2.AccountNumber);
        }

        public int GetHashCode(BankAccountModel bankaccount)
        {
            return bankaccount.GetHashCode();
        }
    }
}
