﻿using PhoenixObjects.Rewards;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PhoenixObjects.Member
{
    public class MemberReferenceEntryModel
    {
        public int MemberReferenceEntryKey { get; set; }
        public Nullable<System.DateTime> EntryDateTime { get; set; }
        public string Description { get; set; }
        public Nullable<int> Points { get; set; }
        public Nullable<int> CreditPoints { get; set; }
        public Nullable<int> DebitPoints { get; set; }
        public Nullable<int> BalancePoints { get; set; }
        public Nullable<int> ReversedMemberReferenceEntryKey { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string UpdatedByUser { get; set; }
        public Nullable<System.DateTime> UpdatedDateTime { get; set; }
        public Nullable<int> MonthPeriod { get; set; }
        public Nullable<int> YearPeriod { get; set; }
        public Nullable<int> Member_Key { get; set; }
        public Nullable<bool> ManualEntry { get; set; }
        public Nullable<int> DocketPayMethodKey { get; set; }
        public MemberModel Member { get; set; }
        public int MemberReferenceEntryTypeKey { get; set; }
        public string PromotionName { get; set; }
        public int PromotionId { get; set; }
        public int QBRPointTypeID { get; set; }

        public string ApprovedRejectAndReason { get; set; }
        public string ExportStatus { get; set; }

        public string EntryType
        {
            get
            {
                switch (MemberReferenceEntryTypeKey)
                {
                    case 1: return "Manual Entry";
                    case 2: return "Points Earned";
                    case 3: return "Points Chargeback";
                    case 4: return "Points Convereted to QBR";
                }
                return "Ünknown";
            }
        }

        public string MonthName
        {
            get
            {
                return !MonthPeriod.HasValue ? String.Empty : new DateTime(2010, MonthPeriod.Value, 1)
                        .ToString("MMM", CultureInfo.InvariantCulture).ToString();
            }
        }
    }
}
