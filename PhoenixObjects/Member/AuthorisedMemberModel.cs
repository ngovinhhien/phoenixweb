﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class AuthorisedMemberModel
    {
        public int AuthorisedMember_key { get; set; }
        public Nullable<int> Member_AuthorisedMember_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string FName { get; set; }
        public string LastName { get; set; }
        public string TradingName { get; set; }
        public string MemberId { get; set; }
        public bool HasPhoto { get; set; }
    }
}
