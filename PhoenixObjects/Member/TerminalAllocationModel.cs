﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PhoenixObjects.Common;
using PhoenixObjects.Terminal;

namespace PhoenixObjects.Member
{
   public class TerminalAllocationModel
    {
        public TerminalAllocationModel()
        {
            VehicleModel = new VehicleModel();
        }
        [Key]
        public int TerminalAllocation_key { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public string EftTerminalId { get; set; }
        public Nullable<int> Vehicle_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string Member_keyName { get; set; }
        public string EFTTerminalLocation { get; set; }
        public string OldTerminalStatus { get; set; }
       
        public System.DateTime EffectiveDate { get; set; }
        public bool IsCurrent { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<bool> PendingSwapOut { get; set; }
        public Nullable<int> SwappedTerminalAllocation_key { get; set; }
        public Nullable<int> OriginalTerminalAllocation_key { get; set; }
        public Nullable<System.DateTime> DeallocatedDate { get; set; }
        public string OriginalTerminalID { get; set; }
        public Nullable<decimal> ServiceFee { get; set; }
        public Nullable<bool> DeliveryRequired { get; set; }
        public Nullable<bool> AllowMOTO { get; set; }
        public Nullable<bool> AllowRefund { get; set; }

        public bool DeliveryRequiredB { get; set; }
        public bool AllowMOTOB { get; set; }
        public bool AllowRefundB { get; set; }
        public MemberModel Member { get; set; }
        public VehicleModel VehicleModel { get; set; }
        public string  EffectiveDateString { get; set; }
        public Nullable<int> State_key { get; set; }
        public Nullable<int> Employee_key { get; set; }
        //public string VehicleId { get; set; }
        //public string RegisteredState { get; set; }
        //public Nullable<System.DateTime> TerminalAllocatedDate { get; set; }
        //public Nullable<System.DateTime> Modified_dttm { get; set; }
        //public Nullable<int> Operator_key { get; set; }
        //public Nullable<int> VehicleType_key { get; set; }
        //public Nullable<System.DateTime> CreatedDate { get; set; }
        public TimeSpan TerminalAllocationTime { get; set; }
        public bool OnlineOrderEnabled { get; set; }

        public List<TerminalAllocationModel> TerminalAllocationList { get; set; }
        public List<SelectListItem> VirtualTerminalTypes { get; set; }
        public int TerminalType_Key { get; set; }
        public string TerminalID { get; set; }

        public bool NotAllowSelectSubBusinessType { get; set; }
    }


   public class AllVehicleTerminalAllocationModel
   {
       public int Vehicle_key { get; set; }
       public string VehicleId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> TerminalAllocatedDate { get; set; }
       public string RegisteredState { get; set; }
       public string Name { get; set; }
       public int VehicleTypeKey { get; set; }
        public Nullable<int> TerminalAllocation_key { get; set; }
       public string TerminalId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DeallocatedDate { get; set; }
        public string OriginalTID { get; set; }
       public string SwappedTID { get; set; }

        public int MemberKey { get; set; }
        public int TerminalKey { get; set; }
        public Nullable<bool> Inactive { get; set; }
        public Nullable<int> AppKey { get; set; }
        public Nullable<int> ReceiptKey { get; set; }
        public bool IsSetEOS { get; set; }
        public bool EnableSendAPIKey { get; set; }
        public bool IsAllocated { get; set; }
        public bool AutoCreatePortalAccount { get; set; }
    }
}
