﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace PhoenixObjects.Member
{
    public class MemberReferenceModel
    {
        public MemberReferenceModel()
        {

        }

        public int MemberReferenceKey { get; set; }
        public Nullable<int> MemberReferenceSourceKey { get; set; }
        public string MemberReferenceNumber { get; set; }
        public Nullable<int> member_key { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
