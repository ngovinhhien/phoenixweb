﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class RewardPointsModel
    {
        public int member_key { get; set; }
        public string memberID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TradingName { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> CurrentDate { get; set; }
        public decimal TotalTrans { get; set; }
        public int TotalPointsUsed { get; set; }
        public Nullable<decimal> TotalPoints { get; set; }
    }

  
}
