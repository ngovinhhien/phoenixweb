﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
  public class MemberBalanceModel
    {
        public string MemberID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TradingName { get; set; }
        public decimal Balance { get; set; }
        public Nullable<int> Member_key { get; set; }
        public byte[] photo { get; set; }
        public Nullable<decimal> WithdrawAmount { get; set; }
    }
}
