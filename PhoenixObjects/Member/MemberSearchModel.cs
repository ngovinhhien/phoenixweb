﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Mvc;

namespace PhoenixObjects.Member
{
   public class MemberSearchModel
    {
       //public List<MemberModel> Members { get; set; }
       public List<MemberModelSimple> Members { get; set; }
       public List<SearchListModel> SearchList { get; set; }
       public List<string> SearchFieldMetadata { get; set; }
       
    }
   public class SingleMemberSearchModel
   {
       public MemberModel Members { get; set; }

   }
   public class MultiMemberSearchModel
   {
       public MultiMemberSearchModel()
       {
           MemberList = new List<MemberModelSimple>();
       }
       public List<MemberModelSimple> MemberList { get; set; }

   }
   //public class MultiMemberSearchModel
   //{
   //    public MultiMemberSearchModel()
   //    {
   //        MemberList = new List<MemberModel>();
   //    }
   //    public List<MemberModel> MemberList { get; set; }

   //}

   public class SearchListModel
   {
       public string SearchKey { get; set; }
       public string SearchField { get; set; }
   }

}
