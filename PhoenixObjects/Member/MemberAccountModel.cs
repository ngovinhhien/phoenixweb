﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class MemberAccountModel
    {
        public int MemberAccountKey { get; set; }
        public Nullable<int> Member_Key { get; set; }
    }
}
