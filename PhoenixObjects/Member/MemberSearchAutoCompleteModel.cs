﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
  public class MemberSearchAutoCompleteModel
    {
        public string MemberId { get; set; }
        public string TradingName { get; set; }
    }
}
