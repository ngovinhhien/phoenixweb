﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
  public class MerchantModel
    {
        public int UserId { get; set; }
        public int MemberKey { get; set; }
        public string MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TradingName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
}
