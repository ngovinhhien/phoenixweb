﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using System.ComponentModel;

namespace PhoenixObjects.Member
{
    public class MemberAddressModel
    {
        [DisplayName("AddressKey")]
        public int AddressKey { get; set; }
        [DisplayName("Street Number")]
        public string StreetNumber { get; set; }
        [DisplayName("Street Name")]
        public string StreetName { get; set; }
        [DisplayName("Suburb Key")]
        public int suburbKey { get; set; }
        public SuburbModel Suburb { get; set; }
        public string AddressType { get; set; }
       
    }

    public class MerchantAddress : MemberAddressModel
    {
        public string SubburbString { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

    }

   
}
