﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
    public class CommonModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
