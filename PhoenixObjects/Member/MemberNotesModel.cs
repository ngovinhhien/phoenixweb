﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class MemberNotesModel
    {
        public Nullable<System.DateTime> NoteDate { get; set; }
        public Nullable<bool> Warning { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<int> Member_key { get; set; }
        public int MemberNote_key { get; set; }
    }
}
