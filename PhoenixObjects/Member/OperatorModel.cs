﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Member
{
   public class OperatorModel
    {
        public Nullable<float> Bond { get; set; }
        public Nullable<System.DateTime> BondPaidDate { get; set; }
        public string BondReceiptNumber { get; set; }
        public string TaxiRegistrationNo { get; set; }
        public Nullable<bool> AllowCash { get; set; }
        public Nullable<bool> PayByEFT { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public Nullable<int> Dealer_key { get; set; }
        public int Operator_key { get; set; }
        public Nullable<int> PayRunGroupId { get; set; }
        public int? PaymentType { get; set; }

        public  DealerModel Dealer { get; set; }
        public  MemberModel Member { get; set; }
    }

     public class PerformanceOperatorModel
     {
         public Nullable<decimal> AmountSum { get; set; }
         public string TradingName { get; set; }
         public string MemberId { get; set; }
         public string TerminalId { get; set; }
         public string Telephone { get; set; }
         public string Mobile { get; set; }
     }
}
