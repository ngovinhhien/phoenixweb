﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using System.ComponentModel;

namespace PhoenixObjects.Member
{
    public class MemberMonthlyReportModel
    {
        [DisplayName("MemberKey")]
        public int MemberKey { get; set; }
        [DisplayName("Month")]
        public int Month { get; set; }
        [DisplayName("Year")]
        public int Year { get; set; }
    }
}
