﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using PhoenixObjects.Terminal;

namespace PhoenixObjects.Member
{
   public class MemberRentalModel
    {
        public int MemberRental_key { get; set; }
        public int Member_key { get; set; }
        public Nullable<bool> RentFree { get; set; }
        public string RentFreeName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> IndividualTerminalCalculation { get; set; }
        public Nullable<decimal> TransactionTargetPerTerminal { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        [Required]
        public Nullable<decimal> RentalAmount { get; set; }
        public Nullable<int> RentalTurnoverRangeKey { get; set; }
        public string RentalTurnoverRangeName { get; set; }
        public List<TerminalRentalRateModel> TerminalRentalRateList { get; set; }
        public MemberModel Member { get; set; }
        public int? RentalMemberRentalTemplateKey { get; set; }
        public DateTime? RentalStartDate { get; set; }
        string _rentalStartDateString ;
        public string RentalStartDateString
        {
            get
            {
                _rentalStartDateString = "";
                if (RentalStartDate.HasValue)
                _rentalStartDateString = RentalStartDate.Value.ToString("yyyy-MM-dd");//  .ToShortDateString();
                return _rentalStartDateString;

            }
            set
            {

                _rentalStartDateString = value;
            }
        }
        //public string RentalStartDateString { get; set; }
        public DateTime? RentalEndDate { get; set; }
        string _rentalEndDateString = "";
        public string RentalEndDateString
        {
            get
            {
                if (RentalEndDate != null)
                    _rentalEndDateString = RentalEndDate.Value.ToString("yyyy-MM-dd"); //.ToShortDateString();
                return _rentalEndDateString;

            }
            set
            {
                _rentalEndDateString = value;
            }
        }
        public bool RentalRentFree { get; set; }
        public List<MemberRentalModel> MemberRentalModelList { get; set; }
        public List<MemberTerminalRentalModel> MemberCurrentTerminalRentalList { get; set; }
    }
}
