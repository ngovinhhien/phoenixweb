﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
   public class WebCashing_GetPendingBatchesByMemberKey
    {
        public string DocketBatchId { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
        public string BatchYear { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<decimal> Others { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string MemberId { get; set; }


    }
}
