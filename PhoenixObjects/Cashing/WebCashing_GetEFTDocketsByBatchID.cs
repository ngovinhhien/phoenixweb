﻿using PhoenixObjects.GlideBoxCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
    public class WebCashing_GetEFTDocketsByBatchID
    {
        public Nullable<int> Member_key { get; set; }
        public Nullable<System.DateTime> docketdate { get; set; }
        public string BatchNumber { get; set; }
        public string BatchStatus { get; set; }
        public Nullable<bool> IsCashable { get; set; }
        public Nullable<decimal> TotalPayable { get; set; }
        public Nullable<decimal> PendingTransactionValue { get; set; }
        public Nullable<decimal> PendingCommision { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string TerminalId { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> TOtalcommission { get; set; }
        public Nullable<decimal> DocketTotal { get; set; }
        public Nullable<decimal> BatchTotalPayable { get; set; }
        public Nullable<decimal> BatchTotal { get; set; }
        public Nullable<decimal> TipPro { get; set; }
        public Nullable<decimal> TippingFee { get; set; }
        public string BatchID { get; set; }
        public string CardType { get; set; }
        public byte[] PhotoID { get; set; }
        public Nullable<decimal> TotalBatchHandlingFee { get; set; }
        public List<WebCashing_GetEFTDocketByBatchIDChild> ResultListChild = new List<WebCashing_GetEFTDocketByBatchIDChild>();
        public Nullable<decimal> Chargebacks { get; set; }  
        public Nullable<decimal> LevyCharge {get;set;}
        public Nullable<decimal> TotalLevyCharge { get; set; }
        public Nullable<decimal> LevyChargeSummary { get; set; }

        public decimal GlideboxCommission { get; set; }
        public decimal TotalGlideboxCommission { get; set; }

        public List<GlideboxCategoriesCommissionForSameDayCashingModel> ResultGlideboxCommissionListChild = new List<GlideboxCategoriesCommissionForSameDayCashingModel>();
    }
}
