﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
  public  class WebCashing_GetPendingBatchesDetailsByMemberKey
    {       
            public string Description { get; set; }
            public string DocketBatchId { get; set; }
            public string TerminalID { get; set; }
            public string BatchNumber { get; set; }
            public string BatchYear { get; set; }
            public Nullable<decimal> FaceValue { get; set; }
            public Nullable<decimal> Commission { get; set; }
            public Nullable<decimal> Others { get; set; }
            public Nullable<decimal> Total { get; set; }
            public Nullable<System.DateTime> DocketDate { get; set; }
            public decimal PayableFaceValue { get; set; }
            public decimal CommissionRate { get; set; }
            public decimal PayableCommission { get; set; }
            public decimal PayableOthers { get; set; }
            public Nullable<decimal> TotalPayable { get; set; }
            public int Docket_key { get; set; }

            public decimal GlideboxCommission { get; set; }
    }

  public  class WebCashing_GetPendingBatchesHeaderByMemberKey
    {
      public WebCashing_GetPendingBatchesHeaderByMemberKey()
      {
          WebCashing_GetPendingBatchesDetailsByMemberKey = new List<WebCashing_GetPendingBatchesDetailsByMemberKey>();
      }
        public string Description { get; set; }
        public string DocketBatchId { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
        public string BatchYear { get; set; }
        public Nullable<decimal> FaceValue { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<decimal> Others { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string count { get; set; }
       public List<WebCashing_GetPendingBatchesDetailsByMemberKey> WebCashing_GetPendingBatchesDetailsByMemberKey { get; set; }

        public decimal? TotalGlideboxCommision { get; set; }
    }
}
