﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
  public  class DocketCheckModel
    {
        public Nullable<System.DateTime> docketdate { get; set; }
        public string docketdateString { get; set; }
        public int docket_key { get; set; }
        public string DocketBatchId { get; set; }
        public string CreatedByUser { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public string state { get; set; }
        public string TerminalId { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> BatchTotalPayable { get; set; }
        public Nullable<decimal> TotalPayable { get; set; }
        public string CardNumberMasked { get; set; }
        public string BatchNumber { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public string BatchStatus { get; set; }
    }
}
