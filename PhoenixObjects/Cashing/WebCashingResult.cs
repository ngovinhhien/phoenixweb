﻿using PhoenixObjects.Common;
using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
    public class WebCashingResult
    {
        public string memberid { get; set; }
        public string TradingName { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? FaceValueTotal { get; set; }
        public decimal? ChargebacksTotal { get; set; }
        public decimal? GrandPaidTotal { get; set; }
        public decimal? GrandfaceValue_N_PaidTotal { get; set; }
        public decimal? CommissionTotal { get; set; }
        public decimal? ExtraTotal { get; set; }
        public decimal? DocketTotal { get; set; }
        public decimal? TipProTotal { get; set; }
        public decimal? TippingFeeTotal { get; set; }
        public Nullable<decimal> BatchTotal { get; set; }
        public byte[] PhotoID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string ABN { get; set; }
        public int TotalPointsUsed { get; set; }
        public int Count { get; set; }
        public Nullable<decimal> TotalPoints { get; set; }
        public Nullable<decimal> TotalPointsAvailable { get; set; }
        public IEnumerable<MemberNotesModel> MemberNotes { get; set; }
        public string BatchStatus { get; set; }
        public string BatchNumber { get; set; }
        public int DocketCheckSummaryKey { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string DriverCertificateId { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal? LastCashedAmt { get; set; }
        public DateTime? LastCashedDateTime { get; set; }
        public int? Companykey { get; set; }
        public decimal Debit { get; set; }
        public decimal Visa { get; set; }
        public decimal Master { get; set; }
        public decimal Diners { get; set; }
        public decimal Amex { get; set; }
        public decimal UnionPay { get; set; }
        public decimal ZipPay { get; set; }
        public decimal Qantas { get; set; }
        public decimal Alipay { get; set; }
        public decimal QantasPercentage{ get; set; }
        public decimal DebitPercentage { get; set; }


        public string ErrorMsg { get; set; }


        public Nullable<decimal> HandlingFeeSummary { get; set; }
        public List<WebCashing_GetEFTDocketsByBatchID> ResultList = new List<WebCashing_GetEFTDocketsByBatchID>();
        public List<WebCashing_MassScanModel> WebCashing_MassScanModelList = new List<WebCashing_MassScanModel>();
        public List<VoucherTypeModel> VoucherTypeList = new List<VoucherTypeModel>();
        public List<TerminalAllocationModel> TerminalAllocationList = new List<TerminalAllocationModel>();
        public Nullable<decimal> ChargebacksFeeSummary { get; set; }
        //public decimal? ChargebacksTotal { get; set; }
        public Nullable<decimal> LevyChargeSummary { get; set; }

        public decimal GlideboxCommission { get; set; }
        public decimal TotalGlideboxCommission { get; set; }
    }
}
