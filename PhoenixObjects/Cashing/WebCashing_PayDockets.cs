﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
    public class WebCashing_PayDockets
    {
        public Nullable<int> TransactionKey { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public string LodgementReferenceNo { get; set; }
    }
}
