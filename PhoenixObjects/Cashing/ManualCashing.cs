﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Cashing
{
   public class ManualCashing
    {      
        public List<ServiceFeeRadio> ServiceFeeRadioList = new List<ServiceFeeRadio>();
        public string  MemberId { get; set; }
        public string BusinessTypeId { get; set; }
        public decimal ServiceFeeRateId { get; set; }
      
    }
    public class ServiceFeeRadio
    {
        public decimal ServiceFeeRate { get; set; }
        public string BusinessType { get; set; }
    }
}
