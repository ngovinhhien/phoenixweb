﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using PhoenixObjects.GlideBoxCategory;

namespace PhoenixObjects.Financial
{
   public class CommissionRate_AddedMemberRateSameModel
   {
       public int Id { get; set; }
       public string Name { get; set; }
       public Nullable<int> DocketPayMethod_key { get; set; }
       public Nullable<double> FaceValue { get; set; }
       public Nullable<decimal> ManualCommissionRate { get; set; }
       public Nullable<decimal> CommissionRate { get; set; }
       public Nullable<double> Total { get; set; }
       public Nullable<double> Commission { get; set; }
       public string HandlingCharge { get; set; }
       public Nullable<int> Quantity { get; set; }

        [Display(Name = "Levy Charge", Description = "Levy Charge for each transaction")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid levy charge")]
        public double LevyCharge { get; set; }

    }
   public class CommissionRate_AddedMemberRateModelSameTotals
   {
       public Nullable<double> FaceValue { get; set; }
       public Nullable<double> Commission { get; set; }
       public Nullable<double> HandlingChrg { get; set; }
       public Nullable<double> Total { get; set; }
   }

   public class CommissionRate_MemberRateModelSameList
   {
       public string BottomBC { get; set; } 
       public string TopBC { get; set; }
       public string Terminal { get; set; }
       public string BatchAmount { get; set; }
       public List<CommissionRate_MemberRateModel> MemberRateModelList = new List<CommissionRate_MemberRateModel>();
       public List<CommissionRate_AddedMemberRateSameModel> AddedMemberRateSameModelList = new List<CommissionRate_AddedMemberRateSameModel>();
       public CommissionRate_AddedMemberRateModelSameTotals AddedMemberRateSameTotalModelList = new CommissionRate_AddedMemberRateModelSameTotals();

        public List<GlideboxCategoriesCommissionForSameDayCashingModel> GlideboxCommissionList = new List<GlideboxCategoriesCommissionForSameDayCashingModel>();

        public List<GlideboxCategoriesCommissionForSameDayCashingModel> AddedGlideboxCommissionList = new List<GlideboxCategoriesCommissionForSameDayCashingModel>();
    }
}
