﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PhoenixObjects.Financial
{
   public class CommissionRate_MemberRateModel
    {
      
       public Nullable<double> FaceValue { get; set; }

        public Nullable<int> DocketPayMethod_key { get; set; }
        public Nullable<int> DocketPayMethod_key1 { get; set; }
        public Nullable<decimal> ManualCommissionRate { get; set; }
        public string BusinessType { get; set; }
        public Nullable<decimal> OperatorCommissionRate { get; set; }
        public string Name { get; set; }
        public Nullable<int> vehicletype_key { get; set; }
        public string MerchantId { get; set; }
        public int CommissionRate_key { get; set; }
        public int member_key { get; set; }
        public string memberid { get; set; }
        public bool FlatAmount { get; set; }
        public Nullable<decimal> HandlingAmount { get; set; }
        public Nullable<double> HandlingRate { get; set; }
        public string HandlingCharge { get; set; }

    }
   public class CommissionRate_AddedMemberRateModel
   {
       public int Id { get; set; }
       public string Name { get; set; }
       public Nullable<int> DocketPayMethod_key { get; set; }
       public Nullable<double> FaceValue { get; set; }
       public Nullable<decimal> ManualCommissionRate { get; set; }
       public Nullable<double> Total { get; set; }
       public string HandlingCharge { get; set; }

        public Nullable<decimal> LevyCharge { get; set; }
    }
   public class CommissionRate_AddedMemberRateModelTotals
   {
       public string CardName { get; set; }
       public int Qty { get; set; }
       public Nullable<double> Total { get; set; }

   }

   public class CommissionRate_MemberRateModelList
   {
       public decimal ServiceFeeRate { get; set; }
       public string BusinessType { get; set; }
       public List<CommissionRate_MemberRateModel> MemberRateModelList = new List<CommissionRate_MemberRateModel>();
       public List<CommissionRate_AddedMemberRateModel> AddedMemberRateModelList = new List<CommissionRate_AddedMemberRateModel>();
       public List<CommissionRate_AddedMemberRateModelTotals> AddedMemberRateTotalModelList = new List<CommissionRate_AddedMemberRateModelTotals>();

   }
}
