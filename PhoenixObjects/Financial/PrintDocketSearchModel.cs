﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PhoenixObjects.Financial
{
    public class PrintDocketSearchModel
    {
        public PrintDocketSearchModel()
        {
            MemberID = string.Empty;
            TerminalID = string.Empty;
            TaxiID = string.Empty;
            CardMask = string.Empty;
            CardType = string.Empty;
            ApproveStatus = string.Empty;
            ProcessBy = string.Empty;
            BatchNumber = string.Empty;
            EndDateString = string.Empty;
            StartDateString = string.Empty;
            ReceiptNo = string.Empty;
        }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? FileDateFrom { get; set; }
        public DateTime? FileDateTo { get; set; }
        public string MemberID { get; set; }
        public string WestPacMerchantID { get; set; }
        public string AmexMerchantID { get; set; }
        public string TerminalID { get; set; }
        public string TaxiID { get; set; }
        public string CardMask { get; set; }
        public string CardType { get; set; }
        public int CompanyKey { get; set; }
        public string ApproveStatus { get; set; }
        public bool ApproveStatusBool { get; set; }
        public bool DeclineStatusBool { get; set; }
        public bool BothStatusBool { get; set; }

        public bool DocketPaidStatusChecked { get; set; }
        public bool DocketPendingStatusChecked { get; set; }
        public bool DocketUnknownStatusChecked { get; set; }
        public string ProcessBy { get; set; }
        public string BatchNumber { get; set; }
        [DisplayName("Total Amount From")]
        public double? DocketFrom { get; set; }
        [DisplayName("Total Amount To")]
        public double? DocketTo { get; set; }
        public string EndDateString { get; set; }
        public string StartDateString { get; set; }
        public string FileDateToString { get; set; }
        public string FileDateFromString { get; set; }
        public string ReceiptNo { get; set; }
        public bool OnlyDisplayGlideboxTransaction { get; set; }

        public List<TransactionSearch_ResultModel> TransactionSearch_ResultModel = new List<TransactionSearch_ResultModel>();
    }
}