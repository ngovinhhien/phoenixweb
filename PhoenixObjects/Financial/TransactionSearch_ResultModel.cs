﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Financial
{
   public class TransactionSearch_ResultModel
    {
        public string DocketStatus { get; set; }
        public string paytype { get; set; }
        public string PayDate { get; set; }
        public string PaidTo { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
        public string Merchantid { get; set; }
        public string Taxi { get; set; }
        public string DriverId { get; set; }
        public string StartShift { get; set; }
        public string EndShift { get; set; }
        public string Status { get; set; }
        public string Authorisation { get; set; }
        public string CardType { get; set; }
        public string MaskedCardNum { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Fare { get; set; }
        public string CashOut { get; set; }
        public string Extras { get; set; }
        public string ServiceFee { get; set; }
        public string TotalAmount { get; set; }
        public decimal GlideboxAmount { get; set; }
        public decimal GlideboxCommission { get; set; }
        public string Pickup { get; set; }
        public string DropOff { get; set; }
        public string SourceFile { get; set; }
        public string OfficeName { get; set; }
        public string Filedate { get; set; }
        public string ProcessedBy { get; set; }
        public string TransactionID { get; set; }
        public string IsApproved { get; set; }
        public string CardEntryMode { get; set; }
        public string BusinessTypeName { get; set; }
        public string AmexMID { get; set; }
        public string TransactionDateTime { get; set; }
    }
}
