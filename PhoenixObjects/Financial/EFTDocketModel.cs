﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Financial
{
  public class EFTDocketModel
    {
        public Nullable<long> BatchId { get; set; }
        public Nullable<System.DateTime> TripStart { get; set; }
        public Nullable<System.DateTime> TripEnd { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<int> Authorisation { get; set; }
        public Nullable<int> Passengers { get; set; }
        public Nullable<float> Tarrif { get; set; }
        public Nullable<float> Distance { get; set; }
        public string PickerZone { get; set; }
        public string DropoffZone { get; set; }
        public string CadmusFilename { get; set; }
        public string Status { get; set; }
        public string BatchNumber { get; set; }
        public Nullable<int> TransactionNumber { get; set; }
        public Nullable<bool> Presented { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public int EftDocket_key { get; set; }
        public Nullable<int> EftTerminal_key { get; set; }
        public Nullable<int> Operator_key { get; set; }
        public Nullable<int> Vehicle_key { get; set; }
        public Nullable<bool> Quarantined { get; set; }
        public string terminalid { get; set; }
    }
}
