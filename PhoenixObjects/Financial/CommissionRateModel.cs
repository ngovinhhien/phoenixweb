﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Financial
{
   public class CommissionRateModel
    {
        public int Number { get; set; }
        public Nullable<decimal> GrossDealerCommissionRate { get; set; }
        public Nullable<decimal> OperatorCommissionRate { get; set; }
        public Nullable<decimal> ManualCommissionRate { get; set; }
        public Nullable<decimal> TransactionFee { get; set; }
        public Nullable<decimal> RebateForPurchaseWithCashOut { get; set; }
        public Nullable<decimal> RebateForCashAdvance { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> Current { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public int CommissionRate_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<int> DocketPayMethod_key { get; set; }
        public Nullable<bool> FixAmount { get; set; }
        public string SubBusinessType { get; set; }
        public string BusinessType { get; set; }
        public string CardName { get; set; }
        public string State { get; set; }
        public decimal? CustomCommissionRate { get; set; }
    }
   
   public class DefaultRateModel
   {
       public Nullable<decimal> Visa { get; set; }
       public Nullable<decimal> VisaDebit { get; set; }
       public Nullable<decimal> Master { get; set; }
       public Nullable<decimal> MasterDebit { get; set; }
       public Nullable<decimal> Amex { get; set; }
       public Nullable<decimal> Diners { get; set; }
       public Nullable<decimal> Debit { get; set; }
       public Nullable<decimal> DebitPercentage { get; set; }
        public Nullable<decimal> JCB { get; set; }

        public Nullable<decimal> UnionPay { get; set; }
        public Nullable<decimal> Zip { get; set; }
        public Nullable<decimal> Alipay { get; set; }
        public Nullable<decimal> Qantas { get; set; }
        public Nullable<decimal> QantasPercentage { get; set; }
        public decimal? DebitTranFee { get; set; }
        public decimal? VisaTranFee { get; set; }
        public decimal? VisaDebitTranFee { get; set; }
        public decimal? MasterTranFee { get; set; }
        public decimal? MasterDebitTranFee { get; set; }
        public decimal? AmexTranFee { get; set; }
        public decimal? DinersTranFee { get; set; }
        public decimal? UnionPayTranFee { get; set; }
        public decimal? ZipTranFee { get; set; }
        public decimal? QantasTranFee { get; set; }
        public decimal? AlipayTranFee { get; set; }
        public decimal? JCBTranFee { get; set; }
        public decimal? DebitRebateForPurchaseWithCashOut { get; set; }
        public decimal? DebitRebateForCashAdvance { get; set; }
        public bool DebitIsFixed { get; set; }
        public bool QantasIsFixed { get; set; }
        public int Visa_Commision_Key { get; set; }
        public int VisaDebit_Commision_Key { get; set; }
       public int Master_Commision_Key { get; set; }
       public int MasterDebit_Commision_Key { get; set; }
       public int Amex_Commision_Key { get; set; }
       public int Diners_Commision_Key { get; set; }
        public int UnionPay_Commision_Key { get; set; }
        public int Zip_Commision_Key { get; set; }
        public int Alipay_Commision_Key { get; set; }
        public int JCB_Commision_Key { get; set; }
        public int Qantas_Commision_Key { get; set; }
        public int QantasPercentage_Commision_Key { get; set; }
        public int Debit_Commision_Key { get; set; }
       public int DebitPercentage_Commision_Key { get; set; }
        public string SubBusinessType { get; set; }
        public string BusinessType { get; set; }
    }

   public class DefaultRateVMModel
   {
       public string Visa { get; set; }
       public string Master { get; set; }
       public string Amex { get; set; }
       public string Diners { get; set; }
       public string Debit { get; set; }
        public string UnionPay { get; set; }
        public string Qantas { get; set; }
        public string Zip { get; set; }
        public string Alipay { get; set; }
    }

    public class CommissionRateVM
    {
        public Nullable<decimal> CRDebitRate { get; set; }
        public int CRDebitRateKey { get; set; }
        public Nullable<decimal> CRDebitRatePercentage { get; set; }
        public int CRDebitRatePercentageKey { get; set; }
        public Nullable<decimal> CRVisaRate { get; set; }
        public int CRVisaRateKey { get; set; }
        public Nullable<decimal> CRMasterCardRate { get; set; }
        public int CRMasterCardRateKey { get; set; }
        public Nullable<decimal> CRAmexRate { get; set; }
        public int CRAmexRateKey { get; set; }
        public Nullable<decimal> CRDinersRate { get; set; }
        public int CRDinersRateKey { get; set; }

        public Nullable<decimal> CRUnionPayRate { get; set; }
        public int CRUnionPayRateKey { get; set; }

        public Nullable<decimal> CRZipRate { get; set; }
        public int CRZipRateKey { get; set; }

        public Nullable<decimal> CRQantasRate { get; set; }
        public int CRQantasRateKey { get; set; }

        public Nullable<decimal> CRQantasRatePercentage { get; set; }
        public int CRQantasRatepercentageKey { get; set; }


        public Nullable<decimal> CRAlipayRate { get; set; }
        public int CRAlipayRateKey { get; set; }
        public Nullable<decimal> CRDealerRate { get; set; }
        public int CRDealerRateKey { get; set; }
        public Nullable<decimal> CRManualVoucherRate { get; set; }
        public int CRManualVoucherRateKey { get; set; }
        public DefaultRateModel DefaultRateModel { get; set; }
        public string DebitRateType { get; set; }
        public string QantasRateType { get; set; }
        public string BusinessType { get; set; }
        public int VehicleTypeId { get; set; }

        private string GetRateString(decimal? rate, bool fixable = false)
        {
            if (fixable)
            {
                if (Math.Abs(rate.GetValueOrDefault(0.0m)) > 0.10m && fixable)
                    return String.Format("${0:N4}", rate.GetValueOrDefault(0.0m));
                else
                    return String.Format("{0:N4}%", (rate.GetValueOrDefault(0.0m) * 100.0m));
            }
            else
            {
                return String.Format("{0:N4}%", (rate.GetValueOrDefault(0.0m)));
            }
            
        }
        public string DebitRateString { 
            get 
            {
                return GetRateString(CRDebitRate, true);
                
            } 
        }

        public string VisaRateString
        {
            get
            {
                return GetRateString(CRVisaRate);
            }
        }

        public string MasterRateString
        {
            get
            {
                return GetRateString(CRMasterCardRate);
            }
        }

        public string AmexRateString
        {
            get
            {
                return GetRateString(CRAmexRate);
            }
        }

        public string DinersRateString
        {
            get
            {
                return GetRateString(CRDinersRate);

            }
        }

        public string UnionPayRateString
        {
            get
            {
                return GetRateString(CRUnionPayRate);

            }
        }

        public string ZipRatesString
        {
            get
            {
                return GetRateString(CRZipRate);

            }
        }

        public string QantasRateString
        {
            get
            {
                return GetRateString(CRQantasRate, true);

            }
        }

        public string AlipayRateString
        {
            get
            {
                return GetRateString(CRAlipayRate);

            }
        }
    }

    public class CommissionRateModelVM
    {
        public CommissionRateVM CommissionRateModel { get; set; }
        public CommissionRateVM CommissionRateModelAdded { get; set; }
        public CommissionRateVM SalesforceData { get; set; }
        public CommissionRateVM CurrentCommissionRateModel { get; set; }
        public string StartDate { get; set; }
        public string MemberID { get; set; }
    }
}
