using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.TransactionHostLog
{
    public class SearchLogModel
    {
        public int Id { get; set; }
        [Display(Name = "Logged Date From")]
        [Required]
        public DateTime? LoggedDateFrom { get; set; }
        [Display(Name = "Logged Date To")]
        [Required]
        public DateTime? LoggedDateTo { get; set; }
        public DateTime? Date { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        [Display(Name = "Message")]
        [MaxLength(4000)]
        public string Message { get; set; }
        public string Exception { get; set; }
        [Display(Name = "Terminal ID")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string TerminalID { get; set; }
    }
}
