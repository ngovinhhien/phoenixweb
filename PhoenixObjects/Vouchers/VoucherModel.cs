﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Vouchers
{
   public class VoucherModel
    {
        public int Voucher_key { get; set; }
        public string VoucherNumber { get; set; }
        public int Member_key { get; set; }
        public int VoucherType_key { get; set; }
        public Nullable<int> VoucherValidity_Key { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<System.DateTime> UseDate { get; set; }
        public string Comment { get; set; }
        public Nullable<int> PointUsed { get; set; }
        public string memberid { get; set; }
        public string tradingname { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string VoucherTypeName { get; set; }
        public int ExpiryPeriod { get; set; }
        public Nullable<System.DateTime> expirydate { get; set; }
        public Nullable<System.DateTime> LastestScanDate { get; set; }
        public Nullable<int> UsageQuantity { get; set; }
        public Nullable<int> UsagePeriod { get; set; }
        public Nullable<int> UsagePeriodLimit { get; set; }
        public bool MultipleUse { get; set; }
        public Nullable<int> ScanCount { get; set; }
        public Nullable<decimal> CashValue { get; set; }
        public Nullable<int> TerminalAllocation_key { get; set; }
        public Nullable<System.DateTime> VoucherTypeStartDate { get; set; }
        public Nullable<System.DateTime> VoucherTypeEndDate { get; set; }
        public Nullable<decimal> TransactionAmountTarget { get; set; }
        public Nullable<int> TerminalAmountTarget { get; set; }
        public Nullable<decimal> TerminalTransactionTotal { get; set; }
        public Nullable<System.DateTime> TerminalTransactedStartDate { get; set; }
        public Nullable<int> TransactionCalculatedPeriod { get; set; }
        public Nullable<System.DateTime> TransactionCutOffDate { get; set; }
        public int ScanCountWithinCurrentPeriod { get; set; }
    }
}
