﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Vouchers
{
   public class SPGetVoucherTypeByCompanyModel    
    {
        public int VoucherType_key { get; set; }
        public string vouchername { get; set; }
        public int ConversionValue { get; set; }
        public int ExpiryPeriod { get; set; }
        public int Company_key { get; set; }
        public string VoucherPrefix { get; set; }
        public Nullable<int> nextnumber { get; set; }
        public bool UsedLater { get; set; }
    }
}
