﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Vouchers
{
   public class SPGetVoucherByMemberKeyModel
    {
        public int voucher_key { get; set; }
        public int member_key { get; set; }
        public string VoucherTypeName { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<System.DateTime> LastestScanDate { get; set; }
        public decimal CashValue { get; set; }
        public string vouchernumber { get; set; }
    }
}
