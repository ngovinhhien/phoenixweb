﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
//using System.Web.Mvc;
using PhoenixObjects.Financial;
using PhoenixObjects.Terminal;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Report;
using PhoenixObjects.Customer;
using PhoenixObjects.Transaction;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace PhoenixObjects.Fee
{
    public class MemberRefundFeeModel
    {
        public int MemberKey { get; set; }
        public int FeeChargeHistoryID { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public string TicketNumber { get; set; }
    }
}
