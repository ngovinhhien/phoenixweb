﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace PhoenixObjects.Fee
{
    public class FeeChargeDetailModel
    {
        public int FeeChargeDetailID { get; set; }
        public int MemberKey { get; set; }
        public decimal Amount { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [MaxLength(300)]
        public string Reason { get; set; }
        [Required]
        [MaxLength(10)]
        [RegularExpressionAttribute("^[0-9]{1,10}$")]
        public string TicketNumber { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public string StartDate { get; set; }

    }
}
