﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Fee
{
    public class MemberFeeChargeHistory
    {
        public int ID { get; set; }
        public int RefundID { get; set; }

        public string IDCombined
        {
            get { return ID + "_" + RefundID; }
        }
        public DateTime ProcessedDate { get; set; }
        public string FeeTypeName { get; set; }
        public int FeeTypeID { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime? FailedDate { get; set; }
        public bool IsRefunded { get; set; }
        public string ParentReferenceNumber { get; set; }
        public string RefundDate { get; set; }
        public string RefundReason { get; set; }
        public string RefundTicketNumber { get; set; }
        public string StatusDescription { get; set; }
        public decimal? RefundAmount { get; set; }
    }
}
