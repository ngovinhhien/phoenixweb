﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Fee
{
    public class MemberFeeChangeHistory
    {
        public int ID { get; set; }
        public DateTime ProcessedDate { get; set; }
        public string Username { get; set; }
        public string Reason { get; set; }
        public decimal? OldAmount { get; set; }
        public decimal? NewAmount { get; set; }
        public bool? IsActive { get; set; }
        public string TicketNumber { get; set; }
        public string Description { get; set; }

    }
}
