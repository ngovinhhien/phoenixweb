﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
//using System.Web.Mvc;
using PhoenixObjects.Financial;
using PhoenixObjects.Terminal;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Report;
using PhoenixObjects.Customer;
using PhoenixObjects.Transaction;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace PhoenixObjects.Fee
{
    public class MemberFeeModel
    {
        public string MemberID { get; set; }
        public int MemberKey { get; set; }
        public int FeeTypeID { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }
        public string TicketNumber { get; set; }
        public int FrequencyID { get; set; }
        public string FeeAsJson { get; set; }
    }
}
