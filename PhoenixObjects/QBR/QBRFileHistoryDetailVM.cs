﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.QBR
{
    public class QBRFileHistoryDetailVM
    {
        public List<QBRFileHistoryVM> qbrFileHistories { get; set; }
        public List<QBRFileDetailVM> qbrFileDetails { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string Points { get; set; }
        public string SummaryEntryDescription { get; set; }
        public string PromotionName { get; set; }
        public string QBRNumber { get; set; }
        public bool IsError { get; set; }
    }

    public class QBRFileHistoryVM
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SubmittedDate { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public string UploadStatus { get; set; }
    }

    public class QBRFileDetailVM
    {
        public int MonthPeriod { get; set; }
        public int YearPeriod { get; set; }
        public string SubBusinessTypeName { get; set; }
        public string CardTypeName { get; set; }
        public double? ExchangeRate { get; set; }
        public int Points { get; set; }
        public string Description { get; set; }
    }
}
