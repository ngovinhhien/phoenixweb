﻿using PhoenixObjects.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.QBR
{
    public class MemberQBRPointHistoryCriteriaVM
    {
        public int MemberKey { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
