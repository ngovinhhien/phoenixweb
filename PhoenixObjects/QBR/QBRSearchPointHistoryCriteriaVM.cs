﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixObjects.QBR
{
    public class QBRSearchPointHistoryCriteriaVM
    {
        public string MemberID { get; set; }
        public string CompanyKey { get; set; }
        public string PromotionID { get; set; }
        public string FileName { get; set; }
        public string QBRNumber { get; set; }
        public string StatusID { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string SubmittedFrom { get; set; }
        public string SubmittedTo { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}