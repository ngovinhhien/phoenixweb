﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.QBR
{
    public class QBRPointHistoryResultVM
    {
        public int MemberReferenceEntrySummaryKey { get; set; }
        public string TradingName { get; set; }
        public string MemberID { get; set; }
        public int Points { get; set; }
        public string CompanyName { get; set; }
        public string QBRNumber { get; set; }
        public int MonthPeriod { get; set; }
        public int YearPeriod { get; set; }
        public string PromotionName { get; set; }
        public string FileName { get; set; }
        public string Status { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string Period
        {
            get
            {
                return MonthPeriod + "/" + YearPeriod;
            }
        }
        public string Pointstr
        {
            get { return String.Format("{0:n0}", Points); }
        }
    }
}
