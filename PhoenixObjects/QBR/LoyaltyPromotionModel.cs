﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.QBR
{
    public class LoyaltyPromotionModel
    {
        public int PromotionKey { get; set; }
        public string PromotionName { get; set; }
    }
}
