﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.QBR
{
    public class MemberQBRPointHistoryResultVM
    {
        public int MemberReferenceAuditKey { get; set; }
        public string MemberReferenceNumber { get; set; }
        public Nullable<int> member_key { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
