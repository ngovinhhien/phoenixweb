﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.PricingPlan
{
    public class PricingPlanQueryModel
    {
        public int MSFPLanID { get; set; }
        public string MSFPLanName { get; set; }
        public int PayMethodKey { get; set; }
        public string PayMethodName { get; set; }
        public decimal Rate { get; set; }
        public decimal TransactionFee { get; set; }
        public bool IsRateFixed { get; set; }
    }
}
