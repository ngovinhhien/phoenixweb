﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixObjects.CustomAttributes
{
    public class RequireIfAttribute : ValidationAttribute, IClientValidatable
    {
        public string PropertyTo { get; set; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult valid = null;
            var property = validationContext.ObjectType.GetProperty(PropertyTo);
            if (PropertyTo != null)
            {
                var otherValue = property.GetValue(validationContext.ObjectInstance, null);
                if (otherValue != null && otherValue.ToString() != string.Empty && (value == null || value.ToString() == string.Empty))
                {
                    valid = new ValidationResult(ErrorMessageString);
                }
            }
            return valid;
        
        }
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var valid = new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessageString,
                ValidationType = "requiredif"
            };
            valid.ValidationParameters.Add("propertyto", PropertyTo);

            yield return valid;
        }
    }

}