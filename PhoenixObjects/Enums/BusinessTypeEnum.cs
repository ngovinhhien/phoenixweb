﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Enums
{
    public enum BusinessType
    {
        TaxiEPay = 1,
        LiveEftpos = 18,
        DirectLiveEftpos = 19,
        TaxiPro = 20,
        TaxiApp = 21,
        LiveEftposBlack = 22,
        LiveEftposBlackForTaxi = 23,
        LiveLimo = 24,
        Glide = 25,
        LiveEftposIntegrated = 26,
        LiveEftposBlackIntegrated = 27,
        LiveSMS = 28,

    }
}
