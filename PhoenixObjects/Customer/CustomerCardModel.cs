﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Customer
{
   public class CustomerCardModel
    {
        public int CustomerCardKey { get; set; }
        public string CardMasked { get; set; }
        public Nullable<int> ExpiryMonth { get; set; }
        public Nullable<int> ExpiryYear { get; set; }
        public string CardHolderName { get; set; }
        public string Token { get; set; }
        public string CardHash { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
    }
}
