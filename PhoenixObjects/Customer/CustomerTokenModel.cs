﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Customer
{
  public  class CustomerTokenModel
    {

        public int CustomerTokenKey { get; set; }
        public Nullable<int> CustomerKey { get; set; }
        public string Token { get; set; }
        public Nullable<System.DateTime> DeactivateDate { get; set; }
      
        public virtual CustomerAccountModel CustomerAccount { get; set; }
        public virtual CustomerCardModel CustomerCard { get; set; }
    }
}
