﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Customer
{
  public  class CustomerAccountModel
    {
        public int CustomerAccountKey { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
    }


    public class BSBListModel
    {
        public int BSBListKey { get; set; }
        public string BSB { get; set; }
        public string Bank { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
    }
}
