﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Customer
{
   public class CustomerAddressModel
    {
        public int CustomerAddressKey { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public Nullable<int> Suburb_Key { get; set; }
        public Nullable<int> CustomerKey { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
    }
}
