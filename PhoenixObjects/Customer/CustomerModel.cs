﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixObjects.Customer
{
  public  class CustomerModel
    {

      public CustomerModel()
      {
          CustomerAddresses = new List<CustomerAddressModel>();
          CustomerTokens = new List<CustomerTokenModel>();
      }
        public int CustomerKey { get; set; }
        public string TradingName { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public Nullable<int> PrimaryAddressKey { get; set; }
        public Nullable<int> MailingAddressKey { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<bool> AbsorbSurcharge { get; set; }
        public Nullable<int> DefaultPaymentTokenKey { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
     
        public List<CustomerAddressModel> CustomerAddresses { get; set; }
        public List<CustomerTokenModel> CustomerTokens { get; set; }
    }
}
