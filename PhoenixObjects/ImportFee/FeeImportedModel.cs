﻿using System;

namespace PhoenixObjects.ImportFee
{
    public class FeeImportedModel
    {
        public int FeeImporedFileId { get; set; }
        public string FileName { get; set; }
        public string UploadedBy { get; set; }
        public DateTime UploadDate { get; set; }
        public string UploadDateString => UploadDate.ToString("dd-MM-yyyy hh:mm tt");
        public string StoredFileName { get; set; }
    }
}