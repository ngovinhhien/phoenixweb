﻿using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace PhoenixObjects.ImportFee
{
    public class InterchangeSchemeFeeModel
    {
        public InterchangeSchemeFeeModel()
        {
            ErrorDescriptionList = new List<string>();
        }

        public int MKey { get; set; }
        public string TradingName { get; set; }
        public string MID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string CardType { get; set; }
        public string FeeType { get; set; }
        public string Description { get; set; }
        public string Txns_No { get; set; }
        public string Rate { get; set; }
        public string FeeValue { get; set; }
        public List<string> ErrorDescriptionList { get; }
        public string ErrorDescription => string.Join(", ", ErrorDescriptionList.ToArray());
        public int NumOfDuplicated { get; set; }
    }
}