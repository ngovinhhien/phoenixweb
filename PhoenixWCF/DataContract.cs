﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Runtime.Serialization;

namespace PhoenixWebWCF
{
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class MemberModel
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public int MemberKey
        {
            get;
            set;
        }

        [DataMember]
        public string MemberID
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }
        [DataMember]
        public string Tradingname
        {
            get;
            set;
        }
        [DataMember]
        public string ABN
        {
            get;
            set;
        }
        [DataMember]
        public string ACN
        {
            get;
            set;
        }
        [DataMember]
        public string DriverLicenseNumber
        {
            get;
            set;
        }
        [DataMember]
        public bool Active
        {
            get;
            set;
        }

        [DataMember]
        public BusinessTypeModel BusinessType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsOperator
        {
            get;
            set;
        }

        [DataMember]
        public bool IsDealer
        {
            get;
            set;
        }

       

        [DataMember]
        public bool IsBankCustomer
        {
            get;
            set;
        }

        [DataMember]
        public List<BankAccountModel> BankAccounts { get; set; }

        [DataMember]
        public bool IsFlatCommissionRate
        {get;set;}
    }

   

    public enum PaymentType
    {

        Bank = 1,
        Cash = 0
    }

    [DataContract]
    public class BusinessTypeModel
    {
        int businesstypekey;
        [DataMember]
        public int BusinessTypeKey
        {
            get{return businesstypekey;}
        }

        [DataMember]
        public string BusinessTypeName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class BankAccountModel
    {
        [DataMember]
        public string BSB { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string AccountName { get; set; }

        [DataMember]
        public int Weight { get; set; }

        int Accountkey;
        [DataMember]
        public int AccountKey { get { return AccountKey; } }
    }

    [DataContract]
    public class MemberAddressModel
    {
        [DataMember]
        public string StreetNumber { get; set; }
        [DataMember]
        public string StreetName { get; set; }

        [DataMember]
        public SuburbModel Suburb { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Email { get; set; }


    }

    [DataContract]
    public class CountryModel
    {
        [DataMember]
        public string CountryName {get;set;}

        int countrykey;
        [DataMember]
        public int CountryKey { get { return countrykey; } }
    }

    [DataContract]
    public class StateModel
    {
        [DataMember]
        public string StateName {get;set;}

        [DataMember]
        public CountryModel Country {get;set;}

        int statekey;
        [DataMember]
        public int StateKey { get{return statekey;} }
    }

    [DataContract]
    public class SuburbModel
    {
        [DataMember]
        public string SuburbName{get;set;}

        [DataMember]
        public PostCodeModel State {set;get;}

        int suburbkey;
        [DataMember]
        public int SuburbKey { get { return suburbkey; } }
    }

    [DataContract]
    public class PostCodeModel
    {
        [DataMember]
        public string PostCode{get;set;}

        [DataMember ]
        public StateModel State {get;set;}

        int postcodekey;
        [DataMember]
        public int PostCodeKey { get { return postcodekey; } }
    }

}