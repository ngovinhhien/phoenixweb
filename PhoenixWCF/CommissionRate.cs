//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhoenixWebWCF
{
    using System;
    using System.Collections.Generic;
    
    public partial class CommissionRate
    {
        public Nullable<decimal> GrossDealerCommissionRate { get; set; }
        public Nullable<decimal> OperatorCommissionRate { get; set; }
        public Nullable<decimal> ManualCommissionRate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> Current { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public int CommissionRate_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<int> DocketPayMethod_key { get; set; }
        public Nullable<bool> FixAmount { get; set; }
    
        public virtual Member Member { get; set; }
    }
}
