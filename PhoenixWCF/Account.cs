//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhoenixWebWCF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Account
    {
        public string Bsb { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public Nullable<float> Weight { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public int Account_key { get; set; }
        public Nullable<int> Member_key { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    
        public virtual Member Member { get; set; }
    }
}
