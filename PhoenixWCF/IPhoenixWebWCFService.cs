﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PhoenixWebWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPhoenixWebWCFService

    {


        [OperationContract]
        MemberModel AddNewMember(MemberModel newMember);

        [OperationContract]
        List<CountryModel> GetCountries();

        [OperationContract]
        List<StateModel> GetStates(CountryModel country);

        [OperationContract]
        List<PostCodeModel> GetPostCodes(StateModel state);

        [OperationContract]
        List<SuburbModel> GetSuburs(PostCodeModel state);
        // TODO: Add your service operations here
    }


    
}
