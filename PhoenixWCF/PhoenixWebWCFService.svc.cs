﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PhoenixWebWCF
{
    public class PhoenixWebWCFService : IPhoenixWebWCFService
    {

        public MemberModel AddNewMember(MemberModel newMember)
        {
            PhoenixWebWCF.TaxiEpayEntities entities = new TaxiEpayEntities();
            Member mem = new Member();
            string prefix = "";
            switch (mem.State)
	        {
                case "NSW":
                case "ACT":
                    prefix = "N";
                    break;
                case "VIC":
                    prefix = "V";
                    break;
                case "QLD":
                    prefix = "Q";
                    break;
                case "SA":
                    prefix = "S";
                    break;
                case "TAS":
                case "NT":
                    prefix = "T";
                    break;
                case "WA":
                    prefix = "W";
                    break;
		        default:
                break;
	        }
            mem.MemberId = prefix + mem.Member_key;
            entities.Members.Add(mem);
            newMember.MemberKey = mem.Member_key;

            return newMember;
            


        }

        public List<CountryModel> GetCountries() {
            List<CountryModel> countries = new List<CountryModel>(); return countries;
        }

        public List<StateModel> GetStates(CountryModel country) {
            List<StateModel> states = new List<StateModel>();
            return states;

        }

        public List<PostCodeModel> GetPostCodes(StateModel state) {
            List<PostCodeModel> postcodes = new List<PostCodeModel>();
            return postcodes;
        }

        public List<SuburbModel> GetSuburs(PostCodeModel state) { List<SuburbModel> suburbs = new List<SuburbModel>(); return suburbs; }
    }
}
