//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhoenixWebWCF
{
    using System;
    using System.Collections.Generic;
    
    public partial class MemberAddress
    {
        public int MemberAddressKey { get; set; }
        public Nullable<int> Member_key { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public Nullable<int> Suburb_key { get; set; }
        public Nullable<int> AddressTypeKey { get; set; }
    
        public virtual AddressType AddressType { get; set; }
        public virtual Member Member { get; set; }
        public virtual Suburb Suburb { get; set; }
    }
}
