//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    
    public partial class GetBINRangeData_Result
    {
        public string CardName { get; set; }
        public string PANHigh { get; set; }
        public string PANLow { get; set; }
        public string ServiceFeeMode { get; set; }
        public Nullable<decimal> ServiceFeeValue { get; set; }
        public string VersionNo { get; set; }
    }
}
