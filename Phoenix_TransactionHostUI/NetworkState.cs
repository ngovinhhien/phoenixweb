//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    using System.Collections.Generic;
    
    public partial class NetworkState
    {
        public int NetworkStateKey { get; set; }
        public int NetworkKey { get; set; }
        public int StateKey { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid CreatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public System.Guid UpdatedBy { get; set; }
        public Nullable<bool> Inactive { get; set; }
    
        public virtual Network Network { get; set; }
        public virtual State State { get; set; }
    }
}
