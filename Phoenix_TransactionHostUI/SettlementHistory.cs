//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    using System.Collections.Generic;
    
    public partial class SettlementHistory
    {
        public int SettlementHistoryKey { get; set; }
        public string BatchNumber { get; set; }
        public Nullable<System.DateTime> Logon { get; set; }
        public Nullable<System.DateTime> Logoff { get; set; }
        public System.Data.Entity.Spatial.DbGeography GPSPosition { get; set; }
        public string TerminalID { get; set; }
        public Nullable<int> TerminalKey { get; set; }
        public string MerchantID { get; set; }
        public Nullable<int> CreditNumber { get; set; }
        public Nullable<decimal> CreditAmount { get; set; }
        public Nullable<int> DebitNumber { get; set; }
        public Nullable<decimal> DebitAmount { get; set; }
        public Nullable<int> CreditReversalNumber { get; set; }
        public Nullable<decimal> CreditReversalAmount { get; set; }
        public Nullable<int> DebitReversalNumber { get; set; }
        public Nullable<decimal> DebitReversalAmount { get; set; }
        public Nullable<decimal> SettlementNetAmount { get; set; }
        public Nullable<bool> Loaded { get; set; }
        public Nullable<System.DateTime> LoadedDateTime { get; set; }
        public Nullable<int> DeclinedCreditNumber { get; set; }
        public Nullable<decimal> DeclinedCreditAmount { get; set; }
        public Nullable<int> DeclinedDebitNumber { get; set; }
        public Nullable<decimal> DeclinedDebitAmount { get; set; }
        public Nullable<int> DeclinedCreditReversalNumber { get; set; }
        public Nullable<decimal> DeclinedCreditReversalAmount { get; set; }
        public Nullable<int> DeclinedDebitReversalNumber { get; set; }
        public Nullable<decimal> DeclinedDebitReversalAmount { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public Nullable<byte> LoadType { get; set; }
        public string PaymentAppMerchantID { get; set; }
    
        public virtual Terminal Terminal { get; set; }
    }
}
