//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReceiptPicture
    {
        public int ReceiptPictureKey { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public string TerminalID { get; set; }
        public string TransactionNumber { get; set; }
        public string MerchantID { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string MobileNumberToSend { get; set; }
        public string FileType { get; set; }
        public string ReferenceNumber { get; set; }
        public System.Guid ID { get; set; }
        public string ReceiptID { get; set; }
    }
}
