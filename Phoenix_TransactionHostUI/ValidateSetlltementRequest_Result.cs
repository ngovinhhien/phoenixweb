//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    
    public partial class ValidateSetlltementRequest_Result
    {
        public Nullable<int> TransactionCount { get; set; }
        public Nullable<decimal> BatchTotal { get; set; }
    }
}
