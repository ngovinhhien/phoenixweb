//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    
    public partial class RPT_GetApprovedTransaction00And08Details_Result
    {
        public Nullable<int> RT { get; set; }
        public string responsecode { get; set; }
        public int Hrs { get; set; }
        public Nullable<int> Quantity { get; set; }
    }
}
