//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Phoenix_TransactionHostUI
{
    using System;
    using System.Collections.Generic;
    
    public partial class FileDownloadMessage
    {
        public int FileDownloadMessageKey { get; set; }
        public string MessageContent { get; set; }
        public Nullable<short> MessageNumber { get; set; }
        public Nullable<short> MessageLastNumber { get; set; }
        public int FileDownloadSplitKey { get; set; }
    
        public virtual FileDownloadSplit FileDownloadSplit { get; set; }
    }
}
