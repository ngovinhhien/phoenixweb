﻿DECLARE @err INT = 0
DECLARE @currentDateTime DATETIME = GETDATE()
DECLARE @updatedBy VARCHAR(50) = 'vunguyen'


PRINT 'Running script in aspnetdb'
USE aspnetdb
SET @err = 0
BEGIN TRAN

IF NOT EXISTS(SELECT TOP 1 1 FROM Permission WHERE PermissionName = 'ViewGlideBox')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   'ViewGlideBox', 
		'Allow user can view all functions of GlideBox'  
    )
	IF @@ERROR <> 0 SET @err = 1
END

IF NOT EXISTS(SELECT TOP 1 1 FROM Permission WHERE PermissionName = 'ManageGlideBox')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   'ManageGlideBox', 
		'Allow user can manage with handling as add/edit/delete of GlideBox feature'  
    )
	IF @@ERROR <> 0 SET @err = 1
END


IF EXISTS(SELECT TOP 1 1 FROM dbo.aspnet_Roles WHERE RoleName = 'Administrator')
BEGIN

DECLARE @Id INT
 
	SELECT @Id = PermissionId FROM dbo.Permission WHERE PermissionName = 'ViewGlideBox'

	INSERT INTO RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @Id, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @Id
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
	IF @@ERROR <> 0 SET @err = 1

	SELECT @Id = PermissionId FROM dbo.Permission WHERE PermissionName = 'ManageGlideBox'

	INSERT INTO RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @Id, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @Id
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
	IF @@ERROR <> 0 SET @err = 1
END


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in aspnetdb'


PRINT 'Running script in Taxiepay'
USE Taxiepay
SET @err = 0
BEGIN TRAN

--Insert new config MaxCategoryAssignedInTerminal to SystemConfiguration
IF NOT EXISTS(SELECT TOP 1 1 FROM SystemConfiguration  WHERE ConfigurationKey = 'MaxCategoryAssignedInTerminal')
BEGIN
	INSERT dbo.SystemConfiguration
(
    SystemID,
    ConfigurationKey,
    ConfigurationValue,
    Description,
    CreatedBy,
    CreatedDate,
    UpdatedBy,
    UpdatedDate
)
VALUES
(   2,         -- SystemID - int
    'MaxCategoryAssignedInTerminal',        -- ConfigurationKey - varchar(50)
    '4',        -- ConfigurationValue - varchar(100)
    'Max category is assigned to terminal',        -- Description - varchar(100)
    @updatedBy,        -- CreatedBy - varchar(50)
    @currentDateTime, -- CreatedDate - datetime
    @updatedBy,        -- UpdatedBy - varchar(50)
    @currentDateTime  -- UpdatedDate - datetime
    )
	IF @@ERROR <> 0 SET @err = 1
END

--Insert new config MaxTotalGlideboxCategoryCanCreate to SystemConfiguration
IF NOT EXISTS(SELECT TOP 1 1 FROM SystemConfiguration  WHERE ConfigurationKey = 'MaxTotalGlideboxCategoryCanCreate')
BEGIN
	INSERT dbo.SystemConfiguration
(
    SystemID,
    ConfigurationKey,
    ConfigurationValue,
    Description,
    CreatedBy,
    CreatedDate,
    UpdatedBy,
    UpdatedDate
)
VALUES
(   2,         -- SystemID - int
    'MaxTotalGlideboxCategoryCanCreate',        -- ConfigurationKey - varchar(50)
    '-1',        -- ConfigurationValue - varchar(100)
    'Maximum total glide box categoty can create',        -- Description - varchar(100)
    @updatedBy,        -- CreatedBy - varchar(50)
    @currentDateTime, -- CreatedDate - datetime
    @updatedBy,        -- UpdatedBy - varchar(50)
    @currentDateTime  -- UpdatedDate - datetime
    )
	IF @@ERROR <> 0 SET @err = 1
END


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in Taxiepay'