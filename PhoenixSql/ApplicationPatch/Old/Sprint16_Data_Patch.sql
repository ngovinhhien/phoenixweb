﻿USE aspnetdb
GO

--Insert ProcessMissingPayment permisson to "Operations" role
BEGIN TRAN
DECLARE @IsError INT  = 0
DECLARE @RolePermissionTable TABLE(RoleName VARCHAR(50) NOT NULL, PermissionName VARCHAR(50) NOT NULL)  
INSERT INTO @RolePermissionTable
VALUES ('Operations', 'ProcessMissingPayment')

INSERT INTO dbo.RolePermission(RoleName,PermissionId,RoleId)
SELECT r.RoleName, p.PermissionId, r.RoleId FROM @RolePermissionTable tmp
	JOIN dbo.aspnet_Roles r ON r.RoleName = tmp.RoleName
	JOIN dbo.Permission p ON p.PermissionName = tmp.PermissionName 
	LEFT JOIN dbo.RolePermission rp ON rp.RoleName = tmp.RoleName AND rp.PermissionId = p.PermissionId
WHERE rp.Id IS NULL
IF @@ERROR <> 0 SET @IsError = 1

--PHW-667 Adding Refund limits to Terminal Configuration page in PhoenixWeb
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.Permission WHERE PermissionName = 'VAATerminalConfigRefund')
BEGIN 
	INSERT INTO dbo.Permission
	SELECT 'VAATerminalConfigRefund', 'VAATerminal - View Config Refund'
	IF @@ERROR <> 0 SET @IsError = 1
END

IF @IsError <> 0
	ROLLBACK TRAN 
ELSE 
	COMMIT TRAN