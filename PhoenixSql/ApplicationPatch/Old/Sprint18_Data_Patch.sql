﻿---For TaxiEpay
PRINT 'Running script in TaxiEpay'
USE TaxiEpay

DECLARE @err INT = 0
DECLARE @currentDateTime DATETIME = GETDATE()
DECLARE @updatedBy VARCHAR(50) = 'vunguyen'
BEGIN TRAN

INSERT [dbo].[ExchangeRateType]
(
    [ExchangeRateTypeCode],
    [ExchangeRateTypeName],
    [IsActive],
    [CreatedBy],
    [UpdatedBy],
    [CreatedDate],
    [UpdatedDate]
)
VALUES
(N'DEFAULT', N'Default', 1, @updatedBy, @updatedBy, @currentDateTime, @currentDateTime);
IF @@ERROR <> 0 SET @err = 1;

INSERT [dbo].[ExchangeRateType]
(
    [ExchangeRateTypeCode],
    [ExchangeRateTypeName],
    [IsActive],
    [CreatedBy],
    [UpdatedBy],
    [CreatedDate],
    [UpdatedDate]
)
VALUES
(N'GROUP', N'Group', 1, @updatedBy, @updatedBy, @currentDateTime, @currentDateTime);
IF @@ERROR <> 0 SET @err = 1;

INSERT [dbo].[ExchangeRateType]
(
    [ExchangeRateTypeCode],
    [ExchangeRateTypeName],
    [IsActive],
    [CreatedBy],
    [UpdatedBy],
    [CreatedDate],
    [UpdatedDate]
)
VALUES
(N'NEW_MERCHANT', N'New Customer', 1, @updatedBy, @updatedBy, @currentDateTime,
@currentDateTime);
IF @@ERROR <> 0 SET @err = 1;

INSERT [dbo].[ExchangeRateType]
(
    [ExchangeRateTypeCode],
    [ExchangeRateTypeName],
    [IsActive],
    [CreatedBy],
    [UpdatedBy],
    [CreatedDate],
    [UpdatedDate]
)
VALUES
(N'PERIOD', N'Period', 1, @updatedBy, @updatedBy, @currentDateTime,@currentDateTime);
IF @@ERROR <> 0 SET @err = 1;


UPDATE QBRExchangeRate
SET Name = 'Default', ExchangeRateTypeCode = 'DEFAULT', UpdatedDate = @currentDateTime, UpdatedBy = @updatedBy
WHERE ID = 1
IF @@ERROR <> 0 SET @err = 1;

--1.Update IsDefault and CompanyKey for LoyaltyPromotion
--2. Insert new data for LiveSMS
IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.LoyaltyPromotion WHERE PromotionName = 'LEB Standard Points')
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'LEB Standard Points', -- PromotionName - varchar(100)
        4,                     -- OrganisationforLoyaltyID - int
        1,                     -- IsDefault - bit
        22,                    -- CompanyKey - int
        @updatedBy,            -- CreatedBy - varchar(50)
        @currentDateTime,      -- CreatedDateTime - datetime
        @updatedBy,            -- UpdatedBy - varchar(50)
        @currentDateTime       -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END
ELSE
BEGIN
    UPDATE dbo.LoyaltyPromotion
    SET IsDefault = 1,
        CompanyKey = 22,
		UpdatedBy = @updatedBy,
		UpdatedDateTime = @currentDateTime
    WHERE PromotionName = 'LEB Standard Points';
    IF @@ERROR <> 0
        SET @err = 1;
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.LoyaltyPromotion WHERE PromotionName = 'Live sms Standard Points')
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'Live sms Standard Points', -- PromotionName - varchar(100)
        4,                          -- OrganisationforLoyaltyID - int
        1,                          -- IsDefault - bit
        28,                         -- CompanyKey - int
        @updatedBy,                 -- CreatedBy - varchar(50)
        @currentDateTime,           -- CreatedDateTime - datetime
        @updatedBy,                 -- UpdatedBy - varchar(50)
        @currentDateTime            -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.LoyaltyPromotion WHERE PromotionName = 'Live eftpos Standard Points')
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'Live eftpos Standard Points', -- PromotionName - varchar(100)
        4,                             -- OrganisationforLoyaltyID - int
        1,                             -- IsDefault - bit
        18,                            -- CompanyKey - int
        @updatedBy,                    -- CreatedBy - varchar(50)
        @currentDateTime,              -- CreatedDateTime - datetime
        @updatedBy,                    -- UpdatedBy - varchar(50)
        @currentDateTime               -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END;


IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.LoyaltyPromotion WHERE PromotionName = 'Live eftpos black for taxi Standard Points')
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'Live eftpos black for taxi Standard Points', -- PromotionName - varchar(100)
        4,                                            -- OrganisationforLoyaltyID - int
        1,                                            -- IsDefault - bit
        23,                                           -- CompanyKey - int
        @updatedBy,                                   -- CreatedBy - varchar(50)
        @currentDateTime,                             -- CreatedDateTime - datetime
        @updatedBy,                                   -- UpdatedBy - varchar(50)
        @currentDateTime                              -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM dbo.LoyaltyPromotion
    WHERE PromotionName = 'Live eftpos integrated Standard Points'
)
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'Live eftpos integrated Standard Points', -- PromotionName - varchar(100)
        4,                                        -- OrganisationforLoyaltyID - int
        1,                                        -- IsDefault - bit
        26,                                       -- CompanyKey - int
        @updatedBy,                               -- CreatedBy - varchar(50)
        @currentDateTime,                         -- CreatedDateTime - datetime
        @updatedBy,                               -- UpdatedBy - varchar(50)
        @currentDateTime                          -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.LoyaltyPromotion WHERE PromotionName = 'live eftpos black integrated Standard Points')
BEGIN
    INSERT INTO dbo.LoyaltyPromotion
    (
        PromotionName,
        OrganisationforLoyaltyID,
        IsDefault,
        CompanyKey,
        CreatedBy,
        CreatedDateTime,
        UpdatedBy,
        UpdatedDateTime
    )
    VALUES
    (   'live eftpos black integrated Standard Points', -- PromotionName - varchar(100)
        4,                                              -- OrganisationforLoyaltyID - int
        1,                                              -- IsDefault - bit
        27,                                             -- CompanyKey - int
        @updatedBy,                                     -- CreatedBy - varchar(50)
        @currentDateTime,                               -- CreatedDateTime - datetime
        @updatedBy,                                     -- UpdatedBy - varchar(50)
        @currentDateTime                                -- UpdatedDateTime - datetime
        );
    IF @@ERROR <> 0 SET @err = 1;
END;

--3. Insert MemberReferenceEntryStatusCode
IF NOT EXISTS (SELECT TOP 1 1 FROM MemberReferenceEntryStatusCode WHERE StatusCode = 'MNG_REJECTED')
BEGIN
	INSERT INTO dbo.MemberReferenceEntryStatusCode (StatusCode, StatusName, Description, IsActive, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
	VALUES('MNG_REJECTED','Manager Rejected','Manager Rejected',1, @updatedBy, @currentDateTime, @updatedBy, @currentDateTime)
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM MemberReferenceEntryStatusCode WHERE StatusCode = 'MNG_APPROVAL')
BEGIN
	INSERT INTO dbo.MemberReferenceEntryStatusCode (StatusCode, StatusName, Description, IsActive, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
	VALUES('MNG_APPROVAL','Manager approval','Manager approval',1, @updatedBy, @currentDateTime, @updatedBy, @currentDateTime)
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM MemberReferenceEntryStatusCode WHERE StatusCode = 'MNG_APPROVAL_REJECTED_RECORDS')
BEGIN
	INSERT INTO dbo.MemberReferenceEntryStatusCode (StatusCode, StatusName, Description, IsActive, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
	VALUES('MNG_APPROVAL_REJECTED_RECORDS','Sale/Fin Rejected','Sale/Fin Rejected',1, @updatedBy, @currentDateTime, @updatedBy, @currentDateTime)
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM MemberReferenceEntryStatusCode WHERE StatusCode = 'SF_REJECTED')
BEGIN
	INSERT INTO dbo.MemberReferenceEntryStatusCode (StatusCode, StatusName, Description, IsActive, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
	VALUES('SF_REJECTED','Sale/Fin Rejected','Sale/Fin Rejected',1,@updatedBy, @currentDateTime, @updatedBy, @currentDateTime)
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM MemberReferenceEntryStatusCode WHERE StatusCode = 'SF_APPROVAL')
BEGIN
	INSERT INTO dbo.MemberReferenceEntryStatusCode (StatusCode, StatusName, Description, IsActive, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
	VALUES('SF_APPROVAL','Sale/Fin Approval','Sale/Fin Approval',1,@updatedBy, @currentDateTime, @updatedBy, @currentDateTime)
	IF @@ERROR <> 0 SET @err = 1
END

--4. Migration qbr number from MemberExtension to MemberReference
DECLARE @QbrNumberTable TABLE(memberKey int)

INSERT INTO @QbrNumberTable(memberKey)
SELECT Member_key
FROM dbo.MemberExtension
WHERE ISNULL(QantasNumber,'') <> ''

--Using for testing purpose
--SELECT * FROM @QbrNumberTable

DECLARE @memberKey INT,
		@qbrNumber VARCHAR(50)

DECLARE qbrnumber_cursor CURSOR FOR 
SELECT memberKey
FROM @QbrNumberTable


OPEN qbrnumber_cursor  
FETCH NEXT FROM qbrnumber_cursor INTO @memberKey

WHILE @@FETCH_STATUS = 0  
BEGIN  
	UPDATE dbo.MemberExtension
	SET QantasNumber = QantasNumber
	WHERE Member_key = @memberKey
	IF @@ERROR <> 0 SET @err = 1

    FETCH NEXT FROM qbrnumber_cursor INTO @memberKey
END 

CLOSE qbrnumber_cursor  
DEALLOCATE qbrnumber_cursor 


IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'URLLoginPageForLiveSMS')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'URLLoginPageForLiveSMS', 'href=https://portaltest.livesms.com.au/auth/login', 'This is live sms login page for test env', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
END

IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'SparkPostRemoveDefaultPaymentMethod')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'SparkPostRemoveDefaultPaymentMethod', 'live-sms-remove-default-payment-method', 'This is sparkpost remove default payment method', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
END

IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'MarketoNoteWhenMerchantNotVerifyTheirPhoneNumber')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'MarketoNoteWhenMerchantNotVerifyTheirPhoneNumber', 'Sign up incomplete. Awaiting phone number verification', 'This is a message from Marketo to identify merchant is verify phone or not', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
END


PRINT 'LV-549-Insert configuaration for verification phone'
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioAuthyAPIKey')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioAuthyAPIKey', 'bnE3Os769HC8xFr2pgDkDkiHs80oVFtn', 'This is Authencation key of twilio', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioViaType')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioViaType', 'sms', 'This is via type for verificatipn', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioCountryCode')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioCountryCode', '61', 'This is country code for send sms verification', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioCodeLength')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioCodeLength', '6', 'This is length of verification code', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 

IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'EnableTwilioVerification')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'EnableTwilioVerification', 'true', 'This is config for check using twilio verification', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioSendCodeUri')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioSendCodeUri', 'https://api.authy.com/protected/json/phones/verification/start', 'This is url for excute send verify', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'TwilioCheckCodeUri')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'TwilioCheckCodeUri', 'https://api.authy.com/protected/json/phones/verification/check', 'This is url for excute check verification', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 

IF NOT EXISTS(SELECT TOP 1 1 FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'SparkPostDeactivateDefaultPaymentMethod')
BEGIN
	INSERT INTO SystemConfiguration
	SELECT 1, 'SparkPostDeactivateDefaultPaymentMethod', 'live-sms-deactivate-default-payment-method', 'This is spark post template for deactivating default payment method', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
	IF @@ERROR <> 0 SET @err = 1
END 

UPDATE dbo.SystemConfiguration
SET ConfigurationValue = 'https://sandbox.livegroup.com.au/LivegroupAPI/api/qantas/ValidateQBRNumber', UpdatedBy = @updatedBy, UpdatedDate = @currentDateTime
WHERE ConfigurationKey = 'QantasValidateQBRURL'
IF @@ERROR <> 0 SET @err = 1


IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.QantasExportFileStatus WHERE Name = 'Waiting for send to Qantas')
BEGIN
	INSERT INTO dbo.QantasExportFileStatus
	(
	    Name,
	    NextStatus,
	    CreatedBy,
	    CreatedOn,
	    UpdatedBy,
	    UpdatedOn,
	    IsActive,
	    IsDeleted
	)
	VALUES
	(   'Waiting for send to Qantas',        -- Name - varchar(255)
	    NULL,         -- NextStatus - int
	    @updatedBy,        -- CreatedBy - varchar(255)
	    @currentDateTime, -- CreatedOn - datetime
	    @updatedBy,        -- UpdatedBy - varchar(255)
	    @currentDateTime, -- UpdatedOn - datetime
	    1,      -- IsActive - bit
	    0       -- IsDeleted - bit
	    )
IF @@ERROR <> 0 SET @err = 1

END

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.QantasExportFileEntryStatus WHERE Name = 'Waiting for send to Qantas')
BEGIN
	INSERT INTO dbo.QantasExportFileEntryStatus
	(
	    Name,
	    NextStatus,
	    CreatedBy,
	    CreatedOn,
	    UpdatedBy,
	    UpdatedOn,
	    IsActive,
	    IsDeleted
	)
	VALUES
	(   'Waiting for send to Qantas',        -- Name - varchar(255)
	    NULL,         -- NextStatus - int
	    @updatedBy,        -- CreatedBy - varchar(255)
	    @currentDateTime, -- CreatedOn - datetime
	    @updatedBy,        -- UpdatedBy - varchar(255)
	    @currentDateTime, -- UpdatedOn - datetime
	    1,      -- IsActive - bit
	    0       -- IsDeleted - bit
	    )
IF @@ERROR <> 0 SET @err = 1

END

IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in TaxiEpay'


---For another database
PRINT 'Running script in TransactionHostLog'
USE TransactionHostLog
SET @err = 0
BEGIN TRAN

IF NOT EXISTS (SELECT TOP 1 1 FROM LogLevel WHERE name = 'ERROR')
BEGIN
	INSERT INTO LogLevel ([Name]) 
	VALUES ('ERROR')
	IF @@ERROR <> 0 SET @err = 1
END

IF NOT EXISTS (SELECT TOP 1 1 FROM LogLevel WHERE name = 'FATAL')
BEGIN
	INSERT INTO LogLevel ([Name]) 
	VALUES ('FATAL')
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM LogLevel WHERE name = 'INFO')
BEGIN
	INSERT INTO LogLevel ([Name]) 
	VALUES ('INFO')
	IF @@ERROR <> 0 SET @err = 1
END
IF NOT EXISTS (SELECT TOP 1 1 FROM LogLevel WHERE name = 'WARN')
BEGIN
	INSERT INTO LogLevel ([Name]) 
	VALUES ('WARN')
	IF @@ERROR <> 0 SET @err = 1
END


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in TransactionHostLog'


PRINT 'Running script in AspnetDb'
USE AspnetDb
SET @err = 0
BEGIN TRAN


PRINT 'This is permission for Reset Config cache in Administrator > Add Roles'
INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'AdminResetConfigCache', -- PermissionName - varchar(50)
    'Allow user can force system reload config cache'  -- PermissionDescription - varchar(100)
    )
IF @@ERROR <> 0 SET @err = 1


PRINT 'This is permission for Reset Config cache in Member > UnallocateDockets'
INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'UnallocateDockets', -- PermissionName - varchar(50)
    'Allow user can using unallocatate docket'  -- PermissionDescription - varchar(100)
    )
IF @@ERROR <> 0 SET @err = 1

PRINT 'This is permission for Reset Config cache in Transaction > CloseBatches'
INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'CloseBatches', -- PermissionName - varchar(50)
    'Close Batches'  -- PermissionDescription - varchar(100)
    )
IF @@ERROR <> 0 SET @err = 1

PRINT 'This is permission for Reset Config cache in Transaction > ImportTerminal'
INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'ImportTerminal', -- PermissionName - varchar(50)
    'Allow user can using Import Terminal function'  -- PermissionDescription - varchar(100)
    )
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'QantasProcessing', -- PermissionName - varchar(50)
    'Allow user can using qantas menu function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'QantasReviewingForSaleFin', -- PermissionName - varchar(50)
    'Allow user can using qantas reviewing for sale/fin function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'QantasReviewingForManager', -- PermissionName - varchar(50)
    'Allow user can using qantas reviewing for manager function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'QantasPointUpload', -- PermissionName - varchar(50)
    'Allow user can using qantas point file upload function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'QantasPointFileReviewing', -- PermissionName - varchar(50)
    'Allow user can using qantas point file reviewing function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.Permission
(
    PermissionName,
    PermissionDescription
)
VALUES
(   'Livesms', -- PermissionName - varchar(50)
    'Allow user can using Livesms function'  -- PermissionDescription - varchar(100)
)
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT aspnet_Roles.RoleName, Permission.PermissionId,aspnet_Roles.RoleId
FROM dbo.Permission
INNER JOIN dbo.aspnet_Roles ON RoleName = 'Administrator'
LEFT JOIN dbo.RolePermission ON RolePermission.PermissionId = Permission.PermissionId AND RolePermission.RoleId = aspnet_Roles.RoleId
WHERE PermissionName IN ('QantasProcessing','QantasReviewingForSaleFin','QantasReviewingForManager','QantasPointUpload','QantasPointFileReviewing','Livesms')  AND RolePermission.PermissionId IS NULL
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT aspnet_Roles.RoleName, Permission.PermissionId,aspnet_Roles.RoleId
FROM dbo.Permission
INNER JOIN dbo.aspnet_Roles ON RoleName = 'Administrator'
LEFT JOIN dbo.RolePermission ON RolePermission.PermissionId = Permission.PermissionId AND RolePermission.RoleId = aspnet_Roles.RoleId
WHERE PermissionName = 'AdminResetConfigCache'  AND RolePermission.PermissionId IS NULL
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT aspnet_Roles.RoleName, Permission.PermissionId,aspnet_Roles.RoleId
FROM dbo.Permission
INNER JOIN dbo.aspnet_Roles ON RoleName IN ( 'Administrator','FrontCounterAdmin','Operations')
LEFT JOIN dbo.RolePermission ON RolePermission.PermissionId = Permission.PermissionId AND RolePermission.RoleId = aspnet_Roles.RoleId
WHERE PermissionName = 'UnallocateDockets'  AND RolePermission.PermissionId IS NULL
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT aspnet_Roles.RoleName, Permission.PermissionId,aspnet_Roles.RoleId
FROM dbo.Permission
INNER JOIN dbo.aspnet_Roles ON RoleName IN ( 'Administrator','Operations')
LEFT JOIN dbo.RolePermission ON RolePermission.PermissionId = Permission.PermissionId AND RolePermission.RoleId = aspnet_Roles.RoleId
WHERE PermissionName = 'CloseBatches' AND RolePermission.PermissionId IS NULL
IF @@ERROR <> 0 SET @err = 1

INSERT INTO dbo.RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT aspnet_Roles.RoleName, Permission.PermissionId,aspnet_Roles.RoleId
FROM dbo.Permission
INNER JOIN dbo.aspnet_Roles ON aspnet_Roles.RoleName IN ( 'Administrator','Operations')
LEFT JOIN dbo.RolePermission ON RolePermission.PermissionId = Permission.PermissionId AND RolePermission.RoleId = aspnet_Roles.RoleId
WHERE PermissionName = 'ImportTerminal' AND RolePermission.PermissionId IS NULL
IF @@ERROR <> 0 SET @err = 1

IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in AspnetDb'