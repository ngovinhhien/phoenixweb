﻿DECLARE @err INT = 0
DECLARE @currentDateTime DATETIME = GETDATE()
DECLARE @updatedBy VARCHAR(50) = 'vunguyen'


PRINT 'Running script in AspnetDb'
USE AspnetDb
SET @err = 0
BEGIN TRAN

--Insert permision GhostMyWebUser
IF NOT EXISTS(SELECT TOP 1 1 FROM Permission WHERE PermissionName = 'GhostMyWebUser')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   'GhostMyWebUser', -- PermissionName - varchar(50)
		'Allow user can ghost myweb account'  -- PermissionDescription - varchar(100)
    )
	IF @@ERROR <> 0 SET @err = 1
END


-- Assign permision to role Administrator
IF EXISTS(SELECT TOP 1 1 FROM dbo.aspnet_Roles WHERE RoleName = 'Administrator')
BEGIN

DECLARE @ghostPerId INT 
SELECT @ghostPerId = PermissionId FROM dbo.Permission WHERE PermissionName = 'GhostMyWebUser'

	INSERT INTO RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @ghostPerId, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @ghostPerId
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
	IF @@ERROR <> 0 SET @err = 1

END


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in AspnetDb'