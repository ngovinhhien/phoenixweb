﻿

---For TaxiEpay
PRINT 'Running script in TaxiEpay'
USE TaxiEpay

DECLARE @err INT = 0
DECLARE @currentDateTime DATETIME = GETDATE()
DECLARE @updatedBy VARCHAR(50) = 'vunguyen'

BEGIN TRAN

INSERT INTO dbo.CadmusinCardType
(
    CardTypeKey,
    IsActive,
    CreatedBy,
    CreatedDate,
    ModifiedBy,
    ModifiedDate
)
SELECT item, 1, @updatedBy, @currentDateTime, @updatedBy, @currentDateTime
FROM dbo.Split('Visa,MasterCard,Diners,Eftpos,AMEX,debit,UnionPay,Zip,Qantas', ',')
LEFT JOIN dbo.CadmusinCardType ON CardTypeKey = Item
WHERE CardTypeKey IS NULL
IF @@ERROR <> 0 SET @err = 1;


--Migration script for update current MemberReference number
SELECT DISTINCT  m.MemberReferenceNumber, m.member_key, m.CreatedByUser, m.CreatedDate 
FROM dbo.MemberReference m
LEFT JOIN dbo.MemberReferenceAudit ma ON ma.member_key = m.member_key 
	AND ma.MemberReferenceNumber = m.MemberReferenceNumber
WHERE MemberReferenceAuditKey IS NULL AND REPLACE(ISNULL(m.MemberReferenceNumber, ''),'	','') <> '' 

INSERT INTO dbo.MemberReferenceAudit(MemberReferenceNumber, member_key, CreatedByUser,CreatedDate)
SELECT DISTINCT  m.MemberReferenceNumber, m.member_key, m.CreatedByUser, m.CreatedDate 
FROM dbo.MemberReference m
LEFT JOIN dbo.MemberReferenceAudit ma ON ma.member_key = m.member_key 
	AND ma.MemberReferenceNumber = m.MemberReferenceNumber
WHERE MemberReferenceAuditKey IS NULL AND REPLACE(ISNULL(m.MemberReferenceNumber, ''),'	','') <> '' 
IF @@ERROR <> 0 SET @err = 1

SELECT DISTINCT  m.MemberReferenceNumber, m.member_key, m.CreatedByUser, m.CreatedDate 
FROM dbo.MemberReference m
LEFT JOIN dbo.MemberReferenceAudit ma ON ma.member_key = m.member_key 
	AND ma.MemberReferenceNumber = m.MemberReferenceNumber
WHERE MemberReferenceAuditKey IS NULL AND REPLACE(ISNULL(m.MemberReferenceNumber, ''),'	','') <> ''
IF @@ROWCOUNT > 0 SET @err = 1 


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in TaxiEpay'


---For TransactionHost
PRINT 'Running script in TransactionHost'
USE TransactionHost
BEGIN TRAN
SELECT 'Get the list terminal with enable moto refund is enable.'
SELECT *
FROM dbo.Terminal
WHERE EnableMOTORefund = 1

UPDATE dbo.Terminal
SET EnableMOTORefund = 0, UpdatedDate = @currentDateTime, UpdatedByUser = @updatedBy
WHERE EnableMOTORefund = 1
IF @@ERROR <> 0 SET @err = 1;

SELECT *
FROM dbo.Terminal
WHERE EnableMOTORefund = 1
IF @@ROWCOUNT > 0 SET @err = 1

IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in TransactionHost'

PRINT 'Running script in AspnetDb'
USE AspnetDb
SET @err = 0
BEGIN TRAN

--Insert permision EditVehicle
IF NOT EXISTS(SELECT TOP 1 1 FROM Permission WHERE PermissionName = 'EditVehicle')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   'EditVehicle', -- PermissionName - varchar(50)
		'Allow user can edit Vehicle'  -- PermissionDescription - varchar(100)
    )
	IF @@ERROR <> 0 SET @err = 1
END

--Insert permision EditEffectiveDate
IF NOT EXISTS(SELECT TOP 1 1 FROM Permission WHERE PermissionName = 'EditEffectiveDate')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   'EditEffectiveDate', -- PermissionName - varchar(50)
		'Allow user can changes Effective Date'  -- PermissionDescription - varchar(100)
    )
	IF @@ERROR <> 0 SET @err = 1
END

-- Assign permision to role Administrator
IF EXISTS(SELECT TOP 1 1 FROM dbo.aspnet_Roles WHERE RoleName = 'Administrator')
BEGIN

DECLARE @EditEffectiveDateId INT 
DECLARE @EditVehicleId INT 
SELECT @EditEffectiveDateId = PermissionId FROM dbo.Permission WHERE PermissionName = 'EditEffectiveDate'
SELECT @EditVehicleId = PermissionId FROM dbo.Permission WHERE PermissionName = 'EditVehicle'

	INSERT INTO RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @EditVehicleId, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @EditVehicleId
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
	IF @@ERROR <> 0 SET @err = 1

	INSERT INTO RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @EditEffectiveDateId, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @EditEffectiveDateId
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
	IF @@ERROR <> 0 SET @err = 1
END


IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in AspnetDb'