﻿DECLARE @err INT = 0;
DECLARE @currentDateTime DATETIME = GETDATE();
DECLARE @updatedBy VARCHAR(50) = 'vunguyen';

PRINT 'Running script in aspnetdb';
USE aspnetdb;
SET @err = 0;
BEGIN TRAN;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'ViewFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('ViewFees', 'Allow user can view all fees and history of all fees');
    IF @@ERROR <> 0
        SET @err = 1;
END;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'EditFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('EditFees', 'Allow user can edit/add all fees');
    IF @@ERROR <> 0
        SET @err = 1;
END;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'RefundFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('RefundFees', 'Allow user can refund fees');
END;

SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'ViewFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ( 'Administrator', 'Finance Admin', 'Operations' );
IF @@ERROR <> 0
    SET @err = 1;

SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'EditFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ( 'Administrator', 'Finance Admin','Operations Admin' );
IF @@ERROR <> 0
    SET @err = 1;

SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'RefundFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ( 'Administrator', 'Finance Admin' );
IF @@ERROR <> 0
    SET @err = 1;



IF @err = 1
    ROLLBACK TRAN;
ELSE
    COMMIT TRAN;
PRINT 'End script in aspnetdb';


PRINT 'Running script in Taxiepay';
USE Taxiepay;
SET @err = 0;
BEGIN TRAN;

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.FrequencyType WHERE Name = 'Monthly')
BEGIN
	INSERT INTO dbo.FrequencyType
	(
	    Name,
	    CreatedBy,
	    CreatedDate,
	    UpdatedBy,
	    UpdatedDate
	)
	VALUES
	(   'Monthly',        -- Name - varchar(120)
	    'vunguyen',        -- CreatedBy - varchar(120)
	    GETDATE(), -- CreatedDate - datetime
	    'vunguyen',        -- UpdatedBy - varchar(120)
	    GETDATE()  -- UpdatedDate - datetime
	    )

END

IF NOT EXISTS (SELECT TOP 1 1 FROM Fee WHERE Name = 'Account Fee')
BEGIN
	INSERT INTO dbo.Fee
	(
	    Name,
	    Description,
	    MinAmount,
	    MaxAmount,
	    FlatFee,
	    CreatedDate,
	    CreatedBy,
	    UpdatedDate,
	    IsActive
	)
	VALUES
	(   'Account Fee',        -- Name - varchar(120)
	    'Account Fee',        -- Description - varchar(120)
	    0.01,      -- MinAmount - decimal(18, 2)
	    99.99,      -- MaxAmount - decimal(18, 2)
	    0,      -- FlatFee - bit
	    GETDATE(), -- CreatedDate - datetime
	    'vunguyen',        -- CreatedBy - varchar(120)
	    GETDATE(), -- UpdatedDate - datetime
	    1       -- IsActive - bit
	    )

END

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'SUCCESS')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'SUCCESS', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 10, N'Success');
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'PENDING')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'PENDING', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 20, N'Pending');
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'FAILED')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'FAILED', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 30, N'Failed');
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'PENDING_REFUND')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'PENDING_REFUND', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 40, N'Pending refund');
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'REFUNDED')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'REFUNDED', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 50, N'Refunded');
END;

IF NOT EXISTS (SELECT TOP 1 1 FROM Status WHERE Name = 'FAILED_REFUND')
BEGIN

    INSERT [dbo].[Status]
    (
        [Name],
        [CreatedBy],
        [CreatedDate],
        [UpdatedBy],
        [UpdatedDate],
        [ListOrder],
        [Description]
    )
    VALUES
    (N'FAILED_REFUND', @updatedBy, @currentDateTime, @updatedBy, @currentDateTime, 60, N'Failed refund');
END;


--Insert new config MaxCategoryAssignedInTerminal to SystemConfiguration
IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM SystemConfiguration
    WHERE ConfigurationKey = 'MaxCategoryAssignedInTerminal'
)
BEGIN
    INSERT dbo.SystemConfiguration
    (
        SystemID,
        ConfigurationKey,
        ConfigurationValue,
        Description,
        CreatedBy,
        CreatedDate,
        UpdatedBy,
        UpdatedDate
    )
    VALUES
    (   2,                                      -- SystemID - int
        'MaxCategoryAssignedInTerminal',        -- ConfigurationKey - varchar(50)
        '4',                                    -- ConfigurationValue - varchar(100)
        'Max category is assigned to terminal', -- Description - varchar(100)
        @updatedBy,                             -- CreatedBy - varchar(50)
        @currentDateTime,                       -- CreatedDate - datetime
        @updatedBy,                             -- UpdatedBy - varchar(50)
        @currentDateTime                        -- UpdatedDate - datetime
        );
    IF @@ERROR <> 0
        SET @err = 1;
END;
IF @err = 1
    ROLLBACK TRAN;
ELSE
    COMMIT TRAN;
PRINT 'End script in AspnetDb';