﻿DECLARE @err INT = 0
DECLARE @currentDateTime DATETIME = GETDATE()
DECLARE @updatedBy VARCHAR(50) = 'vunguyen'


PRINT 'Running script in Taxiepay'
USE Taxiepay
SET @err = 0
BEGIN TRAN

--Insert new config MaxCategoryAssignedInTerminal to SystemConfiguration
IF NOT EXISTS(SELECT TOP 1 1 FROM SystemConfiguration  WHERE ConfigurationKey = 'MaxCategoryAssignedInTerminal')
BEGIN
	INSERT dbo.SystemConfiguration
(
    SystemID,
    ConfigurationKey,
    ConfigurationValue,
    Description,
    CreatedBy,
    CreatedDate,
    UpdatedBy,
    UpdatedDate
)
VALUES
(   2,         -- SystemID - int
    'MaxCategoryAssignedInTerminal',        -- ConfigurationKey - varchar(50)
    '4',        -- ConfigurationValue - varchar(100)
    'Max category is assigned to terminal',        -- Description - varchar(100)
    @updatedBy,        -- CreatedBy - varchar(50)
    @currentDateTime, -- CreatedDate - datetime
    @updatedBy,        -- UpdatedBy - varchar(50)
    @currentDateTime  -- UpdatedDate - datetime
    )
	IF @@ERROR <> 0 SET @err = 1
END



IF @err = 1
	ROLLBACK TRAN
ELSE 
	COMMIT TRAN
PRINT 'End script in Taxiepay'