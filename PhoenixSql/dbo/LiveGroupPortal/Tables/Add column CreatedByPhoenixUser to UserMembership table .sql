﻿IF COL_LENGTH('[LivegroupPortal].[dbo].[UserMembership]', 'CreatedByPhoenixUser') IS NULL
BEGIN
    ALTER TABLE [LivegroupPortal].[dbo].[UserMembership]
	ADD CreatedByPhoenixUser varchar(50);
END