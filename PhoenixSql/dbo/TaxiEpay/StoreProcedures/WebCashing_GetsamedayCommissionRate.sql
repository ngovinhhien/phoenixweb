﻿ALTER PROCEDURE [dbo].[WebCashing_GetsamedayCommissionRate] @Memberkey int = null
AS
BEGIN


select distinct m.member_key, m.memberid, cr.DocketPayMethod_key, cr.BusinessType, cr.OperatorCommissionRate, cr.ManualCommissionRate, cr.Name, cr.vehicletype_key, cr.MerchantId, cr.CommissionRate_key, 
isnull(pf.flatamount,0) as FlatAmount, -pf.Feeamount as HandlingAmount, -coalesce(pf.feeRate,cr.farehandlingrate,0) as HandlingRate   from member m
inner join MemberCommissionRate cr on cr.Member_key = m.Member_key and m.Company_key = cr.Company_key
left join state s on s.StateName = m.State
left join paymentfee pf on pf.docketpaymethod_key = cr.DocketPayMethod_key and pf.state_key = s.State_key and pf.transactiontype_key = 1 --only for cashing over the counter
where m.Member_key = @Memberkey and cr.MerchantID in ( '23166226', '23166200')
and  isnull(issameday,0)=1 AND cr.name IN ('Alipay', 'AMEX', 'Debit', 'Diners', 'MasterCard', 'UnionPay', 'Visa', 'Zip')
order by vehicletype_key

END