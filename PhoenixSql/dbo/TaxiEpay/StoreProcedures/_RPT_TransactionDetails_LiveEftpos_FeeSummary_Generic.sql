﻿ALTER PROC [dbo].[_RPT_TransactionDetails_LiveEftpos_FeeSummary_Generic] @MemberKey int, @startdate datetime, @enddate datetime -- @TranMonth int,@TranYear int
AS


--declare @startdate datetime
--declare @enddate datetime

--select @startdate = FirstDateOfMonth, @enddate = FirstDateOfNextMonth from FirstDateOfMonth where MonthPart = @TranMonth and YearPart = @TranYear

--select Sum(isnull(td.amount,0)) as TotalAmount, 'Other Fees' as FeeGroup,
--'Terminal Rental TID: ' + cast(tal.TerminalId as varchar) +'  ' + (cast(trh.rentMonth as varchar) +'/'+ cast(trh.RentYear as varchar)) as descriptions,
--trh.rentMonth, trh.RentYear
--from  [transaction] t 
--inner join transactiondetail td on t.transaction_key = td.transaction_key
--inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = td.Docket_key
--inner JOIN vTerminalAllocations Tal
--ON  Tal.TerminalAllocation_key = trh.TerminalAllocation_key
--where t.member_key = @MemberKey and td.TransactionDetailType = 9
----and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
----AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
--and t.faileddate is null  and t.TransactionDate  between @startdate and @enddate
--group by trh.TerminalAllocation_key,tal.TerminalId , trh.rentMonth, trh.RentYear




--select Sum(isnull(-r.RentAmount,0)) as TotalAmount, 'Other Fees' as FeeGroup,
--'Terminal Rental TID: ' + cast(r.TerminalId as varchar) +'  ' + (cast(r.tranmonth as varchar) +'/'+ cast(r.tranyear as varchar)) as descriptions,
--r.tranmonth rentMonth, r.tranyear RentYear
--from  TerminalRentalCalculationLE r
--where 
--Member_key = @MemberKey and tranmonth = DATEPART(month, @startdate) and tranyear = DATEPART(year, @startdate)
--group by r.Member_key, r.TerminalId, r.tranmonth, r.tranyear
--union


select Sum(isnull(td.amount,0)) as TotalAmount,'Other Fees' as FeeGroup,
'Terminal Rental Refund TID: ' + cast(Tal.TerminalId as varchar) +'  ' + (cast(trh.rentMonth as varchar) +'/'+ cast(trh.RentYear as varchar)) as descriptions,
trh.rentMonth, trh.RentYear
from  [transaction] t 
inner join transactiondetail td on t.transaction_key = td.transaction_key
inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = td.Docket_key
inner JOIN vTerminalAllocations Tal
ON  Tal.TerminalAllocation_key = trh.TerminalAllocation_key
where t.member_key = @MemberKey and td.TransactionDetailType = 10
--and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
--AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
and t.faileddate is null and t.TransactionDate  between @startdate and @enddate
group by trh.TerminalAllocation_key,tal.TerminalId , trh.rentMonth, trh.RentYear

UNION
   SELECT
		   Sum(isnull(td.amount,0)) as TotalAmount,
		   'Other Fees' as FeeGroup,
		   'Account Fee ' + right('00' + cast(datepart(month, fch.StartDate) as varchar(2)), 2) + '/' + right('0000' + cast(datepart(year, fch.StartDate) as varchar(4)), 4) as descriptions,
		   datepart(month, fch.StartDate) as rentMonth, 
		   datepart(year, fch.StartDate) as RentYear
        FROM [Transaction] t
        INNER JOIN TransactionDetail td
            ON td.Transaction_key = t.Transaction_key
		INNER JOIN vOperators 
			ON t.Member_key = vOperators.Operator_key
		INNER JOIN FeeChargeHistory fch
			ON fch.DocketKey = td.Docket_key
    WHERE td.TransactionDetailType = 19
          AND vOperators.operator_key = @MemberKey 
		  AND t.FailedDate IS NULL
		  AND t.TransactionDate BETWEEN @startdate AND @enddate
	group by t.Member_key, datepart(month, fch.StartDate), datepart(year, fch.StartDate)

UNION
   SELECT
		   Sum(isnull(td.amount,0)) as TotalAmount,
		   'Other Fees' as FeeGroup,
		   'Refund Account Fee ' + right('00' + cast(datepart(month, fch.StartDate) as varchar(2)), 2) + '/' + right('0000' + cast(datepart(year, fch.StartDate) as varchar(4)), 4) as descriptions,
		   datepart(month, fch.StartDate) as rentMonth, 
		   datepart(year, fch.StartDate) as RentYear
        FROM [Transaction] t
        INNER JOIN TransactionDetail td
            ON td.Transaction_key = t.Transaction_key
		INNER JOIN vOperators 
			ON t.Member_key = vOperators.Operator_key
		INNER JOIN FeeChargedRefund fcr
			ON fcr.DocketKey = td.Docket_key
		INNER JOIN FeeChargeHistory fch
			ON fch.ID = fcr.FeeChargeHistoryID
    WHERE td.TransactionDetailType = 20
          AND vOperators.operator_key = @MemberKey 
		  AND t.FailedDate IS NULL
		  AND t.TransactionDate BETWEEN @startdate AND @enddate
	group by t.Member_key, datepart(month, fch.StartDate), datepart(year, fch.StartDate)

union
select Sum(isnull(td.amount,0)) as TotalAmount,'Other Fees' as FeeGroup,
'Charge Back Fee'  as descriptions, 0 as rentMonth, 0 as RentYear
from  [transaction] t 
inner join transactiondetail td on t.transaction_key = td.transaction_key
inner join CadmusIN c on c.docket_key = td.Docket_key
where t.member_key = @MemberKey and (td.TransactionDetailType = 7 and td.Description like  '%Charge Back Fee%')
--and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
--AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
and t.faileddate is null and t.TransactionDate  between @startdate and @enddate
group by t.Member_key


union
select Sum(isnull(td.amount,0)) as TotalAmount,'Merchant Service Fees' as FeeGroup,
'MSF'  as descriptions, 1 as rentMonth, 1 as RentYear
from  [transaction] t 
inner join transactiondetail d on t.transaction_key = d.transaction_key
outer apply (
select SUM(md.Amount) Amount from TransactionDetail md where md.Docket_key = d.Docket_key and md.TransactionDetailType = 2
) td
inner join CadmusIN c on c.docket_key = d.Docket_key
where t.member_key = @MemberKey and (d.TransactionDetailType = 0)
and t.faileddate is null and t.TransactionDate  > = @startdate and t.TransactionDate < @enddate
group by t.Member_key
order by RentYear, rentMonth
