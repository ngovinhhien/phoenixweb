﻿SET QUOTED_IDENTIFIER ON;
SET ANSI_NULLS ON;
GO
CREATE PROC [dbo].[usp_GetFeeChargeHistoryData]
    @memberKey INT,
    @page INT = 1,
    @offset INT = 10,
    @orderColumn VARCHAR(100) = '',
    @status VARCHAR(50) = '',
    @startdate VARCHAR(10),
    @enddate VARCHAR(10),
    @totalNumber INT OUTPUT
AS
BEGIN
    DECLARE @query NVARCHAR(MAX);

    SET @query
        = N'DECLARE @tempTable TABLE
		(
			ID INT,
			Amount DECIMAL(18, 2),
			ProcessedDate DATETIME,
			FeeTypeName VARCHAR(100),
			StartDate DATETIME,
			FailedDate DATETIME,
			ReferenceNumber VARCHAR(20),
			Status VARCHAR(20),
			IsRefunded BIT
		) ';

    SET @query += N' DECLARE @latestTransactionDetail TABLE
		(
			DocketKey INT,
			TransacionDetailKey INT
		) ';

    SET @query += N' 
		DECLARE @tempTableRefund TABLE
		(
			ID INT,
			RefundID INT,
			Amount DECIMAL(18, 2),
			ProcessedDate DATETIME,
			FailedDate DATETIME,
			ReferenceNumber VARCHAR(20),
			Status VARCHAR(20),
			IsRefunded BIT,
			RefundDate VARCHAR(30),
			RefundReason VARCHAR(300),
			RefundTicketNumber VARCHAR(30)
		); ';

    SET @query += N' 
		DECLARE @tempTableResult TABLE
		(
			ID INT,
			Amount DECIMAL(18, 2),
			ProcessedDate DATETIME,
			StartDate DATETIME,
			FailedDate DATETIME,
			Status VARCHAR(20),
			ParentReferenceNumber VARCHAR(20),
			ReferenceNumber VARCHAR(20),
			IsRefunded BIT,
			RefundAmount DECIMAL(18, 2),
			RefundTicketNumber VARCHAR(30),
			RefundReason VARCHAR(300),
			RefundID INT,
			FeeTypeName VARCHAR(100),
			RefundDate VARCHAR(30)
		); ';
    --Order:Amount, FeeName, Period, Status, ProcessDate
    --Filter: StartDate/EndDate, Status
    SET @query += N'
		INSERT INTO @latestTransactionDetail
		(
			DocketKey,
			TransacionDetailKey
		)
		SELECT Docket_key,
			   MAX(TransactionDetail_key)
		FROM dbo.TransactionDetail
		WHERE Member_key = ' + CAST(@memberKey AS VARCHAR(20)) + ' AND TransactionDetailType IN (19,20)
		GROUP BY Docket_key;
'   ;

    SET @query += N'
INSERT INTO @tempTable
SELECT DISTINCT
       FeeChargeHistory.ID,
       FeeChargeHistory.Amount,
       FeeChargeHistory.ProcessedDate,
       Name,
       StartDate,
       t.FailedDate,
       t.LodgementReferenceNo,
       CASE
           WHEN (
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NULL
				)
				OR 
				(
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NOT NULL
					AND t.FailedDate IS NULL
				)
				OR
				(
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NOT NULL
					AND t.FailedDate IS NOT NULL
				) 
				THEN ''PENDING''
           WHEN ISNULL(ptd.PendingTransactionDetail_key, 0) = 0
                AND td.Docket_key IS NOT NULL
                AND t.FailedDate IS NULL THEN
               ''SUCCESS''
           WHEN ISNULL(ptd.PendingTransactionDetail_key, 0) = 0
                AND td.Docket_key IS NOT NULL
                AND t.FailedDate IS NOT NULL THEN
               ''FAILED''
       END AS Status,
	   CASE
			WHEN ISNULL(fcr.FeeChargeHistoryID, 0) <> 0
			THEN 1
			ELSE 0
		END AS IsRefunded
FROM dbo.FeeChargeHistory
    INNER JOIN dbo.FeeChargeDetail
        ON FeeChargeDetail.ID = FeeChargeHistory.FeeChargeDetailId
    INNER JOIN dbo.Fee
        ON Fee.ID = FeeChargeDetail.FeeID
    INNER JOIN dbo.DocketTable
        ON Docket_key = DocketKey
	LEFT JOIN dbo.FeeChargedRefund fcr
		ON fcr.FeeChargeHistoryID = FeeChargeHistory.ID
    LEFT JOIN dbo.PendingTransactionDetail ptd
        ON ptd.Docket_key = DocketTable.Docket_key
    LEFT JOIN
    (
        SELECT *
        FROM TransactionDetail tde
            INNER JOIN @latestTransactionDetail ltd
                ON tde.Docket_key = ltd.DocketKey
                   AND tde.TransactionDetail_key = ltd.TransacionDetailKey
    ) td
        ON td.Docket_key = DocketTable.Docket_key
    LEFT JOIN dbo.[Transaction] t
        ON t.Transaction_Key = td.Transaction_Key
		';
    DECLARE @whereQuery VARCHAR(MAX) = ' WHERE (1=1) ';
    IF ISNULL(@memberKey, 0) <> 0
    BEGIN
        SET @whereQuery += ' AND MemberKey = ' + CAST(@memberKey AS VARCHAR(20)) + ' ';
    END;

    IF ISNULL(@startdate, '') <> ''
       AND ISNULL(@enddate, '') <> ''
    BEGIN
        SET @whereQuery += ' AND DATEADD(month, DATEDIFF(month, 0, StartDate), 0) BETWEEN 
								 DATEADD(month, DATEDIFF(month, 0, ''' + @startdate
                           + '''), 0) AND
								 DATEADD(month, DATEDIFF(month, 0, ''' + @enddate + '''), 0)';
    END;
    ELSE
    BEGIN
        IF ISNULL(@startdate, '') <> ''
        BEGIN
            SET @whereQuery += ' AND DATEADD(month, DATEDIFF(month, 0, StartDate), 0) >= DATEADD(month, DATEDIFF(month, 0, '''
                               + @startdate + '''), 0) ';
        END;
        IF ISNULL(@enddate, '') <> ''
        BEGIN
            SET @whereQuery += ' AND DATEADD(month, DATEDIFF(month, 0, StartDate), 0) <= DATEADD(month, DATEDIFF(month, 0, '''
                               + @enddate + '''), 0) ';
        END;
    END;

    SET @query += @whereQuery;


    SET @query += N'
		INSERT INTO @tempTableRefund
(
    ID,
    RefundID,
    Amount,
    ProcessedDate,
    FailedDate,
    ReferenceNumber,
    Status,
    IsRefunded,
    RefundDate,
    RefundReason,
    RefundTicketNumber
)
SELECT t.ID,
       FeeChargedRefund.ID,
       RefundAmount,
       FeeChargedRefund.ProcessedDate,
       tr.FailedDate,
       tr.LodgementReferenceNo,
	   CASE
           WHEN (
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NULL
				) 
				OR
				(
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NOT NULL
					AND tr.FailedDate IS NULL
				) 
				OR 
				(
					ISNULL(ptd.PendingTransactionDetail_key, 0) <> 0
					AND td.Docket_key IS NOT NULL
					AND tr.FailedDate IS NOT NULL
				)
				THEN ''PENDING_REFUND''
           WHEN ISNULL(ptd.PendingTransactionDetail_key, 0) = 0
                AND td.Docket_key IS NOT NULL
                AND tr.FailedDate IS NULL THEN
               ''REFUNDED''
           WHEN ISNULL(ptd.PendingTransactionDetail_key, 0) = 0
                AND td.Docket_key IS NOT NULL
                AND tr.FailedDate IS NOT NULL THEN
               ''FAILED_REFUND''
       END AS Status,
	   1,
	   CONVERT(VARCHAR(30), FeeChargedRefund.CreatedDate, 103),
	   Reason,
	   TicketNumber
FROM dbo.FeeChargedRefund
    INNER JOIN @tempTable t
        ON t.ID = FeeChargedRefund.FeeChargeHistoryID
    INNER JOIN dbo.DocketTable
        ON Docket_key = DocketKey
    LEFT JOIN dbo.PendingTransactionDetail ptd
        ON ptd.Docket_key = DocketTable.Docket_key
    LEFT JOIN
    (
        SELECT *
        FROM TransactionDetail tde
            INNER JOIN @latestTransactionDetail ltd
                ON tde.Docket_key = ltd.DocketKey
                   AND tde.TransactionDetail_key = ltd.TransacionDetailKey
    ) td
        ON td.Docket_key = DocketTable.Docket_key
    LEFT JOIN dbo.[Transaction] tr
        ON tr.Transaction_key = td.Transaction_key;
	';


    SET @query += N'
		INSERT INTO @tempTableResult
		SELECT t.ID,
			   t.Amount,
			   CASE
				   WHEN t.IsRefunded = 1 THEN
					   t1.ProcessedDate
				   ELSE
					   t.ProcessedDate
			   END AS ProcessedDate,
			   t.StartDate,
			   CASE
				   WHEN t.IsRefunded = 1 THEN
					   t1.FailedDate
				   ELSE
					   t.FailedDate
			   END AS FailedDate,
			   CASE
				   WHEN t.IsRefunded = 1 THEN
					   t1.Status
				   ELSE
					   t.Status
			   END AS Status,
			   t.ReferenceNumber AS ParentReferenceNumber,
			   t1.ReferenceNumber,
			   t.IsRefunded,
			   t1.Amount AS RefundAmount,
			   t1.RefundTicketNumber,
			   t1.RefundReason,
			   ISNULL(t1.RefundID,0) as RefundID,
			   t.FeeTypeName,
			   t1.RefundDate
		FROM @tempTable t
			INNER JOIN [Status]
				ON Name = t.Status
			LEFT JOIN @tempTableRefund t1
				ON t.ID = t1.ID
	';

    IF ISNULL(@orderColumn, '') = ''
    BEGIN
        SET @orderColumn = ' ProcessedDate DESC, FeeTypeName ASC, StartDate DESC, ListOrder ASC ';
    END;

    DECLARE @loadTempTable NVARCHAR(MAX) = N'';
    SET @loadTempTable += N'';

    SET @loadTempTable += N'
SELECT t.* FROM @tempTableResult t
INNER JOIN [Status] ON Name = t.STATUS
WHERE (1 = 1) ';

    IF ISNULL(@status, '') <> ''
    BEGIN
        SET @loadTempTable += N' AND Name = ''' + @status + N''' ';
    END;

    DECLARE @loadTempTableWithCount VARCHAR(MAX) = REPLACE(@loadTempTable, 't.*', '@totalNumber = COUNT(*)');
    SET @query += @loadTempTableWithCount;
    SET @query += REPLACE(@loadTempTable, 't.*', 't.*, Description as StatusDescription ');
    SET @query += N' ORDER BY ' + @orderColumn + N' OFFSET ((' + CAST(@page AS VARCHAR) + N' - 1) * '
                  + CAST(@offset AS VARCHAR) + N') ROWS FETCH NEXT ' + CAST(@offset AS VARCHAR) + N' ROWS ONLY';

    PRINT @query;
    DECLARE @Params NVARCHAR(MAX) = N'
			@totalNumber INT OUTPUT
		';
    EXECUTE sp_executesql @query, @Params, @totalNumber = @totalNumber OUT;

END;

GO