﻿ALTER PROCEDURE [dbo].[TerminalRentalProcess]
    @Companykey INT = 0,
    @month INT = 0,
    @year INT = 0
AS
BEGIN



    DECLARE @asofdate DATETIME;



    --declare @Companykey int

    DECLARE @tranmonth INT;
    DECLARE @tranyear INT;
    DECLARE @rentamount DECIMAL(10, 2);
    DECLARE @transactiontarget DECIMAL(10, 2);
    DECLARE @terminalrentalrate_key INT;
    DECLARE @terminalallocation_key INT;
    DECLARE @memberrental_key INT;
    DECLARE @totaltranamount DECIMAL(10, 2);
    DECLARE @member_key INT;
    DECLARE @TerminalID NVARCHAR(10);
    DECLARE @DocketKey INT;
    DECLARE @RentFree BIT;

    IF @month <> 0
       AND @year <> 0
    BEGIN
        SET @asofdate
            = CAST(CAST(@year AS VARCHAR(4)) + RIGHT('00' + CAST(@month AS NVARCHAR(2)), 2) + '01' AS DATETIME);
    END;


    IF @asofdate IS NULL
    BEGIN
        SET @asofdate = GETDATE();
    END;

    --set @Companykey = 19

    DECLARE @temptable TABLE
    (
        TerminalID NVARCHAR(10),
        tranmonth INT,
        tranyear INT,
        rentamount DECIMAL(10, 2),
        transactiontarget DECIMAL(10, 2),
        terminalrentalrate_key INT,
        terminalallocation_key INT,
        memberrental_key INT,
        totaltranamount DECIMAL(10, 2),
        member_key INT,
        RentFree BIT
    );


    DECLARE @IdentityOutput TABLE
    (
        ID INT
    );

    DECLARE @RentalCursor CURSOR;

    INSERT INTO @temptable
    (
        TerminalID,
        tranmonth,
        tranyear,
        rentamount,
        transactiontarget,
        terminalrentalrate_key,
        terminalallocation_key,
        memberrental_key,
        totaltranamount,
        member_key,
        RentFree
    )
    SELECT TerminalId,
           tranmonth,
           tranyear,
           RentAmount,
           TransactionTarget,
           TerminalRentalRate_key,
           terminalallocation_key,
           MemberRental_key,
           totaltranamount,
           Member_key,
           RentFree
    FROM TerminalRentalCalculation r
    WHERE (
              Company_key = @Companykey
              AND @Companykey = 19
          )
          AND tranmonth = DATEPART(MONTH, DATEADD(MONTH, -1, @asofdate))
          AND tranyear = DATEPART(YEAR, DATEADD(MONTH, -1, @asofdate))
          AND RentAmount > 0
          AND NOT EXISTS
    (
        SELECT *
        FROM TerminalRentalHistory h
        WHERE h.RentMonth = r.tranmonth
              AND h.RentYear = r.tranyear
              AND h.TerminalAllocation_key = r.terminalallocation_key
    )
          AND r.TerminalId NOT LIKE '8%'
    UNION
    SELECT TerminalId,
           tranmonth,
           tranyear,
           RentAmount,
           TransactionTarget,
           TerminalRentalRate_Key,
           terminalallocation_key,
           MemberRental_key,
           totaltranamount,
           m.Member_key,
           RentFree
    FROM TerminalRentalCalculationLE c
        INNER JOIN Member m
            ON m.Member_key = c.Member_key
    WHERE (
              c.Company_key = @Companykey
              AND @Companykey IN ( 18, 22, 26, 27 )
          )
          AND tranmonth = DATEPART(MONTH, DATEADD(MONTH, -1, @asofdate))
          AND tranyear = DATEPART(YEAR, DATEADD(MONTH, -1, @asofdate))
          AND RentAmount > 0
          AND NOT EXISTS
    (
        SELECT *
        FROM TerminalRentalHistory h
        WHERE h.RentMonth = c.tranmonth
              AND h.RentYear = c.tranyear
              AND h.TerminalAllocation_key = c.terminalallocation_key
    )
          AND c.TerminalId NOT LIKE '8%'
    UNION
    SELECT TerminalId,
           tranmonth,
           tranyear,
           RentAmount,
           TransactionTarget,
           TerminalRentalRate_Key,
           terminalallocation_key,
           MemberRental_key,
           totaltranamount,
           m.Member_key,
           RentFree
    FROM TerminalRentalCalculationTaxiPro c
        INNER JOIN Member m
            ON m.Member_key = c.Member_key
    WHERE (
              (
                  c.Company_key = @Companykey
                  AND @Companykey = 20
              )
              OR
              (
                  c.Company_key = 1
                  AND @Companykey = 20
                  AND c.memberid = 'N25543'
              )
          )
          AND tranmonth = DATEPART(MONTH, DATEADD(MONTH, -1, @asofdate))
          AND tranyear = DATEPART(YEAR, DATEADD(MONTH, -1, @asofdate))
          AND RentAmount > 0
          AND NOT EXISTS
    (
        SELECT *
        FROM TerminalRentalHistory h
        WHERE h.RentMonth = c.tranmonth
              AND h.RentYear = c.tranyear
              AND h.TerminalAllocation_key = c.terminalallocation_key
    )
          AND c.TerminalId NOT LIKE '8%'
    UNION
    SELECT TerminalId,
           tranmonth,
           tranyear,
           RentAmount,
           TransactionTarget,
           TerminalRentalRate_key,
           terminalallocation_key,
           MemberRental_key,
           totaltranamount,
           Member_key,
           RentFree
    FROM TerminalRentalCalculation r
    WHERE (
              Company_key = @Companykey
              AND @Companykey = 23
          )
          AND tranmonth = DATEPART(MONTH, DATEADD(MONTH, -1, @asofdate))
          AND tranyear = DATEPART(YEAR, DATEADD(MONTH, -1, @asofdate))
          AND RentAmount > 0
          AND NOT EXISTS
    (
        SELECT *
        FROM TerminalRentalHistory h
        WHERE h.RentMonth = r.tranmonth
              AND h.RentYear = r.tranyear
              AND h.TerminalAllocation_key = r.terminalallocation_key
    )
          AND r.TerminalId NOT LIKE '8%';





    SET @RentalCursor = CURSOR FOR
    SELECT TerminalID,
           tranmonth,
           tranyear,
           rentamount,
           transactiontarget,
           terminalrentalrate_key,
           terminalallocation_key,
           memberrental_key,
           totaltranamount,
           member_key,
           RentFree
    FROM @temptable;

    OPEN @RentalCursor;

    FETCH NEXT FROM @RentalCursor
    INTO @TerminalID,
         @tranmonth,
         @tranyear,
         @rentamount,
         @transactiontarget,
         @terminalrentalrate_key,
         @terminalallocation_key,
         @memberrental_key,
         @totaltranamount,
         @member_key,
         @RentFree;
    WHILE @@fetch_status = 0 --for each member and account
    BEGIN

        INSERT INTO DocketTable
        (
            BatchId,
            DocketDate,
            DocketPaymentStatus,
            FaceValue,
            DocketTotal,
            Office_key,
            ModifiedByUser,
            Modified_dttm
        )
        OUTPUT inserted.Docket_key
        INTO @IdentityOutput
        VALUES
        (CAST(@tranyear AS VARCHAR(4)) + RIGHT('00' + CAST(@tranmonth AS NVARCHAR(2)), 2)
         +  RIGHT('0000000' + @TerminalID, 8) + RIGHT('00' + CAST(DATEPART(MONTH, GETDATE()) AS NVARCHAR(2)), 2)
         +  RIGHT('00' + CAST(DATEPART(DAY, GETDATE()) AS NVARCHAR(2)), 2), CAST(GETDATE() AS DATE), 0, @rentamount,
         @rentamount, 51, 'Auto Process', GETDATE());

        SET @DocketKey =
        (
            SELECT ID FROM @IdentityOutput
        );

        INSERT INTO TerminalRentalHistory
        (
            TerminalRentalHistory_key,
            MemberRental_key,
            RentMonth,
            RentYear,
            RentAmount,
            RentFree,
            TerminalAllocation_key,
            TerminalRentalRate_key,
            TransactionTarget,
            TransactionTotalAmount,
            ModifiedByUser,
            Modified_dttm,
            ProcessByUser,
            ProcessedDate
        )
        VALUES
        (@DocketKey, @memberrental_key, @tranmonth, @tranyear, @rentamount, @RentFree, @terminalallocation_key,
         @terminalrentalrate_key, @transactiontarget, @totaltranamount, 'Auto Process', GETDATE(), 'Auto Process',
         GETDATE());

        IF (@RentFree = 0)
        BEGIN
            INSERT INTO PendingTransactionDetail
            (
                ItemDate,
                RateUsed,
                Amount,
                TransactionDetailType,
                Description,
                ModifiedByUser,
                Modified_dttm,
                Docket_key,
                Member_key
            )
            VALUES
            (CAST(GETDATE() AS DATE), -1, -@rentamount, 9, 'Terminal Rental Fee', 'Auto Process', GETDATE(),
             @DocketKey, @member_key);
        END;

        DELETE FROM @IdentityOutput;

        FETCH NEXT FROM @RentalCursor
        INTO @TerminalID,
             @tranmonth,
             @tranyear,
             @rentamount,
             @transactiontarget,
             @terminalrentalrate_key,
             @terminalallocation_key,
             @memberrental_key,
             @totaltranamount,
             @member_key,
             @RentFree;


    END;

    DELETE FROM @temptable;


END;
