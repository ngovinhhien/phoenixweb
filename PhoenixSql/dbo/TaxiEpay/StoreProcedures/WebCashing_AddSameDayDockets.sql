﻿ALTER PROCEDURE [dbo].[WebCashing_AddSameDayDockets]
    @user NVARCHAR(50),
    @BatchID NVARCHAR(20),
    @BatchTop NVARCHAR(20),
    @BatchBottom NVARCHAR(20),
    @memberKey INT,
    @DocketpaymethodKey INT,
    @amount MONEY,
    @quantity INT,
    @DocketCheckSummary INT = NULL,
    @CashingCenterRate FLOAT = NULL,
    @OfficeKey INT = NULL,
	@TvpGlideboxCommission TVPGlideboxCommissionForSameDayCashing READONLY,
	@ProcessGlideboxCommission BIT = 0
AS
BEGIN

    DECLARE @DocketDate DATETIME;
    --declare @BatchID nvarchar(20)
    DECLARE @FaceValue MONEY;
    DECLARE @servicecharge MONEY;
    DECLARE @GST MONEY;
    DECLARE @MemberID NVARCHAR(20);
    DECLARE @docketkey INT;
    DECLARE @handlingrate FLOAT;
    DECLARE @commissionRate FLOAT;
    DECLARE @Commission MONEY;
    DECLARE @HandlingCharge MONEY;
    DECLARE @docketchecksummarykey INT;
    DECLARE @totalamount MONEY;
    DECLARE @sequence INT = 0;
    DECLARE @FlatHandlingCharge BIT;
    DECLARE @transactiontypeDescription NVARCHAR(30);
    DECLARE @TerminalID NVARCHAR(12);
    DECLARE @Batchnumber NVARCHAR(10);

    DECLARE @BID NVARCHAR(20);
    DECLARE @LevyCharge MONEY;
    SET @DocketDate = CAST(GETDATE() AS DATETIME);

    SET @FaceValue = @amount;

    DECLARE @allowCash BIT = 0;
	DECLARE @CompanyKey INT;

    --set @BatchID = cast(datepart(year, @DocketDate)as nvarchar(4)) 
    --			+ right('00'+cast(datepart(month, @DocketDate)as nvarchar),2)+ right('00'+cast(datepart(day, @DocketDate)as nvarchar),2)
    --			+ right('00'+cast(datepart(hour, @DocketDate)as nvarchar),2)+ right('00'+cast(datepart(minute, @DocketDate)as nvarchar),2)

    SELECT @AllowCash = o.AllowCash, 
			@CompanyKey = t.company_key
	FROM dbo.Operator o
	INNER JOIN dbo.vTerminalAllocations t
	ON o.Operator_key = t.Member_key
	WHERE o.Operator_key = @MemberKey;




    IF (ISNULL(@allowCash, 0) = 1)
    BEGIN
        SELECT @DocketDate = CAST(GETDATE() AS DATETIME), --@batchid=memberid+@BatchID, 
               @FaceValue = @amount,
               @servicecharge = CAST(@FaceValue * CASE
                                                      WHEN State IN ( 'VIC, NSW, WA, SA, ACT' ) THEN
                                                          0.045
                                                      ELSE
                                                          0.1
                                                  END AS DECIMAL(10, 2)),
               @GST = CAST(@FaceValue * CASE
                                            WHEN State IN ( 'VIC, NSW, WA, SA, ACT' ) THEN
                                                0.045
                                            ELSE
                                                0.1
                                        END * 0.1 AS DECIMAL(5, 2)),
               @commissionRate = cr.OperatorCommissionRate,
               @handlingrate = COALESCE(pff.FeeRate, cr.FareHandlingRate, 0),
               @HandlingCharge = CASE
                                     WHEN ISNULL(pff.FlatAmount, 0) = 1 THEN
                                         pff.FeeAmount
                                     ELSE
                                         COALESCE(pff.FeeRate, cr.FareHandlingRate, 0) * @amount
                                 END,
               @FlatHandlingCharge = ISNULL(pff.FlatAmount, 0),
               @transactiontypeDescription = N'Same day - ' + cr.Name,
               @LevyCharge = CASE
                                 WHEN
                                 (
                                     State IN ( 'SA' )
                                     AND sal.memberkey IS NULL
                                 ) THEN
                                     @quantity * 1.00
                                 ELSE
                                     0
                             END                          --add levy charge for SA customer
        FROM Member m
            LEFT JOIN Transactionhost.dbo.SAMemberWithoutLevy() sal
                ON sal.memberkey = m.Member_key
            INNER JOIN Operator o
                ON o.Operator_key = m.Member_key
                   AND o.PayByEFT = 0
                   AND o.AllowCash = 1
            OUTER APPLY
        (
            SELECT TOP 1
                   OperatorCommissionRate,
                   FareHandlingRate,
                   DocketPayMethod_key,
                   cr.Name
            FROM [MemberCommissionRate] cr
            WHERE cr.Member_key = m.Member_key
                  AND cr.DocketPayMethod_key = @DocketpaymethodKey
        ) cr
            LEFT JOIN State s
                ON s.StateName = m.State
            LEFT JOIN PaymentFee pff
                ON pff.DocketPayMethod_key = cr.DocketPayMethod_key
                   AND pff.State_Key = s.State_key
                   AND pff.TransactionType_key = 1 --only for Cash over the counter payment

        WHERE m.Member_key = @memberKey;

        SET @TerminalID = LEFT(ISNULL(@BatchTop, ''), 8);
        SET @Batchnumber = LEFT(ISNULL(@BatchBottom, ''), 6);
        SET @BID = @BatchID;
        IF NOT EXISTS
        (
            SELECT Docket_key
            FROM DocketTable
            WHERE BatchId = @BID
                  AND DocketPayMethod_key = @DocketpaymethodKey
        )
        BEGIN


            --Add to DocketTable
            INSERT INTO DocketTable
            (
                DocketDate,
                BatchId,
                FaceValue,
                ServiceCharge,
                Gst,
                DocketTotal,
                ModifiedByUser,
                Modified_dttm,
                Office_key,
                DocketPayMethod_key,
                LevyCharge
            )
            VALUES
            (CAST(@DocketDate AS DATE), @BatchID, @FaceValue, @servicecharge, @GST, @FaceValue + @servicecharge + @GST,
             @user, GETDATE(), @OfficeKey, @DocketpaymethodKey, @LevyCharge);

            SET @docketkey = SCOPE_IDENTITY();

            ----add to ManualDocket table
            --insert into ManualDocket (ProcessingDate, ModifiedByUser, Modified_dttm, ManualDocket_key, Member_key)
            --values (cast(@DocketDate as date), @user, GETDATE(), @docketkey, @memberKey)

            --Add to SummaryDocket table
            INSERT INTO SummaryDocket
            (
                SummaryDocket_key,
                Quantity,
                DocketType,
                BatchTop,
                BatchBottom,
                Modified_dttm,
                ModifiedByUser,
                Member_key,
                BatchId,
                LevyCharge
            )
            VALUES
            (@docketkey, @quantity, 0, @BatchTop, @BatchBottom, GETDATE(), @user, @memberKey, @BatchID, @LevyCharge);

            --Add to PendingTransaction
            --Add FaceValue
            INSERT INTO PendingTransactionDetail
            (
                ItemDate,
                RateUsed,
                Amount,
                TransactionDetailType,
                Description,
                ModifiedByUser,
                Modified_dttm,
                Docket_key,
                Member_key
            )
            VALUES
            (CAST(@DocketDate AS DATE), 1, @FaceValue, 8, @transactiontypeDescription, @user, GETDATE(), @docketkey,
             @memberKey);
            --Add CommissionRate/Charge
            IF (ISNULL(@commissionRate, 0) <> 0)
            BEGIN
                INSERT INTO PendingTransactionDetail
                (
                    ItemDate,
                    RateUsed,
                    Amount,
                    TransactionDetailType,
                    Description,
                    ModifiedByUser,
                    Modified_dttm,
                    Docket_key,
                    Member_key
                )
                VALUES
                (CAST(@DocketDate AS DATE), @commissionRate, @FaceValue * @commissionRate, 2, 'Operator Commission',
                 @user, GETDATE(), @docketkey, @memberKey);
            END;

            IF (@LevyCharge <> 0)
            BEGIN
                INSERT INTO PendingTransactionDetail
                (
                    ItemDate,
                    RateUsed,
                    Amount,
                    TransactionDetailType,
                    Description,
                    ModifiedByUser,
                    Modified_dttm,
                    Docket_key,
                    Member_key
                )
                VALUES
                (CAST(@DocketDate AS DATE), 1, @LevyCharge, 18, 'Levy Charge', @user, GETDATE(), @docketkey, @memberKey);
            END;

            --Add Handling charge
            IF ISNULL(@HandlingCharge, 0) <> 0
            BEGIN
                INSERT INTO PendingTransactionDetail
                (
                    ItemDate,
                    RateUsed,
                    Amount,
                    TransactionDetailType,
                    Description,
                    ModifiedByUser,
                    Modified_dttm,
                    Docket_key,
                    Member_key
                )
                VALUES
                (   CAST(@DocketDate AS DATE), CASE
                                                   WHEN @FlatHandlingCharge = 1 THEN
                                                       1
                                                   ELSE
                                                       @handlingrate
                                               END * -1, @HandlingCharge * -1, 5, 'Handling Fee', @user, GETDATE(),
                    @docketkey, @memberKey);
            END;


            --Add to DocketCheckSummaryKey
            IF ISNULL(@docketkey, 0) <> 0
            BEGIN
                IF @DocketCheckSummary IS NULL
                BEGIN
                    INSERT INTO DocketCheckSummary
                    (
                        CreatedDate,
                        CreatedByUser,
                        MemberKey
                    )
                    VALUES
                    (GETDATE(), @user, @memberKey);

                    SET @docketchecksummarykey = SCOPE_IDENTITY();


                END;
                ELSE
                BEGIN
                    SET @docketchecksummarykey = @DocketCheckSummary;

                END;

                IF @memberKey IS NULL
                BEGIN
                    --select @memberKey = max(member_key) from @temptable
                    UPDATE DocketCheckSummary
                    SET MemberKey = @memberKey
                    WHERE DocketCheckSummaryKey = @docketchecksummarykey;
                END;
                ELSE
                BEGIN
                    UPDATE DocketCheckSummary
                    SET MemberKey = @memberKey
                    WHERE DocketCheckSummaryKey = @docketchecksummarykey
                          AND MemberKey IS NULL;
                END;



                INSERT INTO DocketCheckDetail
                (
                    DocketCheckAmount,
                    DocketCheckSummaryKey,
                    Docket_key,
                    CreatedDate,
                    CreatedByUser,
                    BatchStatus,
                    IsCashable,
                    TerminalID,
                    BatchNumber
                )
                SELECT SUM(Amount),
                       @docketchecksummarykey,
                       Docket_key,
                       GETDATE(),
                       @user,
                       'Valid',
                       1,
                       @TerminalID,
                       @Batchnumber
                FROM PendingTransactionDetail
                WHERE Docket_key = @docketkey
                GROUP BY Docket_key;
                --select *

                SELECT @totalamount = SUM(dcd.DocketCheckAmount)
                FROM DocketCheckSummary dcs
                    INNER JOIN DocketCheckDetail dcd
                        ON dcd.DocketCheckSummaryKey = dcs.DocketCheckSummaryKey
                WHERE dcs.DocketCheckSummaryKey = @docketchecksummarykey;
                --group by dcd.DocketCheckSummaryKey

                UPDATE DocketCheckSummary
                SET TotalAmount = @totalamount
                WHERE DocketCheckSummaryKey = @docketchecksummarykey;

            END;

        END;

		--Glidebox
		IF ISNULL(@ProcessGlideboxCommission, 0) = 1
			AND @CompanyKey IN (1, 20, 23) 
			AND EXISTS (SELECT TOP(1) 1 FROM @TvpGlideboxCommission)
		BEGIN
			DECLARE @CategoryID INT,
					@ShortName VARCHAR(50),
					@Price DECIMAL(10,2),
					@GlideboxQuantity INT,
					@GlideboxCommissionAmount DECIMAL(10,2),
					@TotalGlideboxCommissionAmount DECIMAL(10,2),
					@IsPercentage BIT

			DECLARE GlideboxCur CURSOR LOCAL READ_ONLY FOR
			SELECT 
				CategoryID,
				ShortName,
				Price, 
				Quantity, 
				CommissionAmount,
				TotalCommissionAmount,
				IsPercentage
			FROM @TvpGlideboxCommission

			OPEN GlideboxCur

			FETCH NEXT FROM GlideboxCur INTO @CategoryID,
											@ShortName,
											@Price, 
											@GlideboxQuantity, 
											@GlideboxCommissionAmount, 
											@TotalGlideboxCommissionAmount,
											@IsPercentage

			WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Insert into DocketTable
				INSERT INTO dbo.DocketTable
				(
					DocketDate,
					BatchId,
					FaceValue,
					ServiceCharge,
					Gst,
					DocketTotal,
					ModifiedByUser,
					Modified_dttm,
					Office_key,
					DocketPayMethod_key,
					LevyCharge
				)
				VALUES
				(
					CAST(@DocketDate AS DATE), 
					@BatchID, 
					@GlideboxQuantity * @Price, 
					0, 
					0, 
					@GlideboxQuantity * @Price,
					@user, 
					GETDATE(), 
					@OfficeKey, 
					NULL, 
					0
				);
				SET @docketkey = SCOPE_IDENTITY();

				--Insert into GlideboxSameDayCashing
				INSERT INTO dbo.GlideboxSameDayCashing
				(
					DocketKey,
					TerminalID,
					BatchTop,
					BatchBottom,
					BatchID,
					MemberKey,
					CategoryID,
					ShortName,
					Price,
					Quantity,
					TotalGlideboxAmount,
					CommissionAmount,
					TotalGlideboxCommission,
					IsPercentage,
					CreatedDatetime,
					CreatedBy
				)
				VALUES
				(   
				   @DocketKey,
				   @TerminalID,
				   @BatchTop,
				   @BatchBottom,
				   @BatchID,
				   @MemberKey,
				   @CategoryID,
				   @ShortName,
				   @Price,
				   @GlideboxQuantity,
				   @Price * @GlideboxQuantity,
				   @GlideboxCommissionAmount,
				   @TotalGlideboxCommissionAmount,
				   @IsPercentage,
				   GETDATE(),
				   @user
				)

				--Insert into PendingTransactionDetail
				IF ISNULL(@TotalGlideboxCommissionAmount, 0) <> 0
				BEGIN
					INSERT INTO dbo.PendingTransactionDetail
					(
						ItemDate,
						RateUsed,
						Amount,
						TransactionDetailType,
						Description,
						ModifiedByUser,
						Modified_dttm,
						Docket_key,
						Member_key
					)
					VALUES
					(
						CAST(@DocketDate AS DATE), 
						1, 
						@TotalGlideboxCommissionAmount, 
						21, 
						'Same day - Glidebox Commission',
						@user, 
						GETDATE(), 
						@DocketKey, 
						@MemberKey
					);
				END

				--Insert into DocketCheckDetail
				INSERT INTO dbo.DocketCheckDetail
                (
                    DocketCheckAmount,
                    DocketCheckSummaryKey,
                    Docket_key,
                    CreatedDate,
                    CreatedByUser,
                    BatchStatus,
                    IsCashable,
                    TerminalID,
                    BatchNumber
                )
                SELECT SUM(Amount),
                       @docketchecksummarykey,
                       Docket_key,
                       GETDATE(),
                       @user,
                       'Valid',
                       1,
                       @TerminalID,
                       @Batchnumber
                FROM dbo.PendingTransactionDetail
                WHERE Docket_key = @docketkey
				GROUP BY Docket_key;

				SELECT @totalamount = SUM(dcd.DocketCheckAmount)
                FROM dbo.DocketCheckSummary dcs
                    INNER JOIN dbo.DocketCheckDetail dcd
                        ON dcd.DocketCheckSummaryKey = dcs.DocketCheckSummaryKey
                WHERE dcs.DocketCheckSummaryKey = @docketchecksummarykey;

				--Update DocketCheckSummary
                UPDATE dbo.DocketCheckSummary
                SET TotalAmount = @totalamount
                WHERE DocketCheckSummaryKey = @docketchecksummarykey;

				FETCH NEXT FROM GlideboxCur INTO @CategoryID, 
												@ShortName,
												@Price, 
												@GlideboxQuantity, 
												@GlideboxCommissionAmount,
												@TotalGlideboxCommissionAmount,
												@IsPercentage
			END

			CLOSE GlideboxCur
			DEALLOCATE GlideboxCur
		END
    END;

    --select Member_key, docketdate, 
    --docket_key, DocketBatchId,CreatedByUser ,DocketCheckSummaryKey,BatchID,
    --memberid,TradingName,state, TerminalId,
    -- FaceValue,
    -- Extras,
    -- DocketTotal,
    -- TotalCommission,
    -- TotalPaid ,
    -- BatchTotalPayable, 
    -- TotalPayable, 
    --CardNumberMasked, BatchNumber, PaymentStatus, GrandTotal, BatchStatus,IsCashable, 
    --PendingTransactionValue, PendingCommision, PaidTransactionValue, PaidCommission, LodgementReferenceNo, BatchTotal, DocketTotalSummary,
    --FaceValueSummary,ExtrasSummary,CommissionSummary,TotalBatchCommission,TotalBatchFaceValue,TotalBatchExtras, CardType, handlingFee, TotalBatchHandlingFee, HandlingFeeSummary,Chargebacks, TotalBatchChargebacks,ChargebacksSummary
    --from GetWebCashingDocketCheckSummary (@docketchecksummarykey)
    ----where docketchecksummarykey = @docketchecksummarykey
    --order by CreatedDate desc, DocketDate

    EXEC spGetWebCashingDocketCheckSummary @docketchecksummarykey;
END;