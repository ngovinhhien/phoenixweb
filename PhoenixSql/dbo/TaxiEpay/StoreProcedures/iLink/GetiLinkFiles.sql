﻿CREATE PROCEDURE [dbo].[GetiLinkFiles]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @FileName VARCHAR(200) = NULL,
    @FileStatus VARCHAR(50) = NULL,
	@FileType int = NULL,
	@SortOrder VARCHAR(20) = NULL,
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT
	
AS
BEGIN
	DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM iLinkFiles f JOIN dbo.iLinkFileType t ON t.ID = f.FileTypeID ',
			@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'

	SET @QueryWhere += CHAR(10) + 'AND f.DownloadedDate >= @FromDate'
	SET @QueryWhere += CHAR(10) + 'AND f.DownloadedDate <= @ToDate'
	IF(ISNULL(@FileName,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND f.Filename LIKE '''+'%' +@FileName+ '%' + ''''
	END
	IF(ISNULL(@FileStatus,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND f.StatusCode = @FileStatus'
	END
	SET @QueryWhere += CHAR(10) + 'AND (f.FileTypeID = @FileType OR @FileType = 0)'

	
	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = 'WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY f.DownloadedDate ' + @SortOrder + ') AS RowId, f.ID, f.Filename, f.FileSize, f.StatusCode, t.Name as FileType, f.URL, f.DownloadedDate, f.TotalTransaction, f.TotalImportedTransaction, f.TotalRejectedTransaction, f.TotalAmountImportedTransaction '
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = 
		N'
			@FromDate DATETIME,
			@ToDate DATETIME,
			@FileName VARCHAR(200) = NULL,
			@FileStatus VARCHAR(50) = NULL,
			@FileType int = NULL,
			@SortOrder VARCHAR(20) = ''DESC'',
			@RowIDFrom INT = 1,
			@RowIDTo INT = 20,
			@TotalItems INT OUTPUT
		'
	EXEC SP_EXECUTESQL @Query, @Params
	, @FromDate = @FromDate
	, @ToDate = @ToDate
	, @FileName = @FileName
	, @FileStatus = @FileStatus
	, @FileType = @FileType
	, @SortOrder = @SortOrder
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT
END