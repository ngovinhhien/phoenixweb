﻿ALTER PROCEDURE [dbo].[spGetWebCashingDocketCheckSummary] @DocketCheckSummaryKey INT
WITH RECOMPILE
AS
BEGIN

    SELECT Member_key,
           DocketDate,
           docket_key,
           DocketBatchId,
           CreatedByUser,
           DocketCheckSummaryKey,
           BatchID,
           MemberId,
           TradingName,
           State,
           TerminalID,
           FaceValue,
           Extras,
           DocketTotal,
           LevyCharge,
           TotalCommission,
           TotalPaid,
           BatchTotalPayable,
           TotalPayable,
           TotalLevyCharge,
           PaidBy,
           PaidDate,
           CardNumberMasked,
           BatchNumber,
           PaymentStatus,
           GrandTotal,
           BatchStatus,
           IsCashable,
           PendingTransactionValue,
           PendingCommision,
           PaidTransactionValue,
           PaidCommission,
           LodgementReferenceNo,
           BatchTotal,
           DocketTotalSummary,
           FaceValueSummary,
           ExtrasSummary,
           CommissionSummary,
           TotalBatchCommission,
           TotalBatchFaceValue,
           TotalBatchExtras,
           CardType,
           HandlingFee,
           TotalBatchHandlingFee,
           HandlingFeeSummary,
           TipPro,
           TipProSummary,
           TippingFee,
           TippingFeeSummary,
           TotalBatchTippingFee,
           Chargebacks,
           TotalBatchChargebacks,
           ChargebacksSummary,
           LevyChargeSummary,
           GrandTotalPaidAmount,
           PaymentType,
		   GlideboxCommission,
		   TotalGlideboxCommission
    FROM
    (
        SELECT dcd.CreatedDate,
               m.Member_key AS Member_key,
               doc.DocketDate,
               doc.LevyCharge,
               ISNULL(doc.Docket_key, 0) AS docket_key,
               ISNULL(dcd.BatchNumber, '') AS DocketBatchId,
               dcs.CreatedByUser,
               dcs.DocketCheckSummaryKey,
               m.MemberId,
               m.TradingName,
               m.State,
               dcd.TerminalID,
               --d.FaceValue, d.Extras, d.DocketTotal, d.TOtalcommission,
               CAST(CASE
                        WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                             OR md.ManualDocket_key IS NOT NULL
                             OR sd.SummaryDocket_key IS NOT NULL THEN
                            ISNULL(doc.FaceValue, 0) - ISNULL(d.AutoTipAmount, 0)
                        ELSE
                            0
                    END AS MONEY) AS FaceValue,
               CAST(CASE
                        WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                             OR md.ManualDocket_key IS NOT NULL
                             OR sd.SummaryDocket_key IS NOT NULL THEN
                            ISNULL(doc.Extras, 0)
                        ELSE
                            0
                    END AS MONEY) AS Extras,
               CAST(CASE
                        WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                             OR md.ManualDocket_key IS NOT NULL
                             OR sd.SummaryDocket_key IS NOT NULL THEN
                            ISNULL(d.AutoTipAmount, 0)
                        ELSE
                            0
                    END AS MONEY) AS TipPro,
               CAST(CASE
                        WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                             OR md.ManualDocket_key IS NOT NULL
                             OR sd.SummaryDocket_key IS NOT NULL THEN
                            ISNULL(doc.FaceValue, 0) + ISNULL(doc.Extras, 0)
                        ELSE
                            0
                    END AS MONEY) AS DocketTotal,
               CAST(CASE
                        WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                             OR md.ManualDocket_key IS NOT NULL
                             OR sd.SummaryDocket_key IS NOT NULL THEN
                            COALESCE(pc.PendingCommision, 0)
                        ELSE
                            0
                    END AS MONEY) AS TotalCommission,
               CASE
                   WHEN ISNULL(d.TotalPending, 0) <> 0 THEN
                       ISNULL(d.TotalPending, 0)
                   WHEN d.TotalPaid <> 0 THEN
                       d.TotalPaid
                   ELSE
                       0
               END AS TotalPaid,
               SUM(ISNULL(dcd.DocketCheckAmount, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS BatchTotalPayable,
               CAST(ISNULL(dcd.DocketCheckAmount, 0) AS DECIMAL(12, 2)) AS TotalPayable,
               d.CardNumberMasked,
               dcd.BatchNumber,
               d.PaymentStatus,
               dcs.TotalAmount AS GrandTotal,
               dcd.BatchStatus,
               dcd.IsCashable,
               pf.PendingTransactionValue,
               pc.PendingCommision,
               tf.PaidTransactionValue,
               tc.PaidCommission,
               d.ReferenceNumber AS LodgementReferenceNo,
               dcd.DockeCheckDetailKey,
               COALESCE(doc.BatchId, dcd.TopBarcode + dcd.BottomBarcode) AS BatchID,
               CASE
                   WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                        OR md.ManualDocket_key IS NOT NULL
                        OR sd.SummaryDocket_key IS NOT NULL THEN
                       SUM(ISNULL(doc.FaceValue, 0) + ISNULL(doc.Extras, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey)
                   ELSE
                       0
               END AS BatchTotal,
               SUM(   CASE
                          WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                               OR md.ManualDocket_key IS NOT NULL
                               OR sd.SummaryDocket_key IS NOT NULL THEN
                              ISNULL(doc.FaceValue, 0) + ISNULL(doc.Extras, 0)
                          ELSE
                              0
                      END
                  ) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS DocketTotalSummary,
               SUM(   CASE
                          WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                               OR md.ManualDocket_key IS NOT NULL
                               OR sd.SummaryDocket_key IS NOT NULL THEN
                              ISNULL(doc.FaceValue, 0) - ISNULL(d.AutoTipAmount, 0)
                          ELSE
                              0
                      END
                  ) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS FaceValueSummary,
               SUM(   CASE
                          WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                               OR md.ManualDocket_key IS NOT NULL
                               OR sd.SummaryDocket_key IS NOT NULL THEN
                              ISNULL(d.AutoTipAmount, 0)
                          ELSE
                              0
                      END
                  ) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS TipProSummary,
               SUM(   CASE
                          WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                               OR md.ManualDocket_key IS NOT NULL
                               OR sd.SummaryDocket_key IS NOT NULL THEN
                              ISNULL(doc.Extras, 0)
                          ELSE
                              0
                      END
                  ) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS ExtrasSummary,
               SUM(   CASE
                          WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                               OR md.ManualDocket_key IS NOT NULL
                               OR sd.SummaryDocket_key IS NOT NULL THEN
                              COALESCE(pc.PendingCommision, 0)
                          ELSE
                              0
                      END
                  ) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS CommissionSummary,
               CASE
                   WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                        OR md.ManualDocket_key IS NOT NULL
                        OR sd.SummaryDocket_key IS NOT NULL THEN
                       SUM(COALESCE(pc.PendingCommision, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey)
                   ELSE
                       0
               END AS TotalBatchCommission,
               CASE
                   WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                        OR md.ManualDocket_key IS NOT NULL
                        OR sd.SummaryDocket_key IS NOT NULL THEN
                       SUM(ISNULL(doc.FaceValue, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey)
                   ELSE
                       0
               END AS TotalBatchFaceValue,
               CASE
                   WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                        OR md.ManualDocket_key IS NOT NULL
                        OR sd.SummaryDocket_key IS NOT NULL THEN
                       SUM(ISNULL(doc.Extras, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey)
                   ELSE
                       0
               END AS TotalBatchExtras,
               dpm.Name AS CardType,
               ISNULL(vhf.HandlingFee, 0) AS HandlingFee,
               SUM(ISNULL(vhf.HandlingFee, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS TotalBatchHandlingFee,
               SUM(ISNULL(vhf.HandlingFee, 0)) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS HandlingFeeSummary,
               dcd.Description,
               tm.MemberId AS TransactionMemberID,
               tm.TradingName AS TransactionMemberName,
               tf.PaidBy,
               tf.PaidDate,
               SUM((ISNULL(tf.PaidTransactionValue, 0) + ISNULL(tc.PaidCommission, 0))) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS GrandTotalPaidAmount,
               tf.PaymentType,
               ISNULL(vtf.TippingFee, 0) AS TippingFee,
               SUM(ISNULL(vtf.TippingFee, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS TotalBatchTippingFee,
               --cast(case when abs(isnull(d.TotalPending,0)) >0 or md.ManualDocket_key is not null or sd.SummaryDocket_key is not null  then coalesce( lc.LevyCharge,0) else 0 end as money) as TotalLevyCharge,
               SUM(ISNULL(lc.LevyCharge, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS TotalLevyCharge,
               SUM(ISNULL(vtf.TippingFee, 0)) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS TippingFeeSummary,
               ISNULL(vcb.Chargebacks, 0) AS Chargebacks,
               SUM(ISNULL(vcb.Chargebacks, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey) AS TotalBatchChargebacks,
               SUM(ISNULL(vtf.TippingFee, 0)) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS ChargebacksSummary,
               SUM(ISNULL(lc.LevyCharge, 0)) OVER (PARTITION BY dcs.DocketCheckSummaryKey) AS LevyChargeSummary,
			   CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                            OR md.ManualDocket_key IS NOT NULL
                            OR sd.SummaryDocket_key IS NOT NULL 
							OR gsdc.DocketKey IS NOT NULL 
							THEN
								ISNULL(gc.GlideboxCommission, 0)
                    ELSE
                        0
                END AS DECIMAL(10,2)) AS GlideboxCommission,
			CASE
                WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                    OR md.ManualDocket_key IS NOT NULL
                    OR sd.SummaryDocket_key IS NOT NULL 
					OR gsdc.DocketKey IS NOT NULL 
					THEN
						SUM(ISNULL(gc.GlideboxCommission, 0)) OVER (PARTITION BY doc.BatchId, m.Member_key, dcs.DocketCheckSummaryKey)
                ELSE
                    0
            END AS TotalGlideboxCommission
        FROM DocketCheckSummary dcs
            LEFT JOIN DocketCheckDetail dcd
                ON dcs.DocketCheckSummaryKey = dcd.DocketCheckSummaryKey
            LEFT JOIN DocketTable doc
                ON doc.Docket_key = dcd.Docket_key
            LEFT JOIN DocketPayMethodLookupTable dpm
                ON dpm.DocketPayMethod_key = doc.DocketPayMethod_key
            LEFT JOIN vEftDocket d
                ON d.Docket_key = doc.Docket_key
            LEFT JOIN ManualDocket md
                ON md.ManualDocket_key = doc.Docket_key
            LEFT JOIN SummaryDocket sd
                ON sd.SummaryDocket_key = doc.Docket_key
			LEFT JOIN GlideboxSameDayCashing gsdc
                ON gsdc.DocketKey = doc.Docket_key
            LEFT JOIN Operator o
                ON ISNULL(dcs.AuthorisedMemberKey, dcs.MemberKey) = o.Operator_key
            LEFT JOIN Member m
                ON (m.Member_key = ISNULL(dcs.AuthorisedMemberKey, dcs.MemberKey)) -- or (m.Member_key = md.Member_key) or (m.member_key = sd.Member_key)
            LEFT JOIN Member tm
                ON tm.Member_key = dcs.MemberKey
            LEFT JOIN EftTerminal t
                ON t.EftTerminal_key = d.EftTerminal_key
            OUTER APPLY
        (
            SELECT SUM(Amount) AS PendingTransactionValue
            FROM PendingTransactionDetail pf
            WHERE pf.Docket_key = doc.Docket_key
                  AND pf.Member_key = m.Member_key
                  AND pf.TransactionDetailType = 0
            GROUP BY pf.Docket_key,
                     pf.Member_key
        ) pf
            OUTER APPLY
        (
            SELECT SUM(Amount) AS PendingCommision
            FROM PendingTransactionDetail pc
            WHERE pc.Docket_key = doc.Docket_key
                  AND pc.Member_key = m.Member_key
                  AND
                  (
                      pc.TransactionDetailType IN ( 1 )
                      OR
                      (
                          pc.TransactionDetailType IN ( 2 )
                          AND ISNULL(m.EOMMSFPayment, 0) = 0
                      )
                  )
            GROUP BY pc.Docket_key,
                     pc.Member_key
        ) pc
            OUTER APPLY
        (
            SELECT SUM(Amount) AS PaidTransactionValue,
                   MIN(th.TransactionDate) AS PaidDate,
                   MIN(th.ModifiedByUser) AS PaidBy,
                   th.LodgementReferenceNo AS ReferenceNo,
                   te.Name AS PaymentType
            FROM TransactionDetail tf
                INNER JOIN [Transaction] th
                    ON th.Transaction_key = tf.Transaction_key
                INNER JOIN TransactionType te
                    ON te.TransactionType_key = th.TransactionType_key
            --left join PayRun pr on pr.PayRun_key = th.PayRun_key
            WHERE tf.Docket_key = doc.Docket_key
                  AND tf.Member_key = d.Operator_key
                  AND tf.TransactionDetailType = 0
            GROUP BY tf.Docket_key,
                     tf.Member_key,
                     th.Transaction_key,
                     th.LodgementReferenceNo,
                     te.Name
        ) tf --paid Total Fare
            OUTER APPLY
        (
            SELECT SUM(Amount) AS PaidCommission
            FROM TransactionDetail tc
            WHERE tc.Docket_key = doc.Docket_key
                  AND tc.Member_key = m.Member_key
                  AND tc.TransactionDetailType IN ( 1, 2 )
            GROUP BY tc.Docket_key,
                     tc.Member_key
        ) tc --paid Commission
            OUTER APPLY
        (
            SELECT SUM(Amount) AS HandlingFee
            FROM PendingTransactionDetail pc
            WHERE pc.Docket_key = doc.Docket_key
                  AND pc.Member_key = m.Member_key
                  AND pc.TransactionDetailType IN ( 5 )
            GROUP BY pc.Docket_key,
                     pc.Member_key
        ) vhf --handling fee
            OUTER APPLY
        (
            SELECT SUM(Amount) AS TippingFee
            FROM PendingTransactionDetail pc
            WHERE pc.Docket_key = doc.Docket_key
                  AND pc.Member_key = m.Member_key
                  AND pc.TransactionDetailType IN ( 14 )
            GROUP BY pc.Docket_key,
                     pc.Member_key
        ) vtf --Auto Tip Fee
            OUTER APPLY
        (
            SELECT SUM(Amount) AS Chargebacks
            FROM PendingTransactionDetail pc
            WHERE pc.Docket_key = doc.Docket_key
                  AND pc.Member_key = m.Member_key
                  AND pc.TransactionDetailType IN ( 7 )
            GROUP BY pc.Docket_key,
                     pc.Member_key
        ) vcb -- chargebacks

            OUTER APPLY
        (
            SELECT SUM(ISNULL(Amount, 0)) AS LevyCharge
            FROM PendingTransactionDetail pc
            WHERE pc.Docket_key = doc.Docket_key
                  AND pc.Member_key = m.Member_key
                  AND pc.TransactionDetailType IN ( 18 )
            GROUP BY pc.Docket_key,
                     pc.Member_key
        ) lc -- chargebacks
		OUTER APPLY
		(
			SELECT SUM(ISNULL(Amount, 0)) AS GlideboxCommission
			FROM dbo.PendingTransactionDetail gc
			WHERE gc.Docket_key = doc.Docket_key
					AND gc.Member_key = m.Member_key
					AND gc.TransactionDetailType IN ( 21 )
			GROUP BY gc.Docket_key,
					gc.Member_key
		) gc -- glidebox commission
        WHERE dcs.DocketCheckSummaryKey = @DocketCheckSummaryKey
    ) a
    ORDER BY CreatedDate DESC,
             DocketDate;

END;