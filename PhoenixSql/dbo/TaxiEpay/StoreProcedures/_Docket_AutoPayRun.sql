﻿ALTER PROCEDURE [dbo].[_Docket_AutoPayRun]
    @Company_key INT = 0,
    @MSFPayRun BIT = 0,
    @DealerCommission BIT = 0,
    @payrundate DATE = NULL,
    @vehiclekey INT = 0,
    @member_key INT = 0
AS
BEGIN
    DECLARE @trancount INT;
    DECLARE @memberKey INT;
    DECLARE @transactionkey INT = 0;
    DECLARE @sumamount DECIMAL(10, 2) = 0.0;
    DECLARE @payrunkey INT = 0;
    DECLARE @accountkey INT = 0;
    DECLARE @memcounter INT = 0;
    DECLARE @Refno NVARCHAR(50) = N'';
    DECLARE @TranDesc NVARCHAR(100) = N'';
    DECLARE @Prefix NVARCHAR(1);
    DECLARE @PayrunReportType NVARCHAR(1);
    DECLARE @memberid NVARCHAR(10);
    DECLARE @payrunID NVARCHAR(20);
    DECLARE @currentdate DATETIME = GETDATE();
    DECLARE @temptable TABLE
    (
        memberKey INT,
        docketKey INT,
        pendingtransactionkey INT,
        TransactionDetailType INT,
        amount DECIMAL(10, 2),
        accountKey INT,
        memberID NVARCHAR(10)
    );

    IF @MSFPayRun = 1
    BEGIN
        SET @TranDesc = N'Merchant Service Fee Charge';
        SET @Prefix = N'M';
        SET @PayrunReportType = N'O';
    END;
    ELSE IF @DealerCommission = 1
    BEGIN
        SET @TranDesc = N'Dealer Commission Payment';
        SET @Prefix = N'D';
        SET @PayrunReportType = N'D';
    END;
    ELSE
    BEGIN
        SET @Prefix = N'P';
        SET @TranDesc = N'Transaction Payment';
        SET @PayrunReportType = N'O';
    END;

    IF @payrundate IS NULL
    BEGIN
        SET @payrundate = CAST(GETDATE() AS DATE);
    END;

    DECLARE @IdentityOutput TABLE
    (
        ID INT,
        ReferenceNo NVARCHAR(50)
    );



    INSERT INTO @temptable
    (
        memberKey,
        docketKey,
        pendingtransactionkey,
        TransactionDetailType,
        amount,
        accountKey,
        memberID
    )
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    --memberKey,docketKey, pendingtransactionkey,TransactionDetailType, amount
    FROM dbo.PreparePayrun pp
        INNER JOIN PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN EftDocket e
            ON e.EftDocket_key = p.Docket_key
               AND ISNULL(e.Quarantined, 0) = 0
        INNER JOIN Vehicle v
            ON v.Vehicle_key = e.Vehicle_key
               AND
               (
                   v.VehicleType_key = @vehiclekey
                   OR @vehiclekey = 0
               )
        INNER JOIN Account a
            ON a.Member_key = p.Member_key --and a.Weight> 0
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
               AND
               (
                   m.Member_key = @member_key
                   OR @member_key = 0
               )
        INNER JOIN Operator o
            ON o.Operator_key = m.Member_key
               AND
               (
                   (
                       o.PayByEFT = 1
                       AND o.AllowCash = 0
                   )
                   OR
                   (
                       o.AllowCash = 1
                       AND o.PayByEFT = 0
                       AND m.EOMMSFPayment = 1
                   )
               )
        LEFT JOIN MemberPaymentSchedule s
            ON s.MemberPaymentSchedule_key = pp.MemberPaymentSchedule_key
               AND ISNULL(s.InActive, 0) = 0
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
          AND
          (
              (
                  @MSFPayRun = 0
                  AND ISNULL(EOMMSFPayment, 0) = 0
                  AND @DealerCommission = 0
                  AND s.MemberPaymentSchedule_key IS NOT NULL
              )
              OR
              (
                  @MSFPayRun = 0
                  AND ISNULL(EOMMSFPayment, 0) = 1
                  AND @DealerCommission = 0
                  AND
                  (
                      (
                          p.TransactionDetailType <> 2
                          AND p.TransactionDetailType <> 3
                      )
                      OR
                      (
                          p.TransactionDetailType = 7
                          AND p.Description <> 'Charge Back:Operator Commission'
                      )
                  )
              )
              OR
              (
                  @MSFPayRun = 1
                  AND ISNULL(EOMMSFPayment, 0) = 1
                  AND @DealerCommission = 0
                  AND
                  (
                      p.TransactionDetailType = 2
                      OR
                      (
                          p.TransactionDetailType = 7
                          AND p.Description = 'Charge Back:Operator Commission'
                      )
                  )
              )
          )
    UNION
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    FROM dbo.PreparePayrun pp
        INNER JOIN PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN Deposit dep
            ON dep.DepositKey = p.Docket_key
        INNER JOIN Account a
            ON a.Member_key = p.Member_key --and a.Weight> 0
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
               AND
               (
                   m.Member_key = @member_key
                   OR @member_key = 0
               )
        INNER JOIN Operator o
            ON o.Operator_key = m.Member_key
               AND
               (
                   (
                       o.PayByEFT = 1
                       AND o.AllowCash = 0
                   )
                   OR
                   (
                       o.AllowCash = 1
                       AND o.PayByEFT = 0
                       AND m.EOMMSFPayment = 1
                   )
               )
        INNER JOIN MemberPaymentSchedule s
            ON s.MemberPaymentSchedule_key = pp.MemberPaymentSchedule_key
               AND ISNULL(s.InActive, 0) = 0
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
          AND @MSFPayRun = 0
    UNION
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    FROM dbo.PreparePayrun pp
        INNER JOIN PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN EftDocket e
            ON e.EftDocket_key = p.Docket_key
               AND ISNULL(e.Quarantined, 0) = 0
        INNER JOIN Vehicle v
            ON v.Vehicle_key = e.Vehicle_key
               AND
               (
                   v.VehicleType_key = @vehiclekey
                   OR @vehiclekey = 0
               )
        INNER JOIN Account a
            ON a.Member_key = p.Member_key --and a.Weight> 0
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
               AND
               (
                   m.Member_key = @member_key
                   OR @member_key = 0
               )
        INNER JOIN Dealer d
            ON d.Dealer_key = m.Member_key
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
          AND @DealerCommission = 1
          AND p.TransactionDetailType IN ( 3, 7 )
    UNION
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    FROM dbo.PreparePayrun pp
        INNER JOIN PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN DocketTable doc
            ON doc.Docket_key = p.Docket_key
        INNER JOIN MemberChargeHistory mch
            ON mch.MemberChargeHistory_key = doc.Docket_key
        INNER JOIN Account a
            ON a.Member_key = p.Member_key --and a.Weight> 0
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
               AND
               (
                   m.Member_key = @member_key
                   OR @member_key = 0
               )
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
          AND p.TransactionDetailType IN
              (
                  SELECT TransactionDetailType FROM MemberChargeType
              )

    --Glidebox commission											
    UNION
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    FROM dbo.PreparePayrun pp
        INNER JOIN dbo.PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN dbo.Account a
            ON a.Member_key = p.Member_key
        INNER JOIN dbo.Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
               AND
               (
                   m.Member_key = @member_key
                   OR @member_key = 0
               )
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
		  AND p.TransactionDetailType IN (21,22) --21: Glidebox commission | 22: Glidebox commission: Charge back
    ORDER BY Member_key,
             Docket_key,
             TransactionDetailType;

    SELECT @trancount = COUNT(t.pendingtransactionkey)
    FROM @temptable t;
    DECLARE @MemberCursor CURSOR;
    IF @trancount > 0
    BEGIN

        SET @payrunID
            = @Prefix + RIGHT(CAST(DATEPART(YEAR, @currentdate) AS NVARCHAR(4)), 2)
              + RIGHT('00' + CAST(DATEPART(MONTH, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(DAY, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(HOUR, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(MINUTE, @currentdate) AS NVARCHAR), 2);

        SELECT @Refno = PayRunId
        FROM PayRun
        WHERE PayRunId = @payrunID;

        --make sure the Reference number doesn't exist
        WHILE ISNULL(@Refno, '') <> ''
        BEGIN
            --add one minute to the Reference Number if reference number already exists
            SET @currentdate = DATEADD(MINUTE, 1, @currentdate);
            SET @payrunID
                = @Prefix + RIGHT(CAST(DATEPART(YEAR, @currentdate) AS NVARCHAR(4)), 2)
                  + RIGHT('00' + CAST(DATEPART(MONTH, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(DAY, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(HOUR, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(MINUTE, @currentdate) AS NVARCHAR), 2);

            SELECT @Refno = PayRunId
            FROM PayRun
            WHERE PayRunId = @payrunID;

        END;


        --insert into PayRunAuto (PayrunID, PayrunDate, Comments, Modified_dttm, ModifiedByUser)
        INSERT INTO PayRun
        (
            PayRunId,
            PayRunDate,
            Comments,
            Modified_dttm,
            ModifiedByUser,
            PayrunType
        )
        OUTPUT inserted.PayRun_key,
               inserted.PayRunId
        INTO @IdentityOutput
        VALUES
        (
            --@Prefix
            --+ right(cast(datepart(year, getdate())as nvarchar(4)) , 2)
            --+ right('00'+cast(datepart(month, getdate())as nvarchar),2)+ right('00'+cast(datepart(day, getdate())as nvarchar),2)
            --+ right('00'+cast(datepart(hour, getdate())as nvarchar),2)+ right('00'+cast(datepart(minute, getdate())as nvarchar),2),
            @payrunID, CAST(GETDATE() AS DATE), '', GETDATE(), 'Auto Payrun', 1);

        SELECT @payrunkey =
        (
            SELECT ID FROM @IdentityOutput
        );

        SELECT @Refno =
        (
            SELECT ReferenceNo FROM @IdentityOutput
        );

        --insert a new record into payrun report control table so that the operator pay run report will pick up and email report to operator.	
        INSERT INTO PayRunReportControl
        (
            PayRunId,
            PayRunType
        )
        VALUES
        (@Refno, @PayrunReportType);

        SET @MemberCursor = CURSOR FOR
        SELECT memberKey,
               accountKey,
               CAST(SUM(amount) AS DECIMAL(10, 2)) AS amount,
               t.memberID
        FROM @temptable t
        --where t.memberKey in (818,819)
        GROUP BY memberKey,
                 accountKey,
                 t.memberID
        --having cast(abs(Sum(amount)) as decimal(10,2))>0
        ORDER BY memberKey,
                 accountKey;

        OPEN @MemberCursor;

        FETCH NEXT FROM @MemberCursor
        INTO @memberKey,
             @accountkey,
             @sumamount,
             @memberid;

        WHILE @@fetch_status = 0 --for each member and account
        BEGIN
            SET @memcounter = @memcounter + 1;
            --insert into transaction table
            DELETE FROM @IdentityOutput
            WHERE ID = @payrunkey;
            BEGIN TRAN; --start transaction 
            --insert into [TransactionAuto] (TransactionDate, TotalAmount, LodgementReferenceNo, Status, ModifiedByUser, Modified_dttm, Office_key, TransactionType_key, PayRun_key, Member_key, Account_key)
            INSERT INTO [Transaction]
            (
                TransactionDate,
                TotalAmount,
                LodgementReferenceNo,
                Status,
                ModifiedByUser,
                Modified_dttm,
                Office_key,
                TransactionType_key,
                PayRun_key,
                Member_key,
                Account_key,
                Description
            )
            OUTPUT inserted.Transaction_key,
                   inserted.LodgementReferenceNo
            INTO @IdentityOutput
            VALUES
            (   GETDATE(), @sumamount, @Refno + '/' + @memberid, --cast(@memcounter as nvarchar(20))
                'Paid', 'Auto Payrun', GETDATE(), 51, 2, @payrunkey, @memberKey, @accountkey, @TranDesc);
            --from @tempSelectedRecords t

            SELECT @transactionkey =
            (
                SELECT ID FROM @IdentityOutput
            );

            --insert to transactiondetail table
            --insert into [TransactionDetailAuto] (Transaction_key, ItemDate, RateUsed, Amount, TransactionDetailType, Description, Modified_dttm, ModifiedByUser, Docket_key, Member_key)
            INSERT INTO [TransactionDetail]
            (
                Transaction_key,
                ItemDate,
                RateUsed,
                Amount,
                TransactionDetailType,
                Description,
                Modified_dttm,
                ModifiedByUser,
                Docket_key,
                Member_key
            )
            SELECT @transactionkey,
                   p.ItemDate,
                   p.RateUsed,
                   p.Amount,
                   p.TransactionDetailType,
                   p.Description,
                   GETDATE(),
                   'Auto Payrun',
                   p.Docket_key,
                   p.Member_key
            FROM @temptable t
                --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key= t.pendingtransactionkey
                INNER JOIN PendingTransactionDetail p
                    ON p.PendingTransactionDetail_key = t.pendingtransactionkey
            WHERE t.memberKey = @memberKey
                  AND t.accountKey = @accountkey;

            UPDATE MemberPaymentSchedule
            SET LastRunTime = @currentdate
            WHERE MemberPaymentSchedule_key IN
                  (
                      SELECT DISTINCT
                             pp.MemberPaymentSchedule_key
                      FROM PreparePayrun pp
                          --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
                          INNER JOIN PendingTransactionDetail p
                              ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
                      WHERE p.Member_key = @memberKey
                            AND Account_key = @accountkey
                  );


            DELETE FROM PreparePayrun
            WHERE PreparePayrun_key IN
                  (
                      SELECT pp.PreparePayrun_key
                      FROM PreparePayrun pp
                          INNER JOIN @temptable t
                              ON t.pendingtransactionkey = pp.PendingTransactionDetail_key
                      --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
                      --inner join PendingTransactionDetail p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key  and p.PendingTransactionDetail_key = t.pendingtransactionkey
                      WHERE t.memberKey = @memberKey
                            AND t.accountKey = @accountkey
                  );

            -- delete from pendingtransactiondetail table
            --delete from PendingTransactionDetailAuto where PendingTransactionDetail_key in (
            DELETE FROM PendingTransactionDetail
            WHERE PendingTransactionDetail_key IN
                  (
                      SELECT pendingtransactionkey
                      FROM @temptable t
                      WHERE t.memberKey = @memberKey
                            AND t.accountKey = @accountkey
                  );

            DELETE FROM @temptable
            WHERE memberKey = @memberKey
                  AND accountKey = @accountkey;




            DELETE FROM @IdentityOutput
            WHERE ID = @transactionkey;
            --delete from @tempSelectedRecords -- remove everything from @tempSelectedRecords, ready for the next account.
            IF @@ERROR <> 0
               AND @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;
            ELSE
                COMMIT TRAN;

            FETCH NEXT FROM @MemberCursor
            INTO @memberKey,
                 @accountkey,
                 @sumamount,
                 @memberid;
        --end --accountcursor
        END; --membercursor
    END;
END;