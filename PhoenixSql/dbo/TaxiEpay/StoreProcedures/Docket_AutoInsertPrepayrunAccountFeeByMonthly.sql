﻿CREATE PROCEDURE [dbo].[usp_BatchJob_Docket_AutoInsertPrepayrunAccountFeeByMonthly]
    @PayRunDate DATE = NULL,
    @AccountFeeMonth INT = NULL,
    @AccountFeeYear INT = NULL
AS
BEGIN
    IF @PayRunDate IS NULL
    BEGIN
        SET @PayRunDate = CAST(GETDATE() AS DATE);
    END;

    IF @AccountFeeMonth IS NULL
    BEGIN
        SET @AccountFeeMonth = DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE()));
    END;

    IF @AccountFeeYear IS NULL
    BEGIN
        SET @AccountFeeYear = DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE()));
    END;

    DECLARE @TempTable TABLE
    (
        MemberKey INT NOT NULL,
        DocketKey INT NOT NULL,
        PendingTransactionKey INT NOT NULL,
        TransactionDetailType INT NOT NULL,
        Amount DECIMAL(18, 2) NOT NULL,
        MemberPaymentSchedule_key INT NULL
    );

    INSERT INTO @TempTable
    (
        MemberKey,
        DocketKey,
        PendingTransactionKey,
        TransactionDetailType,
        Amount,
        MemberPaymentSchedule_key
    )
    (SELECT DISTINCT
            p.Member_key,
            h.DocketKey,
            p.PendingTransactionDetail_key,
            p.TransactionDetailType,
            p.Amount,
            NULL
     FROM dbo.FeeChargeHistory h
         INNER JOIN dbo.FeeChargeDetail d
             ON h.FeeChargeDetailId = d.ID
                AND d.FrequencyTypeID = 1
         INNER JOIN dbo.PendingTransactionDetail p
             ON h.DocketKey = p.Docket_key
                AND p.TransactionDetailType IN ( 19 )
         INNER JOIN dbo.Account ac
             ON ac.Member_key = p.Member_key
                AND ac.Weight > 0
         LEFT JOIN dbo.PreparePayrun pp
             ON pp.PendingTransactionDetail_key = p.PendingTransactionDetail_key
                AND pp.PayRunDate = @PayRunDate
     WHERE MONTH(h.StartDate) = @AccountFeeMonth
           AND YEAR(h.StartDate) = @AccountFeeYear
           AND pp.PreparePayrun_key IS NULL
     UNION
     SELECT DISTINCT
            p.Member_key,
            fcr.DocketKey,
            p.PendingTransactionDetail_key,
            p.TransactionDetailType,
            p.Amount,
            NULL
     FROM dbo.FeeChargedRefund fcr
         INNER JOIN dbo.PendingTransactionDetail p
             ON fcr.DocketKey = p.Docket_key
                AND p.TransactionDetailType IN ( 20 )
         INNER JOIN dbo.Account ac
             ON ac.Member_key = p.Member_key
                AND ac.Weight > 0
         LEFT JOIN dbo.PreparePayrun pp
             ON pp.PendingTransactionDetail_key = p.PendingTransactionDetail_key
     WHERE pp.PreparePayrun_key IS NULL);

    DECLARE @TranCount INT;

    SELECT @TranCount = COUNT(t.PendingTransactionKey)
    FROM @TempTable t;

    DECLARE @MemberKey INT,
            @SumAmount MONEY,
            @AccountKey INT,
            @Weight REAL,
            @TransactionAmount MONEY;

    DECLARE @DocketCursor CURSOR;
    DECLARE @MemberCursor CURSOR;
    DECLARE @AccountCursor CURSOR;

    IF @TranCount > 0
    BEGIN
        SET @MemberCursor = CURSOR READ_ONLY FOR
        SELECT MemberKey,
               SUM(Amount)
        FROM @TempTable t
        GROUP BY MemberKey;

        OPEN @MemberCursor;

        FETCH NEXT FROM @MemberCursor
        INTO @MemberKey,
             @SumAmount;

        WHILE @@FETCH_STATUS = 0 --for each member
        BEGIN
            DECLARE @TOTALLIMIT MONEY;
            DECLARE @AccountCount INT;
            DECLARE @Accounter INT;
            --get bank account details
            SET @AccountKey = NULL;
            SET @Weight = NULL;
            SET @AccountCount = 0;
            SET @Accounter = 0;

            SET @AccountCursor = CURSOR READ_ONLY FOR
            SELECT TOP 1
                   ISNULL(mac.Account_key, pac.Account_key) AS AccountKey,
                   100,
                   1 AS AccountCount
            FROM dbo.Member m
                OUTER APPLY
            (
                SELECT *
                FROM dbo.Account acc
                    INNER JOIN dbo.AccountPurposeType apt
                        ON apt.AccountKey = acc.Account_key
                WHERE acc.Member_key = m.Member_key
                      AND apt.PurposeTypeID = 1
            ) pac
                OUTER APPLY
            (
                SELECT *
                FROM dbo.Account acc
                    INNER JOIN dbo.AccountPurposeType apt
                        ON apt.AccountKey = acc.Account_key
                WHERE acc.Member_key = m.Member_key
                      AND apt.PurposeTypeID = 3
            ) mac
            WHERE m.Member_key = @MemberKey;

            OPEN @AccountCursor;
            SET @TransactionAmount = 0.0;

            FETCH NEXT FROM @AccountCursor
            INTO @AccountKey,
                 @Weight,
                 @AccountCount;

            IF @AccountKey IS NOT NULL
            BEGIN
                SET @TOTALLIMIT = CAST((@SumAmount * @Weight / 100) AS MONEY);
                SET @Accounter = @Accounter + 1;

                DECLARE @DocketAmount MONEY;
                DECLARE @DocketKey INT;

                SET @DocketCursor = CURSOR READ_ONLY FOR
                SELECT DocketKey,
                       SUM(Amount) total
                FROM @TempTable
                WHERE MemberKey = @MemberKey
                GROUP BY DocketKey
                ORDER BY SUM(Amount),
                         DocketKey;

                OPEN @DocketCursor;

                FETCH NEXT FROM @DocketCursor
                INTO @DocketKey,
                     @DocketAmount;

                WHILE @@FETCH_STATUS = 0
                BEGIN
                    INSERT INTO dbo.PreparePayrun
                    (
                        PendingTransactionDetail_key,
                        Account_key,
                        PayRunDate,
                        MemberPaymentSchedule_key
                    )
                    SELECT PendingTransactionKey,
                           @AccountKey,
                           CAST(GETDATE() AS DATE),
                           p.MemberPaymentSchedule_key
                    FROM @TempTable p
                    WHERE p.DocketKey = @DocketKey
                          AND MemberKey = @MemberKey;

                    SELECT @TransactionAmount = @TransactionAmount + SUM(p.Amount)
                    FROM @TempTable p
                    WHERE p.DocketKey = @DocketKey
                          AND MemberKey = @MemberKey;

                    UPDATE dbo.FeeChargedRefund
                    SET ProcessedDate = GETDATE()
                    WHERE DocketKey = @DocketKey;

                    IF @TransactionAmount > @TOTALLIMIT
                       AND @Accounter < @AccountCount
                    -- if transaction total greater than limit of that account and there is more account, fetch another account
                    BEGIN
                        FETCH NEXT FROM @AccountCursor
                        INTO @AccountKey,
                             @Weight,
                             @AccountCount;

                        SET @TOTALLIMIT = CAST((@SumAmount * @Weight / 100) AS MONEY);
                        SET @Accounter = @Accounter + 1;
                        SET @TransactionAmount = 0;
                    END;

                    FETCH NEXT FROM @DocketCursor
                    INTO @DocketKey,
                         @DocketAmount;
                END;
                CLOSE @DocketCursor;
                DEALLOCATE @DocketCursor;
            END;

            CLOSE @AccountCursor;
            DEALLOCATE @AccountCursor;

            FETCH NEXT FROM @MemberCursor
            INTO @MemberKey,
                 @SumAmount;

        END; --MemberCursor
        CLOSE @MemberCursor;
        DEALLOCATE @MemberCursor;
    END;
END;