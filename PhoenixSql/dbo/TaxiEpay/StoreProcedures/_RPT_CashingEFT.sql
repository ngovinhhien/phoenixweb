﻿ALTER Procedure [dbo].[_RPT_CashingEFT] (@refNumber varchar(50))
AS
BEGIN
	SET NOCOUNT ON
	SELECT    
	detail.LodgementReferenceNo, 
	detail.TerminalId, 
	detail.VehicleId, 
	detail.BatchNumber, 
	detail.PayMethodName, 
	detail.DocketDate, 
	detail.Fare AS Fare1, 
	detail.Extras AS Extras1, 
	detail.FareReimbursement, 
	detail.CommissionRate,
	detail.Commissions, 
	detail.TotalPaid, 
	CASE FareReimbursement WHEN 0 THEN 0 ELSE Fare END AS Fare, 
	CASE FareReimbursement WHEN 0 THEN 0 ELSE Extras END AS Extras, 
	detail.FirstName, 
	detail.LastName, 
	detail.MemberId, 
	detail.TradingName, 
	detail.RegisteredState, 
	ISNULL(detail.TradingName, '') AS OperatorName, 
	detail.Other, 
	detail.Charges, 
	detail.Payments,
	CadmusIN.driverid,
	Company_key,
	detail.AutoTippingFee, detail.AutoTipAmount,
	State,
	detail.Chargebacks,
	detail.ChargebacksFee,
	detail.levyCharge,
	detail.GlideboxCommission
	FROM         vCashingReportEFT AS detail LEFT OUTER JOIN
	CadmusIN ON CadmusIN.docket_key = detail.Docket_key
	
	WHERE     (detail.LodgementReferenceNo = @RefNumber) 
END