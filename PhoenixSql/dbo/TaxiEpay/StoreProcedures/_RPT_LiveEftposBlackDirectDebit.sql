﻿ALTER PROCEDURE [dbo].[_RPT_LiveEftposBlackDirectDebit]
(
    @PayRunId VARCHAR(50),
    @MemberKey VARCHAR(50) = 'ALL'
)
AS
BEGIN

    SELECT DISTINCT
           PayRun.PayRunId,
           PayRun.PayRunDate,
           a.TerminalId,
           d.BatchId AS EftDocketBatchId,
           d.DocketDate,
           p.Amount TotalCharge,
           PayRun.PayRun_key,
           p.Amount Charge,
           vOperators.FirstName,
           vOperators.LastName,
           vOperators.MemberId,
           vOperators.TradingName,
           vOperators.State AS RegisteredState,
           ISNULL(vOperators.TradingName, '') AS OperatorName,
           0 Other,
           p.Amount Charges,
           p.Description + ' ' + CAST(trh.RentMonth AS VARCHAR(2)) + '/' + CAST(trh.RentYear AS VARCHAR(4)) AS TranDescription
    FROM [Transaction] AS h
        INNER JOIN TransactionDetail p
            ON p.Transaction_key = h.Transaction_key
        INNER JOIN PayRun
            ON h.PayRun_key = PayRun.PayRun_key
        INNER JOIN vOperators
            ON h.Member_key = vOperators.Operator_key
        INNER JOIN TerminalRentalHistory trh
            ON trh.TerminalRentalHistory_key = p.Docket_key
        INNER JOIN DocketTable d
            ON d.Docket_key = trh.TerminalRentalHistory_key
        INNER JOIN vTerminalAllocations a
            ON a.TerminalAllocation_key = trh.TerminalAllocation_key
    WHERE (PayRun.PayRunId = @PayRunId)
          AND
          (
              (h.Member_key = @MemberKey)
              OR (@MemberKey = 'ALL')
          )
          AND vOperators.Company_key IN (22,27)
    UNION
    SELECT DISTINCT
           PayRun.PayRunId,
           PayRun.PayRunDate,
           NULL,
           d.BatchId AS EftDocketBatchId,
           d.DocketDate,
           p.Amount TotalCharge,
           PayRun.PayRun_key,
           p.Amount Charge,
           vOperators.FirstName,
           vOperators.LastName,
           vOperators.MemberId,
           vOperators.TradingName,
           vOperators.State AS RegisteredState,
           ISNULL(vOperators.TradingName, '') AS OperatorName,
           0 Other,
           p.Amount Charges,
           p.Description + ' ' + CAST(MONTH(StartDate) AS VARCHAR(2)) + '/' + CAST(YEAR(StartDate) AS VARCHAR(4)) AS TranDescription
    FROM [Transaction] AS h
        INNER JOIN TransactionDetail p
            ON p.Transaction_key = h.Transaction_key
        INNER JOIN PayRun
            ON h.PayRun_key = PayRun.PayRun_key
        INNER JOIN vOperators
            ON h.Member_key = vOperators.Operator_key
        INNER JOIN dbo.FeeChargeHistory trh
            ON trh.DocketKey = p.Docket_key
        INNER JOIN DocketTable d
            ON d.Docket_key = trh.DocketKey
    WHERE (PayRun.PayRunId = @PayRunId)
          AND
          (
              (h.Member_key = @MemberKey)
              OR (@MemberKey = 'ALL')
          )
          AND vOperators.Company_key IN (22,27)
    UNION
    SELECT DISTINCT
           PayRun.PayRunId,
           PayRun.PayRunDate,
           NULL,
           d.BatchId AS EftDocketBatchId,
           d.DocketDate,
           p.Amount TotalCharge,
           PayRun.PayRun_key,
           p.Amount Charge,
           vOperators.FirstName,
           vOperators.LastName,
           vOperators.MemberId,
           vOperators.TradingName,
           vOperators.State AS RegisteredState,
           ISNULL(vOperators.TradingName, '') AS OperatorName,
           0 Other,
           p.Amount Charges,
           p.Description + ' ' + CAST(MONTH(StartDate) AS VARCHAR(2)) + '/' + CAST(YEAR(StartDate) AS VARCHAR(4)) AS TranDescription
    FROM [Transaction] AS h
        INNER JOIN TransactionDetail p
            ON p.Transaction_key = h.Transaction_key
        INNER JOIN PayRun
            ON h.PayRun_key = PayRun.PayRun_key
        INNER JOIN vOperators
            ON h.Member_key = vOperators.Operator_key
        INNER JOIN dbo.FeeChargedRefund trh
            ON trh.DocketKey = p.Docket_key
			INNER JOIN dbo.FeeChargeHistory ON FeeChargeHistory.ID = trh.FeeChargeHistoryID
        INNER JOIN DocketTable d
            ON d.Docket_key = trh.DocketKey
    WHERE (PayRun.PayRunId = @PayRunId)
          AND
          (
              (h.Member_key = @MemberKey)
              OR (@MemberKey = 'ALL')
          )
          AND vOperators.Company_key IN (22,27)
END;
GO