﻿CREATE PROCEDURE [dbo].[_RPT_LiveEftposBlackGetQBRActivities_new]
(
    @MemberKey INT,
    @month INT,
    @year INT
)
AS
BEGIN

    --select EntryDateTime, Description, Points, BalancePoints as Balance
    -- from MemberReferenceEntry where DATEPART(month, EntryDateTime) = @month and DATEPART(year, EntryDateTime) = @year
    --and Member_Key = @memberkey 

    SELECT TOP 1
           fdom.FirstDateOfMonth EntryDateTime,
           'Opening Balance' Description,
           ISNULL(mr.Balance, 0) Points,
           ISNULL(mr.Balance, 0) AS Balance,
           mrn.MemberReferenceNumber AS QBRNumber,
           com.Name AS MembershipType
    FROM Member m
        INNER JOIN Company com
            ON com.Company_key = m.Company_key
        LEFT JOIN
        (
            SELECT RANK() OVER (PARTITION BY member_key ORDER BY CreatedDate DESC) RankId,
                   MemberReferenceNumber,
                   member_key
            FROM MemberReference
        ) mrn
            ON mrn.member_key = m.Member_key
               AND mrn.RankId = 1
        INNER JOIN FirstDateOfMonth fdom
            ON MonthPart = @month
               AND YearPart = @year
        LEFT JOIN [MonthlyMemberReferenceEntry](@MemberKey) mr
            ON DATEPART(MONTH, mr.EntryDateTime) = DATEPART(MONTH, (DATEADD(MONTH, -1, fdom.FirstDateOfMonth)))
               AND DATEPART(YEAR, mr.EntryDateTime) = DATEPART(YEAR, (DATEADD(MONTH, -1, fdom.FirstDateOfMonth)))
               AND mr.Member_Key = m.Member_key
    WHERE m.Member_key = @MemberKey
    --order by EntryDateTime desc

    UNION ALL
    SELECT EntryDateTime,
           Description,
           Points,
           Balance,
           mrn.MemberReferenceNumber AS QBRNumber,
           com.Name AS MembershipType
    FROM [MonthlyMemberReferenceEntry](@MemberKey) q
        INNER JOIN Member m
            ON m.Member_key = q.Member_Key
        INNER JOIN Company com
            ON com.Company_key = m.Company_key
        LEFT JOIN
        (
            SELECT RANK() OVER (PARTITION BY member_key ORDER BY CreatedDate DESC) RankId,
                   MemberReferenceNumber,
                   member_key
            FROM MemberReference
        ) mrn
            ON mrn.member_key = m.Member_key
               AND mrn.RankId = 1
    WHERE DATEPART(MONTH, EntryDateTime) = @month
          AND DATEPART(YEAR, EntryDateTime) = @year
    ORDER BY EntryDateTime;

--select EntryDateTime, Description, CreditPoints as Points, BalancePoints as Balance
-- from MemberReferenceEntry where MonthPeriod = @month and YearPeriod = @year
--and Member_Key = @memberkey and ISNULL(CreditPoints,0)<>0
--union

--select EntryDateTime, Description, -DebitPoints as Points, BalancePoints as Balance
-- from MemberReferenceEntry where MonthPeriod = @month and YearPeriod = @year
--and Member_Key = @memberkey and ISNULL(DebitPoints,0)<>0
--order by EntryDateTime

END;


GO
