﻿ALTER PROC [dbo].[_RPT_TransactionSearch]
    @startdate DATETIME = NULL,
    @enddate DATETIME = NULL,
    @memberid VARCHAR(20) = NULL,
    @terminalid VARCHAR(20) = NULL,
    @taxiid VARCHAR(10) = NULL,
    @BatchNumber VARCHAR(10) = NULL,
    @Fare REAL = NULL,
    @CardNumber VARCHAR(4) = NULL,
    @InvRoc VARCHAR(20) = NULL,
    @CardType VARCHAR(10) = 'ALL',
    @ApprovedIncluded BIT = 'True',
    @DeclinedIncluded BIT = 'False',
    @ProcessedBy VARCHAR(50) = NULL,
    @FullCardNumber NVARCHAR(30) = NULL,
	@OnlyDisplayGlideboxTransaction BIT = 0
--with recompile 
AS
IF @startdate IS NULL
BEGIN
    SET @startdate = '01/01/2009';
END;
IF @enddate IS NULL
BEGIN
    SET @enddate = '12/31/2099';
END;

IF @CardType IS NULL
BEGIN
    SET @CardType = 'ALL';
END;
DECLARE @memberkey INT;

DECLARE @terminalkey INT;


IF @memberid IS NOT NULL
BEGIN
    SELECT @memberkey = Member_key
    FROM Member
    WHERE MemberId = @memberid;
END;

IF @terminalid IS NOT NULL
BEGIN
    SELECT @terminalkey = EftTerminal_key
    FROM EftTerminal
    WHERE TerminalId = @terminalid;
END;
--if @memberid is null and @terminalid is not null
--begin 
--	declare @memcount  int = 0
--	select @memcount = count( distinct memberid) 
--	from vterminalallocations 
--	where terminalid=@terminalid 
--	and EffectiveDate > @startdate and (UnallocatedDate is null or UnallocatedDate < @enddate)
--	and EftTerminal_key is not null and Member_key is not null

--	if @memcount = 1 
--	begin 
--		select @memberid = memberid
--		from vterminalallocations 
--		where terminalid=@terminalid 
--		and EffectiveDate > @startdate and (UnallocatedDate is null or UnallocatedDate < @enddate)
--		and EftTerminal_key is not null and Member_key is not null

--	end
--end
BEGIN
    SELECT DISTINCT
           COALESCE(tdf.[Type], 'Unknown') AS DocketStatus,
           CASE trans.TransactionType_key
               WHEN 1 THEN
                   'cash'
               WHEN 0 THEN
                   'bank'
               ELSE
                   'Unknown'
           END AS paytype,
           TransactionDate AS PayDate,
           Account.AccountName AS PaidTo,
           MemberId,
           TradingName,
           terminalid AS TerminalID,
           cad.batchnumber AS BatchNumber,
           cad.merchantid AS Merchantid,
           taxiid AS Taxi,
           driverid AS DriverId,
           startshift AS StartShift,
           endshift AS EndShift,
           trans_status AS [Status],
           transauthorisationnumber AS Authorisation,
           cardname AS CardType,
           transpan AS MaskedCardNum,
           transstart AS StartDate,
           transend AS EndDate,
           transfare AS Fare,
           transextras AS Extras,
           transtip AS ServiceFee,
           transtotalamount AS TotalAmount,
           transpickupzone AS Pickup,
           transdropoffzone AS DropOff,
           sourcefile AS SourceFile,
           Office.Name AS OfficeName,
           SUBSTRING(sourcefile, 14, 8) AS Filedate,
           tdf.ModifiedByUser AS ProcessedBy,
           cad.id AS TransactionID,
           1 AS IsApproved,
           cad.CardEntryMode,
		   cad.GlideboxAmount,
		   c.GlideboxCommission
    FROM CadmusIN cad
        INNER JOIN EftDocket eft
            ON eft.EftDocket_key = cad.docket_key
        INNER JOIN DocketTable doc
            ON doc.Docket_key = cad.docket_key
        LEFT JOIN SummaryDocket sd
            ON sd.BatchId = doc.BatchId
        OUTER APPLY
    (
        SELECT 'Paid' AS [Type],
               ItemDate,
               RateUsed,
               Amount,
               TransactionDetailType,
               Description,
               Modified_dttm,
               ModifiedByUser,
               Docket_key,
               Transaction_key,
               Member_key,
               TransactionDetail_key AS [key]
        FROM TransactionDetail td
        WHERE td.TransactionDetailType = 0
              AND
              (
                  td.Docket_key = doc.Docket_key
                  OR td.Docket_key = sd.SummaryDocket_key
              )
        UNION ALL
        SELECT 'Pending' AS [Type],
               ItemDate,
               RateUsed,
               Amount,
               TransactionDetailType,
               Description,
               Modified_dttm,
               ModifiedByUser,
               Docket_key,
               -1 AS Transaction_key,
               Member_key,
               PendingTransactionDetail_key
        FROM PendingTransactionDetail pt
        WHERE TransactionDetailType = 0
              AND
              (
                  pt.Docket_key = doc.Docket_key
                  OR pt.Docket_key = sd.SummaryDocket_key
              )
    ) tdf

        --INNER JOIN 
        --(SELECT     'Paid' as [Type], ItemDate, RateUsed, Amount, TransactionDetailType, 
        --                      Description, Modified_dttm, ModifiedByUser, Docket_key, 
        --                      Transaction_key, Member_key, TransactionDetail_key as [key]
        --FROM  TransactionDetail
        --WHERE TransactionDetailType = 0
        --UNION ALL
        --SELECT    'Pending' as [Type], ItemDate, RateUsed, Amount, TransactionDetailType, 
        --                      Description, Modified_dttm, ModifiedByUser, Docket_key, 
        --                      -1 as Transaction_key, Member_key, PendingTransactionDetail_key
        --FROM PendingTransactionDetail
        --WHERE TransactionDetailType = 0 ) tdf
        --ON tdf.docket_key = eft.eftdocket_key

        LEFT JOIN dbo.[Transaction] trans
            ON tdf.Transaction_key = trans.Transaction_key
        LEFT JOIN dbo.TransactionType trtype
            ON trtype.TransactionType_key = trans.TransactionType_key
        LEFT JOIN Member
            ON Member.Member_key = tdf.Member_key
        LEFT JOIN Account
            ON Account.Account_key = trans.Account_key
        LEFT JOIN Office
            ON trans.Office_key = Office.Office_key
		OUTER APPLY
		(
			SELECT ISNULL(SUM(gt.GlideboxCommission), 0) AS GlideboxCommission
			FROM dbo.GlideboxTransaction gt
			WHERE gt.HostTransactionGuid = cad.HostTransactionGuid
		) AS c
    WHERE transstart
          BETWEEN @startdate AND @enddate
          AND
          (
              cad.terminalid = @terminalid
              OR @terminalid IS NULL
          )
          AND
          (
              Member.Member_key = @memberkey
              OR @memberid IS NULL
          )
          --AND (memberid = @memberid or @memberid IS NULL)
          AND
          (
              cad.taxiid = @taxiid
              OR @taxiid IS NULL
          )
          AND
          (
              cardname = @CardType
              OR @CardType = 'ALL'
          )
          AND
          (
              (
                  transfare = @Fare
                  OR @Fare IS NULL
              )
              OR
              (
                  transtotalamount = @Fare
                  OR @Fare IS NULL
              )
          )
          AND
          (
              cad.batchnumber = @BatchNumber
              OR @BatchNumber IS NULL
          )
          AND
          (
              RIGHT(LTRIM(RTRIM(transpan)), 4) LIKE @CardNumber
              OR @CardNumber IS NULL
          )
          AND
          (
              transauthorisationnumber = @InvRoc
              OR @InvRoc IS NULL
          )
          --AND trans_status LIKE 'Approve%'
          AND @ApprovedIncluded = 1
          AND
          (
              tdf.ModifiedByUser = @ProcessedBy
              OR @ProcessedBy IS NULL
          )
          AND
          (
              cad.transpan LIKE '%' + @FullCardNumber + '%'
              OR @FullCardNumber IS NULL
          )
		  AND
		  (
			@OnlyDisplayGlideboxTransaction = 0
			OR
			(
				@OnlyDisplayGlideboxTransaction = 1 AND ISNULL(cad.GlideboxAmount, 0) > 0
			)
		  )
    UNION ALL
    SELECT DISTINCT
           'DECLINED' AS DocketStatus,
           NULL AS paytype,
           NULL AS PayDate,
           NULL AS PaidTo,
           MemberId,
           TradingName,
           cad.terminalid AS TerminalID,
           cad.batchnumber AS BatchNumber,
           cad.merchantid AS Merchantid,
           taxiid AS Taxi,
           driverid AS DriverId,
           startshift AS StartShift,
           endshift AS EndShift,
           trans_status AS [Status],
           transauthorisationnumber AS Authorisation,
           cardname AS CardType,
           transpan AS MaskedCardNum,
           transstart AS StartDate,
           transend AS EndDate,
           transfare AS Fare,
           transextras AS Extras,
           transtip AS ServiceFee,
           transtotalamount AS TotalAmount,
           transpickupzone AS Pickup,
           transdropoffzone AS DropOff,
           sourcefile AS SourceFile,
           '' AS OfficeName,
           SUBSTRING(sourcefile, 14, 8) AS Filedate,
           '' AS ProcessedBy,
           cad.id AS TransactionID,
           0 AS IsApproved,
           cad.CardEntryMode,
		   cad.GlideboxAmount,
		   c.GlideboxCommission
    FROM CadmusIN cad
        INNER JOIN EftTerminal
            ON EftTerminal.TerminalId = cad.terminalid
        INNER JOIN TerminalAllocation
            ON TerminalAllocation.EftTerminal_key = EftTerminal.EftTerminal_key
               AND
               (
                   IsCurrent = 1
                   OR
                   (
                       IsCurrent = 0
                       AND EffectiveDate
               BETWEEN @startdate AND @enddate
                   )
               )
        INNER JOIN Member
            ON Member.Member_key = TerminalAllocation.Member_key
		OUTER APPLY
		(
			SELECT ISNULL(SUM(gt.GlideboxCommission), 0) AS GlideboxCommission
			FROM dbo.GlideboxTransaction gt
			WHERE gt.HostTransactionGuid = cad.HostTransactionGuid
		) AS c
    WHERE transstart
          BETWEEN @startdate AND @enddate
          AND
          (
              cad.terminalid = @terminalid
              OR @terminalid IS NULL
          )
          AND
          (
              Member.Member_key = @memberkey
              OR @memberid IS NULL
          )
          --AND (memberid = @memberid or @memberid IS NULL)
          AND
          (
              taxiid = @taxiid
              OR @taxiid IS NULL
          )
          AND
          (
              cardname = @CardType
              OR @CardType = 'ALL'
          )
          AND
          (
              (
                  transfare = @Fare
                  OR @Fare IS NULL
              )
              OR
              (
                  transtotalamount = @Fare
                  OR @Fare IS NULL
              )
          )
          AND
          (
              cad.batchnumber = @BatchNumber
              OR @BatchNumber IS NULL
          )
          AND
          (
              RIGHT(LTRIM(RTRIM(transpan)), 4) LIKE @CardNumber
              OR @CardNumber IS NULL
          )
          AND
          (
              transauthorisationnumber = @InvRoc
              OR @InvRoc IS NULL
          )
          AND @DeclinedIncluded = 1
          AND RTRIM(LTRIM(trans_status)) IN ( 'Declined', 'Voided' )
          AND
          (
              cad.transpan LIKE '%' + @FullCardNumber + '%'
              OR @FullCardNumber IS NULL
          )
		  AND
		  (
			@OnlyDisplayGlideboxTransaction = 0 
			OR
			(
				@OnlyDisplayGlideboxTransaction = 1 AND ISNULL(cad.GlideboxAmount, 0) > 0
			)
		  )
    UNION ALL
    SELECT DISTINCT
           'DECLINED' AS DocketStatus,
           NULL AS paytype,
           NULL AS PayDate,
           NULL AS PaidTo,
           MemberId,
           TradingName,
           cad_rej.terminalid AS TerminalID,
           cad_rej.batchnumber AS BatchNumber,
           cad_rej.merchantid AS Merchantid,
           taxiid AS Taxi,
           driverid AS DriverId,
           startshift AS StartShift,
           endshift AS EndShift,
           trans_status AS [Status],
           transauthorisationnumber AS Authorisation,
           cardname AS CardType,
           transpan AS MaskedCardNum,
           transstart AS StartDate,
           transend AS EndDate,
           transfare AS Fare,
           transextras AS Extras,
           transtip AS ServiceFee,
           transtotalamount AS TotalAmount,
           transpickupzone AS Pickup,
           transdropoffzone AS DropOff,
           sourcefile AS SourceFile,
           '' AS OfficeName,
           SUBSTRING(sourcefile, 14, 8) AS Filedate,
           '' AS ProcessedBy,
           cad_rej.id AS TransactionID,
           0 AS IsApproved,
           cad_rej.CardEntryMode,
		   cad_rej.GlideboxAmount,
		   c.GlideboxCommission
    FROM CadmusDeclined cad_rej
        INNER JOIN EftTerminal
            ON EftTerminal.TerminalId = cad_rej.terminalid
        INNER JOIN TerminalAllocation
            ON TerminalAllocation.EftTerminal_key = EftTerminal.EftTerminal_key
               AND
               (
                   IsCurrent = 1
                   OR
                   (
                       IsCurrent = 0
                       AND EffectiveDate
               BETWEEN @startdate AND @enddate
                   )
               )
        INNER JOIN Member
            ON Member.Member_key = TerminalAllocation.Member_key
		OUTER APPLY
		(
			SELECT ISNULL(SUM(gt.GlideboxCommission), 0) AS GlideboxCommission
			FROM dbo.GlideboxTransaction gt
			WHERE gt.HostTransactionGuid = cad_rej.HostTransactionGuid
		) AS c
    WHERE transstart
          BETWEEN @startdate AND @enddate
          AND
          (
              cad_rej.terminalid = @terminalid
              OR @terminalid IS NULL
          )
          AND
          (
              Member.Member_key = @memberkey
              OR @memberid IS NULL
          )
          --AND (memberid = @memberid or @memberid IS NULL)
          AND
          (
              taxiid = @taxiid
              OR @taxiid IS NULL
          )
          AND
          (
              cardname = @CardType
              OR @CardType = 'ALL'
          )
          AND
          (
              (
                  transfare = @Fare
                  OR @Fare IS NULL
              )
              OR
              (
                  transtotalamount = @Fare
                  OR @Fare IS NULL
              )
          )
          AND
          (
              cad_rej.batchnumber = @BatchNumber
              OR @BatchNumber IS NULL
          )
          AND
          (
              RIGHT(LTRIM(RTRIM(transpan)), 4) LIKE @CardNumber
              OR @CardNumber IS NULL
          )
          AND
          (
              transauthorisationnumber = @InvRoc
              OR @InvRoc IS NULL
          )
          AND @DeclinedIncluded = 1
          AND
          (
              cad_rej.transpan LIKE '%' + @FullCardNumber + '%'
              OR @FullCardNumber IS NULL
          )
		  AND
		  (
			@OnlyDisplayGlideboxTransaction = 0 
			OR
			(
				@OnlyDisplayGlideboxTransaction = 1 AND ISNULL(cad_rej.GlideboxAmount, 0) > 0
			)
		  )
    UNION ALL
    SELECT DISTINCT
           tdf.[Type] AS DocketStatus,
           CASE trans.TransactionType_key
               WHEN 1 THEN
                   'cash'
               WHEN 0 THEN
                   'bank'
               ELSE
                   NULL
           END AS paytype,
           TransactionDate AS PayDate,
           Account.AccountName AS PaidTo,
           MemberId,
           TradingName,
           terminalid AS TerminalID,
           cad.batchnumber AS BatchNumber,
           cad.merchantid AS Merchantid,
           taxiid AS Taxi,
           driverid AS DriverId,
           startshift AS StartShift,
           endshift AS EndShift,
           trans_status AS [Status],
           transauthorisationnumber AS Authorisation,
           cardname AS CardType,
           transpan AS MaskedCardNum,
           transstart AS StartDate,
           transend AS EndDate,
           transfare AS Fare,
           transextras AS Extras,
           transtip AS ServiceFee,
           transtotalamount AS TotalAmount,
           transpickupzone AS Pickup,
           transdropoffzone AS DropOff,
           sourcefile AS SourceFile,
           Office.Name AS OfficeName,
           SUBSTRING(sourcefile, 14, 8) AS Filedate,
           tdf.ModifiedByUser AS ProcessedBy,
           0 AS TransactionID,
           1 AS IsApproved,
           '' CardEntryMode,
		   0 AS GlideboxAmount,
		   0 AS GlideboxCommission
    FROM SameDayReconciliation sameday
        INNER JOIN
        (
            SELECT 'Paid' AS [Type],
                   ItemDate,
                   RateUsed,
                   Amount,
                   TransactionDetailType,
                   Description,
                   Modified_dttm,
                   ModifiedByUser,
                   Docket_key,
                   Transaction_key,
                   Member_key,
                   TransactionDetail_key AS [key]
            FROM TransactionDetail
            WHERE TransactionDetailType = 8
        ) tdf
            ON tdf.Docket_key = sameday.SummaryDocket_key
        INNER JOIN CadmusIN cad
            ON cad.docket_key = sameday.Docket_key
        LEFT JOIN dbo.[Transaction] trans
            ON tdf.Transaction_key = trans.Transaction_key
        LEFT JOIN dbo.TransactionType trtype
            ON trtype.TransactionType_key = trans.TransactionType_key
        INNER JOIN Member
            ON Member.Member_key = tdf.Member_key
        LEFT JOIN Account
            ON Account.Account_key = trans.Account_key
        LEFT JOIN Office
            ON trans.Office_key = Office.Office_key
    WHERE transstart
          BETWEEN @startdate AND @enddate
          AND
          (
              TerminalID = @terminalid
              OR @terminalid IS NULL
          )
          AND
          (
              Member.Member_key = @memberkey
              OR @memberid IS NULL
          )
          --AND (memberid = @memberid or @memberid IS NULL)
          AND
          (
              taxiid = @taxiid
              OR @taxiid IS NULL
          )
          AND
          (
              cardname = @CardType
              OR @CardType = 'ALL'
          )
          AND
          (
              (
                  transfare = @Fare
                  OR @Fare IS NULL
              )
              OR
              (
                  transtotalamount = @Fare
                  OR @Fare IS NULL
              )
          )
          AND
          (
              cad.batchnumber = @BatchNumber
              OR @BatchNumber IS NULL
          )
          AND
          (
              RIGHT(LTRIM(RTRIM(transpan)), 4) LIKE @CardNumber
              OR @CardNumber IS NULL
          )
          AND
          (
              transauthorisationnumber = @InvRoc
              OR @InvRoc IS NULL
          )
          --AND trans_status LIKE 'Approve%'
          AND @ApprovedIncluded = 1
          AND
          (
              tdf.ModifiedByUser = @ProcessedBy
              OR @ProcessedBy IS NULL
          )
          AND
          (
              cad.transpan LIKE '%' + @FullCardNumber + '%'
              OR @FullCardNumber IS NULL
          )
		  ORDER BY TransactionID DESC
    OPTION (RECOMPILE);
END;