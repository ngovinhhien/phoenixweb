﻿ALTER PROCEDURE [dbo].[_RPT_CashingSummary]
(@refNumber VARCHAR(50))
AS
BEGIN

    SELECT [Transaction].TransactionDate,
           [Transaction].LodgementReferenceNo,
           Member.MemberId,
           Member.Member_key,
           Member.TradingName,
           Member.State,
           SUM(   CASE
                      WHEN md.ManualDocket_key IS NOT NULL
                           AND TransactionDetailType = 0 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS ManualFare,
           SUM(   CASE
                      WHEN md.ManualDocket_key IS NOT NULL
                           AND TransactionDetailType = 1 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS ManualCommission,
           SUM(   CASE
                      WHEN DocketStatus IS NULL
                           AND TransactionDetailType = 5 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS HandlingFee,
           SUM(   CASE
                      WHEN DocketStatus IS NULL
                           AND TransactionDetailType = 7 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS ManualChargeback,
           SUM(   CASE
                      WHEN md.ManualDocket_key IS NOT NULL
                           AND TransactionDetailType = 18 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS ManualLevyCharge,
           SUM(   CASE
                      WHEN e.EftDocket_key IS NOT NULL
                           AND TransactionDetailType = 0 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS TotalFare,
           SUM(   CASE
                      WHEN e.EftDocket_key IS NOT NULL
                           AND TransactionDetailType = 2 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS OperatorCommission,
           SUM(   CASE
                      WHEN e.EftDocket_key IS NOT NULL
                           AND TransactionDetailType = 14 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS AutoTipFee,
           SUM(   CASE
                      WHEN e.EftDocket_key IS NOT NULL
                           AND TransactionDetailType = 18 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS LevyCharge,
           SUM(   CASE
                      WHEN DocketStatus = 0
                           AND TransactionDetailType = 7 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS EFTChargebacks,
           SUM(   CASE
                      WHEN sd.SummaryDocket_key IS NOT NULL
                           AND TransactionDetailType = 8 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS SameDayFare,
           SUM(   CASE
                      WHEN sd.SummaryDocket_key IS NOT NULL
                           AND TransactionDetailType = 2 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS SameDayCommission,
           SUM(   CASE
                      WHEN sd.SummaryDocket_key IS NOT NULL
                           AND TransactionDetailType = 18 THEN
                          Amount
                      ELSE
                          0
                  END
              ) AS SameDayLevyCharge,
           MAX([Transaction].TotalAmount) AS TotalAmount,
           [Transaction].ModifiedByUser,
           [Transaction].Office_key,
           Office.Name AS OfficeName,
           SUM(   CASE
                      WHEN e.EftDocket_key IS NOT NULL
                           AND TransactionDetailType = 0 THEN
                          ISNULL(c.AutoTipAmount, 0)
                      ELSE
                          0
                  END
              ) AS TipAmount,
           MIN(Member.Company_key) AS Company_key,
           CAST(ISNULL(ot.ShowLiveMessageOnReport, 0) AS BIT) ShowLiveMessageOnReport,
           SUM(   CASE
                      WHEN gsdc.GlideboxSameDayCashingID IS NOT NULL
                           AND TransactionDetail.TransactionDetailType = 21 THEN
                          TransactionDetail.Amount
                      ELSE
                          0
                  END
              ) AS GlideboxCommissionForSameDayCashing,
			SUM(CASE WHEN gsdc.GlideboxSameDayCashingID IS NULL AND TransactionDetail.TransactionDetailType = 21 THEN TransactionDetail.Amount ELSE 0 END) AS GlideboxCommissionForCashing
    FROM [Transaction]
        INNER JOIN Member
            ON [Transaction].Member_key = Member.Member_key
        LEFT JOIN TransactionDetail
            ON TransactionDetail.Transaction_key = [Transaction].Transaction_key
        LEFT JOIN DocketTable
            ON TransactionDetail.Docket_key = DocketTable.Docket_key
        INNER JOIN Office
            ON Office.Office_key = [Transaction].Office_key
        LEFT JOIN OfficeType ot
            ON ot.OfficeType_key = Office.OfficeType_key
        LEFT JOIN ManualDocket md
            ON md.ManualDocket_key = DocketTable.Docket_key
        LEFT JOIN EftDocket e
            ON e.EftDocket_key = DocketTable.Docket_key
        LEFT JOIN SummaryDocket sd
            ON sd.SummaryDocket_key = DocketTable.Docket_key
        LEFT JOIN dbo.GlideboxSameDayCashing gsdc
            ON gsdc.DocketKey = DocketTable.Docket_key
        LEFT JOIN CadmusIN c
            ON c.docket_key = e.EftDocket_key
    WHERE ([Transaction].LodgementReferenceNo = @refNumber)
    GROUP BY [Transaction].TransactionDate,
             [Transaction].LodgementReferenceNo,
             Member.MemberId,
             Member.TradingName,
             Member.State,
             Member.Member_key,
             [Transaction].ModifiedByUser,
             [Transaction].Office_key,
             Office.Name,
             ot.OfficeType_key,
             ot.ShowLiveMessageOnReport;


/*
	Refer to https://livegroup.freshdesk.com/a/tickets/187437
*/
/*                           
SELECT        TransactionDate, LodgementReferenceNo, MemberId, t.Member_key, TradingName, t.State, 
                         SUM(CASE WHEN md.ManualDocket_key IS not NULL AND TransactionDetailType = 0 THEN SumAmount ELSE 0 END) AS ManualFare, 
                         SUM(CASE WHEN  md.ManualDocket_key IS not NULL AND TransactionDetailType = 1 THEN SumAmount ELSE 0 END) AS ManualCommission, 
                         SUM(CASE WHEN t.DocketStatus IS NULL AND TransactionDetailType = 5 THEN SumAmount ELSE 0 END) AS HandlingFee, 
                         SUM(CASE WHEN t.DocketStatus IS NULL AND TransactionDetailType = 7 THEN SumAmount ELSE 0 END) AS ManualChargeback, 
                         SUM(CASE WHEN md.ManualDocket_key IS not NULL AND TransactionDetailType = 18 THEN SumAmount ELSE 0 END) AS ManualLevyCharge, 
                         SUM(CASE WHEN e.EftDocket_key IS not Null AND TransactionDetailType = 0 THEN SumAmount ELSE 0 END) AS TotalFare, 
                         SUM(CASE WHEN e.EftDocket_key IS not Null AND TransactionDetailType = 2 THEN SumAmount ELSE 0 END) AS OperatorCommission, 
                         SUM(CASE WHEN e.EftDocket_key IS not Null AND TransactionDetailType = 14 THEN SumAmount ELSE 0 END) AS AutoTipFee, 
                          SUM(CASE WHEN e.EftDocket_key IS not Null AND TransactionDetailType = 18 THEN SumAmount ELSE 0 END) AS LevyCharge, 
                         SUM(CASE WHEN t.DocketStatus = 0 AND TransactionDetailType = 7 THEN SumAmount ELSE 0 END) AS EFTChargebacks, 
                         SUM(CASE WHEN sd.SummaryDocket_key IS Not null AND TransactionDetailType = 8 THEN SumAmount ELSE 0 END) AS SameDayFare, 
                         SUM(CASE WHEN sd.SummaryDocket_key IS Not null AND TransactionDetailType = 2 THEN SumAmount ELSE 0 END) AS SameDayCommission, 
                         SUM(CASE WHEN sd.SummaryDocket_key IS Not null AND TransactionDetailType = 18 THEN SumAmount ELSE 0 END) AS SameDayLevyCharge, 
                         Sum(TotalPaid) AS TotalAmount, t.ModifiedByUser, t.Office_key, Office.Name AS OfficeName,
                         SUM(CASE WHEN e.EftDocket_key IS not Null AND TransactionDetailType = 0 THEN isnull(c.AutoTipAmount,0) ELSE 0 END) AS TipAmount, 
                          min(t.Company_key) as Company_key, cast(isnull(ot.ShowLiveMessageOnReport,0) as bit) ShowLiveMessageOnReport
FROM            
						[vCashingReportEFT] t left JOIN
                         DocketTable ON t.Docket_key = DocketTable.Docket_key INNER JOIN
                         Office ON Office.Office_key = t.Office_key
                         left join officetype ot on ot.OfficeType_key = office.Officetype_key
                         left join ManualDocket md on md.ManualDocket_key = DocketTable.Docket_key 
                         left join EftDocket e on e.EftDocket_key = DocketTable.Docket_key 
                         left join SummaryDocket sd on sd.SummaryDocket_key = DocketTable.Docket_key
                         left join cadmusin c on c.docket_key =e.eftdocket_key
WHERE        (LodgementReferenceNo = @refNumber)
GROUP BY			TransactionDate, LodgementReferenceNo, MemberId, TradingName, t.State, t.Member_key, 
                         t.ModifiedByUser, t.Office_key, Office.Name, ot.OfficeType_key, ot.ShowLiveMessageOnReport               
   */

END;
