﻿CREATE PROCEDURE [dbo].[GetTerminalAllcations]	
	@TerminalId VARCHAR(20),	
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT
AS
BEGIN

	DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM vTerminalAllocations v LEFT JOIN VehicleType ON  v.VehicleType_key = VehicleType.VehicleType_key',
	@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'
	
	IF(ISNULL(@TerminalId,'')<>'')
	BEGIN
		SET @QueryWhere += ' AND TerminalId = @TerminalId'
	END
	ELSE 
	BEGIN
		SET @QueryWhere = '1=2'
	END

	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = ';WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY IsCurrent DESC, EffectiveDate DESC) AS RowId, TerminalId, MemberId, VehicleId, EffectiveDate, IsCurrent, v.ModifiedByUser, UnallocatedDate,TerminalAllocation_key, VehicleType.Name AS VehicleTypeName' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = N'	
	@TerminalId VARCHAR(20),	
	@RowIDFrom INT,
	@RowIDTo INT,
	@TotalItems INT OUT'	

	EXEC SP_EXECUTESQL @Query, @Params	
	, @TerminalId= @TerminalId	
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT
END
GO
