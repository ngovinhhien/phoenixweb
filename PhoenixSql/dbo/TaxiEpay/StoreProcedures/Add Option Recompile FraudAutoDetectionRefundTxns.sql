﻿ALTER PROC [dbo].[FraudAutoDetectionRefundTxns] @Backdate int, @amount money, @alertKey int
AS

begin

declare @BackDatePeriod date = cast(dateadd(day, -@Backdate, getdate()) as date)



declare @todayfiledate date  = cast(getdate() as date)

declare @filedate15 date  = cast(dateadd(day, -15, getdate()) as date)

declare @filedate30 date  = cast(dateadd(day, -30, getdate()) as date)

declare @filedate60 date  = cast(dateadd(day, -60, getdate()) as date)


declare @temp table (member_key int, stdeviation decimal(20,2))

insert into @temp (member_key, stdeviation)
select member_key, stdeviation from MemberStandardDeviation30Days

----detect Declined attempts Same member same card, multiple times in last two day and the amount greater than 125
insert into FraudTransaction (eftdocket_key, fraudalertKey, createdDate, OccurenceLast15days, OccurenceLast30Days, OccurenceLast60days,TurnoverStdv30days, TrancountStdv30days,terminalallocation_key, TerminalID, CardType, CardNumber, FileDate, TransactionTotal, DocketDate)
select distinct e.eftdocket_key,@alertKey as FraudAlertKey, getdate(),d15.trancount as OccurenceLast15days, d30.trancount as OccurrenceLast30days, 
d60.trancount as OccurenceLast60days,isnull(std.stdeviation, 0) stdeviation, 0,a.TerminalAllocation_key, c.terminalid, c.cardname, c.transpan, c.filedate,
 c.transtotalamount, c.transstart
 from eftdocket e
inner join cadmusin c on c.docket_key = e.EftDocket_key and c.transtotalamount <@amount 
cross apply ( 
select top 1 a.TerminalAllocation_key
from  TerminalAllocation a where a.Member_key = e.Operator_key 
and a.EftTerminal_key = e.EftTerminal_key and a.EffectiveDate < e.TripStart
order by a.EffectiveDate 
 ) a
inner join member m on m.member_key  = e.Operator_key
left join @temp std on std.member_key = m.Member_key --and (c.transtotalamount > std.stdeviation+250)
left join FraudTransaction ft on ft.eftdocket_key = e.eftdocket_key and ft.fraudalertkey = @alertKey
left join [RefundTransactionsLast15Days] d15 on d15.Operator_key = e.Operator_key and c.transpan = d15.transpan 
left join [RefundTransactionsLast30Days] d30 on d30.Operator_key = e.Operator_key and c.transpan = d30.transpan 
left join [RefundTransactionsLast60Days] d60 on d60.Operator_key = e.Operator_key and c.transpan = d60.transpan 
where c.filedate > @BackDatePeriod
and m.member_key not in (select member_key from FraudWhitelist where member_key = m.Member_key) 
and ft.eftdocket_key is null
option(recompile)


end