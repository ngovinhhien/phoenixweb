﻿ALTER proc [dbo].[_Transaction_GetPayRunPendingOperatorByDateRange] 
@from char(8),
@to char(8)
as
begin

exec ('
 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
 from GetOperatorPayRunPendingDetails(null, null, null) pend 
 inner join Member mem on mem.Member_key = pend.Member_key
 
 where pend.filedate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''
 
 union
 
 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = docket.Docket_key
 
 where docket.docketdate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''

 union

 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargeHistory fch on fch.DocketKey = docket.Docket_key
 
 where docket.docketdate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''


 union

 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargedRefund fch on fch.DocketKey = docket.Docket_key
 
 where docket.docketdate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''



 union
 
 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join MemberChargeHistory mch on mch.MemberChargeHistory_key = docket.Docket_key
 
 where docket.docketdate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''
 order by 1,2
 
 

 '
 
 
 
 ) 
 
 
/*
exec ('
 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
 from GetOperatorPayRunPendingDetails(null, null, null) pend 
 inner join Member mem on mem.Member_key = pend.Member_key
 
 where substring(pend.sourcefile,14,8) between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''
 
 union
 
 select 
  isnull(mem.TradingName, '' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')'' as OperatorName,
  pend.Member_key as OperatorKey, mem.company_key
  
from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = docket.Docket_key
 
 where docket.docketdate between ' + '''' + @from + '''' + ' and ' + '''' + @to + '''' +
 ' group by mem.company_key, pend.Member_key, isnull(mem.TradingName,'' '') + '' ('' + isnull(mem.FirstName +' + ''' ''' + '+ mem.LastName, '''') + '')''
 order by 1,2

 '
 
 
 )
*/


end

