﻿ALTER proc [dbo].[_Transaction_GetPayRunPendingOperatorByMember] 
@from char(8),
@to char(8),
@inMember varchar(max)
as
begin

--exec ('

--	select 
--		''O'' as RunType,
--		Docket.BatchId,
--		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
--		pend.Member_key,
--		pend.SourceFile,
--		TransactionDetailType,
--		SUM(Amount) as Total

--	from GetOperatorPayRunPendingDetails(null, null, null) pend 
--	inner join Docket on Docket.Docket_key =  pend.Docket_key
--	inner join Member mem on mem.Member_key = pend.Member_key
	
--	where substring(pend.sourcefile,14,8) between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
--		' and mem.Member_key in (' + @inMember + ')
	
--	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, pend.SourceFile, Docket.BatchId, TransactionDetailType
--	order by 3,2')							 
					 
--exec ('
--	select 
--		''O'' as RunType,
--		Docket.BatchId,
--		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
--		pend.Member_key,
--		pend.SourceFile,
--		TransactionDetailType,
--		SUM(Amount) as Total

--	from GetOperatorPayRunPendingDetails(null, null, null) pend 
--	inner join Docket on Docket.Docket_key =  pend.Docket_key
--	inner join Member mem on mem.Member_key = pend.Member_key
	
--	where substring(pend.sourcefile,14,8) between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
--		' and mem.Member_key in (' + @inMember + ')
	
--	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, pend.SourceFile, Docket.BatchId, TransactionDetailType
	
--	union 
	
--	select 
--		''O'' as RunType,
--		Docket.BatchId,
--		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
--		pend.Member_key,
--		''TerminalRentalCharge''+cast(trh.RentMonth as varchar(2))+''-''+cast(trh.RentYear as varchar(4)) as SourceFile,
--		TransactionDetailType,
--		SUM(Amount) as Total

--	from pendingtransactiondetail pend 
--	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
--	inner join Member mem on mem.Member_key = pend.Member_key
--	inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = docket.Docket_key
	
--	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
--		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (9,10)
	
--	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, ''TerminalRentalCharge''+cast(trh.RentMonth as varchar(2))+''-''+cast(trh.RentYear as varchar(4)), Docket.BatchId, TransactionDetailType
	
	
	
--	order by 3,2'
--	)						 

exec ('


	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		pend.SourceFile,
		TransactionDetailType,
		SUM(Amount) as Total

	from GetOperatorPayRunPendingDetails(null, null, null) pend 
	inner join Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	
	where substring(pend.sourcefile,14,8) between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ')
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, pend.SourceFile, Docket.BatchId, TransactionDetailType
	
	union 
	
	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		''TerminalRentalCharge''+cast(trh.RentMonth as varchar(2))+''-''+cast(trh.RentYear as varchar(4)) as SourceFile,
		TransactionDetailType,
		SUM(Amount) as Total

	from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = docket.Docket_key
	
	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (9,10)
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, ''TerminalRentalCharge''+cast(trh.RentMonth as varchar(2))+''-''+cast(trh.RentYear as varchar(4)), Docket.BatchId, TransactionDetailType
	union
	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		''TerminalReplacementCharge''+right(''0000''+cast(datepart(year, mch.CreatedDate) as nvarchar(4)),4)+ right(''00''+cast(datepart(month, mch.CreatedDate) as nvarchar(2)),2) + right(''00''+cast(datepart(day, mch.CreatedDate) as nvarchar(2)),2) as SourceFile,
		
		TransactionDetailType,
		SUM(Amount) as Total

	from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join MemberChargeHistory mch on mch.MemberChargeHistory_key = docket.Docket_key
	
	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (6,11,12)
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, ''TerminalReplacementCharge''+right(''0000''+cast(datepart(year, mch.CreatedDate) as nvarchar(4)),4)+ right(''00''+cast(datepart(month, mch.CreatedDate) as nvarchar(2)),2) + right(''00''+cast(datepart(day, mch.CreatedDate) as nvarchar(2)),2),
		 Docket.BatchId, TransactionDetailType

	union 
	
	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		''AccountFeeCharge''+cast(MONTH(fch.StartDate) as varchar(2))+''-''+cast(YEAR(fch.StartDate) as varchar(4)) as SourceFile,
		TransactionDetailType,
		SUM(fch.Amount) as Total

	from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargeHistory fch on fch.DocketKey = docket.Docket_key
	
	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (19)
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, ''AccountFeeCharge''+cast(MONTH(fch.StartDate) as varchar(2))+''-''+cast(YEAR(fch.StartDate) as varchar(4)), Docket.BatchId, TransactionDetailType

	union 
	
	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		''RefundAccountFeeCharge ''+cast(MONTH(fch.StartDate) as varchar(2))+''-''+cast(YEAR(fch.StartDate) as varchar(4)) as SourceFile,
		TransactionDetailType,
		SUM(fcr.RefundAmount) as Total

	from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargedRefund fcr on fcr.DocketKey = docket.Docket_key
	inner join FeeChargeHistory fch on fcr.FeeChargeHistoryID = fch.ID
	
	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (20)
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName, ''RefundAccountFeeCharge ''+cast(MONTH(fch.StartDate) as varchar(2))+''-''+cast(YEAR(fch.StartDate) as varchar(4)), Docket.BatchId, TransactionDetailType


	union
	select 
		''O'' as RunType,
		Docket.BatchId,
		ISNULL(mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName) as MemberName,
		pend.Member_key,
		''Terminal Insurance charge reversal''+right(''0000''+cast(datepart(year, mch.CreatedDate) as nvarchar(4)),4)+ right(''00''+cast(datepart(month, mch.CreatedDate) as nvarchar(2)),2) + right(''00''+cast(datepart(day, mch.CreatedDate) as nvarchar(2)),2) as SourceFile,
		
		TransactionDetailType,
		SUM(Amount) as Total

	from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join Deposit mch on mch.Depositkey = docket.Docket_key
	
	where docket.docketdate between ' + '''' + @from + '''' + 'and ' + '''' + @to + '''' +
		' and mem.Member_key in (' + @inMember + ') and pend.TransactionDetailType in (13)
	
	group by pend.Member_key,  mem.TradingName, mem.FirstName +' + ''' ''' + '+ mem.LastName,
	''Terminal Insurance charge reversal''+right(''0000''+cast(datepart(year, mch.CreatedDate) as nvarchar(4)),4)+ right(''00''+cast(datepart(month, mch.CreatedDate) as nvarchar(2)),2) + right(''00''+cast(datepart(day, mch.CreatedDate) as nvarchar(2)),2),
		 Docket.BatchId, TransactionDetailType
	
	
	order by 3,2'
	)			

		 
end