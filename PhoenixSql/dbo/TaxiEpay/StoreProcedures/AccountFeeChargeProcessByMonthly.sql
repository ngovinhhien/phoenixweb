﻿CREATE PROCEDURE [dbo].[usp_BatchJob_AccountFeeChargeProcessByMonthly]
    @Month INT = 0,
    @Year INT = 0,
    @AllowDuplicate BIT = 0
AS
BEGIN
    DECLARE @TempTable TABLE
    (
        FeeChargeDetailId INT NOT NULL,
        MemberKey INT NOT NULL,
        Amount DECIMAL(18, 2) NOT NULL
    );

    DECLARE @IdentityOutput TABLE
    (
        ID INT NOT NULL
    );

    DECLARE @FirstDateOfCurrentMonth DATE = GETDATE(),
            @StartOfCurrentMonth DATE,
            @EndOfCurentMonth DATE,
            @Tranmonth INT,
            @Tranyear INT;

    IF @Month <> 0
       AND @Year <> 0
    BEGIN
        SET @FirstDateOfCurrentMonth
            = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('00' + CAST(@Month AS NVARCHAR(2)), 2) + '01' AS DATETIME);
    END;

    SELECT @StartOfCurrentMonth = DATEADD(MONTH, 0, @FirstDateOfCurrentMonth),
           @EndOfCurentMonth = EOMONTH(@FirstDateOfCurrentMonth, 0),
           @Tranmonth = DATEPART(MONTH, DATEADD(MONTH, 0, @FirstDateOfCurrentMonth)),
           @Tranyear = DATEPART(YEAR, DATEADD(MONTH, 0, @FirstDateOfCurrentMonth));

    IF @AllowDuplicate = 0
    BEGIN
        INSERT INTO @TempTable
        (
            FeeChargeDetailId,
            MemberKey,
            Amount
        )
        SELECT ID,
               MemberKey,
               Amount
        FROM dbo.FeeChargeDetail d
        WHERE IsActive = 1
              AND FrequencyTypeID = 1
              AND Amount > 0
              AND NOT EXISTS
        (
            SELECT TOP (1)
                   1
            FROM dbo.FeeChargeHistory h
            WHERE d.ID = h.FeeChargeDetailId
                  AND h.StartDate = @StartOfCurrentMonth
                  AND h.EndDate = @EndOfCurentMonth
        )
              AND @StartOfCurrentMonth >= (CAST(CAST(YEAR(d.FeeChargeStartDate) AS VARCHAR(4))
                                                + RIGHT('00' + CAST(MONTH(d.FeeChargeStartDate) AS NVARCHAR(2)), 2)
                                                + '01' AS DATETIME)
                                          );
    END;
    ELSE
    BEGIN
        INSERT INTO @TempTable
        (
            FeeChargeDetailId,
            MemberKey,
            Amount
        )
        SELECT ID,
               MemberKey,
               Amount
        FROM dbo.FeeChargeDetail
        WHERE IsActive = 1
              AND FrequencyTypeID = 1
              AND Amount > 0
              AND @StartOfCurrentMonth >= (CAST(CAST(YEAR(FeeChargeStartDate) AS VARCHAR(4))
                                                + RIGHT('00' + CAST(MONTH(FeeChargeStartDate) AS NVARCHAR(2)), 2)
                                                + '01' AS DATETIME)
                                          );
    END;


    --Adding Terminal Allocation validation
    --1. Terminal terminal allocation, which just deallocated in previous month
    DELETE FROM @TempTable
    WHERE MemberKey NOT IN
          (
              SELECT DISTINCT
                     MemberKey
              FROM dbo.vTerminalAllocations
                  INNER JOIN @TempTable
                      ON MemberKey = Member_key
              WHERE (
                        IsCurrent = 0
						AND VirtualTerminal = 0
                        AND
                        (
                            (DATEADD(MONTH, 1, UnallocatedDate)
                    BETWEEN @StartOfCurrentMonth AND @EndOfCurentMonth
                            )
                            OR (UnallocatedDate
                    BETWEEN @StartOfCurrentMonth AND @EndOfCurentMonth
                               )
                        )
                    )
                    OR
                    (
                        --Terminal allocating
                        IsCurrent = 1
                        AND Status = 'LIVE'
						AND VirtualTerminal = 0
                    )
          );

    DECLARE @FeeChargeDetailId INT,
            @MemberKey INT,
            @FeeChargeAmount DECIMAL(18, 2),
            @DocketKey INT;

    DECLARE FeeCursor CURSOR READ_ONLY FOR
    SELECT FeeChargeDetailId,
           MemberKey,
           Amount
    FROM @TempTable;

    OPEN FeeCursor;

    FETCH NEXT FROM FeeCursor
    INTO @FeeChargeDetailId,
         @MemberKey,
         @FeeChargeAmount;

    WHILE @@FETCH_STATUS = 0
    BEGIN


        INSERT INTO DocketTable
        (
            BatchId,
            DocketDate,
            DocketPaymentStatus,
            FaceValue,
            DocketTotal,
            Office_key,
            ModifiedByUser,
            Modified_dttm
        )
        OUTPUT inserted.Docket_key
        INTO @IdentityOutput
        VALUES
        (CAST(@Tranyear AS VARCHAR(4)) + RIGHT('00' + CAST(@Tranmonth AS NVARCHAR(2)), 2)
         +  RIGHT('0000000' + @MemberKey, 8) + RIGHT('00' + CAST(DATEPART(MONTH, GETDATE()) AS NVARCHAR(2)), 2)
         +  RIGHT('00' + CAST(DATEPART(DAY, GETDATE()) AS NVARCHAR(2)), 2), CAST(GETDATE() AS DATE), 0,
         @FeeChargeAmount, @FeeChargeAmount, 51, 'Auto Process', GETDATE());

        SET @DocketKey =
        (
            SELECT ID FROM @IdentityOutput
        );

        INSERT INTO dbo.FeeChargeHistory
        (
            FeeChargeDetailId,
            ProcessedDate,
            StartDate,
            EndDate,
            Amount,
            DocketKey,
            CreatedBy,
            CreatedDate,
            UpdatedBy,
            UpdatedDate
        )
        VALUES
        (@FeeChargeDetailId, GETDATE(), @StartOfCurrentMonth, @EndOfCurentMonth, @FeeChargeAmount, @DocketKey,
         'Auto Process', GETDATE(), 'Auto Process', GETDATE());

        INSERT INTO dbo.PendingTransactionDetail
        (
            ItemDate,
            RateUsed,
            Amount,
            TransactionDetailType,
            [Description],
            ModifiedByUser,
            Modified_dttm,
            Docket_key,
            Member_key
        )
        VALUES
        (GETDATE(), -1, -@FeeChargeAmount, 19, 'Account Fee', 'Auto Process', GETDATE(), @DocketKey, @MemberKey);

        DELETE FROM @IdentityOutput;

        FETCH NEXT FROM FeeCursor
        INTO @FeeChargeDetailId,
             @MemberKey,
             @FeeChargeAmount;
    END;

    CLOSE FeeCursor;
    DEALLOCATE FeeCursor;
END;
