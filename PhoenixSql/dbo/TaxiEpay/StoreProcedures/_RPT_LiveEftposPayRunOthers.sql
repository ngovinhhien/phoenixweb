﻿
CREATE PROCEDURE [dbo].[_RPT_LiveEftposPayRunOthers] (
	@PayRunId varchar(50),
	@MemberKey varchar(50) = 'ALL'
	)
AS
BEGIN
	SELECT DISTINCT
       pp.PayRunId,
       CASE
           WHEN t.TransactionDetailType IN ( 9, 10, 11 ) THEN -- 9:Terminal Rental Charge | 10: Reverse: Terminal Rental Charge | 11:Terminal Rental Charge
               t.Description + '-' + et.TerminalId + '-' + CAST(trh.RentMonth AS VARCHAR(2)) + '/' + CAST(trh.RentYear AS VARCHAR(4))
           ELSE
               t.Description
       END AS Description,
       d.transid1 AS TransactionNumber,
       ISNULL(c.FaceValue, 0) + ISNULL(c.Extras, 0) AS TransactionAmount,
       t.RateUsed,
       t.Amount,
	   DATENAME(m,DATEADD(m, trh.RentMonth, -1)) + ' ' + RIGHT(trh.RentYear, 2)  AS Period
FROM PayRun pp
    INNER JOIN [Transaction] h
        ON h.PayRun_key = pp.PayRun_key
    INNER JOIN TransactionDetail t
        ON t.Transaction_key = h.Transaction_key
    INNER JOIN vOperators
        ON h.Member_key = vOperators.Operator_key
    INNER JOIN DocketTable c
        ON c.Docket_key = t.Docket_key
	LEFT JOIN TerminalRentalHistory trh
        ON trh.TerminalRentalHistory_key = c.Docket_key
    INNER JOIN dbo.TerminalAllocation ta
        ON ta.TerminalAllocation_key = trh.TerminalAllocation_key
    INNER JOIN dbo.EftTerminal et
        ON et.EftTerminal_key = ta.EftTerminal_key
    LEFT JOIN CadmusIN d
        ON d.docket_key = c.Docket_key
WHERE (pp.PayRunId = @PayRunId)
      AND
      (
          t.TransactionDetailType > 4
          AND
          (
              t.TransactionDetailType <> 7
              OR
              (
                  t.TransactionDetailType = 7
                  AND t.Description NOT LIKE 'Charge Back%'
              )
          )
          AND t.TransactionDetailType <> 14
      )
      AND
      (
          (vOperators.Operator_key = @MemberKey)
          OR (@MemberKey = 'ALL')
      )
      AND vOperators.Company_key IN ( 18, 26 )

UNION ALL
	SELECT DISTINCT
		  pp.PayRunId,
		  t.Description,
		  d.transid1 AS TransactionNumber,
		  ISNULL(c.FaceValue, 0) + ISNULL(c.Extras, 0) AS TransactionAmount,
		  t.RateUsed,
		  t.Amount,
		  FORMAT(fch.StartDate, 'MMMM') + ' ' + FORMAT(fch.StartDate, 'yy')   AS Period
	FROM dbo.PayRun pp
	   INNER JOIN [Transaction] h
		   ON h.PayRun_key = pp.PayRun_key
	   INNER JOIN TransactionDetail t
		   ON t.Transaction_key = h.Transaction_key
	   INNER JOIN
		  vOperators ON h.Member_key = vOperators.Operator_key
	   INNER JOIN DocketTable c
		   ON c.Docket_key = t.Docket_key
	   INNER JOIN FeeChargeHistory fch
			ON c.Docket_key = fch.DocketKey
	   LEFT JOIN CadmusIN d
		   ON d.docket_key = c.Docket_key
	WHERE pp.PayRunId = @PayRunId
		 AND t.TransactionDetailType in (19, 20)
		 AND  ((vOperators.operator_key = @MemberKey) OR (@MemberKey = 'ALL')) And vOperators.Company_key in ( 18, 26)

UNION ALL
	SELECT DISTINCT
		  pp.PayRunId,
		  t.Description,
		  d.transid1 AS TransactionNumber,
		  ISNULL(c.FaceValue, 0) + ISNULL(c.Extras, 0) AS TransactionAmount,
		  t.RateUsed,
		  t.Amount,
		  FORMAT(StartDate, 'MMMM') + ' ' + FORMAT(StartDate, 'yy')   AS Period
	FROM dbo.PayRun pp
	   INNER JOIN [Transaction] h
		   ON h.PayRun_key = pp.PayRun_key
	   INNER JOIN TransactionDetail t
		   ON t.Transaction_key = h.Transaction_key
	   INNER JOIN
		  vOperators ON h.Member_key = vOperators.Operator_key
	   INNER JOIN DocketTable c
		   ON c.Docket_key = t.Docket_key
	   INNER JOIN dbo.FeeChargedRefund fch
			ON c.Docket_key = fch.DocketKey
		INNER JOIN dbo.FeeChargeHistory ON FeeChargeHistory.ID = fch.FeeChargeHistoryID
	   LEFT JOIN CadmusIN d
		   ON d.docket_key = c.Docket_key
	WHERE pp.PayRunId = @PayRunId
		 AND t.TransactionDetailType in (19, 20)
		 AND  ((vOperators.operator_key = @MemberKey) OR (@MemberKey = 'ALL')) And vOperators.Company_key in ( 18, 26)
	END

GO
