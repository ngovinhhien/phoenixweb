﻿CREATE PROCEDURE GetMemberEntryForManagerApprovalKey @pointID VARCHAR(100), @businessTypeID INT
AS
DECLARE @key INT = 0

;WITH cte_name
  AS (SELECT MemberReferenceEntryAndEntryForManagerApproval.MemberReferenceEntryForManagerApprovalKey,
             MIN(MemberReferenceEntryKey) AS MinKey,
             COUNT(MemberReferenceEntryKey) AS Number,
             MemberKey,
             OriginalMonth,
             OriginalYear,
             Point
      FROM dbo.MemberReferenceEntryAndEntryForManagerApproval
          INNER JOIN dbo.MemberReferenceEntryForManagerApproval
              ON MemberReferenceEntryForManagerApproval.MemberReferenceEntryForManagerApprovalKey = MemberReferenceEntryAndEntryForManagerApproval.MemberReferenceEntryForManagerApprovalKey
      WHERE BusinessTypeID = @businessTypeID
      GROUP BY MemberReferenceEntryAndEntryForManagerApproval.MemberReferenceEntryForManagerApprovalKey,
               MemberKey,
               OriginalMonth,
               OriginalYear,
               Point)
SELECT TOP 1 @key = MemberReferenceEntryForManagerApprovalKey
       FROM cte_name
GROUP BY MemberReferenceEntryForManagerApprovalKey,
         MemberKey,
         OriginalMonth,
         OriginalYear,
         Point,
         MinKey
		  HAVING  (CAST(MemberKey AS VARCHAR(50)) + CAST(OriginalMonth AS VARCHAR(10))
                + CAST(OriginalYear AS VARCHAR(10))
                + CAST(SUM(Point) AS VARCHAR(50))
                + CAST(MIN(MinKey) AS VARCHAR(50))
               )  = @pointID 
	
SELECT @key;
