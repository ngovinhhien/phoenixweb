﻿CREATE PROCEDURE [dbo].[Docket_GetListUnAllocateDockets]
	@DocketBatchId VARCHAR(20),
	@TerminalId VARCHAR(20),
	@BatchNumber VARCHAR(20),
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT
AS
BEGIN

	DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM vUnallocatedEftDocket',
	@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'

	IF(ISNULL(@DocketBatchId,'')<>'')
	BEGIN
		SET @QueryWhere += ' AND DocketBatchId = @DocketBatchId'
	END
	IF(ISNULL(@TerminalId,'')<>'')
	BEGIN
		SET @QueryWhere += ' AND TerminalId = @TerminalId'
	END
	IF(ISNULL(@BatchNumber,'')<>'')
	BEGIN
		SET @QueryWhere += ' AND BatchNumber = @BatchNumber'
	END

	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = ';WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY [DocketDate] DESC) AS RowId, Docket_key as Docket_key, DocketBatchId, TerminalId, vehicleid as VehicleId, DocketDate, FaceValue, Extras, ServiceCharge, Gst, DocketTotal, BatchNumber '
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = N'
	@DocketBatchId VARCHAR(20),
	@TerminalId VARCHAR(20),
	@BatchNumber VARCHAR(20),
	@RowIDFrom INT,
	@RowIDTo INT,
	@TotalItems INT OUT'	

	EXEC SP_EXECUTESQL @Query, @Params
	, @DocketBatchId = @DocketBatchId
	, @TerminalId= @TerminalId
	, @BatchNumber = @BatchNumber
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT
END