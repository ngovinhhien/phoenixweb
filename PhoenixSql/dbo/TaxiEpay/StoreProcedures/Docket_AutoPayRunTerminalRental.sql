﻿CREATE PROCEDURE [dbo].[_Docket_AutoPayRunTerminalRental]
    @Company_key INT = 0,
    @payrundate DATE = NULL,
    @Refund BIT = 0,
    @rentmonth INT = NULL,
    @rentyear INT = NULL,
    @ignoredeallocation BIT = 0,
    @ignorerentalmonth BIT = 0,
    @accountFeeChargeMonth INT = NULL,
    @accountFeeChargeYear INT = NULL
AS
BEGIN
    DECLARE @trancount INT;
    DECLARE @memberKey INT;
    DECLARE @transactionkey INT = 0;
    DECLARE @sumamount DECIMAL(10, 2) = 0.0;
    DECLARE @payrunkey INT = 0;
    DECLARE @accountkey INT = 0;
    DECLARE @memcounter INT = 0;
    DECLARE @Refno NVARCHAR(50) = N'';
    DECLARE @TranDesc NVARCHAR(100) = N'';
    DECLARE @Prefix NVARCHAR(1);
    DECLARE @PayrunReportType NVARCHAR(1);
    DECLARE @currentdate DATETIME = GETDATE();
    DECLARE @payrunID NVARCHAR(20);
    DECLARE @memberid NVARCHAR(10);
    DECLARE @temptable TABLE
    (
        memberKey INT,
        docketKey INT,
        pendingtransactionkey INT,
        TransactionDetailType INT,
        amount DECIMAL(10, 2),
        accountKey INT,
        memberID NVARCHAR(10)
    );

    SET @Prefix = N'T';
    SET @TranDesc = N'Terminal Rental';
    SET @PayrunReportType = N'O';

    IF @payrundate IS NULL
    BEGIN
        SET @payrundate = CAST(GETDATE() AS DATE);
    END;

    DECLARE @IdentityOutput TABLE
    (
        ID INT,
        ReferenceNo NVARCHAR(50)
    );

    IF @rentmonth IS NULL
    BEGIN
        SET @rentmonth = DATEPART(MONTH, DATEADD(MONTH, -1, GETDATE()));
    END;

    IF @rentyear IS NULL
    BEGIN
        SET @rentyear = DATEPART(YEAR, DATEADD(MONTH, -1, GETDATE()));
    END;
	--Account fee will be charged for current month
    IF @accountFeeChargeMonth IS NULL
    BEGIN
        SET @accountFeeChargeMonth = DATEPART(MONTH, DATEADD(MONTH, 0, GETDATE()));
    END;

    IF @accountFeeChargeYear IS NULL
    BEGIN
        SET @accountFeeChargeYear = DATEPART(YEAR, DATEADD(MONTH, 0, GETDATE()));
    END;

    --DECLARE @tempSelectedRecords TABLE  
    --		 (  
    --		  Amount decimal(10, 2),  
    --		  Docket_key int,  
    --		  member_Key int,  
    --		  pendingtransactionkey int  
    --		 )  


    INSERT INTO @temptable
    (
        memberKey,
        docketKey,
        pendingtransactionkey,
        TransactionDetailType,
        amount,
        accountKey,
        memberID
    )
    SELECT p.Member_key,
           p.Docket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           CAST(p.Amount AS DECIMAL(10, 2)) AS amount,
           a.Account_key,
           m.MemberId
    --memberKey,docketKey, pendingtransactionkey,TransactionDetailType, amount
    FROM dbo.PreparePayrun pp
        --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN PendingTransactionDetail p
            ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
        INNER JOIN TerminalRentalHistory trh
            ON trh.TerminalRentalHistory_key = p.Docket_key --and trh.Modified_dttm > dateadd(day,-30, getdate()) 
               AND
               (
                   (
                       trh.RentMonth = @rentmonth
                       AND trh.RentYear = @rentyear
                   )
                   OR
                   (
                       @ignorerentalmonth = 1
                       AND trh.ProcessedDate > DATEADD(YEAR, -1, GETDATE())
                   )
                   OR ISNULL(@Refund, 0) = 1
               )
        INNER JOIN Account a
            ON a.Member_key = p.Member_key --and a.Weight> 0
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
               AND m.Company_key = @Company_key
    WHERE a.Account_key = pp.Account_key
          AND pp.PayRunDate = @payrundate
          AND ISNULL(pp.Holdoff, 0) = 0
          AND ISNULL(m.Active, 0) = 1
          AND
          (
              (
                  p.TransactionDetailType IN ( 9 )
                  AND @Refund = 0
              )
              OR
              (
                  p.TransactionDetailType IN ( 10 )
                  AND @Refund = 1
              )
          )
    UNION

    --Account fee charge by monthly--
    (SELECT p.Member_key,
            p.Docket_key,
            p.PendingTransactionDetail_key,
            p.TransactionDetailType,
            CAST(p.Amount AS DECIMAL(10, 2)) AS Amount,
            pp.Account_key,
            m.MemberId
     FROM dbo.PreparePayrun pp
         INNER JOIN dbo.PendingTransactionDetail p
             ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
         INNER JOIN dbo.FeeChargeHistory h
             ON h.DocketKey = p.Docket_key
         INNER JOIN dbo.FeeChargeDetail d
             ON h.FeeChargeDetailId = d.ID
                AND d.FrequencyTypeID = 1
         INNER JOIN dbo.Member m
             ON m.Member_key = p.Member_key
                AND m.Company_key = @Company_key
     WHERE pp.PayRunDate = @payrundate
           AND ISNULL(pp.Holdoff, 0) = 0
           AND ISNULL(m.Active, 0) = 1
           AND p.TransactionDetailType IN ( 19 )
           --- Account Fee need to charge for this month.
           AND MONTH(h.StartDate) = @accountFeeChargeMonth
           AND YEAR(h.StartDate) = @accountFeeChargeYear
     UNION
     SELECT p.Member_key,
            p.Docket_key,
            p.PendingTransactionDetail_key,
            p.TransactionDetailType,
            CAST(p.Amount AS DECIMAL(10, 2)) AS Amount,
            pp.Account_key,
            m.MemberId
     FROM dbo.PreparePayrun pp
         INNER JOIN dbo.PendingTransactionDetail p
             ON p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
         INNER JOIN dbo.FeeChargedRefund fcr
             ON fcr.DocketKey = p.Docket_key
         INNER JOIN dbo.Member m
             ON m.Member_key = p.Member_key
     WHERE pp.PayRunDate = @payrundate
           AND ISNULL(pp.Holdoff, 0) = 0
           AND ISNULL(m.Active, 0) = 1
           AND p.TransactionDetailType IN ( 20 ));


    SELECT @trancount = COUNT(t.pendingtransactionkey)
    FROM @temptable t;
    DECLARE @MemberCursor CURSOR;
    IF @trancount > 0
    BEGIN


        SET @payrunID
            = @Prefix + RIGHT(CAST(DATEPART(YEAR, @currentdate) AS NVARCHAR(4)), 2)
              + RIGHT('00' + CAST(DATEPART(MONTH, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(DAY, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(HOUR, @currentdate) AS NVARCHAR), 2)
              + RIGHT('00' + CAST(DATEPART(MINUTE, @currentdate) AS NVARCHAR), 2);

        SELECT @Refno = PayRunId
        FROM PayRun
        WHERE PayRunId = @payrunID;


        --make sure the Reference number doesn't exist
        WHILE ISNULL(@Refno, '') <> ''
        BEGIN
            --add one minute to the Reference Number if reference number already exists
            SET @currentdate = DATEADD(MINUTE, 1, @currentdate);
            SET @payrunID
                = @Prefix + RIGHT(CAST(DATEPART(YEAR, @currentdate) AS NVARCHAR(4)), 2)
                  + RIGHT('00' + CAST(DATEPART(MONTH, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(DAY, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(HOUR, @currentdate) AS NVARCHAR), 2)
                  + RIGHT('00' + CAST(DATEPART(MINUTE, @currentdate) AS NVARCHAR), 2);

            SELECT @Refno = PayRunId
            FROM PayRun
            WHERE PayRunId = @payrunID;

        END;

        --insert into PayRunAuto (PayrunID, PayrunDate, Comments, Modified_dttm, ModifiedByUser)
        INSERT INTO PayRun
        (
            PayRunId,
            PayRunDate,
            Comments,
            Modified_dttm,
            ModifiedByUser,
            PayrunType
        )
        OUTPUT inserted.PayRun_key,
               inserted.PayRunId
        INTO @IdentityOutput
        VALUES
        (@payrunID, CAST(GETDATE() AS DATE), '', GETDATE(), 'Auto Payrun', 1);



        SELECT @payrunkey =
        (
            SELECT ID FROM @IdentityOutput
        );

        SELECT @Refno =
        (
            SELECT ReferenceNo FROM @IdentityOutput
        );

        --insert a new record into payrun report control table so that the operator pay run report will pick up and email report to operator.	
        INSERT INTO PayRunReportControl
        (
            PayRunId,
            PayRunType
        )
        VALUES
        (@Refno, @PayrunReportType);

        SET @MemberCursor = CURSOR FOR
        SELECT memberKey,
               accountKey,
               CAST(SUM(amount) AS DECIMAL(10, 2)) AS amount,
               memberID
        FROM @temptable t
        GROUP BY memberKey,
                 accountKey,
                 memberID
        --having cast(abs(Sum(amount)) as decimal(10,2))>0
        ORDER BY memberKey,
                 accountKey;

        OPEN @MemberCursor;

        FETCH NEXT FROM @MemberCursor
        INTO @memberKey,
             @accountkey,
             @sumamount,
             @memberid;

        WHILE @@fetch_status = 0 --for each member and account
        BEGIN
            SET @memcounter = @memcounter + 1;
            --insert into transaction table
            DELETE FROM @IdentityOutput
            WHERE ID = @payrunkey;
            BEGIN TRAN; --start transaction 
            --insert into [TransactionAuto] (TransactionDate, TotalAmount, LodgementReferenceNo, Status, ModifiedByUser, Modified_dttm, Office_key, TransactionType_key, PayRun_key, Member_key, Account_key)
            INSERT INTO [Transaction]
            (
                TransactionDate,
                TotalAmount,
                LodgementReferenceNo,
                Status,
                ModifiedByUser,
                Modified_dttm,
                Office_key,
                TransactionType_key,
                PayRun_key,
                Member_key,
                Account_key,
                Description
            )
            OUTPUT inserted.Transaction_key,
                   inserted.LodgementReferenceNo
            INTO @IdentityOutput
            VALUES
            (   GETDATE(), @sumamount, @Refno + '/' + @memberid, --cast(@memcounter as nvarchar(20))
                'Paid', 'Auto Payrun', GETDATE(), 51, 2, @payrunkey, @memberKey, @accountkey, @TranDesc);
            --from @tempSelectedRecords t

            SELECT @transactionkey =
            (
                SELECT ID FROM @IdentityOutput
            );

            --insert to transactiondetail table
            --insert into [TransactionDetailAuto] (Transaction_key, ItemDate, RateUsed, Amount, TransactionDetailType, Description, Modified_dttm, ModifiedByUser, Docket_key, Member_key)
            INSERT INTO [TransactionDetail]
            (
                Transaction_key,
                ItemDate,
                RateUsed,
                Amount,
                TransactionDetailType,
                Description,
                Modified_dttm,
                ModifiedByUser,
                Docket_key,
                Member_key
            )
            SELECT @transactionkey,
                   p.ItemDate,
                   p.RateUsed,
                   p.amount,
                   p.TransactionDetailType,
                   p.Description,
                   GETDATE(),
                   'Auto Payrun',
                   p.Docket_key,
                   p.Member_key
            FROM @temptable t
                --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key= t.pendingtransactionkey
                INNER JOIN PendingTransactionDetail p
                    ON p.PendingTransactionDetail_key = t.pendingtransactionkey
            WHERE t.memberKey = @memberKey
                  AND t.accountKey = @accountkey;


            DELETE FROM PreparePayrun
            WHERE PreparePayrun_key IN
                  (
                      SELECT pp.PreparePayrun_key
                      FROM PreparePayrun pp
                          INNER JOIN @temptable t
                              ON t.pendingtransactionkey = pp.PendingTransactionDetail_key
                      --inner join PendingTransactionDetailAuto p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key
                      --inner join PendingTransactionDetail p on p.PendingTransactionDetail_key = pp.PendingTransactionDetail_key  and p.PendingTransactionDetail_key = t.pendingtransactionkey
                      WHERE t.memberKey = @memberKey
                            AND t.accountKey = @accountkey
                  );

            -- delete from pendingtransactiondetail table
            --delete from PendingTransactionDetailAuto where PendingTransactionDetail_key in (
            DELETE FROM PendingTransactionDetail
            WHERE PendingTransactionDetail_key IN
                  (
                      SELECT pendingtransactionkey
                      FROM @temptable t
                      WHERE t.memberKey = @memberKey
                            AND t.accountKey = @accountkey
                  );

            DELETE FROM @temptable
            WHERE memberKey = @memberKey
                  AND accountKey = @accountkey;




            DELETE FROM @IdentityOutput
            WHERE ID = @transactionkey;
            --delete from @tempSelectedRecords -- remove everything from @tempSelectedRecords, ready for the next account.
            IF @@ERROR <> 0
               AND @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;
            ELSE
                COMMIT TRAN;

            FETCH NEXT FROM @MemberCursor
            INTO @memberKey,
                 @accountkey,
                 @sumamount,
                 @memberid;

        --end --accountcursor
        END; --membercursor
    END;



END;