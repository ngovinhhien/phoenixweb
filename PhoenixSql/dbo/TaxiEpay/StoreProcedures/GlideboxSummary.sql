﻿CREATE PROC [dbo].[GlideBoxSummary]
    @MemberKey INT,
    @Month INT,
    @FinancialYear INT
AS
DECLARE @startdate DATETIME;
DECLARE @enddate DATETIME;

SELECT @startdate = FirstDateOfMonth,
       @enddate = FirstDateOfNextMonth
FROM FirstDateOfMonth
WHERE MonthPart = @Month
      AND YearPart = @FinancialYear;


SELECT gbc.CategoryName AS CategoryName,
       gt.CategoryID AS CategoryID,
       gt.CommissionAmount AS CommissionAmount,
       SUM(gt.CountSold) AS TotalItemSold,
       SUM(gt.GlideboxCommission) AS TotalCommission
FROM [dbo].[Transaction] t
    INNER JOIN dbo.TransactionDetail td
        ON t.Transaction_key = td.Transaction_key
    INNER JOIN dbo.CadmusIN cin
        ON cin.docket_key = td.Docket_key
    INNER JOIN dbo.GlideboxTransaction gt
        ON gt.HostTransactionGuid = cin.HostTransactionGuid
    INNER JOIN Transactionhost.dbo.GlideBoxCategory gbc
        ON gt.CategoryID = gbc.CategoryID
WHERE t.Member_key = @MemberKey
      AND t.TransactionDate >= @startdate
      AND t.TransactionDate < @enddate
      AND td.TransactionDetailType = 21
GROUP BY gbc.CategoryName,
         gt.CategoryID,
         gt.CommissionAmount
UNION ALL
SELECT gbc.CategoryName AS CategoryName,
       gsdc.CategoryID AS CategoryID,
       gsdc.CommissionAmount AS CommissionAmount,
       SUM(gsdc.Quantity) AS TotalItemSold,
       SUM(gsdc.TotalGlideboxCommission) AS TotalCommission
FROM [dbo].[Transaction] t
    INNER JOIN dbo.TransactionDetail td
        ON t.Transaction_key = td.Transaction_key
    INNER JOIN dbo.GlideboxSameDayCashing gsdc
        ON gsdc.DocketKey = td.Docket_key
    INNER JOIN Transactionhost.dbo.GlideBoxCategory gbc
        ON gsdc.CategoryID = gbc.CategoryID
WHERE t.Member_key = @MemberKey
      AND t.TransactionDate >= @startdate
      AND t.TransactionDate < @enddate
      AND td.TransactionDetailType = 21
GROUP BY gbc.CategoryName,
         gsdc.CategoryID,
         gsdc.CommissionAmount;