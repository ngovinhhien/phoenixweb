﻿CREATE PROC [dbo].[_RPT_DeclinedTransactionSearch] 
	@startDate datetime = NULL, 
	@endDate datetime = NULL,
	@terminalId varchar(20) = NULL
AS
IF @startDate is NULL 
BEGIN
	SET @startDate = '01/01/2009'
END
IF @endDate IS NULL
BEGIN
	SET @endDate = '12/31/2099'
END
BEGIN 
	SELECT d.*
	FROM cadmusdeclined d
	LEFT JOIN cadmusin c ON c.HostTransactionHistoryKey = d.HostTransactionHistoryKey
	LEFT JOIN cadmusin n ON  n.terminalid = d.terminalid AND n.HostTransactionNumber = d.HostTransactionNumber AND d.HostTransactionHistoryKey IS NULL
	WHERE  d.terminalid = @terminalId
	AND d.transstart >= @startDate
	AND d.transend <= @endDate
	AND c.id is NULL AND n.id IS NULL
END
GO