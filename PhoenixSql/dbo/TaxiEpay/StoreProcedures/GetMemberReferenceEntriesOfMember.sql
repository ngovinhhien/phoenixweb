﻿CREATE PROC GetMemberReferenceEntriesOfMember @memberKey INT
AS
BEGIN
    SELECT m.MemberReferenceEntryKey,
           m.EntryDateTime,
           m.Description,
           m.Points,
           m.CreditPoints,
           m.DebitPoints,
           m.BalancePoints,
           m.ReversedMemberReferenceEntryKey,
           m.CreatedByUser,
           m.CreatedDateTime,
           m.UpdatedByUser,
           m.UpdatedDateTime,
           m.MonthPeriod,
           m.YearPeriod,
           m.Member_Key,
           m.ManualEntry,
           m.DocketPayMethodKey,
           m.MemberReferenceEntryTypeKey,
           lp.PromotionName
    FROM dbo.MemberReferenceEntry m
        LEFT JOIN dbo.LoyaltyPromotion lp
            ON lp.ID = m.LoyaltyPromotionID
    WHERE m.Member_Key = @memberKey
    ORDER BY m.EntryDateTime DESC;
END;