﻿ALTER PROCEDURE [dbo].[WebCashing_AddDocketsToCashing]
    @user NVARCHAR(50),
    @Docketkey INT,
    @DocketCheckSummary INT = NULL,
    @CashingCenterRate FLOAT = NULL,
    @OfficeKey INT = NULL,
    @MassScanning BIT = 0,
    @AuthorisedMemberKey INT = NULL
AS
BEGIN


    DECLARE @temptable TABLE
    (
        id INT IDENTITY(1, 1),
        Username NVARCHAR(50),
        Docket_key INT,
        batchid NVARCHAR(30),
        DocketDate DATETIME,
        MemberID NVARCHAR(20),
        TradingName NVARCHAR(50),
        member_key INT,
        TerminalID NVARCHAR(20),
        Facevalue DECIMAL(18, 2),
        Extras DECIMAL(18, 2),
        DocketTotal DECIMAL(18, 2),
        TotalCommission DECIMAL(18, 2),
        TotalPaid DECIMAL(18, 2),
        TotalPending DECIMAL(18, 2),
        TotalPayable DECIMAL(18, 2),
        CardNumberMasked NVARCHAR(30),
        BatchNumber NVARCHAR(20),
        PaymentStatus NVARCHAR(50),
        IsChecked BIT,
        State NVARCHAR(10),
        DocketSummaryKey INT,
        BatchStatus NVARCHAR(200),
        IsCashable BIT,
        DocketValuePending DECIMAL(10, 2),
        CommissionPending DECIMAL(10, 2),
        DocketValuePaid DECIMAL(10, 2),
        CommissionPaid DECIMAL(10, 2),
        LodgementReferenceNo NVARCHAR(30)
    );

    --test only
    --delete from DocketCheckDetail
    --delete from DocketCheckSummary

    --set @user = 'sydney'

    --set @Batchid = '356399550509022013'


    --end test


    DECLARE @recordcount INT;
    DECLARE @docketchecksummarykey INT;
    DECLARE @totalamount DECIMAL(18, 2);
    DECLARE @memberKey INT = NULL;
    DECLARE @Batchid NVARCHAR(30) = NULL;
    IF @Docketkey <> 0
    BEGIN
        SELECT @Batchid = BatchId
        FROM DocketTable
        WHERE Docket_key = @Docketkey;
    END;

    IF @DocketCheckSummary IS NOT NULL
    BEGIN
        SELECT @docketchecksummarykey = DocketCheckSummaryKey,
               @memberKey = COALESCE(MemberKey, AuthorisedMemberKey)
        FROM DocketCheckSummary
        WHERE DocketCheckSummaryKey = @DocketCheckSummary;
    END;

    IF @MassScanning = 1
    BEGIN
        SET @memberKey = NULL;
    END;

    INSERT INTO @temptable
    (
        member_key,
        DocketDate,
        Docket_key,
        batchid,
        Username,
        MemberID,
        TradingName,
        State,
        TerminalID,
        Facevalue,
        Extras,
        DocketTotal,
        TotalCommission,
        TotalPaid,
        TotalPayable,
        CardNumberMasked,
        BatchNumber,
        PaymentStatus,
        IsChecked,
        BatchStatus,
        LodgementReferenceNo,
        IsCashable
    )
    SELECT COALESCE(dcd.MemberKey, d.Operator_key, 0) AS Member_key,
           d.DocketDate,
           d.Docket_key,
           d.DocketBatchId,
           @user,
           m.MemberId,
           m.TradingName,
           m.State,
           t.TerminalId,
           CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                         AND
                         (
                             o.AllowCash = 1
                             OR m.Member_key IN
                                (
                                    SELECT member_key FROM [CashableBankOperator]
                                )
                         ) THEN
                        d.FaceValue
                    ELSE
                        0
                END AS MONEY) AS FaceValue,
           CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                         AND
                         (
                             o.AllowCash = 1
                             OR m.Member_key IN
                                (
                                    SELECT member_key FROM [CashableBankOperator]
                                )
                         ) THEN
                        d.Extras
                    ELSE
                        0
                END AS MONEY) AS Extras,
           CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                         AND
                         (
                             o.AllowCash = 1
                             OR m.Member_key IN
                                (
                                    SELECT member_key FROM [CashableBankOperator]
                                )
                         ) THEN
                        d.DocketTotal
                    ELSE
                        0
                END AS MONEY) AS DocketTotal,
           CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                         AND
                         (
                             o.AllowCash = 1
                             OR m.Member_key IN
                                (
                                    SELECT member_key FROM [CashableBankOperator]
                                )
                         ) THEN
                        d.TotalCommission
                    ELSE
                        0
                END AS MONEY) AS TotalCommission,
           CASE
               WHEN d.TotalPending <> 0
                    AND
                    (
                        o.AllowCash = 1
                        OR m.Member_key IN
                           (
                               SELECT member_key FROM [CashableBankOperator]
                           )
                    ) THEN
                   d.TotalPending
               WHEN d.TotalPaid <> 0 THEN
                   d.TotalPaid
               ELSE
                   0
           END AS TotalPaid,
           CAST(CASE
                    WHEN ABS(ISNULL(d.TotalPending, 0)) > 0
                         AND
                         (
                             o.AllowCash = 1
                             OR m.Member_key IN
                                (
                                    SELECT member_key FROM [CashableBankOperator]
                                )
                         ) THEN
                        d.TotalPending
                    ELSE
                        0
                END * (1 - ISNULL(0, 0)) AS DECIMAL(18, 2)) AS TotalPayable,
           d.CardNumberMasked,
           d.BatchNumber,
           d.PaymentStatus,
           CAST(CASE
                    WHEN dcd.DockeCheckDetailKey IS NOT NULL THEN
                        1
                    ELSE
                        0
                END AS BIT) AS IsChecked,
           CASE
               WHEN ABS(ISNULL(d.TotalPaid, 0)) <> 0 THEN
                   'Paid'
               WHEN
               (
                   o.AllowCash = 0
                   AND o.PayByEFT = 1
                   AND m.Member_key NOT IN
                       (
                           SELECT member_key FROM [CashableBankOperator]
                       )
               ) THEN
                   'Bank Customer'
               WHEN d.Docket_key IS NULL THEN
                   'Invalid batch'
               WHEN ABS(ISNULL(d.TotalPending, 0)) <> 0 THEN
                   'Valid'
               ELSE
                   'Invalid batch'
           END AS BatchStatus,
           ISNULL(d.ReferenceNumber, '') AS LodgementReferenceNo,
           CASE
               WHEN
               (
                   o.AllowCash = 1
                   OR m.Member_key IN
                      (
                          SELECT c.member_key FROM [CashableBankOperator] c
                      )
               ) THEN
                   1
               ELSE
                   0
           END
    FROM Operator o
        INNER JOIN Member m
            ON m.Member_key = o.Operator_key
               AND m.Active = 1
        LEFT JOIN vEftDocket d
            ON d.Operator_key = o.Operator_key
			   AND d.DocketBatchId = @Batchid
               AND ISNULL(d.Quarantined, 0) = 0
        LEFT JOIN EftTerminal t
            ON t.EftTerminal_key = d.EftTerminal_key
        OUTER APPLY
    (
        SELECT dcs.MemberKey,
               dcd.*
        FROM DocketCheckSummary dcs
            LEFT JOIN DocketCheckDetail dcd
                ON dcd.DocketCheckSummaryKey = dcs.DocketCheckSummaryKey
                   AND dcd.Docket_key = d.Docket_key
                   AND dcd.DocketCheckSummaryKey IS NOT NULL
        WHERE dcs.DocketCheckSummaryKey = @DocketCheckSummary
    ) dcd
        LEFT JOIN AuthorisedMember am
            ON (
                   am.Member_AuthorisedMember_key = @AuthorisedMemberKey
                   AND am.Member_key = o.Operator_key
               )
    WHERE (
              m.Member_key = @memberKey
              OR @memberKey IS NULL
          )
          AND dcd.DockeCheckDetailKey IS NULL
          AND COALESCE(dcd.MemberKey, d.Operator_key, 0) <> 0
          AND
          (
              (@AuthorisedMemberKey IS NULL)
              OR
              (
                  @AuthorisedMemberKey IS NOT NULL
                  AND
                  (
                      am.AuthorisedMember_key IS NOT NULL
                      OR @AuthorisedMemberKey = o.Operator_key
                  )
              )
          );

    SELECT @recordcount = COUNT(id)
    FROM @temptable;

    IF (@recordcount = 0)
    BEGIN

        IF (
               @memberKey IS NOT NULL
               AND @Batchid IS NOT NULL
               AND LEN(@Batchid) >= 14
           )
        BEGIN
            IF NOT EXISTS
            (
                SELECT TerminalId
                FROM vTerminalAllocations
                WHERE Member_key = @memberKey
                      AND IsCurrent = 1
                      AND TerminalId = LEFT(@Batchid, 8)
            )
            BEGIN
                INSERT INTO @temptable
                (
                    TerminalID,
                    BatchNumber,
                    batchid,
                    Username,
                    TotalPaid,
                    TotalPayable,
                    TotalPending,
                    BatchStatus,
                    IsCashable
                )
                VALUES
                (LEFT(@Batchid, 8), SUBSTRING(@Batchid, 9, 6), @Batchid, @user, 0, 0, 0, 'Member Not Authorised', 0);
            END;
            ELSE IF EXISTS
            (
                SELECT d.BatchId
                FROM DocketTable d
                    INNER JOIN EftDocket e
                        ON e.EftDocket_key = d.Docket_key
                WHERE d.BatchId = @Batchid
                      AND e.Operator_key IS NULL
            )
            BEGIN
                INSERT INTO @temptable
                (
                    TerminalID,
                    BatchNumber,
                    batchid,
                    Username,
                    TotalPaid,
                    TotalPayable,
                    TotalPending,
                    BatchStatus,
                    IsCashable
                )
                VALUES
                (LEFT(@Batchid, 8), SUBSTRING(@Batchid, 9, 6), @Batchid, @user, 0, 0, 0, 'Transactions Not Allocated',
                 0  );
            END;
            ELSE IF EXISTS
            (
                SELECT d.BatchId
                FROM DocketTable d
                    INNER JOIN SummaryDocket s
                        ON s.SummaryDocket_key = d.Docket_key
                WHERE d.BatchId = @Batchid
            )
            BEGIN
                INSERT INTO @temptable
                (
                    TerminalID,
                    BatchNumber,
                    batchid,
                    Username,
                    TotalPaid,
                    TotalPayable,
                    TotalPending,
                    BatchStatus,
                    IsCashable
                )
                VALUES
                (LEFT(@Batchid, 8), SUBSTRING(@Batchid, 9, 6), @Batchid, @user, 0, 0, 0,
                 'Transactions Cashed As Sameday', 0);
            END;
            ELSE
            BEGIN
                INSERT INTO @temptable
                (
                    TerminalID,
                    BatchNumber,
                    batchid,
                    Username,
                    TotalPaid,
                    TotalPayable,
                    TotalPending,
                    BatchStatus,
                    IsCashable
                )
                VALUES
                (LEFT(@Batchid, 8), SUBSTRING(@Batchid, 9, 6), @Batchid, @user, 0, 0, 0, 'Invalid Batch', 0);
            END;
        END;
        ELSE
        BEGIN

            INSERT INTO @temptable
            (
                TerminalID,
                BatchNumber,
                batchid,
                Username,
                TotalPaid,
                TotalPayable,
                TotalPending,
                BatchStatus,
                IsCashable
            )
            VALUES
            (LEFT(@Batchid, 8), SUBSTRING(@Batchid, 9, 6), @Batchid, @user, 0, 0, 0, 'Invalid Batch', 0);
        END;

    END;

    SELECT @recordcount = COUNT(id)
    FROM @temptable;


    IF @recordcount > 0
    BEGIN


        IF @DocketCheckSummary IS NULL
        BEGIN
            INSERT INTO DocketCheckSummary
            (
                CreatedDate,
                CreatedByUser,
                MemberKey,
                AuthorisedMemberKey
            )
            VALUES
            (GETDATE(), @user, @memberKey, @AuthorisedMemberKey);

            SET @docketchecksummarykey = SCOPE_IDENTITY();


        END;
        ELSE
        BEGIN
            SET @docketchecksummarykey = @DocketCheckSummary;
        END;

        IF @memberKey IS NULL
           AND ISNULL(@MassScanning, 0) = 0
        BEGIN
            SELECT @memberKey = MAX(member_key)
            FROM @temptable;

            UPDATE DocketCheckSummary
            SET MemberKey = @memberKey
            WHERE DocketCheckSummaryKey = @docketchecksummarykey;
        END;


        INSERT INTO DocketCheckDetail
        (
            DocketCheckAmount,
            DocketCheckSummaryKey,
            Docket_key,
            CreatedDate,
            CreatedByUser,
            TerminalID,
            BatchNumber,
            BatchStatus,
            IsCashable
        )
        SELECT TotalPayable,
               @docketchecksummarykey,
               Docket_key,
               GETDATE(),
               @user,
               TerminalID,
               BatchNumber,
               BatchStatus,
               IsCashable
        FROM @temptable;

        SELECT @totalamount = SUM(dcd.DocketCheckAmount)
        FROM DocketCheckSummary dcs
            INNER JOIN DocketCheckDetail dcd
                ON dcd.DocketCheckSummaryKey = dcs.DocketCheckSummaryKey
        WHERE dcs.DocketCheckSummaryKey = @docketchecksummarykey;

        UPDATE DocketCheckSummary
        SET TotalAmount = @totalamount
        WHERE DocketCheckSummaryKey = @docketchecksummarykey;

    END;


    --select Member_key, docketdate, 
    --docket_key, DocketBatchId,CreatedByUser ,DocketCheckSummaryKey,BatchID,
    --memberid,TradingName,state, TerminalId,
    -- FaceValue,
    -- Extras,
    -- DocketTotal,
    -- TotalCommission,
    -- TotalPaid ,
    -- BatchTotalPayable, 
    -- TotalPayable, 
    --CardNumberMasked, BatchNumber, PaymentStatus, GrandTotal, BatchStatus,IsCashable, 
    --PendingTransactionValue, PendingCommision, PaidTransactionValue, PaidCommission, LodgementReferenceNo, BatchTotal, DocketTotalSummary,
    --FaceValueSummary,ExtrasSummary,CommissionSummary,TotalBatchCommission,TotalBatchFaceValue,TotalBatchExtras, CardType , handlingFee, TotalBatchHandlingFee, HandlingFeeSummary, 
    --TipPro, TipProSummary, TippingFee, TippingFeeSummary, TotalBatchTippingFee, Chargebacks, TotalBatchChargebacks,ChargebacksSummary
    --from GetWebCashingDocketCheckSummary (@docketchecksummarykey)
    ----where docketchecksummarykey = @docketchecksummarykey
    --order by CreatedDate desc, DocketDate


    EXEC spGetWebCashingDocketCheckSummary @docketchecksummarykey;

END;