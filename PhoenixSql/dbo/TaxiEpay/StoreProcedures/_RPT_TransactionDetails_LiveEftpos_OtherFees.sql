﻿ALTER PROC [dbo].[_RPT_TransactionDetails_LiveEftpos_OtherFees] @MemberKey int,  @TranMonth int,@TranYear int
AS


declare @startdate datetime
declare @enddate datetime

select @startdate = FirstDateOfMonth, @enddate = FirstDateOfNextMonth from FirstDateOfMonth where MonthPart = @TranMonth and YearPart = @TranYear

--select Sum(isnull(td.amount,0)) as TotalAmount,
--'Terminal Rental TID: ' + cast(tal.TerminalId as varchar) +'  ' + (cast(trh.rentMonth as varchar) +'/'+ cast(trh.RentYear as varchar)) as descriptions, trh.rentMonth, trh.RentYear
--from  [transaction] t 
--inner join transactiondetail td on t.transaction_key = td.transaction_key
--inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = td.Docket_key
--inner JOIN vTerminalAllocations Tal
--ON  Tal.TerminalAllocation_key = trh.TerminalAllocation_key
--where t.member_key = @MemberKey and td.TransactionDetailType = 9
----and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
----AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
--and t.faileddate is null  and t.TransactionDate  between @startdate and @enddate
--group by trh.TerminalAllocation_key,tal.TerminalId , trh.rentMonth, trh.RentYear

--union
select Sum(isnull(td.amount,0)) as TotalAmount,
'Terminal Rental Refund TID: ' + cast(Tal.TerminalId as varchar) +'  ' + (cast(trh.rentMonth as varchar) +'/'+ cast(trh.RentYear as varchar)) as descriptions,trh.rentMonth, trh.RentYear
from  [transaction] t 
inner join transactiondetail td on t.transaction_key = td.transaction_key
inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = td.Docket_key
inner JOIN vTerminalAllocations Tal
ON  Tal.TerminalAllocation_key = trh.TerminalAllocation_key
where t.member_key = @MemberKey and td.TransactionDetailType = 10
--and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
--AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
and t.faileddate is null and t.TransactionDate  between @startdate and @enddate
group by trh.TerminalAllocation_key,tal.TerminalId , trh.rentMonth, trh.RentYear

UNION
   SELECT
		   Sum(isnull(td.amount,0)) as TotalAmount,
		   'Account Fee ' + right('00' + cast(datepart(month, fch.StartDate) as varchar(2)), 2) + '/' + right('0000' + cast(datepart(year, fch.StartDate) as varchar(4)), 4) as descriptions,
		   datepart(month, fch.StartDate) as rentMonth, 
		   datepart(year, fch.StartDate) as RentYear
        FROM [Transaction] t
        INNER JOIN TransactionDetail td
            ON td.Transaction_key = t.Transaction_key
		INNER JOIN vOperators 
			ON t.Member_key = vOperators.Operator_key
		INNER JOIN FeeChargeHistory fch
			ON fch.DocketKey = td.Docket_key
    WHERE td.TransactionDetailType = 19
          AND vOperators.operator_key = @MemberKey 
		  AND t.FailedDate IS NULL
		  AND t.TransactionDate BETWEEN @startdate AND @enddate
	group by t.Member_key, datepart(month, fch.StartDate), datepart(year, fch.StartDate)

UNION
   SELECT
		   Sum(isnull(td.amount,0)) as TotalAmount,
		   'Refund Account Fee ' + right('00' + cast(datepart(month, fch.StartDate) as varchar(2)), 2) + '/' + right('0000' + cast(datepart(year, fch.StartDate) as varchar(4)), 4) as descriptions,
		   datepart(month, fch.StartDate) as rentMonth, 
		   datepart(year, fch.StartDate) as RentYear
        FROM [Transaction] t
        INNER JOIN TransactionDetail td
            ON td.Transaction_key = t.Transaction_key
		INNER JOIN vOperators 
			ON t.Member_key = vOperators.Operator_key
		INNER JOIN FeeChargedRefund fcr
			ON fcr.DocketKey = td.Docket_key
		INNER JOIN FeeChargeHistory fch
			ON fch.ID = fcr.FeeChargeHistoryID
    WHERE td.TransactionDetailType = 20
          AND vOperators.operator_key = @MemberKey 
		  AND t.FailedDate IS NULL
		  AND t.TransactionDate BETWEEN @startdate AND @enddate
	group by t.Member_key, datepart(month, fch.StartDate), datepart(year, fch.StartDate)


union
select Sum(isnull(td.amount,0)) as TotalAmount,
'Charge Back Fee '  as descriptions, 0 rentMonth, 0 RentYear
from  [transaction] t 
inner join transactiondetail td on t.transaction_key = td.transaction_key
inner join CadmusIN c on c.docket_key = td.Docket_key
where t.member_key = @MemberKey and (td.TransactionDetailType = 7 and td.Description = 'Charge Back Fee')
--and (Month(t.TransactionDate) =@TranMonth or @TranMonth is null) 
--AND (Year(t.TransactionDate) = @TranYear or @TranYear is null)
and t.faileddate is null and t.TransactionDate  between @startdate and @enddate
group by t.Member_key

order by rentMonth, RentYear
