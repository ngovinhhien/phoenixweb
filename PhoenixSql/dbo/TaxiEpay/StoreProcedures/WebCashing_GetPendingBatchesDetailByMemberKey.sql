﻿ALTER PROCEDURE [dbo].[WebCashing_GetPendingBatchesDetailByMemberKey]
    @MemberKey INT,
    @docketchecksummaryKey INT = NULL,
    @NumberOfMonth INT = NULL
AS
BEGIN

    SET @NumberOfMonth = 355;

    DECLARE @date DATETIME;

    SET @date = DATEADD(MONTH, -ISNULL(@NumberOfMonth, 355), GETDATE());

    SELECT DISTINCT
           'EFT Docket' AS Description,
           d.DocketBatchId,
           c.terminalid,
           c.batchnumber,
           RIGHT(d.DocketBatchId, 4) AS BatchYear,
           SUM(ISNULL(p.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                       c.terminalid,
                                                       c.batchnumber,
                                                       RIGHT(d.DocketBatchId, 4)
                                         ) AS FaceValue,
           SUM(ISNULL(pc.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                        c.terminalid,
                                                        c.batchnumber,
                                                        RIGHT(d.DocketBatchId, 4)
                                          ) AS Commission,
           SUM(ISNULL(po.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                        c.terminalid,
                                                        c.batchnumber,
                                                        RIGHT(d.DocketBatchId, 4)
                                          ) AS Others,
           SUM(ISNULL(p.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                       c.terminalid,
                                                       c.batchnumber,
                                                       RIGHT(d.DocketBatchId, 4)
                                         ) + SUM(ISNULL(pc.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                                                          c.terminalid,
                                                                                          c.batchnumber,
                                                                                          RIGHT(d.DocketBatchId, 4)
                                                                            )
           + SUM(ISNULL(po.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                          c.terminalid,
                                                          c.batchnumber,
                                                          RIGHT(d.DocketBatchId, 4)
                                            ) + SUM(ISNULL(gc.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                                                             c.terminalid,
                                                                                             c.batchnumber,
                                                                                             RIGHT(d.DocketBatchId, 4)
                                                                               ) Total,
           d.DocketDate,
           ISNULL(p.Amount, 0) AS PayableFaceValue,
           ISNULL(pc.RateUsed, 0) CommissionRate,
           ISNULL(pc.Amount, 0) AS PayableCommission,
           ISNULL(po.Amount, 0) PayableOthers,
           ISNULL(p.Amount, 0) + ISNULL(pc.Amount, 0) + ISNULL(po.Amount, 0) + ISNULL(gc.Amount, 0) AS TotalPayable,
           ISNULL(gc.Amount, 0) AS GlideboxCommission,
           SUM(ISNULL(gc.Amount, 0)) OVER (PARTITION BY d.DocketBatchId,
                                                        c.terminalid,
                                                        c.batchnumber,
                                                        RIGHT(d.DocketBatchId, 4)
                                          ) AS TotalGlideboxCommission,
           d.Docket_key
    FROM PendingTransactionDetail p
        INNER JOIN CadmusIN c
            ON c.docket_key = p.Docket_key
        INNER JOIN Member m
            ON m.Member_key = p.Member_key
        INNER JOIN vEftDocket d
            ON d.Docket_key = p.Docket_key
               AND ISNULL(d.Quarantined, 0) = 0
        LEFT JOIN PendingTransactionDetail pc
            ON pc.Docket_key = p.Docket_key
               AND pc.TransactionDetailType = 2
        LEFT JOIN PendingTransactionDetail po
            ON pc.Docket_key = p.Docket_key
               AND pc.TransactionDetailType NOT IN ( 0, 2, 3 )
        LEFT JOIN PendingTransactionDetail gc
            ON gc.Docket_key = p.Docket_key
               AND gc.TransactionDetailType IN ( 21 )
        LEFT JOIN DocketCheckDetail dcd
            ON dcd.Docket_key = p.Docket_key
               AND (dcd.DocketCheckSummaryKey = @docketchecksummaryKey)
    WHERE m.Member_key = @MemberKey
          --and p.TransactionDetailType = 0 
          AND
          (
              p.TransactionDetailType = 0
              OR
              (
                  p.TransactionDetailType = 7
                  AND p.Description = 'Charge Back:Total Fare'
              )
              OR p.TransactionDetailType = 14
          )
          AND
          (
              dcd.DockeCheckDetailKey IS NULL
              OR @docketchecksummaryKey IS NULL
          )
          AND c.filedate > @date
    --and (c.filedate > dateadd(month, -@NumberOfMonth, getdate()) or @NumberOfMonth is null)
    --group by d.DocketBatchId, c.Terminalid,c.batchnumber, right(d.DocketBatchId, 4)
    UNION
    SELECT DISTINCT
           'Insurance Charge' AS Description,
           d.BatchId,
           ta.TerminalId,
           '' batchnumber,
           CAST(DATEPART(YEAR, dep.CreatedDate) AS NVARCHAR(10)) AS BatchYear,
           SUM(ISNULL(p.Amount, 0)) OVER (PARTITION BY d.BatchId,
                                                       ta.TerminalId,
                                                       CAST(DATEPART(YEAR, dep.CreatedDate) AS NVARCHAR(10))
                                         ) AS FaceValue,
           0 AS Commission,
           0 AS Others,
           SUM(ISNULL(p.Amount, 0)) OVER (PARTITION BY d.BatchId,
                                                       ta.TerminalId,
                                                       CAST(DATEPART(YEAR, dep.CreatedDate) AS NVARCHAR(10))
                                         ) AS Total,
           d.DocketDate,
           ISNULL(p.Amount, 0) AS PayableFaceValue,
           0 AS Rateused,
           0 AS PayableCommission,
           0 PayableOthers,
           ISNULL(p.Amount, 0) TotalPayable,
           0 AS GlideboxCommission,
           0 AS TotalGlideboxCommission,
           d.Docket_key
    FROM Deposit dep
        INNER JOIN DocketTable d
            ON d.Docket_key = dep.DepositKey
        INNER JOIN vTerminalAllocations ta
            ON ta.TerminalAllocation_key = dep.TerminalAllocation_key
        INNER JOIN PendingTransactionDetail p
            ON p.Docket_key = dep.DepositKey
        LEFT JOIN DocketCheckDetail dcd
            ON dcd.Docket_key = p.Docket_key
               AND (dcd.DocketCheckSummaryKey = @docketchecksummaryKey)
    WHERE ta.Member_key = @MemberKey
          AND
          (
              dcd.DockeCheckDetailKey IS NULL
              OR @docketchecksummaryKey IS NULL
          )

    --group by d.BatchId, ta.TerminalId, cast(datepart(year,dep.CreatedDate) as nvarchar(10)) 
    ORDER BY BatchYear DESC;

END;