﻿ALTER proc [dbo].[_RPT_CostOfSale] @StartDate date, @EndDate date
as
BEGIN

declare @StartDateTime datetime
 declare @EndDateTime datetime
 declare @month int 
 declare @year int
 
select @Startdate = [dbo].[GetFirstDateOfTheMonth]( @startdate)

select @EndDate = [dbo].[GetFirstDateOfTheMonth]( dateadd(month, 1, @EndDate))

set @StartDateTime = cast (@StartDate as datetime)

set @EndDateTime = cast(@EndDate as datetime)

set @month = DATEPART(MONTH,@StartDate)
set @year = DATEPART(YEAR, @StartDate)



declare @temp table (Type nvarchar(20))

insert into @temp(Type)
values ('Commission'),
('Charge'),
('Unallocated'),
('')

declare @tempStatus table (Status nvarchar(20))

insert into @tempStatus(Status)
values ('Processed'),
('Pending'),
('Unallocated'),
('SameDay'),
('Unknown')

declare @comp table (Company_key int, Name nvarchar(50))

insert into @comp  (Company_key, Name)
select Company_key, Name from Company
union
select 0, 'Unallocated' 


declare @state table (State_key int, StateName nvarchar(20))
insert into @state (State_key, StateName)
select State_key, StateName from State
union
select 0, 'Unallocated' 

create table #cad (
docket_key int,
filedate date,
transtotalamount float,
transfare float,
transextras float,
hostservicecharge decimal(10,2),
hostGST decimal(10,2),
transtip float
)

insert into #cad (docket_key, filedate, transtotalamount, transfare, transextras, hostservicecharge, hostGST,transtip)
select  c.docket_key, filedate, transtotalamount, transfare, transextras, hostservicecharge, hostGST, transtip
from cadmusin c with (nolock)
 where c.filedate >= @StartDate and c.filedate <@EndDate
 and docket_key is not null


IF OBJECT_ID('tempdb..#commcharge') IS NOT NULL DROP TABLE #commcharge

create table #commcharge (


Name varchar(50),
company_key int,
state varchar(10),
yearperiod int,
monthperiod int,
amount money,
status varchar(20),
Type varchar(20)
)

insert into #commcharge (Name, company_key,state,yearperiod,monthperiod,amount,status,Type)
select  com.Name,m.company_key, m.state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month], sum(t.Amount) as [Commission/Charge], 'Processed' as Status, case when t.amount > 0 then 'Commission' else 'Charge' end as Type
from member m  with (nolock)
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin c on c.docket_key = e.eftdocket_key
inner join #cad c on c.docket_key = e.eftdocket_key
inner join DocketTable d on d.docket_key = c.docket_key
inner join TransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2 and t.RateUsed = -0.015)
inner join Company com on com.company_key = m.Company_key
where  (t.TransactionDetail_key is not null) 
and not exists (select a.TransactionDetail_key
 from TransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = t.member_key and a.Amount = -t.Amount)
and c.filedate >= @StartDate and c.filedate <@EndDate
 and m.Company_key = 1
group by com.Name,m.state, m.Company_key, datepart(year, c.filedate), datepart(month, c.filedate), case when t.amount > 0 then 'Commission' else 'Charge' end
union
select com.Name,m.company_key, m.state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month], sum(t.Amount)  as [Commission/Charge], 'Pending' as Status, case when t.amount > 0 then 'Commission' else 'Charge' end as Type
from member m  with (nolock)
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin c on c.docket_key = e.eftdocket_key
inner join #cad c on c.docket_key = e.eftdocket_key
inner join DocketTable d on d.docket_key = c.docket_key
inner join PendingTransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2 and t.RateUsed = -0.015)
inner join Company com on com.company_key = m.Company_key
where  (t.PendingTransactionDetail_key is not null) 
and not exists (select a.PendingTransactionDetail_key
 from PendingTransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = t.member_key and a.Amount = -t.Amount)
and c.filedate >= @StartDate and c.filedate <@EndDate
 and m.Company_key = 1
group by com.Name,m.state, m.Company_key, datepart(year, c.filedate), datepart(month, c.filedate), case when t.amount > 0 then 'Commission' else 'Charge' end
union
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(t.Amount)  as [Commission/Charge], 'SameDay' as Status, case when t.amount > 0 then 'Commission' else 'Charge' end as Type
from member m  with (nolock)
inner join SummaryDocket e on e.Member_key = m.member_key
inner join DOcketTable d on d.Docket_key = e.SummaryDocket_key
inner join TransactionDetail t on t.Docket_key = e.SummaryDocket_key and t.TransactionDetailType = 2  and t.RateUsed = -0.015
inner join Company com on com.company_key = m.Company_key
where  (t.TransactionDetail_key is not null) 
and not exists (select a.TransactionDetail_key
 from TransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = t.member_key and a.Amount = -t.Amount)
and  d.DocketDate >= @StartDateTime and d.DocketDate <@EndDateTime
and m.Company_key = 1
group by com.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.amount > 0 then 'Commission' else 'Charge' end


create table #maintable (

Name varchar(50),
company_key int,
BankMSF float,
state varchar(20),
yearperiod int,
monthperiod int,
turnover money,
status varchar(20),
Type varchar(20),
Farevalue money,
ServiceFee money,
GSTOnServiceFee money,
LevyCharge money,
CommCharge money
)

insert into #maintable (Name, company_key, state, YearPeriod, monthperiod, Type, BankMSF, turnover, Farevalue, ServiceFee, GSTOnServiceFee, CommCharge, Status, LevyCharge)

select com.Name,m.company_key, m.state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month], case when isnull(t.amount,0) > 0 then 'Commission' else 'Charge' end as Type,  
Sum(case when isnull(pm.fixamount,0) = 1 then pm.bankMSF else c.transtotalamount*isnull(pm.bankmsf,0) end ) as BankMSF, 
sum(c.transtotalamount) as turnover, 
--sum(c.transfare+c.transextras+case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then c.transtip else 0 end) as FareValue,  
--sum(isnull(c.hostservicecharge, case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then 0 else c.transtip end)) as ServiceFee,  
--sum(isnull(c.HostGST,case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then 0 else c.transtip end)*0.1) as GSTOnServiceFee,
sum(c.transfare+c.transextras) as Farevalue,
sum(isnull(c.hostservicecharge,0)) as ServiceFee,
sum(isnull(c.HostGST,0)) as GSTOnServiceFee,

sum(t.Amount) as [Commission/Charge], 'Processed' as Status,sum( d.LevyCharge) LevyCharge
from member m with (nolock) -- Paid 
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin c on c.docket_key = e.eftdocket_key
inner join #cad c on c.docket_key = e.eftdocket_key
inner join DocketTable d on d.docket_key = c.docket_key

inner join DocketPayMethod dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
inner join PayMethod pm on pm.PayMethod_key = dpm.PayMethod_key

--inner join TransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2)
inner join TransactionDetail f on f.Docket_key = e.EftDocket_key and (f.TransactionDetailType = 0)
left join TransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2)
inner join Company com on com.company_key = m.Company_key
where  
--(t.TransactionDetail_key is not null) 
--and not exists (select a.TransactionDetail_key
-- from TransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
-- and a.member_key = t.member_key and a.Amount = -t.Amount)
(f.TransactionDetail_key is not null) 
and not exists (select a.TransactionDetail_key
 from TransactionDetail a where a.Docket_key = f.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = f.member_key and a.Amount = -f.Amount)
and c.filedate >= @StartDate and c.filedate <@EndDate
group by com.Name,m.state, m.Company_key, datepart(year, c.filedate), datepart(month, c.filedate), case when isnull(t.amount,0) > 0 then 'Commission' else 'Charge' end
union
select com.Name,m.company_key, m.state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month], case when isnull(t.amount,0) > 0 then 'Commission' else 'Charge' end as Type, 
Sum(case when isnull(pm.fixamount,0) = 1 then pm.bankMSF else c.transtotalamount*isnull(pm.bankmsf,0) end ) as BankMSF, 
sum(c.transtotalamount) as turnover,  sum(c.transfare+c.transextras) as FareValue,sum(isnull(c.hostservicecharge,c.transtip)) as ServiceFee,sum(isnull(c.HostGST,c.transtip*0.1)) as GSTOnServiceFee,  sum(t.Amount)  as [Commission/Charge], 'Pending' as Status,
sum( d.LevyCharge) LevyCharge
from member m  with (nolock) --  Pending
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin c on c.docket_key = e.eftdocket_key
inner join #cad c on c.docket_key = e.eftdocket_key
inner join DocketTable d on d.docket_key = c.docket_key

inner join DocketPayMethod dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
inner join PayMethod pm on pm.PayMethod_key = dpm.PayMethod_key

inner join PendingTransactionDetail f on f.Docket_key = e.EftDocket_key and (f.TransactionDetailType = 0)
left join PendingTransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2)
inner join Company com on com.company_key = m.Company_key
where  (f.PendingTransactionDetail_key is not null) 
and not exists (select a.PendingTransactionDetail_key
 from PendingTransactionDetail a where a.Docket_key = f.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = f.member_key and a.Amount = -t.Amount)
and c.filedate >= @StartDate and c.filedate <@EndDate
group by com.Name,m.state, m.Company_key, datepart(year, c.filedate), datepart(month, c.filedate),case when isnull(t.amount,0) > 0 then 'Commission' else 'Charge' end 
union
select com.Name,m.company_key, m.state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month], '' Type,  
Sum(case when isnull(pm.fixamount,0) = 1 then pm.bankMSF else c.transtotalamount*isnull(pm.bankmsf,0) end ) as BankMSF, 
sum(c.transtotalamount) as turnover, 
--sum(c.transfare+c.transextras+case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then c.transtip else 0 end) as FareValue,  
--sum(isnull(c.hostservicecharge, case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then 0 else c.transtip end)) as ServiceFee,  
--sum(isnull(c.HostGST,case when (c.sourcefile like '%L.csv' or c.sourcefile like '%F.csv' ) then 0 else c.transtip end)*0.1) as GSTOnServiceFee,
sum(c.transfare+c.transextras) as Farevalue,
sum(isnull(c.hostservicecharge,0)) as ServiceFee,
sum(isnull(c.HostGST,0)) as GSTOnServiceFee,

sum(t.Amount) as [Commission/Charge], 'Unknown' as Status, sum( d.LevyCharge) LevyCharge
from member m  with (nolock) -- Paid
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin c on c.docket_key = e.eftdocket_key
inner join #cad c on c.docket_key = e.eftdocket_key
inner join DocketTable d on d.docket_key = c.docket_key

inner join DocketPayMethod dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
inner join PayMethod pm on pm.PayMethod_key = dpm.PayMethod_key

left join SummaryDocket sd on sd.batchid = d.batchid
--inner join TransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 2)
left join TransactionDetail f on f.Docket_key = e.EftDocket_key and (f.TransactionDetailType = 0)
left join PendingTransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 0)
inner join Company com on com.company_key = m.Company_key
where  
--(t.TransactionDetail_key is not null) 
--and not exists (select a.TransactionDetail_key
-- from TransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
-- and a.member_key = t.member_key and a.Amount = -t.Amount)
(f.TransactionDetail_key is null and t.Pendingtransactiondetail_key is null) 
and not exists (select a.TransactionDetail_key
 from TransactionDetail a where a.Docket_key = f.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = f.member_key and a.Amount = -f.Amount)
and c.filedate >= @StartDate and c.filedate <@EndDate
and sd.summarydocket_key  is null
group by com.Name,m.state, m.Company_key, datepart(year, c.filedate), datepart(month, c.filedate)--, case when isnull(t.amount,0) > 0 then 'Commission' else 'Charge' end

union --Sameday
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], case when t.amount > 0 then 'Commission' else 'Charge' end as Type,
Sum(case when isnull(pm.fixamount,0) = 1 then pm.bankMSF else d.DocketTotal*isnull(pm.bankmsf,0) end ) as BankMSF, 
 sum(d.DocketTotal) as turnover,  sum(isnull(d.facevalue,0)+isnull(d.Extras,0)) as FareValue,sum(isnull(d.ServiceCharge,0)) as ServiceFee,sum(isnull(d.gst,0)) as GSTOnServiceFee,sum(t.Amount)  as [Commission/Charge], 
 'SameDay' as Status, sum( d.LevyCharge) LevyCharge
from member m  with (nolock)
inner join SummaryDocket e on e.Member_key = m.member_key
inner join DOcketTable d on d.Docket_key = e.SummaryDocket_key
inner join DocketPayMethod dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
inner join PayMethod pm on pm.PayMethod_key = dpm.PayMethod_key
inner join TransactionDetail t on t.Docket_key = e.SummaryDocket_key and t.TransactionDetailType = 2
inner join Company com on com.company_key = m.Company_key
where  (t.TransactionDetail_key is not null) 
and not exists (select a.TransactionDetail_key
 from TransactionDetail a where a.Docket_key = t.docket_key --and a.TransactionDetailType > 20 
 and a.member_key = t.member_key and a.Amount = -t.Amount)
and  d.DocketDate >= @StartDateTime and d.DocketDate <@EndDateTime
group by com.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.amount > 0 then 'Commission' else 'Charge' end

union 
select 'Unallocated' Name, 0 Company_key, 'Unallocated' state, datepart(year, c.filedate) as [Year], datepart(month, c.filedate) as [Month],  'Unallocated'  as Type,  
Sum(case when isnull(pm.fixamount,0) = 1 then pm.bankMSF else c.transtotalamount*isnull(pm.bankmsf,0) end ) as BankMSF, 
sum(c.transtotalamount) as turnover, 
sum(c.transfare+c.transextras) as Farevalue,
sum(isnull(c.hostservicecharge,0)) as ServiceFee,
sum(isnull(c.HostGST,0)) as GSTOnServiceFee,
0 as [Commission/Charge], 'Unallocated' as Status, sum( d.LevyCharge) LevyCharge
--from  Cadmusin c  with (nolock)
from  #cad c  with (nolock)
inner join DocketTable d on d.docket_key = c.docket_key
inner join eftdocket e on d.docket_key = e.eftdocket_key and e.Operator_key is null
left join DocketPayMethod dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
left join PayMethod pm on pm.PayMethod_key = dpm.PayMethod_key
where   c.filedate >= @StartDate and c.filedate <@EndDate

group by  datepart(year, c.filedate), datepart(month, c.filedate)


create table #autotip
(
Name varchar(50),
company_key int,
state varchar(10),
yearperiod int,
monthperiod int,
AutoTipFee money,
status varchar(20),
Type varchar(20)
)

insert into #autotip  (Name, company_key,state,yearperiod,monthperiod,Type,AutoTipFee,status)
select co.Name,m.company_key, m.state, datepart(year, cad.filedate) as [Year], datepart(month, cad.filedate) as [Month], 'Auto Tip Fee' as Type,
sum(isnull(f.amount,0))+sum(isnull(t.Amount,0)) as AutoTipFee, 'Pending' as Status
from member m  with (nolock) --  Pending
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin cad on cad.docket_key = e.eftdocket_key
inner join #cad cad on cad.docket_key = e.eftdocket_key
--inner join DocketTable d on d.docket_key = cad.docket_key
inner join company co on co.Company_key = m.Company_key
inner join PendingTransactionDetail f on f.Docket_key = e.EftDocket_key and (f.TransactionDetailType = 14)
left join PendingTransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 7 and t.Amount = -f.Amount)
where  
--datepart(year, cad.filedate) = @year and datepart(month, cad.filedate) = @month
cad.filedate between @startdate and @enddate
--and m.State = s.StateName  and te.Type = 'Charge' and ts.Status = 'Pending' and m.Company_key = com.Company_key
group by co.Name,m.state, m.Company_key, datepart(year, cad.filedate), datepart(month, cad.filedate)

 union 
select co.Name,m.company_key, m.state, datepart(year, cad.filedate) as [Year], datepart(month, cad.filedate) as [Month], 'Auto Tip Fee' as Type,
sum(isnull(tf.amount,0))+sum(isnull(tc.Amount,0)) as AutoTipFee, 'Processed' as Status
from member m  with (nolock) --  Pending
inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin cad on cad.docket_key = e.eftdocket_key
inner join #cad cad on cad.docket_key = e.eftdocket_key
inner join company co on co.Company_key = m.Company_key
inner join Transactiondetail tf on tf.Docket_key = e.EftDocket_key and (tf.TransactionDetailType = 14)
left join TransactionDetail tc on tc.Docket_key = e.EftDocket_key and (tc.TransactionDetailType = 7 and tc.Amount = -tf.Amount)

where  
--datepart(year, cad.filedate) = @year and datepart(month, cad.filedate) = @month
cad.filedate between @startdate and @enddate
--and m.State = s.StateName  and te.Type = 'Charge' and ts.Status = 'Processed' and m.Company_key = com.Company_key
group by co.Name,m.state, m.Company_key, datepart(year, cad.filedate), datepart(month, cad.filedate)



--select c.Company_key, c.BankMSF as BankMSF, c.[Commission/Charge] as [Commission/Charge] , c.GSTOnServiceFee, c.FareValue as FareValue, c.Month, c.Name, c.ServiceFee, c.State, c.Status, c.Type, c.Year, c.turnover, 
select distinct  com.Company_key, c.BankMSF as BankMSF, c.CommCharge as [Commission/Charge] , c.GSTOnServiceFee, c.FareValue as FareValue, @month as Month, com.Name, c.ServiceFee, s.StateName as State, ts.Status, te.Type, @year as Year, c.turnover, 
h.amount as [Handling Charge], pins.InsuranceCharge as PaidInsurance,--tins.InsuranceCharge as PendingInsuranceCharge, 
r.RentalCharge , ta.AutoTipFee, c.LevyCharge, acf.AccountFeeCharge, racf.RefundAccountFeeCharge
from 
@temp te
left join @tempStatus ts on 1=1
left join @comp com on 1=1
left join @state s on 1=1
left join #maintable c on te.Type = c.Type collate SQL_Latin1_General_CP1_CI_AS
and c.Status = ts.Status collate SQL_Latin1_General_CP1_CI_AS
and c.Company_key = com.Company_key  
and c.State = s.StateName collate SQL_Latin1_General_CP1_CI_AS
and c.monthperiod= @month and c.yearperiod = @year
left join #commcharge h on   h.Company_key = com.Company_key 
and h.state = s.StateName collate SQL_Latin1_General_CP1_CI_AS
and h.[yearperiod] = @year 
and h.[monthperiod] = @month 
and h.Status = ts.Status collate SQL_Latin1_General_CP1_CI_AS
and h.Type = te.Type collate SQL_Latin1_General_CP1_CI_AS

--Paid insurance charge
outer apply(
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(e.DepositAmount) as InsuranceCharge, case when t.amount > 0 then 'Commission' else 'Charge' end as Type
from Deposit e  with (nolock)
inner join terminalallocation ta on ta.TerminalAllocation_key = e.TerminalAllocation_key
inner join member m on ta.Member_key= m.member_key
inner join DOcketTable d on d.Docket_key = e.DepositKey
inner join TransactionDetail t on t.Docket_key = e.DepositKey --and t.TransactionDetailType = 8
inner join Company co on com.company_key = m.Company_key
where --datepart(year, d.DocketDate) = @year and datepart(month, d.DocketDate) = @month 
--and 
(t.TransactionDetail_key is not null ) and ts.Status = 'Processed' 
and  d.DocketDate >= @StartDateTime and d.DocketDate <@EndDateTime 
 and m.Company_key = com.Company_key and m.state = s.StateName and ((te.Type = 'Charge' and t.Amount <0) or (te.Type = 'Commission' and t.Amount>0))
group by co.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.amount > 0 then 'Commission' else 'Charge' end
) pins

----Pending insurance charge
--outer apply(
--select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(e.DepositAmount) as InsuranceCharge
--from Deposit e 
--inner join terminalallocation ta on ta.TerminalAllocation_key = e.TerminalAllocation_key
--inner join member m on ta.Member_key= m.member_key
--inner join DOcketTable d on d.Docket_key = e.DepositKey
--inner join PendingTransactionDetail p on p.Docket_key = e.DepositKey --and t.TransactionDetailType = 8
--inner join Company com on com.company_key = m.Company_key
--where datepart(year, d.DocketDate) = c.[Year] and datepart(month, d.DocketDate) = c.[Month] and (t.TransactionDetail_key is not null or p.PendingTransactionDetail_key is not null) 
--and d.DocketDate> '20140101' and m.Company_key = c.Company_key and m.state = c.State
--group by com.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate) 

--) tins
--rental charge
outer apply(
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(e.RentAmount) as RentalCharge,
case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end as Status
from TerminalRentalHistory e   with (nolock)
inner join terminalallocation ta on ta.TerminalAllocation_key = e.TerminalAllocation_key
inner join member m on ta.Member_key= m.member_key
inner join DOcketTable d on d.Docket_key = e.TerminalRentalHistory_key
left join TransactionDetail t on t.Docket_key = e.TerminalRentalHistory_key --and t.TransactionDetailType = 8
left join PendingTransactionDetail p on p.Docket_key = e.TerminalRentalHistory_key --and t.TransactionDetailType = 8
inner join Company co on com.company_key = m.Company_key
where --datepart(year, d.DocketDate) = @year and datepart(month, d.DocketDate) = @month 
--and 
(t.TransactionDetail_key is not null or p.PendingTransactionDetail_key is not null) 
and  d.DocketDate >= @StartDateTime and d.DocketDate <@EndDateTime and m.Company_key = com.Company_key and m.state = s.StateName and te.Type = 'Charge' and ((ts.Status = 'Processed' and t.TransactionDetail_key is not null) or (ts.Status = 'Pending' and p.PendingTransactionDetail_key is not null))
group by co.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end
) r

--account fee charge

--
outer apply(
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(e.Amount) as AccountFeeCharge,
case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end as Status
from FeeChargeHistory e   with (nolock)
inner join FeeChargeDetail fd on fd.ID = e.FeeChargeDetailId
inner join member m on fd.MemberKey= m.member_key
inner join DOcketTable d on d.Docket_key = e.DocketKey
left join  TransactionDetail t on t.Docket_key = e.DocketKey and t.TransactionDetailType = 19
left join PendingTransactionDetail p on p.Docket_key = e.DocketKey and p.TransactionDetailType = 19
inner join Company co on com.company_key = m.Company_key
where --datepart(year, d.DocketDate) = @year and datepart(month, d.DocketDate) = @month 
--and 
(t.TransactionDetail_key is not null or p.PendingTransactionDetail_key is not null) 
and  d.DocketDate >= @StartDateTime 
and d.DocketDate <@EndDateTime 
and m.Company_key = com.Company_key 
and m.state = s.StateName 
and te.Type = 'Charge' 
and ((ts.Status = 'Processed' and t.TransactionDetail_key is not null) or (ts.Status = 'Pending' and p.PendingTransactionDetail_key is not null))
group by co.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end
) acf

-- refund account fee charge
outer apply(
select com.Name, m.company_key, m.state, datepart(year, d.DocketDate) as [Year], datepart(month, d.DocketDate) as [Month], sum(-e.RefundAmount) as RefundAccountFeeCharge,
case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end as Status
from dbo.FeeChargedRefund e   with (nolock)
INNER JOIN dbo.FeeChargeHistory fch ON e.FeeChargeHistoryID = fch.ID
inner join FeeChargeDetail fd on fd.ID = fch.FeeChargeDetailId
inner join member m on fd.MemberKey= m.member_key
inner join DOcketTable d on d.Docket_key = e.DocketKey
left join  TransactionDetail t on t.Docket_key = e.DocketKey and t.TransactionDetailType = 20
left join PendingTransactionDetail p on p.Docket_key = e.DocketKey and p.TransactionDetailType = 20
inner join Company co on com.company_key = m.Company_key
where --datepart(year, d.DocketDate) = @year and datepart(month, d.DocketDate) = @month 
--and 
(t.TransactionDetail_key is not null or p.PendingTransactionDetail_key is not null) 
and  d.DocketDate >= @StartDateTime 
and d.DocketDate <@EndDateTime 
and m.Company_key = com.Company_key 
and m.state = s.StateName 
and te.Type = 'Charge' 
and ((ts.Status = 'Processed' and t.TransactionDetail_key is not null) or (ts.Status = 'Pending' and p.PendingTransactionDetail_key is not null))
group by co.Name,m.state, m.Company_key, datepart(year, d.DocketDate), datepart(month, d.DocketDate), case when t.TransactionDetail_key is not null then 'Processed' when p.PendingTransactionDetail_key is not null then 'Pending' else 'Canceled' end
) racf

left join #autotip ta on s.StateName = ta.state  collate SQL_Latin1_General_CP1_CI_AS
and te.Type = 'Charge'  collate SQL_Latin1_General_CP1_CI_AS
and ts.Status = ta.status  collate SQL_Latin1_General_CP1_CI_AS
and com.Company_key = ta.company_key
and ta.monthperiod= @month and ta.yearperiod = @year

--outer apply(
--select com.Name,m.company_key, m.state, datepart(year, cad.filedate) as [Year], datepart(month, cad.filedate) as [Month], 'Auto Tip Fee' as Type,
--sum(isnull(f.amount,0))+sum(isnull(t.Amount,0)) as AutoTipFee, 'Pending' as Status
--from member m  with (nolock) --  Pending
--inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin cad on cad.docket_key = e.eftdocket_key
----inner join DocketTable d on d.docket_key = cad.docket_key
--inner join company co on co.Company_key = m.Company_key
--inner join PendingTransactionDetail f on f.Docket_key = e.EftDocket_key and (f.TransactionDetailType = 14)
--left join PendingTransactionDetail t on t.Docket_key = e.EftDocket_key and (t.TransactionDetailType = 7 and t.Amount = -f.Amount)
--where  
----datepart(year, cad.filedate) = @year and datepart(month, cad.filedate) = @month
--cad.filedate between @startdate and @enddate
--and m.State = s.StateName  and te.Type = 'Charge' and ts.Status = 'Pending' and m.Company_key = com.Company_key
--group by co.Name,m.state, m.Company_key, datepart(year, cad.filedate), datepart(month, cad.filedate)

-- union 
--select com.Name,m.company_key, m.state, datepart(year, cad.filedate) as [Year], datepart(month, cad.filedate) as [Month], 'Auto Tip Fee' as Type,
--sum(isnull(tf.amount,0))+sum(isnull(tc.Amount,0)) as AutoTipFee, 'Processed' as Status
--from member m  with (nolock) --  Pending
--inner join eftdocket e on e.operator_key = m.member_key
--inner join Cadmusin cad on cad.docket_key = e.eftdocket_key
--inner join company co on co.Company_key = m.Company_key
--inner join Transactiondetail tf on tf.Docket_key = e.EftDocket_key and (tf.TransactionDetailType = 14)
--left join TransactionDetail tc on tc.Docket_key = e.EftDocket_key and (tc.TransactionDetailType = 7 and tc.Amount = -tf.Amount)

--where  
----datepart(year, cad.filedate) = @year and datepart(month, cad.filedate) = @month
--cad.filedate between @startdate and @enddate
--and m.State = s.StateName  and te.Type = 'Charge' and ts.Status = 'Processed' and m.Company_key = com.Company_key
--group by co.Name,m.state, m.Company_key, datepart(year, cad.filedate), datepart(month, cad.filedate)

--) ta
where (c.BankMSF is not null or c.CommCharge is not null or GSTOnServiceFee is not null or c.Farevalue is not null 
or c.ServiceFee is not null or c.turnover is not null or h.amount is not null or pins.InsuranceCharge is not null
or r.RentalCharge is not null or ta.AutoTipFee is not null or acf.AccountFeeCharge is not NULL OR racf.RefundAccountFeeCharge IS NOT NULL)

end
