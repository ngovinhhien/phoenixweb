﻿ALTER PROCEDURE [dbo].[CopySwappedOutTerminalConfiguration]
AS
BEGIN;



declare @newTID nvarchar(20)
declare @oldTID nvarchar(20)
declare @terminalallocation_key int
declare  @updated_by varchar(20) = 'AutoProcess'
declare  @updated_date DateTime = GETDATE()
declare @CurrentGlideboxItemsOldTerminalID VARCHAR(200)
declare @CurrentGlideboxItemsNewTerminalID VARCHAR(200)
declare @GlideboxItemID INT
declare @BusinessTypeAllowGlideBox VARCHAR(100)
declare @IsProcessingGlideboxData VARCHAR(20)

SELECT @IsProcessingGlideboxData = ConfigurationValue 
FROM dbo.SystemConfiguration
WHERE ConfigurationKey = 'DisplayGlideboxUI' AND SystemID = 2

declare tacursor cursor
for
--get a list of swap out terminals of which configuration haven't been coppied
select nv.HostTID as NewHostTID, ov.HostTID as OldHostTID, a.TerminalAllocation_key  from TerminalAllocation a
left join TerminalAllocation sa on sa.TerminalAllocation_key = a.SwappedTerminalAllocation_key
left join PhoenixHostTerminalView nv on a.EftTerminal_key = nv.EftTerminal_key
left join PhoenixHostTerminalView ov on sa.EftTerminal_key = ov.EftTerminal_key
--left join TransactionHost.dbo.terminal nt on nt.TerminalID = nv.HostTID
--left join TransactionHost.dbo.Terminal ot on ot.TerminalID = ov.HostTID
where a.EffectiveDate > '2016-09-22 16:30' and a.copyconfiguration is null 
and a.IsCurrent = 1 
and a.member_key is not null and a.EftTerminal_key is not null and a.SwappedTerminalAllocation_key is not null

open tacursor

fetch next from tacursor
into @newTID, @oldTID, @terminalallocation_key


WHILE @@FETCH_STATUS = 0
BEGIN 

DECLARE @BusinessTypeKey INT;
SELECT @BusinessTypeKey = Company_key
FROM dbo.TerminalAllocation ta
    JOIN dbo.Member
        ON Member.Member_key = ta.Member_key
WHERE ta.TerminalAllocation_key = @terminalallocation_key;

IF ISNULL(@IsProcessingGlideboxData, '') = 'true'
BEGIN
	SELECT @BusinessTypeAllowGlideBox = ConfigurationValue FROM dbo.SystemConfiguration WHERE ConfigurationKey = 'BusinessTypeEnabledGlideBox'

	IF (CHARINDEX(CAST(@BusinessTypeKey AS VARCHAR(100)), @BusinessTypeAllowGlideBox) > 0) --only run for terminal belong to taxi group
	BEGIN
		--Update Terminalhistory for enable/disable glidebox
		INSERT INTO Transactionhost.dbo.TerminalHistory
		(
			TerminalID,
			EnableGlidebox,
			Reason,
			Source,
			ChangedDatetime,
			ChangedBy
		)
		SELECT t1.TerminalID,
			   t2.EnableGlidebox,
			   NULL,
			   'SwapTerminal',
			   @updated_date,
			   @updated_by
		FROM Transactionhost.dbo.Terminal t1
			CROSS APPLY
		(
			SELECT *
			FROM Transactionhost.dbo.Terminal
			WHERE TerminalID = @oldTID
		) t2
		WHERE t1.TerminalID = @newTID
			  AND t1.EnableGlidebox <> t2.EnableGlidebox; --only add history when glide value is changed
	END;
END


--copy configuration
UPDATE nt SET  AppKey = ot.AppKey, 
ReceiptLogoKey = ot.ReceiptLogoKey, 
ScreenLogoKey = ot.ScreenLogoKey, 
PromptKey = ot.PromptKey, 
TerminalConfigKey = ot.TerminalConfigKey, 
TerminalBINRangeKey = ot.TerminalBINRangeKey, 
TerminalNetworkKey = ot.TerminalNetworkKey,
 StateKey = ot.StateKey,
  DiscountApplied = ot.DiscountApplied, 
  ReceiptKey = ot.ReceiptKey, 
 UserID = ot.UserID,
 BusinessID = ot.BusinessID,
 ABN = ot.ABN, 
 InputTimeoutSec = ot.InputTimeoutSec, 
 BacklightSec = ot.BacklightSec, 
 SleepModeMin = ot.SleepModeMin, 
 ScreenBrightness = ot.ScreenBrightness, 
 Sound = ot.Sound, 
 RemeberMe = ot.RemeberMe,
  ABNSetting = ot.ABNSetting, 
  MinimumCharge = ot.MinimumCharge, 
  MaximumCharge = ot.MaximumCharge, 
  EOSTime = ot.EOSTime, 
  LoginMode = ot.LoginMode,
  UserIdConfig = ot.UserIdConfig, 
  BusinessIdConfig = ot.BusinessIdConfig, 
  ABNConfig = ot.ABNConfig, 
  NetworkTableStatus = ot.NetworkTableStatus, 
  TNNetworkKey = ot.TNNetworkKey, 
  GPRSReconnectTimer = ot.GPRSReconnectTimer, 
  AppRespCode = ot.AppRespCode, 
  AppSignRespCode = ot.AppSignRespCode, 
  IsPreTranPing = ot.IsPreTranPing, 
  TranAdviceSendMode = ot.TranAdviceSendMode, 
 DefaultServiceFee = ot.DefaultServiceFee, 
 ServiceFeeMode = ot.ServiceFeeMode, 
 CustomerPaymentType = ot.CustomerPaymentType, 
 MainRegionKey = ot.MainRegionKey, 
 TipTrickKey = ot.TipTrickKey, 
 TerminalLocationKey = ot.TerminalLocationKey,
 EnableNetworkReceiptPrint = ot.EnableNetworkReceiptPrint, 
 EnableMerchantLogoPrint = ot.EnableMerchantLogoPrint, 
 SignatureVerificationTimeout = ot.SignatureVerificationTimeout,
  AutoLogonHour = ot.AutoLogonHour, 
  AutoLogonMode = ot.AutoLogonMode, 
  EnableRefundTran = ot.EnableRefundTran, 
  EnableAuthTran = ot.EnableAuthTran, 
  EnableTipTran = ot.EnableTipTran, 
  EnableCashOutTran = ot.EnableCashOutTran,
  MerchantPassword = ot.MerchantPassword, 
  BankLogonRespCode = ot.BankLogonRespCode, 
  DefaultBackgroundColour = ot.DefaultBackgroundColour, 
  EnableMOTO = ot.EnableMOTO, 
  BusinessTypeKey = ot.BusinessTypeKey, 
  RebootHours = ot.RebootHours, 
  EnableAutoTip = ot.EnableAutoTip, 
  EnableMOTORefund = ot.EnableMOTORefund, 
  AutoEOSMode = ot.AutoEOSMode, 
  CellTowerUpdateInterval = ot.CellTowerUpdateInterval,
   UpdateCellTowerMode = ot.UpdateCellTowerMode, 
   AutoTipNumOne = ot.AutoTipNumOne, 
   AutoTipNumTwo = ot.AutoTipNumTwo, 
   AutoTipNumThree = ot.AutoTipNumThree, 
   AutoTipNumFour = ot.AutoTipNumFour, 
   AutoTipIsPercentOne = ot.AutoTipIsPercentOne, 
   AutoTipIsPercentTwo = ot.AutoTipIsPercentTwo, 
   AutoTipIsPercentThree = ot.AutoTipIsPercentThree, 
   AutoTipIsPercentFour = ot.AutoTipIsPercentFour, 
   AutoTipButtonOneID = ot.AutoTipButtonOneID, 
   AutoTipButtonTwoID = ot.AutoTipButtonTwoID, 
   AutoTipButtonThreeID = ot.AutoTipButtonThreeID, 
   AutoTipButtonFourID = ot.AutoTipButtonFourID,
   AutoTipDefaultButtonID = ot.AutoTipDefaultButtonID, 
   AutoTipScreenText = ot.AutoTipScreenText, 
   AutoEOSNotificationMode = ot.AutoEOSNotificationMode, 
   AutoEOSOutputMode = ot.AutoEOSOutputMode, 
   AutoTipTwoNumOne = ot.AutoTipTwoNumOne, 
   AutoTipTwoNumTwo = ot.AutoTipTwoNumTwo, 
   AutoTipTwoNumThree = ot.AutoTipTwoNumThree, 
   AutoTipTwoNumFour = ot.AutoTipTwoNumFour,
   AutoTipTwoIsPercentOne = ot.AutoTipTwoIsPercentOne, 
   AutoTipTwoIsPercentTwo = ot.AutoTipTwoIsPercentTwo, 
   AutoTipTwoIsPercentThree = ot.AutoTipTwoIsPercentThree,
   AutoTipTwoIsPercentFour = ot.AutoTipTwoIsPercentFour, 
   AutoTipTwoButtonOneID = ot.AutoTipTwoButtonOneID, 
   AutoTipTwoButtonTwoID = ot.AutoTipTwoButtonTwoID, 
   AutoTipTwoButtonThreeID = ot.AutoTipTwoButtonThreeID, 
   AutoTipTwoButtonFourID = ot.AutoTipTwoButtonFourID, 
   AutoTipTwoDefaultButtonID = ot.AutoTipTwoDefaultButtonID, 
   AutoTipTwoScreenText = ot.AutoTipTwoScreenText, 
   EnableAutoTipTwo = ot.EnableAutoTipTwo, 
   EnableReceiptReference = ot.EnableReceiptReference, 
   BusinessTypeIndicator = ot.BusinessTypeIndicator,
   SkinSelection = ot.SkinSelection,
   
   nt.EnableGlidebox = CASE 
						WHEN ISNULL(@IsProcessingGlideboxData, '') = 'true' 
							THEN  
							(
								CASE 
									WHEN (CHARINDEX(CAST(@BusinessTypeKey AS VARCHAR(100)), @BusinessTypeAllowGlideBox) > 0) 
										THEN 
											ot.EnableGlidebox 
										ELSE 
											0 
								END
							) 
						ELSE 
							nt.EnableGlidebox 
					END,
   VersionNo =nt.VersionNo+1--,
   --SkinSelection = ot.SkinSelection
FROM TransactionHost.dbo.terminal nt
INNER JOIN  TransactionHost.dbo.terminal ot ON ot.TerminalID = @oldTID
WHERE nt.TerminalID = @newTID;

IF ISNULL(@IsProcessingGlideboxData, '') = 'true'
BEGIN
	IF (CHARINDEX(CAST(@BusinessTypeKey AS VARCHAR(100)), @BusinessTypeAllowGlideBox) > 0) --only run for terminal belong to taxi group
	BEGIN
		--Create history when assign categories to terminal
		DECLARE @terminalCategoryHistory TABLE
		(
			CategoryID INT,
			TerminalID VARCHAR(10),
			OldOrder INT,
			NewOrder INT,
			IsActive BIT,
			IsDelete BIT,
			Source VARCHAR(200),
			ChangedDatetime DATETIME,
			ChangedBy VARCHAR(50)
		);

		--History of new categories for new terminal
		INSERT INTO @terminalCategoryHistory
		(
			CategoryID,
			TerminalID,
			OldOrder,
			NewOrder,
			IsActive,
			IsDelete,
			Source,
			ChangedDatetime,
			ChangedBy
		)
		SELECT old.CategoryID,
			   @newTID,
			   NULL,
			   old.OrderCategory,
			   1,
			   NULL,
			   'SwapTerminal',
			   @updated_date,
			   @updated_by
		FROM Transactionhost.dbo.TerminalCategory old
		WHERE TerminalID = @oldTID
			  AND CategoryID NOT IN
				  (
					  SELECT new.CategoryID
					  FROM Transactionhost.dbo.TerminalCategory new
					  WHERE TerminalID = @newTID
				  );

		--History of delete categories for new terminal
		INSERT INTO @terminalCategoryHistory
		(
			CategoryID,
			TerminalID,
			OldOrder,
			NewOrder,
			IsActive,
			IsDelete,
			Source,
			ChangedDatetime,
			ChangedBy
		)
		SELECT new.CategoryID,
			   @newTID,
			   NULL,
			   NULL,
			   NULL,
			   1,
			   'SwapTerminal',
			   @updated_date,
			   @updated_by
		FROM Transactionhost.dbo.TerminalCategory new
		WHERE TerminalID = @newTID
			  AND new.CategoryID NOT IN
				  (
					  SELECT old.CategoryID
					  FROM Transactionhost.dbo.TerminalCategory old
					  WHERE TerminalID = @oldTID
				  );

		--History of update categories for new terminal
		INSERT INTO @terminalCategoryHistory
		(
			CategoryID,
			TerminalID,
			OldOrder,
			NewOrder,
			IsActive,
			IsDelete,
			Source,
			ChangedDatetime,
			ChangedBy
		)
		SELECT old.CategoryID,
			   @newTID,
			   new.OrderCategory,
			   old.OrderCategory,
			   NULL,
			   NULL,
			   'SwapTerminal',
			   @updated_date,
			   @updated_by
		FROM Transactionhost.dbo.TerminalCategory old
			CROSS APPLY
		(
			SELECT *
			FROM Transactionhost.dbo.TerminalCategory new
			WHERE TerminalID = @newTID
		) new
		WHERE old.TerminalID = @oldTID
			  AND old.CategoryID = new.CategoryID
			  AND old.OrderCategory <> new.OrderCategory; --only update when diffrence order number

		SELECT @CurrentGlideboxItemsOldTerminalID = GlideboxItem
		FROM Transactionhost.dbo.Glidebox
		WHERE TerminalID = @oldTID
			
		SELECT @CurrentGlideboxItemsNewTerminalID = GlideboxItem, @GlideboxItemID = GlideboxID
		FROM Transactionhost.dbo.Glidebox
		WHERE TerminalID = @newTID

		--Delete categories in new terminal before clone
		DELETE FROM Transactionhost.dbo.TerminalCategory
		WHERE TerminalID = @newTID;

		--Copy all category from old terminal to new terminal
		INSERT INTO Transactionhost.dbo.TerminalCategory
		(
			CategoryID,
			TerminalID,
			OrderCategory,
			IsActive,
			AssignedDatetime,
			AssignedBy
		)
		SELECT tc.CategoryID,
			   @newTID,
			   tc.OrderCategory,
			   tc.IsActive,
			   @updated_date,
			   @updated_by
		FROM Transactionhost.dbo.TerminalCategory tc
		WHERE TerminalID = @oldTID;

		--Insert terminal category history after copy categories
		INSERT INTO Transactionhost.dbo.TerminalCategoryHistory
		(
			CategoryID,
			TerminalID,
			OldOrder,
			NewOrder,
			IsActive,
			IsDelete,
			Source,
			ChangedDatetime,
			ChangedBy
		)
		SELECT CategoryID,
			   TerminalID,
			   OldOrder,
			   NewOrder,
			   IsActive,
			   IsDelete,
			   Source,
			   ChangedDatetime,
			   ChangedBy
		FROM @terminalCategoryHistory;

		IF @GlideboxItemID IS NOT NULL
		BEGIN
			IF @CurrentGlideboxItemsOldTerminalID <> @CurrentGlideboxItemsNewTerminalID
			BEGIN
				UPDATE Transactionhost.dbo.Glidebox
				SET GlideboxItem = ISNULL(@CurrentGlideboxItemsOldTerminalID, ''), 
					VersionNo = VersionNo + 1,
					UpdatedDatetime = GETDATE(),
					UpdatedBy = 'SwapTerminal'
				WHERE TerminalID = @newTID
			END
		END
		ELSE
		BEGIN
			INSERT INTO Transactionhost.dbo.Glidebox
			(
				TerminalID,
				GlideboxItem,
				VersionNo,
				CreatedDatetime,
				CreatedBy
			)
			VALUES
			(
				@newTID,
				ISNULL(@CurrentGlideboxItemsOldTerminalID, ''),
				1,
				GETDATE(),
				'SwapTerminal'
			)
		END

		DELETE FROM @terminalCategoryHistory
	END;
END
--update terminal allocation record
UPDATE TerminalAllocation SET CopyConfiguration = GETDATE() WHERE TerminalAllocation_key = @terminalallocation_key


FETCH NEXT FROM tacursor
INTO @newTID, @oldTID, @terminalallocation_key

END

CLOSE tacursor;

DEALLOCATE tacursor;
   
   
END


GO
