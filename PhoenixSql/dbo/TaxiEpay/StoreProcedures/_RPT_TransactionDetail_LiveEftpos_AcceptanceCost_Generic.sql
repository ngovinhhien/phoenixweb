﻿ALTER PROC [dbo].[_RPT_TransactionDetail_LiveEftpos_AcceptanceCost_Generic] @memberKey int, @startdate datetime, @enddate datetime
AS

--declare @startdate datetime
--declare @enddate datetime

--select @startdate = FirstDateOfMonth, @enddate = FirstDateOfNextMonth from FirstDateOfMonth where MonthPart = @month and YearPart = @year

select dp.Name as CardType, ISNULL(s.Transactions, 0) as Transactions, isnull(s.NetSales,0) as NetSales, isnull(s.TotalMSF,0) as TotalMSF,
--ISNULL( s.CurrentMSF,0) as CurrentMSF, 
ISNULL( cr.OperatorCommissionRate,0) as CurrentMSF, 
isnull(s.AverageMSF,0) as AverageMSF, SUM(isnull(s.NetSales,0)) over (partition by s.Member_key) TotalTransactions,
case when ISNULL(s.Transactions, 0)<>0 then r.RentalAmount else 0 end as RentalAmount, 
case when ISNULL(s.Transactions, 0)<>0 then isnull(cb.ChargeBackFee,0) else 0 end as ChargeBackFee,
case when ISNULL(s.Transactions, 0)<>0 then isnull(acf.AccountFee,0) else 0 end as AccountFee,
case when ISNULL(s.Transactions, 0)<>0 then (isnull(r.RentalAmount,0) + isnull(acf.AccountFee, 0)) else 0 end as TotalFixedFee
--case when ISNULL(s.Transactions, 0)<>0 then ISNULL(s.Transactions, 0)/(SUM(isnull(s.Transactions,0)) over (partition by s.Member_key)) else 0 end as ShareOfSales -- case when s.Transactions>0 then r.RentalAmount/sum(s.Transactions else 0 end as FixedFee
from DocketPayMethodLookupTable dp
inner join member m on m.Member_key = @memberKey and m.Company_key = dp.Company_key
left join MemberCommissionRate cr on cr.Member_key = m.Member_key and cr.DocketPayMethod_key = dp.DocketPayMethod_key
--outer apply (
--select dpm.DocketPayMethod_key, 
----SUM(isnull(c.transtotalamount,0)) + SUM(isnull(t.amount,0)) as NetSales,  
--SUM(isnull(c.transtotalamount,0))as NetSales, 
--SUM(isnull(t.Amount,0)) TotalMSF, COUNT(d.Docket_key) Transactions, 
--avg(isnull(t.RateUsed,0)) AverageMSF, --MIN(cr.OperatorCommissionRate) as CurrentMSF, 
--AVG(isnull(t.Amount,0)) AvargeMSF, h.Member_key
--from TransactionDetail t
--inner join [Transaction] h on h.Transaction_key = t.Transaction_key and h.TransactionDate >= @startdate and  h.TransactionDate < @enddate
--inner join DocketTable d on d.Docket_key = t.Docket_key
--inner join DocketPayMethodLookupTable dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
--inner join CadmusIN c on c.docket_key = d.Docket_key
----left join MemberCommissionRate cr on cr.Member_key = h.Member_key and cr.DocketPayMethod_key = d.DocketPayMethod_key
--where t.Member_key = m.Member_key and t.TransactionDetailType in (2) and dpm.DocketPayMethod_key = dp.DocketPayMethod_key
--group by dpm.DocketPayMethod_key, dpm.Name, h.Member_key
--) s
outer apply (
select dpm.DocketPayMethod_key, 
SUM(isnull(c.transtotalamount,0))as NetSales, 
SUM(isnull(t.Amount,0)) TotalMSF, COUNT(d.Docket_key) Transactions, 
avg(isnull(t.RateUsed,0)) AverageMSF, --MIN(cr.OperatorCommissionRate) as CurrentMSF, 
AVG(isnull(t.Amount,0)) AvargeMSF, h.Member_key
from TransactionDetail p
inner join [Transaction] h on h.Transaction_key = p.Transaction_key and h.TransactionDate >= @startdate and  h.TransactionDate < @enddate
outer apply (
select SUM(t.Amount) as Amount, AVG(t.RateUsed) as RateUsed  from TransactionDetail t where t.Docket_key = p.Docket_key and t.TransactionDetailType = 2 
) t
inner join DocketTable d on d.Docket_key = p.Docket_key
inner join DocketPayMethodLookupTable dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
inner join CadmusIN c on c.docket_key = d.Docket_key
where p.Member_key = m.Member_key
  and p.TransactionDetailType in (0)
 and dpm.DocketPayMethod_key = dp.DocketPayMethod_key
group by dpm.DocketPayMethod_key, dpm.Name, h.Member_key
) s
--outer apply(
--select SUM(t.Amount) RentalAmount from TransactionDetail t
--inner join [Transaction] h on h.Transaction_key = t.Transaction_key and h.TransactionDate >= @startdate and  h.TransactionDate < @enddate
--and h.FailedDate is null
--inner join DocketTable d on d.Docket_key = t.Docket_key
--where t.Member_key = m.Member_key and (t.TransactionDetailType in (9))
--group by h.Member_key
--) r
outer apply(
select SUM(-t.RentAmount) RentalAmount 
from TerminalRentalCalculationLE t
inner join FirstDateOfMonth fdom on fdom.FirstDateOfMonth >= @startdate and fdom.FirstDateOfMonth < @enddate and t.tranmonth = fdom.MonthPart and t.tranyear = fdom.YearPart
where t.Member_key = m.Member_key --and t.tranmonth = DATEPART(month, @startdate) and t.tranyear = DATEPART(year, @startdate)
group by t.Member_key--, t.tranmonth, t.tranyear
) r

outer apply(
	select SUM(-td.Amount) AccountFee 
	FROM 
	TransactionDetail td 
	INNER JOIN [Transaction] t ON t.Transaction_key = td.Transaction_key and t.TransactionDate >= @startdate 
									and t.TransactionDate < @enddate and t.FailedDate IS NULL
	WHERE td.Member_key = m.Member_key 
		AND td.TransactionDetailType = 19
	GROUP BY td.Member_key
) acf



outer apply(
select SUM(t.Amount) ChargeBackFee from TransactionDetail t
inner join [Transaction] h on h.Transaction_key = t.Transaction_key and h.TransactionDate >= @startdate and  h.TransactionDate < @enddate
and h.FailedDate is null
inner join DocketTable d on d.Docket_key = t.Docket_key
inner join CadmusIN c on c.docket_key = d.Docket_key
inner join DocketPayMethodLookupTable dpm on dpm.DocketPayMethod_key = d.DocketPayMethod_key
where t.Member_key = m.Member_key and (t.TransactionDetailType = 7 and t.Description like  '%Charge Back Fee%') and dpm.DocketPayMethod_key = dp.DocketPayMethod_key
group by h.Member_key
) cb
where dp.Company_key in ( 18, 22) and cr.vehicletype_key in ( 19, 25)
and dp.paymethod_key not in (17,18,19,20)