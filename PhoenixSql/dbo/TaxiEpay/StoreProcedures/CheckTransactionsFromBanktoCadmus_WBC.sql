﻿--Test script
--EXEC [CheckTransactionsFromBanktoCadmus_WBC] '72946425','0','0','12','2012-05-01 00:00:00.000','2019-05-21 23:59:59.000',0,'2205','2019-05-21','TaxiPay00000020190521E.csv'

CREATE PROCEDURE [dbo].[CheckTransactionsFromBanktoCadmus_WBC]
    @terminalID NVARCHAR(8),
    @taxiID NVARCHAR(20),
    @DriverID NVARCHAR(20),
    @batchsequence VARCHAR(2) = '01',
    @firsttrandate DATETIME = NULL,
    @lasttrandate DATETIME = NULL,
    @Bank BIT = 1,
    @batchid NVARCHAR(6) = NULL,
    @filedate DATE = NULL,
    @filename NVARCHAR(40) = NULL
AS
BEGIN

    DECLARE @rand INT = CAST(RAND() * 100 AS INT);

    SET @filedate = ISNULL(@filedate, GETDATE());

    SET @filename = ISNULL(@filename, 'TaxiPay16100020131016E.csv');

    SELECT s.id,
           7000000000 + s.id AS batchid,
           'Loaded' AS batchstatus,
           CASE
               WHEN c.id IS NOT NULL THEN
                   ISNULL(
                             @batchid,
                             SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 7, 2)
                             + SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 5, 2)
                         ) + CAST(RIGHT('00' + CAST(CAST(@rand AS INT) AS VARCHAR(3)), 2) AS VARCHAR(2)) + s.seq
               ELSE
                   ISNULL(
                             @batchid,
                             SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 7, 2)
                             + SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 5, 2)
                         ) + @batchsequence + s.seq
           END AS transid1,
           '23166226' AS Merchantid,
           'LIVE TAXIEPAY' AS merchantname,
           @DriverID AS driverid,
           --CAST(SUBSTRING(CAST(processingdate as varchar(8)),1,4) + '-' + SUBSTRING(CAST(processingdate as varchar(8)),5,2) + '-' +  SUBSTRING(CAST(processingdate as varchar(8)),7,2) + ' 23:59:59.999' as datetime) as Loaded,
           --CAST(SUBSTRING(CAST(processingdate as varchar(8)),1,4) + '-' + SUBSTRING(CAST(processingdate as varchar(8)),5,2) + '-' +  SUBSTRING(CAST(processingdate as varchar(8)),7,2) + ' 00:00:00'as datetime)as Start,
           --CAST(SUBSTRING(CAST(processingdate as varchar(8)),1,4) + '-' + SUBSTRING(CAST(processingdate as varchar(8)),5,2) + '-' +  SUBSTRING(CAST(processingdate as varchar(8)),7,2) + ' 23:59:59' as datetime) as [End],
           s.endshift AS Loaded,
           s.startshift AS Startshift,
           s.endshift AS endshift,
           ISNULL(
                     @batchid,
                     SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 7, 2)
                     + SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 5, 2)
                 ) + @batchsequence AS BatchNumber,
           @taxiID AS taxiid,
           @terminalID AS terminalnumber,
           CASE
               WHEN RTRIM(s.accounttype) = 'CRD' THEN
                   'Credit'
               WHEN RTRIM(s.accounttype) = 'CRT' THEN
                   'Credit'
               WHEN
               (
                   RTRIM(s.accounttype) = ''
                   AND s.cardtype = 'AMEX'
               ) THEN
                   'Credit'
               ELSE
                   'Debit'
           END AS TransType,
           CASE
               WHEN RTRIM([s].status) = 'APPROVED' THEN
                   'Approved (00)'
               ELSE
                   'Declined'
           END AS Trans_Status,
           CASE cardtype
               WHEN 'AMEX' THEN
                   'AMEX'
               WHEN 'VISA' THEN
                   'VISA'
               WHEN 'MasterCard' THEN
                   'MASTERCARD'
               WHEN 'M/CD' THEN
                   'MASTERCARD'
               WHEN 'DC' THEN
                   'DINERS'
               WHEN 'DNRS' THEN
                   'DINERS'
               ELSE
                   'DEBIT'
           END AS CardName,
           cardnumber AS transpan,

           --CAST(SUBSTRING(CAST(transactiondate as varchar(8)),1,4) + '-' + SUBSTRING(CAST(transactiondate as varchar(8)),5,2) + '-' +  SUBSTRING(CAST(transactiondate as varchar(8)),7,2) + ' ' + CAST(transactiontime as varchar(5)) as datetime) as TransStart,
           --CAST(SUBSTRING(CAST(transactiondate as varchar(8)),1,4) + '-' + SUBSTRING(CAST(transactiondate as varchar(8)),5,2) + '-' +  SUBSTRING(CAST(transactiondate as varchar(8)),7,2) + ' ' + CAST(transactiontime as varchar(5)) as datetime) as TransEnd,
           s.trandate AS transstart,
           s.trandate AS transend,
           --cast((CAST(transactionamount as Decimal(9,2))/100) /1.11 as decimal(9,2)) as TransFare,
           CASE
               WHEN s.company_key IN ( 1, 20, 23 ) THEN
                   CAST(CASE
                            WHEN s.state IN ( 'WA', 'VIC', 'NSW', 'ACT', 'QLD', 'NT' ) THEN
           (CAST(transactionamount AS DECIMAL(9, 1)) / 100) / 1.05
                            WHEN s.state IN ( 'SA' ) THEN
           (CAST(transactionamount AS DECIMAL(9, 1)) / 100) / 1.055
                            ELSE
           (CAST(transactionamount AS DECIMAL(9, 2)) / 100) / 1.11
                        END AS DECIMAL(10, 1))
               ELSE
                   CAST((CAST(transactionamount AS DECIMAL(9, 2)) / 100) AS MONEY)
           END AS TransFare,
           CAST(0 AS MONEY) AS TransExtras,
           CAST(0 AS MONEY) AS TransTip,
           CAST((CAST(transactionamount AS DECIMAL(9, 2)) / 100) AS MONEY) AS TransTotalAmount,
           transactionnumber AS TransAuthorisationNumber,
           0 AS TransPassengers,
           0 AS TransTarrif,
           0 AS TransKilometers,
           'Not Supplied' AS TransPickupZone,
           'Not Supplied' AS TransDropoffZone,
           @filename AS sourcefile,
           GETDATE() AS recordcreated,
           @filedate AS filedate,
           CASE cardtype
               WHEN 'AMEX' THEN
                   'AMEX'
               WHEN 'VISA' THEN
                   'VISA'
               WHEN 'MasterCard' THEN
                   'MASTERCARD'
               WHEN 'AUS DebitCard' THEN
                   'DEBIT'
               WHEN 'DC' THEN
                   'DINERS'
               WHEN 'DNRS' THEN
                   'DINERS'
               ELSE
                   'DEBIT'
           END AS orig_CardName
    --c.batchnumber, c.startshift, c.endshift, clow.batchnumber, clow.startshift, clow.endshift, * 
    FROM
    (
        SELECT DISTINCT
               MIN(CAST(SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 1, 4) + '-'
                        + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 5, 2) + '-'
                        + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 7, 2) + ' '
                        + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 1, 2) + ':'
                        + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 3, 2) AS DATETIME)
                  ) OVER (PARTITION BY s.terminalnumber) AS startshift,
               MAX(CAST(SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 1, 4) + '-'
                        + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 5, 2) + '-'
                        + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 7, 2) + ' '
                        + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 1, 2) + ':'
                        + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 3, 2) AS DATETIME)
                  ) OVER (PARTITION BY s.terminalnumber) AS endshift,
               CAST(SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 1, 4) + '-'
                    + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 5, 2) + '-'
                    + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 7, 2) + ' '
                    + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 1, 2) + ':'
                    + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 3, 2) AS DATETIME) AS trandate,
               --s.*,
               s.transactiondate,
               s.accounttype,
               s.cardtype,
               s.status,
               s.cardnumber,
               s.transactionamount,
               s.terminalnumber,
               s.id,
               s.transactionnumber,
               RIGHT('00'
                     + CAST(DENSE_RANK() OVER (ORDER BY s.terminalnumber, s.transactiondate, s.transactiontime) AS VARCHAR), 2) AS seq,
               ISNULL(a.state, '') AS state,
               a.company_key
        FROM WestPacIN s
            OUTER APPLY
        (
            SELECT TOP 1
                   a.state,
                   a.Member_key,
                   a.company_key
            FROM vTerminalAllocations a
            WHERE a.TerminalId = s.terminalnumber
                  AND a.Member_key IS NOT NULL --and a.IsCurrent = 1 
                  AND s.transactiondate >= a.EffectiveDate
            --and a.company_key in (1,20,18,22)
            ORDER BY a.EffectiveDate DESC
        ) a
            INNER JOIN Operator o
                ON o.Operator_key = a.Member_key
                   AND
                   (
                       o.PayByEFT = 1
                       OR @Bank = 0
                   )
            LEFT JOIN WestPacIN v
                ON v.terminalnumber = s.terminalnumber
                   AND v.transactiondate = s.transactiondate
                   AND v.transactionnumber = s.transactionnumber
                   AND RTRIM(v.transactiontype) IN ( 'REVERSAL PURCHASE', 'PURCHASE REVERSAL' )
        WHERE ISDATE(s.transactiondate) = 1
              AND s.transactiondate >= CAST(ISNULL(@firsttrandate, GETDATE()) AS DATETIME)
              AND s.docket_key IS NULL
              AND CAST(s.transactiondate AS DATE) >= CAST(CAST(ISNULL(@firsttrandate, GETDATE()) AS DATETIME) AS DATE)
              AND CAST(s.transactiondate AS DATE) <= CAST(CAST(ISNULL(@lasttrandate, GETDATE()) AS DATETIME) AS DATE)
              AND (CAST(s.transactionamount AS DECIMAL(9, 2)) / 100) > 1 --and c.batchnumber is not null
              AND RTRIM(s.transactiontype) NOT IN ( 'REVERSAL PURCHASE', 'PURCHASE REVERSAL' )
              AND v.id IS NULL
              AND s.terminalnumber = @terminalID
              AND
              (
                  CAST(SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 1, 4) + '-'
                       + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 5, 2) + '-'
                       + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 7, 2) + ' '
                       + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 1, 2) + ':'
                       + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 3, 2) AS DATETIME) >= @firsttrandate
                  OR @firsttrandate IS NULL
              )
              AND
              (
                  CAST(SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 1, 4) + '-'
                       + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 5, 2) + '-'
                       + SUBSTRING(CAST(s.transactiondate AS VARCHAR(8)), 7, 2) + ' '
                       + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 1, 2) + ':'
                       + SUBSTRING(CAST(s.transactiontime AS VARCHAR(4)), 3, 2) AS DATETIME) <= @lasttrandate
                  OR @lasttrandate IS NULL
              )
    ) s
        LEFT JOIN CadmusIN c
            ON c.terminalid = s.terminalnumber
               AND c.transid1 = ISNULL(
                                          @batchid,
                                          SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 7, 2)
                                          + SUBSTRING(CONVERT(VARCHAR, s.startshift, 112), 5, 2)
                                      ) + @batchsequence + s.seq
    WHERE ISDATE(s.transactiondate) = 1
          AND s.trandate >= CAST(ISNULL(@firsttrandate, GETDATE()) AS DATETIME)
          AND s.trandate <= CAST(ISNULL(@lasttrandate, GETDATE()) AS DATETIME)
		  AND c.id IS NULL
    ORDER BY s.terminalnumber,
             s.trandate;


END;

