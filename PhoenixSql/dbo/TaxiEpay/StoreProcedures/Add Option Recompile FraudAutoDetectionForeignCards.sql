﻿ALTER PROC [dbo].[FraudAutoDetectionForeignCards] @Backdate int, @amount money, @alertKey int
AS

begin

declare @BackDatePeriod date = cast(dateadd(day, -@Backdate, getdate()) as date)



declare @todayfiledate date  = cast(getdate() as date)

declare @filedate15 date  = cast(dateadd(day, -15, getdate()) as date)

declare @filedate30 date  = cast(dateadd(day, -30, getdate()) as date)

declare @filedate60 date  = cast(dateadd(day, -60, getdate()) as date)


--declare @temp table (member_key int, stdeviation decimal(20,2))

--insert into @temp (member_key, stdeviation)
--select member_key, stdeviation from MemberStandardDeviation30Days

--create table #MemberStD30
--(
--member_key int,
--stdeviation decimal(20,2)
--)
--insert into #MemberStD30 (member_key, stdeviation)
--select member_key, stdeviation from MemberStandardDeviation30Days

--detect foreign cards and transaction amount greater tan $250 in the previous day
insert into FraudTransaction (eftdocket_key, fraudalertKey, createdDate, OccurenceLast15days, OccurenceLast30Days, OccurenceLast60days,TurnoverStdv30days, TrancountStdv30days,terminalallocation_key, TerminalID, CardType, CardNumber, FileDate, TransactionTotal, DocketDate)
select distinct e.eftdocket_key, @alertKey as FraudAlertKey , getdate(),d15.trancount as OccurenceLast15days, d30.trancount as OccurrenceLast30days, d60.trancount as OccurenceLast60days,std.stdeviation, 0,a.TerminalAllocation_key, c.terminalid, c.cardname, c.transpan, c.filedate, c.transtotalamount, c.transstart
 from eftdocket e
inner join cadmusin c on c.docket_key = e.EftDocket_key 
cross apply ( 
select top 1 a.TerminalAllocation_key
from  TerminalAllocation a where a.Member_key = e.Operator_key 
and a.EftTerminal_key = e.EftTerminal_key and a.EffectiveDate < e.TripStart
order by a.EffectiveDate 
 ) a
inner join member m on m.member_key  = e.Operator_key
inner join vBinRange b on b.BINRangeNumber = left(c.transpan, 6) and b.CountryName <>'Australia'
left join FraudTransaction ft on ft.eftdocket_key = e.eftdocket_key and ft.fraudalertkey = @alertKey
outer apply(
select  count(c15.id) as trancount from eftdocket e15 with ( nolock)
inner join cadmusin c15 on c.docket_key = e15.eftdocket_key
where c15.filedate > @filedate15 and c15.transtotalamount>100 and c15.filedate <= @todayfiledate
and e15.Operator_key = e.Operator_key and c15.transpan = c.transpan
group by e15.Operator_key, c15.transpan,isnull(c15.CardExpiry, '')
having count(c15.id)> 1
) d15

outer apply(
select  count(c30.id) as trancount from eftdocket e30 with ( nolock)
inner join cadmusin c30 on c.docket_key = e30.eftdocket_key
where c30.filedate > @filedate30 and c30.transtotalamount>100 and c30.filedate <= @todayfiledate
and e30.Operator_key = e.Operator_key and c30.transpan = c.transpan
group by e30.Operator_key, c30.transpan,isnull(c30.CardExpiry, '')
having count(c30.id)> 1
) d30

outer apply(
select  count(c60.id) as trancount from eftdocket e60 with ( nolock)
inner join cadmusin c60 on c.docket_key = e60.eftdocket_key
where c60.filedate > @filedate60 and c60.transtotalamount>100 and c60.filedate <= @todayfiledate
and e60.Operator_key = e.Operator_key and c60.transpan = c.transpan
group by e60.Operator_key, c60.transpan,isnull(c60.CardExpiry, '')
having count(c60.id)> 1
) d60

--left join [FraudSameCardSameMemberLast15Days] d15 on d15.Operator_key = e.Operator_key and c.transpan = d15.transpan 
--left join [FraudSameCardSameMemberLast30Days] d30 on d30.Operator_key = e.Operator_key and c.transpan = d30.transpan 
--left join [FraudSameCardSameMemberLast60Days] d60 on d60.Operator_key = e.Operator_key and c.transpan = d60.transpan
--left join MemberStandardDeviation30Days std on std.member_key = m.Member_key 
left join [MemberStandardDeviation30Day] std on std.member_key = m.Member_key 
 
where c.filedate > @BackDatePeriod and c.transtotalamount>@amount
and m.member_key not in (select member_key from FraudWhitelist where member_key = m.Member_key)
and ft.eftdocket_key is null
option(recompile)
--drop table #MemberStD30

end