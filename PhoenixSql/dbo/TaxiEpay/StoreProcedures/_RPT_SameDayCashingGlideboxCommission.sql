﻿CREATE PROCEDURE [dbo].[_RPT_SameDayCashingGlideboxCommission] 
@RefNumber VARCHAR(50)
AS
BEGIN
    SELECT LEFT(dt.BatchId, 8) AS TerminalId,
           SUBSTRING(dt.BatchId, 9, 6) AS BatchNumber,
           SUM(td.Amount) AS GlideboxCommission
    FROM dbo.[Transaction] t
        INNER JOIN dbo.TransactionDetail td
            ON t.Transaction_key = td.Transaction_key
        INNER JOIN dbo.DocketTable dt
            ON td.Docket_key = dt.Docket_key
        INNER JOIN dbo.GlideboxSameDayCashing gsdc
            ON dt.Docket_key = gsdc.DocketKey
        INNER JOIN dbo.Member m
            ON td.Member_key = m.Member_key
    WHERE t.LodgementReferenceNo = @RefNumber
          AND td.TransactionDetailType = 21
    GROUP BY LEFT(dt.BatchId, 8),
             SUBSTRING(dt.BatchId, 9, 6);
END;