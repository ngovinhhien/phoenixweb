﻿ALTER PROC [dbo].[_Transaction_PayRunProcessPreparePendingTrans]  
@sourcefile varchar(max),  
@memberKey varchar(10),  
@transactionType int  
-- tested on 41000+ records and it took 12 seconds to complete  
as  
  
SET NOCOUNT ON  
  
--CLEAR UP TABLE  
DELETE FROM PayRunPendingTransaction  
  
--FILL IN TABLE WITH PENDING TRANSACTIONS FOR PAYRUN PROCESS  
IF @transactionType = 0 OR @transactionType = 2 -- operator payrun
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		FROM CadmusIN inner join DocketTable on CadmusIN.docket_key = DocketTable.Docket_key and DocketTable.DocketPayMethod_key not in (61,62,63) 
	   inner join EftDocket on EftDocket_key = DocketTable.docket_key  and isnull(EftDocket.Quarantined ,0) = 0
	   inner join PendingTransactionDetail pend on pend.Docket_key = DocketTable.Docket_key  
	WHERE (TransactionDetailType = 0 OR TransactionDetailType = 2 OR (TransactionDetailType = 7 and [Description] != 'Charge Back: Dealer Commission') or (TransactionDetailType in (13))) -- 7 is chargeback  
	  --and substring(CadmusIN.sourcefile,14,8) between @from and @to -- '''' + @from + '''' and '''' + @to + ''''  
	  and pend.member_key = @memberkey  
	  and CHARINDEX( ',' + CadmusIN.sourcefile + ',', ',' + @sourcefile  + ',' ) > 0  
	  --and CHARINDEX( ',' + cast(pend.member_key  as varchar(5)) + ',', ',' + @memberKey + ',' ) > 0  
	ORDER BY pend.Docket_key  
END
Else IF @transactionType = 9 or @transactionType =10 --Terminal Rental Charge
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join TerminalRentalHistory trh on trh.TerminalRentalHistory_key = docket.Docket_key
	WHERE (TransactionDetailType in (9,10) ) --Terminal Rental Charge
	  and pend.member_key = @memberkey  
	  and CHARINDEX( ',' + 'TerminalRentalCharge'+cast(trh.RentMonth as varchar(2))+'-'+cast(trh.RentYear as varchar(4))+ ',', ',' + @sourcefile  + ',' ) > 0 
	  --and @sourcefile = 'TerminalRentalCharge'+cast(trh.RentMonth as varchar(2))+'-'+cast(trh.RentYear as varchar(4))
	ORDER BY pend.Docket_key  
END
Else IF @transactionType = 6 or @transactionType =11 or @transactionType =12 --Member Charge
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join MemberChargeHistory mch on mch.MemberChargeHistory_key = docket.Docket_key
	WHERE (TransactionDetailType in (6,11,2) ) --Terminal Rental Charge
	  and pend.member_key = @memberkey  
	  and CHARINDEX( ',' + 'TerminalReplacementCharge'+right('0000'+cast(datepart(year, mch.CreatedDate) as nvarchar(4)),4)+ right('00'+cast(datepart(month, mch.CreatedDate) as nvarchar(2)),2) + right('00'+cast(datepart(day, mch.CreatedDate) as nvarchar(2)),2)+',', ',' + @sourcefile  + ',' ) > 0 
	  --and @sourcefile = 'TerminalRentalCharge'+cast(trh.RentMonth as varchar(2))+'-'+cast(trh.RentYear as varchar(4))
	ORDER BY pend.Docket_key  
END

Else IF @transactionType = 19
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		from pendingtransactiondetail pend 
	inner join dockettable Docket on Docket.Docket_key =  pend.Docket_key
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargeHistory fch on fch.DocketKey = docket.Docket_key
	WHERE (TransactionDetailType in (19) ) --account fee
	  and pend.member_key = @memberkey  
	  and @sourcefile = 'AccountFeeCharge' + cast(datepart(month, fch.startdate) as varchar(2)) + '-' + cast(datepart(year, fch.startdate) as varchar(4))
	ORDER BY pend.Docket_key  
END

Else IF @transactionType = 20
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		from pendingtransactiondetail pend 
	inner join Member mem on mem.Member_key = pend.Member_key
	inner join FeeChargeHistory fch on fch.DocketKey = pend.Docket_key
	inner join FeeChargedRefund fcr on fcr.FeeChargeHistoryID = fch.ID
	WHERE (TransactionDetailType in (20) ) --refund account fee
	  and pend.member_key = @memberkey  
	  and @sourcefile = 'RefundAccountFeeCharge' + cast(datepart(month, fch.startdate) as varchar(2)) + '-' + cast(datepart(year, fch.startdate) as varchar(4))
	ORDER BY pend.Docket_key  
END

ELSE 
BEGIN
	INSERT INTO PayRunPendingTransaction (ItemDate,RateUsed,Amount,TransactionDetailType,[Description],Docket_key,PendingTransactionDetail_key,Member_key)  
	SELECT pend.ItemDate,    
		pend.RateUsed,    
		pend.Amount,    
		pend.TransactionDetailType,    
		pend.Description,    
		pend.Docket_key,    
		pend.PendingTransactionDetail_key,    
		pend.Member_key  
		FROM CadmusIN inner join DocketTable on CadmusIN.docket_key = DocketTable.Docket_key   and DocketTable.DocketPayMethod_key not in (61,62,63) 
	   inner join EftDocket on EftDocket_key = DocketTable.docket_key  and isnull(EftDocket.Quarantined ,0) = 0 
	   inner join PendingTransactionDetail pend on pend.Docket_key = DocketTable.Docket_key  
	WHERE (TransactionDetailType = @transactionType OR TransactionDetailType = 7 ) -- 7 is chargeback  
	  --and substring(CadmusIN.sourcefile,14,8) between @from and @to -- '''' + @from + '''' and '''' + @to + ''''  
	  and pend.member_key = @memberkey  
	  and CHARINDEX( ',' + CadmusIN.sourcefile + ',', ',' + @sourcefile  + ',' ) > 0  
	  --and CHARINDEX( ',' + cast(pend.member_key  as varchar(5)) + ',', ',' + @memberKey + ',' ) > 0  
	ORDER BY pend.Docket_key  
END
SET NOCOUNT OFF  
  
--UPDATE PayRunPendingTransaction SET TotalAmount = (select SUM(amount) from PayRunPendingTransaction)  
select SUM(amount) from PayRunPendingTransaction  



