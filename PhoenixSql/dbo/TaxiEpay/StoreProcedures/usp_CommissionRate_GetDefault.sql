﻿CREATE PROC usp_CommissionRate_GetDefault @State VARCHAR(50), @CompanyKey INT
AS
SELECT 
		dmc.GrossDealerCommissionRate AS GrossDealerCommissionRate, 
		dmc.OperatorCommissionRate AS OperatorCommissionRate, 
		dmc.ManualCommissionRate AS ManualCommissionRate,
		dmc.DocketPayMethod_key AS DocketPayMethod_key,
		dmc.State AS State,
		CAST(vt.Company_key AS VARCHAR(10)) AS BusinessType,
		CAST(vt.VehicleType_key AS VARCHAR(10)) AS SubBusinessType,
		pm.Name AS CardName
		

FROM dbo.DefaultMemberCommissionRate dmc
INNER JOIN dbo.DocketPayMethod dpm ON dpm.DocketPayMethod_key = dmc.DocketPayMethod_key
INNER JOIN dbo.VehicleType vt ON vt.VehicleType_key = dpm.VehicleType_key
INNER JOIN dbo.PayMethod pm ON pm.PayMethod_key = dpm.PayMethod_key
WHERE dmc.State = @State AND vt.Company_key = @CompanyKey AND dmc.IsBankCustomer = 1