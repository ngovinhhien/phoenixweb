﻿CREATE PROC [dbo].[GetImportedTransactionsExport]
	@TransactionDateFrom  VARCHAR(10),
	@TransactionDateTo  VARCHAR(10),
	@ImportedDateFrom  VARCHAR(10),
	@ImportedDateTo  VARCHAR(10),
	@TransactionNumber  INT,
	@AuthorisationNumber  VARCHAR(10),
	@MemberID  VARCHAR(20),
	@TerminalID  VARCHAR(18),	
	@CardNumber  VARCHAR(21),
	@CardType  VARCHAR(6),
	@AmountFrom  VARCHAR(15),
	@AmountTo  VARCHAR(15),
	@BatchNumber  VARCHAR(6),
	@Status  VARCHAR(20),
	@SourceFile  VARCHAR(50),
	@TransactionType  VARCHAR(21),
	@IsVoid  VARCHAR(2)
AS
BEGIN
	DECLARE @Select AS NVARCHAR(MAX) 
	DECLARE @From AS NVARCHAR(MAX)
	DECLARE @Where AS NVARCHAR(MAX)
	DECLARE @Params NVARCHAR(MAX)
	DECLARE @Query AS NVARCHAR(MAX)

	SET @Select = N'SELECT 
			wpac.transactionnumber AS TransactionNumber,
			wpac.merchantname AS MerchantName,
			mem.MemberId AS MemberID,
			mem.MerchantID AS MerchantID,
			wpac.terminalnumber AS TerminalID,
			wpac.status AS ApproveStatus,
			TRIM(''a'' FROM TRIM(''0'' FROM LEFT(wpac.transactionamount,13) + ''a'')) + ''.'' + RIGHT(wpac.transactionamount,2) AS Amount,
			SUBSTRING(wpac.transactiondate,7,2) + ''-'' + SUBSTRING(wpac.transactiondate,5,2) + ''-'' + SUBSTRING(wpac.transactiondate,1,4) AS TransactionDate,
			wpac.transactiontype AS TransactionType,
			wpac.authorisationnumber AS AuthorisationNumber,
			ved.BatchNumber END AS BatchNumber,
			ved.Docket_key AS Docket,
			wpac.cardtype AS CardType,
			wpac.accounttype AS AccountType,
			wpac.cardnumber AS CardNumber,
			wpac.sourcefile AS FileName,
			wpac.tracktwo AS CardEntryMode,
			wpac.voidtransactionflag AS IsVoid,
			ilf.ImportedDate AS ImportedDate,
			wpac.merchantbsbaccnumber '

	SET @From = N' FROM dbo.WestPacIN wpac
		INNER JOIN dbo.iLinkFiles ilf
			ON ilf.ID = wpac.iLinkFileID
		LEFT JOIN dbo.vEftDocket ved
			ON ved.Docket_key = wpac.docket_key
		LEFT JOIN dbo.Member mem
			ON ved.Operator_key = mem.Member_key '

	SET @Where = N' WHERE wpac.iLinkFileID IS NOT NULL'

	IF(ISNULL(@TransactionDateFrom,'') <> '')
		SET @Where = @Where + ' AND wpac.transactiondate >= @TransactionDateFrom'
	IF(ISNULL(@TransactionDateTo,'') <> '')
		SET @Where = @Where + ' AND wpac.transactiondate <= @TransactionDateTo'
	IF(ISNULL(@ImportedDateFrom,'') <> '')
		SET @Where = @Where + ' AND ilf.ImportedDate >= CONVERT(DATETIME, @ImportedDateFrom)'
	IF(ISNULL(@ImportedDateTo,'') <> '')
		SET @Where = @Where + ' AND ilf.ImportedDate <= DATEADD(DAY, 1, CONVERT(DATETIME, @ImportedDateTo))'
	IF(ISNULL(@TransactionNumber, 0 ) <> 0)
		SET @Where = @Where + ' AND	wpac.transactionnumber = @TransactionNumber'
	IF(ISNULL(@AuthorisationNumber,'') <> '')
		SET @Where = @Where + ' AND wpac.authorisationnumber = @AuthorisationNumber'
	IF(ISNULL(@MemberID,'') <> '')
		SET @Where = @Where + ' AND mem.MemberId = @MemberID'
	IF(ISNULL(@TerminalId,'') <> '')
		SET @Where = @Where + ' AND wpac.terminalnumber = @TerminalID'
	IF(ISNULL(@CardNumber,'') <> '')
		SET @Where = @Where + ' AND wpac.cardnumber = @CardNumber'
	IF(ISNULL(@CardType,'') <> '')
		SET @Where = @Where + ' AND wpac.cardtype = @CardType'
	IF(ISNULL(@AmountFrom,'') <> '')
		SET @Where = @Where + ' AND wpac.transactionamount >= @AmountFrom'
	IF(ISNULL(@AmountTo,'') <> '')
		SET @Where = @Where + ' AND wpac.transactionamount <= @AmountTo'
	IF(ISNULL(@BatchNumber,'') <> '')
		SET @Where = @Where + ' AND ved.BatchNumber = @BatchNumber'
	IF(ISNULL(@Status,'') <> '')
		SET @Where = @Where + ' AND wpac.status = @Status'
	IF(ISNULL(@SourceFile,'') <> '')
		SET @Where = @Where + ' AND wpac.sourcefile LIKE ''%'' + @SourceFile + ''%'''
	IF(ISNULL(@TransactionType,'') <> '')
		SET @Where = @Where + ' AND wpac.transactiontype = @TransactionType'
	IF(ISNULL(@IsVoid,'') <> '')
		SET @Where = @Where + ' AND wpac.voidtransactionflag = @IsVoid'

	SET @Query = @Select + @From + @Where 

	SET @Params = 
			N'
				@TransactionDateFrom VARCHAR(10),
				@TransactionDateTo VARCHAR(10),
				@ImportedDateFrom VARCHAR(10),
				@ImportedDateTo VARCHAR(10),
				@TransactionNumber INT,
				@AuthorisationNumber VARCHAR(10),
				@MemberID VARCHAR(20),
				@TerminalId VARCHAR(18),	
				@CardNumber VARCHAR(21),
				@CardType VARCHAR(6),
				@AmountFrom VARCHAR(15),
				@AmountTo VARCHAR(15),
				@BatchNumber VARCHAR(6),
				@Status VARCHAR(20),
				@SourceFile VARCHAR(50),
				@TransactionType VARCHAR(21),
				@IsVoid VARCHAR(2)
			'

	EXEC SP_EXECUTESQL @Query, @Params
		,@TransactionDateFrom = @TransactionDateFrom
		,@TransactionDateTo = @TransactionDateTo
		,@ImportedDateFrom = @ImportedDateFrom
		,@ImportedDateTo = @ImportedDateTo
		,@TransactionNumber = @TransactionNumber
		,@AuthorisationNumber = @AuthorisationNumber 
		,@MemberID = @MemberID
		,@TerminalID = @TerminalID
		,@CardNumber = @CardNumber
		,@CardType = @CardType
		,@AmountFrom = @AmountFrom
		,@AmountTo = @AmountTo
		,@BatchNumber = @BatchNumber
		,@Status = @Status
		,@SourceFile = @SourceFile
		,@TransactionType = @TransactionType
		,@IsVoid = @IsVoid
END
