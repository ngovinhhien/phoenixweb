﻿ALTER PROC [dbo].[_RPT_TransactionDetail_LiveEftpos_NewPaymentSummary] @paytype tinyint, 
@memberkey varchar(8000),@month int, @year int
AS
DECLARE @d char(1)

declare @temptablename TABLE
(
    transactiondate datetime,
	lodgementreferenceno nvarchar(50),
	payrun_key nvarchar(50), 
	bsb nvarchar(50), 
	accountnumber nvarchar(50),
	accountname nvarchar(50),
	totalamount float,
	debits float, 
	credits float, 
	description nvarchar (200),
	status nvarchar(50)
	
)

DECLARE @latestTransactionKey TABLE
		(
			DocketKey INT,
			TransactionKey INT
		)

		INSERT INTO @latestTransactionKey
		(
			DocketKey,
			TransactionKey
		)
		SELECT Docket_key,
			   MAX(Transaction_key)
		FROM dbo.TransactionDetail
		WHERE Member_key = @memberkey AND TransactionDetailType IN (19,20)
		GROUP BY Docket_key;

insert into @temptablename (
 transactiondate ,
	lodgementreferenceno ,
	payrun_key , 
	bsb , 
	accountnumber ,
	accountname ,
	totalamount ,
	debits , 
	credits ,
	description,
	status
	)
	
(SELECT  t.transactiondate, t.lodgementreferenceno,t.payrun_key,a.bsb,'****'+right(a.accountnumber, 4) accountnumber, a.accountname, t.totalamount,
case when t.totalamount < 0 then t.totalamount else 0 end as debits,
case when t.totalamount > 0 then t.totalamount else 0 end as credits, t.description, 
case when t.FailedDate IS not null then 'Bounce back' else 'Accepted' end as Status
FROM [Transaction] t 
INNER JOIN dbo.TransactionDetail td ON t.Transaction_key = td.Transaction_key
INNER join account a on a.account_key = t. account_key

WHERE  Month(t.transactiondate) = @month AND Year(t.transactiondate) = @year 
and t.Member_key =@memberkey
AND td.TransactionDetailType NOT IN (19, 20)
)
UNION
(SELECT t.transactiondate, t.lodgementreferenceno,t.payrun_key,a.bsb,'****'+right(a.accountnumber, 4) accountnumber, a.accountname, t.totalamount,
 case when t.totalamount < 0 then t.totalamount else 0 end as debits,
 CASE when t.totalamount > 0 then t.totalamount else 0 end as credits, t.description, 
 CASE when t.FailedDate IS not null then 'Bounce back' else 'Accepted' end as Status
 FROM [Transaction] t 
 INNER JOIN @latestTransactionKey td ON t.Transaction_key = td.TransactionKey
 INNER join account a on a.account_key = t. account_key

 WHERE  Month(t.transactiondate) = @month AND Year(t.transactiondate) = @year 
 AND t.Member_key = @memberkey
)


begin
	select * from @temptablename order by transactiondate
end