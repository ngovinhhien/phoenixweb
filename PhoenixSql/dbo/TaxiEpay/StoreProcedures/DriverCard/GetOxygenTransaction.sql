﻿CREATE PROC [dbo].[GetOxygenTransactions]
	@TransactionDateFrom  DATE = NULL,
	@TransactionDateTo  DATE = NULL,
	@CreatedDateFrom  DATE = NULL,
	@CreatedDateTo  DATE = NULL,
	@OxygenTransRefNo  VARCHAR(100) = NULL,
	@MemberID  VARCHAR(100) = NULL,
	@LodgementReferenceNo  VARCHAR(100) = NULL,	
	@AccountIdentifier  VARCHAR(100) = NULL,
	@TransactionType  VARCHAR(1) = NULL,
	@AmountFrom  NVARCHAR(20) = NULL,
	@AmountTo  NVARCHAR(20) = NULL,
	@StatusID  VARCHAR(2) = NULL,
	@SortField  VARCHAR(25) = NULL,
	@SortOrder  VARCHAR(4) = NULL,
	@PageSize  INT = NULL,
	@PageIndex  INT = NULL,
	@TotalItems  INT = NULL OUTPUT 
AS
BEGIN
	DECLARE @Select AS NVARCHAR(MAX) 
	DECLARE @From AS NVARCHAR(MAX)
	DECLARE @Where AS NVARCHAR(MAX)
	DECLARE @OrderBy AS NVARCHAR(100) = ' order by oth.TransactionDate desc '
	DECLARE @Params NVARCHAR(MAX)
	DECLARE @Query AS NVARCHAR(MAX)

	SET @Select = N'select 
				ot.ID, 
				ot.Transaction_key, 
				t.LodgementReferenceNo, 
				ot.OxygenTransRefNo, 
				m.MemberId, 
				oth.AccountIdentifier, 
				t.TotalAmount AS Amount,
				os.Name AS Status,
				CASE WHEN t.TotalAmount >= 0 THEN ''Load Fund'' ELSE ''Withdraw Fund'' END AS TransactionType,
				oth.TransactionDate,
				ot.CreatedDate AS CreatedDate,
				ot.Attempts '			

	SET @From = N' from OxygenTransaction ot
				inner join [Transaction] t on ot.Transaction_key = t.Transaction_key
				inner join OxygenTransactionStatus os on ot.StatusID = os.ID
				cross apply 
					(select top 1 oh.AccountIdentifier, oh.CreatedDate AS TransactionDate
					from OxygenTransactionHistory oh 
					where oh.OxygenTransactionID = ot.ID order by oh.CreatedDate desc) oth
				inner join Member m on t.Member_key = m.Member_key '

	SET @Where = N' where ot.Transaction_key IS NOT NULL'

	IF(ISNULL(@TransactionDateFrom,'') <> '')
		SET @Where = @Where + ' and oth.TransactionDate >= @TransactionDateFrom'
	IF(ISNULL(@TransactionDateTo,'') <> '')
	BEGIN
		SET @Where = @Where + ' and oth.TransactionDate <= @TransactionDateTo'
		SET @TransactionDateTo = DATEADD(DAY, 1, @TransactionDateTo)
	END
	IF(ISNULL(@CreatedDateFrom,'') <> '')
		SET @Where = @Where + ' and ot.CreatedDate >= @CreatedDateFrom'
	IF(ISNULL(@CreatedDateTo,'') <> '')
	BEGIN
		SET @Where = @Where + ' and ot.CreatedDate <= @CreatedDateTo'
		SET @CreatedDateTo = DATEADD(DAY, 1, @CreatedDateTo)
	END
	IF(ISNULL(@OxygenTransRefNo, '' ) <> '')
		SET @Where = @Where + ' and ot.OxygenTransRefNo = @OxygenTransRefNo'
	IF(ISNULL(@MemberID,'') <> '')
		SET @Where = @Where + ' and m.MemberId = @MemberID'
	IF(ISNULL(@LodgementReferenceNo,'') <> '')
		SET @Where = @Where + ' and t.LodgementReferenceNo = @LodgementReferenceNo'
	IF(ISNULL(@AccountIdentifier,'') <> '')
		SET @Where = @Where + ' and oth.AccountIdentifier = @AccountIdentifier'
	IF(ISNULL(@AmountFrom,'') <> '')
		SET @Where = @Where + ' and t.TotalAmount >= @AmountFrom'
	IF(ISNULL(@AmountTo,'') <> '')
		SET @Where = @Where + ' and t.TotalAmount <= @AmountTo'
	IF(ISNULL(@StatusID,'') <> '')
		SET @Where = @Where + ' and ot.StatusID = @StatusID'
	IF(ISNULL(@TransactionType,'') <> '')
		SET @Where = @Where + ' and ((t.TotalAmount >= 0 and @TransactionType = 1) OR (t.TotalAmount < 0 and @TransactionType <> 1))'


	IF(ISNULL(@SortField,'') <> '')
		BEGIN
			SET @OrderBy = ' ORDER BY ' 
			+ CASE WHEN @SortField = 'CreatedDate' THEN 'ot.CreatedDate'
					WHEN @SortField = 'LodgementReferenceNo' THEN 't.LodgementReferenceNo'
					WHEN @SortField = 'OxygenTransRefNo' THEN 'ot.OxygenTransRefNo'
					WHEN @SortField = 'MemberID' THEN 'm.MemberId'
					WHEN @SortField = 'AccountIdentifier' THEN 'oth.AccountIdentifier'
					ELSE 'oth.TransactionDate' END

			+ ' ' 
			+ @SortOrder
		END
	
	IF(ISNULL(@PageIndex,0)=0 AND ISNULL(@PageSize,0)=0)
	BEGIN
		SET @Query = @Select + @From + @Where + @OrderBy
	END
	ELSE
	BEGIN
		SET @Query = @Select + @From + @Where + @OrderBy + ' OFFSET ((@PageIndex - 1) * @PageSize) ROWS FETCH NEXT @PageSize ROWS ONLY;'
				+ ' SELECT @TotalItems = COUNT(ot.ID) ' + @From + @Where 
	END
	

	SET @Params = 
			N'
				@TransactionDateFrom  DATE,
				@TransactionDateTo  DATE,
				@CreatedDateFrom  DATE,
				@CreatedDateTo  DATE,
				@OxygenTransRefNo  VARCHAR(100),
				@MemberID  VARCHAR(100),
				@LodgementReferenceNo  VARCHAR(100),	
				@AccountIdentifier  VARCHAR(100),
				@TransactionType  VARCHAR(1),
				@AmountFrom  VARCHAR(15),
				@AmountTo  VARCHAR(15),
				@StatusID  INT,
				@SortField  VARCHAR(25),
				@SortOrder  VARCHAR(4),
				@PageSize  INT,
				@PageIndex  INT,
				@TotalItems  INT OUTPUT
			'

	EXEC SP_EXECUTESQL @Query, @Params
		,@TransactionDateFrom = @TransactionDateFrom
		,@TransactionDateTo = @TransactionDateTo
		,@CreatedDateFrom = @CreatedDateFrom
		,@CreatedDateTo = @CreatedDateTo
		,@OxygenTransRefNo = @OxygenTransRefNo
		,@MemberID = @MemberID 
		,@LodgementReferenceNo = @LodgementReferenceNo
		,@AccountIdentifier = @AccountIdentifier
		,@TransactionType = @TransactionType
		,@AmountFrom = @AmountFrom
		,@AmountTo = @AmountTo
		,@StatusID = @StatusID
		,@SortField = @SortField
		,@SortOrder = @SortOrder
		,@PageSize = @PageSize
		,@PageIndex = @PageIndex
		,@TotalItems = @TotalItems OUT
END