﻿CREATE PROC [dbo].[ApproveTimeoutOxygenTransaction](
	@TransRefID VARCHAR(20),
	@ID INT,
	@ResponseDescription VARCHAR(50),
	@UpdatedBy VARCHAR(50),
	@Error VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	SET @Error = ''
	DECLARE @LodgementReferenceNo VARCHAR(30)
	SELECT @LodgementReferenceNo = t.LodgementReferenceNo
	FROM OxygenTransaction ot 
		INNER JOIN [Transaction] t on t.Transaction_key = ot.Transaction_key
		INNER JOIN OxygenTransactionStatus os ON ot.StatusID = os.ID
		CROSS APPLY (
			SELECT TOP 1 oh.AccountIdentifier FROM OxygenTransactionHistory oh
			WHERE oh.OxygenTransactionID = ot.ID
			ORDER BY oh.CreatedDate DESC
		) oh
	WHERE ot.ID = @ID

	IF(CHARINDEX(@LodgementReferenceNo, @ResponseDescription) <> 1)
	BEGIN
		SET @Error = @Error + 'Transaction not match'
	END

	IF(@Error = '')
	BEGIN
		UPDATE OxygenTransaction	
		SET
			OxygenTransRefNo = @TransRefID,
			StatusID = (SELECT TOP 1 ID FROM OxygenTransactionStatus WHERE Name = 'Paid'),
			UpdatedDate = GETDATE(),
			UpdatedBy = @UpdatedBy
		WHERE ID = @ID
	END
END

