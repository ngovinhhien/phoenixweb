﻿CREATE PROC [dbo].[_RPT_TransactionDetail_LiveEftpos_NewPaymentSummaryDriverCard] 
@memberkey varchar(8000),@month int, @year int
AS


SELECT  t.transactiondate, t.lodgementreferenceno,t.payrun_key,t1h.AccountIdentifier as accountIdentifier, t.totalamount,
case when t.totalamount < 0 then t.totalamount else 0 end as debits,
case when t.totalamount >= 0 then t.totalamount else 0 end as credits, t.description, 
 'Accepted'  as Status
FROM [Transaction] t 
INNER JOIN OxygenTransaction ot ON t.Transaction_key = ot.Transaction_key
INNER JOIN OxygenTransactionStatus os ON ot.StatusID = os.ID
CROSS APPLY (SELECT TOP 1 AccountIdentifier FROM OxygenTransactionHistory oh  WHERE oh.OxygenTransactionID = ot.ID ORDER BY oh.CreatedDate DESC) t1h
WHERE  Month(t.transactiondate) = @month AND Year(t.transactiondate) = @year 
and t.Member_key =@memberkey and os.Name = 'Paid'
