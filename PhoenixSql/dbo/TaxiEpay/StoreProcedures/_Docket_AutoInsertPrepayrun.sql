﻿ALTER PROCEDURE [dbo].[_Docket_AutoInsertPrepayrun] @payrundate DATE = NULL
AS
BEGIN
    DECLARE @trancount INT;
    DECLARE @memberKey INT;
    DECLARE @transactionkey INT;
    DECLARE @payrunkey INT;
    DECLARE @accountkey INT;
    DECLARE @weight REAL;
    DECLARE @sumamount MONEY;
    DECLARE @transactionamount MONEY;
    DECLARE @memcounter INT;
    DECLARE @Refno NVARCHAR(50);

    IF (@payrundate IS NULL)
    BEGIN
        SET @payrundate = CAST(GETDATE() AS DATE);
    END;

    DECLARE @includefromdate DATETIME = DATEADD(WEEK, -30, GETDATE());

    DECLARE @twodayincludedate DATE = CAST(DATEADD(DAY, -2, @payrundate) AS DATE);

    DECLARE @onedayincludedate DATE = CAST(DATEADD(DAY, -1, @payrundate) AS DATE);

    DECLARE @nextdayincludedate DATE = CAST(@payrundate AS DATE);

    DECLARE @temptable TABLE
    (
        memberKey INT,
        docketKey INT,
        pendingtransactionkey INT,
        TransactionDetailType INT,
        amount DECIMAL(10, 2),
        MemberPaymentSchedule_key INT
    );

    DECLARE @IdentityOutput TABLE
    (
        ID INT,
        ReferenceNo NVARCHAR(50)
    );

    DECLARE @tempSelectedRecords TABLE
    (
        Amount DECIMAL(10, 2),
        Docket_key INT,
        member_Key INT,
        pendingtransactionkey INT
    );

    INSERT INTO @temptable
    (
        memberKey,
        docketKey,
        pendingtransactionkey,
        TransactionDetailType,
        amount,
        MemberPaymentSchedule_key
    )
    SELECT o.Operator_key,
           e.EftDocket_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           p.Amount,
           s.MemberPaymentSchedule_key
    FROM Operator o
        INNER JOIN Member m
            ON m.Member_key = o.Operator_key
               AND m.Active = 1
        OUTER APPLY ---get last payment date
    (
        SELECT CASE
                   WHEN DATEPART(DW, @payrundate) = 3 THEN
                       DATEADD(DAY, -1, MAX(TransactionDate))
                   ELSE
                       MAX(TransactionDate)
               END AS LastPaymentDate
        FROM [Transaction] h --Tuesday, it needs to pay for Sunday file
            INNER JOIN PayRun pr
                ON pr.PayRun_key = h.PayRun_key
                   AND ISNULL(pr.PayrunType, 0) = 1 --only check the last auto payment date
        WHERE h.Member_key = o.Operator_key
              AND h.PayRun_key IS NOT NULL
    ) h
        INNER JOIN PendingTransactionDetail p
            ON p.Member_key = o.Operator_key
               AND
               (
                   p.TransactionDetailType IN ( 0, 7, 14, 18, 19, 21, 22 )
                   OR p.TransactionDetailType > 40
                   OR
                   (
                       p.TransactionDetailType IN ( 2 )
                       AND ISNULL(EOMMSFPayment, 0) = 0
                   ) --prepare for LE customer want to get charged MSF EOM
               --or (p.TransactionDetailType in (14) and m.State = 'WA'  and m.Company_key = 1 )
               )
        CROSS APPLY
    (
        SELECT e.EftDocket_key,
               doc.DocketPayMethod_key
        FROM EftDocket e
            INNER JOIN DocketTable doc
                ON doc.Docket_key = e.EftDocket_key
            INNER JOIN EftTerminal t
                ON t.EftTerminal_key = e.EftTerminal_key
            --inner join TerminalType tt on tt.TerminalType_key = t.TerminalType
            INNER JOIN CadmusIN cad
                ON cad.docket_key = e.EftDocket_key
        WHERE e.EftDocket_key = p.Docket_key
              AND ISNULL(e.Quarantined, 0) = 0
              --and e.Operator_key = m.Member_key 
              AND
              (
                  m.Company_key IN ( 1, 20, 21, 23, 24, 25 )
                  OR
                  (
                      m.Company_key IN ( 19 )
                      AND
                      (
                          doc.DocketPayMethod_key NOT IN
                          (
                              SELECT DocketPayMethod_key
                              FROM DocketPayMethod dpm
                                  INNER JOIN PayMethod pm
                                      ON pm.PayMethod_key = dpm.PayMethod_key
                              WHERE pm.PayMethod_key IN ( 1, 2, 6 )
                                    AND dpm.VehicleType_key = 20
                          )
                          OR m.Member_key IN ( 18241, 18311 ) --put an exception for these members because they got Direct LE terminals but we got the payment and then pay them

                      )
                  )
                  OR
                  (
                      m.Company_key IN ( 18, 22, 26, 27 ) --and (tt.TerminalType_key in (5,8,33) or tt.VirtualTerminal = 1) 
                      AND NOT (
                                  m.Member_key IN
                                  (
                                      SELECT member_key FROM [LECustomersWithOwnAMEXMID]
                                  )
                                  AND cad.cardname IN ( 'AMEX' )
                              ) --don't pay Amex transactions for these customers. they get paid directly from Amex
                  )
              )
    ) e
        INNER JOIN CadmusIN c
            ON c.docket_key = e.EftDocket_key
        CROSS APPLY
    (
        SELECT TOP 1
               s.Member_key,
               s.MemberPaymentSchedule_key,
               ISNULL(s.ScheduleStartDate, '20120101') AS ScheduleStartDate,
               ISNULL(s.ScheduleEndDate, '21000101') AS ScheduleEndDate,
               ISNULL(t.WeeksInterval, 0) AS weeksinterval,
               ISNULL(t.MonthInterval, 0) AS MonthInterval --, s.* 
        FROM MemberPaymentSchedule s
            INNER JOIN ScheduleType t
                ON t.ScheduleType_key = s.ScheduleType_key
        WHERE ISNULL(s.InActive, 0) = 0
              AND
              (
                  (
                      EXISTS
        (
            SELECT Item
            FROM Split(s.DaysOfMonthString, ',')
            WHERE Item = CAST(DATEPART(DAY, @payrundate) AS NVARCHAR)
        )
                      AND ISNULL(t.MonthInterval, 0) > 0
                  ) --check for monthly schedules
                  OR
                  (
                      EXISTS
        (
            SELECT Item
            FROM Split(s.DaysOfWeekString, ',')
            WHERE Item = CAST(DATEPART(dw, @payrundate) AS NVARCHAR)
        )
                      AND ISNULL(t.WeeksInterval, 0) > 0
                  --and h.LastPaymentDate < DATEADD(WEEK, -isnull(t.WeeksInterval-1,0), @payrundate)
                  --and (h.LastPaymentDate < DATEADD(WEEK, -isnull(t.WeeksInterval-1,0), @payrundate) or h.LastPaymentDate is null)
                  )
              ) -- check for weekly schedules and fortnightly

              AND ISNULL(s.ScheduleEndDate, '21000101') >= @payrundate
              AND s.Member_key = m.Member_key
        ORDER BY ScheduleEndDate ASC,
                 ScheduleStartDate DESC,
                 weeksinterval DESC,
                 MonthInterval DESC
    ) s
        LEFT JOIN PreparePayrun pp
            ON pp.PendingTransactionDetail_key = p.PendingTransactionDetail_key
               AND pp.PayRunDate = @payrundate
               AND s.MemberPaymentSchedule_key = pp.MemberPaymentSchedule_key
    WHERE o.PayByEFT = 1
          AND o.AllowCash = 0
          AND
          (
              p.TransactionDetailType IN ( 0, 7, 14, 18, 19, 21, 22 )
              OR p.TransactionDetailType > 40
              OR
              (
                  p.TransactionDetailType IN ( 2 )
                  AND ISNULL(EOMMSFPayment, 0) = 0
              ) --prepare for LE customer want to get charged MSF EOM
          )
          --and o.operator_key = 14763 
          AND NOT EXISTS
    (
        SELECT * FROM PublicHoliday WHERE HolidayDate = CAST(GETDATE() AS DATE)
    )
          AND s.Member_key = m.Member_key
          AND
          (
              --c.filedate>= cast(dateadd(day, -case when m.Company_key in (1,19) then 2 else 1 end, 
              --CASE 
              --when isnull(h.LastPaymentDate, dateadd(month, -1, getdate())) >= dateadd(month, -1, getdate()) 
              --then isnull(h.LastPaymentDate, dateadd(month, -1, getdate())) 
              --else dateadd(month, -1, getdate())
              --end 
              --) as date)
              c.filedate >= @includefromdate
              AND
              (
                  (
                      m.Company_key IN ( 1, 19, 20, 21, 24 )
                      AND
                      (
                          (
                              c.filedate < @onedayincludedate
                              AND DATEPART(dw, @payrundate) <> 2 --for any day that is not Monday
                          )
                          OR
                          (
                              c.filedate < @twodayincludedate
                              AND DATEPART(dw, @payrundate) = 2 --for Monday
                          )
                      )
                  )
                  OR
                  (
                      (
                          (
                              m.Company_key IN ( 18, 22, 25, 26, 27, 23 )
                              OR
                              (
                                  m.Company_key = 1
                                  AND e.DocketPayMethod_key IN ( 146, 147, 148, 149, 150, 151 )
                              )
                          )
                          OR (m.Member_key IN ( 25991, 26093 ))
                      )
                      AND --c.filedate < cast(@payrundate as DATE)
                      (
                          (
                              c.filedate < @payrundate
                              AND DATEPART(dw, @payrundate) <> 2 --for any day that is not Monday
                          )
                          OR
                          (
                              c.filedate < @onedayincludedate
                              AND DATEPART(dw, @payrundate) IN ( 2, 3 ) --for Monday and Tuesday
                          )
                      )
                  )
              )
          )
          AND pp.PendingTransactionDetail_key IS NULL
    ORDER BY o.Operator_key,
             e.EftDocket_key,
             p.TransactionDetailType;



    --include charge for terminal Insurance and terminal insurance reverse
    INSERT INTO @temptable
    (
        memberKey,
        docketKey,
        pendingtransactionkey,
        TransactionDetailType,
        amount,
        MemberPaymentSchedule_key
    )
    SELECT o.Operator_key,
           dep.DepositKey,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           p.Amount,
           s.MemberPaymentSchedule_key
    FROM vTerminalAllocations a
        INNER JOIN TerminalType tt
            ON tt.TerminalType_key = a.TerminalType_key
        INNER JOIN Operator o
            ON o.Operator_key = a.Member_key
        INNER JOIN Deposit dep
            ON dep.TerminalAllocation_key = a.TerminalAllocation_key
               AND dep.CreatedDate < DATEADD(DAY, -7, GETDATE())
        INNER JOIN PendingTransactionDetail p
            ON p.Docket_key = dep.DepositKey
        OUTER APPLY
    (
        SELECT t.memberKey,
               SUM(t.amount) AS Totalpaid
        FROM @temptable t
        WHERE t.memberKey = a.Member_key
        GROUP BY t.memberKey
        --having sum(t.amount) > abs(p.Amount)
        HAVING (
                   SUM(t.amount) > ABS(p.Amount)
                   OR p.Amount > 0
               )
    ) t
        CROSS APPLY
    (
        SELECT TOP 1
               s.Member_key,
               s.MemberPaymentSchedule_key,
               ISNULL(s.ScheduleStartDate, '20120101') AS ScheduleStartDate,
               ISNULL(s.ScheduleEndDate, '21000101') AS ScheduleEndDate,
               ISNULL(t.WeeksInterval, 0) AS weeksinterval,
               ISNULL(t.MonthInterval, 0) AS MonthInterval --, s.* 
        FROM MemberPaymentSchedule s
            INNER JOIN ScheduleType t
                ON t.ScheduleType_key = s.ScheduleType_key
        WHERE ISNULL(s.InActive, 0) = 0
              AND
              (
                  (
                      EXISTS
        (
            SELECT Item
            FROM Split(s.DaysOfMonthString, ',')
            WHERE Item = CAST(DATEPART(DAY, @payrundate) AS NVARCHAR)
        )
                      AND ISNULL(t.MonthInterval, 0) > 0
                  ) --check for monthly schedules
                  OR
                  (
                      EXISTS
        (
            SELECT Item
            FROM Split(s.DaysOfWeekString, ',')
            WHERE Item = CAST(DATEPART(dw, @payrundate) AS NVARCHAR)
        )
                      AND ISNULL(t.WeeksInterval, 0) > 0
                  --and (h.LastPaymentDate < DATEADD(WEEK, -isnull(t.WeeksInterval-1,0), @payrundate) or h.LastPaymentDate is null)
                  )
              ) -- check for weekly schedules and fortnightly
              AND s.Member_key = a.Member_key
        ORDER BY ScheduleEndDate ASC,
                 ScheduleStartDate DESC,
                 weeksinterval DESC,
                 MonthInterval DESC
    ) s
    WHERE a.TerminalType_key IN ( 5, 8 )
          AND a.company_key = 1 --and a.state = 'WA'
          AND
          (
              t.Totalpaid > ABS(p.Amount)
              OR p.Amount > 0
          )
    ORDER BY o.Operator_key,
             dep.DepositKey,
             p.TransactionDetailType;

    --include charge for Member Charge i.e. Terminal recovery charge and other charges in the future. Only include those got created in the last 7 days.
    INSERT INTO @temptable
    (
        memberKey,
        docketKey,
        pendingtransactionkey,
        TransactionDetailType,
        amount,
        MemberPaymentSchedule_key
    )
    SELECT mch.Member_key,
           mch.MemberChargeHistory_key,
           p.PendingTransactionDetail_key,
           p.TransactionDetailType,
           p.Amount,
           NULL
    FROM MemberChargeHistory mch
        INNER JOIN PendingTransactionDetail p
            ON p.Docket_key = mch.MemberChargeHistory_key
        INNER JOIN MemberChargeType mct
            ON mct.MemberChargeType_key = mch.MemberChargeType_key
        LEFT JOIN vTerminalAllocations a
            ON a.TerminalAllocation_key = mch.TerminalAllocation_key
               AND a.IsCurrent = 1
        INNER JOIN Account ac
            ON ac.Member_key = p.Member_key
               AND ac.Weight > 0
        LEFT JOIN PreparePayrun pp
            ON pp.PendingTransactionDetail_key = p.PendingTransactionDetail_key
               AND pp.PayRunDate = @payrundate
    WHERE mch.CreatedDate > DATEADD(DAY, -7, GETDATE())
          AND pp.PreparePayrun_key IS NULL
          AND
          (
              (
                  a.TerminalAllocation_key IS NOT NULL
                  AND ISNULL(mct.RequireTerminalID, 0) = 1
              )
              OR (ISNULL(mct.RequireTerminalID, 0) = 0)
          );


    SELECT @trancount = COUNT(t.pendingtransactionkey)
    FROM @temptable t;
    DECLARE @docketcursor CURSOR;
    DECLARE @MemberCursor CURSOR;
    DECLARE @accountcursor CURSOR;
    IF @trancount > 0
    BEGIN

        SET @MemberCursor = CURSOR FOR
        SELECT memberKey,
               SUM(amount)
        FROM @temptable t
        GROUP BY memberKey;

        OPEN @MemberCursor;

        FETCH NEXT FROM @MemberCursor
        INTO @memberKey,
             @sumamount;

        WHILE @@fetch_status = 0 --for each member
        BEGIN
            --set @memcounter = @memcounter + 1
            DECLARE @TOTALLIMIT MONEY;
            DECLARE @accountcount INT;
            DECLARE @accounter INT;
            --get bank account details
            SET @accountkey = NULL;
            SET @weight = NULL;
            SET @accountcount = 0;
            SET @accounter = 0;
            SET @accountcursor = CURSOR FOR
            SELECT Account_key,
                   Weight,
                   COUNT(Account_key) OVER (PARTITION BY Member_key) AS accountcount
            FROM Account
            WHERE Member_key = @memberKey
                  AND Weight > 0;

            OPEN @accountcursor;
            SET @transactionamount = 0.0;

            FETCH NEXT FROM @accountcursor
            INTO @accountkey,
                 @weight,
                 @accountcount;


            IF @accountkey IS NOT NULL
            BEGIN

                SET @TOTALLIMIT = CAST((@sumamount * @weight / 100) AS MONEY);
                SET @accounter = @accounter + 1;
                DECLARE @docketamount MONEY;
                DECLARE @docketkey INT;
                SET @docketcursor = CURSOR FOR
                SELECT docketKey,
                       SUM(amount) total
                FROM @temptable
                WHERE memberKey = @memberKey
                GROUP BY docketKey
                ORDER BY SUM(amount),
                         docketKey;

                OPEN @docketcursor;
                FETCH NEXT FROM @docketcursor
                INTO @docketkey,
                     @docketamount;
                WHILE @@FETCH_STATUS = 0
                BEGIN

                    INSERT INTO PreparePayrun
                    (
                        PendingTransactionDetail_key,
                        Account_key,
                        PayRunDate,
                        MemberPaymentSchedule_key
                    )
                    SELECT pendingtransactionkey,
                           @accountkey,
                           @payrundate,
                           p.MemberPaymentSchedule_key
                    FROM @temptable p
                        LEFT JOIN PreparePayrun pp
                            ON pp.PendingTransactionDetail_key = p.pendingtransactionkey
                               AND pp.Account_key = @accountkey
                               AND p.MemberPaymentSchedule_key = pp.MemberPaymentSchedule_key
                               AND pp.PayRunDate = @payrundate
                    WHERE p.docketKey = @docketkey
                          AND memberKey = @memberKey
                          AND pp.PreparePayrun_key IS NULL;

                    SELECT @transactionamount = @transactionamount + SUM(p.amount)
                    FROM @temptable p
                    WHERE p.docketKey = @docketkey
                          AND memberKey = @memberKey;
                    IF @transactionamount > @TOTALLIMIT
                       AND @accounter < @accountcount -- if transaction total greater than limit of that account and there is more account, fetch another account
                    BEGIN

                        FETCH NEXT FROM @accountcursor
                        INTO @accountkey,
                             @weight,
                             @accountcount;
                        SET @TOTALLIMIT = CAST((@sumamount * @weight / 100) AS MONEY);
                        SET @accounter = @accounter + 1;
                        SET @transactionamount = 0;
                    END;
                    FETCH NEXT FROM @docketcursor
                    INTO @docketkey,
                         @docketamount;
                END;
                CLOSE @docketcursor;
                DEALLOCATE @docketcursor;
            END;
            CLOSE @accountcursor;
            DEALLOCATE @accountcursor;
            FETCH NEXT FROM @MemberCursor
            INTO @memberKey,
                 @sumamount;
        END; --MemberCursor
        CLOSE @MemberCursor;
        DEALLOCATE @MemberCursor;
    END;
END;