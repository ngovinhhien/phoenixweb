﻿SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[WebCashing_GetMemberTerminalAllocation_AllVehicles] @MemberKey INT
AS
BEGIN
    DECLARE @TerminalAllocation TABLE
    (
        Vehicle_key INT,
        VehicleId VARCHAR(25),
        TerminalAllocatedDate DATETIME,
        RegisteredState VARCHAR(15),
        Name VARCHAR(25),
        TerminalAllocation_key INT,
        TerminalId VARCHAR(20),
        EffectiveDate DATETIME,
        DeallocatedDate DATETIME,
        OriginalTID VARCHAR(20),
        SwappedTID VARCHAR(20),
        TerminalKey INT,
        VehicleType_key INT,
        Inactive BIT,
        AppKey INT,
        ReceiptKey INT,
        IsSetEOS BIT,
        EnableSendAPIKey BIT
    );
    INSERT INTO @TerminalAllocation
    SELECT v.Vehicle_key,
           v.VehicleId,
           v.EffectiveDate AS TerminalAllocatedDate,
           v.state AS RegisteredState,
           vt.Name,
           v.TerminalAllocation_key,
           v.TerminalId,
           v.EffectiveDate,
           NULL AS DeallocatedDate,
           v.originalterminalid AS OriginalTID,
           v.SwappedTerminalID AS SwappedTID,
           tt.TerminalKey AS TerminalKey,
           vt.VehicleType_key,
           tt.Inactive,
           tt.AppKey,
           tt.ReceiptKey,
           CASE
               WHEN ISNULL(rc.EOSReceiptSummary, '') = 'P' THEN
                   CONVERT(BIT, 1)
               ELSE
                   CONVERT(BIT, 0)
           END AS IsSetEOS, --check the terminal set EOS or unset EOS before
           ISNULL(vt.EnableSendAPIKey, 0) AS EnableSendAPIKey
    FROM dbo.vTerminalAllocations v
        INNER JOIN dbo.VehicleType vt
            ON vt.VehicleType_key = v.VehicleType_key
        LEFT JOIN Transactionhost.dbo.Terminal tt
            ON tt.TerminalID = v.TerminalId COLLATE Latin1_General_CI_AS
        LEFT OUTER JOIN Transactionhost.dbo.Receipt AS rc
            ON rc.ReceiptKey = tt.ReceiptKey
    WHERE v.Member_key = @MemberKey
          AND v.IsCurrent = 1
          AND v.EftTerminal_key IS NOT NULL
    --AND v.TerminalAllocation_key IS NOT NULL
    GROUP BY CASE
                 WHEN ISNULL(rc.EOSReceiptSummary, '') = 'P' THEN
                     CONVERT(BIT, 1)
                 ELSE
                     CONVERT(BIT, 0)
             END,
             ISNULL(vt.EnableSendAPIKey, 0),
             v.Vehicle_key,
             v.VehicleId,
             v.EffectiveDate,
             v.state,
             vt.Name,
             v.TerminalAllocation_key,
             v.TerminalId,
             v.originalterminalid,
             v.SwappedTerminalID,
             tt.TerminalKey,
             vt.VehicleType_key,
             tt.Inactive,
             tt.AppKey,
             tt.ReceiptKey;
    --de-allocated terminals
    INSERT INTO @TerminalAllocation
    SELECT v.Vehicle_key,
           v.VehicleId,
           v.FirstEffectiveDate AS TerminalAllocatedDate,
           v.state AS RegisteredState,
           vt.Name,
           v.TerminalAllocation_key,
           v.TerminalId,
           v.FirstEffectiveDate,
           v.EffectiveDate AS DeallocatedDate,
           v.originalterminalid AS OriginalTID,
           v.SwappedTerminalID AS SwappedTID,
           tt.TerminalKey AS TerminalKey,
           vt.VehicleType_key,
           tt.Inactive,
           tt.AppKey,
           tt.ReceiptKey,
           CASE
               WHEN ISNULL(rc.EOSReceiptSummary, '') = 'P' THEN
                   CONVERT(BIT, 1)
               ELSE
                   CONVERT(BIT, 0)
           END AS IsSetEOS, --check the terminal set EOS or unset EOS before
           ISNULL(vt.EnableSendAPIKey, 0) AS EnableSendAPIKey
    FROM dbo.vTerminalAllocations v
        INNER JOIN dbo.VehicleType vt
            ON vt.VehicleType_key = v.VehicleType_key
        LEFT JOIN Transactionhost.dbo.Terminal tt
            ON tt.TerminalID = v.TerminalId COLLATE Latin1_General_CI_AS
        LEFT OUTER JOIN Transactionhost.dbo.Receipt AS rc
            ON rc.ReceiptKey = tt.ReceiptKey
        LEFT JOIN @TerminalAllocation ta1
            ON ta1.Vehicle_key = v.Vehicle_key
               AND ta1.VehicleId = v.VehicleId
    WHERE v.Member_key = @MemberKey
          --AND v.EftTerminal_key IS NULL
          AND v.IsCurrent = 1
          AND ISNULL(v.TerminalId, '') = ''
          --AND v.TerminalAllocation_key IS NOT NULL
          AND ta1.Vehicle_key IS NULL
    --AND v.UnallocatedDate IS NOT NULL
    GROUP BY CASE
                 WHEN ISNULL(rc.EOSReceiptSummary, '') = 'P' THEN
                     CONVERT(BIT, 1)
                 ELSE
                     CONVERT(BIT, 0)
             END,
             ISNULL(vt.EnableSendAPIKey, 0),
             v.Vehicle_key,
             v.VehicleId,
             v.FirstEffectiveDate,
             v.state,
             vt.Name,
             v.TerminalAllocation_key,
             v.TerminalId,
             v.FirstEffectiveDate,
             v.EffectiveDate,
             v.originalterminalid,
             v.SwappedTerminalID,
             tt.TerminalKey,
             vt.VehicleType_key,
             tt.Inactive,
             tt.AppKey,
             tt.ReceiptKey;
    SELECT DISTINCT
           *
    FROM @TerminalAllocation;
END;




GO