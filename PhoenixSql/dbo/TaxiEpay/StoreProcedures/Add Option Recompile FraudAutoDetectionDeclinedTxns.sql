﻿ALTER PROC [dbo].[FraudAutoDetectionDeclinedTxns] @Backdate int, @amount money, @alertKey int
AS

begin

declare @BackDatePeriod date = cast(dateadd(day, -@Backdate, getdate()) as date)



declare @todayfiledate date  = cast(getdate() as date)

declare @filedate15 date  = cast(dateadd(day, -15, getdate()) as date)

declare @filedate30 date  = cast(dateadd(day, -30, getdate()) as date)

declare @filedate60 date  = cast(dateadd(day, -60, getdate()) as date)


declare @temp table (member_key int, stdeviation decimal(20,2))

insert into @temp (member_key, stdeviation)
select member_key, stdeviation from MemberStandardDeviation30Days

----detect Declined attempts Same member same card, multiple times in last two day and the amount greater than 125
insert into FraudTransaction (fraudalertKey, createdDate, OccurenceLast15days, OccurenceLast30Days, OccurenceLast60days,TurnoverStdv30days, TrancountStdv30days, CadmusDeclinedID, TerminalAllocation_Key, TerminalID, CardType, CardNumber, FileDate, TransactionTotal, DocketDate)
select distinct 8 as FraudAlertKey, getdate(),d15.trancount as OccurenceLast15days, d30.trancount as OccurrenceLast30days, d60.trancount as OccurenceLast60days,std.stdeviation, 0, c.id, a.TerminalAllocation_key, c.terminalid, c.cardname, c.transpan, c.filedate, c.transtotalamount, c.transstart
 from CadmusDeclined c 
inner join vTerminalAllocations a on a.TerminalId  = c.terminalid and a.Member_key is not null and a.IsCurrent=1 and a.EftTerminal_key is not null
inner join (
select  a.Member_key, c.transpan, isnull(c.CardExpiry, '') CardExpiry, count(c.id) as trancount from CadmusDeclined c
inner join vTerminalAllocations a on a.TerminalId  = c.terminalid and a.Member_key is not null and a.IsCurrent=1 and a.EftTerminal_key is not null
where c.filedate > @BackDatePeriod and c.transtotalamount>@amount
group by a.Member_key, c.transpan,isnull(c.CardExpiry, '')
having count(c.id)> 1 and c.transpan <> '*******'
) d on  d.transpan = c.transpan and isnull(c.CardExpiry, '') = isnull(d.cardExpiry , '')
and a.Member_key = d.Member_key
left join FraudTransaction ft on ft.CadmusDeclinedID = c.id and ft.fraudalertkey = @alertKey

left join [FraudSameCardSameMemberLast15Days] d15 on d15.transpan = c.transpan and c.Cardexpiry = d15.Cardexpiry and d15.Operator_key = a.Member_key
left join [FraudSameCardSameMemberLast30Days] d30 on d30.transpan = c.transpan and c.Cardexpiry = d30.Cardexpiry and d30.Operator_key = a.Member_key
left join [FraudSameCardSameMemberLast60Days] d60 on d60.transpan = c.transpan and c.Cardexpiry = d60.Cardexpiry and d60.Operator_key = a.Member_key
left join MemberStandardDeviation30Days std on std.member_key = a.Member_key 
where c.filedate > @BackDatePeriod and c.transtotalamount>@amount
and ft.FraudTransactionKey is null
option(recompile)

end

