﻿--EXEC dbo.GetQBRPointForSaleFinByYearMonthAndBusinessType @Year = 2019,          -- int
--                                                         @Month = 4,         -- int
--                                                         @BusinessTypeID = 18 -- int



CREATE PROC GetQBRPointForSaleFinByYearMonthAndBusinessType
    @Year INT,
    @Month INT,
    @BusinessTypeID INT
AS
BEGIN

    DECLARE @loyaltyPromotionIDs TABLE
    (
        LoyaltyID INT
    );

    INSERT INTO @loyaltyPromotionIDs
    (
        LoyaltyID
    )
    SELECT DISTINCT
           ID
    FROM dbo.LoyaltyPromotion
    WHERE CompanyKey = @BusinessTypeID;

    DECLARE @hef TABLE
    (
        MemKey INT,
        TheNumberOfExportFile INT,
        TheNumberOfResponseFile INT,
        ReasonForReject VARCHAR(1000)
    );

    INSERT INTO @hef
    (
        MemKey,
        TheNumberOfExportFile
    )
    SELECT MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey,
           COUNT(QantasFileEntry.QantasExportFileKey) AS TheNumberOfExportFile
    FROM dbo.MemberReferenceEntrySummary
        INNER JOIN dbo.QantasFileEntry
            ON QantasFileEntry.MemberReferenceEntrySummaryKey = MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey
        INNER JOIN dbo.QantasExportFile
            ON QantasExportFile.QantasExportFileKey = QantasFileEntry.QantasExportFileKey
        INNER JOIN dbo.LoyaltyPromotion
            ON LoyaltyPromotion.OrganisationforLoyaltyID = QantasExportFile.OrganisationforLoyaltyID
    WHERE FileName LIKE 'A%'
          AND CompanyKey = @BusinessTypeID
    GROUP BY MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey;

    DECLARE @kkkk TABLE
    (
        MemRefKey INT,
        ReasonforRejection VARCHAR(1000),
        AcceptorReject VARCHAR(1000)
    );
    WITH ranked_messages
    AS (SELECT MemKey,
               fff1.AcceptorReject,
               fff1.ReasonforRejection,
               ROW_NUMBER() OVER (PARTITION BY LTRIM(RTRIM(fff1.TransactionID))
                                  ORDER BY fff1.QantasHandbackFileRecordId DESC
                                 ) AS rn
        FROM QantasHandbackFileRecords fff1
            INNER JOIN @hef
                ON MemKey = LTRIM(RTRIM(fff1.TransactionID)))
    INSERT INTO @kkkk
    SELECT MemKey,
           ReasonforRejection,
           AcceptorReject
    FROM ranked_messages;

    DECLARE @jjjj TABLE
    (
        MemRefKey INT,
        thenumberofreponsefile INT
    );
    INSERT INTO @jjjj
    (
        MemRefKey,
        thenumberofreponsefile
    )
    SELECT MemKey,
           COUNT(QantasHandbackFileId) AS TheNumberOfResponseFile
    FROM dbo.QantasHandbackFileRecords
        INNER JOIN @hef
            ON MemKey = LTRIM(RTRIM(TransactionID))
    GROUP BY MemKey;

    UPDATE abc
    SET abc.TheNumberOfResponseFile = gg.thenumberofreponsefile
    FROM @hef abc
        INNER JOIN @jjjj gg
            ON gg.MemRefKey = abc.MemKey;

    ;WITH cte_name
    AS (SELECT MemberReferenceEntryKey,
               MAX(MemberReferenceEntryForManagerApprovalKey) AS MemberReferenceEntryForManagerApprovalKey
        FROM dbo.MemberReferenceEntryAndEntryForManagerApproval
        GROUP BY MemberReferenceEntryKey)
    SELECT *
    FROM
    (
        SELECT MemberReferenceEntry.EntryDateTime AS EntryDate,
               SUM(MemberReferenceEntry.Points) AS Point,
               Company.Name AS BusinessType,
               MemberReferenceEntry.MonthPeriod AS Month,
               MemberReferenceEntry.YearPeriod AS Year,
               Member.Member_key AS MemberKey,
               Member.MemberId AS MemberId,
               Member.TradingName,
               mr.MemberReferenceNumber AS QBRNumber,
               MemberReferenceEntry.CreatedByUser AS CreatedBy,
               CASE
                   WHEN SUM(MemberReferenceEntry.Points) > 0 THEN
                       DATENAME(MONTH, DATEADD(MONTH, MemberReferenceEntry.MonthPeriod, 0) - 1) + '-'
                       + CAST(MemberReferenceEntry.YearPeriod AS VARCHAR(10)) + ' Points Earned'
                   ELSE
                       DATENAME(MONTH, DATEADD(MONTH, MemberReferenceEntry.MonthPeriod, 0) - 1) + '-'
                       + CAST(MemberReferenceEntry.YearPeriod AS VARCHAR(10)) + ' Charged Back'
               END AS Description,
               CASE
                   WHEN
                   (
                       ma.MemberReferenceEntrySummaryKey IS NOT NULL
                       AND abc.AcceptorReject = 'R'
                       AND ISNULL(abc.ReasonforRejection, '') <> ''
                       AND ma.StatusCode = 'MNG_APPROVAL'
                   ) THEN
                       'Qantas Rejected'
                   ELSE
                       ISNULL(StatusName, '')
               END AS Status,
               ma.StatusCode,
               --ISNULL(ma.Comment, '') AS Comment,
               CASE
                   WHEN
                   (
                       ma.MemberReferenceEntrySummaryKey IS NOT NULL
                       AND abc.AcceptorReject = 'R'
                       AND ma.StatusCode = 'MNG_APPROVAL'
                   ) THEN
                       abc.ReasonforRejection
                   ELSE
                       ISNULL(ma.Comment, '')
               END AS Comment,
               Company.Company_key AS BusinessTypeID,
               STUFF(
               (
                   SELECT DISTINCT
                          ',' + CAST(em.MemberReferenceEntryKey AS VARCHAR(MAX))
                   FROM MemberReferenceEntry em
                   WHERE em.Member_Key = Member.Member_key
                         AND em.MonthPeriod = MemberReferenceEntry.MonthPeriod
                         AND em.YearPeriod = MemberReferenceEntry.YearPeriod
                         AND em.EntryDateTime = MemberReferenceEntry.EntryDateTime
                         AND em.CreatedByUser = MemberReferenceEntry.CreatedByUser
                         AND em.LoyaltyPromotionID IN
                             (
                                 SELECT LoyaltyID FROM @loyaltyPromotionIDs
                             )
                   FOR XML PATH('')
               ),
               1,
               1,
               ''
                    ) AS MemberReferenceEntryKeyList,
               (CAST(Member.Member_key AS VARCHAR(50)) + CAST(MemberReferenceEntry.MonthPeriod AS VARCHAR(10))
                + CAST(MemberReferenceEntry.YearPeriod AS VARCHAR(10))
                + CAST(SUM(MemberReferenceEntry.Points) AS VARCHAR(50))
                + CAST(MIN(MemberReferenceEntry.MemberReferenceEntryKey) AS VARCHAR(50))
               ) AS ID,
               LoyaltyPromotion.ID AS LoyaltyPromotionID
        FROM dbo.MemberReferenceEntry
            INNER JOIN dbo.LoyaltyPromotion
                ON ID = MemberReferenceEntry.LoyaltyPromotionID
            INNER JOIN dbo.Company
                ON Company.Company_key = LoyaltyPromotion.CompanyKey
            LEFT JOIN cte_name mrma
                ON mrma.MemberReferenceEntryKey = MemberReferenceEntry.MemberReferenceEntryKey
            LEFT JOIN dbo.MemberReferenceEntryForManagerApproval ma
                ON ma.MemberReferenceEntryForManagerApprovalKey = mrma.MemberReferenceEntryForManagerApprovalKey
            LEFT JOIN dbo.MemberReferenceEntryStatusCode
                ON MemberReferenceEntryStatusCode.StatusCode = ma.StatusCode
            INNER JOIN dbo.Member
                ON Member.Member_key = MemberReferenceEntry.Member_Key
            INNER JOIN
            (
                SELECT RANK() OVER (PARTITION BY member_key ORDER BY CreatedDate DESC) RankId,
                       MemberReferenceNumber,
                       member_key
                FROM MemberReference
                WHERE LEN(ISNULL(MemberReferenceNumber, '')) = 11 -- Only allow length of QBR Number is 11 characters
            ) mr
                ON mr.member_key = Member.Member_key
                   AND mr.RankId = 1
            --Get 
            LEFT JOIN
            (
                SELECT mres.MemKey,
                       CASE
                           WHEN mres.TheNumberOfExportFile <> mres.TheNumberOfResponseFile THEN
                               ''
                           ELSE
                       (ReasonforRejection)
                       END AS ReasonforRejection,
                       --'' AS ReasonforRejection,
                       CASE
                           WHEN mres.TheNumberOfExportFile <> mres.TheNumberOfResponseFile THEN
                               ''
                           ELSE
                       (AcceptorReject)
                       END AS AcceptorReject
                --'' AS AcceptorReject
                FROM @hef mres
                    INNER JOIN @kkkk
                        ON mres.MemKey = [@kkkk].MemRefKey
            ) abc
                ON abc.MemKey = ma.MemberReferenceEntrySummaryKey
        WHERE (
                  mrma.MemberReferenceEntryKey IS NULL -- new Entry
                  OR ma.StatusCode IN ( 'SF_REJECTED', 'MNG_REJECTED', 'MNG_APPROVAL_REJECTED_RECORDS' )
                  OR
                  (
                      ma.MemberReferenceEntrySummaryKey IS NOT NULL
                      AND abc.AcceptorReject = 'R'
                      AND ISNULL(abc.ReasonforRejection, '') <> ''
                      AND ma.StatusCode = 'MNG_APPROVAL'
                  ) -- Qantas Reject

              )
              AND ISNULL(mr.MemberReferenceNumber, '') <> ''
        GROUP BY dbo.MemberReferenceEntry.EntryDateTime,
                 MemberReferenceEntry.CreatedByUser,
                 Company.Name,
                 MemberReferenceEntry.MonthPeriod,
                 MemberReferenceEntry.YearPeriod,
                 Member.Member_key,
                 Member.MemberId,
                 LoyaltyPromotion.ID,
                 Member.TradingName,
                 mr.MemberReferenceNumber,
                 ma.MemberReferenceEntrySummaryKey,
                 abc.AcceptorReject,
                 abc.ReasonforRejection,
                 Name,
                 ma.StatusCode,
                 StatusName,
                 ma.Comment,
                 Company.Company_key
    ) v
    WHERE (
              v.BusinessTypeID = @BusinessTypeID
              AND (
              (
                  (
                      (
                          [Month] <= @Month
                          AND v.Year = @Year
                      )
                      OR v.Year < @Year
                  )
                  OR
                  (
                      v.Point < 0
                      AND [Month] > @Month
                      AND v.Year > @Year
                  )
                  OR
                  (
                      StatusCode IN ( 'MNG_REJECTED', 'SF_REJECTED', 'MNG_APPROVAL_REJECTED_RECORDS' )
                      AND
                      (
                          (
                              [Month] <= @Month
                              AND v.Year = @Year
                          )
                          OR v.Year < @Year
                      )
                  )
              )
                  )
          )
    ORDER BY v.Year,
             v.Month,
             v.MemberId;


END;