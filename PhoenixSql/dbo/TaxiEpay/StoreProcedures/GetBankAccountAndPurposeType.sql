﻿CREATE PROCEDURE GetBankAccountAndPurposeTypeOfMember @MemberKey INT
AS
BEGIN
--DECLARE @AccountTable TABLE(AccountKey INT, AccountName VARCHAR(100), AccountNumber VARCHAR(100), BSB VARCHAR(100), CanDelete BIT)
--INSERT INTO @AccountTable
--SELECT a.Account_key, a.AccountName, a.AccountNumber, a.bsb, (CASE WHEN EXISTS(SELECT TOP 1 1 FROM [dbo].[Transaction] t WHERE t.Account_key = a.Account_key) THEN 0 ELSE 1 END )  
--FROM Account a 
--JOIN 
--WHERE Member_key =  @MemberKey

DECLARE @AccountTable TABLE(AccountKey INT, AccountName VARCHAR(100), AccountNumber VARCHAR(100), BSB VARCHAR(100))
INSERT INTO @AccountTable
SELECT a.Account_key, a.AccountName, a.AccountNumber, a.bsb
FROM Account a 
WHERE Member_key =  @MemberKey

DECLARE @res nvarchar(max) = ''

SELECT @res = '[' +  STUFF((SELECT 
	',{"AccountKey":' +'"'+ CAST(a.AccountKey AS VARCHAR(30))+'"' +
	',"AccountName":' +'"'+ a.AccountName+'"'+
	',"AccountNumber":' +'"'+ a.AccountNumber +'"'+
	--',"CanDelete":' + CAST(a.CanDelete AS VARCHAR(10)) +
	',"BSB":' +'"'+ a.BSB +'"'+
	',"TransactionPurposeDtoAsJson":' +
										(SELECT '[' +  STUFF((SELECT 
										',{"PurposeTypeID":' +'"'+ CAST(pt.PurposeTypeID AS VARCHAR(30))+'"' +
										',"PurposeTypeName":' +'"'+ pt.PurposeTypeName +'"'+
										',"Checked":' + CAST((CASE WHEN ISNULL(ap.ID,'')<> '' THEN 1 ELSE 0 END) AS VARCHAR(10))+'}'
										FROM PurposeType pt
										LEFT JOIN AccountPurposeType ap 
										ON pt.PurposeTypeID = ap.PurposeTypeID AND ap.AccountKey = a.AccountKey
										FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)'),1,1,'') + ']') 
	+'}'

FROM @AccountTable a
FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)'),1,1,'') + ']'

IF ISNULL(@res,'') = ''
	SELECT '[]'
ELSE
	SELECT @res
END