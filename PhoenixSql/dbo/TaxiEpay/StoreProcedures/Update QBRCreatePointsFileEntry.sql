﻿
ALTER PROCEDURE [dbo].[QBRCreatePointsFileEntry]
    @month INT = NULL,
    @year INT = NULL,
    @memberid VARCHAR(10) = NULL,
    @backrun BIT = 0,
    @LoyaltyPromotionID INT = NULL,
    @Filename VARCHAR(100) OUTPUT --, @companykey int
AS
BEGIN

    IF @month IS NULL
    BEGIN
        SET @month = DATEPART(MONTH, DATEADD(MONTH, -3, GETDATE()));
    END;
    IF @year IS NULL
    BEGIN
        SET @year = YEAR(DATEADD(MONTH, -3, GETDATE()));
    END;

    DECLARE @startdate DATE;
    DECLARE @endDate DATE;
    DECLARE @monthname NVARCHAR(20);
    DECLARE @qantasfilekey INT;
    DECLARE @count INT;

    DECLARE @qantasrejectfilekey INT;

    SELECT @startdate = FirstDateOfMonth,
           @endDate = FirstDateOfNextMonth
    FROM FirstDateOfMonth
    WHERE MonthPart = @month
          AND YearPart = @year;

    SELECT @monthname = DATENAME(MONTH, @startdate); -- cast(datename(month, @startdate) as CHAR(3))-- as 


    CREATE TABLE #temp
    (
        MemberReferenceEntrySummaryKey INT,
        points INT,
        QantasExportFileKey INT,
        QBRnumber VARCHAR(50),
        UploadStatus VARCHAR(50),
        QantasExportFileEntryStatusId INT,
		IsQBRDisabled BIT
    );

    INSERT INTO #temp
    (
        MemberReferenceEntrySummaryKey,
        points,
        QantasExportFileKey,
        QBRnumber,
        UploadStatus,
        QantasExportFileEntryStatusId,
		IsQBRDisabled
    )
    SELECT mrs.MemberReferenceEntrySummaryKey,
           mrs.Points,
           NULL,
           REPLACE(mr.MemberReferenceNumber, ' ', ''),
           'Pending',
           2,
		   m.IsDisableQBR
    FROM MemberReferenceEntrySummary mrs
        INNER JOIN Member m
            ON m.Member_key = mrs.Member_key
        INNER JOIN FirstDateOfMonth fdom
            ON fdom.MonthPart = mrs.MonthPeriod
               AND fdom.YearPart = mrs.YearPeriod
        OUTER APPLY
    (
        SELECT TOP 1
               qfe.*,
               qef.ExportDateTime
        FROM QantasFileEntry qfe
            INNER JOIN QantasExportFile qef
                ON qef.QantasExportFileKey = qfe.QantasExportFileKey
        WHERE qfe.MemberReferenceEntrySummaryKey = mrs.MemberReferenceEntrySummaryKey
              --and qfe.UploadStatus is not null
              AND qfe.QantasExportFileEntryStatusId IS NOT NULL
        ORDER BY qef.ExportDateTime DESC
    ) qfe
        OUTER APPLY
    (
        SELECT TOP 1
               *
        FROM MemberReference mr
        WHERE mr.member_key = mrs.Member_key
        ORDER BY CreatedDate DESC
    ) mr
    WHERE (
              (
                  fdom.FirstDateOfMonth <= @startdate
                  AND @backrun = 1
              )
              OR
              (
                  fdom.MonthPart = @month
                  AND fdom.YearPart = @year
                  AND @backrun = 0
              )
          )
          --and mre.MemberReferenceEntryTypeKey = 4
          --and ((mre.MemberReferenceEntryTypeKey = 4 and mre.monthperiod <10 and  mre.yearperiod = 2017) or ( mre.MemberReferenceEntryTypeKey in (1,2,3) and (mre.monthperiod >=10 or mre.yearperiod > 2017)))
          AND LEN(REPLACE(mr.MemberReferenceNumber, ' ', '')) = 11
          --and replace(mr.memberreferencenumber ,' ','') <> isnull(qfe.QBRNumber,'')
          --and ( isnull(qfe.UploadStatus,'') not in ('Approved', 'Pending') or qfe.QantasExportFileEntryStatusId is null)
          AND
          (
              qfe.QantasExportFileEntryStatusId IS NULL
              OR qfe.QantasExportFileEntryStatusId = 7
          )
          AND
          (
              m.MemberId = @memberid
              OR @memberid IS NULL
          )
          AND mrs.LoyaltyPromotionID = @LoyaltyPromotionID;

    SELECT @count = COUNT(MemberReferenceEntrySummaryKey)
    FROM #temp;

    IF (@count > 0)
    BEGIN

        INSERT INTO QantasExportFile
        (
            FileName,
            FileDate,
            IsDeleted,
            IsActive,
            QantasExportFileStatusId,
            OrganisationforLoyaltyID,
            LoyaltyPromotionID
        )
        SELECT MIN(ol.PartnerId) + 'P.'
               + RIGHT('000' + CAST((CAST(MAX(RIGHT(FileName, 3)) AS INT) + 1) AS VARCHAR(4)), 3),
               CAST(GETDATE() AS DATE),
               0,
               1,
               1,
               MIN(ol.OrganisationforLoyaltyId),
               MIN(lp.ID)
        FROM QantasExportFile qef
            INNER JOIN LoyaltyPromotion lp
                ON lp.ID = @LoyaltyPromotionID
            INNER JOIN OrganisationforLoyalty ol
                ON ol.OrganisationforLoyaltyId = lp.OrganisationforLoyaltyID
                   AND qef.OrganisationforLoyaltyID = ol.OrganisationforLoyaltyId;

        SELECT @qantasfilekey = SCOPE_IDENTITY();

        INSERT INTO QantasFileEntry
        (
            MemberReferenceEntrySummaryKey,
            Points,
            QantasExportFileKey,
            QBRNumber,
            UploadStatus,
            QantasExportFileEntryStatusId
        )
        SELECT MemberReferenceEntrySummaryKey,
               points,
               @qantasfilekey,
               QBRnumber,
               UploadStatus,
               QantasExportFileEntryStatusId
        FROM #temp
		WHERE IsQBRDisabled = 0

        INSERT INTO QantasExportFile
        (
            FileName,
            FileDate,
            ExportDateTime,
            IsDeleted,
            IsActive,
            QantasExportFileStatusId
        )
        SELECT 'R' + FileName,
               CAST(GETDATE() AS DATE),
               GETDATE(),
               0,
               1,
               9
        FROM QantasExportFile
        WHERE QantasExportFileKey = @qantasfilekey;

        SELECT @qantasrejectfilekey = SCOPE_IDENTITY();

        INSERT INTO QantasFileEntry
        (
            MemberReferenceEntrySummaryKey,
            Points,
            QantasExportFileKey,
            QBRNumber,
            UploadStatus,
            QantasExportFileEntryStatusId
        )
        SELECT mrs.MemberReferenceEntrySummaryKey,
               mrs.Points,
               @qantasrejectfilekey,
               REPLACE(mr.MemberReferenceNumber, ' ', ''),
               CASE WHEN m.IsDisableQBR = 1 THEN 'Disabled' ELSE 'Invalid ABN format' END,
               7
        FROM MemberReferenceEntrySummary mrs
            INNER JOIN Member m
                ON m.Member_key = mrs.Member_key
            INNER JOIN FirstDateOfMonth fdom
                ON fdom.MonthPart = mrs.MonthPeriod
                   AND fdom.YearPart = mrs.YearPeriod
            OUTER APPLY
        (
            SELECT TOP 1
                   qfe.*,
                   qef.ExportDateTime
            FROM QantasFileEntry qfe
                INNER JOIN QantasExportFile qef
                    ON qef.QantasExportFileKey = qfe.QantasExportFileKey
            WHERE qfe.MemberReferenceEntrySummaryKey = mrs.MemberReferenceEntrySummaryKey
                  --and qfe.UploadStatus is not null
                  --and qfe.QantasExportFileEntryStatusId is not null
                  AND
                  (
                      qfe.QantasExportFileEntryStatusId IS NULL
                      OR qfe.QantasExportFileEntryStatusId = 7
                  )
            ORDER BY qef.ExportDateTime DESC
        ) qfe
            OUTER APPLY
        (
            SELECT TOP 1
                   *
            FROM MemberReference mr
            WHERE mr.member_key = mrs.Member_key
            ORDER BY CreatedDate DESC
        ) mr
        WHERE (
                  (
                      fdom.FirstDateOfMonth <= @startdate
                      AND @backrun = 1
                  )
                  OR
                  (
                      fdom.MonthPart = @month
                      AND fdom.YearPart = @year
                      AND @backrun = 0
                  )
              )
              --and mre.MemberReferenceEntryTypeKey = 4
              --and ((mre.MemberReferenceEntryTypeKey = 4 and mre.monthperiod <10 and  mre.yearperiod = 2017) or ( mre.MemberReferenceEntryTypeKey in (1,2,3) and (mre.monthperiod >=10 or mre.yearperiod > 2017)))
              AND
              (
                  LEN(REPLACE(mr.MemberReferenceNumber, ' ', '')) <> 11
                  OR REPLACE(mr.MemberReferenceNumber, ' ', '') = ''
              )
              --and replace(mr.memberreferencenumber ,' ','') <> isnull(qfe.QBRNumber,'')
              --and ( isnull(qfe.UploadStatus,'') not in ('Approved', 'Pending') or qfe.QantasExportFileEntryStatusId is null)
              AND (qfe.QantasExportFileEntryStatusId IS NULL)
              AND
              (
                  m.MemberId = @memberid
                  OR @memberid IS NULL
              )
              AND mrs.LoyaltyPromotionID = @LoyaltyPromotionID
			  OR m.IsDisableQBR = 1;

        SELECT @Filename = FileName
        FROM QantasExportFile
        WHERE QantasExportFileKey = @qantasfilekey;

    END;

    DROP TABLE #temp;

END;
