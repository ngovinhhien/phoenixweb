﻿CREATE TABLE [dbo].[FeeChargeDetail] (
    [ID]  [INT] IDENTITY(1,1)  NOT NULL,
    [FeeID] INT NOT NULL,
	[MemberKey] INT NOT NULL,
	[Amount] DECIMAL(18, 2) NOT NULL,
	[FrequencyTypeID] INT NULL,
	[IsActive] BIT DEFAULT 1 NOT NULL,
	[CreatedBy] VARCHAR(120) NULL,
	[CreatedDate] DATETIME NULL,
	[FeeChargeStartDate] DATETIME NULL,
	[FeeChargeEndDate] DATETIME NULL,
	[IsDeleted] [bit] NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
ALTER TABLE [dbo].[FeeChargeDetail]  WITH CHECK ADD  CONSTRAINT [FeeChargeDetail_Fee_linked] FOREIGN KEY([FeeID])
REFERENCES [dbo].[Fee] ([ID])
GO
ALTER TABLE [dbo].[FeeChargeDetail]  WITH CHECK ADD  CONSTRAINT [FeeChargeDetail_Frequency_linked] FOREIGN KEY([FrequencyTypeID])
REFERENCES [dbo].[FrequencyType] ([ID])
GO
ALTER TABLE [dbo].[FeeChargeDetail]  WITH CHECK ADD  CONSTRAINT [FeeChargeDetail_Member_linked] FOREIGN KEY([MemberKey])
REFERENCES [dbo].[Member] ([Member_Key])
GO