﻿CREATE TABLE [dbo].[DriverCard] (
    [ID]  [INT] IDENTITY(1,1)  NOT NULL,
	[AccountIdentifier] VARCHAR(20) NOT NULL,
	[StatusID] INT NULL,
	[MemberKey] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO

ALTER TABLE [dbo].[DriverCard]  WITH CHECK ADD  CONSTRAINT [FK_DriverCard_Member] FOREIGN KEY([MemberKey])
REFERENCES [dbo].[Member] ([Member_key])
GO
ALTER TABLE [dbo].[DriverCard]  WITH CHECK ADD  CONSTRAINT [FK_DriverCard_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[DriverCardStatus] ([ID])
GO