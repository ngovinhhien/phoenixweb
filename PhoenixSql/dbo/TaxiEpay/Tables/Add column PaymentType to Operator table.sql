﻿IF COL_LENGTH('[Taxiepay].[dbo].[Operator]', '[PaymentType]') IS NULL
BEGIN
    ALTER TABLE [Taxiepay].[dbo].[Operator]
	ADD [PaymentType] INT NULL;
END

UPDATE dbo.Operator SET PaymentType  = 1 WHERE AllowCash = 0
UPDATE dbo.Operator SET PaymentType  = 2 WHERE AllowCash = 1