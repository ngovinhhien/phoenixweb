﻿CREATE TABLE [dbo].[MemberReference](
	[MemberReferenceKey] [INT] IDENTITY(1,1) NOT NULL,
	[MemberReferenceSourceKey] [INT] NULL,
	[MemberReferenceNumber] [NVARCHAR](50) NULL,
	[member_key] [INT] NULL,
	[CreatedByUser] [NVARCHAR](50) NULL,
	[CreatedDate] [DATETIME] NULL,
 CONSTRAINT [PK_MemberReference] PRIMARY KEY CLUSTERED 
(
	[MemberReferenceKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MemberReference] ADD  CONSTRAINT [DF_MemberReferenceSourceKey]  DEFAULT ((1)) FOR [MemberReferenceSourceKey]
GO

ALTER TABLE [dbo].[MemberReference]  WITH CHECK ADD  CONSTRAINT [FK_MemberReference_Member] FOREIGN KEY([member_key])
REFERENCES [dbo].[Member] ([Member_key])
GO

ALTER TABLE [dbo].[MemberReference] CHECK CONSTRAINT [FK_MemberReference_Member]
GO

ALTER TABLE [dbo].[MemberReference]  WITH CHECK ADD  CONSTRAINT [FK_MemberReference_MemberReferenceSource] FOREIGN KEY([MemberReferenceSourceKey])
REFERENCES [dbo].[MemberReferenceSource] ([MemberReferenceSourceKey])
GO

ALTER TABLE [dbo].[MemberReference] CHECK CONSTRAINT [FK_MemberReference_MemberReferenceSource]
GO

CREATE TRIGGER [dbo].[tblMemberReferenceAuditRecord] ON [dbo].[MemberReference] 
 AFTER INSERT , UPDATE
 AS  BEGIN
		INSERT INTO MemberReferenceAudit(MemberReferenceNumber, member_key, CreatedByUser, CreatedDate)
		SELECT m.MemberReferenceNumber, m.member_key, m.CreatedByUser, GETDATE()
		FROM dbo.MemberReference m
		JOIN Inserted i ON m.MemberReferenceKey = i.MemberReferenceKey
END

ALTER TABLE [dbo].[MemberReference] ENABLE TRIGGER [tblMemberReferenceAuditRecord]
GO