﻿CREATE TABLE [dbo].[FeeChargeHistory] (
    [ID]  [INT] IDENTITY(1,1) NOT NULL,
    [FeeChargeDetailId] INT NOT NULL,
	[ProcessedDate] DATETIME NULL,
	[StartDate] DATETIME NULL,
	[EndDate] DATETIME NULL,
	[Amount] DECIMAL(18, 2) NOT NULL,
	[DocketKey] INT NULL,
	[CreatedBy] VARCHAR(120) NULL,
	[CreatedDate] DATETIME NULL,
	[UpdatedBy] VARCHAR(120) NULL,
	[UpdatedDate] DATETIME NULL
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
ALTER TABLE [dbo].[FeeChargeHistory]  WITH CHECK ADD  CONSTRAINT [FeeChargeHistory_FeeChargeDetail_linked] FOREIGN KEY([FeeChargeDetailId])
REFERENCES [dbo].[FeeChargeDetail] ([ID])
GO
