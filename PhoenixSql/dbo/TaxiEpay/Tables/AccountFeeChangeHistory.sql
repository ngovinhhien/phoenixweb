﻿CREATE TABLE [dbo].[AccountFeeChangeHistory](
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[FeeChargeDetailId] [INT] NULL,
	[OldAmount] [DECIMAL](18, 2) NULL,
	[NewAmount] [DECIMAL](18, 2) NULL,
	[IsActive] [BIT] NULL,
	[Reason] VARCHAR(500) NOT NULL,
	[TicketNumber] VARCHAR(20) NOT NULL,
	[CreatedDate] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](120) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
ALTER TABLE [dbo].[AccountFeeChangeHistory]  WITH CHECK ADD  CONSTRAINT [AccountFeeChangeHistory_FeeChargeDetail_linked] FOREIGN KEY([FeeChargeDetailId])
REFERENCES [dbo].[FeeChargeDetail] ([ID])
GO