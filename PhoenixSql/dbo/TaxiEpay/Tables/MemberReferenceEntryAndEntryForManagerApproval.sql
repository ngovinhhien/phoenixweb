﻿CREATE TABLE [dbo].[MemberReferenceEntryAndEntryForManagerApproval](
	[MemberReferenceEntryKey] [INT] NOT NULL,
	[MemberReferenceEntryForManagerApprovalKey] [INT] NOT NULL,
 CONSTRAINT [PK_MemberReferenceEntryAndEntryForManagerApproval] PRIMARY KEY CLUSTERED 
(
	[MemberReferenceEntryKey] ASC,
	[MemberReferenceEntryForManagerApprovalKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],

CONSTRAINT [FK_MemberReferenceEntryAndEntryForManagerApproval_MemberReferenceEntry] FOREIGN KEY([MemberReferenceEntryKey])
REFERENCES [dbo].[MemberReferenceEntry] ([MemberReferenceEntryKey]),

CONSTRAINT [FK_MemberReferenceEntryAndEntryForManagerApproval_MemberReferenceEntryForManagerApproval] FOREIGN KEY([MemberReferenceEntryForManagerApprovalKey])
REFERENCES [dbo].[MemberReferenceEntryForManagerApproval] ([MemberReferenceEntryForManagerApprovalKey])

) ON [PRIMARY]

