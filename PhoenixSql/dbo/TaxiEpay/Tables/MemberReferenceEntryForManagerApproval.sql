﻿CREATE TABLE [dbo].[MemberReferenceEntryForManagerApproval](
	[MemberReferenceEntryForManagerApprovalKey] [INT] IDENTITY(1,1) NOT NULL,
	[MemberReferenceEntrySummaryKey] [INT] NULL,
	[EntryDateTime] [DATETIME] NULL,
	[Point] [INT] NULL,
	[BusinessTypeID] [INT] NULL,
	[BusinessType] [VARCHAR](50) NULL,
	[OriginalMonth] [INT]             NOT NULL,
    [SendMonth]  [INT]             NOT NULL,
    [SendYear]	[INT]             NOT NULL,
	[OriginalYear]  [INT]             NOT NULL,
	[MemberKey] [INT] NOT NULL,
	[TradingName] [VARCHAR](50) NULL,
	[QBRNumber] [NVARCHAR](50) NULL,
	[Description] [NVARCHAR](1000) NULL,
	[Status] [VARCHAR](100) NULL,
	[Comment] [NVARCHAR](2000) NULL,
	[MemberReferenceEntryKeyList] [VARCHAR](MAX) NULL,
	[IsActive] [BIT] NULL DEFAULT 1,
	[CreatedBy] [NVARCHAR](100) NULL,
	[CreatedDate] [DATETIME] NULL,
	[ModifiedBy] [NVARCHAR](100) NULL,
	[ModifiedDate] [DATETIME] NULL,
 CONSTRAINT [PK_MemberReferenceEntryForManagerApproval] PRIMARY KEY CLUSTERED 
(
	[MemberReferenceEntryForManagerApprovalKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
CONSTRAINT[FK_MemberReferenceEntryForManagerApproval_MemberReferenceEntrySummary] FOREIGN KEY([MemberReferenceEntrySummaryKey])
REFERENCES [dbo].[MemberReferenceEntrySummary] ([MemberReferenceEntrySummaryKey]),
CONSTRAINT [FK_MemberReferenceEntryForManagerApproval_MemberReferenceEntryStatusCode] FOREIGN KEY([StatusCode])
REFERENCES [dbo].[MemberReferenceEntryStatusCode] ([StatusCode])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
