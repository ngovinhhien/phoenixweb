﻿CREATE TYPE [dbo].[TVPGlideboxCommissionForSameDayCashing] AS TABLE(
	[CategoryID] [INT] NULL,
	[ShortName] [VARCHAR](50) NULL,
	[Price] [DECIMAL](10, 2) NULL,
	[Quantity] [INT] NULL,
	[CommissionAmount] [DECIMAL](10, 2) NULL,
	[TotalCommissionAmount] [DECIMAL](10, 2) NULL,
	[IsPercentage] [BIT] NULL
)