﻿CREATE TABLE [dbo].[Fee] (
    [ID]  [INT] IDENTITY(1,1)  NOT NULL,
	[Name] VARCHAR(120) NULL,
	[Description] VARCHAR(MAX) NULL,
    [MinAmount] DECIMAL(18,2) NULL,
	[MaxAmount] DECIMAL(18,2) NULL,
	[FlatFee] BIT DEFAULT 0 NOT NULL,
	[CreatedDate] DATETIME NULL,
	[CreatedBy] VARCHAR(120) NULL,
	[UpdatedDate] DATETIME NULL,
	[IsActive] BIT DEFAULT 1 NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[Fee] WHERE [Name] = 'Account Fee'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[Fee] ([Name], [Description], [MinAmount], [MaxAmount], [FlatFee], [CreatedDate], [CreatedBy], [UpdatedDate], [IsActive]) 
	VALUES('Account Fee','Account Fee', 6.95, 9.95, 0, GETDATE(), 'admin', NULL, 1)
END
GO