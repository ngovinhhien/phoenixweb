﻿CREATE TABLE [dbo].[DriverCardHistory] (
    [ID]  [INT] IDENTITY(1,1)  NOT NULL,
	[ActionBy] VARCHAR(50) NULL,
	[ActionDescription] VARCHAR(200) NULL,
	[Note] VARCHAR(max) NULL,
	[Date] DATETIME NOT NULL,
	[MemberKey] INT NOT NULL,
	[DriverCardId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO

ALTER TABLE [dbo].[DriverCardHistory]  WITH CHECK ADD  CONSTRAINT [FK_DriverCardHistory_Member] FOREIGN KEY([MemberKey])
REFERENCES [dbo].[Member] ([Member_key])
GO
ALTER TABLE [dbo].[DriverCardHistory]  WITH CHECK ADD  CONSTRAINT [FK_DriverCardHistory_DriverCard] FOREIGN KEY([DriverCardId])
REFERENCES [dbo].[DriverCard] ([ID])
GO