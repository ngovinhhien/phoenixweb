﻿IF COL_LENGTH('[Taxiepay].[dbo].[Member]', '[IsDisableQBR]') IS NULL
BEGIN
    ALTER TABLE [Taxiepay].[dbo].[Member]
	ADD [IsDisableQBR] BIT NOT NULL DEFAULT 0;
END