﻿IF COL_LENGTH('[Taxiepay].[dbo].[VehicleType]', '[MaximumTerminalAllocation]') IS NULL
BEGIN
    ALTER TABLE dbo.VehicleType 
	ADD MaximumTerminalAllocation INT NOT NULL DEFAULT(-1)

	UPDATE dbo.VehicleType SET MaximumTerminalAllocation = 1 WHERE Name IN ('eCommerce', 'Live local')
END