﻿CREATE TABLE [dbo].[LGSystem] (
    [LGSystemID]  INT NOT NULL,
    [LGSystemName] VARCHAR (120) NOT NULL
    PRIMARY KEY CLUSTERED ([LGSystemID] ASC)
);
GO

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[LGSystem] WHERE [LGSystemName] = 'LiveSMS'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[LGSystem] ([LGSystemID], [LGSystemName]) 
	VALUES(1, 'LiveSMS')
END
GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[LGSystem] WHERE [LGSystemName] = 'PhoenixWeb'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[LGSystem] ([LGSystemID], [LGSystemName]) 
	VALUES(2, 'PhoenixWeb')
END