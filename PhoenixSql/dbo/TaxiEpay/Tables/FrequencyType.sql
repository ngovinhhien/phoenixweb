﻿CREATE TABLE [dbo].[FrequencyType] (
    [ID]  [INT] IDENTITY(1,1)  NOT NULL,
    [Name] VARCHAR (120) NOT NULL,
	[CreatedBy] VARCHAR(120) NULL,
	[CreatedDate] DATETIME NULL,
	[UpdatedBy] VARCHAR(120) NULL,
	[UpdatedDate] DATETIME NULL
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[FrequencyType] WHERE [Name] = 'Monthly'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[FrequencyType] ([Name], [CreatedBy], [CreatedDate]) 
	VALUES('Monthly', 'admin', GETDATE())
END
GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[FrequencyType] WHERE [Name] = 'Weekly'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[FrequencyType] ([Name], [CreatedBy], [CreatedDate]) 
	VALUES('Weekly', 'admin', GETDATE())
END
GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[FrequencyType] WHERE [Name] = 'Fortnightly'))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[FrequencyType] ([Name], [CreatedBy], [CreatedDate]) 
	VALUES('Fortnightly', 'admin', GETDATE())
END
GO
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[FrequencyType] WHERE [Name] = 'Yearly '))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[FrequencyType] ([Name], [CreatedBy], [CreatedDate]) 
	VALUES('Yearly', 'admin', GETDATE())
END
GO