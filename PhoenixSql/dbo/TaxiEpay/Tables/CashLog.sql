﻿CREATE TABLE [dbo].[CashLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedUser] [varchar](120) NULL,
	[TerminalId] [varchar](20) NOT NULL,
	[BatchNumber] [varchar](120) NOT NULL,
	[MemberId] [varchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
