﻿IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[CadmusinCardType] WHERE [CardTypeKey] = 'JCB'))
BEGIN
	INSERT INTO dbo.CadmusinCardType
(
    CardTypeKey,
    IsActive,
    CreatedBy,
    CreatedDate,
    ModifiedBy,
    ModifiedDate
)
VALUES
(   'JCB',        
    1,      
    N'vunguyen',      
    GETDATE(),
    N'vunguyen',      
    GETDATE()  
    )
END

