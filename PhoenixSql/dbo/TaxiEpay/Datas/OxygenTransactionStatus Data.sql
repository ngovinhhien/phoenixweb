﻿DECLARE @Data AS TABLE(Name VARCHAR(20))
insert into @Data(Name)
values ('Pending'), ('Paid'), ('Failed'), ('Timeout')

INSERT INTO OxygenTransactionStatus(Name)
SELECT new.Name FROM @Data new
LEFT JOIN OxygenTransactionStatus old on new.Name = old.Name
WHERE old.ID IS NULL

