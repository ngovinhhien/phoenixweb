﻿IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveEftpos' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveEftpos', '18,22,26,27', 'All company key related to LiveEftpos', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveTaxi' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveTaxi', '1,20,21,23', 'All company key related to LiveTaxi', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LinkMerchantTemplateID' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LinkMerchantTemplateID', 'link-merchant-phoenix-web', 'Link Merchant Template ID', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioEnableSendSMS' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioEnableSendSMS', 'false', 'This is config for check using twilio verification in PhoenixWeb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioSendSMSVia' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioSendSMSVia', 'sms', 'This is via type for verification in PhoenixWeb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioAuthKey' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioAuthKey', 'bnE3Os769HC8xFr2pgDkDkiHs80oVFtn', 'This is Authencation key of twilio in PhoenixWeb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioCountryCode' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioCountryCode', '61', 'This is country code for send sms verification in PhoenixWeb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioCodeLength' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioCodeLength', '6', 'This is length of verification code in PhoenixWeb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioSendCodeUri' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioSendCodeUri', 'https://api.authy.com/protected/json/phones/verification/start', 'This is url for excute send verify', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixTwilioCheckCodeUri' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixTwilioCheckCodeUri', 'https://api.authy.com/protected/json/phones/verification/check', 'This is url for excute check verification', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'MerchantIdDefaultForEachBusinessType' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'MerchantIdDefaultForEachBusinessType', '[{''BusinessTypeKey'': 1, ''MerchantId'': ''24353542''},{''BusinessTypeKey'': 23, ''MerchantId'': ''24353542''}]', 'MerchantId Default For Each Business Type', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
	

END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'SaleRecipient' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'SaleRecipient', 'hien.ngo@livegroup.com.au;sang.huynh@livegroup.com.au;ai.nguyen@livegroup.com.au', 'Sale Recipient', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'RejectSparkPostTemplateID' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'RejectSparkPostTemplateID', 'qantas-manager-reject-point', 'Reject SparkPost TemplateID', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ReportFolder' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ReportFolder', '/PhoenixReport/', 'Report Folder', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ManagerRecipient' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ManagerRecipient', 'hien.ngo@livegroup.com.au;sang.huynh@livegroup.com.au;ai.nguyen@livegroup.com.au', 'Manager Recipient', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ApprovedSparkPostTemplateID' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ApprovedSparkPostTemplateID', 'qantas-manager-reviewing', 'Approved SparkPost TemplateID', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'MyWebGhostKey' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'MyWebGhostKey', 'zOW4FO5ELLmx86IFE55+putR', 'Secret key used for ghosting to myweb account from phoenix', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END


IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveTaxiGhostUri' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveTaxiGhostUri', 'https://aws-myweb-test.livetaxi.com.au/Account/GhostLogin?token=', 'Live Taxi Ghost Uri', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END


IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveEftposGhostUri' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveEftposGhostUri', 'https://aws-myweb-test.liveeftpos.com.au/Account/GhostLogin?token=', 'Live Eftpos Ghost Uri', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ReportDomainAdmin' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ReportDomainAdmin', 'administrator', 'Report Domain Admin in phoenixweb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ReportDomain' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ReportDomain', 'aws-test-ssrs.livegroup.com.au', 'Report Domain in phoenixweb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ReportDomainPassword' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ReportDomainPassword', 'M56=F*tuQ%IymylZo9qBBhP;t%Y;nN*%', 'Report Domain Password in phoenixweb', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixWebVersion' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixWebVersion', '1.0.13.0', 'Current PhoenixWeb Version', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ApplicationNameForMultiFactor' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ApplicationNameForMultiFactor', 'LiveGroupAus.PhoenixWeb', 'Application name for multi factor', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'EnableMultiFactor' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'EnableMultiFactor', 'true', 'This is for checking using multifactor', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'qbrEndPoint' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'qbrEndPoint', 'https://aws-test-phoenixweb.livegroup.com.au/LivegroupAPI/api/qantas/ValidateQBRNumber', 'Endpoint for validating QBRNumber', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PurposeTypeIdForSettingWeight' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PurposeTypeIdForSettingWeight', '1', 'Purpose Type for setting weight', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixWebMemberCreationDisableBusinessType' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixWebMemberCreationDisableBusinessType', 'DirectLiveEftpos,TaxiApp,Live limo,Glide,LiveSMS', 'List of Business type need to disable when create new member', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveSMSAPI' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveSMSAPI', 'http://www.livesms.com.au/livesms_back/public/', 'LiveSMS api endpoint', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'LiveSMSAPIKey' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'LiveSMSAPIKey', '2C8283C6FB3C316725328CB81437F', 'List of Business type need to disable when create new member', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixWebSparkPostAPIKey' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixWebSparkPostAPIKey', 'a85ba12df866faabfb5d8713ed73a82959b8f543', 'This is spark post api key using in phoenix web', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PhoenixWebDNS' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PhoenixWebDNS', 'https://aws-test-phoenixweb.livegroup.com.au', 'This is dns for PHW project', 'System', '2019-10-01 15:21:17.780', 'System', '2019-10-01 15:21:17.780')
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'PrintReceiptMode' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'PrintReceiptMode', '0,Print full copy-1,Print short copy-2,Disabled', 'All available items for print receipt modes', 'System', '2020-02-13 15:21:17.780', 'System', '2020-02-13 15:21:17.780')
END
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'DisplayGlideboxUI' AND [SystemID] = 2))
BEGIN
	INSERT INTO dbo.SystemConfiguration
	(
		SystemID,
		ConfigurationKey,
		ConfigurationValue,
		Description,
		CreatedBy,
		CreatedDate,
		UpdatedBy,
		UpdatedDate
	)
	VALUES
	(   
		2,         
		'DisplayGlideboxUI',       
		'false',       
		'This is a configuration to allow (not) display UI controls for Glidebox. The value is true or false.', 
		'System',     
		GETDATE(), 
		'System', 
		GETDATE()
	)
IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'MyWebLiveTaxiURL' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'MyWebLiveTaxiURL', 'https://aws-myweb-test.livetaxi.com.au/', 'Myweb live taxi url', 'System', GETDATE(), 'System', GETDATE())
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'MyWebLiveEftposURL' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'MyWebLiveEftposURL', 'https://aws-myweb-test.liveeftpos.com.au/', 'Myweb live eftpos url', 'System', GETDATE(), 'System', GETDATE())
END

IF(NOT EXISTS (SELECT * FROM [TaxiEpay].[dbo].[SystemConfiguration] WHERE [ConfigurationKey] = 'ListMCCRequiredBusinessType' AND [SystemID] = 2))
BEGIN
	INSERT INTO [TaxiEpay].[dbo].[SystemConfiguration] ([SystemID], [ConfigurationKey], [ConfigurationValue], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) 
	VALUES(2, 'ListMCCRequiredBusinessType', '22;26;27', 'Business types that require MCC as a mandatory field', 'tung', GETDATE(), 'tung', GETDATE())
END