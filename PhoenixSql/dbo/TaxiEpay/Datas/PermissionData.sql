﻿--Insert new Permission
IF(Not exists (select * from Permission where PermissionName = 'DriverCardRegister'))
BEGIN
	insert into permission(PermissionName, PermissionDescription)
	values ('DriverCardRegister','Allow user to register card.')
END

IF(Not exists (select * from Permission where PermissionName = 'DriverCardViewAccountDetail'))
BEGIN
	insert into permission(PermissionName, PermissionDescription)
	values ('DriverCardViewAccountDetail','Allow user to view account detail.')
END

IF(Not exists (select * from Permission where PermissionName = 'DriverCardReissue'))
BEGIN
	insert into permission(PermissionName, PermissionDescription)
	values ('DriverCardReissue','Allow user to reissue card.')
END

IF(Not exists (select * from Permission where PermissionName = 'DriverCardUpdateCardStatus'))
BEGIN
	insert into permission(PermissionName, PermissionDescription)
	values ('DriverCardUpdateCardStatus','Allow user to update card status.')
END

--insert admin's rolepermission data
insert into RolePermission (RoleName,RoleId,PermissionId)

SELECT new.RoleName, new.RoleId, new.PermissionId
FROM
(

select a.RoleName, a.RoleId, b.PermissionId from aspnet_Roles a, Permission b
where a.RoleName in ('Administrator') and b.PermissionName in ('DriverCardRegister','DriverCardViewAccountDetail','DriverCardReissue','DriverCardUpdateCardStatus')

) new
LEFT JOIN RolePermission old ON new.PermissionId = old.PermissionId and new.RoleId = old.RoleId
WHERE old.PermissionId IS NULL AND old.RoleId IS NULL
