﻿CREATE VIEW [dbo].[View_GetQBRPointForManager]
AS
SELECT ma.MemberReferenceEntryForManagerApprovalKey,
       ma.EntryDateTime AS EntryDate,
       ISNULL(mc.StatusName, '') AS Status,
       ma.OriginalMonth AS Month,
       ma.SendMonth,
       ma.BusinessTypeID,
       ma.MemberKey,
       Member.MemberId,
       ma.OriginalYear AS Year,
       ma.SendYear,
       dbo.Company.Name AS BusinessType,
       ISNULL(ma.Comment, '') AS Comment,
       ma.CreatedBy,
       ma.Description,
       ma.Point,
       ma.QBRNumber,
       Member.TradingName,
       ma.StatusCode
FROM dbo.MemberReferenceEntryForManagerApproval ma
    JOIN dbo.MemberReferenceEntryStatusCode mc
        ON mc.StatusCode = ma.StatusCode
    JOIN dbo.Member
        ON Member.Member_key = ma.MemberKey
    JOIN dbo.Company
        ON Company.Company_key = ma.BusinessTypeID;
