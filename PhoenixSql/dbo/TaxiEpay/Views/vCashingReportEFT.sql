﻿ALTER VIEW [dbo].[vCashingReportEFT]
AS
SELECT Docket.Vehicle_key,
       Docket.EftTerminal_key,
       Vehicle.Operator_key,
       Docket.Docket_key,
       Member.Member_key,
       LodgementReferenceNo,
       MemberId,
       FirstName,
       LastName,
       TradingName,
       TerminalId,
       Vehicle.VehicleId,
       EftDocketBatchId,
       BatchNumber,
       PayMethodName,
       DocketDate,
       RegisteredState,
       State,

       -- Hack but it gets the answer (could do as a jion using this a sa subquery)
       AVG(Fare - ISNULL(Docket.AutoTipAmount, 0)) AS Fare,
       --avg(isnull(Docket.AutoTipAmount,0)) as AutoTipAmount,
       AVG(Extras) AS Extras,
       SUM(   CASE
                  WHEN TransactionDetailType IN ( 1, 2, 4 ) THEN
                      RateUsed
                  ELSE
                      0
              END
          ) AS CommissionRate,
       SUM(   CASE
                  WHEN TransactionDetailType = 0 THEN
                      Amount
                  ELSE
                      0
              END
          ) AS FareReimbursement,
       SUM(   CASE
                  WHEN TransactionDetailType IN ( 1, 2, 4 ) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS Commissions,
       SUM(   CASE
                  WHEN (TransactionDetailType IN ( 14 )) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS AutoTippingFee,
       SUM(   CASE
                  WHEN (
                           (TransactionDetailType IN ( 5, 6 ))
                           OR (TransactionDetailType > 7)
                       )
                       AND TransactionDetailType NOT IN ( 18, 14, 21 ) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS Other,
       SUM(   CASE
                  WHEN
                  (
                      TransactionDetailType IN ( 7 )
                      AND TransactionDetail.Description LIKE 'Charge back%'
                      AND TransactionDetail.Description NOT LIKE 'Charge back fee%'
                  ) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS Chargebacks,
       SUM(   CASE
                  WHEN
                  (
                      TransactionDetailType IN ( 7 )
                      AND TransactionDetail.Description LIKE 'Charge back Fee%'
                  ) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS ChargebacksFee,
       SUM(   CASE
                  WHEN Amount < 0
                       AND
                       (
                           TransactionDetailType NOT IN ( 7 )
                           OR
                           (
                               TransactionDetailType IN ( 7 )
                               AND TransactionDetail.Description NOT LIKE 'Charge back%'
                           )
                       ) THEN
                      Amount
                  ELSE
                      0
              END
          ) AS Charges,
       SUM(   CASE
                  WHEN Amount > 0 THEN
                      Amount
                  ELSE
                      0
              END
          ) AS Payments,
       SUM(   CASE
                  WHEN TransactionDetailType = 18 THEN
                      Amount
                  ELSE
                      0
              END
          ) AS LevyCharge,
       SUM(Amount) AS TotalPaid,
       Member.Company_key,
       [Transaction].Transaction_key,
       MIN(Docket.AutoTipAmount) AS AutoTipAmount,
	   SUM(CASE WHEN TransactionDetail.TransactionDetailType = 21 THEN TransactionDetail.Amount ELSE 0 END) AS GlideboxCommission
--, TransactionDate, TransactionDetailType, DocketStatus, Sum(Amount) as SumAmount, [Transaction].Office_key, [transaction].ModifiedByUser
FROM TransactionDetail
    INNER JOIN [Transaction]
        ON [Transaction].Transaction_key = TransactionDetail.Transaction_key
    INNER JOIN vEftDocket Docket
        ON Docket.Docket_key = TransactionDetail.Docket_key
    INNER JOIN Member
        ON Member.Member_key = [Transaction].Member_key
    INNER JOIN Vehicle
        ON Vehicle.Vehicle_key = Docket.Vehicle_key
GROUP BY Docket.Vehicle_key,
         Docket.EftTerminal_key,
         Vehicle.Operator_key,
         Docket.Docket_key,
         Member.Member_key,
         LodgementReferenceNo,
         MemberId,
         FirstName,
         LastName,
         TradingName,
         TerminalId,
         Vehicle.VehicleId,
         EftDocketBatchId,
         BatchNumber,
         PayMethodName,
         DocketDate,
         RegisteredState,
         Member.Company_key,
         [Transaction].Transaction_key,
         State;
--, TransactionDate,  TransactionDetailType, DocketStatus, [Transaction].Office_key, [transaction].ModifiedByUser
GO