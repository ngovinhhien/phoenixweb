﻿CREATE VIEW [dbo].[View_RPT_QBRPointDetail]
AS
SELECT MemberReferenceEntry.EntryDateTime AS EntryDate,
       MemberReferenceEntry.Points,
       Company.Name AS BusinessType,
	   Description,
       MemberReferenceEntry.MonthPeriod AS Month,
       MemberReferenceEntry.YearPeriod AS Year,
       Member.Member_key AS MemberKey,
       MemberId,
       Member.Email,
       Member.TradingName,
       mr.MemberReferenceNumber AS QBRNumber,
       MemberReferenceEntryKey
FROM dbo.MemberReferenceEntry
    INNER JOIN dbo.MEMBER
        ON Member.Member_key = MemberReferenceEntry.Member_key
    INNER JOIN dbo.Company
        ON Company.Company_key = Member.Company_key
    INNER JOIN (
        SELECT RANK() OVER(PARTITION BY member_key ORDER BY CreatedDate DESC) RankId, MemberReferenceNumber, member_key
        FROM MemberReference
		WHERE LEN(ISNULL(MemberReferenceNumber,'')) = 11
    ) mr ON mr.member_key = Member.member_key AND mr.RankId = 1
