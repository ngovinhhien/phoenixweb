﻿CREATE VIEW [dbo].[vTransactionDetailTypes]
AS
(
/* View for TransactionDetailType enumeration to assist in reporting
*/
SELECT 0 as TransactionDetailType, 'Total Fare' as [Description] UNION ALL
SELECT 1, 'Manual Commission' UNION ALL 
SELECT 2, 'Operator Commission' UNION ALL 
SELECT 3, 'Dealer Commmission' UNION ALL 
SELECT 4, 'SubDealer Commission' UNION ALL 
SELECT 5, 'Handling Fee' UNION ALL 
SELECT 6, 'Replacement' UNION ALL 
SELECT 7, 'Charge Back' UNION ALL
SELECT 8, 'SameDay' UNION ALL 
SELECT 9, 'Terminal Rental Charge' UNION ALL 
SELECT 10, 'Reverse: Terminal Rental Charge' union all
SELECT 11, 'Terminal Rental Charge' UNION ALL 
SELECT 13, 'Insurance charge' UNION ALL 
SELECT 14, 'Auto Tip Fee' UNION ALL 
SELECT 18, 'Levy Charge' UNION ALL
SELECT 19, 'Account Fee Charge' UNION ALL
SELECT 20, 'Account Fee Refund'
)
GO