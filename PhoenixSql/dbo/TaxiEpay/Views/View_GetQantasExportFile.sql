﻿CREATE VIEW View_GetQantasExportFile
--WITH ENCRYPTION, SCHEMABINDING, VIEW_METADATA
AS
    SELECT DISTINCT QantasExportFile.* FROM dbo.QantasExportFile
	INNER JOIN dbo.QantasFileEntry ON QantasFileEntry.QantasExportFileKey = QantasExportFile.QantasExportFileKey
	INNER JOIN dbo.MemberReferenceEntrySummary ON MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey = QantasFileEntry.MemberReferenceEntrySummaryKey
-- WITH CHECK OPTION
GO
