﻿ SELECT et.TerminalId AS PhoenixTID, et.EftTerminal_key, t.TerminalID AS HostTID, t.TerminalKey AS HostTerminalKey, a.MemberID, a.company_key, t.VAAVersion, t.VAAIPAddress, t.VersionNo, t.ConnectToTMS, t.SerialNumber, t.OSVersion, 
                  a.TradingName, a.Mobile, a.Email, a.Member_key, a.state, d.Dealer_key, o.OverrideTerminalConfiguration AS MemberOverrideConfig, v.OverrideTerminalConfiguration AS VehicleOverrideConfig, 
                  CASE WHEN o.PayByEFT = 1 THEN 'B' ELSE 'C' END AS PaymentMethod, a.ExtendedSettlement, a.NoLevyCharge, ISNULL(com.TwilioSMSFrom, 'Live group') AS TwilioSMSFrom, ISNULL(com.SupportEmail, 'support@livegroup.com.au') 
                  AS SupportEmail, maw.FullAddress AS DeliveryAddress, ISNULL(com.NoReplyEmail, 'no-reply@livegroup.com.au') AS NoReplyEmail, com.PaperRollOrderEmailTemplate, com.SupportCallBackEmailTemplate, 
                  com.ResetPasswordEmailTemplate, m.FirstName, m.LastName, ISNULL(mrr.QantasPointRate, 0) AS QantasPointRate, ISNULL(z.ZipAPIKey, '') AS ZIPAPIKey, ISNULL(z.ZipLocationID, '') AS ZipLocationID, com.BrandName, 
                  com.BrandPhoneNumber, o.PaymentType
FROM     dbo.EftTerminal AS et LEFT OUTER JOIN
                  Transactionhost.dbo.Terminal AS t ON et.TerminalId COLLATE Latin1_General_CI_AS = t.TerminalID AND ISNULL(t.Inactive, 0) = 0 LEFT OUTER JOIN
                  dbo.vTerminalAllocations AS a ON a.EftTerminal_key = et.EftTerminal_key AND a.Member_key IS NOT NULL AND a.IsCurrent = 1 LEFT OUTER JOIN
                  dbo.Operator AS o ON o.Operator_key = a.Member_key LEFT OUTER JOIN
                  dbo.Vehicle AS v ON v.Vehicle_key = a.Vehicle_key AND v.Operator_key = a.Member_key LEFT OUTER JOIN
                  dbo.Dealer AS d ON d.Dealer_key = o.Operator_key LEFT OUTER JOIN
                  dbo.Company AS com ON a.company_key = com.Company_key LEFT OUTER JOIN
                  dbo.Member AS m ON m.Member_key = a.Member_key LEFT OUTER JOIN
                  dbo.MemberAddressView AS maw ON maw.Member_key = m.Member_key AND maw.MemberAddressKey = ISNULL(m.MailingAddressKey, m.PrimaryAddressKey) LEFT OUTER JOIN
                  dbo.MemberRewardRate AS mrr ON mrr.MemberKey = m.Member_key LEFT OUTER JOIN
                  dbo.ZipMerchantAPI AS z ON z.MemberKey = m.Member_key