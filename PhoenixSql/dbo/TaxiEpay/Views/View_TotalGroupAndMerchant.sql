﻿CREATE VIEW [dbo].[View_TotalGroupAndMerchant]
       AS
SELECT [Group].GroupID, GroupName, COUNT(MemberKey) AS TotalMerchant
FROM dbo.[Group]
LEFT JOIN dbo.GroupMember ON GroupMember.GroupID = [Group].GroupID
WHERE IsActive = 1
GROUP BY [Group].GroupID,  GroupName