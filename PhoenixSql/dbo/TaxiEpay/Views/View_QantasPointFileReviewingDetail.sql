﻿CREATE VIEW View_QantasPointFileReviewingDetail
AS
SELECT DISTINCT
       QantasFileEntry.QantasFileEntryKey,
       QantasExportFile.QantasExportFileKey,
       EntryDateTime,
       Company.Name AS BusinessType,
       MemberReferenceEntryForManagerApproval.Description,
       QantasFileEntry.Points,
       MonthPeriod,
       YearPeriod,
       Member.Member_key,
       Member.MemberId,
       TradingName,
       MemberReferenceEntryForManagerApproval.QBRNumber,
       CASE
           WHEN QantasHandbackFileRecords.QantasHandbackFileRecordId IS NULL THEN
               QantasExportFileStatus.Name
           WHEN QantasHandbackFileRecords.QantasHandbackFileRecordId IS NOT NULL
                AND AcceptorReject = 'R' THEN
               'Qantas Rejected'
           ELSE
               'Qantas Approved'
       END AS Status,
       CASE
           WHEN QantasHandbackFileRecords.QantasHandbackFileRecordId IS NULL THEN
               Comment
           WHEN QantasHandbackFileRecords.QantasHandbackFileRecordId IS NOT NULL
                AND AcceptorReject = 'R' THEN
               LTRIM(QantasHandbackFileRecords.ReasonforRejection)
           ELSE
               ''
       END AS Comment,
       MemberReferenceEntryForManagerApproval.CreatedBy AS ReviewBySale,
       MemberReferenceEntryForManagerApproval.ModifiedBy AS ReviewByManager
FROM dbo.QantasFileEntry
    INNER JOIN dbo.MemberReferenceEntrySummary
        ON MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey = QantasFileEntry.MemberReferenceEntrySummaryKey
    INNER JOIN dbo.MemberReferenceEntryForManagerApproval
        ON MemberReferenceEntryForManagerApproval.MemberReferenceEntrySummaryKey = MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey
    INNER JOIN dbo.QantasExportFile
        ON QantasExportFile.QantasExportFileKey = QantasFileEntry.QantasExportFileKey
    INNER JOIN dbo.QantasExportFileStatus
        ON QantasExportFileStatus.QantasExportFileStatusId = QantasExportFile.QantasExportFileStatusId
    LEFT JOIN dbo.QantasHandbackFileRecords
        ON LTRIM(RTRIM(TransactionID)) = MemberReferenceEntrySummary.MemberReferenceEntrySummaryKey
    INNER JOIN dbo.Member
        ON Member.Member_key = MemberReferenceEntryForManagerApproval.MemberKey
    INNER JOIN dbo.Company
        ON Company.Company_key = MemberReferenceEntryForManagerApproval.BusinessTypeID;
-- WITH CHECK OPTION
GO