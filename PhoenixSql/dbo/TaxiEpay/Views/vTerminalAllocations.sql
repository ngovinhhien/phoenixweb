﻿ALTER VIEW [dbo].[vTerminalAllocations]
AS
SELECT   distinct  a.TerminalAllocation_key, et.TerminalId, dbo.Vehicle.VehicleId, dbo.Member.TradingName, a.EffectiveDate, 
                      a.IsCurrent, a.ModifiedByUser, dbo.Member.Member_key, dbo.Member.MemberID, et.EftTerminal_key, dbo.Vehicle.Vehicle_key,
                      a.pendingswapout, a.SwappedTerminalAllocation_key, a.originalterminalAllocation_key, 
                      e.eftterminal_key as swappedeftterminal_key,  e.terminalid as SwappedTerminalID, e2.eftterminal_key as originaleftterminal_key, e2.terminalid as originalterminalid, 
                      t2.effectivedate as FirstEffectiveDate, dbo.TerminalType.name as TerminalType, isnull(dbo.TerminalType.TerminalType_key,0) as TerminalType_key, et.company_key as TerminalBusinessType,
                      dbo.Member.Firstname, dbo.Member.lastname, dbo.Member.address, dbo.Member.state, dbo.Member.postcode, dbo.Member.city, dbo.Member.company_key, dbo.Vehicle.VehicleType_key,
                      --ISNULL(t.deallocateddate, t3.EffectiveDate) AS UnallocatedDate
                      a.deallocateddate as UnallocatedDate, Vehicle.Modified_dttm as VehicleModified_dttm, member.Mobile, member.Telephone, member.Email, et.Status, isnull(TerminalType.VirtualTerminal,0) as VirtualTerminal,
                      isnull(a.AllowMOTO,0) AllowMOTO, isnull(a.AllowRefund,0) AllowRefund,
                      et.PPID, ht.SerialNumber,  Et.SoftwareVersion as Location,
                      isnull(member.MerchantID, '') as MerchantID, et.SIMSerialNumber, ISNULL(dbo.Member.ExtendedSettlement,0) ExtendedSettlement,
                      cast(coalesce(a.NoLevyCharge,dbo.Member.NoLevyCharge, 0) as bit) as NoLevyCharge,
					  dbo.Member.Modified_dttm AS ModifiedDate,
					  dbo.Member.Active AS MemberStatus
FROM         dbo.TerminalAllocation a LEFT OUTER JOIN
                      dbo.Vehicle ON dbo.Vehicle.Vehicle_key = a.Vehicle_key LEFT OUTER JOIN
                      dbo.EftTerminal et ON et.EftTerminal_key = a.EftTerminal_key LEFT OUTER JOIN
                      dbo.TerminalType ON dbo.TerminalType.TerminalType_key = et.terminalType LEFT OUTER JOIN
                      dbo.Member ON dbo.Member.Member_key = a.Member_key
			left join TerminalAllocation t on t.TerminalAllocation_key = a.SwappedTerminalAllocation_key
			left join EftTerminal e ON e.EftTerminal_key = t.EftTerminal_key 
			left join TerminalAllocation t2 on t2.TerminalAllocation_key = a.originalterminalAllocation_key --AND t2.EftTerminal_key IS NOT NULL AND t2.Member_key IS NOT NULL AND t2.Vehicle_key IS NOT NULL
			left join EftTerminal e2 ON e2.EftTerminal_key = t2.EftTerminal_key 
			LEFT JOIN dbo.TerminalAllocation t3 ON a.TerminalAllocation_key = t3.OriginalTerminalAllocation_key AND t3.EftTerminal_key IS NOT NULL AND t3.Member_key IS NULL AND t3.Vehicle_key IS NULL
			--left join PhoenixHostTerminalView v on v.EftTerminal_key = a.EftTerminal_key
			outer apply (
			select top 1 ht.SerialNumber from TransactionHost.dbo.Terminal ht 
			where  et.TerminalId collate Latin1_General_CI_AS = ht.terminalid and isnull(ht.inactive,0) = 0
			order by ht.SerialNumber desc
			) ht
			--left join TransactionHost.dbo.Terminal ht on et.TerminalId collate Latin1_General_CI_AS = ht.terminalid and isnull(ht.inactive,0) = 0 and ht.SerialNumber <> ''
