﻿ALTER PROCEDURE [dbo].[GetTransactionHistoryOfTerminal]
	@TerminalId VARCHAR(10),
    @FromDate DATETIME,
    @ToDate DATETIME,
    @BatchNumber VARCHAR(20) = NULL,
    @CardName VARCHAR(20) = NULL,
	@MinAmount MONEY = NULL,
	@MaxAmount MONEY = NULL,
	@IsApproved BIT = NULL,
	@IsDeclined BIT = NULL,
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT,
	@OnlyDisplayGlideboxTransaction BIT = 0
AS
BEGIN
	DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM View_Transaction',
			@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'

	IF(ISNULL(@TerminalID,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND TerminalID = @TerminalID'
	END
	IF(ISNULL(@BatchNumber,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND BatchNumber = @BatchNumber'
	END
	IF(ISNULL(@CardName,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND CardName = @CardName'
	END
	SET @QueryWhere += CHAR(10) + 'AND StartDateTime >= @FromDate'
	SET @QueryWhere += CHAR(10) + 'AND StartDateTime <= @ToDate'
	SET @QueryWhere += CHAR(10) + 'AND (TotalSaleAmount >= @MinAmount OR @MinAmount = -1)'
	SET @QueryWhere += CHAR(10) + 'AND (TotalSaleAmount <= @MaxAmount OR @MaxAmount = -1)'
	SET @QueryWhere += CHAR(10) + 'AND ((ISNULL(@IsApproved, 1) = 1 AND  Status = ''Approved'') OR (ISNULL(@IsDeclined, 1) = 1 AND  Status = ''Declined'') OR Status NOT IN (''Approved'', ''Declined''))'
	SET @QueryWhere += CHAR(10) + 'AND (@OnlyDisplayGlideboxTransaction = 0 OR (@OnlyDisplayGlideboxTransaction = 1 AND GlideboxAmount > 0))'

	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = ';WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY [StartDateTime] DESC) AS RowId,Status, responsetime, TransactionHistoryKey, TransactionNumber, MerchantID, UserID, BusinessID, TerminalID, ResponseCode, TransPAN, CardExpiry, StartDateTime, EndDateTime, Amount1, Amount2, Amount3, Amount4, Amount5, Amount6, Amount7, Amount8, Amount9, TotalTransaction, ServiceFee, GSTOnServiceFee, TotalSaleAmount, TranAuth, SettlementHistoryKey, AccountType, CardName, TranType, Pickup, Dropoff, ABN, ProcessingType, STAN, ProcessTime, BatchStatus, BatchNumber, GlideboxAmount, GlideboxCommission'
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = 
		N'
			@TerminalId VARCHAR(10),
			@FromDate DATETIME,
			@ToDate DATETIME,
			@BatchNumber VARCHAR(20) = NULL,
			@CardName VARCHAR(20) = NULL,
			@MinAmount MONEY = NULL,
			@MaxAmount MONEY = NULL,
			@IsApproved BIT = NULL,
			@IsDeclined BIT = NULL,
			@OnlyDisplayGlideboxTransaction BIT = 0,
			@RowIDFrom INT = 1,
			@RowIDTo INT = 20,
			@TotalItems INT OUTPUT
		'
	
	EXEC SP_EXECUTESQL @Query, @Params
	, @TerminalID = @TerminalID
	, @FromDate = @FromDate
	, @ToDate = @ToDate
	, @BatchNumber = @BatchNumber
	, @CardName = @CardName
	, @MinAmount = @MinAmount
	, @MaxAmount = @MaxAmount
	, @IsApproved = @IsApproved
	, @IsDeclined = @IsDeclined
	, @OnlyDisplayGlideboxTransaction = @OnlyDisplayGlideboxTransaction
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT
END