﻿ALTER PROCEDURE [dbo].[GetTerminalConfigs] 
	-- Add the parameters for the stored procedure here
@TerminalID VARCHAR(8) ,
@MerchantID VARCHAR(15) 
AS
BEGIN


SELECT DISTINCT
COALESCE(t.inputTimeoutsec, 0) AS InputTimeoutsec,
COALESCE(t.BacklightSec, 0) AS BacklightSec,
COALESCE(t.SleepModeMin, 0) AS SleepModeMin,
COALESCE(t.ScreenBrightness, 0) AS ScreenBrightness,
COALESCE(t.Sound, 0) AS Sound,
COALESCE(t.RemeberMe, 0) AS RemeberMe,
COALESCE(t.ABNSetting, '000000') AS ABNSetting,
COALESCE(t.MinimumCharge, 0) AS MinimumCharge,
COALESCE(t.MaximumCharge, 0) AS MaximumCharge,
CASE WHEN REPLACE(ISNULL(EOSTime, ''),' ','') <>'' THEN COALESCE(RIGHT('0000'+ REPLACE(EOSTime,' ',''),4), '') ELSE '' END AS EOSTime,
COALESCE(t.LoginMode, 'V') AS LoginMode,
'UID'+COALESCE(t.UserIdconfig, 'V') AS UserId,
'BID'+COALESCE(t.BusinessIdconfig,'V') AS BusinessId,
'ABN'+COALESCE(t.ABNconfig, 'V') AS ABN,
COALESCE(t.NetworkTableStatus, '0') AS NetworkTableStatus,
COALESCE(tnw.Prefix,'') AS Prefix,
ISNULL(bt.BusinessTypeName , '') AS LiveBusinessUnit,
dbo.CalculateVersion(COALESCE(t.terminalKey,0), COALESCE(t.VersionNo, 0)) AS TermConfigVer,
COALESCE(t.GPRSReconnectTimer, 20) AS GPRSReconnectTimer,
ISNULL(t.AppRespCode, '010203040506') AS ApprovedResponseCode,
ISNULL(t.AppSignRespCode, '101112Y1Y2Y3') AS ApprovedSignatureResponseCode,
ISNULL(t.IsPreTranPing, 0) AS IsPreTranPing,
--isnull(t.TranAdviceSendMode,0) as TranAdviceSendMode,
CASE WHEN s.terminalid IS NOT NULL 
THEN ISNULL(t.TranAdviceSendMode, 1) 
ELSE ISNULL(t.TranAdviceSendMode, 0)
END AS TranAdviceSendMode,
CASE 
WHEN (t.StateKey IN (3) AND t.BusinessTypeKey = 1 AND t.appkey NOT IN (20) AND ct.TerminalID IS NULL) THEN '1100' --VIC limo
ELSE ISNULL(t.DefaultServiceFee, '1000') 
END AS DefaultServiceFee,
--isnull(t.DefaultServiceFee, '1000')  as DefaultServiceFee,
ISNULL(t.ServiceFeeMode, 'F') AS ServiceFeeMode,
--isnull(t.CustomerPaymentType, '') as CustomerPaymentType,
ISNULL(v.PaymentMethod, '') AS CustomerPaymentType,
--case when o.AllowCash = 1 then 'Cash' when o.PayByEFT = 1 then 'Bank' else  isnull(t.CustomerPaymentType, '') end as CustomerPaymentType,
ISNULL(t.EnableNetworkReceiptPrint, 1) AS EnableNetworkReceiptPrint,
ISNULL(t.EnableMerchantLogoPrint, 1) AS EnableMerchantLogoPrint,
CASE WHEN ISNULL(t.SignatureVerificationTimeout, 0)>999 THEN 999 WHEN ISNULL(t.SignatureVerificationTimeout, 2)<0 THEN 0 ELSE ISNULL(t.SignatureVerificationTimeout, 2) END AS SignatureVerificationTimeout,
ISNULL(t.AutoLogonHour, 24) AS AutoLogonHour,
ISNULL(t.AutoLogonMode,0) AS AutoLogonMode,
ISNULL(t.EnableRefundTran, 0) AS EnableRefundTran,
ISNULL(t.EnableAuthTran, 0) AS EnableAuthTran,
ISNULL(t.EnableTipTran, 0) AS EnableTipTran,
ISNULL(t.EnableCashOutTran, 0) AS EnableCashOutTran,
ISNULL(t.MerchantPassword, '112233') AS MerchantPassword,
ISNULL(t.BankLogonRespCode, 'XT') AS BankLogonRespCode,
ISNULL(t.DefaultBackgroundColour, 'FFFFFF') AS DefaultBackgroundColour,
ISNULL(t.EnableMOTO, 0) AS EnableMOTO,
ISNULL(t.RebootHours,23) AS RebootHours,
ISNULL (t.EnableAutoTip, 0) AS EnableAutoTip,
ISNULL(t.EnableMOTORefund, 0) AS EnableMOTORefund,
--cast(0 as bit) as EnableMOTORefund,
ISNULL(t.AutoEOSMode, 0) AS AutoEOSMode,
CASE WHEN s.terminalid IS NOT NULL 
THEN CAST(0 AS BIT) -- ISNULL(t.EnableDualAPNMode, 0) 
ELSE CAST(1 AS BIT) --ISNULL(t.EnableDualAPNMode, 1) 
END AS EnableDualAPNMode,

CASE WHEN s.terminalid IS NOT NULL 
THEN ISNULL(t.CellTowerUpdateInterval, 0) 
ELSE ISNULL(t.CellTowerUpdateInterval, 0) 
END AS CellTowerUpdateInterval,
CASE WHEN s.terminalid IS NOT NULL 
THEN ISNULL(t.UpdateCellTowerMode, 0) 
ELSE ISNULL(t.UpdateCellTowerMode, 0) 
END AS UpdateCellTowerMode,
--case 
--when tt.terminaltype_key = 5 then '192.168.110.070' 
--when tt.terminaltype_key = 8 then '010.233.205.012'
--else '192.168.110.070' 
--end
--as VAAIPAddress, 
--case 
--when tt.terminaltype_key = 5 then 6100
--when tt.terminaltype_key = 8 then 6005
--else 6100 end
--as VAAPHostPort, 

'' AS VAAIPAddress,
0 AS VAAPHostPort,

--case when s.terminalid is not null 
--then '192.168.110.070'-- isnull(t.VAAIPAddress, '192.168.110.070') 
--else '182.23.212.16' --isnull(t.VAAIPAddress, '203.166.101.062')
--end as VAAIPAddress,
--case when s.terminalid is not null 
--then 6100-- isnull(t.VAAPHostPort, 6100)
--else 2006 --isnull(t.VAAPHostPort, 2006)
-- end as VAAPHostPort,
--isnull(t.ClearOverrideSettings, 0) as ClearOverrideSettings,
CAST(0 AS BIT) AS ClearOverrideSettings,

ISNULL (t.EnableAutoTipTwo, 0) AS EnableAutoTipTwo,
ISNULL(t.EnableReceiptReference,0) AS EnableReceiptReference,
ISNULL(t.BusinessTypeIndicator,1) AS BusinessTypeIndicator,

ISNULL(t.SkinSelection, 0) AS SkinSelection,

ISNULL(t.EnableOnlineOrder, 0) AS EnableOnlineOrder,

--case when v.state ='SA' and v.company_key in (1,23) and l.memberkey IS null
-- then 100 else Coalesce(t.LevyCharge, 0) end as LevyCharge,
--case when v.state ='SA' and v.company_key in (1,23) and l.memberkey IS null
-- then 'LEVY CHARGE' else Coalesce(t.LevyLabel, 'LEVY CHARGE') end as LevyLabel,
 COALESCE(t.LevyCharge, 0) AS LevyCharge,
 COALESCE(t.LevyLabel, 'LEVY CHARGE') LevyLabel,

ISNULL(t.EnableAlipay, 0) AS EnableAlipay,

ISNULL(t.EnableQantasReward, 0) AS EnableQantasReward,

ISNULL(t.EnableBusinessLogo, 0) AS EnableBusinessLogo,
ISNULL(t.EnableQantasPointsRedemption, 0) AS EnableQantasPointsRedemption,
ISNULL(t.CustomFooter, '') AS CustomFooter,
ISNULL(t.AddLevyChargeToAmount, 0) AS AddLevyChargeToAmount,

ISNULL(t.EnablePaperRollOrder, 0) AS EnablePaperRollOrder,
ISNULL(t.EnableSupportCallBack, 0) AS EnableSupportCallBack,

ISNULL(t.RefundTransMax, 0) AS RefundTransMax,

ISNULL(t.RefundCumulativeMax, 0) AS RefundCumulativeMax,
ISNULL(t.enableZip, 0) AS EnableZip
, ISNULL(t.IdleTimer, 0) AS IdleTimer
, ISNULL(t.IdleTransactions, 0) AS IdleTransactions,
ISNULL(tnw.NetworkKey,0) AS DefaultTaxiNetworkID,
t.RecordingMode,
t.UploadTimer,
t.EnableLiveMail,
t.EnableGlidebox,
t.PrintReceiptMode,
t.EnablePrintCustomerReceiptFirst,
t.EnablePrintSettlement
FROM terminal t
LEFT JOIN CabcashTerminal() ct ON ct.Terminalid = t.TerminalID
INNER JOIN Merchant m ON m.MerchantKey = t.MerchantKey
LEFT JOIN merchantgroup mg ON m.MerchantGroupKey = mg.MerchantGroupKey
LEFT JOIN BusinessType bt ON bt.BusinessTypeKey = mg.BusinessTypeKey
LEFT JOIN TerminalNetworkDetail tad ON tad.TNNetworkKey = t.TNNetworkKey
LEFT JOIN Network tnw ON tnw.NetworkKey = tad.NetworkKey
LEFT JOIN [View_SingleAPNTerminal] s ON s.terminalid = t.terminalid
LEFT JOIN TaxiEpay.dbo.PhoenixHostTerminalView v ON v.HostTerminalKey = t.TerminalKey
LEFT JOIN SAMemberWithoutLevy() l ON l.memberkey = v.Member_key
--left join Taxiepay.dbo.eftterminal ter on ter.eftterminal_key = v.eftterminal_key
--left join taxiepay.dbo.terminaltype tt on tt.terminaltype_key = ter.terminaltype
--left join taxiepay.dbo.PhoenixHostTerminalView v on v.HostTID = t.terminalid
--left join taxiepay.dbo.vTerminalAllocations a on v.eftterminal_key = a.eftterminal_key and a.member_key is not null and a.IsCurrent =1
--left join taxiepay.dbo.Operator o on o.Operator_key = a.Member_key
WHERE ISNULL(t.inactive,0) = 0 AND (t.TerminalID = @TerminalID OR @TerminalID IS NULL) AND (m.MerchantID = @MerchantID OR @MerchantID IS  NULL)


END