﻿CREATE PROCEDURE [dbo].[usp_PHW_Glidebox_SearchCategoryHistory]
    @CategoryID INT = NULL,
    @SearchText NVARCHAR(1000) = NULL
AS
BEGIN
    SELECT CONCAT(
                     CategoryNameHistory,
                     CASE
                         WHEN CategoryNameHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     ShortNameHistory,
                     CASE
                         WHEN ShortNameHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     PriceHistory,
                     CASE
                         WHEN PriceHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     CommissionHistory,
                     CASE
                         WHEN PriceHistory <> ''
                              AND CommissionHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     ActivateDeactivateHistory
                 ) AS ActionLog,
           [Source],
           FORMAT(ChangedDatetime, 'dd/MM/yyyy') AS ChangedDatetime,
           ChangedBy
    FROM
    (
        SELECT CategoryHistoryID,
               CategoryID,
               CASE
                   WHEN OldCategoryName IS NULL
                        AND NewCategoryName IS NOT NULL THEN
                       CONCAT('Created new category with information: ', '<br>', '- Category name: ', NewCategoryName)
                   WHEN OldCategoryName IS NOT NULL
                        AND NewCategoryName IS NOT NULL THEN
                       CONCAT('Changed category name from ', OldCategoryName, ' to ', NewCategoryName)
                   ELSE
                       ''
               END AS CategoryNameHistory,
               CASE
                   WHEN OldShortName IS NULL
                        AND NewShortName IS NOT NULL THEN
                       CONCAT('- Short name: ', NewPrice)
                   WHEN OldShortName IS NOT NULL
                        AND NewShortName IS NOT NULL THEN
                       CONCAT('Changed short name from ', OldShortName, ' to ', NewShortName)
                   ELSE
                       ''
               END AS ShortNameHistory,
               CASE
                   WHEN OldPrice IS NULL
                        AND NewPrice IS NOT NULL THEN
                       CONCAT('- Price: ', NewPrice)
                   WHEN OldPrice IS NOT NULL
                        AND NewPrice IS NOT NULL THEN
                       CONCAT('Changed price from ', OldPrice, ' to ', NewPrice)
                   ELSE
                       ''
               END AS PriceHistory,
               CASE
                   WHEN OldCommissionAmount IS NULL
                        AND NewCommissionAmount IS NOT NULL THEN
                       CONCAT('- Commission amount: ', NewCommissionAmount)
                   WHEN OldCommissionAmount IS NOT NULL
                        AND NewCommissionAmount IS NOT NULL THEN
                       CONCAT('Changed commission amount from ', OldCommissionAmount, ' to ', NewCommissionAmount)
                   ELSE
                       ''
               END AS CommissionHistory,
               CASE
                   WHEN IsActive = 1
                        AND Reason IS NOT NULL THEN
                       CONCAT('Activated category with reason: ', Reason)
                   WHEN IsActive = 1
                        AND Reason IS NULL
                        AND [Source] = 'Create' THEN
                       '- Status: activated'
                   WHEN IsActive = 1
                        AND Reason IS NULL
                        AND [Source] = 'Activate' THEN
                       'Activated category'
                   WHEN IsActive = 0
                        AND Reason IS NOT NULL THEN
                       CONCAT('Deactivated category with reason: ', Reason)
                   ELSE
                       ''
               END AS ActivateDeactivateHistory,
               [Source],
               ChangedDatetime,
               ChangedBy
        FROM dbo.GlideBoxCategoryHistory
        WHERE CategoryID = @CategoryID
    ) AS h
    WHERE (@SearchText IS NULL)
          OR
          (
              @SearchText IS NOT NULL
              AND
              (
                  CategoryNameHistory LIKE '%' + @SearchText + '%'
                  OR ShortNameHistory LIKE '%' + @SearchText + '%'
                  OR PriceHistory LIKE '%' + @SearchText + '%'
                  OR CommissionHistory LIKE '%' + @SearchText + '%'
                  OR ActivateDeactivateHistory LIKE '%' + @SearchText + '%'
                  OR FORMAT(ChangedDatetime, 'dd/MM/yyyy') LIKE '%' + @SearchText + '%'
                  OR ChangedBy LIKE '%' + @SearchText + '%'
              )
          )
    ORDER BY ChangedDatetime DESC;
END;