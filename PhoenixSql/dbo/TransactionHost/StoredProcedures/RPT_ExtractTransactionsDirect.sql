﻿ALTER PROCEDURE [dbo].[RPT_ExtractTransactionsDirect]
    -- Add the parameters for the stored procedure here
    @SetLoaded BIT,
    @SettlementHistoryKey INT = NULL
AS
BEGIN

    DECLARE @FileDate DATE;
    DECLARE @sourceFile NVARCHAR(20);
    DECLARE @logoff DATETIME;
    DECLARE @CutOfTime INT;
	DECLARE @BusinessTypeKey INT

    DECLARE @currentdate DATETIME = GETDATE();
    DECLARE @currentdateminus30min DATETIME;
    DECLARE @currentdateplus16hours DATETIME;
    DECLARE @currentdateminus3months DATETIME;

    DECLARE @cadmusid BIGINT;

    SET @currentdateminus30min = DATEADD(MINUTE, -30, @currentdate);

    SET @currentdateplus16hours = DATEADD(HOUR, 16, @currentdate);


    SET @currentdateminus3months = DATEADD(MONTH, -3, @currentdate);


    CREATE TABLE #settlementtemp
    (
        SettlementHistoryKey INT
    );

    INSERT INTO #settlementtemp
    SELECT SettlementHistoryKey
    FROM SettlementHistory sh WITH (NOLOCK)
    WHERE ISNULL(sh.Loaded, 0) = 0
          --and abs(sh.SettlementNetAmount) > 0.1  --uncomment this on production to exlude all less than $0.1 batch
          --and (isnull(sh.CreditNumber,0) +isnull(sh.DebitNumber,0)+isnull(sh.CreditReversalNumber,0) + ISNULL(DebitReversalNumber,0))>0
          AND
          (
              (
                  sh.Logoff < @currentdateplus16hours
                  AND sh.Logoff > @currentdateminus3months
              )
              OR sh.SettlementHistoryKey = @SettlementHistoryKey
          )
          AND
          (
              sh.SettlementHistoryKey = @SettlementHistoryKey
              OR
              (
                  @SettlementHistoryKey IS NULL
                  AND sh.Logoff < @currentdateminus30min
              )
          )
          AND sh.MerchantID IS NOT NULL;
	DECLARE @SettleHistoryWithFiledate TABLE(
		SettlementHistoryKey INT,
		Filedate DATE,
		SourceFile VARCHAR(20)
	)

	DECLARE @SettlementCursor CURSOR;
	SET @SettlementCursor = CURSOR FOR 
	SELECT DISTINCT
           sh.SettlementHistoryKey 
    --into @Temptable
    FROM SettlementHistory sh WITH (NOLOCK)
        INNER JOIN #settlementtemp s
            ON s.SettlementHistoryKey = sh.SettlementHistoryKey
        CROSS APPLY
    (
        SELECT DISTINCT
			t.TransactionHistoryKey
        FROM TransactionHistory t WITH (NOLOCK)
            LEFT JOIN TransactionHistory v WITH (NOLOCK)
                ON v.TerminalID = t.TerminalID
                   AND v.TransactionNumber = t.TransactionNumber
                   AND v.MerchantID = t.MerchantID
                   AND v.ProcessingType = 2
                   AND v.SettlementHistoryKey = sh.SettlementHistoryKey --check for void transaction
            LEFT JOIN Taxiepay.dbo.CadmusIN cad WITH (NOLOCK)
                ON cad.HostTransactionHistoryKey = t.TransactionHistoryKey
            LEFT JOIN TransactionHistory dv WITH (NOLOCK)
                ON dv.TerminalID = t.TerminalID
                   AND dv.TransactionNumber = t.TransactionNumber
                   AND dv.MerchantID = t.MerchantID
                   AND dv.TotalSaleAmount <> t.TotalSaleAmount
                   AND dv.TransactionHistoryKey <> t.TransactionHistoryKey
        WHERE t.SettlementHistoryKey = sh.SettlementHistoryKey
              AND
              (
                  t.ProcessingType IN ( 1, 3 )
                  OR
                  (
                      t.ProcessingType IN ( 2 )
                      AND t.CardName = 'ZIP'
                  )
              )
              AND ABS(t.TotalSaleAmount) >= 0.1 -- exclude all transaction less than $1
              AND t.CadmusINID IS NULL
              AND cad.id IS NULL
              AND dv.TransactionHistoryKey IS NULL --and caddl.id is null
    ) th
        INNER JOIN Merchant mer WITH (NOLOCK)
            ON mer.MerchantID = sh.MerchantID
        LEFT JOIN Taxiepay.dbo.CadmusIN c WITH (NOLOCK)
            ON c.HostTransactionHistoryKey = th.TransactionHistoryKey
		LEFT JOIN Taxiepay.dbo.PhoenixHostTerminalView v -- driver card
                ON v.HostTID = sh.TerminalID
		INNER JOIN Terminal ter
                ON ter.TerminalID = sh.TerminalID
		INNER JOIN App a
                ON ter.AppKey = a.AppKey
		LEFT JOIN BusinessType b
                ON b.BusinessTypeKey = COALESCE(v.company_key, ter.BusinessTypeKey, a.BusinessTypeKey, 1)
    WHERE --(sh.MerchantID = '03009075961' or sh.MerchantID = '03009077967') and
        ISNULL(sh.Loaded, 0) = 0
        AND c.id IS NULL;
	
	DECLARE @SHKey INT
	OPEN @SettlementCursor 
	FETCH NEXT FROM @SettlementCursor INTO @SHKey
	WHILE @@fetch_status = 0 --for each member and account
		BEGIN
			        SELECT @logoff = CASE
									WHEN b.BusinessTypeKey = 1 THEN
										GETDATE()
									ELSE
										Logoff
								END,
						   @CutOfTime = CASE
											WHEN ISNULL(v.ExtendedSettlement, 0) = 1 OR v.PaymentType = 3 THEN
												25
											ELSE
												ISNULL(b.FileDateCutOffTime, 16)
										END --if a member set to have extendedsettlement time, set the cutofftime to 25 so that the script always pick the logoff date as file date
					FROM SettlementHistory sh
						INNER JOIN Terminal t
							ON t.TerminalID = sh.TerminalID
						INNER JOIN App a
							ON t.AppKey = a.AppKey
						LEFT JOIN Taxiepay.dbo.PhoenixHostTerminalView v
							ON v.HostTID = sh.TerminalID
						LEFT JOIN BusinessType b
							ON b.BusinessTypeKey = COALESCE(v.company_key, t.BusinessTypeKey, a.BusinessTypeKey, 1)
					WHERE SettlementHistoryKey = @SHKey;
					
					--Calculate Filedate for the equivalent settlement
					SET @FileDate = CASE
                            WHEN DATEPART(HOUR, @logoff) < @CutOfTime THEN
                                CAST(@logoff AS DATE)
                            ELSE
                                CAST(DATEADD(DAY, 1, @logoff) AS DATE)
                        END;

					--Calculate sourceFile base on filedate and the run option
					IF (@SettlementHistoryKey IS NOT NULL) -- for sending transaction directly to Phoenix check the cut off time. it will be different for different business type.
					BEGIN
						SET @sourceFile
							= N'000000' + RIGHT('0000' + CAST(DATEPART(YEAR, @FileDate) AS VARCHAR(4)), 4)
							  + RIGHT('00' + +CAST(DATEPART(MONTH, @FileDate) AS VARCHAR(2)), 2)
							  + RIGHT('00' + +CAST(DATEPART(DAY, @FileDate) AS VARCHAR(2)), 2) + N'E.csv';
					END;
					ELSE
					BEGIN
						SET @sourceFile
							= N'160000' + RIGHT('0000' + CAST(DATEPART(YEAR, @FileDate) AS VARCHAR(4)), 4)
							  + RIGHT('00' + +CAST(DATEPART(MONTH, @FileDate) AS VARCHAR(2)), 2)
							  + RIGHT('00' + +CAST(DATEPART(DAY, @FileDate) AS VARCHAR(2)), 2) + N'E.csv';
					END;

					INSERT INTO @SettleHistoryWithFiledate(SettlementHistoryKey,Filedate,SourceFile)
					VALUES(@SHKey,@FileDate,@sourceFile);
			FETCH NEXT FROM @SettlementCursor INTO @SHKey
		END
-- driver card
    CREATE TABLE #TempTable
    (
        SettlementHistoryKey INT,
        BatchID VARCHAR(10),
        BatchStatus NVARCHAR(20),
        TransID1 NVARCHAR(10),
        MerchantID NVARCHAR(20),
        MerchantName NVARCHAR(30),
        DriverID NVARCHAR(30),
        Loaded DATETIME,
        Start DATETIME,
        [[End]]] DATETIME,
        BatchNumber NVARCHAR(10),
        TaxiID NVARCHAR(30),
        TerminalID NVARCHAR(30),
        TransType NVARCHAR(30),
        Trans_Status NVARCHAR(30),
        CardName NVARCHAR(30),
        TransPAN NVARCHAR(30),
        TransStart DATETIME,
        TransEnd DATETIME,
        TransFare DECIMAL(10, 2),
        TransExtras DECIMAL(10, 2),
        TransTip DECIMAL(10, 2),
        TransTotalAmount DECIMAL(10, 2),
        TransAuthorisationNumber INT,
        TransPassengers INT,
        TransTarrif INT,
        TransKilometers INT,
        TransPickupZone NVARCHAR(30),
        TransDropoffZone NVARCHAR(30),
        FileDate DATE,
        SourceFile NVARCHAR(30),
        ServiceFee DECIMAL(10, 2),
        GSTOnServiceFee DECIMAL(10, 2),
        TransactionHistoryKey BIGINT,
        BusinessID VARCHAR(40),
        UserID VARCHAR(40),
        HostTransactionNumber VARCHAR(14),
        ResponseCode VARCHAR(2),
        ABN VARCHAR(11),
        CardEntryMode NVARCHAR(10),
        CardHolderName NVARCHAR(50),
        CellTowerID NVARCHAR(50),
        CellTowerMCC NVARCHAR(50),
        CellTowerMNC NVARCHAR(50),
        CellTowerLAC NVARCHAR(50),
        AutoTipAmount MONEY,
        AutoTipFee MONEY,
        LevySurcharge MONEY,
        TransactionGUID VARCHAR(50),
		GlideboxAmount MONEY,
        QFFNumber VARCHAR(20),
        QFFPointsEarned INT,
        PaymentAppMerchantID VARCHAR(50)
    );

    INSERT INTO #TempTable
    (
        SettlementHistoryKey,
        BatchID,
        BatchStatus,
        TransID1,
        MerchantID,
        MerchantName,
        DriverID,
        Loaded,
        Start,
        [[End]]],
        BatchNumber,
        TaxiID,
        TerminalID,
        TransType,
        Trans_Status,
        CardName,
        TransPAN,
        TransStart,
        TransEnd,
        TransFare,
        TransExtras,
        TransTip,
        TransTotalAmount,
        TransAuthorisationNumber,
        TransPassengers,
        TransTarrif,
        TransKilometers,
        TransPickupZone,
        TransDropoffZone,
        FileDate,
        SourceFile,
        ServiceFee,
        GSTOnServiceFee,
        TransactionHistoryKey,
        BusinessID,
        UserID,
        HostTransactionNumber,
        ResponseCode,
        ABN,
        CardEntryMode,
        CellTowerID,
        CellTowerMCC,
        CellTowerMNC,
        CellTowerLAC,
        CardHolderName,
        AutoTipAmount,
        AutoTipFee,
        LevySurcharge, 
		TransactionGUID,
		GlideboxAmount -- QFFNumber, QFFPointsEarned, PaymentAppMerchantID
    )
    SELECT DISTINCT
           sh.SettlementHistoryKey,
           CAST(1000000000 + sh.SettlementHistoryKey AS VARCHAR(10)) AS BatchID,
           'Loaded' AS BatchStatus,
           CASE
               WHEN @SettlementHistoryKey IS NOT NULL THEN
                   SUBSTRING(th.TransactionNumber, 1, 4) + SUBSTRING(th.TransactionNumber, 9, 2)
                   + RIGHT(th.TransactionNumber, 3)
               ELSE
                   SUBSTRING(th.TransactionNumber, 1, 4) + SUBSTRING(th.TransactionNumber, 9, 2)
                   + RIGHT(th.TransactionNumber, 3) --SUBSTRING(th.TransactionNumber, 13,2) 
           END AS TransID1,
           mer.MIDToImport AS MerchantID,
           MNameToImport AS MerchantName,
           CASE
               WHEN ISNUMERIC(LEFT(CAST(REPLACE(th.UserID, '.', '') AS NVARCHAR), 8)) = 1 THEN
                   LEFT(CAST(th.UserID AS NVARCHAR), 8)
               ELSE
                   '0'
           END AS DriverID,
                               --case when @SettlementHistoryKey is not null then  SUBSTRING(th.UserID, 1, 20) else 0 end as DriverID, 
           CONVERT(
                      VARCHAR,
                      ISNULL(   sh.Logoff,
                                MAX(th.EndDateTime) OVER (PARTITION BY th.TerminalID,
                                                                       th.MerchantID,
                                                                       SUBSTRING(th.TransactionNumber, 1, 10)
                                                         )
                            ),
                      120
                  ) AS Loaded,
           CONVERT(
                      VARCHAR,
                      ISNULL(   sh.Logon,
                                MIN(th.StartDateTime) OVER (PARTITION BY th.TerminalID,
                                                                         th.MerchantID,
                                                                         SUBSTRING(th.TransactionNumber, 1, 10)
                                                           )
                            ),
                      120
                  ) AS Start,
           CONVERT(
                      VARCHAR,
                      ISNULL(   sh.Logoff,
                                MAX(th.EndDateTime) OVER (PARTITION BY th.TerminalID,
                                                                       th.MerchantID,
                                                                       SUBSTRING(th.TransactionNumber, 1, 10)
                                                         )
                            ),
                      120
                  ) AS '[End]',
                               --sh.Logoff, th.StartDateTime as TransStart, th.EndDateTime as TransEnd,
           SUBSTRING(sh.BatchNumber, 1, 4) + SUBSTRING(th.TransactionNumber, 9, 2) AS BatchNumber,
           SUBSTRING(th.BusinessID, 1, 10) AS TaxiID,
           sh.TerminalID,
           CASE th.AccountType
               WHEN 'CRD' THEN
                   'Credit'
               WHEN 'SAV' THEN
                   'Debit'
               WHEN 'CHQ' THEN
                   'Debit'
               ELSE
                   'Unknown'
           END AS TransType,
           CASE
               WHEN th.ResponseCode IN ( '00', '08', '11', '76', 'Y1', 'Y3' ) THEN
                   CASE
                       WHEN th.ProcessingType = 2
                            AND th.CardName = 'ZIP' THEN
                           'Refund (' + th.ResponseCode + ')'
                       WHEN th.ProcessingType = 2 THEN
                           'Voided'
                       ELSE
                           'Approved (' + th.ResponseCode + ')'
                   END
               ELSE
                   'Declined (' + th.ResponseCode + ')'
           END AS Trans_Status,
           CASE th.CardName
               WHEN 'DINNERS' THEN
                   'DINERS'
               ELSE
                   th.CardName
           END AS CardName,
                               --substring(th.transpan,1,6) + '*******'+substring(th.transpan,8,3) as TransPAN,
           SUBSTRING(th.TransPAN, 1, 6) + RIGHT('*******' + REPLACE(SUBSTRING(th.TransPAN, 8, 4), ' ', ''), 10) AS TransPAN,
           CONVERT(VARCHAR, th.StartDateTime, 120) AS TransStart,
           CONVERT(VARCHAR, th.EndDateTime, 120) AS TransEnd,
           CASE
               WHEN th.ProcessingType IN ( 3 ) THEN
                   -th.Amount1
               ELSE
                   th.Amount1
           END + ISNULL(th.AutoTipAmount, 0) AS TransFare,
                               --case when th.ProcessingType in ( 3) then isnull(-th.Amount2,0) +isnull(-th.Amount3,0) else isnull(th.Amount2,0) +isnull(th.Amount3,0) end as TransExtras, -- commented this for Daffodil day
           CASE
               WHEN th.ProcessingType IN ( 3 ) THEN
                   ISNULL(-th.Amount2, 0)
               ELSE
                   ISNULL(th.Amount2, 0)
           END AS TransExtras, -- exclude amount3 for Daffodil day
                               --case when th.ProcessingType in ( 3) then isnull(-th.Amount4,0) else isnull(th.Amount4,0) end as TransTip,-- commented this for Daffodil day
           0 TransTip,         --set this to 0 for Daffodil day
                               --case when th.ProcessingType in ( 3) then isnull(-th.TotalSaleAmount ,0) else isnull(th.TotalSaleAmount ,0) end as TransTotalAmount, 
           CASE
               WHEN th.ProcessingType IN ( 3 ) THEN
                   ISNULL(-th.TotalSaleAmount, 0) - ISNULL(-th.Amount3, 0) - ISNULL(-th.Amount4, 0)
               ELSE
                   ISNULL(th.TotalSaleAmount, 0) - ISNULL(th.Amount3, 0) - ISNULL(th.Amount4, 0)
           END AS TransTotalAmount,
           ISNULL(th.STAN, '0') AS transauthorisationnumber,
                               --''''+right('000000'+isnull( th.STAN,'000000'), 6) as TransAuthorisationNumber, 
           0 AS TransPassengers,
           0 AS TransTarrif,
           0 AS TransKilometers,
           ISNULL(pick.LocationName, 'Live') AS TransPickupZone,
           ISNULL(dropoff.LocationName, 'Live') AS TransDropoffZone,
           s.Filedate AS FileDate,
           ISNULL(mer.FilePrefix, '') + s.SourceFile AS sourcefile,
           th.ServiceFee,
           th.GSTOnServiceFee,
           th.TransactionHistoryKey,
           th.BusinessID,
           th.UserID,
           th.TransactionNumber,
           th.ResponseCode,
           th.ABN,
           th.TranEntryMode,
           th.CellTowerID,
           th.CellTowerMCC,
           th.CellTowerMNC,
           th.CellTowerLAC,
           th.CardHolderName,
           th.AutoTipAmount,
           th.AutoTipFee,
           th.LevyCharge,       
		   th.TransactionGUID,
		   th.GlideboxAmount -- th.QFFNumber, th.QFFPointsEarned, th.PaymentAppMerchantID
    --into @Temptable
    FROM SettlementHistory sh WITH (NOLOCK)
        INNER JOIN @SettleHistoryWithFiledate s
            ON s.SettlementHistoryKey = sh.SettlementHistoryKey
        CROSS APPLY
    (
        SELECT DISTINCT
               t.TransactionNumber,
               t.StartDateTime,
               t.Amount1,
               t.Amount2,
               t.BusinessID,
               t.AccountType,
               t.ResponseCode,
               t.CardName,
               t.Pickup,
               t.Dropoff,
               t.EndDateTime,
               t.STAN,
               t.Amount3,
               t.Amount4,
               t.TotalSaleAmount,
               ISNULL(v.ProcessingType, t.ProcessingType) AS ProcessingType,
               t.UserID,
               t.TransPAN,
               t.TerminalID,
               t.MerchantID,
               t.ServiceFee,
               t.GSTOnServiceFee,
               t.TransactionHistoryKey,
               t.ABN,
               t.TranEntryMode,
               t.CellTowerID,
               t.CellTowerMCC,
               t.CellTowerMNC,
               t.CellTowerLAC,
               t.CardHolderName,
               t.AutoTipAmount,
               t.AutoTipFee,
               t.LevyCharge,
               t.TransactionGUID,
			   t.GlideboxAmount,
               t.QFFNumber,
               t.QFFPointsEarned,
               t.PaymentAppMerchantID
        FROM TransactionHistory t WITH (NOLOCK)
            LEFT JOIN TransactionHistory v WITH (NOLOCK)
                ON v.TerminalID = t.TerminalID
                   AND v.TransactionNumber = t.TransactionNumber
                   AND v.MerchantID = t.MerchantID
                   AND v.ProcessingType = 2
                   AND v.SettlementHistoryKey = sh.SettlementHistoryKey --check for void transaction
            --OUTER APPLY(
            --SELECT z.ZipTransactionKey FROM dbo.ZipTransaction z
            --INNER JOIN dbo.TransactionHistory rz ON rz.TransactionGUID = z.TransactionGUID
            -- WHERE z.OriginalTransactionGUID = t.TransactionGUID
            --) z
            LEFT JOIN Taxiepay.dbo.CadmusIN cad WITH (NOLOCK)
                ON cad.HostTransactionHistoryKey = t.TransactionHistoryKey
            --left join TaxiEpay.dbo.CadmusDeclined  caddl with (nolock) on caddl.HostTransactionHistoryKey = t.TransactionHistoryKey
            LEFT JOIN TransactionHistory dv WITH (NOLOCK)
                ON dv.TerminalID = t.TerminalID
                   AND dv.TransactionNumber = t.TransactionNumber
                   AND dv.MerchantID = t.MerchantID
                   AND dv.TotalSaleAmount <> t.TotalSaleAmount
                   AND dv.TransactionHistoryKey <> t.TransactionHistoryKey
        WHERE t.SettlementHistoryKey = sh.SettlementHistoryKey
              AND
              (
                  t.ProcessingType IN ( 1, 3 )
                  OR
                  (
                      t.ProcessingType IN ( 2 )
                      AND t.CardName = 'ZIP'
                  )
              )
              AND ABS(t.TotalSaleAmount) >= 0.1 -- exclude all transaction less than $1
              AND t.CadmusINID IS NULL
              AND cad.id IS NULL
              AND dv.TransactionHistoryKey IS NULL --and caddl.id is null
    --AND z.ZipTransactionKey IS NULL AND NOT (t.CardName = 'ZIP' AND t.ProcessingType IN (3)) --exclude for Zip void transaction
    ) th
        INNER JOIN Merchant mer WITH (NOLOCK)
            ON mer.MerchantID = sh.MerchantID
        LEFT JOIN Location pick WITH (NOLOCK)
            ON pick.LocationID = th.Pickup
        LEFT JOIN Location dropoff WITH (NOLOCK)
            ON dropoff.LocationID = th.Dropoff
        LEFT JOIN Taxiepay.dbo.CadmusIN c WITH (NOLOCK)
            ON c.HostTransactionHistoryKey = th.TransactionHistoryKey
    WHERE --(sh.MerchantID = '03009075961' or sh.MerchantID = '03009077967') and
        ISNULL(sh.Loaded, 0) = 0
        --and abs(sh.SettlementNetAmount) > 0.1  --uncomment this on production to exlude all less than $1 batch

        AND
        (
            sh.Logoff < @currentdateplus16hours
            OR sh.SettlementHistoryKey = @SettlementHistoryKey
        )
        AND
        (
            sh.SettlementHistoryKey = @SettlementHistoryKey
            OR
            (
                @SettlementHistoryKey IS NULL
                AND sh.Logoff < @currentdateminus30min
            )
        )
        AND sh.MerchantID IS NOT NULL
        AND c.id IS NULL;


    --and sh.TerminalID = '35673964'
    --sh.SettlementNetAmount >= 5  
    --and isnull(sh.loaded, 0) = 1 and sh.LoadedDateTime > '2013-04-17 00:00:00' 


    --and sh.LoadedDateTime ='2013-04-04 16:00:12.820'
    --and sh.Logoff <dateadd(hour, 16, cast(cast(getdate() as date) as datetime))


    INSERT INTO Taxiepay.dbo.CadmusIN
    (
        batchid,
        batchstatus,
        transid1,
        merchantid,
        merchantname,
        driverid,
        loaded,
        startshift,
        endshift,
        batchnumber,
        taxiid,
        terminalid,
        transtype,
        trans_status,
        cardname,
        transpan,
        transstart,
        transend,
        transfare,
        transextras,
        transtip,
        transtotalamount,
        transauthorisationnumber,
        transpassengers,
        transtarrif,
        transkilometers,
        transpickupzone,
        transdropoffzone,
        filedate,
        sourcefile,
        HostServiceCharge,
        HostGST,
        HostTransactionHistoryKey,
        ABN,
        CardEntryMode,
        CellTowerID,
        CellTowerMCC,
        CellTowerMNC,
        CellTowerLAC,
        CardHolderName,
        HostTransactionNumber,
        AutoTipAmount,
        AutoTipFee,
        Amount1,
        Amount2,
        Amount3,
        Amount4,
        Amount5,
        Amount6,
        Amount7,
        Amount8,
        Amount9,
        HostBatchNumber,
        ResponseCode,
        CardExpiry,
        LevyCharge,
		HostTransactionGuid,
		GlideboxAmount
    )
    SELECT DISTINCT
           BatchID,
           BatchStatus,
           TransID1,
           t.MerchantID,
           MerchantName,
           DriverID,
           Loaded,
           Start,
           [[End]]] AS '[End]',
           BatchNumber,
           TaxiID,
           t.TerminalID,
           TransType,
           Trans_Status,
           t.CardName,
           t.TransPAN,
           TransStart,
           TransEnd,
           TransFare,
           TransExtras,
           TransTip,
           TransTotalAmount,
           TransAuthorisationNumber,
           TransPassengers,
           TransTarrif,
           TransKilometers,
           TransPickupZone,
           TransDropoffZone,
           FileDate,
           SourceFile,
           t.ServiceFee,
           t.GSTOnServiceFee,
           t.TransactionHistoryKey,
           t.ABN,
           t.CardEntryMode,
           t.CellTowerID,
           t.CellTowerMCC,
           t.CellTowerMNC,
           t.CellTowerLAC,
           t.CardHolderName,
           HostTransactionNumber,
           t.AutoTipAmount,
           t.AutoTipFee,
           th.Amount1,
           th.Amount2,
           th.Amount3,
           th.Amount4,
           th.Amount5,
           th.Amount6,
           th.Amount7,
           th.Amount8,
           th.Amount9,
           LEFT(HostTransactionNumber, 10),
           th.ResponseCode,
           th.CardExpiry,
           th.LevyCharge,
		   t.TransactionGuid,
		   t.GlideboxAmount
    FROM #TempTable t
        LEFT JOIN TransactionHistory th
            ON th.TransactionHistoryKey = t.TransactionHistoryKey
    WHERE Trans_Status LIKE '%Approved%'
    ORDER BY BatchID,
             BatchNumber,
             TransID1;


    INSERT INTO Taxiepay.dbo.CadmusDeclined
    (
        batchid,
        batchstatus,
        transid1,
        merchantid,
        merchantname,
        driverid,
        loaded,
        startshift,
        endshift,
        batchnumber,
        taxiid,
        terminalid,
        transtype,
        trans_status,
        cardname,
        transpan,
        transstart,
        transend,
        transfare,
        transextras,
        transtip,
        transtotalamount,
        transauthorisationnumber,
        transpassengers,
        transtarrif,
        transkilometers,
        transpickupzone,
        transdropoffzone,
        filedate,
        sourcefile,
        HostServiceCharge,
        HostGST,
        HostTransactionHistoryKey,
        ABN,
        CardEntryMode,
        CellTowerID,
        CellTowerMCC,
        CellTowerMNC,
        CellTowerLAC,
        CardHolderName,
		HostTransactionGuid,
		GlideboxAmount
    )
    SELECT DISTINCT
           BatchID,
           BatchStatus,
           TransID1,
           MerchantID,
           MerchantName,
           DriverID,
           Loaded,
           Start,
           [[End]]] AS '[End]',
           BatchNumber,
           TaxiID,
           TerminalID,
           TransType,
           Trans_Status,
           CardName,
           TransPAN,
           TransStart,
           TransEnd,
           TransFare,
           TransExtras,
           TransTip,
           TransTotalAmount,
           TransAuthorisationNumber,
           TransPassengers,
           TransTarrif,
           TransKilometers,
           TransPickupZone,
           TransDropoffZone,
           FileDate,
           SourceFile,
           ServiceFee,
           GSTOnServiceFee,
           TransactionHistoryKey,
           t.ABN,
           t.CardEntryMode,
           t.CellTowerID,
           t.CellTowerMCC,
           t.CellTowerMNC,
           t.CellTowerLAC,
           t.CardHolderName,
		   t.TransactionGuid,
		   t.GlideboxAmount
    FROM #TempTable t
    WHERE Trans_Status LIKE '%Declined%'
    ORDER BY BatchID,
             BatchNumber,
             TransID1;

	INSERT INTO Taxiepay.dbo.GlideboxTransaction
	(
		HostTransactionGuid,
		CategoryID,
		CountSold,
		Price,
		TotalGlideboxAmount,
		CommissionAmount,
		GlideboxCommission,
		IsPercentage,
		CreatedDatetime,
		Createdby
	)
	SELECT DISTINCT
		t.TransactionGUID,
		g.CategoryID,
		g.CountSold,
		CAST(g.TotalGlideboxAmount AS MONEY) / g.CountSold,
		g.TotalGlideboxAmount,
		c.CommissionAmount,
		g.CountSold * c.CommissionAmount,
		0,
		GETDATE(),
		'Auto Import'
	FROM #TempTable t
	INNER JOIN dbo.GlideboxSoldDetail g
		ON t.TransactionGuid COLLATE Latin1_General_CI_AS = g.TransactionGuid
	INNER JOIN dbo.GlideBoxCategory c
		ON g.CategoryID = c.CategoryID
	WHERE t.Trans_Status LIKE '%Approved%' OR t.Trans_Status LIKE '%Declined%'

    IF @SetLoaded = 1
    BEGIN
        UPDATE SettlementHistory
        SET Loaded = 1,
            LoadedDateTime = GETDATE(),
            LoadType = CASE
                           WHEN @SettlementHistoryKey IS NOT NULL THEN
                               1
                           ELSE
                               2
                       END
        WHERE SettlementHistoryKey IN
              (
                  SELECT SettlementHistoryKey FROM #TempTable
              );

    END;


    INSERT INTO Taxiepay.dbo.QFFTransaction
    (
        CadmusinID,
        QFFNumber,
        QFFPointsEarned,
        CreatedDateTime,
        CreatedByUser
    )
    SELECT c.id,
           t.QFFNumber,
           ISNULL(t.QFFPointsEarned, 0) QFFPointsEarned,
           GETDATE(),
           'Auto Import'
    FROM TransactionHistory t
        INNER JOIN Taxiepay.dbo.CadmusIN c
            ON c.HostTransactionHistoryKey = t.TransactionHistoryKey
        LEFT JOIN Taxiepay.dbo.QFFTransaction q
            ON q.CadmusinID = c.id
    WHERE ISNULL(t.QFFNumber, '') <> ''
          AND q.ID IS NULL
          AND t.SettlementHistoryKey = @SettlementHistoryKey;


    INSERT INTO Taxiepay.dbo.QFFTransaction
    (
        CadmusinDeclinedID,
        QFFNumber,
        QFFPointsEarned,
        CreatedDateTime,
        CreatedByUser
    )
    SELECT c.id,
           t.QFFNumber,
           ISNULL(t.QFFPointsEarned, 0) QFFPointsEarned,
           GETDATE(),
           'Auto Import'
    FROM TransactionHistory t
        INNER JOIN Taxiepay.dbo.CadmusDeclined c
            ON c.HostTransactionHistoryKey = t.TransactionHistoryKey
        LEFT JOIN Taxiepay.dbo.QFFTransaction q
            ON q.CadmusinDeclinedID = c.id
    WHERE ISNULL(t.QFFNumber, '') <> ''
          AND q.ID IS NULL
          AND t.SettlementHistoryKey = @SettlementHistoryKey;

    DROP TABLE #TempTable;

    DROP TABLE #settlementtemp;
--where sh.Logoff between '2013-03-26 20:00:00 PM' and '2013-03-26 23:59:59 PM'


END;


--exec RPT_ExtractTransactions 0, 261





--select * from SettlementHistory where SettlementHistoryKey = @SettlementHistoryKey


--select * from SettlementHistory where SettlementHistoryKey = 786284
