﻿CREATE PROCEDURE [dbo].[GetCategoryDetailInTerminal] @TerminalID VARCHAR(8)
AS
BEGIN
	SELECT tc.OrderCategory,
           c.CategoryName,
           c.ShortName,
           c.Price,
           COUNT(p.ProductID) AS Product
    FROM dbo.GlideBoxCategory c
        INNER JOIN dbo.TerminalCategory tc
            ON tc.CategoryID = c.CategoryID
        LEFT JOIN dbo.GlideBoxProduct p
            ON p.CategoryID = c.CategoryID AND p.IsActive = 1
    WHERE tc.TerminalID = @TerminalID
    GROUP BY tc.OrderCategory,
             c.CategoryName,
             c.ShortName,
             c.Price

END;
GO