﻿CREATE PROCEDURE [dbo].[usp_PHW_WebCashing_GetGlideboxCategoriesCommissionForSameDay]
AS
BEGIN
    SELECT DISTINCT
        c.CategoryID,
        c.ShortName,
		c.Price,
        c.CommissionAmount,
		CAST(0 AS BIT) AS IsPercentage
	FROM dbo.TerminalCategoryHistory tc
		INNER JOIN dbo.GlideBoxCategory c
			ON c.CategoryID = tc.CategoryID
	ORDER BY c.CategoryID ASC
END