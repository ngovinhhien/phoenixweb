﻿

CREATE PROCEDURE [dbo].[GetUnReconciledBatches]
	@TerminalID varchar(50),
	@SerialNumber nvarchar(50),
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT

AS
BEGIN
	IF(ISNULL(@TerminalId,'') = '' AND ISNULL(@SerialNumber,'') <> '')
	BEGIN
		SELECT @TerminalId = TerminalId FROM Terminal WHERE SerialNumber = @SerialNumber AND ISNULL(Inactive, 0) = 0
	END
	
	IF(ISNULL(@TerminalId,'') = '')
	BEGIN
		RETURn
	END

	SELECT 
		ISNULL(tl.terminalid, '') AS TerminalID
		, tl.MerchantID
		, v.SerialNumber
		, ISNULL(LEFT(tl.transactionnumber, 10), '') AS batchnumber
		, v.EftTerminal_key 
		, CAST(SUM(CASE WHEN tl.responsecode not in ('00', '08', '11','76', 'Y3', 'Y1') THEN 0 WHEN tl.processingType in(1,4,5,6) THEN tl.TotalSaleAmount ELSE -tl.TotalSaleAmount END) AS DECIMAL(18,2)) AS BatchTotal
		, COUNT(tl.TransactionNumber) AS TransactionCount
		, MIN(sos.SOSAttempt) AS SOSAttempt
		, MIN(ISNULL(eos.EOSAttempt, 0)) AS EOSAttempt
		, MIN(eos.FirstEOSAttempt) AS FirstEOSAttempt	
	INTO #tbTransactionLog
	FROM transactionlog tl
		INNER JOIN TaxiEpay.dbo.PhoenixHostTerminalView v ON v.HostTID = tl.TerminalID and isnull(v.SerialNumber,'') <> ''
		LEFT JOIN TransactionHistory th ON th.TerminalID = tl.TerminalID AND th.TransactionNumber = tl.TransactionNumber AND tl.MerchantID = th.MerchantID
		LEFT JOIN SettlementHistory sh ON sh.TerminalID = tl.TerminalID AND sh.BatchNumber = left(tl.transactionnumber, 10)
		LEFT JOIN TaxiEpay.dbo.CadmusIN c ON c.terminalid = v.PhoenixTID AND c.transstart = tl.StartDateTime
		OUTER APPLY(SELECT TOP 1 LogDateTime AS SOSAttempt FROM settlementlog sl  WHERE sl.TerminalID = tl.TerminalID AND sl.Batchnumber = LEFT(tl.transactionnumber, 10) AND sl.LogType = 2 ORDER BY sl.LogDateTime ) SOS
		OUTER APPLY(SELECT COUNT(sl.SettlementLogKey) AS EOSAttempt, MIN(sl.LogDateTime) AS FirstEOSAttempt FROM settlementlog sl WHERE sl.TerminalID = tl.TerminalID AND sl.Batchnumber = LEFT(tl.transactionnumber, 10) AND sl.LogType = 3 GROUP BY sl.TerminalID, sl.Batchnumber) EOS
	WHERE tl.VAAResponseCode = '00'
		AND th.TransactionHistoryKey IS NULL 
		AND sh.SettlementHistoryKey IS NULL 
		AND c.id IS NULL
		AND tl.UpdateDateTime> DATEADD(MONTH, -6, GETDATE()) and tl.UpdateDateTime < DATEADD(HOUR, -24, GETDATE()) 
		AND tl.TerminalID = @TerminalId
	GROUP BY tl.terminalid, tl.MerchantID,left(tl.transactionnumber, 10),  v.EftTerminal_key , v.SerialNumber
	HAVING SUM(CASE WHEN tl.responsecode not in ('00', '08', '11','76', 'Y3', 'Y1') THEN 0 WHEN tl.processingType in(1,4,5,6) THEN tl.TotalSaleAmount ELSE -tl.TotalSaleAmount END) > 1.0

	IF(@RowIDFrom > 0)
	BEGIN
		SET @RowIDFrom = @RowIDFrom - 1
	END

	SELECT tmp.* 
		, SUM(CASE WHEN TransactionLog.ProcessingType = 1 AND TransactionLog.TranType = 'SALE' THEN 1 ELSE 0 END) AS DebitNumber
		, SUM(CASE WHEN TransactionLog.ProcessingType = 1 AND TransactionLog.TranType = 'SALE' AND TransactionLog.ResponseCode IN ('00', '08', '11','76', 'Y3', 'Y1') THEN TransactionLog.TotalSaleAmount ELSE 0 END) DebitAmount 
		, SUM(CASE WHEN TransactionLog.ProcessingType = 3 AND TransactionLog.TranType = 'REFUND' THEN 1 ELSE 0 END) AS CreditNumber	
		, SUM(CASE WHEN TransactionLog.ProcessingType = 3 AND TransactionLog.TranType = 'REFUND' AND TransactionLog.ResponseCode IN ('00', '08', '11','76', 'Y3', 'Y1') THEN TransactionLog.TotalSaleAmount ELSE 0 END) CreditAmount 
		, SUM(CASE WHEN TransactionLog.ProcessingType = 2 AND TransactionLog.TranType = 'SALE' THEN 1 ELSE 0 END) AS DebitReversalNumber	
		, SUM(CASE WHEN TransactionLog.ProcessingType = 2 AND TransactionLog.TranType = 'SALE' AND TransactionLog.ResponseCode IN ('00', '08', '11','76', 'Y3', 'Y1') THEN TransactionLog.TotalSaleAmount ELSE 0 END) DebitReversalAmount 
		, SUM(CASE WHEN TransactionLog.ProcessingType = 2 AND TransactionLog.TranType = 'REFUND' THEN 1 ELSE 0 END) AS CreditReversalNumber	
		, SUM(CASE WHEN TransactionLog.ProcessingType = 2 AND TransactionLog.TranType = 'REFUND' AND TransactionLog.ResponseCode IN ('00', '08', '11','76', 'Y3', 'Y1') THEN TransactionLog.TotalSaleAmount ELSE 0 END) CreditReversalAmount 
	FROM #tbTransactionLog tmp
		LEFT JOIN TransactionLog ON tmp.TerminalID = TransactionLog.TerminalID
	WHERE TransactionLog.TransactionNumber like tmp.batchnumber + '%' AND TransactionLog.VAAResponseCode = '00'
	GROUP BY tmp.TerminalID, tmp.MerchantID, tmp.SerialNumber, tmp.batchnumber, tmp.EftTerminal_key, tmp.BatchTotal, tmp.TransactionCount, tmp.SOSAttempt, tmp.EOSAttempt, tmp.FirstEOSAttempt
	ORDER BY tmp.SOSAttempt DESC
	OFFSET @RowIdFrom ROWS
	FETCH NEXT @RowIdTo ROWS ONLY OPTION (RECOMPILE);
	
	SELECT @TotalItems = COUNT(*) FROM #tbTransactionLog

	DROP TABLE #tbTransactionLog
END
