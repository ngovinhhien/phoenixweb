﻿CREATE PROCEDURE [dbo].GetActiveCategoryListToAssign  @CategoryName VARCHAR(100) = NULL
AS
BEGIN
    SELECT c.CategoryID,
		   c.CategoryName,
           c.ShortName,
           c.Price,
           COUNT(p.ProductID) AS Product
    FROM dbo.GlideBoxCategory c
        LEFT JOIN dbo.GlideBoxProduct p
            ON p.CategoryID = c.CategoryID
    WHERE (c.CategoryName LIKE '%' + @CategoryName + '%' OR ISNULL(@CategoryName, '') = '')
	AND c.IsActive = 1
    GROUP BY c.CategoryID,
			 c.CategoryName,
             c.ShortName,
             c.Price
	ORDER BY c.CategoryName;
END;
GO
