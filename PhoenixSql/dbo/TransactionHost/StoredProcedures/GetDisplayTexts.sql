﻿ALTER PROCEDURE [dbo].[GetDisplayTexts] 
	-- Add the parameters for the stored procedure here
@TerminalID varchar(8),
@MerchantID varchar(15)
AS
BEGIN



--select 
--COALESCE(p.UserID, ap.UserID,'Enter UserID') as UserIDPrompt,
--COALESCE(p.BusinessID, ap.BusinessID,'Enter BusinessID') as BusinessIDPrompt,
--COALESCE(p.AmountOne, ap.AmountOne,null) as Amount1,
--COALESCE(p.AmountTwo, ap.AmountTwo,null) as Amount2,
--COALESCE(p.AmountThree, ap.AmountThree,null) as Amount3,
--COALESCE(p.AmountFour, ap.AmountFour,null) as Amount4,
--COALESCE(p.AmountFive, ap.AmountFive,null) as Amount5,
--COALESCE(p.AmountSix, ap.AmountSix,null) as Amount6,
--COALESCE(p.AmountSeven, ap.AmountSeven,null) as Amount7,
--COALESCE(p.AmountEight, ap.AmountEight,null) as Amount8,
--COALESCE(p.AmountNine, ap.AmountNine,null) as Amount9,
--dbo.CalculateVersion(COALESCE(P.PromptKey,a.promptkey,0), COALESCE(P.VersionNo, ap.versionno,0)) as PromptTableVer
--select 
--case  t.AppKey when null then p.UserID  else  ap.UserID   end as UserIDPrompt,
--case  t.AppKey when null  then  p.BusinessID  else ap.BusinessID   end as BusinessIDPrompt,
--case  t.AppKey when null  then p.AmountOne    else ap.AmountOne  end as Amount1,
--case  t.AppKey when null  then p.AmountTwo   else ap.AmountTwo  end as Amount2,
--case  t.AppKey when null  then p.AmountThree  else  ap.AmountThree  end as Amount3,
--case  t.AppKey when null  then p.AmountFour   else  ap.AmountFour  end as Amount4,
--case  t.AppKey when null  then p.AmountFive  else  ap.AmountFive  end as Amount5,
--case  t.AppKey when null then p.AmountSix   else  ap.AmountSix  end as Amount6,
--case  t.AppKey when null  then p.AmountSeven  else  ap.AmountSeven  end as Amount7,
--case  t.AppKey when null  then p.AmountEight  else  ap.AmountEight  end as Amount8,
--case  t.AppKey when null then  p.AmountNine  else  ap.AmountNine  end as Amount9,
--case  t.AppKey when null 
--	then  dbo.CalculateVersion(isnull(P.PromptKey,0), isnull(P.VersionNo,0))   
--	else  dbo.CalculateVersion(isnull(ap.promptkey,0), isnull( ap.versionno,0)) 
--end as PromptTableVer

select 
--case when  ap.PromptKey is not null then ap.UserId when p.PromptKey is not null then p.UserId else '' end as UserIDPrompt,
--case when  ap.PromptKey  is not  null then ap.BusinessID when p.PromptKey is not null then p.BusinessID else '' end as BusinessIDPrompt,
--case when  ap.PromptKey is not null then ap.AmountOne when p.PromptKey is not null then p.AmountOne else '' end as Amount1,
--case when  ap.PromptKey is not null then ap.AmountTwo when p.PromptKey is not null then p.AmountTwo else '' end as Amount2,
--case when  ap.PromptKey is not null then ap.AmountThree when p.PromptKey is not null then p.AmountThree else '' end as Amount3,
--case when  ap.PromptKey is not null then ap.AmountFour when p.PromptKey is not null then p.AmountFour else '' end as Amount4,
--case when  ap.PromptKey is not null then ap.AmountFive when p.PromptKey is not null then p.AmountFive else '' end as Amount5,
--case when  ap.PromptKey is not null then ap.AmountSix when p.PromptKey is not null then p.AmountSix else '' end as Amount6,
--case when  ap.PromptKey is not null then ap.AmountSeven when p.PromptKey is not null then p.AmountSeven else '' end as Amount7,
--case when  ap.PromptKey is not null then ap.AmountEight when p.PromptKey is not null then p.AmountEight else '' end as Amount8,
--case when  ap.PromptKey is not null then ap.AmountNine when p.PromptKey is not null then p.AmountNine else '' end as Amount9,
--case 
--when  ap.PromptKey is not null
--	then  dbo.CalculateVersion(isnull(aP.PromptKey,0), isnull(aP.VersionNo,0))  
--when  p.PromptKey is not null 
--	then  dbo.CalculateVersion(isnull(p.promptkey,0), isnull( p.versionno,0))
--else
--	dbo.CalculateVersion(0,0) 
--end as PromptTableVer,
Coalesce(p.UserId, ap.UserId, '') as UserIDPrompt,
Coalesce(p.BusinessID, ap.BusinessID, '') as BusinessIDPrompt,
Coalesce(p.AmountOne, ap.AmountOne, '') as Amount1,
Coalesce(p.AmountTwo, ap.AmountTwo, '') as Amount2,
Coalesce(p.AmountThree, ap.AmountThree, '') as Amount3,
Coalesce(p.AmountFour, ap.AmountFour, '') as Amount4,
Coalesce(p.AmountFive, ap.AmountFive, '') as Amount5,
Coalesce(p.AmountSix, ap.AmountSix, '') as Amount6,
Coalesce(p.AmountSeven, ap.AmountSeven, '') as Amount7,
Coalesce(p.AmountEight, ap.AmountEight, '') as Amount8,
Coalesce(p.AmountNine, ap.AmountNine, '') as Amount9,

case 
	when t.AutoTipNumOne is not null or 
		t.AutoTipNumtwo is not null or 
		t.autotipnumthree is not null or 
		t.autotipnumfour is not null or 
		t.AutoTipTwoNumOne is not null or 
		t.AutoTipTwoNumtwo is not null or 
		t.autotipTwonumthree is not null or 
		t.autotipTwonumfour is not null or
        t.AutoTipDefaultButtonID is not null
	then
		dbo.CalculateVersion(Coalesce(p.PromptKey, ap.promptkey, 0),Coalesce(t.versionno,p.versionno, ap.versionno, 0))
	else
		dbo.CalculateVersion(Coalesce(p.PromptKey, ap.promptkey, 0),Coalesce(p.versionno, ap.versionno, 0)) 
end as PromptTableVer,


Coalesce(p.LocationOnePrompt, ap.LocationOnePrompt, 'Select Location From') as LocationOnePrompt,
Coalesce(p.LocationTwoPrompt, ap.LocationTwoPrompt, 'Select Location To') as LocationTwoPrompt,
Coalesce(p.DisableLocationOne, ap.DisableLocationOne, '0') as DisableLocationOne,
Coalesce(p.DisableLocationTwo, ap.DisableLocationTwo, '0') as DisableLocationTwo,
Coalesce(p.EOSButtonText, ap.EOSButtonText, 'Shift') as EOSButtonText,
Coalesce(p.EOSApprovedText, ap.EOSApprovedText, 'End of Shift[NL]Approved') as EOSApprovedText,
Coalesce(p.EOSConfirmText, ap.EOSConfirmText, 'Stop Shift') as EOSConfirmText,
Coalesce(p.EOSPriorText, ap.EOSPriorText, 'Last Shift') as EOSPriorText,
Coalesce(p.EOSNotFoundText, ap.EOSNotFoundText, 'NO SHIFT[NL]DATA FOUND') as EOSNotFoundText,
Coalesce(p.EOSBatchOpenText, ap.EOSBatchOpenText, 'BATCH IS OPEND[NL][NL]PLEASE END THE[NL]CURRENT SHIFT FIRST[BEP]') as EOSBatchOpenText,


Coalesce(t.AutoTipNumOne, p.AutoTipNumOne, ap.AutoTipNumOne, 0) as AutoTipNumOne,
Coalesce(t.AutoTipNumTwo, p.AutoTipNumTwo, ap.AutoTipNumTwo, 0) as AutoTipNumTwo,
Coalesce(t.AutoTipNumThree, p.AutoTipNumThree, ap.AutoTipNumThree, 0) as AutoTipNumThree,
Coalesce(t.AutoTipNumFour, p.AutoTipNumFour, ap.AutoTipNumFour, 0) as AutoTipNumFour,
Coalesce(t.AutoTipIsPercentOne, p.AutoTipIsPercentOne, ap.AutoTipIsPercentOne, 1) as AutoTipIsPercentOne,
Coalesce(t.AutoTipIsPercentTwo, p.AutoTipIsPercentTwo, ap.AutoTipIsPercentTwo, 1) as AutoTipIsPercentTwo,
Coalesce(t.AutoTipIsPercentThree, p.AutoTipIsPercentThree, ap.AutoTipIsPercentThree, 1) as AutoTipIsPercentThree,
Coalesce(t.AutoTipIsPercentFour, p.AutoTipIsPercentFour, ap.AutoTipIsPercentFour, 1) as AutoTipIsPercentFour,

Coalesce(p.AutoTipDisplayOption, ap.AutoTipDisplayOption, 'A') as AutoTipDisplayOption,
Coalesce(p.EnableAutoTipAmountDisplay, ap.EnableAutoTipAmountDisplay, 0) as EnableAutoTipAmountDisplay,
Coalesce(p.EnableAutoTipTotalDisplay, ap.EnableAutoTipTotalDisplay, 0) as EnableAutoTipTotalDisplay,
Coalesce(p.AutoTipTitle, ap.AutoTipTitle, '') as AutoTipTitle,
Coalesce(p.AutoTipPriorAmountLabel, ap.AutoTipPriorAmountLabel, '') as AutoTipPriorAmountLabel,
Coalesce(p.AutoTipEnterConfirmation, ap.AutoTipEnterConfirmation, 0) as AutoTipEnterConfirmation,

Coalesce(p.AutoTipEOSReportOption, ap.AutoTipEOSReportOption, 'G') as AutoTipEOSReportOption,
Coalesce(p.AutoTipButtonOneFeeCents, ap.AutoTipButtonOneFeeCents, 0) as AutoTipButtonOneFeeCents,
Coalesce(p.AutoTipButtonOneFeePercentage, ap.AutoTipButtonOneFeePercentage, 0) as AutoTipButtonOneFeePercentage,
Coalesce(p.AutoTipButtonTwoFeeCents, ap.AutoTipButtonTwoFeeCents, 0) as AutoTipButtonTwoFeeCents,
Coalesce(p.AutoTipButtonTwoFeePercentage, ap.AutoTipButtonTwoFeePercentage, 0) as AutoTipButtonTwoFeePercentage,

Coalesce(p.AutoTipButtonThreeFeeCents, ap.AutoTipButtonThreeFeeCents, 0) as AutoTipButtonThreeFeeCents,
Coalesce(p.AutoTipButtonThreeFeePercentage, ap.AutoTipButtonThreeFeePercentage, 0) as AutoTipButtonThreeFeePercentage,
Coalesce(p.AutoTipButtonFourFeeCents, ap.AutoTipButtonFourFeeCents, 0) as AutoTipButtonFourFeeCents,
Coalesce(p.AutoTipButtonFourFeePercentage, ap.AutoTipButtonFourFeePercentage, 0) as AutoTipButtonFourFeePercentage,

Coalesce(p.AutoTipManualEntryFeeCents, ap.AutoTipManualEntryFeeCents, 0) as AutoTipManualEntryFeeCents,
Coalesce(p.AutoTipManualEntryFeePercentage, ap.AutoTipManualEntryFeePercentage, 0) as AutoTipManualEntryFeePercentage,

Coalesce(p.AutoTipAmountLabel, ap.AutoTipAmountLabel, '') as AutoTipAmountLabel,
Coalesce(p.AutoTipEOSReportMessage, ap.AutoTipEOSReportMessage, '') as AutoTipEOSReportMessage,
Coalesce(p.AutoTipFeeIncluded, ap.AutoTipFeeIncluded, 1) as AutoTipFeeIncluded,

Coalesce(t.AutoTipButtonOneID, p.AutoTipButtonOneID, ap.AutoTipButtonOneID, 0) as AutoTipButtonOneID,
Coalesce(t.AutoTipButtonTwoID,p.AutoTipButtonTwoID, ap.AutoTipButtonTwoID, 0) as AutoTipButtonTwoID,
Coalesce(t.AutoTipButtonThreeID,p.AutoTipButtonThreeID, ap.AutoTipButtonThreeID, 0) as AutoTipButtonThreeID,
Coalesce(t.AutoTipButtonFourID,p.AutoTipButtonFourID, ap.AutoTipButtonFourID, 0) as AutoTipButtonFourID,
Coalesce(t.AutoTipDefaultButtonID, p.AutoTipDefaultButtonID, ap.AutoTipDefaultButtonID, 0) as AutoTipDefaultButtonID,
Coalesce(t.AutoTipScreenText, p.AutoTipScreenText, ap.AutoTipScreenText, '') as AutoTipScreenText,


Coalesce(t.AutoTipTwoNumOne, p.AutoTipTwoNumOne, ap.AutoTipTwoNumOne, 0) as AutoTipTwoNumOne,
Coalesce(t.AutoTipTwoNumTwo, p.AutoTipTwoNumTwo, ap.AutoTipTwoNumTwo, 0) as AutoTipTwoNumTwo,
Coalesce(t.AutoTipTwoNumThree, p.AutoTipTwoNumThree, ap.AutoTipTwoNumThree, 0) as AutoTipTwoNumThree,
Coalesce(t.AutoTipTwoNumFour, p.AutoTipTwoNumFour, ap.AutoTipTwoNumFour, 0) as AutoTipTwoNumFour,
Coalesce(t.AutoTipTwoIsPercentOne, p.AutoTipTwoIsPercentOne, ap.AutoTipTwoIsPercentOne, 1) as AutoTipTwoIsPercentOne,
Coalesce(t.AutoTipTwoIsPercentTwo, p.AutoTipTwoIsPercentTwo, ap.AutoTipTwoIsPercentTwo, 1) as AutoTipTwoIsPercentTwo,
Coalesce(t.AutoTipTwoIsPercentThree, p.AutoTipTwoIsPercentThree, ap.AutoTipTwoIsPercentThree, 1) as AutoTipTwoIsPercentThree,
Coalesce(t.AutoTipTwoIsPercentFour, p.AutoTipTwoIsPercentFour, ap.AutoTipTwoIsPercentFour, 1) as AutoTipTwoIsPercentFour,

Coalesce(p.AutoTipTwoDisplayOption, ap.AutoTipTwoDisplayOption, 'A') as AutoTipTwoDisplayOption,
Coalesce(p.EnableAutoTipTwoAmountDisplay, ap.EnableAutoTipTwoAmountDisplay, 0) as EnableAutoTipTwoAmountDisplay,
Coalesce(p.EnableAutoTipTwoTotalDisplay, ap.EnableAutoTipTwoTotalDisplay, 0) as EnableAutoTipTwoTotalDisplay,
Coalesce(p.AutoTipTwoTitle, ap.AutoTipTwoTitle, '') as AutoTipTwoTitle,
Coalesce(p.AutoTipTwoPriorAmountLabel, ap.AutoTipTwoPriorAmountLabel, '') as AutoTipTwoPriorAmountLabel,
Coalesce(p.AutoTipTwoEnterConfirmation, ap.AutoTipTwoEnterConfirmation, 0) as AutoTipTwoEnterConfirmation,

Coalesce(p.AutoTipTwoEOSReportOption, ap.AutoTipTwoEOSReportOption, 'G') as AutoTipTwoEOSReportOption,
Coalesce(p.AutoTipTwoButtonOneFeeCents, ap.AutoTipTwoButtonOneFeeCents, 0) as AutoTipTwoButtonOneFeeCents,
Coalesce(p.AutoTipTwoButtonOneFeePercentage, ap.AutoTipTwoButtonOneFeePercentage, 0) as AutoTipTwoButtonOneFeePercentage,
Coalesce(p.AutoTipTwoButtonTwoFeeCents, ap.AutoTipTwoButtonTwoFeeCents, 0) as AutoTipTwoButtonTwoFeeCents,
Coalesce(p.AutoTipTwoButtonTwoFeePercentage, ap.AutoTipTwoButtonTwoFeePercentage, 0) as AutoTipTwoButtonTwoFeePercentage,

Coalesce(p.AutoTipTwoButtonThreeFeeCents, ap.AutoTipTwoButtonThreeFeeCents, 0) as AutoTipTwoButtonThreeFeeCents,
Coalesce(p.AutoTipTwoButtonThreeFeePercentage, ap.AutoTipTwoButtonThreeFeePercentage, 0) as AutoTipTwoButtonThreeFeePercentage,
Coalesce(p.AutoTipTwoButtonFourFeeCents, ap.AutoTipTwoButtonFourFeeCents, 0) as AutoTipTwoButtonFourFeeCents,
Coalesce(p.AutoTipTwoButtonFourFeePercentage, ap.AutoTipTwoButtonFourFeePercentage, 0) as AutoTipTwoButtonFourFeePercentage,

Coalesce(p.AutoTipTwoManualEntryFeeCents, ap.AutoTipTwoManualEntryFeeCents, 0) as AutoTipTwoManualEntryFeeCents,
Coalesce(p.AutoTipTwoManualEntryFeePercentage, ap.AutoTipTwoManualEntryFeePercentage, 0) as AutoTipTwoManualEntryFeePercentage,

Coalesce(p.AutoTipTwoAmountLabel, ap.AutoTipTwoAmountLabel, '') as AutoTipTwoAmountLabel,
Coalesce(p.AutoTipTwoEOSReportMessage, ap.AutoTipTwoEOSReportMessage, '') as AutoTipTwoEOSReportMessage,
Coalesce(p.AutoTipTwoFeeIncluded, ap.AutoTipTwoFeeIncluded, 1) as AutoTipTwoFeeIncluded,

Coalesce(t.AutoTipTwoButtonOneID, p.AutoTipTwoButtonOneID, ap.AutoTipTwoButtonOneID, 0) as AutoTipTwoButtonOneID,
Coalesce(t.AutoTipTwoButtonTwoID,p.AutoTipTwoButtonTwoID, ap.AutoTipTwoButtonTwoID, 0) as AutoTipTwoButtonTwoID,
Coalesce(t.AutoTipTwoButtonThreeID,p.AutoTipTwoButtonThreeID, ap.AutoTipTwoButtonThreeID, 0) as AutoTipTwoButtonThreeID,
Coalesce(t.AutoTipTwoButtonFourID,p.AutoTipTwoButtonFourID, ap.AutoTipTwoButtonFourID, 0) as AutoTipTwoButtonFourID,
Coalesce(t.AutoTipTwoDefaultButtonID, p.AutoTipTwoDefaultButtonID, ap.AutoTipTwoDefaultButtonID, 0) as AutoTipTwoDefaultButtonID,
Coalesce(t.AutoTipTwoScreenText, p.AutoTipTwoScreenText, ap.AutoTipTwoScreenText, '') as AutoTipTwoScreenText,

Coalesce(p.ReceiptReferenceLabel, ap.ReceiptReferenceLabel, '') as ReceiptReferenceLabel,
Coalesce(p.ReceiptReferenceText, ap.ReceiptReferenceText, '') as ReceiptReferenceText,
case when v.state ='SA' and v.company_key in (1,23) and s.memberkey IS null
 then 100 else Coalesce(p.LevyCharge, ap.LevyCharge, 0) end as LevyCharge,
case when v.state ='SA' and v.company_key in (1,23) and s.memberkey IS null
 then 'LEVY CHARGE' else Coalesce(p.LevyLabel, ap.LevyLabel, 'LEVY CHARGE') end as LevyLabel
from terminal t
inner join Merchant m on m.MerchantKey = t.MerchantKey
left join App a on a.AppKey = t.AppKey
left join Prompt ap on ap.PromptKey = a.PromptKey
left join Prompt p on p.PromptKey = t.PromptKey
left join TaxiEpay.dbo.PhoenixHostTerminalView v on v.HostTID = t.TerminalID
left join SAMemberWithoutLevy() s on s.memberkey = v.Member_key
where t.TerminalID = @TerminalID 
and m.MerchantID = @MerchantID
and isnull(t.inactive,0) = 0

END