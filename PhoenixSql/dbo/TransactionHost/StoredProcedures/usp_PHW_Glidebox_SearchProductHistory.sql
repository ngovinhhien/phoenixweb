﻿CREATE PROCEDURE [dbo].[usp_PHW_Glidebox_SearchProductHistory]
    @ProductID INT = NULL,
    @SearchText VARCHAR(1000) = NULL
AS
BEGIN
    SELECT CONCAT(
                     ProductNameHistory,
                     CASE
                         WHEN ProductNameHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     PriceHistory,
                     CASE
                         WHEN PriceHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     QuantityHistory,
                     CASE
                         WHEN QuantityHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     CategoryHistory,
                     CASE
                         WHEN CategoryHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     ImageHistory,
                     CASE
                         WHEN ImageHistory <> ''
                              AND ActivateDeactivateHistory <> '' THEN
                             '<br>'
                         ELSE
                             ''
                     END,
                     ActivateDeactivateHistory
                 ) AS ActionLog,
           [Source],
           FORMAT(ChangedDatetime, 'dd/MM/yyyy') AS ChangedDatetime,
           ChangedBy
    FROM
    (
        SELECT ProductHistoryID,
               ProductID,
               CASE
                   WHEN OldProductName IS NULL
                        AND NewProductName IS NOT NULL THEN
                       CONCAT('Created new product with information: ', '<br>', '- Product name: ', NewProductName)
                   WHEN OldProductName IS NOT NULL
                        AND NewProductName IS NOT NULL THEN
                       CONCAT('Changed product name from ', OldProductName, ' to ', NewProductName)
                   ELSE
                       ''
               END AS ProductNameHistory,
               CASE
                   WHEN OldPrice IS NULL
                        AND NewPrice IS NOT NULL THEN
                       CONCAT('- Price: ', NewPrice)
                   WHEN OldPrice IS NOT NULL
                        AND NewPrice IS NOT NULL THEN
                       CONCAT('Changed price from ', OldPrice, ' to ', NewPrice)
                   ELSE
                       ''
               END AS PriceHistory,
               CASE
                   WHEN OldQuantity IS NULL
                        AND NewQuantity IS NOT NULL THEN
                       CONCAT('- Quantity: ', NewQuantity)
                   WHEN OldQuantity IS NOT NULL
                        AND NewQuantity IS NOT NULL THEN
                       CONCAT('Changed quantity from ', OldQuantity, ' to ', NewQuantity)
                   ELSE
                       ''
               END AS QuantityHistory,
               CASE
                   WHEN OldCategoryID IS NULL
                        AND NewCategoryID IS NOT NULL THEN
                       CONCAT('- Category: ', dbo.fn_PHW_Glidebox_GetCategoryNameByID(NewCategoryID))
                   WHEN OldCategoryID IS NOT NULL
                        AND NewCategoryID IS NOT NULL THEN
                       CONCAT(
                                 'Changed category from ',
                                 dbo.fn_PHW_Glidebox_GetCategoryNameByID(OldCategoryID),
                                 ' to ',
                                 dbo.fn_PHW_Glidebox_GetCategoryNameByID(NewCategoryID)
                             )
                   ELSE
                       ''
               END AS CategoryHistory,
               CASE
                   WHEN OldImageContent IS NULL
                        AND NewImageContent IS NOT NULL
                        AND [Source] = 'Create' THEN
                       '- Uploaded image'
                   WHEN (
                            OldImageContent IS NOT NULL
                            OR NewImageContent IS NOT NULL
                        )
                        AND [Source] = 'Edit' THEN
                       'Changed image'
                   WHEN OldImageContent IS NOT NULL
                        AND NewImageContent IS NULL THEN
                       'Removed image'
                   ELSE
                       ''
               END AS ImageHistory,
               CASE
                   WHEN IsActive = 1
                        AND Reason IS NOT NULL THEN
                       CONCAT('Activated product with reason: ', Reason)
                   WHEN IsActive = 1
                        AND Reason IS NULL
                        AND [Source] = 'Create' THEN
                       '- Status: activated'
                   WHEN IsActive = 1
                        AND Reason IS NULL
                        AND [Source] = 'Activate' THEN
                       'Activated product'
                   WHEN IsActive = 0
                        AND Reason IS NOT NULL THEN
                       CONCAT('Deactivated product with reason: ', Reason)
                   ELSE
                       ''
               END AS ActivateDeactivateHistory,
               [Source],
               ChangedDatetime,
               ChangedBy
        FROM dbo.GlideBoxProductHistory
        WHERE ProductID = @ProductID
    ) AS h
    WHERE (@SearchText IS NULL)
          OR
          (
              @SearchText IS NOT NULL
              AND
              (
                  ProductNameHistory LIKE '%' + @SearchText + '%'
                  OR PriceHistory LIKE '%' + @SearchText + '%'
                  OR QuantityHistory LIKE '%' + @SearchText + '%'
                  OR CategoryHistory LIKE '%' + @SearchText + '%'
                  OR ActivateDeactivateHistory LIKE '%' + @SearchText + '%'
                  OR FORMAT(ChangedDatetime, 'dd/MM/yyyy') LIKE '%' + @SearchText + '%'
                  OR ChangedBy LIKE '%' + @SearchText + '%'
              )
          )
    ORDER BY h.ChangedDatetime DESC;
END;