﻿
CREATE PROCEDURE [dbo].[GetUnreconciledTransaction]
	@TerminalID varchar(50),
	@BatchNumber varchar(50),
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT
AS
BEGIN
		DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM View_Unreconciledtransaction',
			@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'

	IF(ISNULL(@TerminalID,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND TerminalID = @TerminalID'
	END
	IF(ISNULL(@BatchNumber,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND batchnumber = @BatchNumber'
	END

	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = ';WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY [StartDateTime] DESC) AS RowId, TerminalID, Batchnumber as BatchNumber, TotalSaleAmount, ResponseCode, StartDateTime, TransactionNumber, CardName, CardMasked, TranType, ProcessingTypeName ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = N'@TerminalID varchar(50),
	@BatchNumber varchar(50),
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUT'
	
	EXEC SP_EXECUTESQL @Query, @Params
	, @TerminalID = @TerminalID
	, @BatchNumber = @BatchNumber
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT
END
