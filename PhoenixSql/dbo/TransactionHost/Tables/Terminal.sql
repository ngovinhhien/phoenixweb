﻿ALTER TABLE Terminal
ADD IdleTimer INT DEFAULT(0)
	,IdleTransactions INT DEFAULT(0)
GO


GO

--2020/02/24-----------PHW-2406
ALTER TABLE Terminal ADD FooterLogoKey [INT] NULL

--2020/02/24-----------PHW-2431
ALTER TABLE Terminal ADD CardPresentTransactionLimit DECIMAL(18,2) DEFAULT(99999.99) NOT NULL
ALTER TABLE Terminal ADD CardNotPresentTransactionLimit DECIMAL(18,2) DEFAULT(99999.99) NOT NULL