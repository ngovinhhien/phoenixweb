﻿--2020/02/21-----------PHW-2406
ALTER TABLE App ADD FooterLogoKey INT NULL
GO

ALTER TABLE [dbo].[App]  WITH CHECK ADD  CONSTRAINT [FK_App_FooterLogo] FOREIGN KEY([FooterLogoKey])
REFERENCES [dbo].[FooterLogo] ([FooterLogoKey])
GO