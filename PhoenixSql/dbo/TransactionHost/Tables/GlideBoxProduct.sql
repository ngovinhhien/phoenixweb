﻿CREATE TABLE [dbo].[GlideBoxProduct](
	[ProductID] [INT] IDENTITY(1,1) NOT NULL,
	[CategoryID] [INT] NOT NULL,
	[ProductName] [VARCHAR](100) NOT NULL,
	[Price] [DECIMAL](10, 2) NOT NULL,
	[Quantity] [INT] NOT NULL,
	[ImageContent] [VARBINARY](MAX) NULL,
	[IsActive] [BIT] NOT NULL,
	[CreatedDatetime] [DATETIME] NOT NULL,
	[CreatedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_GlideBoxProduct] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[GlideBoxProduct] ADD  CONSTRAINT [DF_GlideBoxProduct_Price]  DEFAULT ((0)) FOR [Price]
GO

ALTER TABLE [dbo].[GlideBoxProduct] ADD  CONSTRAINT [DF_GlideBoxProduct_Quantity]  DEFAULT ((0)) FOR [Quantity]
GO

ALTER TABLE [dbo].[GlideBoxProduct] ADD  CONSTRAINT [DF_GlideBoxProduct_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[GlideBoxProduct]  WITH CHECK ADD  CONSTRAINT [FK_GlideBoxProduct_GlideBoxCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[GlideBoxCategory] ([CategoryID])
GO

ALTER TABLE [dbo].[GlideBoxProduct] CHECK CONSTRAINT [FK_GlideBoxProduct_GlideBoxCategory]
GO