﻿ALTER TABLE Audit_Terminal
ADD IdleTimer INT DEFAULT(0)
	, IdleTransactions INT DEFAULT(0)
GO

--2020/02/24-----------PHW-2406
ALTER TABLE Audit_Terminal ADD FooterLogoKey [INT] NULL

--2020/02/24-----------PHW-2431
ALTER TABLE Audit_Terminal ADD CardPresentTransactionLimit DECIMAL(18,2) NULL
ALTER TABLE Audit_Terminal ADD CardNotPresentTransactionLimit DECIMAL(18,2) NULL