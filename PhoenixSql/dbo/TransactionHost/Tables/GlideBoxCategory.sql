﻿CREATE TABLE [dbo].[GlideBoxCategory](
	[CategoryID] [INT] IDENTITY(1,1) NOT NULL,
	[CategoryName] [VARCHAR](100) NOT NULL,
	[ShortName] [VARCHAR](50) NOT NULL,
	[Price] [DECIMAL](10, 2) NOT NULL,
	[CommissionAmount] [DECIMAL](10, 2) NOT NULL,
	[IsActive] [BIT] NOT NULL,
	[CreatedDatetime] [DATETIME] NOT NULL,
	[CreatedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GlideBoxCategory] ADD  CONSTRAINT [DF_Category_Price]  DEFAULT ((0)) FOR [Price]
GO

ALTER TABLE [dbo].[GlideBoxCategory] ADD  CONSTRAINT [DF_Category_CommissionAmount]  DEFAULT ((0)) FOR [CommissionAmount]
GO

ALTER TABLE [dbo].[GlideBoxCategory] ADD  CONSTRAINT [DF_Category_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO