﻿CREATE TABLE [dbo].[GlideBoxProductHistory](
	[ProductHistoryID] [INT] IDENTITY(1,1) NOT NULL,
	[ProductID] [INT] NOT NULL,
	[OldProductName] [VARCHAR](100) NULL,
	[NewProductName] [VARCHAR](100) NULL,
	[OldPrice] [DECIMAL](10, 2) NULL,
	[NewPrice] [DECIMAL](10, 2) NULL,
	[OldQuantity] [INT] NULL,
	[NewQuantity] [INT] NULL,
	[OldImageContent] [VARBINARY](MAX) NULL,
	[NewImageContent] [VARBINARY](MAX) NULL,
	[IsActive] [BIT] NULL,
	[Reason] [VARCHAR](MAX) NULL,
	[Source] [VARCHAR](200) NOT NULL,
	[ChangedDatetime] [DATETIME] NOT NULL,
	[ChangedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_ProductHistory] PRIMARY KEY CLUSTERED 
(
	[ProductHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[GlideBoxProductHistory]  WITH CHECK ADD  CONSTRAINT [FK_GlideBoxProductHistory_GlideBoxProduct] FOREIGN KEY([ProductID])
REFERENCES [dbo].[GlideBoxProduct] ([ProductID])
GO

ALTER TABLE [dbo].[GlideBoxProductHistory] CHECK CONSTRAINT [FK_GlideBoxProductHistory_GlideBoxProduct]
GO