﻿CREATE TABLE [dbo].[TerminalCategory](
	[TerminalCategoryID] [INT] IDENTITY(1,1) NOT NULL,
	[CategoryID] [INT] NOT NULL,
	[TerminalID] [VARCHAR](10) NOT NULL,
	[OrderCategory] [INT] NOT NULL,
	[IsActive] [BIT] NOT NULL,
	[AssignedDatetime] [DATETIME] NOT NULL,
	[AssignedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_TerminalCategory] PRIMARY KEY CLUSTERED 
(
	[TerminalCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TerminalCategory] ADD  CONSTRAINT [DF_TerminalCategory_OrderCategory]  DEFAULT ((0)) FOR [OrderCategory]
GO

ALTER TABLE [dbo].[TerminalCategory] ADD  CONSTRAINT [DF_TerminalCategory_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[TerminalCategory]  WITH CHECK ADD  CONSTRAINT [FK_TerminalCategory_GlideBoxCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[GlideBoxCategory] ([CategoryID])
GO

ALTER TABLE [dbo].[TerminalCategory] CHECK CONSTRAINT [FK_TerminalCategory_GlideBoxCategory]
GO