﻿--2020/02/13-----------PHW-2364
ALTER TABLE Receipt ADD PrintReceiptMode INT DEFAULT(0) NOT NULL
GO

--2020/02/14-----------PHW-2369 & PHW-2367
ALTER TABLE Receipt ADD EnablePrintCustomerReceiptFirst BIT DEFAULT(0) NOT NULL
GO
ALTER TABLE Receipt ADD EnablePrintSettlement BIT DEFAULT(0) NOT NULL