﻿CREATE TABLE [dbo].[ResponseCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ResponseCode] ON 
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (1, N'00', N'Approved')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (2, N'25', N'Unable to locate record on file')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (3, N'20', N'Invalid Response')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (4, N'95', N'Reconcile Error')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (5, N'96', N'System Error')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (6, N'N0', N'Invalid Terminal ID')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (7, N'N1', N'Invalid User ID')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (8, N'N2', N'Invalid Business No')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (9, N'N3', N'Invalid Taxi ID')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (10, N'N4', N'Invalid Network ID')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (11, N'N5', N'Invalid SerialNo or Model')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (12, N'N6', N'Invalid Batch Number')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (13, N'N7', N'Unbalanced Reconcile')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (14, N'N8', N'Transaction Exists')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (15, N'N9', N'Logon Batch Exists')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (16, N'NA', N'Unclosed Batch')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (17, N'NB', N'InvalidMessageID')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (18, N'NC', N'FormatError')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (19, N'ND', N'Old Terminal Not Found')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (20, N'NE', N'New Terminal NotFound')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (21, N'NF', N'Terminal Not Allocated')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (22, N'NG', N'Terminal Not Reachable')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (23, N'NH', N'Invalid API Token or Password')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (24, N'NI', N'TransactionNotExists')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (25, N'NJ', N'Poll again to get a response')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (26, N'NK', N'RewardPointsRejected')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (27, N'NL', N'Transaction Declined by Provider')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (28, N'NM', N'Payment Provider Not Available')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (29, N'NN', N'Bad Order Status')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (30, N'NO', N'Reward Provider No Available')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (31, N'NP', N'Invalid Request')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (32, N'NQ', N'Invalid Zip Store Code')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (33, N'NR', N'Invalid Merchant')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (34, N'NS', N'Duplicate Request')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (35, N'NT', N'Request Expired')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (36, N'NU', N'Request Cancelled')
GO
INSERT [dbo].[ResponseCode] ([Id], [Code], [Description]) VALUES (37, N'NV', N'Request Declined')
GO
SET IDENTITY_INSERT [dbo].[ResponseCode] OFF
GO