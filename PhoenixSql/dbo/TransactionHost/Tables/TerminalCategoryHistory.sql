﻿CREATE TABLE [dbo].[TerminalCategoryHistory](
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[CategoryID] [INT] NOT NULL,
	[TerminalID] [VARCHAR](10) NOT NULL,
	[OldOrder] [INT] NULL,
	[NewOrder] [INT] NULL,
	[IsActive] [BIT] NULL,
	[IsDelete] [BIT] NULL,
	[Source] [VARCHAR](200) NOT NULL,
	[ChangedDatetime] [DATETIME] NOT NULL,
	[ChangedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_TerminalCategoryHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO