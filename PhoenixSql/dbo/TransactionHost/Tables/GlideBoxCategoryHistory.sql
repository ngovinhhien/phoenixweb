﻿CREATE TABLE [dbo].[GlideBoxCategoryHistory](
	[CategoryHistoryID] [INT] IDENTITY(1,1) NOT NULL,
	[CategoryID] [INT] NOT NULL,
	[OldCategoryName] [VARCHAR](100) NULL,
	[NewCategoryName] [VARCHAR](100) NULL,
	[OldShortName] [VARCHAR](50) NULL,
	[NewShortName] [VARCHAR](50) NULL,
	[OldPrice] [DECIMAL](10, 2) NULL,
	[NewPrice] [DECIMAL](10, 2) NULL,
	[OldCommissionAmount] [DECIMAL](10, 2) NULL,
	[NewCommissionAmount] [DECIMAL](10, 2) NULL,
	[IsActive] [BIT] NULL,
	[Reason] [VARCHAR](MAX) NULL,
	[Source] [VARCHAR](200) NOT NULL,
	[ChangedDatetime] [DATETIME] NOT NULL,
	[ChangedBy] [VARCHAR](50) NOT NULL,
 CONSTRAINT [PK_GlideBoxCategoryHistory] PRIMARY KEY CLUSTERED 
(
	[CategoryHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[GlideBoxCategoryHistory]  WITH CHECK ADD  CONSTRAINT [FK_GlideBoxCategoryHistory_GlideBoxCategory] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[GlideBoxCategory] ([CategoryID])
GO

ALTER TABLE [dbo].[GlideBoxCategoryHistory] CHECK CONSTRAINT [FK_GlideBoxCategoryHistory_GlideBoxCategory]
GO