﻿CREATE FUNCTION fn_PHW_Glidebox_GetCategoryNameByID
(
	@CategoryID INT
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @CategoryName VARCHAR(100)

	SELECT @CategoryName = CategoryName
	FROM dbo.GlideBoxCategory
	WHERE CategoryID = @CategoryID

	RETURN @CategoryName
END