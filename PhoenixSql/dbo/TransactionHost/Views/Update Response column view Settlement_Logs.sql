﻿ALTER VIEW [dbo].[View_SettlementLog]
AS
SELECT        SL.SettlementLogKey, 
SL.LogType, SL.Logon, SL.LogDateTime, SL.Batchnumber, SL.TerminalID, SL.MerchantID, SL.TerminalSerialNo, SL.TerminalModel, SL.IMEI, 
                         SL.SimSerialNo, SL.SimMobileNumber, SL.PTID, SL.BatteryStatus, SL.SignalStrength, SL.Initialisation, SL.TerminalOSVer, SL.VAAVer, SL.ApplicationType, 
                         SL.TermConfVer, SL.ReceiptDataVer, SL.PrinterLogoVer, SL.CardTableVer, SL.PromptTableVer, SL.NetworkTableVer, SL.LastTableUpdateDateTime, SL.UserID, 
                         SL.BusinessID, SL.ABNID, SL.NetworkID, SL.Settlement, SL.UpdateDateTime, SL.ScreenLogoVer, SL.ResponseCode, SL.SettlementHistoryKey, SL.TerminalFSVer, 
                         SL.TipsTrickesVer, SL.LocationTableVer, SL.CellTowerVer, SL.CellTowerMCC, SL.CellTowerMNC, SL.CellTowerID, SL.CellTowerLAC, 
                         CASE WHEN logtype = 1 THEN 'Intialize' ELSE CASE WHEN logtype = 2 THEN 'Login' ELSE 'EOS' END END AS Type, CONCAT(RC.Code, ' - ', RC.Description) as Response, 
                         dbo.SettlementAmount.SettlementNetAmount, dbo.SettlementAmount.CreditNumber, dbo.SettlementAmount.DebitNumber, dbo.SettlementAmount.CreditAmount, 
                         dbo.SettlementAmount.DebitAmount, dbo.SettlementAmount.DebitReversalNumber, dbo.SettlementAmount.DebitReversalAmount
FROM            dbo.SettlementLog AS SL 
				LEFT OUTER JOIN dbo.SettlementAmount ON SL.SettlementLogKey = dbo.SettlementAmount.SettlementLogKey 
				LEFT OUTER JOIN dbo.ResponseCode AS RC ON RC.Code = SL.ResponseCode
GO
