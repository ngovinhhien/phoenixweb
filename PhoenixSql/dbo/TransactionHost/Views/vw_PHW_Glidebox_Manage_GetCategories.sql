﻿CREATE VIEW [dbo].[vw_PHW_Glidebox_Manage_GetCategories]
AS
SELECT 
	c.CategoryID,
	c.CategoryName,
	c.ShortName,
	CONCAT('$', c.Price) AS Price,
	CONCAT('$', c.CommissionAmount) AS CommissionAmount,
	c.IsActive,
	FORMAT(c.CreatedDatetime, 'dd/MM/yyyy') AS CreatedDatetime,
	c.CreatedBy, 
	COUNT(p.ProductID) AS TotalProduct 
FROM dbo.GlideBoxCategory c
LEFT JOIN dbo.GlideBoxProduct p 
ON c.CategoryID = p.CategoryID AND p.IsActive = 1
GROUP BY c.CategoryID,
		c.CategoryName,
		c.ShortName,
		c.Price,
		c.CommissionAmount,
		c.IsActive,
		c.CreatedDatetime,
		c.CreatedBy
