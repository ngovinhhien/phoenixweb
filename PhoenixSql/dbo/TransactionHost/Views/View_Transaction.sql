﻿ALTER VIEW [dbo].[View_Transaction]
AS
--SELECT DISTINCT CASE WHEN TH.ResponseCode IN
--                          (SELECT     [ResponseCode]
--                            FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 1 THEN 'Approved' 
--                            WHEN TH.ResponseCode IN (SELECT     [ResponseCode] FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 2 THEN 'Void' 
--                            WHEN TH.ResponseCode IN (SELECT     [ResponseCode] FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 3 THEN 'Refund' 
--                            ELSE 'Declined' END AS Status, ISNULL(TH.ProcessTime, 0) AS responsetime, TH.TransactionHistoryKey, 
--                      TH.TransactionNumber, TH.MerchantID, TH.UserID, TH.BusinessID, TH.TerminalID, TH.ResponseCode, TH.TransPAN, TH.CardExpiry, TH.StartDateTime, TH.EndDateTime, TH.Amount1, TH.Amount2, 
--                      TH.Amount3, TH.Amount4, TH.Amount5, TH.Amount6, TH.Amount7, TH.Amount8, TH.Amount9, TH.TotalTransaction, TH.ServiceFee, TH.GSTOnServiceFee, TH.TotalSaleAmount, TH.TranAuth, 
--                      TH.SettlementHistoryKey, TH.AccountType, TH.CardName, TH.TranType, TH.Pickup, TH.Dropoff, TH.ABN, TH.ProcessingType, TH.STAN, TH.ProcessTime, 'Batch End' as BatchStatus
--FROM         dbo.TransactionHistory AS TH 
----LEFT JOIN dbo.TransactionLog AS TL ON TL.TerminalID = TH.TerminalID AND TL.TransactionNumber = TH.TransactionNumber
--union
--SELECT DISTINCT CASE WHEN TH.ResponseCode IN
--                          (SELECT     [ResponseCode]
--                            FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 1 THEN 'Approved' 
--                            WHEN TH.ResponseCode IN (SELECT     [ResponseCode] FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 2 THEN 'Void'
--                            WHEN TH.ResponseCode IN (SELECT     [ResponseCode] FROM          View_ApprovedResponseCodes) AND TH.ProcessingType = 3 THEN 'Refund'
--                             ELSE 'Declined' END AS Status, ISNULL(TH.ResponseTime, 0) AS responsetime, th.TransactionLogKey as TransactionHistoryKey, 
--                      TH.TransactionNumber, TH.MerchantID, TH.UserID, TH.BusinessID, TH.TerminalID, TH.ResponseCode, TH.TransPAN, TH.CardExpiry, TH.StartDateTime, TH.EndDateTime, TH.Amount1, TH.Amount2, 
--                      TH.Amount3, TH.Amount4, TH.Amount5, TH.Amount6, TH.Amount7, TH.Amount8, TH.Amount9, TH.TotalTransaction, TH.ServiceFee, TH.GSTOnServiceFee, TH.TotalSaleAmount, TH.TranAuth, 
--                      0 SettlementHistoryKey, TH.AccountType, TH.CardName, TH.TranType, TH.Pickup, TH.Dropoff, '' ABN, TH.ProcessingType, TH.STAN, TH.ProcessTime, 'Batch Open' as BatchStatus
--FROM         dbo.TransactionLog AS TH
--left join TransactionHistory t on t.TerminalID = th.TerminalID and t.TransactionNumber = th.TransactionNumber 
--where th.VAAResponseCode = '00' and t.TransactionHistoryKey is null


SELECT CASE
           WHEN v.ResponseCode IS NOT NULL
                AND a.ProcessingType = 1 THEN
               'Approved'
           WHEN v.ResponseCode IS NOT NULL
                AND a.ProcessingType = 2 THEN
               'Void'
           WHEN v.ResponseCode IS NOT NULL
                AND a.ProcessingType = 3 THEN
               'Refund'
           ELSE
               'Declined'
       END AS Status,
       a.*
FROM
(
    SELECT DISTINCT
           ISNULL(TH.ProcessTime, 0) AS responsetime,
           TH.TransactionHistoryKey,
           TH.TransactionNumber,
           TH.MerchantID,
           TH.UserID,
           TH.BusinessID,
           TH.TerminalID,
           TH.ResponseCode,
           TH.TransPAN,
           TH.CardExpiry,
           TH.StartDateTime,
           TH.EndDateTime,
           TH.Amount1,
           TH.Amount2,
           TH.Amount3,
           TH.Amount4,
           TH.Amount5,
           TH.Amount6,
           TH.Amount7,
           TH.Amount8,
           TH.Amount9,
           TH.TotalTransaction,
           TH.ServiceFee,
           TH.GSTOnServiceFee,
           TH.TotalSaleAmount,
           TH.TranAuth,
           TH.SettlementHistoryKey,
           TH.AccountType,
           TH.CardName,
           TH.TranType,
           TH.Pickup,
           TH.Dropoff,
           TH.ABN,
           TH.ProcessingType,
           TH.STAN,
           TH.ProcessTime,
           'Batch End' AS BatchStatus,
		   LEFT(TH.TransactionNumber,10) AS BatchNumber,
		   ISNULL(TH.GlideboxAmount, 0) AS GlideboxAmount,
		   ISNULL(gc.GlideboxCommission, 0) AS GlideboxCommission
    FROM dbo.TransactionHistory AS TH
	INNER JOIN dbo.SettlementHistory ON SettlementHistory.SettlementHistoryKey = TH.SettlementHistoryKey
	OUTER APPLY
	(
		SELECT SUM(gt.GlideboxCommission) AS GlideboxCommission 
		FROM Taxiepay.dbo.GlideboxTransaction gt
		WHERE gt.HostTransactionGuid = TH.TransactionGUID COLLATE Latin1_General_CI_AS
	) AS gc
    UNION
    SELECT DISTINCT
           ISNULL(TH.ResponseTime, 0) AS responsetime,
           TH.TransactionLogKey AS TransactionHistoryKey,
           TH.TransactionNumber,
           TH.MerchantID,
           TH.UserID,
           TH.BusinessID,
           TH.TerminalID,
           TH.ResponseCode,
           TH.Transpan,
           TH.CardExpiry,
           TH.StartDateTime,
           TH.EndDateTime,
           TH.Amount1,
           TH.Amount2,
           TH.Amount3,
           TH.Amount4,
           TH.Amount5,
           TH.Amount6,
           TH.Amount7,
           TH.Amount8,
           TH.Amount9,
           TH.TotalTransaction,
           TH.ServiceFee,
           TH.GSTOnServiceFee,
           TH.TotalSaleAmount,
           TH.TranAuth,
           0 SettlementHistoryKey,
           TH.AccountType,
           TH.CardName,
           TH.TranType,
           TH.Pickup,
           TH.DropOff,
           '' ABN,
           TH.ProcessingType,
           TH.STAN,
           TH.ProcessTime,
           'Batch Open' AS BatchStatus,
		   LEFT(TH.TransactionNumber,10) AS BatchNumber,
		   ISNULL(TH.GlideboxAmount, 0) AS GlideboxAmount,
		   0 AS GlideboxCommission
    FROM dbo.TransactionLog AS TH
	 LEFT JOIN TransactionHistory t
            ON t.TerminalID = TH.TerminalID
               AND t.TransactionNumber = TH.TransactionNumber
    WHERE TH.VAAResponseCode = '00' AND t.TransactionHistoryKey IS NULL
) a
    LEFT JOIN View_ApprovedResponseCodes v
        ON v.ResponseCode = a.ResponseCode;
	

GO