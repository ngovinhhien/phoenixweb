﻿CREATE VIEW [vw_PHW_Glidebox_Manage_GetProducts]
AS
SELECT 
	p.ProductID,
	c.CategoryID,
	p.ProductName,
	CONCAT('$', p.Price) AS Price,
	p.Quantity,
	--p.ImageContent,
	c.CategoryName,
	p.CreatedBy,
	FORMAT(p.CreatedDatetime, 'dd/MM/yyyy') AS CreatedDatetime,
	p.IsActive
FROM dbo.GlideBoxProduct p
INNER JOIN dbo.GlideBoxCategory c
ON p.CategoryID = c.CategoryID