﻿CREATE PROC SearchLog
(
	@Id INT,
	@Date DATETIME,
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@Thread VARCHAR(255),
	@Level VARCHAR(50),
	@Logger VARCHAR(255),
	@Message VARCHAR(4000),
	@Exception VARCHAR(2000),
	@TerminalID VARCHAR(20),
	@RowIDFrom INT = 1,
	@RowIDTo INT = 20,
	@TotalItems INT OUTPUT
)
AS
BEGIN
	
	DECLARE @QueryFrom NVARCHAR(MAX) = 'FROM dbo.[Log] WITH(NOLOCK)',
			@QueryWhere NVARCHAR(MAX) = 'WHERE 1=1'

	IF(ISNULL(@Id,0) > 0)
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Id = @Id'		
	ENd

	IF(ISNULL(@Date,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Date = @Date'
	END
	ELSE
	BEGIN
		IF(ISNULL(@DateFrom,'') <> '')
		BEGIN
			SET @QueryWhere += CHAR(10) + 'AND Date >= @DateFrom'
		END
		IF(ISNULL(@DateTo,'') <> '')
		BEGIN
			SET @DateTo = DATEADD(D, 1, CAST(@DateTo AS DATE))
			SET @QueryWhere += CHAR(10) + 'AND Date < @DateTo'
		END
	END

	IF(ISNULL(@Thread,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Thread = @Thread'
	END

	IF(ISNULL(@Level, '') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Level = @Level'
	END

	IF(ISNULL(@Logger, '') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Logger = @Logger'
	END
	
	IF(ISNULL(@Message, '') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Message LIKE ''%'' + @Message + ''%'''
	END

	IF(ISNULL(@Exception, '') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Exception LIKE ''%'' + @Exception + ''%'''
	END

	IF(ISNULL(@TerminalID,'') <> '')
	BEGIN
		SET @QueryWhere += CHAR(10) + 'AND Message LIKE ''%"TerminalID":"'' + @TerminalID + ''"%'''
	END

	DECLARE @Query NVARCHAR(MAX)
	
	SET @Query = ';WITH tmp AS ('
					+ CHAR(10) + 'SELECT ROW_NUMBER() OVER(ORDER BY [Date] DESC) AS RowId, Id, [Date], [Thread], [Level], [Logger], [Message], [Exception] ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere + ')'
					+ CHAR(10) + 'SELECT * FROM tmp'
					+ CHAR(10) + 'WHERE RowID BETWEEN @RowIDFrom AND @RowIDTo'
					+ CHAR(10) + 'SELECT @TotalItems = COUNT(*) ' 
					+ CHAR(10) + @QueryFrom 
					+ CHAR(10) + @QueryWhere

	DECLARE @Params NVARCHAR(MAX) = N'@Id INT,
	@Date DATETIME,
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@Thread VARCHAR(255),
	@Level VARCHAR(50),
	@Logger VARCHAR(255),
	@Message VARCHAR(4000),
	@Exception VARCHAR(2000),
	@TerminalID VARCHAR(20),
	@RowIDFrom INT,
	@RowIDTo INT,
	@TotalItems INT OUT'
	
	EXEC SP_EXECUTESQL @Query, @Params
	, @Id = @Id
	, @Date = @Date
	, @DateFrom = @DateFrom
	, @DateTo = @DateTo
	, @Thread = @Thread
	, @Level = @Level
	, @Logger = @Logger
	, @Message = @Message
	, @Exception = @Exception
	, @TerminalID = @TerminalID
	, @RowIDFrom = @RowIDFrom
	, @RowIDTo = @RowIDTo
	, @TotalItems = @TotalItems OUT

END
