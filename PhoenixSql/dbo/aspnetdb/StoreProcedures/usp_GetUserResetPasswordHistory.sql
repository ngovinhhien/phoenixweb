﻿CREATE PROCEDURE [dbo].[usp_GetUserResetPasswordHistory]
@FromDate DATETIME = NULL,
@ToDate DATETIME = NULL,
@SearchText NVARCHAR(1000) = NULL
AS
BEGIN
	IF @FromDate IS NULL
	BEGIN
		SET @FromDate = '1970-01-01'	
	END

	IF @ToDate IS NULL
	BEGIN
		SET @ToDate = GETDATE()
	END

	IF @SearchText IS NULL
	BEGIN
		SET @SearchText = ''
	END

	SELECT
		h.UserName,
		m.Email,
		h.CreatedDate,
		h.[Source],
		h.CreatedByUser
	FROM dbo.UserResetPasswordHistory h
	INNER JOIN dbo.aspnet_Users u ON h.UserName = u.UserName
	INNER JOIN dbo.aspnet_Membership m ON u.UserId = m.UserId
	WHERE 
	(
		(
			h.UserName LIKE '%' + @SearchText + '%'
		)
		OR 
		(
			h.CreatedByUser LIKE '%' + @SearchText + '%'
		)
	)
	AND
	(
		h.CreatedDate BETWEEN @FromDate AND DATEADD(DAY, 1, @ToDate)
	)
	ORDER BY h.CreatedDate DESC
END