﻿IF(NOT EXISTS (SELECT * FROM aspnetdb.dbo.Permission WHERE PermissionName = 'ManageMerchant'))
BEGIN
	DECLARE @Permissions TABLE (ID INT)
	INSERT INTO aspnetdb.dbo.Permission (PermissionName, PermissionDescription) 
	OUTPUT inserted.PermissionId INTO @Permissions(ID)
	VALUES('ManageMerchant', 'Manage Merchant')
	
	INSERT INTO aspnetdb.dbo.RolePermission (RoleName, PermissionId, RoleId)
	SELECT 'Administrator', Id, '6402FF6E-9CA7-479C-8778-09AD5DCF6A02'
	FROM @Permissions
END