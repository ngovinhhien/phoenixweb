﻿IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'ViewFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('ViewFees', 'Allow user can view all fees and history of all fees');
END;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'EditFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('EditFees', 'Allow user can edit/add all fees');
END;

IF NOT EXISTS
(
    SELECT TOP 1
           1
    FROM Permission
    WHERE PermissionName = 'RefundFees'
)
BEGIN
    INSERT INTO dbo.Permission
    (
        PermissionName,
        PermissionDescription
    )
    VALUES
    ('RefundFees', 'Allow user can refund fees');
END;

DECLARE @Id INT;
SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'ViewFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ( 'Administrator', 'Finance Admin', 'Operations' );


SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'EditFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ( 'Administrator', 'Finance Admin' );


SELECT @Id = PermissionId
FROM dbo.Permission
WHERE PermissionName = 'RefundFees';

INSERT INTO RolePermission
(
    RoleName,
    PermissionId,
    RoleId
)
SELECT r.RoleName,
       @Id,
       r.RoleId
FROM dbo.aspnet_Roles r
    LEFT JOIN dbo.RolePermission rp
        ON rp.RoleId = r.RoleId
           AND rp.PermissionId = @Id
WHERE rp.Id IS NULL
      AND r.RoleName IN ('Finance Admin' );

