﻿IF NOT EXISTS(SELECT TOP(1)1 FROM dbo.Permission WHERE PermissionName = 'ManageGlideboxSalesTerminalConfiguration')
BEGIN
	INSERT INTO dbo.Permission
	(
		PermissionName,
		PermissionDescription
	)
	VALUES
	(   
		'ManageGlideboxSalesTerminalConfiguration', 
		'Allow user can enable or disable glidebox sales on terminal config'  
    )
END

IF EXISTS(SELECT TOP(1) 1 FROM dbo.aspnet_Roles WHERE RoleName = 'Administrator')
BEGIN
	DECLARE @Id INT 
	SELECT @Id = PermissionId FROM dbo.Permission WHERE PermissionName = 'ManageGlideboxSalesTerminalConfiguration'

	INSERT INTO dbo.RolePermission(RoleName, PermissionId, RoleId)
	SELECT r.RoleName, @Id, r.RoleId
	FROM dbo.aspnet_Roles r
	LEFT JOIN dbo.RolePermission rp ON rp.RoleId = r.RoleId  AND rp.PermissionId = @Id
	WHERE  rp.Id IS NULL AND r.RoleName = 'Administrator'
END