﻿$(function () {
    $("#grdQbrPointHistory").jqGrid({
        url: 'GetLoyaltyFileEntries',
        datatype: 'json',
        postData:
        {
             'exportFileEntry': function () { return $('#LoyaltyExportFileId').val() }
        },
        mtype: 'Get',
        colNames: ['Actions', 'Id', 'Member Id', 'Member', 'Points', 'Status', 'Loyalty Number'],
        colModel: [
            { name: 'act', index: 'act', width: 75, sortable: false }, 
            { key: true, hidden: true, name: 'LoyaltyExportFileLineItemId', index: 'LoyaltyExportFileLineItemId', editable: false },
            { key: false, hidden: true, name: 'LoyaltyUploadFileLineItem.MemberId', index: 'LoyaltyUploadFileLineItem.MemberId', editable: false },
            { key: false, name: 'LoyaltyUploadFileLineItem.Name', index: 'LoyaltyUploadFileLineItem.Name', editable: false, },
            { key: false, name: 'Points', index: 'Points', editable: false, align: "right", formatter: 'number', formatoptions: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "" } },
            { key: false, name: 'QantasExportFileEntryStatus.Name', index: 'QantasExportFileEntryStatus.Name', editable: false, },           
            { key: false, name: 'LoyaltyNumber', index: 'LoyaltyNumber', editable: false, },           
            ],
        pager: jQuery('#grdQbrPointHistoryPager'),
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        height: '100%',
        viewrecords: true,
        caption: 'Qantas Points File',
        emptyrecords: 'No records to display',
        sord: "desc",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        beforeRequest: function () {

        },
        gridComplete: function () {
            if ($("#showApproveReject").val() === "true") {                
                $('#fileAction').show();
                $('#approveRejectLineItem').show();
                $('#approveRejectFile').show()
                var ids = jQuery("#grdQbrPointHistory").getDataIDs();
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = "<input type='button' value='Approve' onclick=onApprove(" + cl + "); /> &nbsp;";
                    se = "<input type='button' value='Reject' onclick=onReject(" + cl + "); />";
                    jQuery("#grdQbrPointHistory").setRowData(ids[i], { act: be + se })
                }
            }
            $.LoadingOverlay("hide");
        }, 
        loadError: function (jqXHR, textStatus, errorThrown) {
            alert('HTTP status code: ' + jqXHR.status + '\n' +
                'textStatus: ' + textStatus + '\n' +
                'errorThrown: ' + errorThrown);
            alert('HTTP message body (jqXHR.responseText): ' + '\n' + jqXHR.responseText);
        },
        autowidth: true,
        multiselect: false,
        editurl: 'edit',
    }).navGrid('#grdQbrPointHistoryPager', { edit: false, add: false, del: false, search: true, refresh: true }    
    );
});

function onApprove(id, mk) {        
    updateLoyaltyFileEntryStatus('approve', id, false, jQuery("#grdQbrPointHistory").getRowData(id)["LoyaltyUploadFileLineItem.MemberId"]);
}

function onReject(id, mk) {
    updateLoyaltyFileEntryStatus('reject', id, false, jQuery("#grdQbrPointHistory").getRowData(id)["LoyaltyUploadFileLineItem.MemberId"]);
}

function reloadGrid() {   
    $.LoadingOverlay("show", {
        image: "",
        text: "load'..."
    });
    $("#grdQbrPointHistory").jqGrid("setGridParam", { datatype: "json" })
        .trigger("reloadGrid", [{ current: true }]);

    $.LoadingOverlay("hide", {
        image: "",
        text: "load'..."
    });

}

function updateLoyaltyFileEntryStatus(status, id, udpateAll, mk) {
    
    $.ajax({
        type: "POST",
        url: "UpdateLoyaltyFileEntryStatus",
        dataType: "json",
        data: JSON.stringify({ qbrFileEntryKey: id, updateStatusWith: status, updateAll: udpateAll, memberKey: mk, loyaltyExportFileId: $('#LoyaltyExportFileId').val()  }),
        contentType: "application/json",
        beforeSend: function () {
            $('#messageQBR').html('please wait...');
        },
        success: function (data) {
            $('#messageQBR').html(data);
            reloadGrid();
        },
        error: function (jqXHR, textStatus, errorThrown, data) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status === 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status === 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#messageQBR').html("An error occured while connecting to server - " + jqXHR.message + "\nDetails: " + msg);           
        }
    });
}

function updateLoyaltyFileStatus(status, loyaltyExportFileId) {    
    $.ajax({
        async: false,
        type: "POST",
        url: "UpdateLoyaltyFileStatus",
        data: JSON.stringify({ 'updateStatusWith': status, 'loyaltyExportFileId': loyaltyExportFileId }),  
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
            $('#messageQBR').html('please wait...');
        },
        success: function (data) {
            $('#messageQBR').html(data);            
        },
        error: function (jqXHR, textStatus, errorThrown, data) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status === 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status === 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#messageQBR').html("An error occured while connecting to server - " + jqXHR.message + "\nDetails: " + msg);
        }
    });
}