﻿$.validator.addMethod("requiredif", function (value, element, params) {
    var currentEl = $(this.currentElements[0]);
    var otherPropertyValue = '';
    if (params != undefined) {
        otherPropertyValue = $(params).val();
    }
    var otherInput = $(params);
    if ((value.length > 0 && otherPropertyValue.length == 0) || otherInput.hasClass('error-required-if')) {
        currentEl.removeClass('error-required-if');
        $(params).valid();
    }
    if (value.length == 0 && otherPropertyValue.length > 0) {
        currentEl.addClass('error-required-if')
        return false;
    } else {
        return true;
    }
});
$.validator.unobtrusive.adapters.add("requiredif", ["propertyto"], function (options) {
	options.rules["requiredif"] = "#" + options.params.propertyto;
	options.messages["requiredif"] = options.message;
});