﻿var urlOrigin = Util.PH_UpdateURL();
var isDisplayUIControlsGlidebox = $('#display-ui-controls-glidebox').val();

$(document).ready(function () {
    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });

    $("#StartDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#EndDate").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#EndDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#StartDate").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $(window).bind('resize', function () {
        $("#transactionLogGrid").setGridWidth($("#wrapper").width());
    }).trigger('resize');

    initTransactionLogGrid();
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var enddate = day + "-" + (month) + "-" + now.getFullYear();
    $('#EndDate').datepicker("setDate", enddate);

    var sixMonthsFromNow = new Date(now.setMonth(now.getMonth() - 6));
    day = ("0" + sixMonthsFromNow.getDate()).slice(-2);
    month = ("0" + (sixMonthsFromNow.getMonth() + 1)).slice(-2);
    var startdate = day + "-" + (month) + "-" + sixMonthsFromNow.getFullYear();
    $('#StartDate').datepicker("setDate", startdate);



    $('#btnSearch').click(function (e) {
        var urlParams = new URLSearchParams(window.location.search);
        var key = urlParams.get('key');

        var isValidStartDateEndDate = CheckStartDateFollowEndDate();
        if (!isValidStartDateEndDate) {
            e.preventDefault();
            return;
        }

        var isValidAmount = CheckAmount();
        if (!isValidAmount) {
            e.preventDefault();
            return;
        }



        var startDate = $("#StartDate").datepicker('getDate');
        var endDate = $("#EndDate").datepicker('getDate');
        var isApproved = $('#uniform-Approved span').attr('class') == 'checked' ? true : false;
        var isDeclined = $('#uniform-Declined span').attr('class') == 'checked' ? true : false;
        var onlyDisplayGlideboxTransaction = $('#uniform-GlideboxTransaction span').attr('class') == 'checked' ? true : false;

        var searchParams = {
            PageIndex: 1,
            PageSize: parseInt($('[id^=transactionLogPager] select.ui-pg-selbox').val()),
            Request: {
                StartDate: startDate,
                EndDate: endDate,
                TerminalKey: key,
                CardType: $('#CardType').val(),
                BatchNumber: $('#BatchNumber').val(),
                MinAmount: $('#MinAmount').val(),
                MaxAmount: $('#MaxAmount').val(),
                ApproveStatusBool: isApproved,
                DeclineStatusBool: isDeclined,
                OnlyDisplayGlideboxTransaction: onlyDisplayGlideboxTransaction
            }
        };
        loadDataTransactionLogGrid(searchParams);
        gridTransactionLogEvents(searchParams);
    });

    $('#btnSearch').trigger('click');
});

function CheckStartDateFollowEndDate() {
    var endDate = $("#EndDate").datepicker('getDate');
    var startDate = $("#StartDate").datepicker('getDate');
    if (startDate > endDate) {
        $("#error").text("The Start Date must be smaller than End Date.");
        $("#StartDate").focus();
        return false;
    }
    $("#error").text("");
    return true;
}

function CheckAmount() {
    var min = parseFloat($("#MinAmount").val());
    var max = parseFloat($("#MaxAmount").val());

    if (min != "" && max != "") {
        if (min > max) {
            $("#error").text("The amount range invalid");
            $("#MinAmount").focus();
            return false;
        }
    }
    
    if (min < 0 || max < 0) {
        return false;
    }
    $("#error").text("");
    return true;
}

function customDisplayCommission (cellvalue, options, rowObject) {
    if (rowObject.BatchStatus == "Batch Open") {
        return "";
    }
    return cellvalue;
}

function BuildColName() {
    var colNames = [
        "Transaction Number",
        "Process Time",
        "Sale Amount",
        "Glide Box Amount",
        "Glide Box Commission",
        "Date",
        "Card Name",
        "Status",
        "Response Code",
        "Card No.",
        "Batch Status"
    ];

    if (isDisplayUIControlsGlidebox == "false") {
        colNames.splice(3, 2);
    }
   
    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "TransactionNumber", index: "TransactionNumber", align: 'center', editable: false, sortable: false },
        { key: false, name: "ProcessTime", index: "ProcessTime", width: "90", align: 'center', editable: false, sortable: false },
        { key: false, name: "TotalSaleAmount", index: "TotalSaleAmount", width: "80", align: 'center', editable: false, sortable: false },
        { key: false, name: "GlideboxAmount", index: "GlideboxAmount", width: "80", align: 'center', editable: false, sortable: false },
        { key: false, name: "GlideboxCommission", index: "GlideboxCommission", width: "90", align: 'center', editable: false, sortable: false, formatter: customDisplayCommission },
        {
            key: false,
            name: "StartDateTime",
            index: "StartDateTime",
            align: 'center',
            editable: false,
            sortable: false,
            width: "100",
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y h:i:s A"
            }
        },
        { key: false, name: "CardName", index: "CardName", align: 'left', width: "90", editable: false, sortable: false },
        { key: false, name: "Status", index: "Status", width: "90", align: 'left', editable: false, sortable: false },
        { key: false, name: "ResponseCode", width: "90", index: "ResponseCode", align: 'center', editable: false, sortable: false },
        { key: false, name: "TransPAN", index: "TransPAN", align: 'center', editable: false, sortable: false },
        { key: false, name: "BatchStatus", index: "BatchStatus", align: 'left', editable: false, sortable: false },
    ];

    if (isDisplayUIControlsGlidebox == "false") {
        colModel.splice(3, 2);
    }

    return colModel;
}

function initTransactionLogGrid() {
    $("#transactionLogGrid").jqGrid({
        url: "",
        colNames: BuildColName(),
        colModel: BuildColModel(),
        pager: "#transactionLogPager",
        rowNum: 20,
        loadui: "disable",
        rowList: [20, 50, 100, 200],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        height: 500,
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });
}


function loadDataTransactionLogGrid(searchParams) {
    LG_ShowProgressMask();

    $.ajax({
        url: urlOrigin + "Terminal/GetTransactionLogs",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#transactionLogGrid')[0];
        $('#transactionLogGrid').jqGrid('clearGridData');
        grid.addJSONData(result);
        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function gridTransactionLogEvents(searchParams) {
    $('#transactionLogGrid').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#transactionLogGrid').getGridParam('page');
            var rowNum = $('#transactionLogGrid').getGridParam('rowNum');

            if (pgButton == "next_transactionLogPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_transactionLogPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_transactionLogPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_transactionLogPager") {
                if ($('#transactionLogGrid').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#transactionLogGrid').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#transactionLogGrid').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='transactionLogPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadDataTransactionLogGrid(searchParams);
        }
    });
}
