﻿$(document).ready(function () {
    var urlOrigin = Util.PH_UpdateURL();

    ReloadPage = function() {
        window.location.href = urlOrigin + "Terminal/Config?key=" + $("#TerminalKey").val();
    };

    //set max value of Refund Limit Per Transaction base on Refund Limit Per Batch/Day 
    onRefundLimitPerDayChanges = function(elRefundDay, elRefundTransactionName) {
        var maxValue = $(elRefundDay).val() || 0;
        $("#" + elRefundTransactionName).attr("max", maxValue);
        $("#" + elRefundTransactionName).trigger("focusout");
    };

    $("#chkEnableGlideBox").on("click",
        function () {
            onEnableDisableGlideBox();
        });

    onEnableDisableGlideBox = function () {
        var originalGlideBox = Util.ConvertStringToBool($("#EnableGlidebox").val());
        var isEnableGlidebox = $("#chkEnableGlideBox").is(":checked");

        if (!isEnableGlidebox && originalGlideBox) {
            Util.ConfirmWithReasonDialog("Please provide a reason for disabling the Glide box").done(function (reason) {
                $("#ReasonDisableGlidebox").val(reason);
            }).fail(function() {
                $("#chkEnableGlideBox").attr("checked", true); //set value of checkbox
                $("#uniform-chkEnableGlideBox span").addClass("checked");// display checkbox checked in UI
            });
        }
    };
});