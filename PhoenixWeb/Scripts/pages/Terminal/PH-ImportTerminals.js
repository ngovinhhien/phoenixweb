﻿$(function () {

    var urlOrigin = Util.PH_UpdateURL();
    var fileData = [];

    $(window).bind("resize", function () {
        $("#grdTerminalGrid").setGridWidth($("#ImportTerminals").width() - 10);
    }).trigger("resize");

    $("#grdTerminalGrid").jqGrid({
        url: "",
        colNames: [
             "Terminal ID", "Terminal Type", "Status", "Location", "Created By"
        ],
        colModel: [
            { key: false, name: "TerminalID", index: "TerminalID", editable: false, sortable: false},
            {
                key: false,
                name: "TerminalType",
                index: "TerminalType",
                editable: false,
                sortable: false
            },
            { key: false, name: "Status", index: "Status", editable: false, sortable: false },
            {
                key: false,
                name: "Location",
                index: "Location",
                editable: false,
                sortable: false
            },
            { key: false, name: "CreatedByUser", index: "CreatedByUser", editable: false, sortable: false }
        ],
        pager: "#grdTerminalGridPager",
        loadui: "disable",
        datatype: 'json',
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            repeatitems: false
        },
        autowidth: true,
        loadonce: true,
    });

    $("#btnLoadFile").on("click", function () {
        var btnChooseFile = $("#btnChooseFile")[0];
        var file = btnChooseFile.files[0];
        if (file) {
            var fileExtension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //validate excel file 
            if ("csv".indexOf(fileExtension) < 0) {
                //Show error massage
                alert('Please select excel file with "csv" format to upload first.');

                //empty grid data
                $('#grdTerminalGrid').jqGrid('clearGridData');
                $("#btnSave").attr("disabled", true);
                return false;
            }

            //file data 
            var formData = new FormData();
            formData.append("File", file);
            LG_ShowProgressMask();

            $.ajax({
                url: urlOrigin + "Terminal/UploadTerminal",
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.IsError) {
                        alert(result.ErrorMessage);
                        $('#grdTerminalGrid').jqGrid('clearGridData');
                        $("#btnSave").attr("disabled", true);
                    } else {
                        //set data display on grid
                        var grid = $('#grdTerminalGrid')[0];
                        $('#grdTerminalGrid').jqGrid('clearGridData');
                        $("#grdTerminalGrid").setGridParam({ datatype: 'json' }).trigger('reloadGrid');//set grid type to json before add data
                        var data = {
                            rows: result.Data
                        }
                        grid.addJSONData(data);
                        $("#grdTerminalGrid").setGridParam({ datatype: 'local' }).trigger('reloadGrid'); //set grid type to local offline

                        fileData = result.Data;
                        $("#btnSave").attr("disabled", false);
                    }

                    LG_HideProgressMask();
                }
            });
        } else {
            alert("Please select a terminal file to upload.");
        }
    });


    $("#btnSave").off("click").on("click",
        function () {
            $("#btnSave").attr("disabled", true);
            var model = {
                BusinessTypeID: $("#BusinessTypeId").val(),
                TerminalList: JSON.stringify(fileData)
            };
            LG_ShowProgressMask();
            $.ajax({
                url: urlOrigin + "Terminal/ImportTerminalToDatabase",
                type: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(model),
                success: function (result) {
                    if (result.IsError) {
                        alert(result.ErrorMessage);
                        $("#btnSave").attr("disabled", false);
                    } else {
                        $('#grdTerminalGrid').jqGrid('clearGridData');
                        alert("All terminals has been imported.");
                        $("#btnChooseFile").val("");
                        $("#btnSave").attr("disabled", true);
                    }
                    LG_HideProgressMask();
                }

            });

        });
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
})


