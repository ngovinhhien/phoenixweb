﻿$(function () {
    var baseUrl = Util.PH_UpdateURL();

    OnSetEOS = function (terminalKey) {
        var isSetEOS = $("#SetEOS" + terminalKey).data("set");;
        var terminalId = $("#SetEOS" + terminalKey).data("id");
        var inactiveTerminal = $("#SetEOS" + terminalKey).attr("data-inactive");
        if (Util.ConvertStringToBool(inactiveTerminal) == true) {
            $("#message").text("Cannot set EOS for inactive terminal");
            return;
        }

        var hasReceipt = $("#SetEOS" + terminalKey).data("has-receipt");
        if (Util.ConvertStringToBool(hasReceipt) == false) {
            $("#message").text("Cannot set EOS for terminal doesn't has Receipt");
            return;
        }

        $.ajax({
            url: baseUrl + "Terminal/SetEOS",
            data: { terminalId: terminalId, isSetEOS: isSetEOS },
            success: function (result) {
                if (result.IsError) {
                    $("#message").text(result.ErrorMessage);
                } else {
                    var label = result.IsSetEOS ? "Unset EOS" : "Set EOS";
                    $("#SetEOS" + terminalKey).text(label);
                    $("#SetEOS" + terminalKey).data("set", result.IsSetEOS);
                    $("#message").text("");
                }
            }
        });
    }

    GeneratePassword = function (terminalKey) {
        var terminalId = $("#GP" + terminalKey).data("id");
        $.ajax({
            url: baseUrl + "Terminal/GeneratePassword",
            data: { terminalId: terminalId },
            success: function (result) {
                if (result.IsError == false) {
                    Util.MessageDialog(result.Message, "Success");
                }
                else {
                    Util.MessageDialog(result.ErrorMessage, "Error");
                }
            }
        });
    }
})


