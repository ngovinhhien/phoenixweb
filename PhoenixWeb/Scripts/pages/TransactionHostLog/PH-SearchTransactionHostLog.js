//Get URL Origin
var urlOrigin = Util.PH_UpdateURL();

$(function () {

    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd',
        yearRange: "1900:" + (new Date().getFullYear())
    });

    loadLoggerTypes();

    LoggerTypeOnChange();

    initGrid();

    $('#btnSearch').click(function () {

        if (!$('#frmSearchLog').valid()) {
            return;
        }

        var searchParams = {
            PageIndex: 1,
            PageSize: $('[id^=grdTransactionHostLogPager] select.ui-pg-selbox').val(),
            Request: {
                LoggedDateFrom: $('#LoggedDateFrom').val(),
                LoggedDateTo: $('#LoggedDateTo').val(),
                Message: $('#Message').val(),
                Level: $('#LevelType').val(),
                Logger: $('#LoggerType').val(),
                TerminalID: $('#TerminalID').val()
            }
        };
        loadGridData(searchParams);
        gridEvents(searchParams);
    });

    $(window).bind("resize", function () {
        $('#grdTransactionHostLog').setGridWidth($('#parentTransactionHostLogGrid').width());
        setSizePopupContain();
    }).trigger("resize");

    //validate From Date must <= To Date
    $("#LoggedDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#LoggedDateTo").datepicker('getDate')
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    //validate From Date must >= To Date
    $("#LoggedDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#LoggedDateFrom").datepicker('getDate')
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

});

function initGrid() {

    $('#grdTransactionHostLog').jqGrid({
        url: "",
        colNames: [
            "Date", "Level", "Logger", "Message", ""
        ],
        colModel: [
            { key: false, name: "Date", index: "Date", editable: false, sortable: false, width: 20, formatter: formatDate },
            { key: false, name: "Level", index: "Level", editable: false, sortable: false, width: 20 },
            { key: false, name: "Logger", index: "Logger", editable: false, sortable: false, width: 20, formatter: getLoggerName },
            { key: false, name: "Message", index: "Message", editable: false, sortable: false, formatter: displayShortMessage },
            { key: false, name: "", index: "", editable: false, sortable: false, width: 15, formatter: addLinkViewDetail },
        ],
        pager: '#grdTransactionHostLogPager',
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    function formatDate(cellValue, option, rowObject) {
        try {            
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
    function addLinkViewDetail(cellValue, option, rowObject) {
        var tag = $('<a>View Detail</a>');
        tag.attr({
            'data-message': rowObject.Message,
            class: 'view-detail-log',
            'onclick': 'viewDetailLog(this)',
            'rowid': option.rowId
        });
        return tag[0].outerHTML;
    }
    function getLoggerName(cellValue, option, rowObject) {
        return $('#LoggerType option[value=' + cellValue + ']').text();
    }
    function displayShortMessage(cellValue, option, rowObject) {
        return (cellValue.length > 120 ? cellValue.substring(0, 120) + '...' : cellValue).replace("\n", " ");
    }
}

function gridEvents(searchParams) {

    $('#grdTransactionHostLog').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdTransactionHostLog').getGridParam('page');
            var rowNum = $('#grdTransactionHostLog').getGridParam('rowNum');

            if (pgButton == "next_grdTransactionHostLogPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdTransactionHostLogPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdTransactionHostLogPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdTransactionHostLogPager") {
                currentPage = $('#grdTransactionHostLog').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdTransactionHostLogPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page.

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridData(searchParams);
        }
    });
}

function loadLoggerTypes() {
    $.ajax({
        url: urlOrigin + '/TransactionHostLog/GetLogger',
        type: "GET",
        dataType: "json",
        success: function (data) {
            $.each(data,
                function (i, item) {
                    var opt = $('<option></option>');
                    opt.attr('data-searchable-termninal-id', item.SearchableTerminalID ? 1 : 0);
                    opt.val(item.Key).html(item.Name);
                    $("#LoggerType").append(opt);
                });
        }
    });
}

function LoggerTypeOnChange() {
    $("#LoggerType").change(function () {
        var self = $(this);
        var searchableTerminalID = self.children('option:selected').attr('data-searchable-termninal-id');
        if (searchableTerminalID == 1) {
            $('#ctrlTerminalID').show();
        } else {
            $('#ctrlTerminalID').hide();
            $('#TerminalID').val('');
        }
    });
}

function loadGridData(searchParams) {

    LG_ShowProgressMask();
    $('#grdTransactionHostLog').jqGrid('clearGridData');

    $.ajax({
        url: urlOrigin + "TransactionHostLog/GetLog",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        if (result.IsError) {
            alert(result.Message);
        } else {
            //Loading data to grid
            var grid = $('#grdTransactionHostLog')[0];
            grid.addJSONData(result);
        }
        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function viewDetailLog(selector) {
    var self = $(selector);
    var pageContent = self.closest('.page-content');
    var popupViewDetailLog = $('#popupViewDetailLog').dialog();
    popupViewDetailLog.dialog('option', { 'width': '80%', 'height': pageContent.height() * 0.8, 'resizable': false });

    var date = self.parents('tr').children('td').eq(0).text();
    var dialog = popupViewDetailLog.parent('.ui-dialog');
    dialog.find('.ui-dialog-title').html('<b>Log View Detail: ' + date + '</b>');

    setSizePopupContain();

    var content = self.attr('data-message');

    var obj = getMessageJSON(content);
    if (obj != null) {
        json = JSON.stringify(obj.json, undefined, 4);
        content = obj.data.replace('{json}', '\n' + json + '\n');
    }
    popupViewDetailLog.children('textarea').text(content);
}

function setSizePopupContain() {
    var popupViewDetailLog = $('#popupViewDetailLog');
    popupViewDetailLog.css({ 'padding-right': '0' });

    var popupW = popupViewDetailLog.width(),
        popupH = popupViewDetailLog.height();
    var textArea = popupViewDetailLog.children('textarea');
    textArea.css({ 'width': popupW - 5, 'height': popupH - 10, 'border': 'none', 'outline': 'none', 'resize': 'none' });
    textArea.attr({ 'readonly': 'readonly' });
}

function getMessageJSON(data) {
    var reg = /\{\"(.*?)\}$/g;
    var matches = reg.exec(data);
    var obj = data;
    if (matches != null && matches.length > 0) {
        obj = matches[0];
    } else {
        var index = obj.indexOf('{"');
        if (index == 0) {
            obj = obj.substring(index);
            if (obj.lastIndexOf("}") < obj.length - 1) {
                if (obj.lastIndexOf('"') < obj.length - 1) {
                    if (obj.lastIndexOf(':"') == obj.lastIndexOf(':')) {
                        obj += '"';
                    }
                }
                obj += '}';
            }
        }
        else {
            reg = /\{\"(.*?)\}+/g;
            matches = reg.exec(data)
            if (matches != null && matches.length > 0) {
                obj = matches[0];
            }
        }
    }

    try {
        var result = JSON.parse(obj);
        if (data.length > obj.length) {
            data = data.replace(obj, '{json}');
        } else {
            data = '{json}';
        }
        return { data: data, json: result };
    } catch (e) {
        return null;
    }
}