﻿(function (OxygenTransactionService, $, undefined) {
    OxygenTransactionService.GetOxygenTransaction = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'DriverCard/GetOxygenTransaction',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchParams),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    OxygenTransactionService.ApproveTimeoutTransaction = function (baseURL, oxygenTransRefNo, id ) {
        var deferred = $.Deferred();
        var params = {
            OxygenTransRefNo: oxygenTransRefNo,
            ID: id
        }
        $.ajax({
            url: baseURL + 'DriverCard/ApproveTimeoutTransaction',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(params),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    return {
        GetImportedTransaction: OxygenTransactionService.GetOxygenTransaction,
        ExportOxygenTransaction: OxygenTransactionService.ExportOxygenTransaction
    };

}(window.OxygenTransactionService = window.OxygenTransactionService || {}, jQuery));