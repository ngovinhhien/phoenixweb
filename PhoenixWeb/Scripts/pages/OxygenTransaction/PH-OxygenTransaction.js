﻿var urlOrigin = Util.PH_UpdateURL();
var sortParams = {
    sortField: '',
    sortOrder: '',
};
var searchParams = {
    Request: {}
};

$(document).ready(function () {
    Util.InitializeDialog('#oxygenTransactionApproveDialog');

    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var currentDate = day + "-" + (month) + "-" + now.getFullYear();
    $('#TransactionDateFrom').datepicker("setDate", currentDate);
    $('#TransactionDateTo').datepicker("setDate", currentDate);
    $('#CreatedDateFrom').datepicker("setDate", currentDate);
    $('#CreatedDateTo').datepicker("setDate", currentDate);

    $("#TransactionDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#TransactionDateTo").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#TransactionDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#TransactionDateFrom").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $("#CreatedDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#CreatedDateTo").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#CreatedDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#CreatedDateFrom").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $(window).bind('resize', function () {
        $("#TransactionGrid").setGridWidth($("#wrapper").width());
    }).trigger('resize');

    initTransactionGrid();
    setTimeout(function () {
        $("#TransactionGrid").setGridWidth($("#wrapper").width());
    }, 300)

    $('#btnSearch').click(function (e) {
        e.preventDefault();
        var urlParams = new URLSearchParams(window.location.search);
        var filterParams = getFilterParams("#param-area");
        filterParams.SortField = sortParams.sortField;
        filterParams.SortOrder = sortParams.sortOrder;
        if (!ValidateParams(filterParams)) {
            return;
        }
        searchParams = {
            PageIndex: 1,
            PageSize: parseInt($('[id^=TransactionGridPager] select.ui-pg-selbox').val()),
            Request: filterParams
        };
        loadDataTransactionGrid(searchParams);
        TransactionGridEvents(searchParams);
    });

    $('#btnExport').click(function (e) {
        e.preventDefault();
        var queryVariablesArray = [];
        for (var el in searchParams.Request) {
            queryVariablesArray.push(el + "=" + searchParams.Request[el]);
        }
        var queryVariables = queryVariablesArray.join("&");
        window.location.href = urlOrigin + "DriverCard/ExportOxygenTransaction?" + queryVariables;
    });

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});


function getFilterParams(area) {
    var listEl = $(area + " .search-param");
    var params = {};
    for (var elName in listEl) {
        if (!isNaN(parseInt(elName))) {
            var el = listEl[elName];
            var val = '';
            var key = '';
            if (el.className.includes('datepicker')) {
                val = el.value.split('-').reverse().join("-");
                key = el.id;
            }
            else {
                val = el.value
                key = el.id;
            }
            if (key || null && key || undefined && key || '') {
                params[key] = val.trim();
            }
        }
    }
    console.log(params);
    return params;
}

function ValidateParams(filterParams) {
    var errorList = [];
    var hasTransDate = true;
    var hasImportDate = true;
    if (filterParams.TransactionDateFrom == '' || filterParams.TransactionDateTo == '') {
        hasTransDate = false;
    }
    if (filterParams.CreatedDateFrom == '' || filterParams.CreatedDateTo == '') {
        hasCreatedDate = false;
    }
    if (!hasTransDate && !hasCreatedDate) {
        errorList.push('Cannot leave both Transaction Date and Created Date blank');
    }
    if (filterParams.AmountFrom != '' && filterParams.AmountTo != '') {
        var amountFrom = parseInt(filterParams.AmountFrom);
        var amountTo = parseInt(filterParams.AmountTo);
        if (!isNaN(amountFrom) && !isNaN(amountTo)) {
            if (amountFrom > amountTo) {
                errorList.push('Amount to must be equal or greater than amount from');
            }
        }

    }
    if (errorList.length > 0) {
        Util.MessageDialog(errorList, 'Error');
        return false;
    }
    return true;
}


function BuildColName() {
    var colNames = [
        "Payment Reference No",
        "Oxygen Ref ID",
        "MemberID",
        "Account Identifier",
        "Amount",
        "Status",
        "Transfer Type",
        "Transfer Date",
        "Created Date",
        "Action"
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "LodgementReferenceNo", index: "LodgementReferenceNo", align: 'center', editable: false, sortable: true, width: 160 },
        { key: false, name: "OxygenTransRefNo", index: "OxygenTransRefNo", align: 'center', editable: false, sortable: true, width: 180 },
        { key: false, name: "MemberID", index: "MemberID", align: 'center', editable: false, sortable: true, width: 100 },
        { key: false, name: "AccountIdentifier", index: "AccountIdentifier", align: 'center', editable: false, sortable: true, width: 160 },
        {
            key: false, name: "Amount", index: "Amount", editable: false, sortable: false, width: 90, align: "right", formatter: function (value) {
                if (value == null) {
                    return "0.00";
                }
                return value.toFixed(2);
            }
        },
        { key: false, name: "Status", index: "Status", align: 'center', editable: false, sortable: false, width: 120 },
        { key: false, name: "TransactionType", index: "TransactionType", align: 'center', editable: false, sortable: false },
        {
            key: false, name: "TransactionDate", index: "TransactionDate", align: 'center', editable: false, sortable: true, width: 130,
            formatter: "date",
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y h:i:s"
            }
        },
        {
            key: false, name: "CreatedDate", index: "CreatedDate", align: 'center', editable: false, sortable: true, width: 130,
            formatter: "date",
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y h:i:s"
            }
        },
        { key: false, name: "", index: "", width: "100", align: 'center', editable: false, sortable: false, formatter: addAction },
    ];

    return colModel;
}

function addAction(cellValue, option, rowObject) {
    if (rowObject.Status == 'Timeout') {
        var tag = $('<button>Approve</button>');
        tag.attr({
            class: 'inline-button',
            'onclick': 'OpenApproveTransactionDialog(' + rowObject.ID + ')',
            'rowid': option.rowId
        });
        return tag[0].outerHTML;
    }
    return '';
}

function initTransactionGrid() {
    $("#TransactionGrid").jqGrid({
        url: "",
        colNames: BuildColName(),
        colModel: BuildColModel(),
        pager: "#TransactionGridPager",
        rowNum: 20,
        loadui: "disable",
        rowList: [20, 50, 100, 200],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No result found",
        height: 500,
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: false,
        shrinkToFit: true,
        multiselect: false
    });
}

function loadDataTransactionGrid(params) {
    LG_ShowProgressMask();
    $('#btnExport').prop("disabled", true);
    OxygenTransactionService.GetOxygenTransaction(urlOrigin, params)
        .done(function (result) {
            if (result.IsError != true) {
                var grid = $('#TransactionGrid')[0];
                $('#TransactionGrid').jqGrid('clearGridData');
                if (result.Response.length > 0) {
                    grid.addJSONData(result);
                    $('#btnExport').prop("disabled", false);
                    $("#TransactionGrid").setGridWidth($("#wrapper").width());
                } else {
                    Util.MessageDialog('No result found', 'Information');
                }
            }
            else {
                Util.MessageDialog(result.Message, "Error");
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            LG_HideProgressMask();
        });
}

function TransactionGridEvents(searchParams) {
    $('#TransactionGrid').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#TransactionGrid').getGridParam('page');
            var rowNum = $('#TransactionGrid').getGridParam('rowNum');

            if (pgButton == "next_TransactionGridPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_TransactionGridPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_TransactionGridPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_TransactionGridPager") {
                if ($('#TransactionGrid').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#TransactionGrid').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#TransactionGrid').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='TransactionGridPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadDataTransactionGrid(searchParams);
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            if ($('#TransactionGrid').getGridParam('lastpage') == 0) {
                return;
            }

            sortParams.sortField = index;
            sortParams.sortOrder = sortOrder;
            searchParams.Request.SortField = sortParams.sortField;
            searchParams.Request.SortOrder = sortParams.sortOrder;
            loadDataTransactionGrid(searchParams);
        }
    });
}

OpenApproveTransactionDialog = function (id) {
    var popupViewDetailLog = $('#oxygenTransactionApproveDialog').dialog({
        title: "Approve Transaction",
        position: "center",
        modal: true,
        width: 500,
        height: 250
    });
    $("#oxygenTransactionApproveDialog #ID").val(id);
    $("#oxygenTransactionApproveDialog #OxygenTransRefNo").val('');
    popupViewDetailLog.dialog();
};

CloseDialog = function (dialogId) {
    $(dialogId).dialog("close");
}

Approve = function () {
    var id = $("#oxygenTransactionApproveDialog #ID").val();
    var oxygenTransRefNo = $("#oxygenTransactionApproveDialog #OxygenTransRefNo").val().trim();
    if (!oxygenTransRefNo) {
        Util.MessageDialog('Please input Oxygen Transaction Ref ID.', 'Approve Transaction');
        return;
    }
    LG_ShowProgressMask();
    OxygenTransactionService.ApproveTimeoutTransaction(urlOrigin, oxygenTransRefNo, id)
        .done(function (result) {
            LG_HideProgressMask();
            if (result.success) {
                CloseDialog('#oxygenTransactionApproveDialog');
                Util.MessageDialog("The transaction has been approved successfully.", "Approve Transaction");
                loadDataTransactionGrid(searchParams);
                TransactionGridEvents(searchParams);
            } else {
                Util.MessageDialog(result.errors, "Approve Transaction");
            }
        }
        );
}

