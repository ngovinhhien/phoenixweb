﻿var baseURL = Util.PH_UpdateURL();

$(function () {

    //init assign category popup
    var colNameArray = ["Button Position", "Category Name", "Category Code", "Price", "Product"];
    var colModelArray = [
        {
            key: false,
            name: "CategoryID",
            index: "CategoryID",
            editable: true,
            sortable: false,
            align: "center",
            formatter: CustomSelectOrderFormatter,
            title: false
        },
        {
            key: false,
            name: "CategoryName",
            index: "CategoryName",
            editable: true,
            sortable: false,
            align: "left",
            classes: 'cell-wrapper'
        },
        {
            key: false,
            name: "ShortName",
            index: "ShortName",
            editable: false,
            sortable: false,
            align: "left",
            classes: 'cell-wrapper'
        },
        {
            key: false,
            name: "Price",
            index: "Price",
            editable: false,
            sortable: false,
            align: "center"
        },
        { key: false, name: "TotalProduct", index: "TotalProduct", editable: false, sortable: false, align: "center" }
    ];


    var props = {
        colNames: colNameArray,
        colModel: colModelArray,
        rowList: [20, 50, 100, 200],
        height: '100%',
        enableAutoWidth: true,
        enableShrinkToFit: true,
        enableMultiselect: false,
        gridId: 'gridAssignCategory',
        allowSort: false,
    };

    Util.InitJqGrid(props, null, null);
    Util.JQGridAlignLeftHeader("gridAssignCategory", "CategoryName");
    Util.JQGridAlignLeftHeader("gridAssignCategory", "ShortName");
    Util.ResizeGrid("gridAssignCategory", "parentAssignCategoryGrid");

    //init category detail popup
    colModelArray = [
        {
            key: false,
            name: "OrderCategory",
            index: "OrderCategory",
            editable: false,
            sortable: false,
            align: "left"
        },
        {
            key: false,
            name: "CategoryName",
            index: "CategoryName",
            editable: true,
            sortable: false,
            classes: 'cell-wrapper',
            align: "left"
        },
        {
            key: false,
            name: "ShortName",
            index: "ShortName",
            editable: false,
            sortable: false,
            classes: 'cell-wrapper',
            align: "left"
        },
        {
            key: false,
            name: "Price",
            index: "Price",
            editable: false,
            sortable: false,
            formatter: CustomPriceFormatter,
            align: "left"
        },
        { key: false, name: "Product", index: "Product", editable: false, sortable: false, align: "left" }
    ];

    props = {
        colNames: colNameArray,
        colModel: colModelArray,
        rowList: [20, 50, 100, 200],
        height: '100%',
        enableAutoWidth: true,
        enableShrinkToFit: true,
        enableMultiselect: false,
        gridId: 'gridDetailCategory',
        allowSort: false,
    };

    Util.InitJqGrid(props, null, null);
    Util.JQGridAlignLeftHeader("gridDetailCategory", "OrderCategory");
    Util.JQGridAlignLeftHeader("gridDetailCategory", "CategoryName");
    Util.JQGridAlignLeftHeader("gridDetailCategory", "ShortName");
    Util.JQGridAlignLeftHeader("gridDetailCategory", "Price");
    Util.JQGridAlignLeftHeader("gridDetailCategory", "Product");
    Util.ResizeGrid("gridDetailCategory", "parentDetailCategoryGrid");

    //only load in bulk assign page
    if ($(".bulk-assign-category").length > 0) {
        LoadAllCategoryList("");
    }

    $("#btnSearch").on("click",
        function () {
            var searchText = $("#textSearch").val();
            var terminalId = $("#txtTerminalId").val(); //the terminalId only valid in assign individual page
            if (searchText.length > 0) {
                LoadAllCategoryList(searchText, terminalId);
            } else {
                LoadAllCategoryList("", terminalId);
            }
        });

    //handle search after user press enter
    $("#textSearch").on("keyup",
        function (event) {
            if (event.keyCode === 13) {
                // Trigger the button element with a click
                $("#btnSearch").click();
            }
        });

    $("#btnAssign").on("click",
        function () {
            var message = "Are you sure you want to assign these items?";

            Util.ConfirmDialog(message).done(function () {
                SaveBulkAssignCategory();
            });
        });

    $("#btnSave").on("click",
        function () {
            SaveIndividualAssignCategory();
        });

    $("#btnEdit").on("click",
        function () {
            var terminalId = $("#txtTerminalId").val();
            $("#detailCategoryDialog").dialog("close");// close detail category dialog before edit categories
            DisplayCategoryPopup(terminalId, true)
        });

    $("#btnCancel").on("click",
        function () {
            $("#assignCategoryDialog").dialog("close");
            var isEdit = $("#btnSave").data("is-edit");
            if (isEdit) {
                DisplayCategoryPopup($("#txtTerminalId").val());
            }
        });

    $("#btnReset").on("click",
        function () {
            var message = "Are you sure you want to reset?";

            Util.ConfirmDialog(message).done(function () {
                var terminalId = $("#txtTerminalId").val();
                ResetCategoryInTerminal(terminalId).done(function () {
                    DoAfterResetAllCategory(terminalId);
                    $("#detailCategoryDialog").dialog("close");
                });
            });
        });
});

function CustomSelectOrderFormatter(cellvalue, options, rowObject) {
    var select = "<select onchange = 'OnChangeOrder(this)' class='select-order' data-id='" + cellvalue + "' id='selectOrder_" + cellvalue + "' >";
    select += "<option value=" + 0 + " ></option>"

    //create value of dropdown list base on number of max category can assign to terminal
    var maxOrder = parseInt($("#txtMaxCategory").val());
    for (var i = 1; i <= maxOrder; i++) {
        select += "<option value=" + i + " >" + i + "</option>"
    }
    select += "</select>"

    return select;
}

function CustomPriceFormatter(cellvalue, options, rowObject) {
    var html = "";
    html += "<span>$" + cellvalue + "</span>";
    return html;
}

function OnChangeOrder(element) {
    if ($(element).val() === "0") {
        $(element).removeClass("selected");
    } else {
        $(element).addClass("selected");
    }
}

function LoadAllCategoryList(categoryName, terminalId) {
    var deferred = $.Deferred();
    //Show progress mask
    LG_ShowProgressMask();

    $.ajax({
        url: baseURL + "Category/GetActiveCategoryListToAssign",
        headers: {
            "Content-Type": "application/json"
        },
        type: "GET",
        data: { categoryName: categoryName, terminalId: terminalId }
    }).done(function (result) {
        if (result.IsError) {
            Util.MessageDialog(result.Message);
            console.log(result.InnerMessage);
            LG_HideProgressMask();
            deferred.reject();
        }

        Util.SetDataToGrid("gridAssignCategory", { rows: result.data });

        //only bind data when assign individual terminal
        if (result.assignData && result.assignData.length) {
            BindSelectedCategory(result.assignData);
        }

        LG_HideProgressMask();
        deferred.resolve();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
        deferred.reject();
    });

    return deferred.promise();
}

function BindSelectedCategory(categoryAssined) {
    categoryAssined.forEach(function (item) {
        $("#selectOrder_" + item.CategoryID).val(item.Order);
        $("#selectOrder_" + item.CategoryID).addClass("selected");
    });
}

function SaveBulkAssignCategory() {
    //get all dropdown list have order value
    var selectedCategoryEl = $(".select-order.selected");

    //check user select least one category
    if (!selectedCategoryEl.length) {
        Util.MessageDialog("Please assign at least one category to the selected Terminal/s.");
        return;
    }

    //check user select lager number of max category
    var maxCategory = $("#txtMaxCategory").val();
    if (selectedCategoryEl.length > maxCategory) {
        Util.MessageDialog("Category limit exceeded. Please select a maximum of " + maxCategory + " categories to assign to the selected Terminal/s.");
        return;
    }

    //get category ID and order number, which user selected
    var categoryOrders = [];
    $(selectedCategoryEl).each(function (i, el) {
        var categoryID = $(el).data("id");
        var order = $(el).val();
        categoryOrders.push({ CategoryID: parseInt(categoryID), Order: parseInt(order) });
    });

    //check order number duplicate here
    var orderArray = categoryOrders.map(function (category) { return category.Order; });

    var isDuplicateValue = Util.IsDuplicateInArray(orderArray);
    if (isDuplicateValue) {
        Util.MessageDialog("One or more category order numbers has been duplicated. Please select unique numbers and try again.");
        return;
    }

    var terminalIds = $("#txtTerminal").val().trim();

    if (terminalIds.charAt(0) === ";") {
        terminalIds = terminalIds.substring(1);
    }

    if (terminalIds.endsWith(';') === ";") {
        terminalIds = terminalIds.substring(0, terminalIds.length - 1);
    }

    //check user inputed terminalID list
    if (!terminalIds) {
        Util.MessageDialog("Please enter at least one valid Terminal ID.");
        return;
    }

    var bulkData = { TerminalIds: terminalIds, CategoryOrders: categoryOrders }

    LG_ShowProgressMask();

    $.ajax({
        url: baseURL + "Category/SaveAssignCategoryToTerminal",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(bulkData)
    }).done(function (result) {
        if (result.IsError) {
            Util.MessageDialog(result.Message);
            console.log(result.InnerMessage);
            LG_HideProgressMask();
            return;
        }

        ClearDataInField();
        Util.BulkMessageDialog("Successfully assigned",
            "Failed to assign",
            result.TerminalAdded,
            result.TerminalError,
            baseURL + "Category/DownloadTerminalList",
            "Done!");
        LoadAllCategoryList("");
        LG_HideProgressMask();

    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function ClearDataInField() {
    $("#textSearch").val("");
    $("#txtTerminal").val("");
};

function DisplayCategoryPopup(terminalId, isEdit) {
    var isSet = Util.ConvertStringToBool($("#btnAssign_" + terminalId).data("set"));
    if (isSet && !isEdit) { //show detail category dialog
        GetCategoriesDetail(terminalId).done(function () {
            //show dialog after get categories data
            ShowCategoryDialog(terminalId, "gridDetailCategory", "parentDetailCategoryGrid", "detailCategoryDialog", "Glide Box");
        });
    } else { //show assign category dialog
        LoadAllCategoryList("", terminalId).done(function () {
            //show dialog after get categories data
            ShowCategoryDialog(terminalId, "gridAssignCategory", "parentAssignCategoryGrid", "assignCategoryDialog", "Assign categories to: " + terminalId, isEdit);
        });
    }
}

function ShowCategoryDialog(terminalId, gridId, parentGridId, dialogId, title, isEdit) {
    $("#" + dialogId).show();
    $("#" + dialogId).dialog({
        resizable: false,
        height: "auto",
        width: "50%",
        title: title,
        drag: function (event, ui) {
            Util.HandleDragDialog(event, ui);
        },
        modal: true
    });

    if (isEdit) {
        $("#btnSave").val("Save Changes");
        $("#btnSave").data("is-edit", true);
    } else {
        $("#btnSave").val("Assign");
        $("#btnSave").data("is-edit", false);
    }
    $("#txtTerminalId").val(terminalId);//set current terminal selected
    Util.ResizeGrid(gridId, parentGridId);
}

function SaveIndividualAssignCategory() {
    //get all dropdown list have order value
    var selectedCategoryEl = $(".select-order.selected");
    var terminalId = $("#txtTerminalId").val();
    var isEdit = $("#btnSave").data("is-edit");

    //if user selected all category to empty then reset all category in terminal
    if (isEdit && !selectedCategoryEl.length) {
        ResetCategoryInTerminal(terminalId, true);
        return;
    }

    //check user select least one category
    if (!selectedCategoryEl.length) {
        Util.MessageDialog("Please assign at least one category to the selected Terminal/s.");
        return;
    }

    //check user select lager number of max category
    var maxCategory = $("#txtMaxCategory").val();
    if (selectedCategoryEl.length > maxCategory) {
        Util.MessageDialog("Category limit exceeded. Please select a maximum of " + maxCategory + " categories to assign to the selected Terminal/s.");
        return;
    }

    //get category ID and order number, which user selected
    var categoryOrders = [];
    $(selectedCategoryEl).each(function (i, el) {
        var categoryID = $(el).data("id");
        var order = $(el).val();
        categoryOrders.push({ CategoryID: parseInt(categoryID), Order: parseInt(order) });
    });

    //check order number duplicate here
    var orderArray = categoryOrders.map(function (category) { return category.Order; });

    var isDuplicateValue = Util.IsDuplicateInArray(orderArray);
    if (isDuplicateValue) {
        Util.MessageDialog("One or more category order numbers has been duplicated. Please select unique numbers and try again.");
        return;
    }

    var individualData = { TerminalId: terminalId, CategoryOrders: categoryOrders };

    LG_ShowProgressMask();

    $.ajax({
        url: baseURL + "Category/UpdateCategoryIndividualTerminal",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(individualData)
    }).done(function (result) {
        $("#assignCategoryDialog").dialog("close");

        if (result.IsError) {
            Util.MessageDialog(result.Message);
            console.log(result.InnerMessage);
            LG_HideProgressMask();
            return;
        }

        if (isEdit) {
            Util.MessageDialog("Categories assignments updated successfully.");
        } else {
            Util.MessageDialog("Categories assigned successfully.");
        }

        //update value for action link in terminal search
        var terminalId = $("#txtTerminalId").val();
        $("#btnAssign_" + terminalId).data("set", true);
        $("#btnAssign_" + terminalId).text("View Assigned Categories");
        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function GetCategoriesDetail(terminalId) {
    var deferred = $.Deferred();
    //Show progress mask
    LG_ShowProgressMask();

    $.ajax({
        url: baseURL + "Category/GetCategoryDetailInTerminal",
        headers: {
            "Content-Type": "application/json"
        },
        type: "GET",
        data: { terminalId: terminalId }
    }).done(function (result) {
        if (result.IsError) {
            LG_HideProgressMask();
            console.log(result.InnerMessage);
            Util.MessageDialog(result.Message);
            deferred.reject();
        }

        Util.SetDataToGrid("gridDetailCategory", { rows: result.data });
        LG_HideProgressMask();
        deferred.resolve();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
        deferred.reject();
    });

    return deferred.promise();
}

function ResetCategoryInTerminal(terminalId, isEdit) {
    var deferred = $.Deferred();
    //Show progress mask
    LG_ShowProgressMask();

    $.ajax({
        url: baseURL + "Category/ResetCategoryInTerminal",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify({ terminalId: terminalId })
    }).done(function (result) {
        if (result.IsError) {
            LG_HideProgressMask();
            console.log(result.InnerMessage);
            Util.MessageDialog(result.Message);
            deferred.reject();
        }

        if (isEdit) {
            Util.MessageDialog("Categories assignments updated successfully.");
            DoAfterResetAllCategory(terminalId);
            $("#assignCategoryDialog").dialog("close");
        } else {
            Util.MessageDialog("Categories assignments reset successful.");
        }
        LG_HideProgressMask();
        deferred.resolve();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
        deferred.reject();
    });

    return deferred.promise();
}

function DoAfterResetAllCategory(terminalId) {
    $("#btnAssign_" + terminalId).text("Assign Categories to Terminal");
    $("#btnAssign_" + terminalId).data("set", false);
}