﻿//@class ActivateDeactivateCategoryDialog
function ActivateDeactivateCategoryDialog() { }
var baseURL = Util.PH_UpdateURL();
var isActivateAction = false;

ActivateDeactivateCategoryDialog.SaveChanges = function (action, url, callbackMethod) {
    if ($('#activateDeactivateForm').valid()) {
        if (action === "Activate") {
            isActivateAction = true;
        }

        var data = {
            ID: $('#ID').val(),
            Reason: $('#Reason').val(),
            IsActivateAction: isActivateAction
        }

        LG_ShowProgressMask();
        manageCategoryService.SaveChanges(url, data).done(function (message) {
            $('#activateDeactivateDialog').dialog('close');

            if (typeof callbackMethod == "function") {
                callbackMethod();
            }

            isActivateAction = false;
        }).fail(function (message) {
            LG_HideProgressMask();
            Util.MessageDialog(message);
        });
    }
}

ActivateDeactivateCategoryDialog.DisplayDialog = function (action, categoryID) {
    $("#activateDeactivateForm").trigger("reset");
    resetValidation();

    switch (action) {
        case "ACTIVATE":
            $('#title-action').text("Are you sure you want to activate this item?");
            $('#ID').val(categoryID);

            //show dialog
            showActivateDeactivateCategoryDialog("Activate");
            break;
        case "DEACTIVATE":
            manageCategoryService.IsCurrentCategoryAssignedToTerminals(baseURL, categoryID).done(function (rowData) {
                if (rowData.IsAssigned)
                {
                    Util.MessageDialog(rowData.WarningMessage);
                }
                else
                {
                    $('#title-action').text("Are you sure you want to deactivate this item?");
                    $('#ID').val(categoryID);

                    //show dialog
                    showActivateDeactivateCategoryDialog("Deactivate");
                }
            });

            break;
        default:
            break;
    }
};

resetValidation = function () {
    $('.field-validation-valid').text("");
}

showActivateDeactivateCategoryDialog = function (action) {
    $('#activateDeactivateDialog').dialog({
        modal: true,
        resizable: false,
        width: '30%',
        buttons: [{
            text: action,
            click: function () {
                onSaveChanges(action);
            },
            class: 'btn blue'
        }]
    });
}

onSaveChanges = function (action) {
    ActivateDeactivateCategoryDialog.SaveChanges(action, "", loadManageCategoryData);
}