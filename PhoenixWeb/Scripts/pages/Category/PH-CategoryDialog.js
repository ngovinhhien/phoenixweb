﻿//@class CategoryDialog
function CategoryDialog() { }
var baseURL = Util.PH_UpdateURL();
var isCreateCategoryAction = false;

CategoryDialog.SaveCategory = function (url, callbackMethod) {
    if ($('#CategoryForm').valid()) {
        var data = {
            CategoryID: $('#CategoryID').val() || 0,
            CategoryName: $('#CategoryName').val(),
            ShortName: $('#ShortName').val(),
            PriceCategory: $('#PriceCategory').val(),
            CommissionAmount: $("#CommissionAmount").val(),
            IsCreateCategoryAction: isCreateCategoryAction
        };

        LG_ShowProgressMask();
        manageCategoryService.SaveCategory(url, data).done(function (message) {
            $('#categoryDialog').dialog('close');

            if (isCreateCategoryAction) {
                Util.MessageDialog('New category successfully created.');
            }

            if (typeof callbackMethod == "function") {
                callbackMethod();
            }

            isCreateCategoryAction = false;
        }).fail(function (message) {
            LG_HideProgressMask();
            Util.MessageDialog(message);
        });
    }
}

CategoryDialog.DisplayCategoryDialog = function (action, categoryID) {
    document.getElementById("CategoryForm").reset();
    resetValidation();
    $("#CategoryID").val(0);

    switch (action) {
        case "ADD":
            $('#category-title').text("Add New Category");

            isCreateCategoryAction = true;
            //show dialog
            Util.ShowDialog('#categoryDialog', '30%');
            break;
        case "EDIT":
            manageCategoryService.GetCategoryDetail(baseURL, categoryID).done(function (rowData) {
                $('#category-title').text("Edit Category");

                //bind data from grid to form
                $('#CategoryName').val(rowData.CategoryName);
                $('#ShortName').val(rowData.ShortName);
                $('#PriceCategory').val(rowData.Price);
                $('#CommissionAmount').val(rowData.CommissionAmount);
                $('#CategoryID').val(categoryID);

                isCreateCategoryAction = false;
                //show dialog
                Util.ShowDialog('#categoryDialog', '30%');
            });

            break;
        default:
            break;
    }
};

CategoryDialog.AddCategoryDialog = function () {
    CategoryDialog.DisplayCategoryDialog("ADD");
};

resetValidation = function () {
    $('.field-validation-valid').text("");
};