﻿var searchGlideboxCategory = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;

$(function () {
    $(window).bind("resize", function () {
        $("#gridManageCategory").setGridWidth($("#manageCategory").width() - 10);
    }).trigger("resize");

    $("#gridManageCategory").jqGrid({
        url: "",
        colNames: [
            "Name", "Category Code", "Price", "Commission", "Product", "Created by", "Date Added", "Status", "Action"
        ],
        colModel: [
            {
                name: "CategoryName",
                index: "CategoryName",
                editable: false,
                sortable: true,
                align: 'left',
                formatter: customGetProductsMappedCategoryActionFormatter
            },
            {
                name: "ShortName",
                index: "ShortName",
                editable: false,
                sortable: false,
                align: 'left',
                formatter: customGetProductsMappedCategoryActionFormatter
            },
            {
                name: "Price",
                index: "Price",
                editable: false,
                sortable: true,
                align: 'center'
            },
            {
                name: "CommissionAmount",
                index: "CommissionAmount",
                editable: false,
                sortable: false,
                align: 'center'
            },
            {
                name: "TotalProduct",
                index: "TotalProduct",
                editable: false,
                sortable: true,
                align: 'center',
                formatter: customGetProductsMappedCategoryActionFormatter
            },
            {
                name: "CreatedBy",
                index: "CreatedBy",
                editable: false,
                sortable: false,
                align: 'left'
            },
            {
                name: "CreatedDatetime",
                index: "CreatedDatetime",
                editable: false,
                sortable: true,
                align: 'left'
            },
            {
                key: false,
                name: "IsActive",
                index: "IsActive",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customStatusFormatter
            },
            {
                key: false,
                name: "CategoryID",
                index: "CategoryID",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customActionFormatter
            }
        ],
        pager: "#gridManageCategoryPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    Util.JQGridAlignLeftHeader("gridManageCategory", "CategoryName");
    Util.JQGridAlignLeftHeader("gridManageCategory", "ShortName");
    Util.JQGridAlignLeftHeader("gridManageCategory", "CreatedBy");
    Util.JQGridAlignLeftHeader("gridManageCategory", "CreatedDatetime");

    $('#gridManageCategory').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#gridManageCategory').getGridParam('page');
            var rowNum = $('#gridManageCategory').getGridParam('rowNum');

            if (pgButton == "next_gridManageCategoryPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_gridManageCategoryPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_gridManageCategoryPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_gridManageCategoryPager") {
                if ($('#gridManageCategory').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#gridManageCategory').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#gridManageCategory').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='gridManageCategoryPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            if (!searchGlideboxCategory) {
                searchGlideboxCategory = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                };
            } else {
                searchGlideboxCategory.CurrentPage = currentPage;
                searchGlideboxCategory.PageSize = rowNum;
            }

            loadManageCategoryData();
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchGlideboxCategory) {
                searchGlideboxCategory.SortName = sortName;
                searchGlideboxCategory.SortOrder = sortOrder;
            }
            loadManageCategoryData();
            return 'stop';
        }
    });

    searchGlideboxCategory = {
        CurrentPage: 1,
        PageSize: 10
    };
    loadManageCategoryData();
});

loadManageCategoryData = function () {

    //Show progress mask
    LG_ShowProgressMask();

    manageCategoryService.SearchGlideboxCategory(baseURL, searchGlideboxCategory)
        .done(function (result) {
            //Loading data to grid
            var grid = $("#gridManageCategory")[0];
            $("#gridManageCategory").jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = false;

            LG_HideProgressMask();
        });
};

$('#btnSearchCategory').on('click', function () {
    searchCategory();
})

$('#textAdvanceSearch').on('keyup', function (event) {
    if (event.which === 13) {
        searchCategory();
    }   
})

searchCategory = function() {
    var searchText = $("#textAdvanceSearch").val();

    searchGlideboxCategory.SearchText = searchText;
    //check in the first time use filter search then reset CurrentPage value
    if (!isFiltering) {
        searchGlideboxCategory.CurrentPage = 1;
    }

    LG_ShowProgressMask();

    manageCategoryService.SearchGlideboxCategory(baseURL, searchGlideboxCategory)
        .done(function (result) {
            var grid = $('#gridManageCategory')[0];
            $('#gridManageCategory').jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = true;
            LG_HideProgressMask();
        });
}

customActionFormatter = function (categoryID, options, rowObject) {
    var html = "";
    if ($("#btnAdd").length) {
        if (rowObject.IsActive) {
            html = html + " <u><a title='Deactivate' onclick='onDeactivateCategory(" + rowObject.CategoryID + ")'>Deactivate</a></u> ";
        }
        else {
            html = html + " <u><a title='Activate' onclick='onActivateCategory(" + rowObject.CategoryID + ")'>Activate</a></u> ";
        }

        html = html + " <u><a title='Edit' onclick='onEditCategory(" + rowObject.CategoryID + ")' id='edit_" + rowObject.CategoryID + "'>Edit</a></u> ";
        html = html + " <u><a title='History' onclick='GetGlideboxHistory(" + rowObject.CategoryID + "," + '"' + rowObject.CategoryName + '"' + "," + '"Category"' + ")'>History</a></u>";
    } else {
        html = "";
    }

    return html;
};

customStatusFormatter = function (isActive, options, rowObject) {
    var html = "";

    if (isActive) {
        html = html + "<i class='glyphicon glyphicon-ok' style='color: #04ac4c'></i>";
    }
    else {
        html = html + "<i class='glyphicon glyphicon-remove' style='color: red'></i>";
    }
    
    return html;
};

customGetProductsMappedCategoryActionFormatter = function (categoryID, options, rowObject) {
    var value = "";
    if (options.colModel.name == "CategoryName") {
        value = rowObject.CategoryName;
    }
    else if (options.colModel.name == "ShortName") {
        value = rowObject.ShortName;
    }
    else if (options.colModel.name == "TotalProduct") {
        value = rowObject.TotalProduct;
    }

    var html = " <a onclick='GetProductsMappedCategory(" + rowObject.CategoryID + ")'> " + value + "</a> ";

    return html;
};

$("#btnAdd").on("click",
    function () {
        CategoryDialog.AddCategoryDialog();
    });

onSaveCategory = function () {
    CategoryDialog.SaveCategory("", loadManageCategoryData);
}

onEditCategory = function (id) {
    CategoryDialog.DisplayCategoryDialog("EDIT", id);
}

onDeactivateCategory = function (id) {
    ActivateDeactivateCategoryDialog.DisplayDialog("DEACTIVATE", id);
}

onActivateCategory = function (id) {
    ActivateDeactivateCategoryDialog.DisplayDialog("ACTIVATE", id);
}

GetProductsMappedCategory = function (categoryID) {
    window.location = baseURL + 'Product?categoryID=' + categoryID;
}