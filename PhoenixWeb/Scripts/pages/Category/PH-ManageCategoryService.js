﻿(function (manageCategoryService, $, undefined) {
    manageCategoryService.SearchGlideboxCategory = function (baseURL, searchGlideboxCategory) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Category/GetCategories",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchGlideboxCategory),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    manageCategoryService.SaveCategory = function (url, formData) {
        var deferred = $.Deferred();
        var saveURL = "";
        if (url) {
            saveURL = url;
        } else {
            saveURL = "Category/SaveCategory";
        }

        $.ajax({
            url: baseURL + saveURL,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(formData),
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve("The Category has been saved successful.");
                }
            }
        });
        return deferred.promise();
    }

    manageCategoryService.GetCategoryDetail = function (baseURL, categoryID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Category/GetCategoryDetail",
            type: "GET",
            data: {
                categoryID: categoryID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    manageCategoryService.SaveChanges = function (url, formData) {
        var deferred = $.Deferred();
        var saveURL = "";
        if (url) {
            saveURL = url;
        } else {
            saveURL = "Category/SaveChanges";
        }

        $.ajax({
            url: baseURL + saveURL,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(formData),
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve();
                }
            }
        });
        return deferred.promise();
    }

    manageCategoryService.IsCurrentCategoryAssignedToTerminals = function (baseURL, categoryID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Category/IsCurrentCategoryAssignedToTerminals",
            type: "GET",
            data: {
                categoryID: categoryID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    manageCategoryService.GetCategoryHistories = function (baseURL, searchCategoryHistory) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Category/SearchCategoryHistory",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchCategoryHistory),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        SearchGlideboxCategory: manageCategoryService.SearchGlideboxCategory
    };

}(window.manageCategoryService = window.manageCategoryService || {}, jQuery));