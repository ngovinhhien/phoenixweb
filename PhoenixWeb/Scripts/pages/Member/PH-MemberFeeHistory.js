﻿var baseURL = Util.PH_UpdateURL();
var memberFeeHistoryModel = undefined;

$(function () {
    $("#grdMemberFeeHistory").jqGrid({
        url: '',
        colNames: ['Charge Fee', 'Description', 'Charged Amount', 'Processed Date'],
        colModel: [
            { key: false, name: 'ChargeFee', index: 'ChargeFee', editable: false, sortable: false },
            { key: false, name: 'Description', index: 'Description', editable: false, sortable: false },
            { key: false, name: 'ChargedAmount', index: 'ChargedAmount', editable: false, sortable: false },
            { key: false, name: 'ProcessedDate', index: 'ProcessedDate', editable: false, formatter: 'date', formatoptions: { newformat: "d/m/Y h:m" }, sortable: false },
        ],
        loadui: "disable",
        rowNum: 10,
        rowList: [],
        height: "100%",
        viewrecords: true,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false,
        pager: '#grdMemberFeeHistoryPager',
    });

    $('#grdMemberFeeHistory').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdMemberFeeHistory').getGridParam('page');
            var rowNum = $('#grdMemberFeeHistory').getGridParam('rowNum');

            if (pgButton == "next_grdMemberFeeHistoryPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdMemberFeeHistoryPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdMemberFeeHistoryPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdMemberFeeHistoryPager") {
                currentPage = $('#grdMemberFeeHistory').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='grdMemberFeeHistoryPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            
            if (currentPage > totalPage) {
                return;
            }

            memberFeeHistoryModel.CurrentPage = currentPage;
            memberFeeHistoryModel.PageSize = rowNum;

            loadMemberFeeHistory();
        }
    });

    //Initialize load notes
    memberFeeHistoryModel = {
        CurrentPage: 1,
        PageSize: 10,
        MemberKey: $("#Member_key").val()
    };

    loadMemberFeeHistory();
});

loadMemberFeeHistory = function () {
    //display loading state
    $("#grdMemberFeeHistory").closest(".ui-jqgrid").find('.loading').show();

    $.ajax({
        url: baseURL + "Member/GetMemberFeeHistory",
        headers: {
            "Content-Type": "application/json"
        },
        type: "GET",
        data: {
            memberKey: memberFeeHistoryModel.MemberKey,
            currentPage: memberFeeHistoryModel.CurrentPage,
            pageSize: memberFeeHistoryModel.PageSize
        }
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdMemberFeeHistory')[0];
        $('#grdMemberFeeHistory').jqGrid('clearGridData');
        grid.addJSONData(result);

        //hide loading state
        $("#grdMemberFeeHistory").closest(".ui-jqgrid").find('.loading').hide();
    }).fail(function (err) {
        console.log(err);
    });
};