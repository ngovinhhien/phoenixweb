﻿var BaseURL = Util.PH_UpdateURL();
SendAPIKey = function (terminalID) {
    if (!terminalID) {
        Util.MessageDialog("Please select virtual terminal ID.");
        return;
    }

    terminalAllocateionService.SendAPIKey(BaseURL, terminalID)
        .done(function (result) {
            if (result.IsSuccess) {
                Util.MessageDialog("Email sent successfully");
            } else {
                Util.MessageDialog("There has been an issue sending the email. Please try again.");
            }
        });

};