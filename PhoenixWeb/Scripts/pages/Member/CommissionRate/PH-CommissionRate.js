﻿var baseURL = Util.PH_UpdateURL();
var businessID = "";
var defaultCommissionRatesJSON = "";
var businessAllowTranFee =
    $('#BusinessKeysAllowFixedFee').val() ? $('#BusinessKeysAllowFixedFee').val().split(';') : [];
;
var payMethodAllowPricingPlan = [
    'CommissionRateCRVisaRate',
    'CommissionRateCRVisaDebitRate',
    'CommissionRateCRMasterCardRate',
    'CommissionRateCRMasterCardDebitRate',
    'CommissionRateCRUnionPayRate',
    'CommissionRateCRDebitRate',
    'CommissionRateCRDebitTranFee',
    'CommissionRateCRVisaTranFee',
    'CommissionRateCRVisaDebitTranFee',
    'CommissionRateCRMasterCardTranFee',
    'CommissionRateCRMasterCardDebitTranFee',
    'CommissionRateCRUnionPayTranFee'
];
var listPricingPlanTemplate = [];

$(document).ready(function () {
    $(".errorIcon").hide();
    $(".warningIcon").hide();
    defaultCommissionRatesJSON = "";

    CloseCommissionRateDialog = function () {
        $(".errorIcon").hide();
        $(".warningIcon").hide();
        $("#editMemberCommissionRateDialog").dialog("close");
    };

    RenderCommissionRates = function (tblContainName) {
        var commissionRatesData = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];
        var isFeeFreeMember = $('#IsFeeFreeMember').prop('checked');
        var tabs = $('#CommissionRateGrid').html($('#CommissionRateGridTemplate').render({ data: {Rates: commissionRatesData, isFeeFreeMember: isFeeFreeMember }}));

        $('a[role="tab"][index="0"]').trigger('click');
    }

    ModifyCommissionRate = function (memberId, subBusinessType, isUpdated, isFromCreatePage) {
        RefreshDialog();
        var businessType = $("#BusinessType").val();
        if (businessType == 0) {
            alert("Please select Business Type");
            return;
        }

        var state = "";
        var windowTitle = "Add Commission Rate";
        var isDisableSubBusinessType = false;
        $('div[name="currentRate"]').hide();
        if (isUpdated) {
            $('div[name="currentRate"]').show();
            isDisableSubBusinessType = true;
            windowTitle = "Update Commission Rate";
        }
        GetItemDataForModifyCommissionRatePopup(businessType, state).done(function (data) {
            var popupViewDetailLog = $('#editMemberCommissionRateDialog').dialog({
                title: windowTitle,
                position: "center",
                width: 900
            });
            popupViewDetailLog.dialog('option', { 'resizable': false });

            BindingInformation(data, businessType, subBusinessType, memberId, isUpdated);
            BindingCommissionData(businessType, subBusinessType);
            BindingPricingPlanComboBox();
            $('#CommissionSubBusinessType').attr('disabled', isDisableSubBusinessType);
        });
    };

    SaveCommissionRate = function () {
        var businessType = $("#CommissionRateBusinessType").val();
        var subBusinessTypeId = $('#CommissionSubBusinessType').val();
        if (!subBusinessTypeId) {
            Util.MessageDialog('Sub-Business Type cannot be empty');
            return;
        }

        SaveRate(businessType, subBusinessTypeId);

        $('a[role="tab"][aria-controls="#SubBusinessType' + subBusinessTypeId + '"]').trigger('click');
    };

    SaveRate = function (businessType, subBusinessTypeId) {
        $("#CommissionRateCRDebitRate").trigger("focusout");
        $("#CommissionRateCRQantasRate").trigger("focusout");
        $("#CommissionRateCRVisaRate").trigger("focusout");
        $("#CommissionRateCRVisaDebitRate").trigger("focusout");
        $("#CommissionRateCRMasterCardRate").trigger("focusout");
        $("#CommissionRateCRMasterCardDebitRate").trigger("focusout");
        $("#CommissionRateCRAmexRate").trigger("focusout");
        $("#CommissionRateCRDinersRate").trigger("focusout");
        $("#CommissionRateCRUnionPayRate").trigger("focusout");
        $("#CommissionRateCRZipPayRate").trigger("focusout");
        $("#CommissionRateCRAlipayRate").trigger("focusout");
        $("#CommissionRateCRJCBRate").trigger("focusout");

        $("#CommissionRateCRDebitTranFee").trigger("focusout");
        $("#CommissionRateCRQantasTranFee").trigger("focusout");
        $("#CommissionRateCRVisaTranFee").trigger("focusout");
        $("#CommissionRateCRVisaDebitTranFee").trigger("focusout");
        $("#CommissionRateCRMasterCardTranFee").trigger("focusout");
        $("#CommissionRateCRMasterCardDebitTranFee").trigger("focusout");
        $("#CommissionRateCRAmexTranFee").trigger("focusout");
        $("#CommissionRateCRDinersTranFee").trigger("focusout");
        $("#CommissionRateCRUnionPayTranFee").trigger("focusout");
        $("#CommissionRateCRZipPayTranFee").trigger("focusout");
        $("#CommissionRateCRAlipayTranFee").trigger("focusout");
        $("#CommissionRateCRJCBTranFee").trigger("focusout");

        var allRates = $("input.cmrate");
        var allTranFees = $("input.TranFee");

        for (var i = 0; i < allRates.length; i++) {
            if ($(allRates[i]).hasClass("errorClass")) {
                $(allRates[i]).focus();
                return;
            }
            else if ($(allTranFees[i]).hasClass("errorClass")) {
                $(allTranFees[i]).focus();
                return;
            }
        }

        var updatedModel = GetUpdatedRateData();

        var rateDatas = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];
        var index = rateDatas.findIndex(obj => obj.BusinessType == businessType && obj.SubBusinessTypeID == subBusinessTypeId);
        if (index != -1) { //edit

            rateDatas[index].StartDate = updatedModel.startDate;
            rateDatas[index].CRVisaRate = isNaN(updatedModel.visaRate) ? rateDatas[index].CRVisaRate : updatedModel.visaRate;
            rateDatas[index].CRVisaDebitRate = isNaN(updatedModel.visaDebitRate) ? rateDatas[index].CRVisaDebitRate : updatedModel.visaDebitRate;
            rateDatas[index].CRMasterCardRate = isNaN(updatedModel.masterRate) ? rateDatas[index].CRMasterCardRate : updatedModel.masterRate;
            rateDatas[index].CRMasterCardDebitRate = isNaN(updatedModel.masterDebitRate) ? rateDatas[index].CRMasterCardDebitRate : updatedModel.masterDebitRate;
            rateDatas[index].CRAmexRate = isNaN(updatedModel.amexRate) ? rateDatas[index].CRAmexRate : updatedModel.amexRate;
            rateDatas[index].CRDinersRate = isNaN(updatedModel.dinersRate) ? rateDatas[index].CRDinersRate : updatedModel.dinersRate;
            rateDatas[index].CRUnionPayRate = isNaN(updatedModel.unionRate) ? rateDatas[index].CRUnionPayRate : updatedModel.unionRate;
            rateDatas[index].CRZipPayRate = isNaN(updatedModel.zipRate) ? rateDatas[index].CRZipPayRate : updatedModel.zipRate;
            rateDatas[index].CRAlipayRate = isNaN(updatedModel.alipayRate) ? rateDatas[index].CRAlipayRate : updatedModel.alipayRate;
            rateDatas[index].CRJCBRate = isNaN(updatedModel.jcbRate) ? rateDatas[index].CRJCBRate : updatedModel.jcbRate;
            rateDatas[index].DebitRateType = updatedModel.debitRateType;
            rateDatas[index].QantasRateType = updatedModel.qantasRateType;
            rateDatas[index].IsDebitFixed = isNaN(updatedModel.debitRate) ? rateDatas[index].IsDebitFixed : updatedModel.isDebitFixed;
            rateDatas[index].IsQantasFixed = isNaN(updatedModel.qantasRate) ? rateDatas[index].IsQantasFixed : updatedModel.isQantasFixed;

            rateDatas[index].CRDebitTranFee = isNaN(updatedModel.debitTranFee) ? rateDatas[index].CRDebitTranFee : updatedModel.debitTranFee;
            rateDatas[index].CRVisaTranFee = isNaN(updatedModel.visaTranFee) ? rateDatas[index].CRVisaTranFee : updatedModel.visaTranFee;
            rateDatas[index].CRVisaDebitTranFee = isNaN(updatedModel.visaDebitTranFee) ? rateDatas[index].CRVisaDebitTranFee : updatedModel.visaDebitTranFee;
            rateDatas[index].CRMasterCardTranFee = isNaN(updatedModel.masterTranFee) ? rateDatas[index].CRMasterCardTranFee : updatedModel.masterTranFee;
            rateDatas[index].CRMasterCardDebitTranFee = isNaN(updatedModel.masterDebitTranFee) ? rateDatas[index].CRMasterCardDebitTranFee : updatedModel.masterDebitTranFee;
            rateDatas[index].CRAmexTranFee = isNaN(updatedModel.amexTranFee) ? rateDatas[index].CRAmexTranFee : updatedModel.amexTranFee;
            rateDatas[index].CRDinersTranFee = isNaN(updatedModel.dinersTranFee) ? rateDatas[index].CRDinersTranFee : updatedModel.dinersTranFee;
            rateDatas[index].CRUnionPayTranFee = isNaN(updatedModel.unionTranFee) ? rateDatas[index].CRUnionPayTranFee : updatedModel.unionTranFee;
            rateDatas[index].CRZipPayTranFee = isNaN(updatedModel.zipTranFee) ? rateDatas[index].CRZipPayTranFee : updatedModel.zipTranFee;
            rateDatas[index].CRQantasTranFee = isNaN(updatedModel.qantasTranFee) ? rateDatas[index].CRQantasTranFee : updatedModel.qantasTranFee;
            rateDatas[index].CRAlipayTranFee = isNaN(updatedModel.alipayTranFee) ? rateDatas[index].CRAlipayTranFee : updatedModel.alipayTranFee;
            rateDatas[index].CRJCBTranFee = isNaN(updatedModel.jcbTranFee) ? rateDatas[index].CRJCBTranFee : updatedModel.jcbTranFee;

            if (!isNaN(updatedModel.debitRate)) {
                if (!rateDatas[index].IsDebitFixed) {
                    rateDatas[index].CRDebitRatePercentage = updatedModel.debitRate;
                    rateDatas[index].CRDebitRate = updatedModel.debitRate;
                }
                else {
                    rateDatas[index].CRDebitRate = updatedModel.debitRate;
                }
            }

            if (!isNaN(updatedModel.qantasRate)) {
                if (!rateDatas[index].IsQantasFixed) {
                    rateDatas[index].CRQantasRatePercentage = updatedModel.qantasRate;
                    rateDatas[index].CRQantasRate = updatedModel.qantasRate;
                }
                else {
                    rateDatas[index].CRQantasRate = updatedModel.qantasRate;
                }
            }

            rateDatas[index].IsEdited = true;
        }
        else {
            if (IsNotEmpty(updatedModel)) {
                rateDatas.push({
                    Number: rateDatas.length + 1,
                    StartDate: updatedModel.startDate,
                    CRDebitRate: updatedModel.debitRate,
                    CRVisaRate: updatedModel.visaRate,
                    CRVisaDebitRate: updatedModel.visaDebitRate,
                    CRMasterCardRate: updatedModel.masterRate,
                    CRMasterCardDebitRate: updatedModel.masterDebitRate,
                    CRAmexRate: updatedModel.amexRate,
                    CRDinersRate: updatedModel.dinersRate,
                    CRUnionPayRate: updatedModel.unionRate,
                    CRZipPayRate: updatedModel.zipRate,
                    CRQantasRate: updatedModel.qantasRate,
                    CRAlipayRate: updatedModel.alipayRate,
                    CRJCBRate: updatedModel.jcbRate,
                    CRDebitTranFee: updatedModel.debitTranFee,
                    CRVisaTranFee: updatedModel.visaTranFee,
                    CRVisaDebitTranFee: updatedModel.visaDebitTranFee,
                    CRMasterCardTranFee: updatedModel.masterTranFee,
                    CRMasterCardDebitTranFee: updatedModel.masterDebitTranFee,
                    CRAmexTranFee: updatedModel.amexTranFee,
                    CRDinersTranFee: updatedModel.dinersTranFee,
                    CRUnionPayTranFee: updatedModel.unionTranFee,
                    CRZipPayTranFee: updatedModel.zipTranFee,
                    CRQantasTranFee: updatedModel.qantasTranFee,
                    CRAlipayTranFee: updatedModel.alipayTranFee,
                    CRJCBTranFee: updatedModel.jcbTranFee,
                    DebitRateType: updatedModel.debitRateType,
                    QantasRateType: updatedModel.qantasRateType,
                    BusinessType: businessType,
                    SubBusinessType: $('#CommissionSubBusinessType :selected').text(),
                    BusinessTypeID: businessID,
                    SubBusinessTypeID: subBusinessTypeId,
                    IsDebitFixed: updatedModel.isDebitFixed,
                    IsQantasFixed: updatedModel.isQantasFixed,
                    CRDebitRatePercentage: updatedModel.debitRate,
                    CRQantasRatePercentage: updatedModel.qantasRate,
                    IsEdited: true
                });
            }
        }
        $("#CommissionRatesDtoAsJson").val(JSON.stringify(rateDatas));
        RenderCommissionRates("commissionRateGrid");

        CloseCommissionRateDialog();
    }

    IsNotEmpty = function (model) {
        if (
            !isNaN(model.debitRate) ||
            !isNaN(model.visaRate) ||
            !isNaN(model.masterRate) ||
            !isNaN(model.dinersRate) ||
            !isNaN(model.unionRate) ||
            !isNaN(model.zipRate) ||
            !isNaN(model.qantasRate) ||
            !isNaN(model.alipayRate) ||
            !isNaN(model.amexRate) ||
            !isNaN(model.jcbRate) ||
            !isNaN(model.debitTranFee) ||
            !isNaN(model.visaTranFee) ||
            !isNaN(model.visaDebitTranFee) ||
            !isNaN(model.masterTranFee) ||
            !isNaN(model.masterDebitTranFee) ||
            !isNaN(model.dinersTranFee) ||
            !isNaN(model.unionTranFee) ||
            !isNaN(model.zipTranFee) ||
            !isNaN(model.qantasTranFee) ||
            !isNaN(model.alipayTranFee) ||
            !isNaN(model.amexTranFee) ||
            !isNaN(model.jcbTranFee) ||
            !(businessAllowTranFee.indexOf(businessID) > 0 && !isNaN(model.visaDebitRate)) ||
            !(businessAllowTranFee.indexOf(businessID) > 0 && !isNaN(model.masterDebitRate))
        ) {
            return true;
        }
        return false;
    }

    ApplyInfoForRates = function (debit, visa, visaDebit, master, masterDebit, amex, diners, union, zip, qantas, alipay, jcb) {
        $("#CommissionRateCRDebitRate").val(debit);
        $("#CommissionRateCRVisaRate").val(visa);
        $("#CommissionRateCRVisaDebitRate").val(visaDebit);
        $("#CommissionRateCRMasterCardRate").val(master);
        $("#CommissionRateCRMasterCardDebitRate").val(masterDebit);
        $("#CommissionRateCRAmexRate").val(amex);
        $("#CommissionRateCRDinersRate").val(diners);
        $("#CommissionRateCRUnionPayRate").val(union);
        $("#CommissionRateCRZipPayRate").val(zip);
        $("#CommissionRateCRQantasRate").val(qantas);
        $("#CommissionRateCRAlipayRate").val(alipay);
        $("#CommissionRateCRJCBRate").val(jcb);
    }

    RefreshDialog = function () {
        ApplyInfoForRates('', '', '', '', '', '', '', '', '', '', '', '');
        $("#CommissionRatecurrentDebitRateString").text('');
        $("#CommissionRatecurrentVisaRateString").text('');
        $("#CommissionRatecurrentVisaDebitRateString").text('');
        $("#CommissionRatecurrentMasterRateString").text('');
        $("#CommissionRatecurrentMastercardDebitRateString").text('');
        $("#CommissionRatecurrentAmexRateString").text('');
        $("#CommissionRatecurrentDinersRateString").text('');
        $("#CommissionRatecurrentUnionRateString").text('');
        $("#CommissionRatecurrentZipRateString").text('');
        $("#CommissionRatecurrentQantasRateString").text('');
        $("#CommissionRatecurrentAlipayRateString").text('');
        $("#CommissionRatecurrentJCBRateString").text('');
    }

    BindingInformation = function (data, businessType, subBusinessType, memberID, isUpdated) {
        var subBusinessTypeCbx = $('#CommissionSubBusinessType');
        var rateDatas = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];

        subBusinessTypeCbx.empty();
        
        if ($('input#IsFeeFreeMember').prop('checked'))
        {
            data.VehicleTypeListItem = data.VehicleTypeListItem.filter(v => v.Text !== 'Retail');
        }

        $.each(data.VehicleTypeListItem, function (i, el) {
            subBusinessTypeCbx.append(new Option(el.Text, el.Value));
        });

        if (!isUpdated) {
            var occupiedSubBusinessIds = rateDatas.map(function (r) {
                return r.SubBusinessTypeID;
            });
            $.each(occupiedSubBusinessIds, function (i, el) {
                subBusinessTypeCbx.find('option[value=' + el + ']').remove();
            });
        }

        if (subBusinessType != null) {
            subBusinessTypeCbx.val(subBusinessType);
        }

        $("#CommissionRateBusinessType").val(data.BusinessName);
        $("#CommissionRateStartDate").val(data.StartDate);

        LH_ChangeDebitRateDisplay(businessType.toString());
        LH_ChangeQatasRateDisplay(businessType.toString());
        
        CommissionSubBusinessTypeChange(subBusinessTypeCbx[0].selectedOptions[0].text);
    }

    BindingCommissionData = function (businessType, subBusinessType) {
        var rateDatas = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];
        var rate = rateDatas.filter(function (r) {
            return r.BusinessTypeID == businessType && r.SubBusinessTypeID == subBusinessType;
        });

        ResetDefaultRateAndTranFee();

        if (rate.length > 0) {
            var rdbDebitRateOption = $('select#IsFixedRateDebit');
            var rdbQantasRateOption = $('select#IsFixedRateQantas');

            var debitRate =
                rate[0].IsDebitFixed == true
                    ? (isNaN(rate[0].CRDebitRate) || rate[0].CRDebitRate == null) ? "" : rate[0].CRDebitRate.toFixed(4)
                    : (isNaN(rate[0].CRDebitRatePercentage) || rate[0].CRDebitRatePercentage == null) ? "" : rate[0].CRDebitRatePercentage.toFixed(4);

            var qantasRate =
                rate[0].IsQantasFixed == true
                    ? (isNaN(rate[0].CRQantasRate) || rate[0].CRQantasRate == null) ? "" : rate[0].CRQantasRate.toFixed(4)
                    : (isNaN(rate[0].CRQantasRatePercentage) || rate[0].CRQantasRatePercentage == null) ? "" : rate[0].CRQantasRatePercentage.toFixed(4);
            var visaRate = (isNaN(rate[0].CRVisaRate) || (rate[0].CRVisaRate == null)) ? "" : rate[0].CRVisaRate.toFixed(4);
            var visaDebitRate = (isNaN(rate[0].CRVisaDebitRate) || (rate[0].CRVisaDebitRate == null)) ? "" : rate[0].CRVisaDebitRate.toFixed(4);
            var masterRate = (isNaN(rate[0].CRMasterCardRate) || (rate[0].CRMasterCardRate == null)) ? "" : rate[0].CRMasterCardRate.toFixed(4);
            var masterDebitRate = (isNaN(rate[0].CRMasterCardDebitRate) || (rate[0].CRMasterCardDebitRate == null)) ? "" : rate[0].CRMasterCardDebitRate.toFixed(4);
            var amexRate = (isNaN(rate[0].CRAmexRate) || (rate[0].CRAmexRate == null)) ? "" : rate[0].CRAmexRate.toFixed(4);
            var dinersRate = (isNaN(rate[0].CRDinersRate) || (rate[0].CRDinersRate == null)) ? "" : rate[0].CRDinersRate.toFixed(4);
            var unionRate = (isNaN(rate[0].CRUnionPayRate) || (rate[0].CRUnionPayRate == null)) ? "" : rate[0].CRUnionPayRate.toFixed(4);
            var zipRate = (isNaN(rate[0].CRZipPayRate) || (rate[0].CRZipPayRate == null)) ? "" : rate[0].CRZipPayRate.toFixed(4);
            var alipayRate = (isNaN(rate[0].CRAlipayRate) || (rate[0].CRAlipayRate == null)) ? "" : rate[0].CRAlipayRate.toFixed(4);
            var jcbRate = (isNaN(rate[0].CRJCBRate) || (rate[0].CRJCBRate == null)) ? "" : rate[0].CRJCBRate.toFixed(4);


            rdbDebitRateOption.val(rate[0].IsDebitFixed ? 'Dollar' : 'Percentage');
            rdbQantasRateOption.val(rate[0].IsQantasFixed ? 'Dollar' : 'Percentage');

            ApplyInfoForRates(
                debitRate,
                visaRate,
                visaDebitRate,
                masterRate,
                masterDebitRate,
                amexRate,
                dinersRate,
                unionRate,
                zipRate,
                qantasRate,
                alipayRate,
                jcbRate
            );

            var debitTranFee = (isNaN(rate[0].CRDebitTranFee) || rate[0].CRDebitTranFee == null) ? "" : rate[0].CRDebitTranFee.toFixed(4);
            var qantasTranFee = (isNaN(rate[0].CRQantasTranFee) || rate[0].CRQantasTranFee == null) ? "" : rate[0].CRQantasTranFee.toFixed(4);
            var visaTranFee = (isNaN(rate[0].CRVisaTranFee) || (rate[0].CRVisaTranFee == null)) ? "" : rate[0].CRVisaTranFee.toFixed(4);
            var visaDebitTranFee = (isNaN(rate[0].CRVisaDebitTranFee) || (rate[0].CRVisaDebitTranFee == null)) ? "" : rate[0].CRVisaDebitTranFee.toFixed(4);
            var masterTranFee = (isNaN(rate[0].CRMasterCardTranFee) || (rate[0].CRMasterCardTranFee == null)) ? "" : rate[0].CRMasterCardTranFee.toFixed(4);
            var masterDebitTranFee = (isNaN(rate[0].CRMasterCardDebitTranFee) || (rate[0].CRMasterCardDebitTranFee == null)) ? "" : rate[0].CRMasterCardDebitTranFee.toFixed(4);
            var amexTranFee = (isNaN(rate[0].CRAmexTranFee) || (rate[0].CRAmexTranFee == null)) ? "" : rate[0].CRAmexTranFee.toFixed(4);
            var dinersTranFee = (isNaN(rate[0].CRDinersTranFee) || (rate[0].CRDinersTranFee == null)) ? "" : rate[0].CRDinersTranFee.toFixed(4);
            var unionTranFee = (isNaN(rate[0].CRUnionPayTranFee) || (rate[0].CRUnionPayTranFee == null)) ? "" : rate[0].CRUnionPayTranFee.toFixed(4);
            var zipTranFee = (isNaN(rate[0].CRZipPayTranFee) || (rate[0].CRZipPayTranFee == null)) ? "" : rate[0].CRZipPayTranFee.toFixed(4);
            var alipayTranFee = (isNaN(rate[0].CRAlipayTranFee) || (rate[0].CRAlipayTranFee == null)) ? "" : rate[0].CRAlipayTranFee.toFixed(4);
            var jcbTranFee = (isNaN(rate[0].CRJCBTranFee) || (rate[0].CRJCBTranFee == null)) ? "" : rate[0].CRJCBTranFee.toFixed(4);


            ApplyInfoForTranFees(
                debitTranFee,
                visaTranFee,
                visaDebitTranFee,
                masterTranFee,
                masterDebitTranFee,
                amexTranFee,
                dinersTranFee,
                unionTranFee,
                zipTranFee,
                qantasTranFee,
                alipayTranFee,
                jcbTranFee
            );
        }
    }

    GetRateString = function (rate, fixable) {
        if (fixable) {
            if (Math.abs(rate) > 0.1 && fixable)
                return rate;
            else
                return rate * 100;
        }
        else {
            return rate;
        }
    }

    IsDebitDollarChecked = function () {
        return $("select#IsFixedRateDebit").val() == 'Dollar';
    }

    IsQantasDollarChecked = function () {
        return $("select#IsFixedRateQantas").val() == 'Dollar';
    }

    GetUpdatedRateData = function () {
        var isDollarCheckedQantas = IsQantasDollarChecked();
        var isDollarCheckedDebit = IsDebitDollarChecked();

        return {
            startDate: $("#CommissionRateStartDate").val(),
            debitRate: parseFloat($("#CommissionRateCRDebitRate").val()),
            visaRate: parseFloat($("#CommissionRateCRVisaRate").val()),
            visaDebitRate: parseFloat($("#CommissionRateCRVisaDebitRate").val()),
            masterRate: parseFloat($("#CommissionRateCRMasterCardRate").val()),
            masterDebitRate: parseFloat($("#CommissionRateCRMasterCardDebitRate").val()),
            amexRate: parseFloat($("#CommissionRateCRAmexRate").val()),
            dinersRate: parseFloat($("#CommissionRateCRDinersRate").val()),
            unionRate: parseFloat($("#CommissionRateCRUnionPayRate").val()),
            zipRate: parseFloat($("#CommissionRateCRZipPayRate").val()),
            qantasRate: parseFloat($("#CommissionRateCRQantasRate").val()),
            alipayRate: parseFloat($("#CommissionRateCRAlipayRate").val()),
            jcbRate: parseFloat($("#CommissionRateCRJCBRate").val()),
            debitRateType: isDollarCheckedDebit ? "Dollar" : "Percentage",
            qantasRateType: isDollarCheckedQantas ? "Dollar" : "Percentage",
            isDebitFixed: isDollarCheckedDebit,
            isQantasFixed: isDollarCheckedQantas,
            debitTranFee: parseFloat($("#CommissionRateCRDebitTranFee").val()),
            visaTranFee: parseFloat($("#CommissionRateCRVisaTranFee").val()),
            visaDebitTranFee: parseFloat($("#CommissionRateCRVisaDebitTranFee").val()),
            masterTranFee: parseFloat($("#CommissionRateCRMasterCardTranFee").val()),
            masterDebitTranFee: parseFloat($("#CommissionRateCRMasterCardDebitTranFee").val()),
            amexTranFee: parseFloat($("#CommissionRateCRAmexTranFee").val()),
            dinersTranFee: parseFloat($("#CommissionRateCRDinersTranFee").val()),
            unionTranFee: parseFloat($("#CommissionRateCRUnionPayTranFee").val()),
            zipTranFee: parseFloat($("#CommissionRateCRZipPayTranFee").val()),
            qantasTranFee: parseFloat($("#CommissionRateCRQantasTranFee").val()),
            alipayTranFee: parseFloat($("#CommissionRateCRAlipayTranFee").val()),
            jcbTranFee: parseFloat($("#CommissionRateCRJCBTranFee").val())
        };
    }

    ResetDefaultRateAndTranFee = function () {
        ResetDefaultRate();
        ResetDefaultTranFee();

        $('input.errorClass').removeClass('errorClass');
        $('img.errorIcon').hide();
    }

    ResetDefaultRate = function () {
        var isDollarCheckedQantas = IsQantasDollarChecked();
        var isDollarCheckedDebit = IsDebitDollarChecked();
        var subBusinessType = $('#CommissionSubBusinessType :selected').text();
        var businessTypeId = $("#BusinessType").val();
        var commission = LG_GetDefaultValueForCommissionRateByBusinessType(businessTypeId, subBusinessType, isDollarCheckedDebit, isDollarCheckedQantas)
        ApplyInfoForRates(
            commission.DebitRate,
            commission.Visa,
            commission.VisaDebit,
            commission.Master,
            commission.MasterDebit,
            commission.Amex,
            commission.Diners,
            commission.UnionPay,
            commission.Zip,
            commission.Qantas,
            commission.Alipay,
            commission.JCB
        );
    }

    GetItemDataForModifyCommissionRatePopup = function (businessType, state) {
        var deferred = $.Deferred();
        businessID = businessType.toString();
        $.ajax({
            url: baseURL + "Member/LoadDataForCommissionRatePopup",
            data: { BusinessType: businessType, State: state },
            type: "GET",
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve(result);
                }
            }
        });

        return deferred.promise();
    }

    $("#CommissionSubBusinessType").change(function () {
        ResetDefaultRateAndTranFee();
        BindingPricingPlanComboBox();

        $('input.errorClass').removeClass('errorClass');
        $('img.errorIcon').hide();
    });

    LH_ChangeDebitRateDisplay = function (BusinessTypeVal) {
        var rdbDebitRateOption = $('select#IsFixedRateDebit');

        rdbDebitRateOption.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1"://TaxiEpay
                {
                    rdbDebitRateOption.val('Percentage')
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "18"://Live Eftpos
                rdbDebitRateOption.val('Dollar');
                break;
            case "19"://DirectLiveEftpos
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "20"://TaxiPro
                {
                    rdbDebitRateOption.val('Dollar');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "21"://TaxiApp
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "22"://LiveEftpos black
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "23"://LiveEftpos black for taxi
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "24"://Live limo
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "25"://Glide
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "26"://Live Eftpos Integrated
                {
                    rdbDebitRateOption.val('Dollar');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "27"://Live Eftpos Black Integrated
                {
                    rdbDebitRateOption.val('Percentage');
                    rdbDebitRateOption.attr('disabled', true);
                }
                break;
            case "28"://LiveSMS
                break;
            case "29"://CSA Integrated
                rdbDebitRateOption.val('Dollar');
                break;
        }
    };

    LH_ChangeQatasRateDisplay = function (BusinessTypeVal) {
        var rdbQantasRateOption = $('select#IsFixedRateQantas');

        rdbQantasRateOption.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1": //TaxiEpay
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "18": //Live Eftpos
                rdbQantasRateOption.val('Dollar');
                break;
            case "19": //DirectLiveEftpos
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "20": //TaxiPro
                {
                    rdbQantasRateOption.val('Dollar');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "21": //TaxiApp
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "22": //LiveEftpos black
                break;
            case "23": //LiveEftpos black for taxi
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "24": //Live limo
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "25": //Glide
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "26": //Live Eftpos Integrated
                {
                    rdbQantasRateOption.val('Percentage');
                }
                break;
            case "27": //Live Eftpos Black Integrated
                {
                    rdbQantasRateOption.val('Percentage');
                }
                break;
            case "28": //LiveSMS
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
            case "29": //CSA Integrated
                {
                    rdbQantasRateOption.val('Percentage');
                    rdbQantasRateOption.attr('disabled', true);
                }
                break;
        }
    };

    validateAmount = function (fieldname, minValue, maxValue, isDollarChecked) {
        var subBusinessType = $('#CommissionSubBusinessType :selected').text();
        if (subBusinessType == "Live local" || subBusinessType == "eCommerce") {
            if (isDollarChecked) {
                minValue = -2;
                maxValue = 0;
            }
            else {
                minValue = -5;
                maxValue = 0;
            }
        }

        var fieldValue = $(fieldname).val();

        if (!fieldValue) {
            return false;
        }

        var regularExpress = "^(\-?[0-9]+(.[0-9]{1,4})?)$";
        var re = new RegExp(regularExpress);

        if (fieldValue < minValue || fieldValue > maxValue || !re.test(fieldValue)) {
            var message = "Inputted amount must be between " + minValue + " to " + maxValue + " and maximum 4 digits after decimal points.";
            $(fieldname).attr("title", message);

            var errorIcon = $(fieldname).parent().find(".errorIcon");
            if (errorIcon && errorIcon.length) {
                $(errorIcon).attr("title", message);
                $(errorIcon).show();
            }
            $(fieldname).addClass("errorClass");
            return true;
        }

        return false;
    };

    $.each($('input.cmrate'), function (index, element) {
        $(element).keypress(function (e) {
            var currentValue = $(this).val();
            var selectionStart = $(this)[0].selectionStart;
            var fullContent = PH_BuildFullContentWithKeyPressEvent(currentValue, event.key, selectionStart);
            if (isNaN(Number(fullContent))) {
                event.preventDefault();
            }
        });
    });

    $("#CommissionRateCRDebitRate").focusout(function () {

        var fieldname = "#CommissionRateCRDebitRate";
        var ratename = "Debit rate";
        var placement = "bottom";

        var isDollarChecked = IsDebitDollarChecked();
        var subBusinessType = $('#CommissionSubBusinessType :selected').text();
        
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "18": //Live Eftpos
                if (isDollarChecked) {
                    if (subBusinessType == "Retail" || subBusinessType == "Virtual Terminal") {
                        result = validateAmount(fieldname, -2, 0, isDollarChecked);
                    }
                    else {
                        result = validateAmount(fieldname, -2, -0.1, isDollarChecked);
                    }
                } else {
                    result = validateAmount(fieldname, -5, 0, isDollarChecked);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, -0.5, isDollarChecked);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2, isDollarChecked);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, -0.5, isDollarChecked);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "26": //Live Eftpos Integrated
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    if (subBusinessType == "Live eftpos int" || subBusinessType == "Virtual Terminal") {
                        result = validateAmount(fieldname, -2, 0, isDollarChecked);
                    }
                    else {
                        result = validateAmount(fieldname, -2, -0.1, isDollarChecked);
                    }
                } else {
                    if (subBusinessType == "Live eftpos int" || subBusinessType == "Virtual Terminal") {
                        result = validateAmount(fieldname, -5, 0, isDollarChecked);
                    }
                    else
                        result = validateAmount(fieldname, -5, -0.5, isDollarChecked);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, -0.5, isDollarChecked);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                //Check percentice or dollar
                //if dollar, from -2 to 0
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0, isDollarChecked);
                } else {
                    result = validateAmount(fieldname, -3, 0, isDollarChecked);
                }
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);

    });

    $("#CommissionRateCRVisaRate").focusout(function () {
        var fieldname = "#CommissionRateCRVisaRate";
        var ratename = "Visa rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRVisaDebitRate").focusout(function () {
        var fieldname = "#CommissionRateCRVisaDebitRate";
        var ratename = "Visa debit rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
            case "20": //TaxiPro
            case "21": //TaxiApp
            case "22": //LiveEftpos black
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
            case "28": //LiveSMS
                result = validateAmount(fieldname, 0, 0);
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRMasterCardRate").focusout(function () {

        var fieldname = "#CommissionRateCRMasterCardRate";
        var ratename = "Master Card rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRMasterCardDebitRate").focusout(function () {

        var fieldname = "#CommissionRateCRMasterCardDebitRate";
        var ratename = "Master Card debit rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
            case "20": //TaxiPro
            case "21": //TaxiApp
            case "22": //LiveEftpos black
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
            case "28": //LiveSMS
                result = validateAmount(fieldname, 0, 0);
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRAmexRate").focusout(function () {

        var fieldname = "#CommissionRateCRAmexRate";
        var ratename = "Amex rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRDinersRate").focusout(function () {

        var fieldname = "#CommissionRateCRDinersRate";
        var ratename = "Diners rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRQantasRate").focusout(function () {

        var fieldname = "#CommissionRateCRQantasRate";
        var ratename = "Qantas rate";
        var placement = "bottom";
        var isDollarChecked = IsQantasDollarChecked();
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "18": //Live Eftpos
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0, isDollarChecked);
                } else {
                    result = validateAmount(fieldname, -5, 0, isDollarChecked);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0, isDollarChecked);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2, isDollarChecked);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "22": //LiveEftpos black
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0, isDollarChecked);
                } else {
                    result = validateAmount(fieldname, -5, 0, isDollarChecked);
                }
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5, isDollarChecked);
                break;
            case "26": //Live Eftpos Integrated
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0, isDollarChecked);
                } else {
                    result = validateAmount(fieldname, -5, 0, isDollarChecked);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0, isDollarChecked);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0, isDollarChecked);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRUnionPayRate").focusout(function () {
        var fieldname = "#CommissionRateCRUnionPayRate";
        var ratename = "UnionPay rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRZipPayRate").focusout(function () {

        var fieldname = "#CommissionRateCRZipPayRate";
        var ratename = "ZipPay rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -6, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRAlipayRate").focusout(function () {

        var fieldname = "#CommissionRateCRAlipayRate";
        var ratename = "Alipay rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    $("#CommissionRateCRJCBRate").focusout(function () {

        var fieldname = "#CommissionRateCRJCBRate";
        var ratename = "JCB rate";
        var placement = "right";
        var result = false;
        switch (businessID) {
            case "1": //TaxiEpay
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
            case "20": //TaxiPro
            case "21": //TaxiApp
            case "22": //LiveEftpos black
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
            case "28": //LiveSMS
                result = validateAmount(fieldname, 0, 5);
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, businessID, ratename, placement, result);
    });

    ResetDefaultTranFee = function () {
        var businessTypeId = $("#BusinessType").val();
        var tranFee = LG_GetDefaultValueForTranFeeByBusinessType(businessTypeId);

        ApplyInfoForTranFees(
            tranFee.DebitRate,
            tranFee.Visa,
            tranFee.VisaDebit,
            tranFee.Master,
            tranFee.MasterDebit,
            tranFee.Amex,
            tranFee.Diners,
            tranFee.UnionPay,
            tranFee.Zip,
            tranFee.Qantas,
            tranFee.Alipay,
            tranFee.JCB
        );
    }

    ApplyInfoForTranFees = function (debit, visa, visaDebit, master, masterDebit, amex, diners, union, zip, qantas, alipay, jcb) {
        $("#CommissionRateCRDebitTranFee").val(debit);
        $("#CommissionRateCRVisaTranFee").val(visa);
        $("#CommissionRateCRVisaDebitTranFee").val(visaDebit);
        $("#CommissionRateCRMasterCardTranFee").val(master);
        $("#CommissionRateCRMasterCardDebitTranFee").val(masterDebit);
        $("#CommissionRateCRAmexTranFee").val(amex);
        $("#CommissionRateCRDinersTranFee").val(diners);
        $("#CommissionRateCRUnionPayTranFee").val(union);
        $("#CommissionRateCRZipPayTranFee").val(zip);
        $("#CommissionRateCRQantasTranFee").val(qantas);
        $("#CommissionRateCRAlipayTranFee").val(alipay);
        $("#CommissionRateCRJCBTranFee").val(jcb);
    }

    validateTranFee = function (fieldname, minValue, maxValue, NumOfDecimalDigit) {
        $(fieldname).tooltip('destroy');
        $(fieldname).removeClass("errorClass");
        $(fieldname).attr("data-original-title", "");
        $(fieldname).attr("title", "");

        const errorIcon = $(fieldname).parent().find(".errorIcon");
        if (errorIcon && errorIcon.length) {
            $(errorIcon).attr("title", "");
            $(errorIcon).hide();
        }

        const fieldValue = $(fieldname).val();

        if (!fieldValue) {
            return false;
        }

        const regularExpress = "^(\-?[0-9]+(.[0-9]{1," + NumOfDecimalDigit + "})?)$";
        const re = new RegExp(regularExpress);

        if (fieldValue < minValue || fieldValue > maxValue || !re.test(fieldValue)) {
            const message = "Inputted amount must be between " + minValue + " to " + maxValue + " and maximum " + NumOfDecimalDigit + " digits after decimal points.";
            $(fieldname).attr("title", message);

            if (errorIcon && errorIcon.length) {
                $(errorIcon).attr("title", message);
                $(errorIcon).show();
            }
            $(fieldname).addClass("errorClass");
            return true;
        }

        return false;
    };

    $.each($('input.TranFee'), function (index, element) {
        $(element).keypress(function (e) {
            var currentValue = $(this).val();
            var selectionStart = $(this)[0].selectionStart;
            var fullContent = PH_BuildFullContentWithKeyPressEvent(currentValue, event.key, selectionStart);
            if (isNaN(Number(fullContent))) {
                event.preventDefault();
            }
        });
    });

    $.each($('input.TranFee'), function (index, element) {
        $(element).focusout(function () {
            var fieldname = "#" + this.id;
            var ratename = "JCB rate";
            var placement = "right";
            var result = false;

            switch (businessID) {
                case "1": //TaxiEpay
                case "18": //Live Eftpos
                case "19": //DirectLiveEftpos
                case "20": //TaxiPro
                case "21": //TaxiApp
                case "22": //LiveEftpos black
                case "23": //LiveEftpos black for taxi
                case "24": //Live limo
                case "25": //Glide
                case "26": //Live Eftpos Integrated
                case "27": //Live Eftpos Black Integrated
                case "28": //LiveSMS
                case "29": //CSA Integrated
                    result = validateTranFee(fieldname, -1, 0, 4);
                    break;
            }
        });
    });

    $("#PricingPlan").change(function (e, f) {
        var pricingPlanId = $("select#PricingPlan :selected").val();
        var subBusinessTypeId = $("#CommissionSubBusinessType").val();

        var pricingPlans = listPricingPlanTemplate.filter(p => p.PlanId == pricingPlanId);

        if (pricingPlans.length > 0) {
            var pricingPlan = pricingPlans[0];

            var debitRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 6)[0];
            var visaRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 1)[0];
            var visaDebitRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 24)[0];
            var masterCardRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 2)[0];
            var masterCardDebitRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 25)[0];
            var unionPayRate = pricingPlan.Rates.filter(r => r.PayMethodKey == 17)[0];

            var CRVisaRate = $('#CommissionRateCRVisaRate');
            var CRVisaDebitRate = $('#CommissionRateCRVisaDebitRate');
            var CRMasterCardRate = $('#CommissionRateCRMasterCardRate');
            var CRMasterCardDebitRate = $('#CommissionRateCRMasterCardDebitRate');
            var CRUnionPayRate = $('#CommissionRateCRUnionPayRate');
            var CRDebitRate = $('#CommissionRateCRDebitRate');
            var CRDebitTranFee = $('#CommissionRateCRDebitTranFee');
            var CRVisaTranFee = $('#CommissionRateCRVisaTranFee');
            var CRVisaDebitTranFee = $('#CommissionRateCRVisaDebitTranFee');
            var CRMasterCardTranFee = $('#CommissionRateCRMasterCardTranFee');
            var CRMasterCardDebitTranFee = $('#CommissionRateCRMasterCardDebitTranFee');
            var CRUnionPayTranFee = $('#CommissionRateCRUnionPayTranFee');

            $('select#IsFixedRateDebit').val(debitRate.IsRateFixed ? 'Dollar' : 'Percentage');

            CRVisaRate.val(visaRate.Rate);
            CRVisaDebitRate.val(visaDebitRate.Rate);
            CRMasterCardRate.val(masterCardRate.Rate);
            CRMasterCardDebitRate.val(masterCardDebitRate.Rate);
            CRUnionPayRate.val(unionPayRate.Rate);
            CRDebitRate.val(debitRate.Rate);

            CRVisaTranFee.val(visaRate.TransactionFee);
            CRVisaDebitTranFee.val(visaDebitRate.TransactionFee);
            CRMasterCardTranFee.val(masterCardRate.TransactionFee);
            CRMasterCardDebitTranFee.val(masterCardDebitRate.TransactionFee);
            CRUnionPayTranFee.val(unionPayRate.TransactionFee);
            CRDebitTranFee.val(debitRate.TransactionFee);

            CRVisaRate.trigger('focusout');
            CRVisaDebitRate.trigger('focusout');
            CRMasterCardRate.trigger('focusout');
            CRMasterCardDebitRate.trigger('focusout');
            CRUnionPayRate.trigger('focusout');
            CRDebitRate.trigger('focusout');
            CRDebitTranFee.trigger('focusout');
            CRVisaTranFee.trigger('focusout');
            CRVisaDebitTranFee.trigger('focusout');
            CRMasterCardTranFee.trigger('focusout');
            CRMasterCardDebitTranFee.trigger('focusout');
            CRUnionPayTranFee.trigger('focusout');
        }
    });

    $.each(payMethodAllowPricingPlan, function (index, element) {
        $('#' + element).change(function () {
            $("#PricingPlan").val(-1);
        })
    });

    $.each($('select[id^="IsFixedRate"]'), function (index, element) {
        $(element).change(function () {
            $("#PricingPlan").val(-1);
        })
    });

    BindingPricingPlanComboBox = function () {
        var subBusinessTypeId = $("#CommissionSubBusinessType").val();
        var pricingPlanComboBox = $('select#PricingPlan');
        pricingPlanComboBox.empty();
        pricingPlanComboBox.append(new Option('Custom', -1));

        if (subBusinessTypeId) {
            $.ajax({
                url: baseURL + "Member/GetPricingPlan",
                data: { subBusinessId: subBusinessTypeId },
                type: "GET",
                success: function (result) {
                    if (result.IsError) {
                        Util.MessageDialog(result.ErrorMessage);
                    } else {
                        listPricingPlanTemplate = result.PricingPlanList;

                        $.each(result.PricingPlanList, function (index, el) {
                            pricingPlanComboBox.append(new Option(el.PlanName, el.PlanId));
                        });

                        pricingPlanComboBox.val(-1);
                    }
                }
            });
        }
    }

    $('#CommissionSubBusinessType').change(function (e){
        CommissionSubBusinessTypeChange(e.currentTarget.selectedOptions[0].text);
    });

    CommissionSubBusinessTypeChange = function(subBusinessType)
    {
        if (businessID == "26") {//Live eftpos integrated
            switch (subBusinessType) {
                case 'Live eftpos int':
                case 'Virtual Terminal':
                    $('#IsFixedRateDebit').val('Dollar');
                    $('#IsFixedRateDebit').prop('disabled', false);
                    break;
                case 'eCommerce':
                case 'Live local':
                    $('#IsFixedRateDebit').val('Dollar');
                    $('#IsFixedRateDebit').prop('disabled', true);
                    break;
            }
        }
    }
});