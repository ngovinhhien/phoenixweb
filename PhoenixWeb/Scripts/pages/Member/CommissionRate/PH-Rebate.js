﻿var baseURL = Util.PH_UpdateURL();
var businessID = "";
var purWithCashOutDefault = 0.1595;
var cashOutAdvanceDefault = 0.12;
var purWithCashOutMin = 0.00;
var purWithCashOutMax = 1.00
var rebateJSONDatas = $('#RebatesDtoAsJson');
var isEditRebateAmount = false;


$(document).ready(function () {
    $('#OpenRebatePopup').on('click', () => {
        OpenRebatePopup();
    });

    $('#SaveRebate').on('click', () => {
        SaveRebate();
    });

    $('#CloseRebateDialog').on('click', () => {
        CloseRebateDialog();
    });

    $('#ResetDefaultRebate').on('click', () => {
        $('#CRDebitRebateForPurWithCashOut').val(purWithCashOutDefault);
        $('#CRDebitRebateForCashAdvance').val(cashOutAdvanceDefault);

        ResetDialog();
    });

    $.each($('.Rebate'), (index, elm) => {
        $(elm).keypress(function (e) {
            var currentValue = $(this).val();
            var selectionStart = $(this)[0].selectionStart;
            var fullContent = PH_BuildFullContentWithKeyPressEvent(currentValue, event.key, selectionStart);
            if (isNaN(Number(fullContent))) {
                event.preventDefault();
            }
        });

        $(elm).focusout(function () {
            var fieldname = '#' + elm.id;
            validateAmountRebate(fieldname, purWithCashOutMin, purWithCashOutMax, 4);
        });
    });

    OpenRebatePopup = function () {
        var businessType = $("#BusinessType").val();
        if (businessType == 0) {
            alert("Please select Business Type");
            return;
        }

        ResetDialog();

        var windowTitle = (isEditRebateAmount ? "Edit" : "Add") + " Rebate Amount";
        $('#SaveRebate').text(isEditRebateAmount ? 'Save' : 'Add');

        var popupViewDetailLog = $('#memberRebateDialog').dialog({
            title: windowTitle,
            position: "center",
            width: 600,
            modal: true,
            draggable: true,
            drag: function (event, ui) {
                var scrollTopPx = $(document).scrollTop();
                var positionUI = ui.position;
                positionUI.top = positionUI.top - scrollTopPx;
                $(this).closest(".ui-dialog").css("top", positionUI.top);
            }
        });
        popupViewDetailLog.dialog('option', { 'resizable': false });

        var RebateForPurWithCashOutCurValue = purWithCashOutDefault;
        var RebateForCashAdvanceValue = cashOutAdvanceDefault;
        var rebateDatas = JSON.parse(rebateJSONDatas.val()) || [];

        if (rebateDatas.length !== 0 && !isNaN(rebateDatas[0].CRDebitRebateForPurchaseWithCashOut)) {
            RebateForPurWithCashOutCurValue = rebateDatas[0].CRDebitRebateForPurchaseWithCashOut;
        }

        if (rebateDatas.length !== 0 && !isNaN(rebateDatas[0].CRDebitRebateForCashAdvance)) {
            RebateForCashAdvanceValue = rebateDatas[0].CRDebitRebateForCashAdvance;
        }

        $('#CRDebitRebateForPurWithCashOut').val(RebateForPurWithCashOutCurValue);
        $('#CRDebitRebateForCashAdvance').val(RebateForCashAdvanceValue);
    };

    CloseRebateDialog = function() {
        ResetDialog();
        $("#memberRebateDialog").dialog("close");
    };

    RenderRebatesGrid = function() {
        var commisionRateDatas = JSON.parse(rebateJSONDatas.val()) || [];
        var rebateDatas = [];

        if (commisionRateDatas.length > 0 && (!isNaN(commisionRateDatas[0].CRDebitRebateForPurchaseWithCashOut) || !isNaN(commisionRateDatas[0].CRDebitRebateForCashAdvance))) {
            if (!isNaN(commisionRateDatas[0].CRDebitRebateForPurchaseWithCashOut)) {
                rebateDatas.push({
                    DebitPurchaseWithCashOutText: 'Purchase with cash out',
                    DebitPurchaseWithCashOutValue: "<b>$" +
                        commisionRateDatas[0].CRDebitRebateForPurchaseWithCashOut +
                        "</b> /transaction"
                });
            }
            if (commisionRateDatas[0].CRDebitRebateForCashAdvance != null && !isNaN(commisionRateDatas[0].CRDebitRebateForCashAdvance)) {
                rebateDatas.push({
                    DebitPurchaseWithCashOutText: 'Cash Advance',
                    DebitPurchaseWithCashOutValue: "<b>$" +
                        commisionRateDatas[0].CRDebitRebateForCashAdvance +
                        "</b> /transaction"
                });
            }

            isEditRebateAmount = true;
            $('#OpenRebatePopup').text('Edit');

            $('#RebateGrid').html($('#RebateGridTemplate').render({ data: rebateDatas }));
        } else {
            rebateDatas.push({
                DebitPurchaseWithCashOutText: 'Rebate amount has not been configured.',
                DebitPurchaseWithCashOutValue: ""
            });
            $('#RebateGrid').html($('#RebateGridTemplate').render({ data: rebateDatas }));
        }
    };

    SaveRebate = function () {
        var DebitRebateForPurWithCashOut = parseFloat($('#CRDebitRebateForPurWithCashOut').val());
        var DebitRebateForCashAdvance = parseFloat($('#CRDebitRebateForCashAdvance').val());
        var RebateDatas = JSON.parse(rebateJSONDatas.val()) || [];
        
        if (isNaN(DebitRebateForPurWithCashOut) || isNaN(DebitRebateForCashAdvance))
        {
            $('div#errorMess').show();
            return;
        }

        if ($('input.Rebate.errorClass').length > 0) {
            $('input.Rebate.errorClass')[0].focus();
            return;
        }

        if (RebateDatas.length === 0) {
            RebateDatas.push({
                CRDebitRebateForPurchaseWithCashOut: DebitRebateForPurWithCashOut,
                CRDebitRebateForCashAdvance: DebitRebateForCashAdvance,
                IsEdited: true
            });
        }
        else {
            RebateDatas[0].CRDebitRebateForPurchaseWithCashOut = isNaN(DebitRebateForPurWithCashOut) ? RebateDatas[0].CRDebitRebateForPurchaseWithCashOut : DebitRebateForPurWithCashOut;
            RebateDatas[0].CRDebitRebateForCashAdvance = isNaN(DebitRebateForCashAdvance) ? RebateDatas[0].CRDebitRebateForCashAdvance : DebitRebateForCashAdvance;
            RebateDatas[0].IsEdited = true;
        }

        rebateJSONDatas.val(JSON.stringify(RebateDatas));

        RenderRebatesGrid();
        CloseRebateDialog();
    }

    validateAmountRebate = function (fieldname, minValue, maxValue, NumOfDecimalDigit) {
        $(fieldname).tooltip('destroy');
        $(fieldname).removeClass("errorClass");
        $(fieldname).attr("data-original-title", "");
        $(fieldname).attr("title", "");

        const errorIcon = $(fieldname).parent().find(".errorIcon");
        if (errorIcon && errorIcon.length) {
            $(errorIcon).attr("title", "");
            $(errorIcon).hide();
        }

        const fieldValue = $(fieldname).val();

        if (!fieldValue) {
            return false;
        }

        const regularExpress = "^(\-?[0-9]+(.[0-9]{1," + NumOfDecimalDigit + "})?)$";
        const re = new RegExp(regularExpress);

        if (fieldValue < minValue || fieldValue > maxValue || !re.test(fieldValue)) {
            const message = "Inputted amount must be between " + minValue + " to " + maxValue + " and maximum " + NumOfDecimalDigit + " digits after decimal points.";
            $(fieldname).attr("title", message);

            if (errorIcon && errorIcon.length) {
                $(errorIcon).attr("title", message);
                $(errorIcon).show();
            }
            $(fieldname).addClass("errorClass");
            return true;
        }

        return false;
    };
    
    ResetDialog = function (){
        $('input.errorClass').removeClass('errorClass');
        $('img.errorIcon').hide();
        $('div#errorMess').hide();
    };

    ShowHideRebateSection = function(BusinessTypeVal) {
        var businessAllowRebate = $('#BusinessKeysAllowRebate').val().split(';');

        if (businessAllowRebate.indexOf(BusinessTypeVal) < 0) {
            $('#RebateSection').hide();
        } else {
            $('#RebateSection').show();
        }
    };
});

