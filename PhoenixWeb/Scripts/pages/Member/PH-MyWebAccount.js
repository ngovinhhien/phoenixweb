﻿$(function () {
    var baseUrl = Util.PH_UpdateURL();

    $('#btnGhost').on('click', function () {
        $.ajax({
            url: baseUrl + "Member/GhostMyWebAccount",
            type: 'POST',
            data: { memberKey: $("#Member_key").val() },
            success: function (result) {
                if (result.IsError) {
                    alert(result.Message);
                } else {
                    window.open(result.Url, '_blank');
                }
            }
        });
    });

});
