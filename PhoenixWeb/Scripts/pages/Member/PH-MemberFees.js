﻿var baseURL = Util.PH_UpdateURL();
var data = {
    page: 1,
    records: 0,
    rows: [],
    total: 0
};
var isValid = true;

$(document).ready(function () {

    RenderFees = function (tblContainName) {
        var feeDatas = [{
            Amount: 0,
            FeeChargeEndDate: null,
            FeeChargeStartDate: null,
            FrequencyID: 1,
            ID: 1,
            FeeID: 1,
            IsEnable: true
        }];
        var feeTypeDatas = JSON.parse($("#feeTypes")[0].value) || [];
        var data = {
            fees: feeDatas,
            types: feeTypeDatas
        };
        var recordHtml = $("#tmplFeeList").render({ data: data });
        $("#" + tblContainName).html(recordHtml);
    }

    validateAmount = function (el) {
        if (el.value == "") {
            $("#error_fee_" + el.dataset.id).text("Amount is required.");
            isValid = false;
            return;
        }
        el.value = parseFloat($("#" + el.id).asNumber().toFixed(2));
        var feeTypeData = JSON.parse($("#feeTypes").val()) || [];
        var type = feeTypeData.filter(function (type) {
            return type.ID == el.dataset.feeid;
        });
        if (el.value > type[0].MaxAmount || el.value < type[0].MinAmount) {
            $("#error_fee_" + el.dataset.id).text("Amount must be between " + type[0].MinAmount + " and " + type[0].MaxAmount + ".");
            isValid = false;
        }
        else {
            $("#error_fee_" + el.dataset.id).text("");
            isValid = true;
        }
        $("#" + el.id).formatCurrency();
    }

    customActionForMerchantFormatter = function (MemberId, options, rowObject) {
        var html = "";
        html = html + "<img class='icon' src='" + baseURL + "/Images/delete.png' onClick='RemoveMember(\"" + MemberId + "\")' /> ";
        return html;
    }

    $("#memberGrid").jqGrid({
        url: "",
        colNames: [
            "Member ID", "First Name", "Last Name", "Trading Name", "Email", "Action"
        ],
        colModel: [
            { key: false, name: "MemberId", index: "MemberId", editable: false, sortable: false },
            { key: false, name: "FirstName", index: "FirstName", editable: false, sortable: false },
            { key: false, name: "LastName", index: "LastName", editable: false, sortable: false },
            { key: false, name: "TradingName", index: "TradingName", editable: false, sortable: false },
            { key: false, name: "Email", index: "Email", editable: false, sortable: false },
            {
                key: false,
                name: "MemberId",
                index: "MemberId",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customActionForMerchantFormatter
            }
        ],
        //pager: "#memberGridPager",
        //rowNum: 10,
        loadui: "disable",
        //rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false,
    });

    var grid = $("#memberGrid")[0];
    $("#memberGrid").jqGrid('clearGridData');
    grid.addJSONData(data);

    $("#btnAddMember").on("click",
        function () {
            if ($('#MemberInfo').val() == "") {
                showDialogError("Please enter Member Id.");

                return false;
            }
            LG_ShowProgressMask();
            AddMemberForFee($('#MemberInfo').val())
                .done(function (result) {
                    LoadMemberData(result.records, 1);
                    if (result.addedError != "") {
                        Util.MessageDialog(result.addedError.split("\n"), "Error");
                    }
                    LG_HideProgressMask();
                });
        });

    $("#btnSave").on("click",
        function () {
            //var startDate = $('#startDate').val();
            if (data.rows.length <= 0) {
                showDialogError("Please add Member.");
                return false;
            }
            //if (startDate == "") {
            //    showDialogError("Please enter StartDate.");
            //    return false;
            //}

            //----validate fee type----------//
            var feeTypeDatas = JSON.parse($("#feeTypes")[0].value) || [];
            var feeIdValue = $('#ddl_Fee_' + feeTypeDatas[1].ID).val();
            if (feeIdValue == 0) {
                $('#error_feeType_' + feeTypeDatas[1].ID).text("Please select Fee Type");
                return false;
            }
            else {
                $('#error_feeType_' + feeTypeDatas[1].ID).text("");
            }
            ///////////////////////////////////

            //----validate amount----------//
            var amountTxt = $('#amount_' + feeTypeDatas[1].ID);
            if (amountTxt.val() == "") {
                $("#error_fee_" + feeTypeDatas[1].ID).text("Amount is required.");
                return false;
            }
            var amountValue = parseFloat($('#amount_' + feeTypeDatas[1].ID).asNumber().toFixed(2));
            if (amountValue > feeTypeDatas[1].MaxAmount || amountValue < feeTypeDatas[1].MinAmount) {
                $("#error_fee_" + feeTypeDatas[1].ID).text("Amount must be between " + feeTypeDatas[1].MinAmount + " and " + feeTypeDatas[1].MaxAmount + ".");
                return false;
            }
            ///////////////////////////////////
            if (isValid) {
                LG_ShowProgressMask();
                var memberids = "";
                for (var i = 0; i < data.rows.length; i++) {
                    memberids += data.rows[i].MemberId + ",";
                }
                //AddFeeDetailMultipleMembers(feeIdValue, amountValue, memberids, startDate, endDate)
                AddFeeDetailMultipleMembers(feeIdValue, amountValue, memberids)
                    .done(function (result) {
                        $("#memberGrid").jqGrid('clearGridData');
                        $("#MemberInfo").attr("value", "");
                        data = {
                            page: 1,
                            records: 0,
                            rows: [],
                            total: 0
                        };
                        LG_HideProgressMask();
                        var errorIds = memberids.split(',').filter(function (item) {
                            return item != "";
                        });
                        var message = errorIds.length > 1 ? errorIds.join(", ") : errorIds[0];
                        $('#dialog-error').show();
                        $('#error-message').append("Account Fee has been added to the merchant(s) with MID(s) " + message + " successfully.");
                        showDialogMessage("Successfully", "350px")
                    });
            }
        });

    //AddFeeDetailMultipleMembers = function (feeTypeId, amount, memberIds, startDate, endDate) {
    AddFeeDetailMultipleMembers = function (feeTypeId, amount, memberIds) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Member/AddFeeDetailMultipleMembers",
            type: "POST",
            //data: { feeTypeId: feeTypeId, amount: amount, memberIds: memberIds, startDate: startDate, endDate: endDate },
            data: { feeTypeId: feeTypeId, amount: amount, memberIds: memberIds },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    AddMemberForFee = function (MemberId) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Member/AddMemberForFee",
            type: "POST",
            data: { memberId: MemberId },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }


    LoadMemberData = function (records, page) {
        var totalRows = [];
        Array.prototype.push.apply(totalRows, data.rows);
        var newRecords = records.filter(function (rec) {
            if (totalRows.some(r => r.MemberId == rec.MemberId)) {
                return false;
            }
            return true;
        });
        Array.prototype.push.apply(totalRows, newRecords);
        var pageSize = $("#memberGrid").getGridParam("rowNum");
        var totalPages = Math.ceil(totalRows.length / pageSize);
        data.rows = totalRows;
        data.total = totalPages;
        data.records = totalRows.length; 
        var grid = $("#memberGrid")[0];
        $("#memberGrid").jqGrid('clearGridData');
        grid.addJSONData(data);
    };

    RemoveMember = function (MemberId) {
        var totalRows = [];
        Array.prototype.push.apply(totalRows, data.rows);
        totalRows = totalRows.filter(function (rec) {
            if (rec.MemberId == MemberId) {
                return false;
            }
            return true;
        });
        var pageSize = $("#memberGrid").getGridParam("rowNum");
        var totalPages = Math.ceil(totalRows.length / pageSize);
        data.rows = totalRows;
        data.total = totalPages;
        data.records = totalRows.length;

        var grid = $("#memberGrid")[0];
        $("#memberGrid").jqGrid('clearGridData');
        grid.addJSONData(data);
        $('#dialog-error').show();
        $('#error-message').append("MID " + MemberId + " deleted successfully");
        showDialogMessage("Successfully", "350px")
    };

    showDialogMessage = function (title, width) {
        $('#dialog-message').dialog({
            title: title,
            width: width,
            modal: true,
            drag: function (event, ui) {
                var scrollTopPx = $(document).scrollTop();
                var positionUI = ui.position;
                positionUI.top = positionUI.top - scrollTopPx;
                $(this).closest(".ui-dialog").css("top", positionUI.top);
            },
            open: function () {
                $(this).parent().find('.ui-dialog-titlebar-close').hide();
            },
            buttons: [{
                text: 'Close',
                click: function () {
                    $(this).dialog('close');
                    $('#dialog-error').hide();
                    $('#error-message').text("");
                },
                class: 'btn blue'
            }]
        });
    }

    showDialogError = function (message) {
        $('#dialog-error').show();
        $('#error-message').append(message);

        showDialogMessage("Error", "350px");
        LG_HideProgressMask();
    }
});
