﻿
var baseURL = Util.PH_UpdateURL();
$(document).ready(function() {
    $(function() {
        $('#allocateTerminalsTable li:first-child a').tab('show');
    });
});


EditVehicle = function (vehicleKey) {
    GetItemDataForVehiclePopup(vehicleKey).done(function (data) {

        BindingVehicleDataToPopup(data, vehicleKey);

        var popupViewDetailLog = $('#editVehicleDialog').dialog();
        popupViewDetailLog.dialog('option', { 'width': '70%', 'resizable': false });

        //initialize EffectiveDate datetime picker field
        $("#EffectiveDate").datetimepicker({
            format: 'd/m/Y h:m:s A',
            validateOnBlur: false
        });

    }).fail(function (message) {
        $("#error").text(message);
    });
};

GetItemDataForVehiclePopup = function (vehicleKey) {
    var deferred = $.Deferred();
    var businessType = $("#BusinessType").val();
    $.ajax({
        url: baseURL + "Member/GetItemDataForVehiclePopup",
        data: { businessType: businessType, vehicleKey: vehicleKey },
        type: "GET",
        success: function (result) {
            if (result.IsError) {
                deferred.reject(result.ErrorMessage);
            } else {
                deferred.resolve(result);
            }
        }
    });

    return deferred.promise();
}

BindingVehicleDataToPopup = function (data, vehicleKey) {
    var vehicleId = $("#vehicleId_" + vehicleKey).text().trim();
    var state = $("#state_" + vehicleKey).text().trim();
    var typeKey = $("#type_" + vehicleKey).data("key");
    
    $('#VehicleState').empty();
    $.each(data.VehicleStateList, function (i, el) {
        $('#VehicleState').append(new Option(el.Text, el.Value));
    })

    $('#VehicleType').empty();
    $.each(data.VehicleTypeList, function (i, el) {
        $('#VehicleType').append(new Option(el.Text, el.Value));
    })

    $("#VehicleId").val(vehicleId);
    $("#EffectiveDate").val(data.EffectiveDate);
    $("#VehicleState").val(state);
    $("#VehicleType").val(typeKey);
    $("#VehicleKey").val(vehicleKey);
    $("#error").text("");
}

SavingVehicleData = function (model) {
    var deferred = $.Deferred();
    $.ajax({
        url: baseURL + "Member/UpdateTerminal",
        data: JSON.stringify(model),
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        success: function (result) {
            if (typeof result === "object") {
                deferred.reject(result.ErrorMessage);
            } else {
                deferred.resolve(result);
            }
        }
    });
    return deferred.promise();
}



Submit = function () {
    if ($("#editVehicleForm").valid()) {
        var model = {
            VehicleId: $("#VehicleId").val(),
            RegisteredState: $("#VehicleState").val(),
            VehicleTypeKey: $("#VehicleType").val(),
            EffectiveDate: $("#EffectiveDate").val(),
            Vehicle_key: $("#VehicleKey").val(),
            MemberKey: $("#Member_key").val(),
        };

        SavingVehicleData(model).done(function (data) {
            $("#parentTerminalAllocationGrid").empty();
            $("#parentTerminalAllocationGrid").append(data);
            $("#editVehicleDialog").dialog('close');
        }).fail(function (message) {
            $("#error").text(message);
        });
    }
};

CloseMe = function() {
    $("#editVehicleDialog").dialog('close');
};

