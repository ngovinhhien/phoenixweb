﻿var baseURL = Util.PH_UpdateURL();

var searchObject = {
    PageIndex: '',
    PageSize: '',
    SortField: '',
    SortOrder: '',
    IsUseAutoComplete: false,
    originalAutoCompleteValue: ''
}

var filterParams = {};

$(document).ready(function () {

    var currentYear = new Date().getFullYear();
    Util.InitDatepicker('.datepicker', {
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        yearRange: (currentYear - 10) + ":" + (currentYear),
        allowClear: true,
    });
    Util.ValidateDateFromTo($("#CreatedDateFrom"), $("#CreatedDateTo"));

    $("#CompanyKey").change(function () {
        var BusinessTypeVal = $("#CompanyKey").val();
        $('#DealerKey').empty();
        if (BusinessTypeVal == "") {
            Util.InitializeDropdownList("#DealerKey", "All", null, null, null);
            return;
        }
        $.ajax({
            url: baseURL + 'AjaxAdmin/GetDealersByCompany',
            data: { CompanyKey: BusinessTypeVal },
            type: "GET",
            dataType: "json",
            success: function (data) {
                Util.InitializeDropdownList("#DealerKey", "All", data, "Dealer_key", "DealerName");
            },
            error: function () {
                Util.MessageDialog("Cannot load dealers.", "Error");
            }
        });
    });

    $("#keywordToggle").change(function () {
        ChangeSearchMethod(this.checked);
    });

    $("#reset").click(function () {
        $("#CompanyKey").val("");
        $('#DealerKey').empty();
        $('#DealerKey').append('<option value="">All</option>');
        $("#QBRStatusKey").val("");
        $("#PaymentKey").val("");
        $("#CreatedDateFrom").val("");
        $("#CreatedDateTo").val("");
        $("#SearchKeyword").val("");
        $('#chkActive').prop('checked', true);
        $('#chkActive').parent().addClass('checked')
    });

    $("#SearchKeyword").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: baseURL + 'AjaxAdmin/AutoCompleteSPMemberSearch',
                dataType: "json",
                data: {
                    term: request.term,
                    isActive: $("#chkActive")[0].checked
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        select: function (e, ui) {
            var filterParamsData = Util.getFilterParams('form');
            filterParamsData.IsUseAutoComplete = true;
            filterParamsData.originalAutoCompleteValue = ui.item.value;
            filterParamsData.IsSearchByKeyword = $("#keywordToggle")[0].checked;
            filterParamsData.IsActive = $("#chkActive")[0].checked == true ? "1" : "0";
            $.each(filterParamsData, function (param) {
                searchObject[param] = filterParamsData[param];
            });
            searchObject.SearchKeyword = searchObject.originalAutoCompleteValue;
            $("#SearchKeyword").val(ui.item.value);
            loadGridData();
        },
        open: function (e, ui) {
            var acData = $(this).data('ui-autocomplete');
            var len = acData.term.length;
            var lowerTerm = acData.term.toLowerCase();
            acData
                .menu
                .element
                .find('a')
                .each(function () {
                    var me = $(this);
                    var text = me.text();
                    var lowerText = text.toLowerCase();
                    var finalText = "";
                    var arr = [];
                    var index = lowerText.lastIndexOf(lowerTerm);
                    while (index >= 0) {
                        arr.push({ start: index, end: index + len });
                        lowerText = lowerText.substring(0, index);
                        index = lowerText.lastIndexOf(lowerTerm);
                    }
                    if (arr.length == 0) {
                        finalText = text;
                    } else {
                        var arrFinal = [];
                        for (var i = 0; i < arr.length; i++) {
                            arrFinal.push(text.substring(arr[i].end));
                            text = text.substring(0, arr[i].end);
                            arrFinal.push("<span class='ui-autocomplete-term'>" + text.substring(arr[i].start) + "</span>")
                            text = text.substring(0, arr[i].start);
                        }
                        arrFinal.push(text);
                        for (var i = arrFinal.length - 1; i >= 0; i--) {
                            finalText += arrFinal[i];
                        }
                    }
                    me.html(finalText);
                });
        }
    });
    Util.InitJqGrid(
        {
            colNames: BuildColName(),
            colModel: BuildColModel(),
            rowList: [20, 50, 100, 200],
            height: '500',
            hasPager: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            gridId: 'MemberSearchResult',
            allowSort: true,
            isFrozenColumn: true,
            gridComplete: function () {
                setTimeout(function () {
                    $("#MemberSearchResult").getGrid().resizeFrozenRows();
                }, 100);

            },
            resizeStop: function () {
                $("#MemberSearchResult").getGrid().resizeFrozenRows();
            }
        },
        searchObject,
        loadGridData);
    UpdateColumnTitle();
    $("#QBRStatusKey").val("");
    ChangeSearchMethod(true);
    $("#btnExport").prop('disabled', true);

});

function loadGridData() {
    LG_ShowProgressMask();

    MemberSearchService.Search(baseURL, searchObject)
        .done(function (result) {
            if (result.isError != true) {
                Util.ReloadDataJqgrid('#MemberSearchResultGrid', result, function () {
                    if (result.rows.length > 0) {
                        $("#btnExport").prop('disabled', false);
                    }
                    else {
                        $("#btnExport").prop('disabled', true);
                    }
                });
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            LG_HideProgressMask();
        });
}

function SearchMember(e) {
    e.preventDefault();

    if (Util.isBlank($("#SearchKeyword").val()) && $("#keywordToggle")[0].checked) {
        Util.MessageDialog("Please insert search keyword.", "Error");
        return;
    }

    filterParams = Util.getFilterParams('form');
    filterParams.IsSearchByKeyword = $("#keywordToggle")[0].checked;
    filterParams.IsActive = $("#chkActive")[0].checked == true ? "1" : "0";
    $.each(filterParams, function (param) {
        searchObject[param] = filterParams[param];
    });
    if (!searchObject.IsUseAutoComplete || $.trim(searchObject.originalAutoCompleteValue) != $.trim(searchObject.SearchKeyword)) {
        searchObject.IsUseAutoComplete = false;
    }

    searchObject.PageIndex = 1;
    searchObject.PageSize = $("#MemberSearchResult").getPageSize();
    loadGridData();
}


function ExportMemberSearched(e) {
    e.preventDefault();
    window.location.href =
        baseURL +
        "Member/PHWMemberList?SearchKeyword=" +
        searchObject.SearchKeyword +
        "&IsSearchByKeyword=" +
        searchObject.IsSearchByKeyword +
        "&CompanyKey=" +
        searchObject.CompanyKey +
        "&PaymentKey=" +
        searchObject.PaymentKey +
        "&DealerKey=" +
        searchObject.DealerKey +
        "&QBRStatusKey=" +
        searchObject.QBRStatusKey +
        "&CreatedDateFrom=" +
        searchObject.CreatedDateFrom +
        "&CreatedDateTo=" +
        searchObject.CreatedDateTo +
        "&IsActive=" +
        searchObject.IsActive +
        "&IsUseAutoComplete=" +
        searchObject.IsUseAutoComplete +
        "&SortField=" +
        searchObject.SortField +
        "&SortOrder=" +
        searchObject.SortOrder;
}

function BuildColName() {
    var colNames = [
        "Trading Name",
        "MID",
        "Dealer",
        "Contact details",
        "Business Type",
        "Created date",
        "Payment",
        "Status",
        "QBR No.",
        "EOM MSF",
        "Action"
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "TradingName", index: "TradingName", align: 'left', editable: false, sortable: false, frozen: true, classes: "grid-col-padding-left" },
        { key: false, name: "MemberId", index: "MemberId", align: 'left', width: 80, editable: false, sortable: true, classes: "grid-col-padding-left" },
        { key: false, name: "DealerName", index: "DealerName", align: 'left', editable: false, sortable: false, classes: "grid-col-padding-left" },
        {
            key: false, name: "Email", index: "Email", align: 'left', editable: false, sortable: false
            , formatter: function (cellValue, option, rowObject) {

                var email =
                    rowObject.Email == null
                        ? ""
                        : rowObject.Email
                    ;
                var mobile = rowObject.Mobile == null ? "" : rowObject.Mobile;
                if (email) {
                    return "<div style='display:inline-table;'>" + email + " </br>" + mobile + "</div > ";
                } else {
                    return "<div style='display:inline-table;'>" + mobile + "</div > ";
                }

            }, width: 200, classes: "grid-col-padding-left"
        },
        { key: false, name: "CompanyName", index: "CompanyName", align: 'left', editable: false, sortable: true, classes: "grid-col-padding-left" },
        {
            key: false, name: "CreatedDate", index: "CreatedDate", align: 'right', editable: false, sortable: true,
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y"
            }, width: 110, classes: "grid-col-padding-right"
        },
        { key: false, name: "PaymentType", index: "PaymentType", align: 'left', editable: false, sortable: false, classes: "grid-col-padding-left" },
        { key: false, name: "Active", index: "Active", align: 'left', width: 80, editable: false, sortable: false, formatter: function (cellvalue) { return cellvalue == true ? 'Active' : 'Inactive'; }, classes: "grid-col-padding-left" },
        {
            key: false, name: "QBRNumber", index: "QBRNumber", align: 'left', editable: false, sortable: true
            , formatter: function (cellValue, option, rowObject) {
                var html = "<div style='display:inline-table;'>";
                var qbrStatus = rowObject.IsDisableQBR == true ? 'Disabled' : 'Enabled';
                var qbrNumber = rowObject.QBRNumber == null ? '' : rowObject.QBRNumber;
                if (qbrNumber) {
                    return html + qbrNumber + "</br>" + qbrStatus + "</div > ";
                } else {
                    return html + qbrStatus + "</div > ";
                }
            }, width: 110, classes: "grid-col-padding-left"
        },
        { key: false, name: "EOMMSFPayment", index: "EOMMSFPayment", align: 'left', width: 80, classes: "grid-col-padding-left", editable: false, sortable: false, formatter: function (cellvalue) { return cellvalue == true ? 'Yes' : 'No'; } },
        { key: false, name: "", index: "", align: 'center', editable: false, sortable: false, formatter: addAction }
    ];

    return colModel;
}

function UpdateColumnTitle() {
    var colModels = BuildColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("MemberSearchResultGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("MemberSearchResultGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("MemberSearchResultGrid", col.name);
            Util.JQGridSetPaddingRight("MemberSearchResultGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("MemberSearchResultGrid", col.name);
            }
        }
    }
}

function addAction(cellValue, option, rowObject) {
    var htmlButtons =
        '<div>'
        + '<a href="/Member/Details?member_key=' + rowObject.MemberKey + '" target="_blank" style="margin-right:7px;"><img title="Member Details" class="action-icon" src="/assets/global/img/View.png" /></a>'
        + '<a href="/Member/Edit?member_key=' + rowObject.MemberKey + '" target="_blank" style="margin-right:7px;"><img title="Member Edit" class="action-icon" src="/assets/global/img/edit.png" /></a>'
        + ($("#pms_FraudMonitoringViewReport").val() ? '<a href="/FraudMonitoring/MemberReview?memberKey=' + rowObject.MemberKey + '" target="_blank" style="margin-right:7px;"><img title="Member Review" class="action-icon" src="/assets/global/img/icon-shield.svg" /></a>' : "")
        + '<button style="border: none; background-color: transparent;padding:0px;margin-right: 7px;" title="Add Notes"  onclick="OpenMemberNote(event,\'' + rowObject.MemberId + '\')" > <img class="action-icon" src="/assets/global/img/notes.png" /> </button>'
        + '<button style="border: none; background-color: transparent;padding:0px;"  onclick="OpenTerminalAllocation(event,\'' + rowObject.MemberId + '\')"> <img title="Terminal Allocations" class="action-icon" src="/assets/global/img/icon - dock.png" /> </button>'
        + '</div>';
    return htmlButtons;
}

function OpenTerminalAllocation(event, memberID) {
    event.preventDefault();
    LG_ShowProgressMask();
    MemberSearchService.getTerminalAllocation(baseURL, memberID)
        .done(function (result) {
            $("#dialog").empty();
            $("#dialog").append(result);
            LG_HideProgressMask();
        })
        .fail(function (err) {
            console.log(err);
            LG_HideProgressMask();
        });
}

function OpenMemberNote(event, memberID) {
    event.preventDefault();
    LG_ShowProgressMask();
    MemberSearchService.getMemberNotePage(baseURL, memberID)
        .done(function (result) {
            $("#dialog").empty();
            $("#dialog").append(result);
            LG_HideProgressMask();
            LG_HideProgressMask();
        })
        .fail(function (err) {
            LG_HideProgressMask();
        });
}

function OpenMemberDetails(event, memberKey) {
    event.preventDefault();
    window.location.href = baseURL + 'Member/Details?member_key=' + memberKey;
}

function OpenMemberEdit(event, memberKey) {
    event.preventDefault();
    window.location.href = baseURL + 'Member/Edit?member_key=' + memberKey;
}

function ChangeSearchMethod(isUsingSearchByKeyword) {
    $("#CompanyKey").prop('disabled', isUsingSearchByKeyword);
    $("#QBRStatusKey").prop('disabled', isUsingSearchByKeyword);
    $("#PaymentKey").prop('disabled', isUsingSearchByKeyword);
    $("#DealerKey").prop('disabled', isUsingSearchByKeyword);
    $("#SearchKeyword").prop('disabled', !isUsingSearchByKeyword);



    if (isUsingSearchByKeyword) {
        $('#keywordToggle').closest('.checker').addClass('inputchecked');
        $('#lblKeyword').removeClass('disableKeyword');
        $('#CreatedDateFrom').datepicker('disable');
        $('#CreatedDateTo').datepicker('disable');
        $('#CreatedDateFrom').css('background-color', '#eee');
        $('#CreatedDateTo').css('background-color', '#eee');
    }
    else {
        $('#keywordToggle').closest('.checker').removeClass('inputchecked');
        $('#lblKeyword').addClass('disableKeyword');
        $('#CreatedDateFrom').datepicker('enable');
        $('#CreatedDateTo').datepicker('enable');
        $('#CreatedDateFrom').css('background-color', 'white');
        $('#CreatedDateTo').css('background-color', 'white');
    }
}