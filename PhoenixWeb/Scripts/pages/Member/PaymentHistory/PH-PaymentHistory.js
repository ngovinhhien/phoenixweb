﻿var baseURL = Util.PH_UpdateURL();
var fromDatePicker = $("#PaymentDateFrom");
var toDatePicker = $("#PaymentDateTo");
var businessTypeID = $("#businessTypeID").val();
var businessTypeShowPaymentType = $("#BusinessKeysShowPaymentType").val().split(";");
var showPaymentType = businessTypeShowPaymentType.indexOf(businessTypeID) !== -1;
var paymentHistorySearchObject = {
    PageIndex: '1',
    PageSize: '10',
    SortField: 'PaymentDate',
    SortOrder: 'desc',
    FromDate: null,
    ToDate: null,
    ReferenceNo: null,
    MemberKey: memberKey
}

ViewPaymentDetail = function(transactionKey){
    LG_ShowProgressMask();
    
    PaymentHistoryService.ViewDetail(baseURL, transactionKey)
        .done(function (result) {
            if (result.isError != true) {
                var tabs = $('#PaymentHistoryDetailGrid').html($('#PaymentHistoryDetailTemplate').render(result.Data));
                var widthPopup = result.Data.PaymentDetail.IsShowTransferHistory ? 1200 : 700;
                var popupViewDetailLog = $('#PaymentHistoryDetailPopup').dialog({
                    title: "Payment Details",
                    position: "center",
                    width: widthPopup,
                    modal: true,
                    draggable: true,
                    drag: function (event, ui) {
                        var scrollTopPx = $(document).scrollTop();
                        var positionUI = ui.position;
                        positionUI.top = positionUI.top - scrollTopPx;
                        $(this).closest(".ui-dialog").css("top", positionUI.top);
                    }
                });
                popupViewDetailLog.dialog('option', { 'resizable': false });
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
        })
        .fail(function (err) {
        }).always(function (){
        LG_HideProgressMask();
    });
}

ClosePaymentDetail = function(){
    $("#PaymentHistoryDetailPopup").dialog("close");
}

function BuildPaymentHistoryColName() {
    return [
        "Payment Date",
        "Total Amount",
        "Payment Reference Number",
        "Payment Type",
        "Actions"
    ];
}

function BuildPaymentHistoryColModel() {
    return [
        {
            key: false,
            name: "PaymentDate",
            index: "PaymentDate",
            align: 'left',
            editable: false,
            sortable: false,
            classes: "grid-col-padding-left"
        },
        {
            key: false,
            name: "TotalAmountFormatted",
            index: "TotalAmountFormatted",
            align: 'right',
            editable: false,
            sortable: false,
            classes: "grid-col-padding-right"
        },
        {
            key: false,
            name: "LodgementReferenceNo",
            index: "LodgementReferenceNo",
            align: 'left',
            editable: false,
            sortable: false,
            classes: "grid-col-padding-left"
        },
        {
            key: false,
            name: "PaymentType",
            index: "PaymentType",
            align: 'left',
            editable: false,
            sortable: false,
            classes: "grid-col-padding-left",
            hidden: !showPaymentType
        },
        {
            key: false,
            name: "Action",
            index: "Action",
            align: 'left',
            editable: false,
            sortable: false,
            formatter: actionFormatter
        }
    ];
}

function actionFormatter(cellvalue, options, rowObject) {
    var LodgementReferenceNo = rowObject["LodgementReferenceNo"];
    var PayrunID = rowObject["PayRunId"];
    var DownloadHref = "ViewPaymentsReport?Member_key=" + memberKey + "&LodgementReferenceNo=" + LodgementReferenceNo + "&PayrunID=" + PayrunID;
    var actionElement = '<a href="' + DownloadHref + '" target="_blank" class="btn btn-default" role="button">Download</a>';
    
    if (rowObject["PaymentType"] != 'Cash'){
        actionElement = actionElement + '<button type="button" class="btn btn-default" onclick="ViewPaymentDetail(' + rowObject["TransactionKey"] + ')" role="button">View details</button>';
    }

    return actionElement;
}

function UpdatePaymentHistoryColumnTitle() {
    var colModels = BuildPaymentHistoryColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("PaymentHistoriesNewGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("PaymentHistoriesNewGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("PaymentHistoriesNewGrid", col.name);
            Util.JQGridSetPaddingRight("PaymentHistoriesNewGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("PaymentHistoriesNewGrid", col.name);
            }
        }
    }
}

function loadPaymentHistoriesGridData() {
    Util.RemoveGridMessage("PaymentHistoriesNew");
    Util.BlockjqGrid('#PaymentHistoriesNewGrid');
    var grid = $('#PaymentHistoriesNewGrid');
    grid.jqGrid('clearGridData');
    paymentHistorySearchObject.FromDate = fromDatePicker.val().replace(/[/.]/g, "-");
    paymentHistorySearchObject.ToDate = toDatePicker.val().replace(/[/.]/g, "-");
    paymentHistorySearchObject.ReferenceNo = encodeURI($('#PaymentReferenceNo').val());

    PaymentHistoryService.Search(baseURL, paymentHistorySearchObject)
        .done(function (result) {
            if (result.isError !== true) {
                if (result.Data) {
                    grid[0].addJSONData(result.Data);
                }
                else
                {
                    Util.ShowGridMessage("PaymentHistoriesNew", result.Message)
                }
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
        })
        .fail(function (err) {
        }).always(function (){
        Util.UnBlockjqGrid('#PaymentHistoriesNewGrid');
    });
}

function adjustSortIconAndAlignment(tableId) {
    var columns = BuildColModel();
    var gridId = tableId + 'Grid';
    for (var col of columns) {
        if (col.sortable == false) {
            var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
            $(cellSelector).find('span.s-ico').remove();
            $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            if (col.frozen) {
                var cellSelectorFrozen = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                $(cellSelectorFrozen).find('span.s-ico').remove();
                $(cellSelectorFrozen).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            }

        }
        if (col.align) {
            $("#" + gridId + "_" + col.name).addClass(col.align);
            $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
        }
    }

    //Disabled all sort icon
    $(".ui-grid-ico-sort").addClass("ui-state-disabled");
}

function initPaymentHistoriesGrid() {
    Util.InitJqGrid(
        {
            colNames: BuildPaymentHistoryColName(),
            colModel: BuildPaymentHistoryColModel(),
            rowNum: 10,
            rowList: [10, 20, 50, 100],
            height: '321',
            hasPager: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            autowidth: true,
            gridId: 'PaymentHistoriesNew',
            allowSort: false,
            autoencode: true,
            datatype: 'local'
        },
        paymentHistorySearchObject,
        loadPaymentHistoriesGridData);
    loadPaymentHistoriesGridData();
    UpdatePaymentHistoryColumnTitle();
    adjustSortIconAndAlignment("PaymentHistoriesNew");
}

function initSearchData()
{
    var currentDate = new Date();
    var currentYear = new Date().getFullYear();
    Util.InitDatepicker('.payment-datepicker', {
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        allowClear: false,
        yearRange: "2005:" + currentYear
    });

    Util.ValidateDateFromTo(fromDatePicker, toDatePicker);

    var fromDate = new Date(currentDate);
    fromDate.setDate(currentDate.getDate() - 7);

    fromDatePicker.datepicker("setDate", fromDate);
    toDatePicker.datepicker("setDate", currentDate);
    
    $('#PaymentHistoriesSearch').on('click', function(e){
        var fromDate = fromDatePicker.datepicker("getDate");
        var toDate = toDatePicker.datepicker("getDate");
        
        fromDate.setFullYear(fromDate.getFullYear() + 1);
        
        if (fromDate < toDate) {
            var grid = $('#PaymentHistoriesNewGrid');
            grid.jqGrid('clearGridData');
            Util.RemoveGridMessage("PaymentHistoriesNew");
            Util.ShowGridMessage("PaymentHistoriesNew", "The maximum date range that can be entered is one year.");
            return;
        }
        
        loadPaymentHistoriesGridData();
    });
}

initSearchData();

initPaymentHistoriesGrid();