﻿(function (PaymentHistoryService, $) {
    PaymentHistoryService.Search = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Member/GetPaymentHistory?',
            type: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            data: searchParams,
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    
    PaymentHistoryService.ViewDetail = function (baseURL, transactionKey) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Member/GetPaymentHistoryDetail?',
            type: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            data: {transactionKey: transactionKey},
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    
    return {
        Search: PaymentHistoryService.Search,
        ViewDetail: PaymentHistoryService.ViewDetail,
    };

}(window.PaymentHistoryService = window.PaymentHistoryService || {}, jQuery));