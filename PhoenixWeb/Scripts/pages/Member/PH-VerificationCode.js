﻿var urlOrigin = Util.PH_UpdateURL();

$("#frmVerificationPhone").validate({
    rules: {
        Mobile: {
            required: true,
            mobilematches: '04[0-9]{8}',
            minlength: 10,
            maxlength: 10
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element)
            .addClass('ValidationError')
            .removeClass("ValidationSuccess");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element)
            .addClass("ValidationSuccess")
            .removeClass("ValidationError");
    },
    errorPlacement: function (label, element) {
        //Add this method here to prevent function display error label.
        label.addClass('text-danger errorLabel ErrorLabelPosition ');
        label.css("padding-left", "15px");
        label.insertAfter(element);
    }
});



handleValidateCodeAndCreateMerchant = function () {
    if ($("#frmVerificationPhone").valid() && $("#phoneNumber").valid()) {
        isValidCode().done(function () {
            $('.close-modal').trigger("click");
            createMerchant();
        });
    }
};

sendVerifiedCode = function (phone, showProgressMask) {
    if (showProgressMask) {
        LG_ShowProgressMask();
    }
    $("#btnResendCode").attr("disabled", true);
    var url = urlOrigin + "Member/SendPhoneVerification";

    $.ajax({
        url: url,
        data: {
            mobile: phone
        },
        type: "POST"
    }).fail(function (err) {
        var message = "";
        if (err.state() === "rejected") {
            message = err.responseJSON ? err.responseJSON.Message : "Can not connect to API. Please contact to administrator";

        } else {
            message = err.statusText;
        }

        $("#popupValidateMessage").text(message);
        $("#btnResendCode").attr("disabled", false);
        LG_HideProgressMask();
        return false;
    }).success(function (result) {
        if (result.IsError) {
            $("#popupValidateMessage").text(result.Message);
            LG_HideProgressMask();
            $("#btnResendCode").attr("disabled", false);
            return false;
        }
        $("#popupValidateMessage").text("");
        $("#form-mobile").val($("#phoneNumber").val());
        LG_HideProgressMask();
        setCountDownTimer(30);
        return true;
    });
};

isValidCode = function () {
    var deferred = $.Deferred();

    LG_ShowProgressMask();
    var url = urlOrigin + "Member/ValidateVerificationCode";
    var code = $("#verificationCode").val();
    var phone = $("#phoneNumber").val();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            verificationCode: code,
            mobile: phone
        },
    }).done(function (result) {
        if (result.IsError) {
            $("#popupValidateMessage").text(result.Message);
            LG_HideProgressMask();
            
        } else {
            $("#popupValidateMessage").text("");
            createMerchant();
        }
    }).fail(function (err) {
        var message = "";
        if (err.state() === "rejected") {
            message = err.responseJSON ? err.responseJSON.Message : "Can not connect to API. Please contact to administrator";

        } else {
            message = err.statusText;
        }

        $("#popupValidateMessage").text(message);
        LG_HideProgressMask();
        return deferred.reject();
    });

    return deferred.promise();
};

handleChangePhoneCheckBox = function () {
    var isChecked = $('#isChangePhoneNumber').is(':checked');
    if (isChecked) {
        $("#phoneNumber").attr("disabled", false);
    } else {
        $("#phoneNumber").attr("disabled", true);
    }
}

setCountDownTimer = function (seconds) {
    $("#btnResendCode").attr("disabled", true);
    $("#countNumber").html("(" + seconds + ")");
    timer = setInterval(function () {
        seconds = seconds - 1;
        if (seconds >= 0) {
            $("#refeshIcon").addClass("glyphicon-refresh-animate");
            $("#countNumber").html("(" + seconds + ")");
        } else {
            $("#countNumber").html("");
            $("#btnResendCode");
            $("#btnResendCode").attr("disabled", false);
            $("#refeshIcon").removeClass("glyphicon-refresh-animate");
            clearInterval(timer);
        }
    }, 1000);
};

function createMerchant() {
    var url = urlOrigin + "Member/AddMerchant";

    LG_ShowProgressMask();
    $.ajax({
        url: url,
        type: "POST",
        data: {
            memberKey: $('#memberKeyHidden').val(),
            memberId: $('#memberIdHidden').val(),
            firstName: $('#firstNameHidden').val(),
            tradingName: $('#tradingNameHidden').val(),
            email: $('#emailHidden').val(),
            businessType: $('#businessTypeHidden').val()
        }
    }).done(function (result) {
        if (result && result.IsCompleted) {
            LG_HideProgressMask();
            $("#dialog").dialog("close");
            window.top.location.href = urlOrigin + "Member/Edit?member_key=" + result.MemberKey;
        } else {
            var message = result.Message || "";
            LG_HideProgressMask();
        }

    }).fail(function (err) {
        if (err.status === 401 && err.statusText === "Unauthorized") {
            LG_ClearSession();
            reAuthorize();
        }

        var message = "";
        if (err.state() === "rejected") {
            message = err.responseJSON ? err.responseJSON.Message : "Can not connect to API. Please contact to administrator";

        } else {
            message = err.statusText;
        }

        LG_HideProgressMask();
    });
}