﻿(function (terminalAllocateionService, $, undefined) {
    terminalAllocateionService.SendAPIKey = function(baseURL, terminalID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Member/SendAPIKey",
            type: "PATCH",
            data: { TerminalID: terminalID },
            success: function(result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    };

}(window.terminalAllocateionService = window.terminalAllocateionService || {}, jQuery));