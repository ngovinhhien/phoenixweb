﻿(function (MemberSearchService, $) {
    MemberSearchService.Search = function (baseURL, info) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Member/Search',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(info),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    MemberSearchService.getMemberNotePage = function (baseURL, memberid) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Member/MemberNotes/' + memberid,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    MemberSearchService.getTerminalAllocation = function (baseURL, memberid) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Member/SearchMemberTerminalAllocationDialog/' + memberid,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        Search: MemberSearchService.Search,
        getMemberNotePage: MemberSearchService.getMemberNotePage,
        getTerminalAllocation: MemberSearchService.getTerminalAllocation
    };

}(window.MemberSearchService = window.MemberSearchService || {}, jQuery));