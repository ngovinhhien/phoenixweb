﻿var baseURL = Util.PH_UpdateURL();
var memberNoteModel = undefined;

$(function () {
    $("#grdMemberNotes").jqGrid({
        url: '',
        colNames: ['MemberNote_key', 'Note Date', 'Warning', 'Note', 'Modified By User'],
        colModel: [
            { key: true, hidden: true, name: 'MemberNote_key', index: 'MemberNote_key', editable: false },
            { key: false, name: 'NoteDate', index: 'NoteDate', editable: false, formatter: 'date', formatoptions: { srcformat: "Y-m-d", newformat: "d/m/Y h:m" }, },
            { key: false, name: 'Warning', index: 'Warning', editable: false, formatter: customWarning, },
            { key: false, name: 'Note', index: 'Note', editable: false, },
            { key: false, name: 'ModifiedByUser', index: 'ModifiedByUser', editable: false, },
        ],
        loadui: "disable",
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false,
        pager: '#grdMemberNotesPager',
    });

    $('#grdMemberNotes').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdMemberNotes').getGridParam('page');
            var rowNum = $('#grdMemberNotes').getGridParam('rowNum');

            if (pgButton == "next_grdMemberNotesPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdMemberNotesPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdMemberNotesPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdMemberNotesPager") {
                currentPage = $('#grdMemberNotes').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='grdMemberNotesPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            
            if (currentPage > totalPage) {
                return;
            }

            memberNoteModel.CurrentPage = currentPage;
            memberNoteModel.PageSize = rowNum;

            loadMemberNotes();
        }
    });

    //Initialize load notes
    memberNoteModel = {
        CurrentPage: 1,
        PageSize: 10,
        MemberKey: $("#Member_key").val()
    };

    loadMemberNotes();
});

customWarning = function (cellvalue, options, rowObject) {
    var html = "";
    var res = Util.ConvertStringToBool(cellvalue) ? "Yes" : "No";
    html += "<span>" + res + "</span>";
    return html;
};

loadMemberNotes = function () {
    //display loading state
    $("#grdMemberNotes").closest(".ui-jqgrid").find('.loading').show();

    $.ajax({
        url: baseURL + "Member/GetMemberNotes",
        headers: {
            "Content-Type": "application/json"
        },
        type: "GET",
        data: {
            memberKey: memberNoteModel.MemberKey,
            currentPage: memberNoteModel.CurrentPage,
            pageSize: memberNoteModel.PageSize
        }
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdMemberNotes')[0];
        $('#grdMemberNotes').jqGrid('clearGridData');
        grid.addJSONData(result);

        //highlight WARNING row
        $("tr td[title='Yes']").parent().css("background", "#fff3cd")

        //hide loading state
        $("#grdMemberNotes").closest(".ui-jqgrid").find('.loading').hide();
    }).fail(function (err) {
        console.log(err);
    });
};