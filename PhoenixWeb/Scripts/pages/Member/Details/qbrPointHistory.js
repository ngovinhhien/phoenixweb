﻿var baseURL = Util.PH_UpdateURL();

var searchObject = {
    PageIndex: '',
    PageSize: '',
    SortField: '',
    SortOrder: '',
}

$(document).ready(function () {
    Util.InitJqGrid(
        {
            colNames: BuildColName(),
            colModel: BuildColModel(),
            rowList: [20, 50, 100, 200],
            height: '300',
            hasPager: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            enableAutoWidth: true,
            gridId: 'MemberDetailQBRPointHistorySearch',
            allowSort: true
        },
        searchObject,
        loadGridData);
    searchObject.PageIndex = 1;
    searchObject.PageSize = $("#MemberDetailQBRPointHistorySearch").getPageSize();
    UpdateColumnTitle();
    loadGridData();

});



function loadGridData() {
    searchObject["MemberID"] = $('#memberid').val();
    Util.BlockjqGrid('#MemberDetailQBRPointHistorySearchGrid');
    QBRService.SearchPointHistory(baseURL, searchObject)
        .done(function (result) {
            if (result.isError != true) {
                var grid = $('#MemberDetailQBRPointHistorySearchGrid');
                grid.jqGrid('clearGridData');
                if (result.rows.length > 0) {
                    grid[0].addJSONData(result);
                }
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
            Util.UnBlockjqGrid('#MemberDetailQBRPointHistorySearchGrid');
        })
        .fail(function (err) {
            Util.UnBlockjqGrid('#MemberDetailQBRPointHistorySearchGrid');
        });
}

function OpenDetailPopup(object) {
    QBRService.GetHistoryDetail(baseURL, object)
        .done(function (result) {
            $("#dialog").empty();
            $("#dialog").append(result);
        })
        .fail(function (err) {
        });
}

function BuildColName() {
    var colNames = [
        "",
        "MID",
        "Company Type",
        "Trading Name",
        "QBR No.",
        "Points",
        "Period",
        "Promotion Name",
        "File Name",
        "Submitted Date",
        "Status",
        "Action",
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "MemberReferenceEntrySummaryKey", index: "MemberReferenceEntrySummaryKey", align: 'left', hidden: true, editable: false, sortable: false, classes: "grid-col-padding-left" },
        { key: false, name: "MemberID", index: "MemberID", align: 'left', classes: "grid-col-padding-left", hidden: true, editable: false, sortable: false },
        { key: false, name: "CompanyName", index: "CompanyName", align: 'left', classes: "grid-col-padding-left", hidden: true, editable: false, sortable: true },
        { key: false, name: "TradingName", index: "TradingName", align: 'left', classes: "grid-col-padding-left", hidden: true, editable: false, sortable: true },
        { key: false, name: "QBRNumber", index: "QBRNumber", align: 'right', classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "Points", index: "Points", align: 'right', formatter: "number", formatoptions: { thousandsSeparator: ",", decimalPlaces: 0, defaultValue: '0' }, classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "Period", index: "Period", align: 'right', classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "PromotionName", index: "PromotionName", align: 'left', classes: "grid-col-padding-left", editable: false, sortable: false },
        { key: false, name: "FileName", index: "FileName", align: 'left', classes: "grid-col-padding-left", editable: false, sortable: true },
        {
            key: false, name: "SubmittedDate", index: "SubmittedDate", align: 'right', editable: false, sortable: true,
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y"
            }, classes: "grid-col-padding-right"
        },
        { key: false, name: "Status", index: "Status", align: 'left', classes: "grid-col-padding-left", editable: false, sortable: false },
        { key: false, title: false, name: "", index: "", align: 'center', editable: false, sortable: false, formatter: addAction }
    ];

    return colModel;
}


function UpdateColumnTitle() {
    var colModels = BuildColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("MemberDetailQBRPointHistorySearchGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("MemberDetailQBRPointHistorySearchGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("MemberDetailQBRPointHistorySearchGrid", col.name);
            Util.JQGridSetPaddingRight("MemberDetailQBRPointHistorySearchGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("MemberDetailQBRPointHistorySearchGrid", col.name);
            }

        }
    }
}

function addAction(cellValue, option, rowObject) {
    var htmlButtons =
        '<div>'
        + '<button class="btn-inline clickable"  onclick="MemberDetailOpenQBRDetailPopup(event, \'' + option.rowId + '\')"> View </button>'
        + '</div>';
    return htmlButtons;
}

function MemberDetailOpenQBRDetailPopup(event, rowid) {
    event.preventDefault();
    var summaryKey = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'MemberReferenceEntrySummaryKey');
    var memberId = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'MemberID');
    var tradingName = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'TradingName');
    var points = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'Points');
    var promotionName = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'PromotionName');
    var qbrNumber = $('#MemberDetailQBRPointHistorySearchGrid').jqGrid('getCell', rowid, 'QBRNumber');

    var object = {
        MemberReferenceSummaryKey: summaryKey,
        MemberId: memberId,
        TradingName: tradingName,
        Points: points,
        PromotionName: promotionName,
        QBRNumber: qbrNumber,
    };

    QBRService.GetHistoryDetail(baseURL, object)
        .done(function (result) {
            $("#dialog").empty();
            $("#dialog").append(result);
        })
        .fail(function (err) {
        });
}
