﻿(function (MemberDetailQBRService, $) {
    MemberDetailQBRService.QBRPointHistory = function (baseURL, memberid) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'QBR/MemberPointHistoryDialog/',
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    MemberDetailQBRService.GetMemberQBRNumberPointHistory = function (baseURL, info) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'QBR/GetMemberQBRNumberPointHistory',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(info),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }


    
    return {
        QBRPointHistory: MemberDetailQBRService.QBRPointHistory,
        GetMemberQBRNumberPointHistory: MemberDetailQBRService.GetMemberQBRNumberPointHistory
    };

}(window.MemberDetailQBRService = window.MemberDetailQBRService || {}, jQuery));