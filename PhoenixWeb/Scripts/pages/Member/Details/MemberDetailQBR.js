﻿var baseURL = Util.PH_UpdateURL();

var memberDetailQBRSearchObject = {
    PageIndex: '',
    PageSize: '',
    SortField: '',
    SortOrder: '',
}
function InitializeMemberQBRPointHistoryDialog() {
    var memberDetailQBRSearchObject = {};
    memberDetailQBRSearchObject["MemberKey"] = $('#Member_key').val();
    memberDetailQBRSearchObject.PageIndex = 1;
    memberDetailQBRSearchObject.PageSize = 20;
    MemberDetailQBRService.GetMemberQBRNumberPointHistory(baseURL, memberDetailQBRSearchObject)
        .done(function (result) {
            if (result.isError != true) {
                if (result.rows.length > 0) {
                    MemberDetailQBRService.QBRPointHistory(baseURL)
                        .done(function (data) {
                            $("#dialog").empty();
                            $("#dialog").append(data);
                            InitMemberQBRPointHistoryGrid();
                            var grid = $('#MemberQBRPointHistoryResultGrid');
                            grid.jqGrid('clearGridData');
                            if (result.rows.length > 0) {
                                grid[0].addJSONData(result);
                            }
                        })
                        .fail(function (err) {
                            Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
                        })
                }
                else {
                    Util.MessageDialog("No change history found.", "Success");
                }

            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
        })
        .fail(function (err) {
        });
};
function InitMemberQBRPointHistoryGrid() {
    Util.InitJqGrid(
        {
            colNames: MemberDetailQBRPointHistoryBuildColName(),
            colModel: MemberDetailQBRPointHistoryBuildColModel(),
            rowList: [20, 50, 100, 200],
            height: '300',
            hasPager: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            enableAutoWidth: true,
            gridId: 'MemberQBRPointHistoryResult',
            allowSort: true,
        },
        memberDetailQBRSearchObject,
        loadInitMemberQBRPointHistoryGridData);
    UpdateQBRPointHistorColumnTitle();
}

function loadInitMemberQBRPointHistoryGridData() {
    memberDetailQBRSearchObject["MemberKey"] = $('#Member_key').val();
    Util.BlockjqGrid('#MemberQBRPointHistoryResultGrid');
    MemberDetailQBRService.GetMemberQBRNumberPointHistory(baseURL, memberDetailQBRSearchObject)
        .done(function (result) {
            if (result.isError != true) {
                var grid = $('#MemberQBRPointHistoryResultGrid');
                grid.jqGrid('clearGridData');
                if (result.rows.length > 0) {
                    grid[0].addJSONData(result);
                }
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
            Util.UnBlockjqGrid('#MemberQBRPointHistoryResultGrid');
        })
        .fail(function (err) {
            Util.UnBlockjqGrid('#MemberQBRPointHistoryResultGrid');
        });
}

function UpdateQBRPointHistorColumnTitle() {
    var colModels = MemberDetailQBRPointHistoryBuildColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("MemberQBRPointHistoryResultGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("MemberQBRPointHistoryResultGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("MemberQBRPointHistoryResultGrid", col.name);
            Util.JQGridSetPaddingRight("MemberQBRPointHistoryResultGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("MemberQBRPointHistoryResultGrid", col.name);
            }
        }
    }
}

function MemberDetailQBRPointHistoryBuildColName() {
    var colNames = [
        "QBR Number",
        "Changed Date",
    ];
    return colNames;
}

function MemberDetailQBRPointHistoryBuildColModel() {
    var colModel = [
        { key: false, name: "MemberReferenceNumber", index: "MemberReferenceNumber", align: 'left', hidden: false, editable: false},
        {
            key: false, name: "CreatedDate", index: "CreatedDate", align: 'left', editable: false, sortable: true,
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y"
            }
        },
    ];
    return colModel;
}

