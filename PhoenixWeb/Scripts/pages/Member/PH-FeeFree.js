﻿$(document).ready(function () {
    var FeeFreeMemberChk = $('#IsFeeFreeMember');
    
    FeeFreeMemberChk.on('change',function (){
        var isChecked = FeeFreeMemberChk.prop('checked');
        
        AddRemoveRetailRateForFeeFree(isChecked);
        AddRemoveRentalTemplateParam(isChecked);
    })
});

function AddRemoveRetailRateForFeeFree(isChecked) {
    var rateDatas = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];
    
    if (isChecked) {
        $('input#Inactive').prop('checked', false);
        $('input#Inactive').parent().removeClass('checked');
        $('input#EOMMSFPayment').prop('checked', false);
        $('input#EOMMSFPayment').parent().removeClass('checked', false);
        Util.DisableCheckbox('EOMMSFPayment');
        DisabledEnableZipAPI(true);
        $('span#zip-section.info-icon').removeClass('hidden');
    }
    else{
        Util.EnableCheckbox('EOMMSFPayment');
        DisabledEnableZipAPI(false);
        $('span#zip-section.info-icon').addClass('hidden');
    }
        
    $("#CommissionRatesDtoAsJson").val(JSON.stringify(rateDatas));
    
    RenderCommissionRates("commissionRateGrid");
}

function AddRemoveRentalTemplateParam(isChecked){
    var addRentalTemplateElm = $('a#AddMemberRental').attr('href');
    if (addRentalTemplateElm){
        var hasIsFeeFreeParam = addRentalTemplateElm.indexOf('&IsFeeFree=') !== -1;
        if (!isChecked){
            $('a#AddMemberRental').attr('href',addRentalTemplateElm.replace('&IsFeeFree=True','&IsFeeFree=False'));
        }
        else {
            $('a#AddMemberRental').attr('href',addRentalTemplateElm.replace('&IsFeeFree=False','&IsFeeFree=True'));
        }
    }
}

function DisabledEnableZipAPI(IsDisable) {
    $('#ZipAPIKey').attr('readonly',IsDisable);
    $('#LocationID').attr('readonly',IsDisable);
    if (IsDisable){
        $('div#ZipMerchantAPI input#Inactive').parent().parent().addClass('disabled')
    }
    else{
        $('div#ZipMerchantAPI input#Inactive').parent().parent().removeClass('disabled')
    }
}
