﻿var baseURL = Util.PH_UpdateURL();
$(document).ready(function () {
    $("#btnUpdateStatus").on("click",
        function () {
            var accountIdentifiers = $('#InputtedAccountIdentifiers').val();
            if (accountIdentifiers == "") {
                Util.MessageDialog("Please enter Oxygen Account Identifier(s).", "Error");
                return false;
            }
            LG_ShowProgressMask();
            UpdateMultipleAccountIdentifiersStatus(accountIdentifiers)
                .done(function (result) {
                    if (result.IsSuccess) {
                        Util.MessageDialog(result.Message, "Success");
                        $('#InputtedAccountIdentifiers').val('');
                    }
                    else {
                        Util.MessageDialog(result.Message, "Error");
                    }
                    LG_HideProgressMask();
                });
        });
});

UpdateMultipleAccountIdentifiersStatus = function (accountIdentifiers) {
    var deferred = $.Deferred();
    $.ajax({
        url: baseURL + "DriverCard/UpdateMultipleAccountIdentifiersStatus",
        type: "POST",
        data: { accountIdentifiers: accountIdentifiers },
        success: function (result) {
            deferred.resolve(result);
        }
    });
    return deferred.promise();
}