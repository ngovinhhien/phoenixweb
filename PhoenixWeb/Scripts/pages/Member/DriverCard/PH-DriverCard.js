﻿$(document).ready(function () {
    Util.InitializeDialog('#driverCardReissueDialog');
    Util.InitializeDialog('#driverCardRegisterDialog');
    Util.InitializeDialog('#driverCardChangeStatusDialog');
    Util.InitializeDialog('#driverCardInfoDialog');
    Util.InitializeDialog('#driverCardChangeHistoryDialog');

    RenderDriverCard = function(tblContainName) {
        var driverCardData = null;
        if ($("#DriverCardDtoAsJson").val() != '') {
            driverCardData = JSON.parse($("#DriverCardDtoAsJson").val());
        }

        if (driverCardData) {
            var headerHtml = $("#driverCardGridHeader").render({ data: driverCardData });
            var recordHtml = $("#driverCardGridBody").render({ data: driverCardData });
            $("#" + tblContainName).html(headerHtml + recordHtml);

            if (driverCardData.filter(a => a.StatusCode == "F").length == driverCardData.length) {
                $("#registerRow").show();
            } else {
                $("#registerRow").hide();
            }

        } else {
            var emptyHeaderHtml = $("#driverCardEmptyGridHeader").render();
            var emptyBodyHtml = $("#driverCardEmptyGridBody").render();
            $("#" + tblContainName).html(emptyHeaderHtml + emptyBodyHtml);
        }
    };

    OpenRegisterDriverCard = function () {
        var popupViewDetailLog = $('#driverCardRegisterDialog').dialog({
            title: "Register Card",
            position: "center",
            modal: true,
            width: 500,
            height: 250
        });
        $("#driverCardRegisterDialog #cardReferenceNumber").val('');
        popupViewDetailLog.dialog();
    };

    OpenHistoryDriverCard = function (cardId) {
        var popupViewDetailLog = $('#driverCardChangeHistoryDialog').dialog({
            title: "Change History",
            position: "center",
            modal: true,
            width: 1000,
            height: 550
        });
        $("#driverCardChangeHistoryDialog #currentCardId").val(cardId);
        loadChangeHistories(1, $('#driverCardChangeHistory').getGridParam('rowNum'));
        popupViewDetailLog.dialog();

        $(window).bind('resize',
            function () {
                $("#driverCardChangeHistory").setGridWidth(popupViewDetailLog.width() - 60);
            }).trigger('resize');
    };


    CollectRegisterInfo = function() {
        var info = {
            accountIdentifier: $("#driverCardRegisterDialog #cardReferenceNumber").val(),
            memberKey: $("#Member_key").val(),
            address: $("#StreetNo").val() + " " + $("#Street").val(),
            postCode: $("#PPostCode").val(),
            DOB: $("#DOBstring").val(),
            mobile: $("#Mobile").val(),
            firstName: $("#FirstName").val(),
            lastName: $("#LastName").val()
        };
        return info;
    };

    DriverCardRegister = function() {
        var cardNumber = $("#driverCardRegisterDialog #cardReferenceNumber").val();
        if (cardNumber.length != 12) {
            Util.MessageDialog("Card Reference Number must be 12 digits", "Error");
            return;
        }
        var info = CollectRegisterInfo();
        LG_ShowProgressMask();
        DriverCardService.RegisterCard(baseURL, info)
            .done(function(result) {
                LG_HideProgressMask();
                if (!result.IsError) {
                    Util.MessageDialog([result.Message], "Success");
                    var value = JSON.stringify({
                        AccountIdentifier: cardNumber,
                        MemberKey: info.memberKey
                    });
                    $("#DriverCardDtoAsJson").val(result.DriverCardJson);
                    RenderDriverCard("driverCardGrid");
                    $("#registerDriverCardLink").hide();
                    CloseDriverCardDialog('#driverCardRegisterDialog');
                    $("#driverCardError").hide();
                } else {
                    Util.MessageDialog([result.Message], "Error");
                }
            });
    };


    $("#driverCardChangeHistory").jqGrid({
        datatype: "json",
        colNames: ['ID', 'Date Time', 'Action By', 'Action Description', 'Note'],
        colModel: [
            { name: 'ID', sorttype: "int", key: true, hidden: true },
            {
                name: 'Date',
                sortable: false,
                formatter: 'date',
                formatoptions:
                {
                    srcformat: "ISO8601Long",
                    newformat: "d/m/Y h:i:s A"
                },
                width: "40%"
            },
            { name: 'ActionBy', sortable: false, width: "40%" },
            { name: 'ActionDescription', sortable: false, width: "40%" },
            { name: 'Note', sortable: false },
        ],
        pager: '#driverCardChangeHistoryPager',
        rowNum: 10,
        rowList: [10, 20, 50, 100],
        height: 350,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        shrinkToFit: true
    });

    loadChangeHistories = function (currentPage, pageSize) {
        $("#driverCardChangeHistory").closest(".ui-jqgrid").find('.loading').show();
        var cardId = $("#driverCardChangeHistoryDialog #currentCardId").val();
        DriverCardService.GetCardChangeHistory(baseURL, cardId, currentPage, pageSize)
            .done(function (result) {
                if (!result.IsError) {
                    var grid = $('#driverCardChangeHistory')[0];
                    $('#driverCardChangeHistory').jqGrid('clearGridData');
                    grid.addJSONData(result.Data);
                    $("#driverCardChangeHistory").closest(".ui-jqgrid").find('.loading').hide();
                }
            });
    };



    $('#driverCardChangeHistory').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#driverCardChangeHistory').getGridParam('page');
            var rowNum = $('#driverCardChangeHistory').getGridParam('rowNum');
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='driverCardChangeHistoryPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;


            if (pgButton == "next_driverCardChangeHistoryPager") {
                if (currentPage == totalPage) {
                    return;
                }
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_driverCardChangeHistoryPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_driverCardChangeHistoryPager") {
                currentPage = 1;
            }

            if (pgButton == "last_driverCardChangeHistoryPager") {
                currentPage = $('#driverCardChangeHistory').getGridParam('lastpage');
            }


            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (currentPage > totalPage) {
                return;
            }

            loadChangeHistories(currentPage, rowNum);
        }
    });

    $("[data-original]").focusout(function () {
        var all = $("[data-original]");
        for (var el in all) {
            if (!isNaN(parseInt(el))) {
                if (all[el].type == "date") {
                    if (all[el].value != all[el].attributes["data-original"].value.split("/").reverse().join("-")) {
                        $("#driverCardInfoDialog #btnUpdateProfile").prop("disabled", false);
                        return;
                    }
                }
                else {
                    if (all[el].value != all[el].attributes["data-original"].value) {
                        $("#driverCardInfoDialog #btnUpdateProfile").prop("disabled", false);
                        return;
                    }
                }
            }
        }
        $("#driverCardInfoDialog #btnUpdateProfile").prop("disabled", true);
    });
});

GetChangedFields = function() {
    var changes = [];
    var all = $("[data-original]");
    for (var el in all) {
        if (!isNaN(parseInt(el))) {
            var newValue = all[el].value;
            var oldValue = all[el].attributes["data-original"].value;
            if (all[el].type == "date") {
                newValue = newValue.split("/").reverse().join("-");
            }
            if (newValue != oldValue) {
                var elementId = all[el].id;
                var label = $("#driverCardInfoDialog [for='" + elementId + "']").text();
                var content = "Update " + label + " from " + oldValue + " to " + newValue;
                changes.push(content);
            }
        }
    }
    return changes;
};

OpenChangeCardStatus = function(accountIdentifier, status) {
    $("#driverCardChangeStatusDialog #accountIdentifier").val(accountIdentifier);
    LG_ShowProgressMask();
    DriverCardService.GetCardStatus(baseURL, accountIdentifier, $("#Member_key").val())
        .done(function(result) {
            LG_HideProgressMask();
            if (result.IsError == true) {
                Util.MessageDialog(result.Message, "Error");
            } else {
                if (result.DriverCardJson != null) {
                    $("#DriverCardDtoAsJson").val(result.DriverCardJson);
                    RenderDriverCard("driverCardGrid");
                    return;
                }

                if (result.Data.cardStatus == "A" || result.Data.cardStatus == "F" || result.Data.cardStatus == "E") {
                    var currentStatus = result.Data.cardStatus == "A" ? "Issued and Inactive" : result.Data.cardStatus == "E" ? "Expired" : "Closed";
                    Util.MessageDialog("The current card status is " +
                        currentStatus +
                        ".We cannot update on this status.",
                        "Error");
                    return;
                }
                BindingDataToStatusPopup(result.Data.cardStatus, result.Data.cardStatusDescription);
            }

        });
};

OpenReissueDriverCard = function (oldAccountIdentifier) {
    LG_ShowProgressMask();
    DriverCardService.GetCardStatus(baseURL, oldAccountIdentifier, $("#Member_key").val())
        .done(function (result) {
            LG_HideProgressMask();
            if (result.IsError == true) {
                Util.MessageDialog(result.Message, "Error");
            } else {
                if (result.DriverCardJson != null) {
                    Util.MessageDialog("The current card status is Closed .We cannot reissue card.", "Error");
                    $("#DriverCardDtoAsJson").val(result.DriverCardJson);
                    RenderDriverCard("driverCardGrid");
                }
                BindingDataToReissuePopup(oldAccountIdentifier);
            }
        });
};


ShowAccountDetail = function (accountIdentifier, isFromDetail) {
    $("#driverCardInfoDialog #accountIdentifier").val(accountIdentifier);
    var memberKey = $("#Member_key").val();
    DriverCardService.GetCardInfo(baseURL, accountIdentifier, memberKey)
        .done(function (result) {
            if (result.IsError == true) {
                Util.MessageDialog(result.Message, "Error");
            } else {
                var popupViewDetailLog = $('#driverCardInfoDialog').dialog({
                    title: "Account Details",
                    position: "center",
                    modal: true,
                    width: 1000,
                    height: 450
                });
                DisplayCardInfo(result.Data, isFromDetail);
                LoadStateDropDown(result.StateData, result.Data.State);
                popupViewDetailLog.dialog();
                $("#driverCardInfoDialog #btnUpdateProfile").prop("disabled", true);
            }
        });
};

DisplayCardInfo = function (data, isFromDetail) {
    for (var key in data) {
        var el = $("#driverCardInfoDialog .form-group #" + key);
        if (el[0] != null) {
            if (data[key] != null) {
                switch (el[0].nodeName) {
                    case "INPUT":
                        el.val(data[key]);
                        el.attr("data-original", data[key]);
                        if (el[0].type == 'date') {
                            el.val(data.DOB.split("/").reverse().join("-"));
                            el.attr("data-original", data.DOB.split("/").reverse().join("-"));
                            //el.attr("max", data.DOB.split("/").reverse().join("-"));
                            el.attr("max", Util.LG_BuildDateTimeWithInternationalFormat());
                        }
                        break;
                    case "SPAN":
                        el.text(data[key]);
                        break;
                }
            }
        }
    }
    if (data.Status != "OPEN" || isFromDetail) {
        $("#driverCardInfoDialog .form-group .form-control").prop("disabled", true);
        $("#btnUpdateProfile").prop("disabled", true);
    } else {
        $("#driverCardInfoDialog .form-group .form-control").prop("disabled", false);
        $("#btnUpdateProfile").prop("disabled", false);
    }
};

UpdateCardStatus = function () {
    var statusID = $("#driverCardChangeStatusDialog #CardStatus").val();
    if (!statusID) {
        Util.MessageDialog("Please choose a status", "Error");
        return;
    }

    var statusText = $("#driverCardChangeStatusDialog #CardStatus option:selected").text();
    statusText = statusText.substring(0, statusText.indexOf(' - '));
    Util.ConfirmDialog("Do you want to update card status to " + statusText + "?", "Message").done(function () {
        LG_ShowProgressMask();
        var accountIdentifier = $("#driverCardChangeStatusDialog #accountIdentifier").val();
        var memberKey = $("#Member_key").val();
        DriverCardService.ChangeCardStatus(baseURL, accountIdentifier, memberKey, statusID)
            .done(function (result) {
                LG_HideProgressMask();
                if (result.IsError == true) {
                    Util.MessageDialog(result.Message, "Error");
                } else {
                    $("#DriverCardDtoAsJson").val(result.DriverCardJson);
                    RenderDriverCard("driverCardGrid");
                    CloseDriverCardDialog('#driverCardChangeStatusDialog');
                    Util.MessageDialog("Card status has been updated successfully", "Success");
                }
            });
    });

};


DriverCardReissue = function () {
    var oldIdentifier = $("#driverCardReissueDialog #currentAccountIdentifier").val();
    var cardNumber = $("#driverCardReissueDialog #cardReferenceNumber").val();
    var memberKey = $("#Member_key").val();
    if (cardNumber.length != 12) {
        Util.MessageDialog("Card Reference Number must be 12 digits.", "Error");
        return;
    }
    Util.ConfirmDialog("Do you want to reissue this card?", "Message").done(function () {
        LG_ShowProgressMask();
        DriverCardService.ReissueCard(baseURL, memberKey, oldIdentifier, cardNumber)
            .done(function (result) {
                LG_HideProgressMask();
                if (!result.IsError) {
                    Util.MessageDialog([result.Message], "Success");
                    $("#DriverCardDtoAsJson").val(result.DriverCardJson);
                    RenderDriverCard("driverCardGrid");
                    CloseDriverCardDialog('#driverCardReissueDialog');
                } else {
                    Util.MessageDialog([result.Message], "Error");
                }
            });
    });
};

LoadStateDropDown = function (data, selectedValue) {
    var dropdown = $('#driverCardInfoDialog #State');
    dropdown.empty();

    $.each(data,
        function (key, entry) {
            dropdown.append($('<option></option>').attr('value', entry.StateName).text(entry.FullName));
        });

    dropdown.val(selectedValue);
    dropdown.attr("data-original", selectedValue);
};

CollectUpdateProfileInfo = function () {
    var info = {
        accountIdentifier: $("#driverCardInfoDialog #accountIdentifier").val(),
        memberKey: $("#Member_key").val(),
        address: $("#driverCardInfoDialog #Address1").val(),
        address2: $("#driverCardInfoDialog #Address2").val(),
        postCode: $("#driverCardInfoDialog #PostalCode").val(),
        DOB: $("#driverCardInfoDialog #DOB").val(),
        mobile: $("#driverCardInfoDialog #MobilePhone").val(),
        email: $("#driverCardInfoDialog #Email").val(),
        firstName: $("#driverCardInfoDialog #FirstName").val(),
        lastName: $("#driverCardInfoDialog #LastName").val(),
        city: $("#driverCardInfoDialog #City").val(),
        state: $("#driverCardInfoDialog #State").val()
    };
    return info;
}

DriverCardUpdateProfile = function() {
    var changes = GetChangedFields().join(';');
    var info = CollectUpdateProfileInfo();
    LG_ShowProgressMask();
    DriverCardService.UpdateProfile(baseURL, info, changes)
        .done(function(result) {
            LG_HideProgressMask();
            if (result.IsError == true) {
                Util.MessageDialog(result.Message, "Error");
            } else {
                CloseDriverCardDialog('#driverCardInfoDialog');
                Util.MessageDialog("Card Info has been updated successfully.", "Success");
            }
        });
};

BindingDataToStatusPopup = function(statusCode, statusDescription) {
    var popupViewDetailLog = $('#driverCardChangeStatusDialog').dialog({
        title: "Change Card Status",
        position: "center",
        width: 700,
        height: 250,
        modal: true,
    });
    $("#driverCardChangeStatusDialog #CurrentStatus").text(statusDescription);
    $("#driverCardChangeStatusDialog #CardStatus").children('option').hide();
    $("#driverCardChangeStatusDialog #CardStatus").val('');
    switch (statusCode) {
    case 'B':
        $("#driverCardChangeStatusDialog #CardStatus").children("option:contains('Select status')").show();
        $("#driverCardChangeStatusDialog #CardStatus").children("option:contains('INACTIVE')").show();
        break;
    case 'I':
        $("#driverCardChangeStatusDialog #CardStatus").children("option:contains('Select status')").show();
        $("#driverCardChangeStatusDialog #CardStatus").children("option:contains('OPEN')").show();
        break;
    }


    popupViewDetailLog.dialog();
};

BindingDataToReissuePopup = function(oldAccountIdentifier) {
    $("#currentAccountIdentifier").val(oldAccountIdentifier);
    var popupViewDetailLog = $('#driverCardReissueDialog').dialog({
        title: "Reissue Card",
        position: "center",
        modal: true,
        width: 500,
        height: 250
    });
    $("#driverCardReissueDialog #cardReferenceNumber").val('');
    popupViewDetailLog.dialog();
};

CloseDriverCardDialog = function (dialogId) {
    $(dialogId).dialog("close");
};