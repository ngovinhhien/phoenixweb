﻿(function (DriverCardService, $) {
    DriverCardService.RegisterCard = function (baseURL, info) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/RegisterCard",
            type: "POST",
            data: {
                accountIdentifier: info.accountIdentifier,
                memberKey: info.memberKey,
                address: info.address,
                postCode: info.postCode,
                DOB: info.DOB,
                mobile: info.mobile,
                lastName: info.lastName,
                firstName: info.firstName,
                email: info.email
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.ChangeCardStatus = function (baseURL, accountIdentifier, memberKey, statusID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/ChangeCardStatus",
            type: "POST",
            data: { cardRefNumber: accountIdentifier, memberKey: memberKey, statusID: statusID },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.GetCardInfo = function (baseURL, accountIdentifier, memberKey) {
        var params = "cardRefNumber=" + accountIdentifier + "&" + "memberKey=" + memberKey;
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/GetCardInfo?" + params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.ReissueCard = function (baseURL, memberKey, oldAccountIdentifier, newAccountIdentifier) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/ReissueCard",
            type: "POST",
            data: {
                memberKey: memberKey,
                oldAccountIdentifier: oldAccountIdentifier,
                newAccountIdentifier: newAccountIdentifier
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.GetCardChangeHistory = function (baseURL, cardId, currentPage, pageSize) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/GetCardChangeHistory",
            type: "POST",
            data: {
                driverCardId: cardId,
                currentPage: currentPage,
                pageSize: pageSize
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.UpdateProfile = function (baseURL, info, changedContent) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/UpdateCardProfile",
            type: "POST",
            data: {
                accountIdentifier: info.accountIdentifier,
                memberKey: info.memberKey,
                address: info.address,
                address2: info.address2,
                postCode: info.postCode,
                city: info.city,
                state: info.state,
                DOB: info.DOB,
                mobile: info.mobile,
                lastName: info.lastName,
                firstName: info.firstName,
                email: info.email,
                changedContent: changedContent
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    DriverCardService.GetCardStatus = function (baseURL, accountIdentifier, memberKey) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "DriverCard/GetCardStatus",
            type: "POST",
            data: {
                accountIdentifier: accountIdentifier,
                memberKey: memberKey
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        RegisterCard: DriverCardService.RegisterCard,
        ChangeCardStatus: DriverCardService.ChangeCardStatus,
        GetCardInfo: DriverCardService.GetCardInfo,
        ReissueCard: DriverCardService.ReissueCard,
        GetCardChangeHistory: DriverCardService.GetCardChangeHistory,
        UpdateProfile: DriverCardService.UpdateProfile,
        GetCardStatus: DriverCardService.GetCardStatus
    };

}(window.DriverCardService = window.DriverCardService || {}, jQuery));