﻿var searchFees = undefined;
var selectedFees = [];
var rejectedFees = [];
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;

$(function () {

    $("#grdPendingFees").jqGrid({
        url: "",
        colNames: [
            "Select", "Member ID", "Created User", "Amount", "Status", "Reason"
        ],
        colModel: [
            {
                key: true,
                name: "ID",
                index: "ID",
                align: 'center',
                width: 40,
                editable: true,
                sortable: false,
                formatter: customCheckboxFormatter
            },
            { key: false, name: "MemberID", index: "MemberID", width: 60, align: 'left', editable: false, sortable: true },
            { key: false, name: "Username", index: "Username", width: 60, align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "Amount", index: "Amount", width: 160, align: 'left', editable: false, sortable: true },
            { key: false, name: "Status", index: "Status", width: 110, align: 'left', editable: false, sortable: false },
            { key: false, name: "Reason", index: "Reason", align: 'left', editable: false, sortable: false },
        ],
        pager: "#grdPendingFeesPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#grdPendingFees').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdPendingFees').getGridParam('page');
            var rowNum = $('#grdPendingFees').getGridParam('rowNum');
            var sortName = $('#grdPendingFees').getGridParam('sortname');
            var sortOrder = $('#grdPendingFees').getGridParam("sortorder");

            var selectedYear = $("#Year").val();
            var selectedMonth = $("#Month").val();
            var selectedBusinessType = $("#BusinessTypeID").val();

            if (pgButton == "next_grdPendingFeesPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdPendingFeesPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdPendingFeesPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdPendingFeesPager") {
                currentPage = $('#grdPendingFees').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='grdPendingFeesPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (!searchQBR) {
                searchQBR = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    Year: selectedYear,
                    Month: selectedMonth,
                    BusinessTypeID: selectedBusinessType,
                    SortName: sortName,
                    SortOrder: sortOrder
                };
            } else {
                searchQBR.CurrentPage = currentPage;
                searchQBR.PageSize = rowNum;
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;
            }

            if (isFiltering) {
                $("#textAdvanceSearch").trigger("keyup");
            } else {
                loadQantasPointReward();
            }
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchQBR) {
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;
                searchQBR.RejectedInforObject = rejectedQBR;

                if (isFiltering) {
                    $("#textAdvanceSearch").trigger("keyup");
                } else {
                    loadQantasPointReward();
                }
            }
            return 'stop';
        }
    });


    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
}); // end of document ready


customCheckboxFormatter = function (cellvalue, options, rowObject) {
    var html = "";
    html += "<input type='checkbox' data-id='" + cellvalue + "' onclick='SaveSelected(this)'/>";
    return html;
};

customReportFormatter = function (memberReferenceEntryKeyList, options, rowObject) {
    var reportURL = baseURL + "QantasProcessing/RenderReport?idList=" + memberReferenceEntryKeyList;
    return "<a target='_blank' id='viewReport_" + rowObject.ID + "' data-entry-ids='" + memberReferenceEntryKeyList + "' href=" + reportURL + ">View Report</a>";
}

$("#search").off("click").on("click",
    function () {
        if (rejectedQBR.length > 0) {
            var confirmed =
                confirm(
                    "If you change the filter option now, all your current update on the QBR list will be cancelled. Are you sure?")
            if (confirmed) {
                rejectedQBR = [];
                selectedQBR = [];
            } else {
                return false;
            }
        }

        var selectedYear = $("#Year").val();
        var selectedMonth = $("#Month").val();
        var selectedBusinessType = $("#BusinessTypeID").val();
        searchQBR = {
            Year: selectedYear,
            Month: selectedMonth,
            BusinessTypeID: selectedBusinessType,
            CurrentPage: 1,
            PageSize: 10
        };

        loadQantasPointReward();
    });


onRejectQantasPointReward = function () {
    if (selectedQBR.length) {
        var reason = prompt("Please add a reason:", '');
        reason = $.trim(reason)
        if (reason) {
            $.each(selectedQBR,
                function (i, el) {

                    var rejected = $.grep(rejectedQBR,
                        function (element) {
                            return element.ID == el;
                        });

                    if (rejected.length > 0) {
                        $.grep(rejectedQBR,
                            function (element) {
                                if (element.ID == el) {
                                    element.Comment = reason;
                                }
                            });
                    } else {
                        var newRejectedQBR = {
                            ID: el,
                            Status: "Sale Rejected",
                            Comment: reason
                        }
                        rejectedQBR.push(newRejectedQBR);
                    }

                    //update data on grid after reject
                    var rowData = $('#grdQantasPointVerificationForSaleFin').jqGrid('getRowData', el);
                    rowData.Status = "Sale Reject";
                    rowData.Comment = reason;
                    rowData.ID = el;
                    rowData.MemberReferenceEntryKeyList = $("#viewReport_" + el).data("entry-ids")
                    $('#grdQantasPointVerificationForSaleFin').jqGrid('setRowData', el, rowData);

                });

            loadCheckedRecords();
        } else {
            alert("Please enter a rejection reason.");
            return;
        }

    } else {
        alert("Please select at least one record.");
    }
};

onApproveQantasPointReward = function () {
    if (selectedQBR.length) {
        $.each(selectedQBR,
            function (i, el) {
                var rejectedIndex = rejectedQBR.findIndex(x => x.ID == el);
                if (rejectedIndex >= 0) {
                    rejectedQBR.splice(rejectedIndex, 1);
                }

                var rowData = $('#grdQantasPointVerificationForSaleFin').jqGrid('getRowData', el);
                rowData.Status = "";
                rowData.Comment = "";
                rowData.ID = el;
                rowData.MemberReferenceEntryKeyList = $("#viewReport_" + el).data("entry-ids");
                $('#grdQantasPointVerificationForSaleFin').jqGrid('setRowData', el, rowData);
            });
        loadCheckedRecords();
    } else {
        alert("Please select least one records!");
    }
}

loadQantasPointReward = function () {

    //Show progress mask
    LG_ShowProgressMask();

    //Searching declined transaction
    $.ajax({
        url: baseURL + "QantasProcessing/GetQBRPoint",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchQBR)
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdQantasPointVerificationForSaleFin')[0];
        $('#grdQantasPointVerificationForSaleFin').jqGrid('clearGridData');
        grid.addJSONData(result);
        selectedQBR = [];

        //update status and comment of rejected records
        if (rejectedQBR.length > 0) {
            loadRejectedReasonToGrid();
        }

        //the notice flag is using advance search
        isFiltering = false;

        //handle display the search filter after search QBR
        var dataGridCount = $('#grdQantasPointVerificationForSaleFin').jqGrid('getGridParam', 'reccount');
        if (dataGridCount > 0) {
            $("#filterSection").show();
        } else {
            $("#filterSection").hide();
        }

        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
};


loadCheckedRecords = function () {
    $.each(selectedQBR,
        function (i, el) {
            var checkbox = $("input[data-id='" + el + "']")[0];
            if (checkbox) {
                $(checkbox).prop('checked', true);
            }
        });
};

loadRejectedReasonToGrid = function () {
    var idElemens = $("#grdQantasPointVerificationForSaleFin").find("td[aria-describedby='grdQantasPointVerificationForSaleFin_ID']");
    $.each(idElemens,
        function (i, el) {
            var checkbox = el.firstElementChild;
            var id = $(checkbox).data("id");
            var rejectedObject = $.grep(rejectedQBR,
                function (el) {
                    return el.ID == id;
                });
            if (rejectedObject.length > 0) {
                var rowData = $('#grdQantasPointVerificationForSaleFin').jqGrid('getRowData', id);
                rowData.Status = rejectedObject[0].Status;
                rowData.Comment = rejectedObject[0].Comment;
                rowData.ID = id;
                rowData.MemberReferenceEntryKeyList = $("#viewReport_" + id).data("entry-ids");
                $('#grdQantasPointVerificationForSaleFin').jqGrid('setRowData', id, rowData);
            }
        });
};

SaveSelected = function (checkboxEl) {
    var isChecked = $(checkboxEl).is(":checked");
    var currentselectedQBR = $(checkboxEl).attr("data-id");
    if (isChecked) {
        selectedQBR.push(currentselectedQBR);
    } else {
        selectedQBR.splice($.inArray(currentselectedQBR, selectedQBR), 1);
    }
};


sendQantasPointToApproval = function () {
    var dataGridCount = $('#grdQantasPointVerificationForSaleFin').jqGrid('getGridParam', 'reccount');
    if (dataGridCount > 0) {
        searchQBR.RejectedInforObject = rejectedQBR;
        LG_ShowProgressMask();
        $.ajax({
            url: baseURL + "QantasProcessing/SendQantasPointToApproval",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchQBR),
            success: function (res) {
                if (res.IsError) {
                    alert(res.ErrorMessage);
                    //Loading data to grid
                    if (res.ErrorMessage.indexOf("Data has been processing by") > -1) {
                        $('#grdQantasPointVerificationForSaleFin').jqGrid('clearGridData');
                        rejectedQBR = [];
                        selectedQBR = [];
                        searchQBR = {};
                    }
                } else {
                    loadQantasPointReward();
                    rejectedQBR = [];
                    selectedQBR = [];
                    searchQBR = {};
                    alert("The selected QBR point list have been sent to Manager.");
                }
            }
        });
    } else {
        alert("No record is approved. You can search with other option.");
    }
}

ExportFile = function () {
    if (searchQBR.hasOwnProperty("Month")) {
        window.location.href =
            baseURL +
            "QantasProcessing/GetQBRExportFile?month=" +
            searchQBR.Month +
            "&year=" +
            searchQBR.Year +
            "&businesTypeId=" +
            searchQBR.BusinessTypeID;
    }
}
