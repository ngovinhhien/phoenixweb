﻿var baseURL = Util.PH_UpdateURL();

var searchObject = {
    PageIndex: '1',
    PageSize: '20',
    SortField: 'CreatedDate',
    SortOrder: 'desc',
}

var filterParams = {};
$(document).ready(function () {
    function BuildColName() {
        var colNames = [
            "Plan Name",
            "Created Date",
            "Business Type",
            "Created By",
            "Actions"
        ];

        return colNames;
    }

    function BuildColModel() {
        return [
            {
                key: false,
                name: "MSFPlanName",
                index: "MSFPlanName",
                align: 'left',
                editable: false,
                sortable: true
                , classes: "grid-col-padding-left"
            },
            {
                key: false,
                name: "CreatedDate",
                index: "CreatedDate",
                align: 'left',
                formatter: 'date',
                formatoptions:
                {
                    srcformat: 'd/m/Y',
                    newformat: "d/m/Y"
                },
                editable: false,
                sortable: true,
                classes: "grid-col-padding-left"
            },
            {
                key: false,
                name: "BusinessTypeName",
                index: "BusinessTypeName",
                align: 'left',
                editable: false,
                sortable: true, classes: "grid-col-padding-left"
            },
            {
                key: false,
                name: "CreatedBy",
                index: "CreatedBy",
                align: 'left',
                editable: false,
                sortable: true, classes: "grid-col-padding-left"
            },
            {
                key: false,
                name: "MSFPlanID",
                index: "MSFPlanID",
                align: 'center',
                editable: false,
                sortable: false,
                formatter: editFormatter
            }
        ];
    }

    function editFormatter(cellvalue, options, rowObject) {
        return '<a href="EditPlan?PlanID=' + cellvalue + '" class="btn btn-default" role="button">Edit</a>';
    }

    function initPricingPlanGrid() {
        Util.InitJqGrid(
            {
                colNames: BuildColName(),
                colModel: BuildColModel(),
                rowList: [20, 50, 100, 200],
                height: '363',
                hasPager: true,
                enableShrinkToFit: true,
                enableMultiselect: false,
                autowidth: true,
                gridId: 'PricingPlan',
                allowSort: true,
                autoencode: true
            },
            searchObject,
            loadGridData);
        loadGridData();
        UpdateColumnTitle();
        adjustSortIconAndAlignment("PricingPlan");
    }

    function loadGridData() {
        LG_ShowProgressMask();
        ManagePricingPlanService.LoadPricingPlan(baseURL, searchObject)
            .done(function (result) {
                if (!result.IsSuccess) {
                    Util.MessageDialog(result.Message.split('\n'), "Error");
                } else {
                    var data = result.Data;
                    Util.ReloadDataJqgrid('#PricingPlanGrid', data, function () {
                        LG_HideProgressMask();
                    });
                }
                LG_HideProgressMask();
            })
            .fail(function (err) {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
                LG_HideProgressMask();
            });
    }


    function adjustSortIconAndAlignment(tableId) {
        var columns = BuildColModel();
        var gridId = tableId + 'Grid';
        for (var col of columns) {
            if (col.sortable == false) {
                var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
                $(cellSelector).find('span.s-ico').remove();
                $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
                if (col.frozen) {
                    var cellSelectorFrozen = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                    $(cellSelectorFrozen).find('span.s-ico').remove();
                    $(cellSelectorFrozen).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
                }

            }
            if (col.align) {
                $("#" + gridId + "_" + col.name).addClass(col.align);
                $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
            }
        }

        //Disabled all sort icon
        $(".ui-grid-ico-sort").addClass("ui-state-disabled");
    }

    function UpdateColumnTitle() {
        var colModels = BuildColModel();
        for (var col of colModels) {
            if (col.align === "left") {
                Util.JQGridAlignLeftHeader("PricingPlanGrid", col.name);
                if (!col.frozen) {
                    Util.JQGridSetPaddingLeft("PricingPlanGrid", col.name);
                }
            } else if (col.align === "right") {
                Util.JQGridAlignRightHeader("PricingPlanGrid", col.name);
                Util.JQGridSetPaddingRight("PricingPlanGrid", col.name);
                if (col.sortable) {
                    Util.JQGridSetPaddingRightSort("PricingPlanGrid", col.name);
                }
            }
        }
    }
    initPricingPlanGrid();
});
