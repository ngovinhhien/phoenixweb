﻿(function (ManagePricingPlanService, $, undefined) {
    ManagePricingPlanService.LoadPricingPlan = function(baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'PricingPlan/LoadPricingPlans?',
            type: "GET",
            data: searchParams,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }
    return {
        LoadPricingPlan: ManagePricingPlanService.LoadPricingPlan
    };

}(window.ManagePricingPlanService = window.ManagePricingPlanService || {}, jQuery));