﻿$(document).ready(function () {
    var urlOrigin = Util.PH_UpdateURL();
    $(".errorIcon").hide();
    function validatePricingPlan() {
        $(".errorIcon").hide();
        var isValid = true;
        var planName = $("input[name='PlanName']");
        var businessType = $("select[name='SelectedBusinessTypeKey']");
        var subBusinessType = $("input[name^='SubBusinessType_']:checked");

        $.each($('div.row-fluid.form-group.field-validation-error'),
            function (index, element) {
                $(element).removeClass("field-validation-error errorClass");
            });
        $.each($('.errorClass'),
            function (index, element) {
                $(element).removeClass("errorClass");
            });
        if (!planName.val().trim()) {
            planName.parents('div.row-fluid.form-group').addClass("field-validation-error");
            planName.addClass("errorClass");
            isValid = false;
        }

        if (!businessType.val().trim()) {
            businessType.parents('div.row-fluid.form-group').addClass("field-validation-error");
            isValid = false;
        }

        if (!subBusinessType.length) {
            $("input[name^='SubBusinessType_']").parents('div.row-fluid.form-group').addClass("field-validation-error");
            isValid = false;
        }

        $(".ratenumber").trigger("focusout");
        $(".transactionfee").trigger("focusout");
        return isValid;
    }

    $(".ratenumber").focusout(function () {
        var fieldName = $(this).attr("display-name");
        var fieldValue = $(this).val();
        var regularExpress = /^(\-?[0-9]+(\.[0-9]{1,4})?)$/g;
        var errorIcon = $(this).parent().find(".errorIcon");
        var maxAmount = 100;
        var minAmount = -100;
        
        if (!regularExpress.test(fieldValue) || fieldValue > maxAmount || fieldValue < minAmount) {
            var message = "Invalid " + fieldName + " rate";

            // modify
            if ($.trim(fieldValue) == '') {
                message = "Required";
            }

            if (errorIcon && errorIcon.length) {
                $(errorIcon).attr("title", message);
                $(errorIcon).show();
            }

            $(this).addClass("errorClass");
            return true;
        } else {
            $(errorIcon).attr("title", "");
            $(errorIcon).hide();
            $(this).removeClass("errorClass");
        }
    });

    $(".transactionfee").focusout(function () {
        var fieldName = $(this).attr("display-name");
        var fieldValue = $(this).val();
        if (!fieldValue) {
            var errorIcon = $(this).parent().find(".errorIcon");
            $(errorIcon).attr("title", "");
            $(errorIcon).hide();
            $(this).removeClass("errorClass");
            return false;
        }
        var regularExpress = /^(\-?[0-9]+(\.[0-9]{1,4})?)$/g;
        var errorIcon = $(this).parent().find(".errorIcon");

        var maxAmount = 100;
        var minAmount = -100;
        if (!regularExpress.test(fieldValue) || fieldValue > maxAmount || fieldValue < minAmount) {
            var message = "Invalid " + fieldName + " transaction fee";;
            if (errorIcon && errorIcon.length) {
                $(errorIcon).attr("title", message);
                $(errorIcon).show();
            }
            $(this).addClass("errorClass");
            return true;
        } else {
            $(errorIcon).attr("title", "");
            $(errorIcon).hide();
            $(this).removeClass("errorClass");
        }
    });

    $("#SavePricingPlan").click(function (event) {
        event.preventDefault();

        if (!validatePricingPlan()) {
            return false;
        }

        if ($(".errorClass").length > 0) {
            return false;
        }

        var data = {};
        data.PlanId = $("#PlanId").val();
        data.PlanName = $("#PlanName").val();
        data.SelectedBusinessTypeKey = $("#BusinessType").val();

        var subList = [];
        $.each($('#SubBussinessTypeCheckList input:checkbox'),
            function (index, element) {
                if ($(element).parent().hasClass("checked")) {
                    subList.push(element.id);
                }
            });
        data.SubBusinessTypeIDs = subList;
        var Rates = [];

        $.each($("#rateTable .rateRow"),
            function (index, element) {
                var obj = {};
                obj.PayMethodKey = element.id;
                obj.Rate = $("#ratenumber_" + element.id).val();
                obj.TransactionFee = $("#transactionfee_" + element.id).val();
                var allowswitch = $(element).attr("data-allowswitch") == "True";
                if (allowswitch) {
                    obj.IsRateFixed = $("#rateoption_" + element.id).val() == "true";
                } else {
                    obj.IsRateFixed = false;
                }
                Rates.push(obj);
            });
        data.Rates = Rates;

        $.ajax({
            url: urlOrigin + 'PricingPlan/SavePlan',
            type: "POST",
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                if (result.IsSuccess) {
                    Util.MessageDialog(result.Message, 'Information', function () {
                        window.location = urlOrigin + "PricingPlan/ManagePricingPlan";
                    });
                } else if (!!result.Message) {
                    Util.MessageDialog(result.Message, 'Information');
                } else if (result.includes('Error 401 : Unauthorised Request')) {
                    Util.MessageDialog('You do not have permission to access the requested resource due to security restrictions. In order to gain access, please speak to your system administrator.', 'Information');
                }
                else {
                    Util.MessageDialog("System Error. Please contact IT", 'Information');
                }
            },
            error: function (err) {
                Util.MessageDialog("System Error. Please contact IT", 'Error');
            }
        });
    });


    resetForm = function() {
        $("#SubBussinessTypeCheckList .checked").removeClass("checked");
        $(".errorIcon").hide();
        $.each($('div.row-fluid.form-group.field-validation-error'),
            function (index, element) {
                $(element).removeClass("field-validation-error errorClass");
            });
        $.each($('.errorClass'),
            function (index, element) {
                $(element).removeClass("errorClass");
            });
    }
     cancelUpdatePricingPlan = function() {
        var message = "Are you sure you want to discard all changes?";
        Util.ConfirmDialog(message, 'Confirmation', "YES", "CANCEL").done(function () {
            window.location = urlOrigin + "PricingPlan/ManagePricingPlan";
        });
    };
})
