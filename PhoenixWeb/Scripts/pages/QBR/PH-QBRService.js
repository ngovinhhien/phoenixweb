﻿(function (QBRService, $) {
    QBRService.SearchPointHistory = function (baseURL, info) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'QBR/SearchPointHistory',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(info),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    QBRService.GetHistoryDetail = function (baseURL, info) {
        var deferred = $.Deferred(); 
        $.ajax({
            url: baseURL + 'QBR/FileDetail',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(info),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    QBRService.LoadPromotions = function (baseURL, BusinessTypeVal) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'AjaxAdmin/GetPromotionsByCompany',
            data: { CompanyKey: BusinessTypeVal },
            type: "GET",
            dataType: "json",
            success: function (data) {
                Util.InitializeDropdownList("#PromotionID", "All", data, "PromotionKey", "PromotionName");
            },
            error: function () {
                Util.MessageDialog("Cannot load promotions.", "Error");
            }
        });
        return deferred.promise();
    }

    return {
        SearchPointHistory: QBRService.SearchPointHistory,
        GetHistoryDetail: QBRService.GetHistoryDetail,
        LoadPromotions: QBRService.LoadPromotions
    };

    

}(window.QBRService = window.QBRService || {}, jQuery));