﻿var baseURL = Util.PH_UpdateURL();

var searchObject = {
    PageIndex: '',
    PageSize: '',
    SortField: '',
    SortOrder: '',
}

var filterParams = {};

$(document).ready(function () {

    var currentYear = new Date().getFullYear();
    Util.InitDatepicker('.datepicker', {
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        yearRange: (currentYear - 10) + ":" + (currentYear),
        allowClear: true
    });

    $(".monthpicker").parent().css("display", "flex");
    $(".monthpicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/yy",
        showButtonPanel: true,
        buttonImage: "../Images/iconfinder_calendar_115762.png",
        showOn:"both",
        showOtherMonths: true,
        yearRange: (currentYear - 10) + ":" + (currentYear),
        onChangeMonthYear: function (year, month, inst) {
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month - 1, 1)));
        },
        beforeShow: function (input, inst) {
            inst.dpDiv.addClass('ui-monthpicker-remove-calendar');
        },
        onClose: function (dateText, inst) {
            var month = $(".ui-datepicker-month :selected").val();
            var year = $(".ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
            inst.dpDiv.removeClass('ui-monthpicker-remove-calendar');
        }
    }).focus(function () {
        $(".ui-datepicker-calendar").hide();
        if ($(this).val() != '') {
            let periods = $(this).val().split('/');
            $(".ui-datepicker-month").val(parseInt(periods[0]) - 1);
            $(".ui-datepicker-year").val(parseInt(periods[1]));
        }
    });

    Util.ValidateDateFromTo($("#SubmittedFrom"), $("#SubmittedTo"));

    $("#CompanyKey").change(function () {
        var BusinessTypeVal = $("#CompanyKey").val();
        $('#PromotionID').empty();
        if (BusinessTypeVal == "") {
            Util.InitializeDropdownList("#PromotionID", "All", null, null, null);
            return;
        }
        QBRService.LoadPromotions(baseURL, BusinessTypeVal);
    });

    $("#reset").click(function () {
        $("#CompanyKey").val("");
        $('#PromotionID').empty();
        $('#PromotionID').append('<option value="">All</option>');
        $("#StatusID").val("");
        $("#MemberID").val("");
        $("#PeriodFrom").val("");
        $("#PeriodTo").val("");
        $("#FileName").val("");
        $("#SubmittedFrom").val("");
        $("#SubmittedTo").val("");
        $("#QBRNumber").val("");
    });

    Util.InitJqGrid(
        {
            colNames: BuildColName(),
            colModel: BuildColModel(),
            rowList: [20, 50, 100, 200],
            height: '350',
            hasPager: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            autowidth: true,
            gridId: 'QBRPointHistorySearchResult',
            allowSort: true,
        },
        searchObject,
        loadGridData);
    UpdateColumnTitle();
    $("#btnExport").prop('disabled', true);
    BindingEvent();
    
});

function BindingEvent() {
    Util.AutoCorrectText('#MemberID');
    Util.AutoCorrectText('#QBRNumber');
    Util.AutoCorrectText('#FileName');
}

function SearchQBRHistory(e) {
    e.preventDefault();
    if (!IsValidMonthPeriodFromTo('#PeriodFrom', '#PeriodTo')) {
        Util.MessageDialog("Invalid Period", "Error");
        return;
    }

    filterParams = Util.getFilterParams('form');
    $.each(filterParams, function (param) {
        searchObject[param] = filterParams[param];
    });

    searchObject.PageIndex = 1;
    searchObject.PageSize = $("#QBRPointHistorySearchResult").getPageSize();
    loadGridData();
}

function IsValidMonthPeriodFromTo(from, to) {
    if ($(from).val() == '' || $(to).val() == '') {
        return true;
    }
    var startMonth = new Date($(from).val().split('/')[1] + '-' + $(from).val().split('/')[0]);
    var endMonth = new Date($(to).val().split('/')[1] + '-' + $(to).val().split('/')[0]);
    return startMonth < endMonth;
}

function OpenDetailPopup(event, rowid) {
    event.preventDefault();
    var summaryKey = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'MemberReferenceEntrySummaryKey');
    var memberId = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'MemberID');
    var tradingName = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'TradingName');
    var points = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'Points');
    var promotionName = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'PromotionName');
    var qbrNumber = $('#QBRPointHistorySearchResultGrid').jqGrid('getCell', rowid, 'QBRNumber');

    var object = {
        MemberReferenceSummaryKey: summaryKey,
        MemberId: memberId,
        TradingName: tradingName,
        Points: points,
        PromotionName: promotionName,
        QBRNumber: qbrNumber,
    };

    QBRService.GetHistoryDetail(baseURL, object)
        .done(function (result) {
            $("#dialog").empty();
            $("#dialog").append(result);
        })
        .fail(function (err) {
        });
}

function loadGridData() {
    Util.BlockjqGrid('#QBRPointHistorySearchResultGrid');
    QBRService.SearchPointHistory(baseURL, searchObject)
        .done(function (result) {
            if (result.isError != true) {
                Util.ReloadDataJqgrid('#QBRPointHistorySearchResultGrid', result, function () {
                    if (result.rows.length > 0) {
                        $("#btnExport").prop('disabled', false);
                    }
                    else {
                        $("#btnExport").prop('disabled', true);
                    }
                });
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
            Util.UnBlockjqGrid('#QBRPointHistorySearchResultGrid');
        })
        .fail(function (err) {
            Util.UnBlockjqGrid('#QBRPointHistorySearchResultGrid');
        });
}

function ExportQBRHistory(e) {
    e.preventDefault();
    window.location.href =
        baseURL +
        "QBR/QBR_Point_History?memberId=" +
    searchObject.MemberID +
        "&qbrNumber=" +
    searchObject.QBRNumber +
        "&companyKey=" +
    searchObject.CompanyKey +
        "&promotionKey=" +
    searchObject.PromotionID +
        "&fileName=" +
    searchObject.FileName +
        "&statusKey=" +
    searchObject.StatusID +
        "&submittedDateFrom=" +
    searchObject.SubmittedFrom +
        "&submittedDateTo=" +
    searchObject.SubmittedTo +
        "&periodFrom=" +
    searchObject.PeriodFrom +
        "&periodTo=" +
    searchObject.PeriodTo +
        "&SortField=" +
        searchObject.SortField +
        "&SortOrder=" +
        searchObject.SortOrder;
}

function BuildColName() {
    var colNames = [
        "",
        "MID",
        "Business Type",
        "Trading Name",
        "QBR No.",
        "Points",
        "Period",
        "Promotion Name",
        "File Name",
        "Submitted Date",
        "Status",
        "Action",
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "MemberReferenceEntrySummaryKey", index: "MemberReferenceEntrySummaryKey", align: 'left', hidden: true, editable: false, sortable: false, classes: "grid-col-padding-left"  },
        { key: false, name: "MemberID", index: "MemberID", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: true },
        { key: false, name: "CompanyName", index: "CompanyName", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: true },
        { key: false, name: "TradingName", index: "TradingName", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: true },
        { key: false, name: "QBRNumber", index: "QBRNumber", align: 'right', classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "Points", index: "Points", align: 'right', formatter: "number", formatoptions: {thousandsSeparator: ",", decimalPlaces:0, defaultValue: '0' }, classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "Period", index: "Period", align: 'right', classes: "grid-col-padding-right", editable: false, sortable: true },
        { key: false, name: "PromotionName", index: "PromotionName", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: false },
        { key: false, name: "FileName", index: "FileName", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: true },
        {
            key: false, name: "SubmittedDate", index: "SubmittedDate", align: 'right', editable: false, sortable: true,
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y"
            }, classes: "grid-col-padding-right"
        },
        { key: false, name: "Status", index: "Status", align: 'left', classes: "grid-col-padding-left" , editable: false, sortable: false },
        { key: false, title: false, name: "", index: "", align: 'center', editable: false, sortable: false, formatter: addAction }
    ];

    return colModel;
}

function addAction(cellValue, option, rowObject) {
    var htmlButtons =
        '<div>'
        + '<button class="btn-inline clickable"  onclick="OpenDetailPopup(event, \'' + option.rowId + '\')"> View </button>'
        + '</div>';
    return htmlButtons;
}

function UpdateColumnTitle() {
    var colModels = BuildColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("QBRPointHistorySearchResultGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("QBRPointHistorySearchResultGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("QBRPointHistorySearchResultGrid", col.name);
            Util.JQGridSetPaddingRight("QBRPointHistorySearchResultGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("QBRPointHistorySearchResultGrid", col.name);
            }
        }
    }
}