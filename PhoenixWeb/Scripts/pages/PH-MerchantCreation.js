﻿$(document).ready(function () {
    var RentalStartDateValidate = true;
    var RentalEndDateValidate = true;
    var FeeFreeRentalTemplateValue = 0;
    var classReadOnly = $(".readonly");
    $(".errorIcon").hide();
    $(".warningIcon").hide();

    classReadOnly.find("input").attr("readonly", "readonly");
    //Get URL Origin
    var urlOrigin = Util.PH_UpdateURL();


    $("#RentalStartDate").off("change").on("change",
        function (event) {
            var valid = event.currentTarget.validity.valid;
            var item = $("#RentalStartDate").parent().find(".field-validation-valid");
            if (!valid) {
                item.text(event.currentTarget.validationMessage);
                item.addClass("errorClass");
                RentalStartDateValidate = false;
            } else {
                item.text("");
                RentalStartDateValidate = true;
                item.removeClass("errorClass");
            }
        });

    $("#RentalEndDate").off("change").on("change",
        function (event) {
            var valid = event.currentTarget.validity.valid;
            var item = $("#RentalEndDate").parent().find(".field-validation-valid");
            if (!valid) {
                item.text(event.currentTarget.validationMessage);
                item.addClass("errorClass");
                RentalEndDateValidate = false;
            } else {
                item.text("");
                RentalEndDateValidate = true;
                item.removeClass("errorClass");
            }
        });



    $(".active").removeClass("active open");
    $(".Member").addClass("active open");

    $("#TerminalRentalRateTb").hide();

    $("#DriverAuth").val('');
    $("#DriverAuthId").hide();

    $.ajax({
        //url: '@Url.Action("GetSearchFields", "AjaxAdmin")',
        url: urlOrigin + '/AjaxAdmin/GetSearchFields',
        type: "GET",
        dataType: "json",
        success: function (data) {
            $.each(data,
                function (i, item) {
                    $("#effectTypes").append(
                        $("<option></option>").val(item.SearchField).html(item.SearchKey)
                    );
                });
        }
    });

    $("#submitResult").off("click").on("click",
        function () {
            var businessTypeKey = $('#BusinessType').val();
            if (businessTypeKey == 0) {
                $('#error-message-vehicle-type').html('Please choose a business type.');
                return false;
            }

            var savedRates = JSON.parse($("#CommissionRatesDtoAsJson").val()) || [];

            if (!$('input#IsFeeFreeMember').prop('checked') && savedRates.length < 1) {
                Util.MessageDialog("Please add at least 1 Commission Rate/MSF for this Member.");
                return false;
            }

            var businessKeysAllowRebate = $('#BusinessKeysAllowRebate').val().split(';');
            var savedRebate = JSON.parse($("#RebatesDtoAsJson").val()) || [];
            if (businessKeysAllowRebate.indexOf(businessTypeKey) != -1 && savedRebate.length < 1) {
                Util.MessageDialog("Please input rebate amount.");
                return false;
            }
                
            var allowList = $("#CompanyKeysAllowOnlyOneSubBusToAllTerminals").val();
            var subBusinessTypeCheck = $("#SubBusinessType").val();
            var saveStateCheck = false;

            if (allowList && allowList.length > 1 && allowList.indexOf(businessTypeKey) != -1) {

                $.each(savedRates, function (index, element) {
                    if (element.SubBusinessTypeID == subBusinessTypeCheck) {
                        saveStateCheck = true;
                        return;
                    }
                });

                if (saveStateCheck == false) {
                    Util.MessageDialog("Please add a Commission Rate/MSF for all sub-business types of this Member.");
                    return false;
                }
            }

            if (!IsDriverAuthAvailable(businessTypeKey)) {
                $("#DriverAuth").val("");
            }

            if (!RentalEndDateValidate || !RentalStartDateValidate) {
                return false;
            }

            if (!CheckVehicleTypeAfterAllocationTerminal()) {
                return false;
            }


            var purposeTypeName = IsSettingAccountForRequiredPurpose();
            if (purposeTypeName) {
                $("#ErrorMessage").text("The " + purposeTypeName + " purpose(s) doesn't link to any Bank Account");
                Util.MoveToTopOfPage();
                return false;
            }

            if ($("#textMerchantWebsite").hasClass("errorClass")) {
                $([document.documentElement, document.body]).animate({
                        scrollTop: $("#textMerchantWebsite").offset().top - 70
                    },
                    500);
                return;
            }

            if ($("#AmexMerchantID").hasClass("errorClass")) {
                $([document.documentElement, document.body]).animate({
                        scrollTop: $("#AmexMerchantID").offset().top - 70
                    },
                    500);
                return;
            }

            if ($('#ZipAPIKey').hasClass('input-validation-error')
                || $('#LocationID').hasClass('input-validation-error')){
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#ZipAPIKey").offset().top
                }, 500);

                return;
            }

            //Checking 
            ParsePurposeToJsonDataBeforeSave();

            if (!ValidateSubbusinessTypeBeforeSubmitToServer()) {
                return false;
            }

            $("#CusForm").submit();
        });


    $("#PPostCode").autocomplete({
        //source: '@Url.Action("AutoCompletePostCode")'
        source: urlOrigin + 'Member/AutoCompletePostCode'
    });

    $("#MPostCode").autocomplete({
        //source: '@Url.Action("AutoCompletePostCode")'
        source: urlOrigin + 'Member/AutoCompletePostCode'
    });

    LH_ChangeDebitRateDisplay = function (BusinessTypeVal) {
        var rdbDebitRatePercentage = $('#Percentage[name="rado"]');
        var rdbDebitRateDollar = $('#Dollar[name="rado"]');

        rdbDebitRatePercentage.attr('disabled', false);
        rdbDebitRateDollar.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1": //TaxiEpay
                {
                    rdbDebitRatePercentage.prop('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "18": //Live Eftpos
                {
                    rdbDebitRateDollar.prop('checked', true);
                    //Removed by PHW-1618
                    //rdbDebitRatePercentage.attr('disabled', true);
                    //NeedValidateMinValue = true;
                    NeedValidateMaxValue = false;
                }
                break;
            case "19": //DirectLiveEftpos
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "20": //TaxiPro
                {
                    rdbDebitRateDollar.attr('checked', true);
                    rdbDebitRatePercentage.attr('disabled', true);
                    //NeedValidateMinValue = true;
                    NeedValidateMaxValue = false;
                }
                break;
            case "21": //TaxiApp
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "22": //LiveEftpos black
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "23": //LiveEftpos black for taxi
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "24": //Live limo
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "25": //Glide
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "26": //Live Eftpos Integrated
                {
                    rdbDebitRateDollar.prop('checked', true);
                    rdbDebitRatePercentage.attr('disabled', true);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "28": //LiveSMS
                break;
        }
    };

    LH_ChangeQatasRateDisplay = function (BusinessTypeVal) {
        var rdbQatasRatePercentage = $('#Percentage[name="qantasrado"]');
        var rdbQatasRateDollar = $('#Dollar[name="qantasrado"]');

        rdbQatasRatePercentage.attr('disabled', false);
        rdbQatasRateDollar.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1": //TaxiEpay
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "18": //Live Eftpos
                {
                    //NeedValidateMinValue = true;
                    NeedValidateMaxValue = false;
                }
                break;
            case "19": //DirectLiveEftpos
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);

                }
                break;
            case "20": //TaxiPro
                {
                    rdbQatasRateDollar.attr('checked', true);
                    rdbQatasRatePercentage.attr('disabled', true);
                }
                break;
            case "21": //TaxiApp
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "22": //LiveEftpos black
                break;
            case "23": //LiveEftpos black for taxi
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "24": //Live limo
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "25": //Glide
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "26": //Live Eftpos Integrated
                {
                    rdbQatasRatePercentage.attr('checked', true);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                {
                    rdbQatasRatePercentage.attr('checked', true);
                }
                break;
            case "28": //LiveSMS
                {
                    rdbQatasRateDollar.attr('disabled', true);
                    rdbQatasRatePercentage.attr('disabled', true);
                }
                break;
        }
    };

    LH_ChangeDebitRateDisplay($("#BusinessType").val());
    LH_ChangeQatasRateDisplay($("#BusinessType").val());

    SetDefaultCommissionRate = function () {
        var BusinessTypeVal = $("#BusinessType").val();
        var isDebitRateDollarChecked = $("#CRBeditRateId #Dollar").parent().hasClass("checked");
        var commissionRate = LG_GetDefaultValueForCommissionRateByBusinessType(BusinessTypeVal, isDebitRateDollarChecked);
        //Remove commission rate section
        $("#CRDebitRate").val(commissionRate.DebitRate);
        $("#CRDebitRate").trigger("focusout");
        $("#CRQantasRate").val(commissionRate.Qantas);
        $("#CRQantasRate").trigger("focusout");
        $("#CRVisaRate").val(commissionRate.Visa);
        $("#CRVisaRate").trigger("focusout");
        $("#CRMasterCardRate").val(commissionRate.Master);
        $("#CRMasterCardRate").trigger("focusout");
        $("#CRAmexRate").val(commissionRate.Amex);
        $("#CRAmexRate").trigger("focusout");
        $("#CRDinersRate").val(commissionRate.Diners);
        $("#CRDinersRate").trigger("focusout");
        $("#CRUnionPayRate").val(commissionRate.UnionPay);
        $("#CRUnionPayRate").trigger("focusout");
        $("#CRZipPayRate").val(commissionRate.Zip);
        $("#CRZipPayRate").trigger("focusout");
        $("#CRAlipayRate").val(commissionRate.Alipay);
        $("#CRAlipayRate").trigger("focusout");
    }

    function LoadPaymentType(businessTypeId) {
        if (businessTypeId == "1" || businessTypeId == "23") {
            $('#PaymentType').children('option[value="Card"]').show();
        }
        else {
            if ($('#PaymentType').val() == "Card") {
                $('#PaymentType').val("Bank");
            }
            $('#PaymentType').children('option[value="Card"]').hide();
        }
    }

    $("#SubBusinessType").change(function() {
        var SubBusinessTypeKey = $("#SubBusinessType").val();

        if (SubBusinessTypeKey == 0) {
            $("#error-sub-business-type").show();
        } else {
            $("#error-sub-business-type").hide();
        }

        var jsonTerminals = $("#ListTerminal").val();
        if (!jsonTerminals) {
            jsonTerminals = "[]";
        }

        var jsonList = JSON.parse(jsonTerminals);

        var flg = true;
        $.each(jsonList, function (i, item) {
            if (item && item.VehicleModel.VehicleType.VehicleType_key != SubBusinessTypeKey) {
                flg = false;
                return;
            }
        });
        LH_UpdateAjaxAllocateTerminal();
        if (flg == false) {
            var newSubName = $("#SubBusinessType option:selected").text();
            Util.MessageDialog("The selected sub-business type is different from that of current terminal allocation(s). Please be advised that all terminal allocations will be moved to " + newSubName + ".","Warning");
        }
    });
    $("#BusinessType").change(function () {
        var BusinessTypeVal = $("#BusinessType").val();
        var MerchantIDVal = $("#MerchantID").val();
        LoadPaymentType(BusinessTypeVal);
        ShowHideTransactionFeeByBusiness(BusinessTypeVal);
        LH_HandleAddRemoveSubBusinessTypeDropdown(BusinessTypeVal);
        ShowHidePricingPlanCombobox(BusinessTypeVal);
        ShowHideRebateSection(BusinessTypeVal);
        EnableEOMMSFPaymentByType(BusinessTypeVal);
        EnableDisableSplitPaymentReport(BusinessTypeVal);
        AutoCheckEOMMSF(BusinessTypeVal);
        AutoCheckSplitPaymentReport(BusinessTypeVal);
        EnableTransactionList(BusinessTypeVal);
        ShowHideFeeFreeMemberCheckBox(BusinessTypeVal);

        var merchant = $('#MerchantID');

        if (BusinessTypeVal != 0) {
            var merchantIdDefault = $('option:selected', this).attr('custom-value');

            if (merchantIdDefault != "") {
                merchant.val(merchantIdDefault);
            }
            else {
                merchant.val(MerchantIDVal);
            }

            $('#error-message-vehicle-type').html("");
        }
        else {
            merchant.val('');
            $('#error-message-vehicle-type').html('Please choose a business type.');
        }
        var listMCCRequiredBusinessType = $("#ListMCCRequiredBusinessType").val();
        if (listMCCRequiredBusinessType) {
            var MCCRequiredArray = listMCCRequiredBusinessType.split(';');
            Util.CheckItemInlist(BusinessTypeVal, MCCRequiredArray,
                function () {
                    $("#MCCId .mandatory").show();
                    $("#MCC").prop('required', 'required');
                    $("#MCC").prop('aria-required', 'true');
                },
                function () {
                    $("#MCCId .mandatory").hide();
                    $("#MCC").prop('required', null);
                    $("#MCC").prop('aria-required', null);
                    var mess = $("#MCCId span>span>span");
                    if (mess != null && mess != undefined) {
                        mess.remove();
                    }
                });
        }

        SetDefaultCommissionRate();

        LH_UpdateDriverAuthDisplay(BusinessTypeVal);

        LH_GetDealer(BusinessTypeVal);
        LH_LoadRentalTemplate();
        LH_LoadCommissionRateLabel();
        LH_ChangeDebitRateDisplay(BusinessTypeVal);
        LH_ChangeQatasRateDisplay(BusinessTypeVal);
        LH_UpdateAjaxAllocateTerminal();
        GetSessionTerminalAllocationList();
        CheckVehicleTypeAfterAllocationTerminal();
        $.uniform.update();
    });

    $("#SamePrimaryAdd").change(function () {

        if ($("#SamePrimaryAdd").attr('checked')) {
            var StreetNo = $('#PStreetNo').val();
            var Street = $('#PStreet').val();
            var PostCode = $('#PPostCode').val();

            $('#MStreetNo').val(StreetNo);
            $('#MStreet').val(Street);
            $('#MPostCode').val(PostCode);
        } else {
            $('#MStreetNo').val('');
            $('#MStreet').val('');
            $('#MPostCode').val('');
        }
    });

    $("#MonthlyRental").change(function () {

        var MemberRentalTemplateKeyVal = $("#MonthlyRental").val();

        $("#TerminalRentalRateTbSF").empty();
        $("#TerminalRentalRateTb").show();
        $.ajax({
            //url: '@Url.Action("GetTerminalRentalRateList", "AjaxAdmin")',
            url: urlOrigin + 'AjaxAdmin/GetTerminalRentalRateList',
            //url: 'AjaxAdmin/GetTerminalRentalRateList',
            data: { MemberRentalTemplateKey: MemberRentalTemplateKeyVal },
            type: "GET",
            dataType: "json",
            success: function (dataRes) {
                $("#TerminalRentalRateTb").empty();
                var headerHtml = $("#tmplTerminalRental").render();
                var dataHtml = $("#tmplTerminalRentalList").render({ data: dataRes });
                $("#TerminalRentalRateTb").html(headerHtml + dataHtml);
            }
        });
    });

    $('#IsFeeFreeMember').change(function () {        
        var isFreeFee = $('#IsFeeFreeMember').prop('checked');
        var selectItemValue = 0;

        if (isFreeFee) {
            $("#MonthlyRental option[value=" + FeeFreeRentalTemplateValue + "]").show();
            selectItemValue = FeeFreeRentalTemplateValue;
        }
        else {
            $("#MonthlyRental option[value=" + FeeFreeRentalTemplateValue + "]").hide();
        }

        $("#MonthlyRental").val(selectItemValue);
        $("#MonthlyRental").trigger("change");
    });

    /*
     * This method will validate input number in Terminal Rental/Admin Fee
     * @param {Input element} element 
     * @returns {none} 
     */
    LH_ValidateInput = function (element) {
        var fullContent =
            PH_BuildFullContentWithKeyPressEvent($(element).val(), event.key, $(element)[0].selectionStart, false);
        if (isNaN(Number(fullContent))) {
            event.preventDefault();
        }

        if (fullContent < 0) {
            event.preventDefault();
        }

        if (fullContent.indexOf('.') != -1) {
            var str = fullContent.split('.');
            if (str[1].length > 2) {
                event.preventDefault();
            }
        }
    };
    LH_LoadRentalTemplate = function () {
        var val1 = $("#BusinessType").val();
        var hdnBusinessType = $("#hdnBusinessType").data('value');
        var hdnMonthlyRentalVal = $("#hdnRentalMemberRentalTemplateKey").data('value');
        var hdnFeeFreeMemberTemplateKey = $("#hdnFeeFreeMemberTemplateKey").data('value');

        var isFreeFee = $('#IsFeeFreeMember').prop('checked');

        if (!hdnBusinessType) {
            hdnBusinessType = 0;
        }
        if (!hdnMonthlyRentalVal || hdnBusinessType != val1) {
            hdnMonthlyRentalVal = 0;
        }

        $("#TerminalRentalRateTb").empty();
        $("#TerminalRentalRateTbSF").empty();
        $("#MonthlyRental").empty();

        $.ajax({
            url: urlOrigin + "AjaxAdmin/GetMemberRentalTemplate",
            data: { company_key: val1 },
            type: "GET",
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, item) {
                    if (item.MemberRentalTemplateKey == hdnFeeFreeMemberTemplateKey) {
                        FeeFreeRentalTemplateValue = item.MemberRentalTemplateKey;
                    } 

                    $("#MonthlyRental").append(
                        $("<option></option>").val(item.MemberRentalTemplateKey).html(item.Description)
                    );  
                });

                if (!isFreeFee) {
                    $("#MonthlyRental option[value=" + FeeFreeRentalTemplateValue + "]").hide();
                } else {
                    $("#MonthlyRental option[value=" + FeeFreeRentalTemplateValue + "]").show();
                }
     
                $("#MonthlyRental").val(hdnMonthlyRentalVal);
                $("#MonthlyRental").trigger("change");
            }
        });

    };

    LH_LoadCommissionRateLabel = function () {
        var val1 = $("#BusinessType").val();
        if (val1 == 20) {
            $("#CommissionRateBar").show();
            $("#CommissionRate").show();
            $("#MSFMessgae").hide();
            //$("#CRBeditRateId").hide();
            $("#MonthlyRentallbl").text("Admin Fee");
            $("#RentFreelbl").text("Free Admin Fee");
            $("#WeekReport").show();
        }
        if (val1 == 19) {
            $("#MonthlyRentallbl").text("Monthly Rental");
            $("#RentFreelbl").text("Rent Fee");
            $("#WeekReport").hide();
        }
        if (val1 == 1) {
            $("#CommissionRateBar").show();
            $("#CommissionRate").show();
            $("#MSFMessgae").hide();
            //$("#CRBeditRateId").hide();
            $("#MonthlyRentallbl").text("Admin Fee");
            $("#RentFreelbl").text("Free Admin Fee");
            $("#WeekReport").show();
        }
        if (val1 == 18) {
            $("#CommissionRateBar").hide();
            $("#CommissionRate").hide();
            $("#MSFMessgae").show();
            //$("#CRBeditRateId").show();
            $("#MonthlyRentallbl").text("Monthly Rental");
            $("#RentFreelbl").text("Rent Fee");
            $("#WeekReport").hide();
        }
        if (val1 == 22) {
            $("#CommissionRateBar").hide();
            $("#CommissionRate").hide();
            $("#MSFMessgae").show();
            //$("#CRBeditRateId").show();
            $("#MonthlyRentallbl").text("Monthly Rental");
            $("#RentFreelbl").text("Rent Fee");
            $("#WeekReport").hide();
        }
    };
    LH_LoadCommissionRateLabel();

    LH_UpdateDriverAuthDisplay = function (BusinessTypeVal) {
        if (IsDriverAuthAvailable(BusinessTypeVal)) {
            $("#DriverAuthId").show();
        }
        else {
            $("#DriverAuthId").hide();
        }
    };

    function IsDriverAuthAvailable(BusinessTypeVal) {
        var availableBusinessTypes = [Util.MemberBusinessType.TaxiEPay,
        Util.MemberBusinessType.LiveEftPosBlackForTaxi,
        Util.MemberBusinessType.TaxiPro
        ];
        return availableBusinessTypes.indexOf(BusinessTypeVal) != -1 ? true : false;
    }

    function IsAvailableForCash(BusinessTypeVal) {
        var availableBusinessTypes = [Util.MemberBusinessType.TaxiEPay,
        Util.MemberBusinessType.TaxiPro
        ];
        return availableBusinessTypes.indexOf(BusinessTypeVal) != -1 ? true : false;
    }

    LH_GetDealer = function (BusinessTypeVal) {
        $.ajax({
            //url: '@Url.Action("GetDealersByCompany", "AjaxAdmin")',
            url: urlOrigin + '/AjaxAdmin/GetDealersByCompany',
            data: { CompanyKey: BusinessTypeVal },
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('#Dealer').append('<option value="0">Select One</option>');
                $.each(data,
                    function (i, item) {
                        $('#Dealer').append(
                            '<option value="' +
                            item.Dealer_key +
                            '">' +
                            item.DealerName +
                            '</option>');
                    });
            },
            error: function () {
                alert("Error");
            }
        });
    };

    LH_GetSubBusinessType = function (BusinessTypeVal) {
        var subbusinessTypeEle = $("#subbusinessTypeSection");
        if (subbusinessTypeEle.length == 0) {
            return false;
        }

        if (subbusinessTypeEle.hasClass("ManualRemoved")) {
            return false;
        }
        
        $.ajax({
            url: urlOrigin + '/AjaxAdmin/GetSubBusinessType',
            data: { CompanyKey: BusinessTypeVal },
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log($("#SubBusinessType").val());
                $('#SubBusinessType').empty();
                $('#SubBusinessType').append(
                    '<option value="0">Select Sub-business Type</option>');
                $.each(data,
                    function (i, item) {
                        var curr = $("#subkey").val();
                        if (item.VehicleType_key == curr) {
                            $('#SubBusinessType').append(
                                '<option value="' +
                                item.VehicleType_key +
                                '" selected="selected">' +
                                item.Name +
                                '</option>');
                        } else {
                            $('#SubBusinessType').append(
                                '<option value="' +
                                item.VehicleType_key +
                                '">' +
                                item.Name +
                                '</option>');
                        }
                        
                    });
                $("#error-sub-business-type").hide();
            },
            error: function () {
                alert("Error");
            }
        });
    };

    LH_HandleAddRemoveSubBusinessTypeDropdown = function (BusinessTypeVal) {
        var allowList = $("#CompanyKeysAllowOnlyOneSubBusToAllTerminals").val();
        if (!allowList || allowList.length == 0) {
            $("#subbusinessTypeSection").addClass("ManualRemoved");
            return;
        }

        var allowListArr = allowList.split(";");
        if (allowListArr.indexOf(BusinessTypeVal) == -1) {
            $("#subbusinessTypeSection").addClass("ManualRemoved");
            return;
        }
        $("#subbusinessTypeSection").removeClass("ManualRemoved");
        LH_GetSubBusinessType(BusinessTypeVal);
    }


    $("#CRDebitRate").focusout(function () {

        var fieldname = "#CRDebitRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Debit rate";
        var placement = "right";
        var isDollarChecked = $("#CRBeditRateId #Dollar").parent().hasClass("checked");

        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, -0.1);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, -0.1);
                } else {
                    result = validateAmount(fieldname, -5, -0.5);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRVisaRate").focusout(function () {
        var fieldname = "#CRVisaRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Visa rate";
        var placement = "right";

        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRMasterCardRate").focusout(function () {

        var fieldname = "#CRMasterCardRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Master Card rate";
        var placement = "right";

        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRAmexRate").focusout(function () {

        var fieldname = "#CRAmexRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Amex rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRDinersRate").focusout(function () {

        var fieldname = "#CRDinersRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Diners rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRUnionPayRate").focusout(function () {

        var fieldname = "#CRUnionPayRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Union Pay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRZipPayRate").focusout(function () {

        var fieldname = "#CRZipPayRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Zip Pay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRQantasRate").focusout(function () {

        var fieldname = "#CRQantasRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Qantas rate";
        var placement = "right";
        var result = false;
        var isDollarChecked = $("#CRQantasRateId #Dollar").parent().hasClass("checked");
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRAlipayRate").focusout(function () {

        var fieldname = "#CRAlipayRate";
        var val1 = $("#BusinessType").val();
        var ratename = "Alipay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    function enter(elem) {
        elem.style.color = 'lightblue';
    }
    function leave(elem) {
        elem.style.color = 'white';
    }

    var val1 = $("#SalesforceId").val();
    var text = "LiveEftPos";
    if (val1 != null || val1 != "") {
        $("select option").filter(function () {
            return $(this).text() == text;
        }).prop('selected', true);
        //BusinessType
    }

    $("#CRDebitRate, #CRVisaRate, #CRMasterCardRate, #CRAmexRate, #CRDinersRate, #CRUnionPayRate, #CRZipPayRate, #CRQantasRate, #CRAlipayRate ").keypress(function () {
        var currentValue = $(this).val();
        var selectionStart = $(this)[0].selectionStart;
        var fullContent = PH_BuildFullContentWithKeyPressEvent(currentValue, event.key, selectionStart);

        if (isNaN(Number(fullContent))) {
            event.preventDefault();
        }
    });
    $("#BusinessType").trigger("change");

    function LH_UpdateAjaxAllocateTerminal() {
        var BusinessTypeVal = $("#BusinessType").val();
        var subbusinessTypeKey = $("#SubBusinessType").val();
        var self = $('.AjaxAllocateTerminal');
        var href = self.attr('href');
        href = href.substring(0, href.lastIndexOf('/') + 1) + BusinessTypeVal + "?subBusinessTypeKey=" + subbusinessTypeKey;
        self.attr('href', href);
    }

    $('#textQBR').change(function() {
        $('#messageQBR').html('');
        if ($(this).val().trim() !== "")
        {
            $('#IsQBRvalidated').val('-2');
            $('#submitResult').prop('disabled', true);
        }
        else
        {
            $('#IsQBRvalidated').val('0');
            $('#submitResult').prop('disabled', false);
        }
    });
});

function fnOnBegin() {
    if ($('#BusinessType').val() == 0) {
        Util.MessageDialog("Please choose a business type.");
        return false;
    }

    if (!ValidateSubbusinessTypeBeforeSubmitToServer()) {
        Util.MessageDialog("Please choose a sub-business type.");
        return false;
    }
    return true;
}

var dataSessionTerminalAllocationList = null;

function GetSessionTerminalAllocationList() {
    var url = $('#hdSessionTerminalAllocationListUrl').val();
    $.ajax({
        url: url,
        async: false,
        success: function (result) {
            dataSessionTerminalAllocationList = result;
        },
        error: function (result) {
            console.log(result);
            dataSessionTerminalAllocationList = null;
        }
    });
}

function CheckVehicleTypeAfterAllocationTerminal() {
    if (dataSessionTerminalAllocationList == null) {
        dataSessionTerminalAllocationList = [];
    }
    var flg = true;
    var businessTypesSel = $('#BusinessType');
    var businessTypeId = businessTypesSel.val();
    $.each(dataSessionTerminalAllocationList, function (i, item) {
        if (item.VehicleModel.VehicleType.Company_Key != businessTypeId) {
            flg = false;
            return;
        }
    });
    var errTag = $('#error-vehicle-type');
    if (flg) {
        errTag.hide();
    } else {
        businessTypesSel.focus();
        errTag.show();
    }
    return flg;
}

function ValidateSubbusinessTypeBeforeSubmitToServer() {
    var businessTypeKey = $('#BusinessType').val();
    //Sub-business type validation
    var allowList = $("#CompanyKeysAllowOnlyOneSubBusToAllTerminals").val();
    if (!allowList || allowList.length == 0) {
        return true;
    }

    var allowListArr = allowList.split(";");
    if (allowListArr.indexOf(businessTypeKey) == -1) {
        return true;
    }

    var subbusinessTypeKey = $("#SubBusinessType").val();
    if (subbusinessTypeKey == 0) {
        $("#SubBusinessType").focus();
        $("#error-sub-business-type").show();
        return false;
    } else {
        $("#error-sub-business-type").hide();
        return true;
    }
}

function OnChangeZipMerchantAPI() {

    if ($("#ZipAPIKey").val() || $("#LocationID").val()) {
        $("#ZipAPIKey").attr("required", true);
        $("#LocationID").attr("required", true);
    } else {
        $("#ZipAPIKey").attr("required", false);
        $("#LocationID").attr("required", false);
    }

}

function ShowHideTransactionFeeByBusiness(BusinessTypeVal){
    var businessAllowTranFee = $('#BusinessKeysAllowFixedFee').val().split(';');

    var newPayMethod = $('div[role=NewPayMethod]');
    var tranFees = $('input.TranFee');

    if (businessAllowTranFee.indexOf(BusinessTypeVal) < 0)
    {
        $('label.TranFee').hide();
        $.each(newPayMethod, function (index, elem){
            $(elem).hide();
        });

        $.each(tranFees, function (index, elem){
            $(elem).parent().hide();
        });
    }
    else
    {
        $('label.TranFee').show();
        $.each(newPayMethod, function (index, elem){
            $(elem).show();
        });

        $.each(tranFees, function (index, elem){
            $(elem).parent().show();
        });
    }
}

function ShowHidePricingPlanCombobox(BusinessTypeVal){
    var businessAllowTranFee = $('#BusinessKeysAllowFixedFee').val().split(';');
    var pricingPlanRow = $('#PricingPlanRow');

    if (businessAllowTranFee.indexOf(BusinessTypeVal) < 0)
    {
        $('#PricingPlanRow').hide();
    }
    else
    {
        $('#PricingPlanRow').show();
    }
}

function EnableEOMMSFPaymentByType(BusinessTypeVal) {
    var EOMMSF = $("input[name='EOMMSFPayment'][type='checkbox']");
    var ListEOMMSFPaymentEnableBusinessType = $("#ListEOMMSFPaymentEnableBusinessType").val();
    if (ListEOMMSFPaymentEnableBusinessType) {
        var paymenttypes = ListEOMMSFPaymentEnableBusinessType.split(';');
        Util.CheckItemInlist(BusinessTypeVal,
            paymenttypes,
            function() {
                if (EOMMSF.length > 0) {
                    Util.EnableCheckbox("EOMMSFPayment");
                }
            },
            function() {
                if (EOMMSF.length > 0) {
                    Util.DisableCheckbox("EOMMSFPayment");
                    $(EOMMSF).removeAttr('checked');
                    $(EOMMSF).closest("span").removeClass("checked");
                } else {
                    var EOM = $("#EOMMSFPayment");
                    EOM.val("false");
                    $(EOM).closest("div").find("span").removeClass("checked");
                }
            });
    }

}

function EnableDisableSplitPaymentReport(businessTypeId) {
    var allowList = $("#BusinessTypeAllowSplitPaymentReport").val();
    if (allowList.split(",").indexOf(businessTypeId) < 0) {
        Util.DisableCheckbox("EnableSplitPaymentReport");
    } else {
        Util.EnableCheckbox("EnableSplitPaymentReport");
    }
}

function AutoCheckEOMMSF(businessTypeId){
    var allowList = $("#BusinessTypeAutoEnableEOMMSFPayment").val();
    if (allowList.split(";").indexOf(businessTypeId) < 0) {
        $('#EOMMSFPayment').prop('checked', false);
    } else {
        $('#EOMMSFPayment').prop('checked', true);
    }
}

function ShowHideFeeFreeMemberCheckBox(businessTypeId) {
    var businessTypeAllowFeeFree = $('#BusinessTypeAllowFeeFree').val().split(';');
    if (businessTypeAllowFeeFree.indexOf(businessTypeId) !== -1) {
        $('#FeeFreeMemberSection').show();
        if ($('#IsFeeFreeMember').prop('checked'))//for first load at edit page
        {
            $('#IsFeeFreeMember').trigger('change');
        }
    } else {
        if ($('#IsFeeFreeMember').prop('checked'))//set checked false only when switch from Live Eftpos to another
        {
            $('#IsFeeFreeMember').prop('checked',false);
            $('#IsFeeFreeMember').trigger('change');
        }
        $('#FeeFreeMemberSection').hide();
    }
}

function AutoCheckSplitPaymentReport(businessTypeId){
    var allowList = $("#BusinessTypeAutoEnableSplitPaymentReport").val();
    if (allowList.split(";").indexOf(businessTypeId) < 0) {
        $('#EnableSplitPaymentReport').prop('checked', false);
    } else {
        $('#EnableSplitPaymentReport').prop('checked', true);
    }
}

function EnableTransactionList(businessTypeId) {
    var allowList = $("#BusinessEnableTransactionList").val();
    if (allowList.split(";").indexOf(businessTypeId) < 0) {
        $('#TransactionList').hide();
        $('#EnableTransactionListOnEOMReport').prop('checked', false);
    } else {
        $('#TransactionList').show();
    }
}


$("#validateQBR").on('click',
    function() {
        validateQBRNumber();
    });

function validateQBRNumber() {
    //trim the text and set text to self.
    $("#textQBR").val($("#textQBR").val().trim());
    var editQbr = $("#textQBR").val().trim();
    if (!editQbr) {
        $('#messageQBR').html('Please enter QBR number to validate.');
        return false;
    }
    $.ajax({
        type: "GET",
        url: baseURL + "Member/ValidateQBRNumber",
        data: {
            qbrNumber: editQbr
        },
        beforeSend: function() {
            $('#messageQBR').html('connecting..');
        },
        success: function(data) {
            if (!data.IsSuccess) {
                $('#messageQBR').html("No details were found for QBR #" + editQbr);
                $('#IsQBRvalidated').val('-1');
            } else {
                $('#messageQBR').html("QBR #" + editQbr + " is successfully verified and found");
                $('#IsQBRvalidated').val('1');
                $('#submitResult').prop('disabled', false);
            }
        },
        error: function(jqXHR, textStatus, errorThrown, data) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#messageQBR').html("An error occured while connecting to server - " +
                jqXHR.message +
                "\nDetails: " +
                msg);

            $('#IsQBRvalidated').val('-2');
        }
    });
}
