﻿(function (DownloadAutomationConfigService, $, undefined) {
    DownloadAutomationConfigService.GetiLinkAutomationConfig = function (baseURL) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "iLinkConfig/GetiLinkAutomationConfig",
            type: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    DownloadAutomationConfigService.UpdateiLinkAutomationConfig = function (baseURL, config) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "iLinkConfig/UpdateiLinkAutomationConfig",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(config),     
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        GetiLinkAutomationConfig: DownloadAutomationConfigService.GetiLinkAutomationConfig,
        UpdateiLinkAutomationConfig: DownloadAutomationConfigService.UpdateiLinkAutomationConfig
    };

}(window.DownloadAutomationConfigService = window.DownloadAutomationConfigService || {}, jQuery));