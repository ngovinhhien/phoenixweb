﻿var baseURL = Util.PH_UpdateURL();
var oldData = {
    iLinkAllowedFileExtensions: "",
    iLinkFilenameConvention: "",
    iLinkAutomaticErrorReceivers: "",
    iLinkAutomaticResultReceivers: ""
};
$(function () {
    getiLinkAutomationConfig();
    $("#btn-discard").click(function () {
        $("#iLinkAllowedFileExtensions").val(oldData.iLinkAllowedFileExtensions);
        $("#iLinkFilenameConvention").val(oldData.iLinkFilenameConvention);
        $("#iLinkAutomaticErrorReceivers").val(oldData.iLinkAutomaticErrorReceivers);
        $("#iLinkAutomaticResultReceivers").val(oldData.iLinkAutomaticResultReceivers);
        Util.DisableControlById(["btn-discard", "btn-save"]);
    });
    $("#btn-save").click(function (e) {
        e.preventDefault();
        var iLinkAllowedFileExtensions = $("#iLinkAllowedFileExtensions").val();
        var iLinkFilenameConvention = $("#iLinkFilenameConvention").val();
        var iLinkAutomaticErrorReceivers = $("#iLinkAutomaticErrorReceivers").val();
        var iLinkAutomaticResultReceivers = $("#iLinkAutomaticResultReceivers").val();
        var iLinkAutomationConfig = {
            iLinkAllowedFileExtensions: iLinkAllowedFileExtensions,
            iLinkFilenameConvention: iLinkFilenameConvention,
            iLinkAutomaticErrorReceivers: iLinkAutomaticErrorReceivers,
            iLinkAutomaticResultReceivers: iLinkAutomaticResultReceivers
        }
        if (validate(iLinkAutomationConfig)) {
            Util.ConfirmDialog("Do you want to update the configuration?", "Message").done(function () {
                LG_ShowProgressMask();
                updateiLinkAutomationConfig(iLinkAutomationConfig);
                
            });
        }
    });
}); // end of document ready

getiLinkAutomationConfig = function () {
    DownloadAutomationConfigService.GetiLinkAutomationConfig(baseURL).done(function (result) {
        if (result != null && result != undefined) {
            if (result.Success == false) {
                Util.MessageDialog([result.Message], "Error");
            } else {
                if (result.Data != null && result.Data != undefined) {
                    oldData = result.Data;
                    $("#iLinkAllowedFileExtensions").val(oldData.iLinkAllowedFileExtensions);
                    $("#iLinkFilenameConvention").val(oldData.iLinkFilenameConvention);
                    $("#iLinkAutomaticErrorReceivers").val(oldData.iLinkAutomaticErrorReceivers);
                    $("#iLinkAutomaticResultReceivers").val(oldData.iLinkAutomaticResultReceivers);
                }
            }
        }
        for (var prop in oldData) {
            $("#" + prop).change(function (e) {
                Util.CheckDirty(e, function () {
                    Util.DisableControlById(["btn-discard", "btn-save"], false);
                });
            });
        }
        Util.DisableControlById(["btn-discard", "btn-save"], true);
    });
}

updateiLinkAutomationConfig = function (iLinkCredential) {
    DownloadAutomationConfigService.UpdateiLinkAutomationConfig(baseURL, iLinkCredential).done(function (result) {
        LG_HideProgressMask();
        if (result.IsSuccess) {
            Util.MessageDialog(["The configuration has been saved successfully"], "Saved Successfully");
            getiLinkAutomationConfig();
        } else {
            Util.MessageDialog([result.Message], "Error");
        }
    });
}

validate = function (iLinkAutomationConfig) {
    var valObj = {
        flag: true,
        errors: []
    }
    if (iLinkAutomationConfig == null || iLinkAutomationConfig == undefined) {
        valObj.errors.push("Error!");
        showMessage('messageDialog', 'Error', error);
        return false;
    }
   
    if (Util.CheckNotEmptyValue(iLinkAutomationConfig.iLinkAllowedFileExtensions, "file extension", valObj)) {
        checkValidInput(iLinkAutomationConfig.iLinkAllowedFileExtensions, "file extension", valObj);
    }
    if (Util.CheckNotEmptyValue(iLinkAutomationConfig.iLinkFilenameConvention, "file name convention", valObj)) {

    }

    if (Util.CheckNotEmptyValue(iLinkAutomationConfig.iLinkAutomaticErrorReceivers, "error notificcation receivers", valObj)) {
        checkValidInput(iLinkAutomationConfig.iLinkAutomaticErrorReceivers, "error notificcation receivers", valObj);
    }

    if (Util.CheckNotEmptyValue(iLinkAutomationConfig.iLinkAutomaticResultReceivers, "result receivers", valObj)) {
        checkValidInput(iLinkAutomationConfig.iLinkAutomaticResultReceivers, "result receivers", valObj);
    }

    if (valObj.errors.length > 0)
        Util.MessageDialog(valObj.errors, "Error");
    return valObj.flag;
}

checkValidInput = function (value, displayName, valObj) {
    if (valObj.errors == null || valObj.errors == undefined)
        valObj.errors = [];
    if (value.indexOf(',') != -1) {
        valObj.errors.push(displayName + ' is invalid. Please use semicolon as seperator');
        valObj.flag = false;
    }
}

