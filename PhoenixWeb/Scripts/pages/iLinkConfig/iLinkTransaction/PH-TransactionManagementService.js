﻿(function (TransactionManagementService, $, undefined) {
    TransactionManagementService.GetImportedTransaction = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'iLinkConfig/GetImportedTransaction',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchParams),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    TransactionManagementService.ExportImportedTransaction = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'iLinkConfig/ExportImportedTransaction',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchParams),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    TransactionManagementService.ManuallyImportTransaction = function (baseURL, file) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'iLinkConfig/ManuallyImportTransaction',
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data: file,
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        GetImportedTransaction: TransactionManagementService.GetImportedTransaction,
        ManuallyImportTransaction: TransactionManagementService.ManuallyImportTransaction
    };

}(window.TransactionManagementService = window.TransactionManagementService || {}, jQuery));