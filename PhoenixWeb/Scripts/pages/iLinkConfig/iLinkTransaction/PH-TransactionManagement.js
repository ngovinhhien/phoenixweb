﻿var urlOrigin = Util.PH_UpdateURL();
var sortParams = {
    sortField: '',
    sortOrder: '',
};
var searchParams = {
    Request: {}
};

$(document).ready(function () {
    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var currentDate = day + "-" + (month) + "-" + now.getFullYear();
    $('#TransactionDateFrom').datepicker("setDate", currentDate);
    $('#TransactionDateTo').datepicker("setDate", currentDate);

    $("#TransactionDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#TransactionDateTo").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#TransactionDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#TransactionDateFrom").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $("#ImportedDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#ImportedDateTo").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#ImportedDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#ImportedDateFrom").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $(window).bind('resize', function () {
        $("#TransactionGrid").setGridWidth($("#wrapper").width());
    }).trigger('resize');

    initTransactionGrid();
    initTransactionRejectGrid();
    setTimeout(function () {
        $("#TransactionGrid").setGridWidth($("#wrapper").width());
    }, 300)

    $('#btnSearch').click(function (e) {
        e.preventDefault();
        var urlParams = new URLSearchParams(window.location.search);
        var filterParams = getFilterParams();

        if (!ValidateParams(filterParams)) {
            return;
        }

        searchParams = {
            PageIndex: 1,
            PageSize: parseInt($('[id^=TransactionGridPager] select.ui-pg-selbox').val()),
            Request: filterParams
        };
        loadDataTransactionGrid(searchParams);
        TransactionGridEvents(searchParams);
    });

    $('#btnExport').click(function (e) {
        e.preventDefault();
        var filterParams = getFilterParams();

        if (!ValidateParams(filterParams)) {
            return;
        }
        window.location.href =
            urlOrigin +
            "iLinkConfig/ExportImportedTransaction?TransactionDateFrom=" +
            filterParams.TransactionDateFrom +
            "&TransactionDateTo=" +
            filterParams.TransactionDateTo +
            "&ImportedDateFrom=" +
            filterParams.ImportedDateFrom +
            "&ImportedDateTo=" +
            filterParams.ImportedDateTo +
            "&CardNumber=" +
            filterParams.CardNumber +
            "&TransactionNumber=" +
            filterParams.TransactionNumber +
            "&AuthorisationNumber=" +
            filterParams.AuthorisationNumber +
            "&MemberID=" +
            filterParams.MemberID +
            "&TerminalID=" +
            filterParams.TerminalID +
            "&CardType=" +
            filterParams.CardType +
            "&AmountFrom=" +
            filterParams.AmountFrom +
            "&AmountTo=" +
            filterParams.AmountTo +
            "&BatchNumber=" +
            filterParams.BatchNumber +
            "&Approved=" +
            filterParams.Approved +
            "&Declined=" +
            filterParams.Declined +
            "&SourceFile=" +
            filterParams.SourceFile +
            "&TransactionType=" +
            filterParams.TransactionType +
            "&IsVoid=" +
            filterParams.IsVoid;
    });

    $('#manuallyFile').change(function (evt) {

        if ($('#manuallyFile')[0].files.length > 0)
        {
            LG_ShowProgressMask();

            var fileData = new FormData();
            fileData.append("manuallyFile", $('#manuallyFile')[0].files[0]);

            TransactionManagementService.ManuallyImportTransaction(urlOrigin, fileData)
                .done(function (result) {
                    if (result.IsError != true) {
                        Util.MessageDialog(result.TotalItems + ' transaction(s) have been imported successfully.', 'Information');
                    }
                    else {
                        var grid = $('#TransactionRejectGrid')[0];
                        $('#TransactionRejectGrid').jqGrid('clearGridData');

                        if (result.Response && result.Response.length > 0) {
                            $('#TransactionRejectTitle').html(result.TotalItems + ' transaction(s) cannot be imported. Please contact support.')
                            $('#TransactionRejectGrid').setGridParam({ data: result.Response, rowNum: result.PageSize }).trigger('reloadGrid');
                            $("#TransactionRejectDialog").dialog({ width: 'auto' });
                        }
                        else
                            Util.MessageDialog(result.Message, "Error");
                    }

                    LG_HideProgressMask();
                })
                .fail(function (err) {
                    LG_HideProgressMask();
                })
                .always(function () {
                    $('#manuallyFile').val('');
                    LG_HideProgressMask();
                });
        }
    });

    $('#btnManuallyImport').click(function (e) {
        e.preventDefault();
        $('#manuallyFile').trigger('click');
    });
});


function getFilterParams() {
    return {
        TransactionDateFrom: $('#TransactionDateFrom').val() ? $('#TransactionDateFrom').val() : '',
        TransactionDateTo: $('#TransactionDateTo').val() ? $('#TransactionDateTo').val() : '',
        ImportedDateFrom: $('#ImportedDateFrom').val() ? $('#ImportedDateFrom').val() : '',
        ImportedDateTo: $('#ImportedDateTo').val() ? $('#ImportedDateTo').val() : '',
        CardNumber: $('#CardNumber').val() ? $('#CardNumber').val().trim() : '',
        TransactionNumber: $('#TransactionNumber').val() ? $('#TransactionNumber').val().trim() : '',
        AuthorisationNumber: $('#AuthorisationNumber').val() ? $('#AuthorisationNumber').val().trim() : '',
        MemberID: $('#MemberID').val() ? $('#MemberID').val().trim() : '',
        TerminalID: $('#TerminalID').val() ? $('#TerminalID').val().trim() : '',
        CardType: $('#CardType').val() ? $('#CardType').val() : '',
        AmountFrom: $('#AmountFrom').val() ? convertAmount($('#AmountFrom').val()) : '',
        AmountTo: $('#AmountTo').val() ? convertAmount($('#AmountTo').val()) : '',
        BatchNumber: $('#BatchNumber').val() ? $('#BatchNumber').val().trim() : '',
        Approved: ($('#Approved').is(":checked")),
        Declined: ($('#Declined').is(":checked")),
        SourceFile: $('#SourceFile').val() ? $('#SourceFile').val().trim() : '',
        TransactionType: $('#TransactionType').val() ? $('#TransactionType').val() : '',
        IsVoid: $('#IsVoid').val() ? $('#IsVoid').val() : '',
        SortField: sortParams.sortField,
        SortOrder: sortParams.sortOrder
    }
}

function ValidateParams(filterParams) {
    var errorList = [];
    var hasTransDate = true;
    var hasImportDate = true;
    if (filterParams.TransactionDateFrom == '' || filterParams.TransactionDateTo == '') {
        hasTransDate = false;
    }
    if (filterParams.ImportedDateFrom == '' || filterParams.ImportedDateTo == '') {
        hasImportDate = false;
    }
    if (!hasTransDate && !hasImportDate) {
        errorList.push('Cannot leave both Transaction Date and Imported Date blank');
    }
    if (filterParams.AmountFrom != '' && filterParams.AmountTo != '') {
        if (filterParams.AmountFrom > filterParams.AmountTo) {
            errorList.push('Amount to must be equal or greater than amount from');
        }
    }
    if (errorList.length > 0) {
        Util.MessageDialog(errorList, 'Error');
        return false;
    }
    return true;
}

function convertAmount(amount) {
    var pre = '';
    var suf = '';
    if (amount.includes('.')) {
        var dotIndex = amount.indexOf('.');
        pre = dotIndex == 0 ? '0' : amount.substring(0, dotIndex);
        if (dotIndex != amount.length - 1)
            suf = amount.substring(dotIndex + 1);
        if (suf.length > 2)
            suf = suf.substring(0, 2);
        else {
            suf = (suf + '00').substring(0, 2);
        }

    } else {
        pre = amount;
        suf = '00';
    }
    var result = '000000000000000' + pre + suf;
    return result.substring(result.length - 15, result.length);
}


function BuildColName() {
    var colNames = [
        "Transaction No",
        "Merchant Name",
        "Merchant BSB/Acc Number",
        "MemberID",
        "MerchantID",
        "TerminalID",
        "Status",
        "Amount",
        "Cash Out",
        "Transaction Date Time",
        "Transaction Type",
        "Authorisation",
        "Batch Number",
        "Card Type",
        "Account Type",
        "Card Number",
        "Source File Name",
        "Card Entry Mode",
        "Is Void?",
        "Imported Date"
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "TransactionNumber", index: "TransactionNumber", width: "100", align: 'center', editable: false, sortable: true },
        { key: false, name: "MerchantName", index: "MerchantName", width: "90", align: 'center', editable: false, sortable: false },
        { key: false, name: "MerchantBsbAccNumber", index: "MerchantBsbAccNumber", width: "145", align: 'center', editable: false, sortable: false },
        { key: false, name: "MemberID", index: "MemberID", width: "75", align: 'center', editable: false, sortable: false },
        { key: false, name: "MerchantID", index: "MerchantID", width: "75", align: 'center', editable: false, sortable: false },
        { key: false, name: "TerminalID", index: "TerminalID", width: "75", align: 'center', editable: false, sortable: false },
        { key: false, name: "ApproveStatus", index: "ApproveStatus", width: "70", align: 'center', editable: false, sortable: false },
        {
            key: false, name: "Amount", index: "Amount", width: "70", align: 'center', editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0.00;
                }
                return value;
            }
        },
        {
            key: false, name: "CashOut", index: "CashOut", width: "70", align: 'center', editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0.00;
                }
                return value;
            }
        },
        {
            key: false, name: "TransactionDate", index: "TransactionDate", width: "125", align: 'center', editable: false, sortable: true,
            formatter: formatDate
        },
        { key: false, name: "TransactionType", index: "TransactionType", width: "120", align: 'center', editable: false, sortable: false },
        { key: false, name: "AuthorisationNumber", index: "AuthorisationNumber", width: "80", align: 'center', editable: false, sortable: true },
        { key: false, name: "BatchNumber", index: "BatchNumber", width: "80", align: 'center', editable: false, sortable: false },
        { key: false, name: "CardType", index: "CardType", width: "70", align: 'center', editable: false, sortable: false },
        { key: false, name: "AccountType", index: "AccountType", width: "85", align: 'center', editable: false, sortable: false },
        { key: false, name: "CardNumber", index: "CardNumber", width: "90", align: 'center', editable: false, sortable: false },
        { key: false, name: "FileName", index: "FileName", width: "220", align: 'center', editable: false, sortable: false },
        { key: false, name: "CardEntryMode", index: "CardEntryMode", width: "90", align: 'center', editable: false, sortable: false },
        { key: false, name: "IsVoid", index: "IsVoid", width: "70", align: 'center', editable: false, sortable: false },
        {
            key: false, name: "ImportedDate", index: "ImportedDate", width: "100", align: 'center', editable: false, sortable: true,
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y"
            }
        }
    ];

    return colModel;
}

function initTransactionGrid() {
    $("#TransactionGrid").jqGrid({
        url: "",
        colNames: BuildColName(),
        colModel: BuildColModel(),
        pager: "#TransactionGridPager",
        rowNum: 20,
        loadui: "disable",
        rowList: [20, 50, 100, 200],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No result found",
        height: 500,
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: false,
        shrinkToFit: false,
        multiselect: false
    });
}

function BuildRejectColName() {
    var colNames = [
        "Terminal Number",
        "Processing Date",
        "Transaction Date",
        "Transaction Type",
        "Transaction Number",
        "Transaction Time",
        "Transaction Amount",
        "Card Number",
        "Card Type",
        "Account Type",
        "Authorisation Number",
        "Void Flag",
        "Card Entry Method",
        "Merchant BSB/Account Number",
        "Merchant Name",
        "Merchant Location",
        "Cash out or Tip amount"
    ];

    return colNames;
}

function BuildRejectColModel() {
    var colModel = [
        { key: false, name: "TerminalNumber", index: "TerminalNumber", width: "95", align: 'center', ediable: false, sortable: false },
        { key: false, name: "ProcessingDate", index: "ProcessingDate", width: "90", align: 'center', ediable: false, sortable: false, formatter: formatDate  },
        { key: false, name: "TransactionDate", index: "TransactionDate", width: "90", align: 'center', editable: false, sortable: true, formatter: formatDate },
        { key: false, name: "TransactionType", index: "TransactionType", width: "95", align: 'center', ediable: false, sortable: false },
        { key: false, name: "TransactionNumber", index: "TransactionNumber", width: "110", align: 'center', ediable: false, sortable: false },
        { key: false, name: "TransactionTime", index: "TransactionTime", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "TransactionAmount", index: "TransactionAmount", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "CardNumber", index: "CardNumber", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "CardType", index: "CardType", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "AccountType", index: "AccountType", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "AuthorisationNumber", index: "AuthorisationNumber", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "IsVoid", index: "IsVoid", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "CardEntryMethod", index: "CardEntryMethod", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "MerchantBSBAccountNumber", index: "MerchantBSBAccountNumber", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "MerchantName", index: "MerchantName", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "MerchantLocation", index: "MerchantLocation", width: "100", align: 'center', ediable: false, sortable: false },
        { key: false, name: "CashoutOrTipAmount", index: "CashoutOrTipAmount", width: "100", align: 'center', ediable: false, sortable: false }
    ];

    return colModel;
}

function initTransactionRejectGrid() {
    $("#TransactionRejectGrid").jqGrid({
        url: "",
        colNames: BuildRejectColName(),
        colModel: BuildRejectColModel(),
        pager: "#TransactionRejectGridPager",
        loadui: "disable",
        height: "100%",
        viewrecords: true,
        emptyrecords: "No result found",
        height: 500,
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: false,
        shrinkToFit: false,
        multiselect: false,
        loadonce: true,
        datatype: "local"
    });
}

function formatDate(cellValue, option, rowObject) {
    if (cellValue == null || cellValue == undefined)
        return '';
    return cellValue.substring(6, 8) + "/" + cellValue.substring(4, 6) + "/" + cellValue.substring(0, 4)
        + ' ' + cellValue.substring(8, 10) + ":" + cellValue.substring(10, 12);
}

function loadDataTransactionGrid(searchParams) {
    LG_ShowProgressMask();
    TransactionManagementService.GetImportedTransaction(urlOrigin, searchParams)
        .done(function (result) {
            if (result.IsError != true) {
                var grid = $('#TransactionGrid')[0];
                $('#TransactionGrid').jqGrid('clearGridData');
                if (result.Response.length > 0)
                    grid.addJSONData(result);
                else
                    Util.MessageDialog('No result found', 'Information');
            }
            else {
                Util.MessageDialog(result.Message, "Error");
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            LG_HideProgressMask();
        });
}

function TransactionGridEvents(searchParams) {
    $('#TransactionGrid').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#TransactionGrid').getGridParam('page');
            var rowNum = $('#TransactionGrid').getGridParam('rowNum');

            if (pgButton == "next_TransactionGridPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_TransactionGridPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_TransactionGridPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_TransactionGridPager") {
                if ($('#TransactionGrid').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#TransactionGrid').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#TransactionGrid').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='TransactionGridPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadDataTransactionGrid(searchParams);
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            sortParams.sortField = index;
            sortParams.sortOrder = sortOrder;
            searchParams.Request.SortField = sortParams.sortField;
            searchParams.Request.SortOrder = sortParams.sortOrder;
            loadDataTransactionGrid(searchParams);
        }
    });
}
