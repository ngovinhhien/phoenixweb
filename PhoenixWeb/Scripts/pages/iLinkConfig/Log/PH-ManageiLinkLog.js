//Get URL Origin
var baseURL = Util.PH_UpdateURL();
$(document).ready(function () {

    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var currentDate = day + "-" + (month) + "-" + now.getFullYear();
    $('#LoggedDateFrom').datepicker("setDate", currentDate);
    $('#LoggedDateTo').datepicker("setDate", currentDate);

    initGrid();



    $('#btnSearch').click(function () {
        var searchParams = {
            PageIndex: 1,
            PageSize: $('[id^=grdiLinkAutomationLogPager] select.ui-pg-selbox').val(),
            Request: {
                LoggedDateFrom: $('#LoggedDateFrom').datepicker('getDate'),
                LoggedDateTo: $('#LoggedDateTo').datepicker('getDate'),
                Message: $('#Message').val(),
                Level: $('#Level').val()
            }
        };
        loadGridData(searchParams);
        gridEvents(searchParams);
    });



    //validate From Date must <= To Date
    $("#LoggedDateFrom").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#LoggedDateTo").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    //validate From Date must >= To Date
    $("#LoggedDateTo").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#LoggedDateFrom").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

});

function initGrid() {

    $('#grdiLinkAutomationLog').jqGrid({
        url: "",
        colNames: [
            "Date", "Level", "Logger", "Message", ""
        ],
        colModel: [
            { key: false, name: "Date", index: "Date", editable: false, sortable: false, formatter: 'date', formatoptions: { srcformat: 'ISO8601Long', newformat: "d-m-Y h:i:s A" } },
            { key: false, name: "Level", index: "Level", editable: false, sortable: false },
            { key: false, name: "Logger", index: "Logger", editable: false, sortable: false },
            { key: false, name: "Message", index: "Message", editable: false, sortable: false, formatter: displayShortMessage },
            { key: false, name: "", index: "", editable: false, sortable: false, formatter: addLinkViewDetail },
        ],
        pager: '#grdiLinkAutomationLogPager',
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No result found",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $(window).bind("resize", function () {
        $("#grdiLinkAutomationLog").setGridWidth($("#wrapper").width());
        setSizePopupContain();
    }).trigger("resize");

    setTimeout(function () { $("#grdiLinkAutomationLog").setGridWidth($("#wrapper").width()); }, 100);

    function addLinkViewDetail(cellValue, option, rowObject) {
        var tag = $('<a>View Detail</a>');
        tag.attr({
            'data-message': rowObject.Message,
            class: 'view-detail-log',
            'onclick': 'viewDetailLog(this)',
            'rowid': option.rowId
        });
        return tag[0].outerHTML;
    }
    function displayShortMessage(cellValue, option, rowObject) {
        var span = "<span class=\"message\" fullmessage=\"" + cellValue.replace(/\"/g, "'") + "\">" + (cellValue.length > 120 ? cellValue.substring(0, 120) + '...' : cellValue).replace("\n", " ") + "</span>";
        return span;
    }
}

function gridEvents(searchParams) {

    $('#grdiLinkAutomationLog').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdiLinkAutomationLog').getGridParam('page');
            var rowNum = $('#grdiLinkAutomationLog').getGridParam('rowNum');

            if (pgButton == "next_grdiLinkAutomationLogPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdiLinkAutomationLogPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdiLinkAutomationLogPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdiLinkAutomationLogPager") {
                currentPage = $('#grdiLinkAutomationLog').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdiLinkAutomationLogPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridData(searchParams);
        }
    });
}

function loadGridData(searchParams) {
    LG_ShowProgressMask();
    $('#grdiLinkAutomationLog').jqGrid('clearGridData');
    LogService.SearchiLinkLog(searchParams, baseURL).done(function (result) {
        if (result.IsError) {
            Util.MessageDialog(result.Message, "Error");
        } else {
            var grid = $('#grdiLinkAutomationLog')[0];
            $('#grdiLinkAutomationLog').jqGrid('clearGridData');
            if (result.Response.length > 0)
                grid.addJSONData(result);
            else
                Util.MessageDialog('No result found', 'Information');
        }
        LG_HideProgressMask();
    }).fail(function (err) {
        Util.MessageDialog(err, "Error");
        LG_HideProgressMask();
    });
}

function viewDetailLog(selector) {
    var self = $(selector);
    var pageContent = self.closest('.page-content');
    var popupViewDetailLog = $('#popupViewDetailLog').dialog();
    popupViewDetailLog.dialog('option', { 'width': '80%', 'height': pageContent.height() * 0.8, 'resizable': false });

    var date = self.parents('tr').children('td').eq(0).text();
    var dialog = popupViewDetailLog.parent('.ui-dialog');
    dialog.find('.ui-dialog-title').html('<b>Log View Detail: ' + date + '</b>');

    setSizePopupContain();
    var ele = self.parents('tr').find('span.message');
    var content = self.parents('tr').find('td span.message').attr("fullmessage");
    popupViewDetailLog.children('textarea').text(content);
}

function setSizePopupContain() {
    var popupViewDetailLog = $('#popupViewDetailLog');
    popupViewDetailLog.css({ 'padding-right': '0' });

    var popupW = popupViewDetailLog.width(),
        popupH = popupViewDetailLog.height();
    var textArea = popupViewDetailLog.children('textarea');
    textArea.css({ 'width': popupW - 5, 'height': popupH - 10, 'border': 'none', 'outline': 'none', 'resize': 'none' });
    textArea.attr({ 'readonly': 'readonly' });
}
