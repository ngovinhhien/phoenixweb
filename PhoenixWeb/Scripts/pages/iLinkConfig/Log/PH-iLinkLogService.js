﻿(function (LogService, $, undefined) {
    LogService.SearchiLinkLog = function (searchParams, baseURL) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "iLinkConfig/SearchiLinkLog",
            type: "POST",
            data: JSON.stringify(searchParams),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
    return {
        SearchiLinkLog: LogService.SearchiLinkLog,
    };

}(window.LogService = window.LogService || {}, jQuery));