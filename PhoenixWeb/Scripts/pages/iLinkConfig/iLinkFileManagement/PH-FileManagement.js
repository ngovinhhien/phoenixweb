﻿var urlOrigin = Util.PH_UpdateURL();
var sortDirection = "DESC";

$(document).ready(function () {
    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });

    $("#StartDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#EndDate").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#EndDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#StartDate").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });


    initiLinkFileGrid();
    $(window).bind('resize', function () {
        $("#iLinkFileGrid").setGridWidth($("#wrapper").width());
    }).trigger('resize');


    $("#iLinkFileGrid").css("width", $("#wrapper").width());
    $("#gview_iLinkFileGrid .ui-jqgrid-htable").css("width", $("#wrapper").width());

    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var currentDate = day + "-" + (month) + "-" + now.getFullYear();
    $('#EndDate').datepicker("setDate", currentDate);
    $('#StartDate').datepicker("setDate", currentDate);

    $('#btnSearch').click(function (e) {
        var urlParams = new URLSearchParams(window.location.search);
        var key = urlParams.get('key');

        var isValidStartDateEndDate = CheckStartDateFollowEndDate();
        if (!isValidStartDateEndDate) {
            e.preventDefault();
            return;
        }

        var searchParams = GetSearchParams();
        loadDataiLinkFileGrid(searchParams);
        gridiLinkFileEvents(searchParams);
    });
});

function GetSearchParams() {
    var startDate = $("#StartDate").datepicker('getDate');
    var endDate = $("#EndDate").datepicker('getDate');
    var searchParams = {
        PageIndex: 1,
        PageSize: parseInt($('[id^=iLinkFileGridPager] select.ui-pg-selbox').val()),
        Request: {
            StartDate: startDate,
            EndDate: endDate,
            FileName: $('#FileName').val(),
            Status: $('#FileStatus').val(),
            FileType: $('#FileType').val(),
            SortOrder: sortDirection
        }
    };
    return searchParams;
}

function CheckStartDateFollowEndDate() {
    var endDate = $("#EndDate").datepicker('getDate');
    var startDate = $("#StartDate").datepicker('getDate');
    if (startDate > endDate) {
        $("#error").text("The Start Date must be smaller than End Date.");
        $("#StartDate").focus();
        return false;
    }
    $("#error").text("");
    return true;
}


function BuildColName() {
    var colNames = [
        "File Name",
        "Status",
        "File Size",
        "File Type",
        "Download Date",
        "Total Number of Transactions",
        "Number of Successfully Imported Transactions",
        "Number of Rejected Transactions",
        "Total Amount of Imported Transactions"
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        {
            key: false, name: "Filename", index: "Filename", align: 'center', editable: false, sortable: false,
            formatter: function (value, options, rowObject) {
                var html = "";
                html += "<a target='_blank' href=" + rowObject.URL + " style='color: #428bca'>" + value + "</a>";
                return html;
            }
        },
        { key: false, name: "StatusCode", index: "StatusCode", width: "70", align: 'center', editable: false, sortable: false },
        {
            key: false, name: "FileSize", index: "FileSize", width: "60", align: 'center',
            formatter: function (value, options, rowObject) {
                return value.toFixed(2) + " MB";
            }, editable: false, sortable: false
        },
        { key: false, name: "FileType", index: "FileType", width: "80", align: 'center', editable: false, sortable: false },
        {
            key: false,
            name: "DownloadedDate",
            index: "DownloadedDate",
            align: 'center',
            editable: false,
            sortable: true,
            width: "100",
            formatter: 'date',
            formatoptions:
            {
                srcformat: "ISO8601Long",
                newformat: "d/m/Y h:i:s A"
            }
        },
        {
            key: false, name: "TotalTransaction", index: "TotalTransaction", align: 'center', width: "130", editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0;
                }
                return value;
            }
        },
        {
            key: false, name: "TotalImportedTransaction", index: "TotalImportedTransaction", width: "130", align: 'center', editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0;
                }
                return value;
            }
        },
        {
            key: false, name: "TotalRejectedTransaction", width: "130", index: "TotalRejectedTransaction", align: 'center', editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0;
                }
                return value;
            }
        },
        {
            key: false, name: "TotalAmountImportedTransaction", index: "TotalAmountImportedTransaction", align: 'center', editable: false, sortable: false, formatter: function (value) {
                if (value == null) {
                    return 0.00;
                }
                return value.toFixed(2);
            }
        }
    ];

    return colModel;
}

function initiLinkFileGrid() {
    $("#iLinkFileGrid").jqGrid({
        url: "",
        colNames: BuildColName(),
        colModel: BuildColModel(),
        pager: "#iLinkFileGridPager",
        rowNum: 20,
        loadui: "disable",
        rowList: [20, 50, 100, 200],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No result found",
        height: 500,
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            sortDirection = sortOrder
            var searchParams = GetSearchParams();
            loadDataiLinkFileGrid(searchParams);
        },
        autowidth: true,
        multiselect: false
    });
}


function loadDataiLinkFileGrid(searchParams) {
    LG_ShowProgressMask();
    var baseURL = urlOrigin + "iLinkConfig/GetiLinkFiles";
    iLinkFileManagementService.getiLinkFiles(baseURL, searchParams)
        .done(function (result) {
            if (result.IsError) {
                Util.MessageDialog(result.Message, "Error");
            } else {
                var grid = $('#iLinkFileGrid')[0];
                $('#iLinkFileGrid').jqGrid('clearGridData');
                if (result.Response.length > 0)
                    grid.addJSONData(result);
                else
                    Util.MessageDialog('No result found', 'Information');
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            Util.MessageDialog(err, "Error");
            LG_HideProgressMask();
        });
}

function gridiLinkFileEvents(searchParams) {
    $('#iLinkFileGrid').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#iLinkFileGrid').getGridParam('page');
            var rowNum = $('#iLinkFileGrid').getGridParam('rowNum');

            if (pgButton == "next_iLinkFileGridPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_iLinkFileGridPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_iLinkFileGridPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_iLinkFileGridPager") {
                if ($('#iLinkFileGrid').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#iLinkFileGrid').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#iLinkFileGrid').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='iLinkFileGridPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadDataiLinkFileGrid(searchParams);
        }
    });
}
