﻿(function (iLinkFileManagementService, $, undefined) {
    iLinkFileManagementService.getiLinkFiles = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchParams),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        getiLinkFiles: iLinkFileManagementService.getiLinkFiles,
    };

}(window.iLinkFileManagementService = window.iLinkFileManagementService || {}, jQuery));