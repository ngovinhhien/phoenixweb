﻿(function (iLinkCredentialConfigService, $, undefined) {
    iLinkCredentialConfigService.GetiLinkCredentials = function (baseURL) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "iLinkConfig/GetiLinkCredentials",
            type: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    iLinkCredentialConfigService.UpdateiLinkCredentials = function (baseURL, iLinkCredential) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "iLinkConfig/UpdateiLinkCredentials",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(iLinkCredential),     
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        GetiLinkCredentials: iLinkCredentialConfigService.GetiLinkCredentials,
    };

}(window.iLinkCredentialConfigService = window.iLinkCredentialConfigService || {}, jQuery));