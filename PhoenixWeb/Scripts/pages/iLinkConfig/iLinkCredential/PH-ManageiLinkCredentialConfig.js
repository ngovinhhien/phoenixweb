﻿var baseURL = Util.PH_UpdateURL();
var oldData = {
    iLinkPassword: "",
    iLinkFilename: "",
    iLinkExpiryDate: "",
};
var hasNewFile = false;

$(function () {
    getiLinkCredentials();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "-10:+10"
    });
    $("#iLinkFileInput").change(function () {
        var fileInput = $("#iLinkFileInput")[0];
        var file = fileInput.files[0];
        if (file != null && file != undefined) {
            $("#iLinkFilename").val(file.name);
            Util.DisableControlById(["btn-discard", "btn-save"], false);
            hasNewFile = true;
        } else {
            $("#iLinkFilename").val("");
            Util.DisableControlById(["btn-discard", "btn-save"], true);
            hasNewFile = false;
        }
    });
    $("#btn-discard").click(function () {
        $("#iLinkPassword").val(oldData.iLinkPassword);
        $("#iLinkFilename").val(oldData.iLinkFilename);
        $("#iLinkExpiryDate").val(oldData.iLinkExpiryDate);
        $("#iLinkFileInput").val('');
        hasNewFile = false;
        Util.DisableControlById(["btn-discard", "btn-save"], true);
    });
    $("#btn-save").click(function (e) {
        e.preventDefault();
        var iLinkPassword = $("#iLinkPassword").val();
        var iLinkExpiryDate = $("#iLinkExpiryDate").val() == undefined ? '' : Util.ConvertDMYStringToYMDString($("#iLinkExpiryDate").val());
        var iLinkCredential = {
            iLinkPassword: iLinkPassword,
            iLinkExpiryDate: iLinkExpiryDate
        }
        var file = $("#iLinkFileInput")[0].files[0];
        if (hasNewFile) {
            if (file != null && file != undefined) {
                iLinkCredential.iLinkFilename = file.name;
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.readAsBinaryString(file);
                    reader.onload = function () {
                        iLinkCredential.iLinkFile = btoa(reader.result);
                        if (validateiLinkCredential(iLinkCredential)) {
                            updateiLinkCredentials(iLinkCredential);
                        }
                    }
                    reader.onerror = function (evt) {
                        Util.MessageDialog("Error loading file.", "Error");
                    }
                } else {
                    Util.MessageDialog("This browser does not support FileReader.", "Error");
                }
            }
        } else {
            iLinkCredential.iLinkFilename = oldData.iLinkFilename;
            if (validateiLinkCredential(iLinkCredential)) {
                Util.ConfirmDialog("Do you want to update the configuration?", "Message").done(function () {
                    LG_ShowProgressMask();
                    updateiLinkCredentials(iLinkCredential);
                });
            }
        }
        
    });
}); // end of document ready

getiLinkCredentials = function () {
    iLinkCredentialConfigService.GetiLinkCredentials(baseURL).done(function (result) {
        if (result != null && result != undefined) {
            $("#iLinkPassword").val(result.iLinkPassword);
            $("#iLinkFilename").val(result.iLinkFilename);
            var expiry = new Date(parseInt(result.iLinkExpiryDate.replace(")/", "").replace("/Date(", "")));
            var expiryString = Util.ConvertDateToStringDMY(expiry);
            $("#iLinkExpiryDate").val(expiryString);
            oldData.iLinkPassword = result.iLinkPassword;
            oldData.iLinkFilename = result.iLinkFilename;
            oldData.iLinkExpiryDate = expiryString;
            $("#iLinkPassword").change(function (e) {
                Util.CheckDirty(e, function () {
                    Util.DisableControlById(["btn-discard", "btn-save"], false);
                });
            });
            $("#iLinkExpiryDate").change(function (e) {
                Util.CheckDirty(e, function () {
                    Util.DisableControlById(["btn-discard", "btn-save"], false);
                });
            });
            Util.DisableControlById(["btn-discard", "btn-save"], true);
        }
    });
}

updateiLinkCredentials = function (iLinkCredential) {
    iLinkCredentialConfigService.UpdateiLinkCredentials(baseURL, iLinkCredential).done(function (result) {
        LG_HideProgressMask();
        if (result.IsSuccess) {
            Util.MessageDialog(["The configuration has been saved successfully"], "Saved Successfully");
            getiLinkCredentials();
        }
        else {
            Util.MessageDialog([result.Message], "Error");
        }
    });
}

validateiLinkCredential = function (iLinkCredential) {
    if (iLinkCredential == null || iLinkCredential == undefined) {
        Util.MessageDialog(["System error"], "Error");
        return false;
    }
    var valObj = {
        flag: true,
        errors: []
    }
    if (Util.CheckNotEmptyValue(iLinkCredential.iLinkPassword, " credential password", valObj)) {
        Util.CheckMaxLength(iLinkCredential.iLinkPassword, 20, " credential password", valObj);
    }
    Util.CheckNotEmptyValue(iLinkCredential.iLinkFilename, "Credential File", valObj);
    Util.CheckNotEmptyValue(iLinkCredential.iLinkExpiryDate, "Expiry Date", valObj);
    if (!valObj.flag) {
        Util.MessageDialog(valObj.errors, "Error");
    }
    return valObj.flag;
}
