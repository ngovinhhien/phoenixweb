﻿$(function () {
    var baseUrl = Util.PH_UpdateURL();

    $(".active").removeClass("active open");
    $(".Administration").addClass("active open");

    $('#btnReloadConfiguration').on('click', function () {
        reloadConfiguration(baseUrl);
    });
});


reloadConfiguration = function (baseUrl) {
    LG_ShowProgressMask();

    $.ajax({
        url: baseUrl + "Administration/ReloadConfiguration",
        success: function (result) {
            if (result != null) {
                location.reload(true);

            }
            else {
                alert("System Error");
            } 
            LG_HideProgressMask();
        }
    });

    
}