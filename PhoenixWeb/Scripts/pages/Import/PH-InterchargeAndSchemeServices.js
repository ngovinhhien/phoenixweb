﻿(function (InterchargeAndSchemeServices, $) {
    InterchargeAndSchemeServices.Search = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Import/GetRecentFeesImport',
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchParams),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        Search: InterchargeAndSchemeServices.Search,
    };

}(window.InterchargeAndSchemeServices = window.InterchargeAndSchemeServices || {}, jQuery));