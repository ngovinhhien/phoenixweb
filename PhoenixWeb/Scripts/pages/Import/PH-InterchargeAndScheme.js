﻿var baseURL = Util.PH_UpdateURL();
Dropzone.autoDiscover = false;
var myDropZone;
var popupViewDetailLog;
var InterchargeAndSchemeSearchObject = {
    PageIndex: '1',
    PageSize: '20',
    SortField: 'UploadedDate',
    SortOrder: 'DESC'
};

$(document).ready(function(){
    var config = { 
        uploadURL:'FeesImport',  
        autoProcessQueue: false, 
        acceptedFiles: '.csv',
        maxFiles: 1,
        timeout: 900000
    };
    myDropZone = Util.initUploadZone('#UploadSection #DragAndDropZone', config, UploadSuccess, UploadError);

    $('#UploadButton').on('click', function(){
        OpenFeesUploadPopup(); 
    });
    
    $('#ErrorSection div.error-Section-button button.btn-default').on('click', function(){
        ReturnUploadPage();
    });

    $('#ReviewSection div.review-Section-button button.btn-default').on('click', function(){
        Util.ConfirmDialog('Are you sure you wish to cancel? The data and file will be discarded.'
            ,'Confirmation'
            , 'Yes'
            , 'Cancel').done(function(){
            ReturnUploadPage();
        });
    });

    $('#ErrorSection button#DownloadFile').on('click', function(){
        window.open(baseURL + 'import/DownloadErrorFile?key=' + $(this).attr('data-href'), '_blank').focus();
    });

    $('#ReviewSection button#SubmitFees').on('click', function() {
        SubmitFees(this);
    });
    
    $('#SuccessSection button#ClosePopup').on('click', function(){
        popupViewDetailLog.dialog('close');
        loadFeesRecentUploadGridData();
    });
    
    popupViewDetailLog = $('#UploadFeePopup').dialog({
        title: "Upload File",
        position: "center",
        width: 800,
        modal: true,
        autoOpen: false,
        draggable: true,
        drag: function (event, ui) {
            var scrollTopPx = $(document).scrollTop();
            var positionUI = ui.position;
            positionUI.top = positionUI.top - scrollTopPx;
            $(this).closest(".ui-dialog").css("top", positionUI.top);
        }
    });

    initFeesRecentUploadGrid();
})

function BuildFeesRecentUploadColName() {
    return [
        "File Name",
        "Uploaded by",
        "Upload date",
        "Actions"
    ];
}

function BuildFeesRecentUploadColModel() {
    return [
        {
            key: false,
            name: "FileName",
            index: "FileName",
            align: 'left',
            editable: false,
            sortable: false,
            width:200,
            classes: "grid-col-padding-left"
        },
        {
            key: false,
            name: "UploadedBy",
            index: "UploadedBy",
            align: 'left',
            editable: false,
            sortable: false,
            width:200,
            classes: "grid-col-padding-left"
        },
        {
            key: false,
            name: "UploadDateString",
            index: "UploadDate",
            align: 'left',
            editable: false,
            sortable: false,
            width:200,
            classes: "grid-col-padding-left"
        },
        {
            key: false,
            name: "StoredFileName",
            index: "Actions",
            align: 'left',
            editable: false,
            sortable: false,
            classes: "grid-col-padding-left",
            //width: 5000,
            formatter: actionFormatter
        }
    ];
}

function UpdateFeesRecentUploadColumnTitle() {
    var colModels = BuildFeesRecentUploadColModel();
    for (var col of colModels) {
        if (col.align === "left") {
            Util.JQGridAlignLeftHeader("FeesRecentUploadGrid", col.name);
            if (!col.frozen) {
                Util.JQGridSetPaddingLeft("FeesRecentUploadGrid", col.name);
            }
        } else if (col.align === "right") {
            Util.JQGridAlignRightHeader("FeesRecentUploadGrid", col.name);
            Util.JQGridSetPaddingRight("FeesRecentUploadGrid", col.name);
            if (col.sortable) {
                Util.JQGridSetPaddingRightSort("FeesRecentUploadGrid", col.name);
            }
        }
    }
}

function loadFeesRecentUploadGridData() {
    Util.RemoveGridMessage("FeesRecentUpload");
    Util.BlockjqGrid('#FeesRecentUploadGrid');
    var grid = $('#FeesRecentUploadGrid');
    grid.jqGrid('clearGridData');

    InterchargeAndSchemeServices.Search(baseURL, InterchargeAndSchemeSearchObject)
        .done(function (result) {
            if (result.isError !== true) {
                if (result.Data) {
                    grid[0].addJSONData(result.Data);
                }
                else
                {
                    Util.ShowGridMessage("FeesRecentUpload", result.Message)
                }
            }
            else {
                Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            }
        })
        .fail(function (err) {
        }).always(function (){
        Util.UnBlockjqGrid('#FeesRecentUploadGrid');
    });
}

function adjustSortIconAndAlignment(tableId) {
    var columns = BuildFeesRecentUploadColModel();
    var gridId = tableId + 'Grid';
    for (var col of columns) {
        if (col.sortable === false) {
            var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
            $(cellSelector).find('span.s-ico').remove();
            $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            if (col.frozen) {
                var cellSelectorFrozen = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                $(cellSelectorFrozen).find('span.s-ico').remove();
                $(cellSelectorFrozen).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            }

        }
        if (col.align) {
            $("#" + gridId + "_" + col.name).addClass(col.align);
            $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
        }
    }

    //Disabled all sort icon
    $(".ui-grid-ico-sort").addClass("ui-state-disabled");
}

function initFeesRecentUploadGrid() {
    Util.InitJqGrid(
        {
            colNames: BuildFeesRecentUploadColName(),
            colModel: BuildFeesRecentUploadColModel(),
            rowNum: 20,
            rowList: [20, 50, 100, 200],
            height: '661',
            hasPager: true,
            enableAutoWidth: true,
            enableShrinkToFit: true,
            enableMultiselect: false,
            gridId: 'FeesRecentUpload',
            allowSort: false,
            autoencode: true,
            datatype: 'local'
        },
        InterchargeAndSchemeSearchObject,
        loadFeesRecentUploadGridData);
    loadFeesRecentUploadGridData();
    UpdateFeesRecentUploadColumnTitle();
    adjustSortIconAndAlignment("FeesRecentUpload");
}

function actionFormatter(cellvalue) {
    var DownloadHref = "DownloadImportedFile?key=" + cellvalue;
    
    return '<a href="' + DownloadHref + '" target="_blank" class="btn btn-default download-csv" role="button"><img src=""/>&nbsp;Download as .csv</a>';
}

OpenFeesUploadPopup = function () {
    ReturnUploadPage();
};

UploadSuccess = function(file, result) {
if (!result.isSuccess) {
        Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
        file.status = Dropzone.QUEUED;
    }
    else if (result.isHeaderError) {
        Util.MessageDialog("Invalid headers. Please select another file.", "Error");
        file.status = Dropzone.QUEUED;
    }
    else if (result.isFileError)
    {
        if (result.ErrorMessage) {
            Util.MessageDialog(result.ErrorMessage, "Error");
        }
        else{
            $('#ErrorSection button#DownloadFile').attr("data-href",result.key);
            ChangePopupSection('ErrorSection');
        }
    }
    else 
    {
        $('#ReviewSection button#SubmitFees').attr('data-href',result.key)
        $('#ReviewData').html($('#ReviewDataTemplate').render({data: result.data}));
        ChangePopupSection('ReviewSection');        
    }

}

UploadError = function(file, result, xhr){
    if (xhr)
    {
        var statusString = xhr.status.toString().substring(0,1);
        if (statusString === "5" || statusString === "4")
        {
            Util.MessageDialog("An error happened while processing your request. Please try again.", "Error");
            myDropZone.removeAllFiles();
        }
        else if (xhr.status === 0 && xhr.readyState === 4)
        {
            var errorMess;
            
            if (result === "Server responded with 0 code."){
                errorMess = "An error happened while processing your request. Please try again."
            }
            else {
                errorMess = "Request timeout. Please try again later.";
            }
                
            Util.MessageDialog(errorMess, "Error");
            file.status = Dropzone.QUEUED;
        }
    }
}

ReturnUploadPage = function(){
    myDropZone.removeAllFiles();
    
    ChangePopupSection('UploadSection');
}

ChangePopupSection = function(sectionName){
    if (popupViewDetailLog.dialog('isOpen'))
        popupViewDetailLog.dialog('close');
    
    $.each($('#UploadFeePopup').children(), function () {
        $(this).hide();
    });

    $('#' + sectionName).show();

    popupViewDetailLog.dialog('open');
}

SubmitFees = function(button){
    LG_ShowProgressMask();
    $.ajax({
        url: baseURL + 'import/submitfees',
        data: { key: $(button).attr('data-href')},
        type: "POST",
        success: function (result) {
            if (result.isSuccess) {
                ChangePopupSection('SuccessSection');
            }
            else
                Util.MessageDialog("An error happened while processing your request. Please try again.","Error");
        }
    }).always(function (){
        LG_HideProgressMask();
    });
}
