﻿(function (merchantGroupService, $, undefined) {
    //function call to get all ExchangeRate Type
    merchantGroupService.SearchMerchantGroup = function (baseURL, searchMerchantGroup) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/GetMerchantGroup",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchMerchantGroup),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    merchantGroupService.GetMerchantAssociateWithGroup = function (baseURL, searchMerchant) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/GetMerchantGroupDetail",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchMerchant),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    merchantGroupService.RemoveMerchantOutOfGroup = function (baseURL, merchantID, groupID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/RemoveMerchantOutOfGroup",
            type: "POST",
            data: {
                merchantID: merchantID,
                groupID: groupID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    merchantGroupService.RemoveGroupCampaign = function (baseURL, groupID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/RemoveGroup",
            type: "POST",
            data: {
                groupID: groupID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    merchantGroupService.GetGroupAndExchangeRateInformation = function(baseURL, groupID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/GetGroupAndExchangeRateInformation",
            type: "GET",
            data: {
                groupID: groupID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }


    merchantGroupService.AddMerchantToGroup = function (groupId, merchantKey) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/AddMerchantToGroup",
            type: "POST",
            data: { groupID: groupId, merchantKey: merchantKey },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    merchantGroupService.ImportMerchantToGroup = function (formData) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Group/ImportMerchant",
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    return {
        SearchMerchantGroup: merchantGroupService.SearchMerchantGroup,
        GetMerchantAssociateWithGroup: merchantGroupService.GetMerchantAssociateWithGroup,
        RemoveMerchantOutOfGroup: merchantGroupService.RemoveMerchantOutOfGroup,
        RemoveGroupCampaign: merchantGroupService.RemoveGroupCampaign
    };

}(window.merchantGroupService = window.merchantGroupService || {}, jQuery));