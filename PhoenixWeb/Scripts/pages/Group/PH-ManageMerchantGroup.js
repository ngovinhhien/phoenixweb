﻿var searchMerchantGroup = {};
var searchMerchantGroupDetail = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;

$(function () {

    $(window).bind("resize", function () {
        $("#gridGroup").setGridWidth($("#managegroup").width() - 10);
        $("#merchantGroupGrid").setGridWidth($("#gridMerchantGroupSection").width() - 10);
    }).trigger("resize");

    $("#gridGroup").jqGrid({
        url: "",
        colNames: [
            "Group Name", "Number of merchants", "Action"
        ],
        colModel: [
            {
                name: "GroupName",
                index: "GroupName",
                editable: true,
                sortable: false,
            },
            { key: false, name: "TotalMerchant", index: "TotalMerchant", editable: false, sortable: false, align: "center", formatter: customDisplayPopup },
            {
                key: false,
                name: "GroupID",
                index: "GroupID",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customActionFormatter
            }
        ],
        pager: "#gridGroupPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#gridGroup').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#gridGroup').getGridParam('page');
            var rowNum = $('#gridGroup').getGridParam('rowNum');


            if (pgButton == "next_gridGroupPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_gridGroupPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_gridGroupPager") {
                currentPage = 1;
            }

            if (pgButton == "last_gridGroupPager") {
                currentPage = $('#gridGroup').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='gridGroupPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (!searchMerchantGroup) {
                searchMerchantGroup = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                };
            } else {
                searchMerchantGroup.CurrentPage = currentPage;
                searchMerchantGroup.PageSize = rowNum;
            }

            if (isFiltering) {
                $("#textAdvanceSearch").trigger("keyup");
            } else {
                loadMerchantGroupData();
            }
        }
    });


    $("#merchantGroupGrid").jqGrid({
        url: "",
        colNames: [
            "Merchant Name", "Merchant Email", "Action"
        ],
        colModel: [
            {
                name: "MerchantName",
                index: "MerchantName",
                editable: true,
                sortable: false,
            },
            { key: false, name: "Email", index: "Email", editable: false, sortable: false},
            {
                key: false,
                name: "Member_key",
                index: "Member_key",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customActionForMerchantFormatter
            }
        ],
        pager: "#merchantGroupGridPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#merchantGroupGrid').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#merchantGroupGrid').getGridParam('page');
            var rowNum = $('#merchantGroupGrid').getGridParam('rowNum');


            if (pgButton == "next_merchantGroupGridPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_merchantGroupGridPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_merchantGroupGridPager") {
                currentPage = 1;
            }

            if (pgButton == "last_merchantGroupGridPager") {
                currentPage = $('#merchantGroupGrid').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='merchantGroupGridPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (!searchMerchantGroupDetail) {
                searchMerchantGroupDetail = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                };
            } else {
                searchMerchantGroupDetail.CurrentPage = currentPage;
                searchMerchantGroupDetail.PageSize = rowNum;
            }

            if (isFiltering) {
                $("#textAdvanceSearch").trigger("keyup");
            } else {
                LG_ShowProgressMask();
                ReloadMerchantGroupGrid();
            }
        }
    });

    searchMerchantGroup = {
        CurrentPage: 1,
        PageSize: 10
    };
    loadMerchantGroupData();
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
}); // end of document ready

$("#addGroup").on("click",
    function () {
        EarnedRateDialog.AddEarnedRateDialog("GROUP", true);

    });


onSaveExchangeRate = function () {
    var id = $("#PointEarnedRateForm #GroupID").val();
    if (id && id > 0) {
        EarnedRateDialog.SaveExchangeRate("Group/EditGroupAndCampaign", loadMerchantGroupData);
    } else {
        EarnedRateDialog.SaveExchangeRate("Group/AddGroupAndCampaign", loadMerchantGroupData);
    }

}

EditGroupCampaign = function(groupID) {
    EarnedRateDialog.DisplayPointEarnedRateDialog("GROUP", "EDIT_GROUP", undefined, groupID);
}

$("#textAdvanceSearch").keyup(function () {
    var searchText = $("#textAdvanceSearch").val();
    if (searchText.trim().length > 2) {
        searchMerchantGroup.SearchText = searchText;
        //check in the first time use filter search then reset CurrentPage value
        if (!isFiltering) {
            searchMerchantGroup.CurrentPage = 1;
            searchMerchantGroup.PageSize = 10;
        }

        LG_ShowProgressMask();

        merchantGroupService.SearchMerchantGroup(baseURL, searchMerchantGroup)
            .done(function (result) {
                var grid = $('#gridGroup')[0];
                $('#gridGroup').jqGrid('clearGridData');
                grid.addJSONData(result);

                //the notice flag is using advance search
                isFiltering = true;
                LG_HideProgressMask();
            });
    } else {
        if (isFiltering) {
            searchMerchantGroup.SearchText = "";
            loadMerchantGroupData();
        }
    }
});

$("#btnAddMerchant").on("click",
    function () {
        if ($('#MerchantInfo').val() == "") {
            showDialogError("Please enter merchant id.");

            return false;
        }

        LG_ShowProgressMask();

        merchantGroupService.AddMerchantToGroup($("#groupmemberdialogForm #GroupID").val(), $('#MerchantInfo').val()).done(function (result) {
            if (result.Message != "") {
                showDialogError(result.Message);
            }
            else {
                $('#dialog-result').show();
                $('#added-merchants-to-group-success').append(result.AddedSuccess);
                $('#added-merchants-to-group-error').append(result.AddedError);

                showDialogMessage("Results", "500px");

                ReloadMerchantGroupGrid(); //Load grid of merchant
            }
        });
    });

$('#btnImportFile').on('click', function () {
    var btnChooseFile = $('#btnChooseFile')[0];
    var file = btnChooseFile.files[0];
    if (file) {
        var fileExtension = file.name.substr((file.name.lastIndexOf('.') + 1)).toLowerCase();

        //validate xlsx, xls, csv file
        if (!checkExtensionFile(fileExtension)) {
            showDialogError('Please select excel file with "csv", "xls", "xlsx" format to import.');

            return false;
        }

        //file data 
        var formData = new FormData();
        formData.append("File", file);
        formData.append("GroupId", $("#groupmemberdialogForm #GroupID").val());

        LG_ShowProgressMask();

        merchantGroupService.ImportMerchantToGroup(formData).done(function (result) {
            if (result.Message != "") {
                showDialogError(result.Message);
            }
            else {
                showDialogResult(result.AddedSuccess, result.AddedError);

                ReloadMerchantGroupGrid(); //Load grid of merchant            
            }
        });
    } else {
        showDialogError("Please select a merchant file to import.");
    }
});

loadMerchantGroupData = function () {

    //Show progress mask
    LG_ShowProgressMask();

    merchantGroupService.SearchMerchantGroup(baseURL, searchMerchantGroup)
        .done(function (result) {
            //Loading data to grid
            var grid = $("#gridGroup")[0];
            $("#gridGroup").jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = false;

            //handle display the search filter after search QBR
            var dataGridCount = $('#gridGroup').jqGrid('getGridParam', 'reccount');
            if (dataGridCount > 0) {
                $("#filterSection").show();
            } else {
                $("#filterSection").hide();
            }

            LG_HideProgressMask();
        });
};

customActionFormatter = function (groupID, options, rowObject) {
    var html = "";
    html = html + "<img class='icon' src='Images/edit.png'  onClick='EditGroupCampaign(" + groupID + ")'/>";
    html = html + "<img class='icon' src='Images/delete.png'  onClick='RemoveGroupCampaign(" + groupID + ")'/>";
    return html;
};

customDisplayPopup = function (cellvalue, options, rowObject) {
    return "<a onclick='LoadDetailDialog(" + rowObject.GroupID + ",\"" + rowObject.GroupName + "\",\"" + rowObject.TotalMerchant + "\")'>" + cellvalue + "</a>";
}

customActionForMerchantFormatter = function (MerchantID, options, rowObject) {
    var html = "";
    html = html + "<img class='icon' src='Images/delete.png' onClick='RemoveMerchantOutOfGroup(" + MerchantID + ")'/>";
    return html;
}

ReloadMerchantGroupGrid = function () {
    var deferred = $.Deferred();
    merchantGroupService.GetMerchantAssociateWithGroup(baseURL, searchMerchantGroupDetail).done(function (result) {

        //Loading data to grid
        var grid = $("#merchantGroupGrid")[0];
        $("#merchantGroupGrid").jqGrid('clearGridData');
        grid.addJSONData(result);
        //Loading total of merchant
        $("#totalMerchant").text($("#merchantGroupGrid").jqGrid('getGridParam', 'records'));

        //the notice flag is using advance search
        isFiltering = false;

        //handle display the search filter after search QBR
        var dataGridCount = $('#merchantGroupGrid').jqGrid('getGridParam', 'reccount');
        if (dataGridCount > 0) {
            $("#filterSection").show();
        } else {
            $("#filterSection").hide();
        }

        LG_HideProgressMask();
        deferred.resolve(result);
    });
    return deferred.promise();
}

LoadDetailDialog = function (groupID, groupName, totalMerchant) {
    //Prepare current data for dialog
    $("#groupName").text(groupName);
    $("#totalMerchant").text(totalMerchant);
    $("#groupmemberdialogForm #GroupID").val(groupID);
    $("#MerchantInfo").val("");
    $("#btnChooseFile").val("");

    searchMerchantGroupDetail = {
        CurrentPage: 1,
        PageSize: 10,
        GroupID: groupID
    };
    //Show progress mask
    LG_ShowProgressMask();

    //Load grid of merchant
    ReloadMerchantGroupGrid().done(function () {
        $(".ui-autocomplete").css("z-index", "200");
        $('#groupmemberdialog').dialog({
            resizable: false,
            height: "auto",
            width: "50%",
            modal: true,
            title: "Group " + $("#groupName").text(),
            close: function () {
                // functionality goes here
                $(this).dialog("close");
                loadMerchantGroupData();
            }
        });

        $("#merchantGroupGrid").setGridWidth($("#gridMerchantGroupSection").width() - 30);

        var height = $("#gview_merchantGroupGrid .ui-jqgrid-bdiv").height();
        if (!height) {
            height = 300;
        }
        $("#gview_merchantGroupGrid .ui-jqgrid-bdiv").css("max-height", height);
        $("#gview_merchantGroupGrid .ui-jqgrid-bdiv").css("overflow-y", "auto");

    });
}

RemoveMerchantOutOfGroup = function (merchantID) {
    var confirmation = confirm("Are you sure to delete?");
    if (confirmation) {
        var groupID = $("#groupmemberdialogForm #GroupID").val();
        //Show progress mask
        LG_ShowProgressMask();
        merchantGroupService.RemoveMerchantOutOfGroup(baseURL, merchantID, groupID).done(function (result) {
            if (result.IsSuccess) {
                //Load grid of merchant
                ReloadMerchantGroupGrid();
            } else {
                alert(result.Message);
            }
            LG_HideProgressMask();
        });
    }
};

RemoveGroupCampaign = function (groupID) {
    var confirmation = confirm("Are you sure to delete?"); 
    if (confirmation) {
        //Show progress mask
        LG_ShowProgressMask();

        merchantGroupService.RemoveGroupCampaign(baseURL, groupID).done(function (result) {
            if (result.IsSuccess) {
                loadMerchantGroupData();
            } else {
                alert(result.Message);
            }
            LG_HideProgressMask();
        });
    }
}

showDialogMessage = function (title, width) {
    $('#dialog-message').dialog({
        title: title,
        width: width,
        modal: true,
        open: function () {
            $(this).parent().find('.ui-dialog-titlebar-close').hide();
        },
        buttons: [{
            text: 'Close',
            click: function () {
                $(this).dialog('close');

                $('#dialog-error').hide();
                $('#error-message').text("");

                $('#dialog-result').hide();
                $('#added-merchants-to-group-success').text("");
                $('#added-merchants-to-group-error').text("");
            },
            class: 'btn blue'
        }]
    });
}

showDialogError = function (message) {
    $('#dialog-error').show();
    $('#error-message').append(message);

    showDialogMessage("Error", "350px");
    LG_HideProgressMask();
}

showDialogResult = function (messageSuccess, messageError) {
    $('#dialog-result').show();
    $('#added-merchants-to-group-success').append(messageSuccess);
    $('#added-merchants-to-group-error').append(messageError);

    showDialogMessage("Results", "500px");
    LG_HideProgressMask();
}

checkExtensionFile = function (extensionFile) {
    if (extensionFile == "csv" || extensionFile == "xlsx" || extensionFile == "xls") {
        return true;
    }
    return false;
}