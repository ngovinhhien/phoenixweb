﻿//@class EarnedRateDialog
function EarnedRateDialog() { }

var baseURL = Util.PH_UpdateURL();
var currentYear = new Date().getFullYear();

$(".datecheck").on("change",
    function () {
        EarnedRateDialog.ValidateStartDateEndDate();
    });

$('.datepicker').datepicker({
    changeYear: true,
    showButtonPanel: true,
    changeMonth: true,
    showOtherMonths: true,
    selectOtherMonths: true,
    keepInvalid: true,
    dateFormat: "dd/mm/yy",
    yearRange: (currentYear - 3) + ":" + (currentYear + 3)
});

$("#ExchangeRateTypeCode").on("change",
    function () {
        var type = $("#ExchangeRateTypeCode").val();
        EarnedRateDialog.LoadPointEarnedRateDialogField(type);
        $("#ExchangeRateTypeCode").val(type);
    });

EarnedRateDialog.SaveExchangeRate = function (url, callbackMethod) {
    if ($('#PointEarnedRateForm').valid()) {
        var data = {
            ID: $("#ID").val(),
            ExchangeRateTypeCode: $("#ExchangeRateTypeCode").val(),
            QantasExchangeRate: $("#QantasExchangeRate").val(),
            EndDate: $("#EndDate").val(),
            Name: $("#Name").val(),
            StartDate: $("#StartDate").val(),
            EffectedDay: $("#EffectedDay").val(),
            GroupName: $("#GroupName").val(),
            GroupID: $("#PointEarnedRateForm #GroupID").val() || 0
        }

        $("#qatasEarnedRateDialog").dialog('close');
        LG_ShowProgressMask();
        qantasPointEarnedRateService.SaveQantasPointEarnedRate(url, data).done(function (message) {
            alert(message);
            if (typeof callbackMethod == "function") {
                callbackMethod();
            }
        }).fail(function (message) {
            LG_HideProgressMask();
            alert(message);
        });
    }
}


EarnedRateDialog.DisplayPointEarnedRateDialog = function (exchangeRateTypeCode, action, exchangeRateId, groupID) {

    EarnedRateDialog.LoadPointEarnedRateDialogField(exchangeRateTypeCode);
    switch (action) {
        case "ADD":
            //validate start date and endate
            $("#EndDate").datepicker('option', 'minDate', new Date());
            $("#StartDate").datepicker('option', 'minDate', new Date());
            $("#EndDate").datepicker('option', 'beforeShowDay', function (date) {
                var startDate = $("#StartDate").datepicker('getDate');
                return Util.disabledEndDateFollowStartDate(date, startDate);
            });
            $("#StartDate").datepicker('option', 'beforeShowDay', function (date) {
                var endDate = $("#EndDate").datepicker('getDate')
                return endDate ? Util.disabledStartDateFollowEndDate(date, endDate) : [true];
            });
            $("#PointEarnedRateForm #GroupID").val(0);
            $("#ID").val(0);
            $("#ExchangeRateTypeCode").css("pointer-events", 'unset');
            $("#title").text("Add new QBR Earned Rate");

            //show dialog
            var popupViewDetailLog = $('#qatasEarnedRateDialog').dialog();
            popupViewDetailLog.dialog('option', { 'width': '30%', 'resizable': false });
            break;
        case "EDIT_GROUP":
            merchantGroupService.GetGroupAndExchangeRateInformation(baseURL, groupID).done(function (rowData) {
                //get data row from grid
                $("#ExchangeRateTypeCode").css("pointer-events", 'none'); //disable ExchangeRateType dropdownlist
                $("#title").text("Edit QBR Earned Rate"); //set title
                //set mindate of endate is today
                $("#EndDate").datepicker('option', 'minDate', new Date());

                //update data to type dropdown list
                var typeList = $("#ExchangeRateTypeCode option").map(function() { return $(this).val(); }).get();
                var type = rowData.ExchangeRateTypeCode;
                if (typeList.indexOf(type) < 0) { //incase type is empty field
                    $('#ExchangeRateTypeCode').append(new Option(rowData.Type, rowData.ExchangeRateTypeCode, true));
                }

                //bind data from grid to form
                $("#ExchangeRateTypeCode").val(rowData.ExchangeRateTypeCode);
                $("#QantasExchangeRate").val(rowData.QantasExchangeRate);
                $("#Name").val(rowData.Name);
                $("#StartDate").datepicker("setDate", rowData.FromDate);
                $("#EndDate").datepicker( "setDate", rowData.ToDate);
                $("#ID").val(rowData.ID);
                $("#PointEarnedRateForm #GroupID").val(groupID);
                $("#IsUsed").val(rowData.IsUsed);
                $("#GroupName").val(rowData.GroupName);

                //validate end date > start date
                $("#EndDate").datepicker('option',
                    'beforeShowDay',
                    function(date) {
                        var startDate = $("#StartDate").datepicker('getDate');

                        return Util.disabledEndDateFollowStartDate(date, startDate);
                    });

                //validate not allow change start date when the Exchange Rate was used
                $("#StartDate").datepicker('option',
                    'beforeShowDay',
                    function(date) {
                        var isUsed = $("#IsUsed").val();
                        var endDate = $("#EndDate").datepicker('getDate');
                        return Util.disabledStartDatePicker(date, isUsed, endDate);
                    });


                //show dialog
                var popupViewDetailLog = $('#qatasEarnedRateDialog').dialog();
                popupViewDetailLog.dialog('option', { 'width': '30%', 'resizable': false });
            });

            break;
        case "EDIT_EARNED":
            //get data row from grid
            var rowData = $('#grdQantasPointEarnedRateGrid').jqGrid('getRowData', exchangeRateId);
            $("#ExchangeRateTypeCode").css("pointer-events", 'none'); //disable ExchangeRateType dropdownlist
            $("#title").text("Edit QBR Earned Rate"); //set title

            //update data to type dropdown list
            var typeList = $("#ExchangeRateTypeCode option").map(function () { return $(this).val(); }).get();
            var type = rowData.ExchangeRateTypeCode;
            if (typeList.indexOf(type) < 0) { //incase type is empty field
                $('#ExchangeRateTypeCode').append(new Option(rowData.Type, rowData.ExchangeRateTypeCode, true));
            }

            //bind data from grid to form
            $("#ExchangeRateTypeCode").val(rowData.ExchangeRateTypeCode);
            $("#QantasExchangeRate").val(rowData.QantasExchangeRate);
            $("#Name").val(rowData.Name);
            $("#StartDate").val(rowData.FromDate);
            $("#EndDate").val(rowData.ToDate);
            $("#EffectedDay").val(rowData.RegisterEffectedDays);
            $("#ID").val(rowData.ID);
            $("#PointEarnedRateForm #GroupID").val(0);
            $("#IsUsed").val(rowData.IsUsed);

            //set mindate of endate is today
            $("#EndDate").datepicker('option', 'minDate', new Date());

            //validate end date > start date
            $("#EndDate").datepicker('option',
                'beforeShowDay',
                function (date) {
                    var startDate = $("#StartDate").datepicker('getDate');

                    return Util.disabledEndDateFollowStartDate(date, startDate);
                });

            //validate not allow change start date when the Exchange Rate was used
            $("#StartDate").datepicker('option',
                'beforeShowDay',
                function (date) {
                    var isUsed = $("#IsUsed").val();
                    var endDate = $("#EndDate").datepicker('getDate');
                    return Util.disabledStartDatePicker(date, isUsed, endDate);
                });

            //show dialog
            var popupViewDetailLog = $('#qatasEarnedRateDialog').dialog();
            popupViewDetailLog.dialog('option', { 'width': '30%', 'resizable': false });
            break;
        default:
    }

};


EarnedRateDialog.LoadPointEarnedRateDialogField = function (exchangeRateTypeCode) {
    //reset form
    document.getElementById("PointEarnedRateForm").reset();
    $(".field-validation-valid").text("");
    $("#warningMesage").text("");

    switch (exchangeRateTypeCode) {
        case "PERIOD":
            $("#StartDateRow").show();
            $("#EndDateRow").show();
            $("#EffectedDayRow").hide();
            $("#GroupNameRow").hide();
            break;
        case "NEW_MERCHANT":
            $("#StartDateRow").show();
            $("#EndDateRow").show();
            $("#EffectedDayRow").show();
            $("#GroupNameRow").hide();
            break;
        case "GROUP":
            $("#StartDateRow").show();
            $("#EndDateRow").show();
            $("#EffectedDayRow").hide();
            $("#GroupNameRow").show();
            $("#ExchangeRateTypeCode").attr("disabled", "disabled");
            break;
        default:
            $("#StartDateRow").hide();
            $("#EndDateRow").hide();
            $("#EffectedDayRow").hide();
            $("#GroupNameRow").hide();
    }
};

EarnedRateDialog.AddEarnedRateDialog = function (defaultTypeCode, includedGroupType) {
    //get dropdownlist data
    qantasPointEarnedRateService.GetExchangeRateTypeList(includedGroupType)
        .done(function (data) {
        $('#ExchangeRateTypeCode').empty();
        //allocate ExchangeRate Type to dropdownlist
        $.each(data,
            function (i, el) {
                $('#ExchangeRateTypeCode').append(new Option(el.Text, el.Value, el.Selected));
            })
        //show dialog
        if (!defaultTypeCode) {
            defaultTypeCode = "DEFAULT";
        }
        EarnedRateDialog.DisplayPointEarnedRateDialog(defaultTypeCode, "ADD");
    }).fail(function (message) {
        alert(message);
    });
};

EarnedRateDialog.ValidateStartDateEndDate = function () {
    var startDate = $("#StartDate").val();
    var endDate = $("#EndDate").val();
    var type = $("#ExchangeRateTypeCode").val();
    if (startDate && endDate && type) {
        //check start date and end date, which selected by an other Earned Rate before
        qantasPointEarnedRateService.CheckEarnedRateDateExisting(startDate, endDate, type).done(function (message) {
            $("#warningMesage").text(message);
        }).fail(function (message) {
            $("#warningMesage").text(message);
        });
    }
}