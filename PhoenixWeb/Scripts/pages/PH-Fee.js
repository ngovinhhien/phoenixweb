﻿$(document).ready(function () {

    RenderFees = function (tblContainName) {
        var numberOfFees = JSON.parse($("#FeeDtoAsJson").val()).length;
        var feeDatas = numberOfFees > 0 ? JSON.parse($("#FeeDtoAsJson").val()) : [{
            Amount: 0,
            FeeChargeEndDate: null,
            FeeChargeStartDate: null,
            FrequencyID: 1,
            ID: 0,
            FeeID: 1,
            IsEnable: false,
            MemberKey: $("#Member_key").val()
        }];
        $("#FeeDtoAsJson").val(JSON.stringify(feeDatas));
        var feeTypeDatas = JSON.parse($("#ChargeFeeTypeDtoAsJson").val()) || [];
        var data = {
            fees: feeDatas,
            types: feeTypeDatas,
            isReadOnly: $("#ChargeFee").data("is-readonly")
        };
        var recordHtml = $("#tmplFeeList").render({ data: data });
        $("#" + tblContainName).html(recordHtml);
        $.each(feeDatas, function (i, el) {
            $("#ddl_Fee_" + el.ID).val(el.FeeID);
        });
       
    }

    validateAmount = function (el) {
        if (el.value == "") {
            $("#error_fee_" + el.dataset.id).text("Invalid Amount");
            isValid = false;
            return;
        }
        el.value = parseFloat(el.value).toFixed(2);
        var feeTypeData = JSON.parse($("#ChargeFeeTypeDtoAsJson").val()) || [];
        var type = feeTypeData.filter(function (type) {
            return type.ID == el.dataset.feeid;
        });
        if (el.value > type[0].MaxAmount || el.value < type[0].MinAmount) {
            $("#error_fee_" + el.dataset.id).text("Invalid Amount");
        }
        else {
            $("#error_fee_" + el.dataset.id).text("");
        }
    }

    OnCheckEnable = function (elCheckbox) {
        var id = $(elCheckbox)[0].dataset.feeid;
        var isChecked = $(elCheckbox).prop("checked");
        var span = $(elCheckbox).parent()[0];
        if (isChecked) {
            $(span).addClass("checked");
            $("#ddl_Fee_" + id).removeAttr('disabled');
            $("#amount_" + id).removeAttr('disabled');
        }
        else {
            $(span).removeClass("checked");
            $("#ddl_Fee_" + id).attr('disabled', 'disabled');
            $("#amount_" + id).attr('disabled', 'disabled');
        }
    }
});