﻿(function (DocketTransactionDetailService, $, undefined) {
    DocketTransactionDetailService.DocketTransactionDetailData = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/DocketTransactionDetailData',
            type: "GET",
            data: params,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    DocketTransactionDetailService.TransactionNote = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/TransactionNote',
            type: "GET",
            data: params,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    DocketTransactionDetailService.RelatedTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/RelatedTransaction',
            type: "GET",
            data: params,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    DocketTransactionDetailService.TransactionNoteDialog = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/AddTransNote',
            type: "GET",
            data: params,
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    DocketTransactionDetailService.AddTransactionNote = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/AddTransNote',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    return {
        DocketTransactionDetailData: DocketTransactionDetailService.DocketTransactionDetailData,
    };

}(window.DocketTransactionDetailService = window.DocketTransactionDetailService || {}, jQuery));