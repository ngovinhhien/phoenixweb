﻿var urlOrigin = Util.PH_UpdateURL();

$(document).ready(function () {
    loadDetails();
});

function noteGridProp(rowCount) {
    return {
        gridId: 'tbl_TransactionNote',
        colModel: [
            { key: false, label: "Created Date", name: "CreatedDate", width: "80", align: 'left', editable: false, sortable: false },
            { key: false, label: "Created By", name: "CreatedBy", width: "60", align: 'left', editable: false, sortable: false },
            { key: false, label: "Notes", name: "NoteDescription", width: "200", align: 'left', editable: false, sortable: false },
        ],
        rowNum: 10000,
        height: "auto",
        enableShrinkToFit: true,
        hasPager: false
    };
}
function initRelatedTransactionProp(isTaxi) {
    return {
        gridId: 'tbl_RelatedTransaction',
        colModel: [
            { key: false, label: "Type", name: "RaiseRequestTypeName", width: "160", align: 'left', editable: false, sortable: false },
            { key: false, label: "Reason", name: "ReasonDescription", width: "280", align: 'left', editable: false, sortable: false },
            { key: false, label: "Amount", name: "RequestAmount", width: "120", align: 'right', editable: false, sortable: false },
            { key: false, label: "MSF/Commission", name: "MSF", width: "150", align: 'right', editable: false, sortable: false },
            { key: false, label: "Tip Fee", name: "AutoTipFeeAmount", width: "120", align: 'right', editable: false, sortable: false, hidden: !isTaxi },
            { key: false, label: "Levy", name: "LevyChargeAmount", width: "120", align: 'right', editable: false, sortable: false, hidden: !isTaxi },
            { key: false, label: "Fee", name: "FeeAmount", width: "120", align: 'right', editable: false, sortable: false },
            { key: false, label: "Status", name: "RequestStatus", width: "120", align: 'left', editable: false, sortable: false },
            { key: false, label: "Processed Date", name: "ProcessedDate", width: "165", align: 'left', editable: false, sortable: false },
            { key: false, label: "Failed Date", name: "FailedDate", width: "165", align: 'left', editable: false, sortable: false },
            { key: false, label: "Lodgement Ref. No.", name: "LodgementReferenceNo", width: "180", align: 'left', editable: false, sortable: false },
            {
                key: false, name: "Actions", label: "Actions", width: "100", align: 'left', editable: false, sortable: false,
                formatter: function () {
                    return '<button class="btn-inline clickable viewMore" onclick="viewMore(this)">View More</button>';
                }
            },
            { key: false, name: "CreatedBy", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "CreatedDate", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "RequestNote", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "RaiseRequestID", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "FreshdeskTicketNo", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "FreshdeskTicketURL", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "LodgementReferenceNo", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "ReferenceNumber", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "IsCancelRequest", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "IsReverseRequest", align: 'left', editable: false, sortable: false, hidden: true },
        ],
        rowNum: 10000,
        height: "auto",
        hasPager: false,
    };
};

function loadDetails() {
    LG_ShowProgressMask();
    DocketTransactionDetailService.DocketTransactionDetailData(urlOrigin, { memberKey: getMemberkey(), docketKey: getDocketkey() })
        .done(function (result) {
            if (result.IsSuccess && result.Data) {
                var isTaxi = result.Data.MemberType == "Taxi" ? true : false;
                var title = "Transaction #" + result.Data.DocketKey
                $("#title").text(title);
                var data = {};
                var fieldArr = result.Data.DisplayFields.split(",");
                for (var field of fieldArr) {
                    data[field] = result.Data[field];
                }
                loadDataToSection("#DocketInfo_div", data);
                loadNoteGrid();
                initRelatedTransactionGrid(isTaxi);
                adjustSortIconAndAlignment(initRelatedTransactionProp(isTaxi));
                loadRelatedTransactionGrid()
                $('#btnAddNote').closest("div").show();
            } else {
                $('#btnAddNote').closest("div").hide();
                Util.MessageDialog(result.Message.split('\n'), "Error");
                LG_HideProgressMask();
            }
        })
        .fail(function (err) {
            Util.MessageDialog("Error.", "Error");
            LG_HideProgressMask();
        });
}

function initNoteGrid(rowCount) {
    $("#tbl_TransactionNote").empty();
    Util.InitJqGrid(noteGridProp(rowCount), null, loadNoteGrid
    );
}

function loadNoteGrid() {
    $('#tbl_TransactionNote').text("");
    DocketTransactionDetailService.TransactionNote(urlOrigin, { docketKey: getDocketkey() })
        .done(function (result) {
            if (result.IsSuccess && result.Data) {
                var gridData = {
                    rows: result.Data,
                };
                var rowCount = result.Data.length;
                initNoteGrid(rowCount);
                adjustSortIconAndAlignment(noteGridProp(rowCount));
                var grid = $('#tbl_TransactionNote').getGrid()[0];
                $('#tbl_TransactionNote').getGrid().jqGrid('clearGridData');
                grid.addJSONData(gridData);
                $(window).trigger('resize');
            } else {
                $('#tbl_TransactionNote').text('No notes to display.')
            }
            $('#tbl_TransactionNote').show();
        })
        .fail(function (err) {
            $('#tbl_TransactionNote').hide();
            Util.MessageDialog(err.statusText, "Error");
            LG_HideProgressMask();
        });
}

function initRelatedTransactionGrid(isTaxi) {
    Util.InitJqGrid(initRelatedTransactionProp(isTaxi), null, loadRelatedTransactionGrid);
}

function loadRelatedTransactionGrid() {
    DocketTransactionDetailService.RelatedTransaction(urlOrigin, { docketKey: getDocketkey() })
        .done(function (result) {
            if (result.IsSuccess && result.Data) {
                var gridData = {
                    rows: result.Data,
                };
                var grid = $('#tbl_RelatedTransaction').getGrid()[0];
                $('#tbl_RelatedTransaction').getGrid().jqGrid('clearGridData');
                grid.addJSONData(gridData);
                $('#div_RelatedTransaction').show();
                $(window).trigger('resize');
            } else {
                $('#div_RelatedTransaction').hide();
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            $('#div_RelatedTransaction').hide();
            Util.MessageDialog(err.statusText, "Error");
            LG_HideProgressMask();
        });
}

function viewMore(el) {
    var grid = $("#tbl_RelatedTransaction").getGrid();
    var thisBtn = $(el);
    var allBtn = $("button.viewMore");
    var rownum;
    allBtn.each(function (i) {
        if (thisBtn[0] === allBtn[i]) {
            rownum = i + 1;
        }
    });
    var data = grid.getRowData(rownum);
    if (data) {
        var html = $('#TransactionDetailDialog').html();
        var dialogDiv = $("#dialog")
        dialogDiv.empty();
        dialogDiv.append(html);
        dialogDiv.dialog({
            title: "Transaction Details",
            position: "center",
            modal: true,
            width: 800,
            open: function (event, ui) {
                $("input").blur();
            }
        });
        renderRaisedRequestData(data);
    }
}

function openAddNoteDialog() {
    LG_ShowProgressMask();
    DocketTransactionDetailService.TransactionNoteDialog(urlOrigin, null)
        .done(function (result) {
            var dialogDiv = $("#dialog")
            dialogDiv.empty();
            dialogDiv.append(result);
            dialogDiv.dialog({
                title: "Add Notes",
                position: "center",
                modal: true,
                width: 450,
            });
            LG_HideProgressMask();
        })
        .fail(function (err) {
            Util.MessageDialog(err.statusText, "Error");
            LG_HideProgressMask();
        });
}

function AddNote() {
    var data = {
        docketKey: getDocketkey(),
        noteDescription: $("#dialog #NoteDesciption").val()
    }
    if ($('#div_AddNote form').valid()) {
        DocketTransactionDetailService.AddTransactionNote(urlOrigin,data)
            .done(function (result) {
                CloseDialog();
                loadNoteGrid();
                if (!result.IsSuccess) {
                    Util.MessageDialog(result.Message);
                }

            })
            .fail(function (err) {
                Util.MessageDialog(err.statusText, "Error");
                LG_HideProgressMask();
            });
    }
}

function CloseDialog() {
    $("#dialog").dialog("close");
    $("#dialog").empty();
}

function getMemberkey() {
    return $("#MemberKey").val();
}
function getDocketkey() {
    return $("#DocketKey").val();
}
function loadDataToSection(sectionSelector, data) {
    for (var key in data) {
        var el = $(sectionSelector + " #" + key);
        if (el[0] != null) {
            if (el.hasClass("display-only")) {
                if (el.attr('type') != "checkbox") {
                    var parent = el.parent();
                    parent.append('<label id="' + key + '"></label>');
                    parent.children('label#' + key).attr("class", el.attr("class"));
                    el.remove();
                    el = parent.children('label#' + key);
                }
            }
            el.closest("div.form-group").removeClass("hidden");
            el.attr("data-original", data[key]);
            var value;
            if (el.hasClass("numeric-display")) {
                value = data[key] ? data[key] : "0";
            } else if (el.hasClass("money-display")) {
                value = data[key] ? data[key] : "$0.00";
            } else {
                value = data[key] ? data[key] : "-";
            }
            switch (el[0].nodeName) {
                case "INPUT":
                    if (el.attr('type') == "checkbox") {
                        el.prop("checked", data[key]);
                        $(el).closest("span").addClass(data[key] ? "checked" : "");
                    } else if (el.hasClass("datepicker")) {
                        el.val(data[key]);
                    } else {
                        el.val(value);
                    }
                    break;
                case "SPAN":
                    el.text(value);
                    break;
                case "LABEL":
                    el.text(value);
                    break;
                case "SELECT":
                    el.val(data[key]);
            }
        }
    }
}

function adjustSortIconAndAlignment(prop){
    var columns = prop.colModel;
    var tableId = prop.gridId;
    var gridId = tableId + 'Grid';
    for (var col of columns) {
        if (col.sortable == false) {
            var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
            $(cellSelector).find('span.s-ico').remove();
            $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            if (col.frozen) {
                var cellSelectorFrozen = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                $(cellSelectorFrozen).find('span.s-ico').remove();
                $(cellSelectorFrozen).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            }
            
        }
        if (col.align) {
            $("#" + gridId + "_" + col.name).addClass(col.align);
            $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
        }
    }
}

function reloadAllGrid() {
    loadRelatedTransactionGrid();
    loadNoteGrid();
}

function doRaisedRequestAction(el) {
    proceedAction(el, reloadAllGrid);
}