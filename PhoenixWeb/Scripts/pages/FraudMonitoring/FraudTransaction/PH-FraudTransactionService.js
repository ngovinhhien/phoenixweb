﻿(function (FraudTransactionService, $, undefined) {
    FraudTransactionService.FilterFraudTransaction = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/FilterFraudTransaction?',
            type: "GET",
            data: searchParams,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    FraudTransactionService.QuarantineTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/QuarantineTransaction',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    return {
        FilterFraudTransaction: FraudTransactionService.FilterFraudTransaction,
        QuarantineTransaction: FraudTransactionService.QuarantineTransaction
    };

}(window.FraudTransactionService = window.FraudTransactionService || {}, jQuery));