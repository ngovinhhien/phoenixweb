﻿var urlOrigin = Util.PH_UpdateURL();
var searchObject = {
    SortField: 'transactionDate',
    SortOrder: 'desc',
    PageIndex: '',
    PageSize: ''
};
var filterParams = {};

$(document).ready(function () {
    var currentYear = new Date().getFullYear();
    Util.InitDatepicker('.datepicker', {
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        yearRange: "1900:" + (currentYear)
    });

    $("#FromDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#ToDate").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#ToDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#FromDate").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    Util.InitJqGrid({
        gridId: 'Transaction',
        colNames: BuildColName(),
        colModel: BuildColModel(),
        rowNum: 20,
        height: 700,
        hasPager: true,
        allowSort: true,
        gridComplete: function () { resizeGridRows(50) }
    }, searchObject, loadDataTransactionGrid);

    adjustSortIconAndAlignment('Transaction');
    $('#btnSearch').click(function (e) {
        e.preventDefault();
        filterParams = Util.getFilterParams("#param-area");
        searchObject.PageIndex = 1;
        searchObject.PageSize = $("#Transaction").getPageSize();
        searchObject.SortField = 'transactionDate';
        searchObject.SortOrder = 'desc';
        Util.resetGridSortIcon('#Transaction');
        loadDataTransactionGrid();
    });

    $('.btn-setDate').click(function (e) {
        e.preventDefault();
        var today = new Date();
        var backDate = parseInt($(this).attr('back-date'));
        $('#ToDate').datepicker("setDate", Util.dateToStr(today, '/'));
        $('#FromDate').datepicker("setDate", Util.dateToStr(new Date(today.setDate(today.getDate() - backDate)), '/'));
        ChangeSetDateBtnState();
    });

    $('.filedate').on('change', function () {
        ChangeSetDateBtnState();
    });
    setTimeout(function () {
        var d = new Date();
        if (d.getDay() == 1) {
            $('#btnLast3Days').click();
        } else {
            $('#btnYesterday').click();
        }
    }, 500)

});

function ChangeSetDateBtnState() {
    var today = new Date();
    var setDateBtn = $('.btn-setDate');
    setDateBtn.removeClass('btn-setDate-active');
    if (Util.dateToStr($('#ToDate').datepicker('getDate'), '/') == Util.dateToStr(today, '/')) {
        var fromDateText = Util.dateToStr($('#FromDate').datepicker('getDate'), '/');
        setDateBtn.each(function (btn) {
            var backDate = $(setDateBtn[btn]).attr('back-date');
            var fromDate = new Date();
            if (Util.dateToStr(new Date(fromDate.setDate(fromDate.getDate() - backDate)), '/') == fromDateText) {
                $(setDateBtn[btn]).addClass('btn-setDate-active');
            }
        });
    }
}

function resizeGridRows(delay = null) {
    $('#Transaction ' + '.frozen-bdiv').hide();
    if (delay) {
        setTimeout(function () {
            $("#Transaction").getGrid().resizeFrozenRows();
        }, delay);
    } else {
        $("#Transaction").getGrid().resizeFrozenRows();
    }
    $('#Transaction ' + '.frozen-bdiv').show();
}


function BuildColName() {
    var colNames = [
        "Actions",
        "MID",
        "Trading Name",
        "TID",
        "Type",
        "Txn Total",
        "Txn Date",
        "Fraud Reason",
        "Mode",
        "Card Details",
        "Card Holder",
        "Bank Details",
        "MCC Category",
        "Txn Status",
        "File Date"
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        { key: false, name: "", index: "", width: "100", align: 'center', editable: false, formatter: addAction, frozen: true, sortable: false },
        {
            key: false, name: "MemberId", index: "MemberId", align: 'left', editable: false, width: 80, sortable: false,
            formatter: function (cellValue, option, rowObject) {
                return '<a class="underlined clickable" target="_blank" href="MemberReview?memberKey=' + rowObject.MemberKey + '">' + cellValue + '</a>';
            }
        },
        { key: false, name: "TradingName", index: "TradingName", align: 'left', editable: false, sortable: true, formatter: tradingName, width: 160 },
        { key: false, name: "TerminalId", index: "TerminalId", align: 'left', editable: false, sortable: false, width: 80 },
        { key: false, name: "BusinessTypeName", index: "BusinessTypeName", align: 'left', editable: false, sortable: false, width: 170 },
        {
            key: false, name: "TransactionTotal", firstsortorder: "desc", index: "TransactionTotal", editable: false, sortable: true, width: 110, align: "right", formatter: transactionDetail
        },
        {
            key: false, name: "TransactionDate", index: "TransactionDate", align: 'left', editable: false, sortable: true, width: 100,
            formatter: function (cellValue, option, rowObject) {
                var html = '';
                cellValue = cellValue.replace(' ', ';')
                var arr = cellValue.split(';');
                for (var el of arr) {
                    html += CustomCellText(el);
                }
                return html;
            }
        },
        { key: false, name: "FraudReason", index: "FraudReason", align: 'left', editable: false, width: 170, sortable: false, formatter: reason },
        { key: false, name: "CardEntryMode", index: "CardEntryMode", align: 'left', width: 90, editable: false, sortable: false },
        { key: false, name: "CardType", index: "CardType", align: 'left', editable: false, sortable: true, formatter: cardDetail, width: 140 },
        { key: false, name: "CardHolderName", index: "CardHolderName", align: 'left', editable: false, sortable: true },
        { key: false, name: "BankName", index: "BankName", align: 'left', editable: false, sortable: false, formatter: bankDetail, width: 220 },
        { key: false, name: "mccCode", index: "mccCode", align: 'left', editable: false, sortable: false, formatter: MCCCategory, width: 170 },
        { key: false, name: "TransactionStatus", index: "TransactionStatus", align: 'left', editable: false, sortable: false, width: 110 },
        {
            key: false, name: "FileDate", index: "FileDate", align: 'left', editable: false, sortable: false, width: 90
        }
    ];

    return colModel;
}

function addAction(cellValue, option, rowObject) {
    var buttonHtml = '<button class="btn-inline ' + ((rowObject.IsQuarantinable || rowObject.IsRelease) ? (' clickable" onclick="quarantineTxn(this)" docketKey="'
            + rowObject.EftDocketKey + '" action-type="'
            + (rowObject.IsRelease ? 'release' : 'quarantine') + '"') : ' btn-disabled"')
            + ' style="width: 66px;"  >'
            + (rowObject.IsQuarantined ? 'Release' : 'Quarantine') + '</button>';
    return buttonHtml;
}

function quarantineTxn(el) {
    var docketKey = $(el).attr('docketkey');
    var action = $(el).attr('action-type');
    var message = action == "release" ? "Do you want to release this transaction?" : "Do you want to quarantine this transaction?";
    Util.ConfirmDialog(message, "Confirm").done(function () {
        LG_ShowProgressMask();
        FraudTransactionService.QuarantineTransaction(urlOrigin, { docketKey: docketKey, action: action })
            .done(function (result) {
                if (!result.IsSuccess) {
                    Util.MessageDialog(result.Message.split('\n'), "Error");
                }
                loadDataTransactionGrid();
                LG_HideProgressMask();
            })
            .fail(function (err) {
                Util.MessageDialog("Error", "Error");
                LG_HideProgressMask();
            });
    });
}

function tradingName(cellValue, option, rowObject) {
    return CustomCellText(cellValue, ['text-bold']);
}

function transactionDetail(cellValue, option, rowObject) {
    return CustomCellText(('$' + (cellValue == null ? "0.00" : numberWithCommas(cellValue.toFixed(2)))).replace('$-', '-$'), ['text-right', 'text-bold']) + (rowObject.IsQuarantined ? CustomCellText((rowObject.IsQuarantined ? ' Quarantined' : ''), ['text-right']) : '');
}

function numberWithCommas(num) {
    return String(num).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, '$1,');
}

function reason(cellValue, option, rowObject) {
    var html = '';
    if (cellValue) {
        var reasonArray = cellValue.split(';');
        var count = reasonArray.length;
        for (var i = 0; i < count; i++) {
            html = html + CustomCellText(reasonArray[i], ['has-border', (i > 1 ? 'hidden' : 'dummy')]);
        }
        html = html + (count > 2 ? CustomCellText('+ ' + '<a class="underlined clickable" onclick="showMore(this)">' + (count - 2).toString() + ' more</a>') : '');
    }
    return html;
}

function showMore(el) {
    var all = $(el).parent().parent().find('p');
    all.each(function (i) {
        $(all[i]).removeClass("hidden");
    });
    $(el).parent().remove();
    resizeGridRows();
}

function cardDetail(cellValue, option, rowObject) {
    var html = '';
    var cardName = rowObject.CardType.toUpperCase().replace(/ /g, '');
    var imgHtml = '';
    if (CARD_IMAGE[cardName] != undefined) {
        imgHtml = '<img height="12" width="20" src="' + CARD_IMAGE[cellValue] + '"> ';
    }
    html = html + CustomCellText(imgHtml + rowObject.CardType);
    html = html + CustomCellText(rowObject.CardNumber, ['underlined']);
    return html;
}

function bankDetail(cellValue, option, rowObject) {
    return (rowObject.BankName ? CustomCellText(rowObject.BankName) : '') + (rowObject.BankCountryName ? CustomCellText(rowObject.BankCountryName) : '');
}

function MCCCategory(cellValue, option, rowObject) {
    return (cellValue ? CustomCellText(cellValue) : '') + (rowObject.mccCategory ? CustomCellText(rowObject.mccCategory) : '');
}

const CARD_IMAGE = {
    MASTERCARD: '../assets/global/img/mastercard.svg',
    MAESTRO: '../assets/global/img/maestro.svg',
    PAYPAL: '../assets/global/img/paypal.svg',
    UNIONPAY: '../assets/global/img/unionpay.svg',
    VISA: '../assets/global/img/visa.svg',
    JCB: '../assets/global/img/jcb.svg',
    DISCOVER: '../assets/global/img/discover.svg',
    DINER: '../assets/global/img/diner.svg',
    AMEX: '../assets/global/img/amex.svg',
    ALIPAY: '../assets/global/img/mastercard.svg',
}

function CustomCellText(text, options = null) {
    if (!text) {
        text = '';
    }
    var tagClass = "customedText";
    if (options != null) {
        for (var opt of options) {
            tagClass = tagClass + " " + opt;
        }
    }
    return '<p class="' + tagClass + '">' + text + ' ' + '</p>';
}

function loadDataTransactionGrid() {
    var searchParams = GatherParams();
    if (searchParams.SearchText && !/^[a-zA-Z0-9]*[0-9]+$/.test(searchParams.SearchText)) {
        Util.MessageDialog("Member ID or Terminal ID format is invalid.", "Error");
        return;
    }
    LG_ShowProgressMask();
    FraudTransactionService.FilterFraudTransaction(urlOrigin, searchParams)
        .done(function (result) {
            $('#Transaction').getGrid().jqGrid('clearGridData');
            if (result.IsSuccess) {
                if (result.Data) {
                    var grid = $('#Transaction').getGrid()[0];
                    grid.addJSONData(result.Data);
                    $("#Transaction").getGrid().setGridWidth($("#wrapper").width());
                    $(window).trigger("resize");
                } else {
                    Util.MessageDialog('No result found', 'Information');
                }
            }
            else {
                Util.MessageDialog(result.Message.split('\n'), "Error");
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            Util.MessageDialog("Error", "Error");
            LG_HideProgressMask();
        });
}

function GatherParams() {
    var searchParams = {};
    Util.addParams(searchParams, filterParams);
    Util.addParams(searchParams, searchObject);
    return searchParams
}

function adjustSortIconAndAlignment(tableId){
    var columns = BuildColModel();
    var gridId = tableId + 'Grid';
    for (var col of columns) {
        if (col.sortable == false) {
            var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
            $(cellSelector).find('span.s-ico').remove();
            $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            if (col.frozen) {
                var cellSelectorFrozen = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                $(cellSelectorFrozen).find('span.s-ico').remove();
                $(cellSelectorFrozen).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
            }
            
        }
        if (col.align) {
            $("#" + gridId + "_" + col.name).addClass(col.align);
            $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
        }
    }
}