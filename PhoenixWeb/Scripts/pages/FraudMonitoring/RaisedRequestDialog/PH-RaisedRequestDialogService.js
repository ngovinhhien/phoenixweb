﻿(function (RaisedRequestDialogService, $, undefined) {
    RaisedRequestDialogService.cancelchargeback = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/CancelChargeback',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    RaisedRequestDialogService.reversechargeback = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/ReverseChargeback',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    return {
        DocketTransactionDetailData: RaisedRequestDialogService.DocketTransactionDetailData,
    };

}(window.RaisedRequestDialogService = window.RaisedRequestDialogService || {}, jQuery));