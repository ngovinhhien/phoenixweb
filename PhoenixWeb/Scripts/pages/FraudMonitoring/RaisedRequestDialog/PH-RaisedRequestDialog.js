﻿function renderRaisedRequestData(data) {
    var actionArr = [];
    if (data.IsCancelRequest === 'true')
        actionArr.push('cancel');
    if (data.IsReverseRequest === 'true')
        actionArr.push('reverse');
    for (var action of actionArr) {
        $("#dialog button[request-type=" + data.RaiseRequestTypeName.toLowerCase().replace(/ /g,"") + "][action-type=" + action + "]").removeClass("hidden-btn");
    }
    $('#dialog button.hidden-btn').remove();
    if (data.RaiseRequestTypeName.toLowerCase() == "reversal") {
        $("#dialog #FeeAmount").closest("div.row").remove();
        $("#dialog #ReferenceNumber").closest("div.row").remove();
    }
    loadDataToSection("#dialog", data);
    if (data.FreshdeskTicketURL) {
        $("#dialog #FreshdeskTicketNo").parent().append('<a class="clickable underlined" target="_blank" href="' + data.FreshdeskTicketURL + '">' + data.FreshdeskTicketNo + '</a>');
        $("#dialog #FreshdeskTicketNo").remove();
    }
    setTimeout(function () {
        $("#dialog .form-group a").blur();
    }, 100);
}
function getRequestId() {
    return $("#dialog #RaiseRequestID").val();
}
function proceedAction(el, reload) {
    var action = $(el).attr("action-type");
    var type = $(el).attr("request-type");
    Util.ConfirmDialog("Are you sure you want to " + action + " this " + type + "? This can not be undone.", "Confirmation")
        .done(function () {
            var serviceFunction = RaisedRequestDialogService[action + type];
            if (serviceFunction) {
                serviceFunction(urlOrigin, { RaiseRequestID: getRequestId() })
                    .done(function (result) {
                        if (!result.IsSuccess) {
                            Util.MessageDialog(result.Message.split('\n'), "Error");
                        } else {
                            CloseDialog();
                        }
                        LG_HideProgressMask();
                        reload();
                    })
                    .fail(function (err) {
                        Util.MessageDialog(err.statusText, "Error");
                        LG_HideProgressMask();
                        reload();
                    });
            }
        });
}