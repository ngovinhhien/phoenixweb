﻿$(document).ready(function () {
    initTabComponents();
})


var listTable = {};
var listFilter = {};
var listTab = {
    tab_MemberNotes: {
        MemberNotes: {
            type: "table",
            props: {
                gridId: 'tbl_MemberNotes',
                colNames: ["Created Date", "Type", "Note", "Created By"],
                colModel: [
                    { key: false, name: "CreatedDate", index: "notedate", width: "100", align: 'left', editable: false, sortable: true },
                    { key: false, name: "NoteType", index: "warning", align: 'left', editable: false, sortable: true },
                    { key: false, name: "NoteDesciption", index: "NoteDesciption", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CreatedBy", index: "CreatedBy", align: 'left', editable: false, sortable: false },
                ],
                enableShrinkToFit: true
            },
            sourceFunction: 'GetMemberNotesByMemberKey',
            searchObjectDefault: { SortField: 'NoteDate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () { return null; }
        }
    },
    tab_Transaction: {
        Transaction: {
            type: "table",
            props: {
                gridId: 'tbl_Transaction',
                colNames: ["Actions", "TID", "Txn Date", "Status", "PMT Status", "Amount", "Extra", "Total", "Remaining Amount", "File Date", "Card Number", "Card Holder", "Used?", "Batch ID", "Batch No.", "BID"],
                colModel: [
                    {
                        key: false, name: "Action", frozen: false, width: "265", align: 'left', editable: false, sortable: false, frozen: true, title: false,
                        formatter: function (cellValue, option, rowObject) {
                            var buttonHtml =
                                // view detail
                                '<a class="btn-inline clickable" target="_blank" href="DocketTransactionDetail?docketKey=' + rowObject.EftDocketKey + '&memberKey=' + getMemberkey()
                                + '" formtarget="_blank">View details</a>&nbsp&nbsp'
                                // quarantine
                                + '<button class="btn-inline' + ((rowObject.IsQuarantinable || (rowObject.TransactionStatus.toLowerCase() == 'quarantined' && rowObject.Amount.indexOf("-") == -1))? (' clickable" onclick="quarantineTxn(this)" docketKey="'
                                    + rowObject.EftDocketKey + '" action-type="'
                                    + (rowObject.TransactionStatus == '' ? 'quarantine' : 'release') + '"') : ' btn-disabled"')
                                + ' style="width: 66px;" >'
                                + (rowObject.TransactionStatus == '' ? 'Quarantine' : 'Release') + '</button>&nbsp&nbsp'
                                // chargeback
                                + '<button  class="btn-inline' + (rowObject.IsRaiseChargeback ? (' clickable" onclick="initDialog(this)" docketKey="'
                                    + rowObject.EftDocketKey + '" dialog-name="CreateChargeback" + ') : ' btn-disabled"')
                                + ' >Chargeback</button>';
                            return buttonHtml;
                        }
                    },
                    { key: false, name: "TerminalId", width: '90', index: "TerminalId", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TransactionDate", width: '165', index: "docketDate", align: 'left', editable: false },
                    { key: false, name: "TransactionStatus", width: '90', align: 'left', editable: false, sortable: false },
                    { key: false, name: "PaymentStatus", width: '100', index: "PaymentStatus", align: 'left', editable: false, sortable: false },
                    { key: false, name: "Amount", width: '90', align: 'right', editable: false, sortable: false },
                    { key: false, name: "Extras", width: '90', align: 'right', editable: false, sortable: false },
                    {
                        key: false, name: "TotalAmount", index: "docketTotal", firstsortorder: "desc", align: 'center', editable: false, sortable: true, width: 90, formatter: numericTextFormat
                    },

                    { key: false, name: "RemainingAmount", width: '140', index: "docketTotal", align: 'right', editable: false, sortable: false },
                    { key: false, name: "FileDate", width: '90', index: "FileDate", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardNumber", width: '130', index: "CardNumber", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardHolder", width: '170', index: "CardHolder", align: 'left', editable: false, sortable: false },
                    { key: false, name: "Used", width: '60', align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchId", width: '140', index: "BatchId", align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchNumber", width: '110', index: "BatchNumber", align: 'left', editable: false, sortable: false },
                    { key: false, name: "BusinessId", width: '150', index: "BusinessId", align: 'left', editable: false, sortable: false }
                ],
                enableAutoWidth: false,
            },
            sourceFunction: 'FilterMemberTransaction',
            searchObjectDefault: { SortField: 'docketDate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () {
                var filterParams = Util.getFilterParams("#filter_Transaction");
                filterParams.TotalAmount = filterParams.TotalAmount.replace(',', '').replace('$', '');
                return filterParams;
            },
            validate: function (params) {
                var flag = true;
                var listError = [];
                if (params && params.TerminalId && ! /^[0-9]+$/.test(params.TerminalId)){
                    listError.push('TID format is invalid.');
                    flag = false;
                }
                if (params && params.TotalAmount && ! /^[+-]?((\d+(\.\d*)?)|(\.\d+))$/.test(params.TotalAmount)) {
                    listError.push('Total format is invalid.');
                    flag = false;
                }
                if (params && params.CardNumber && ! /^[0-9.*]+$/.test(params.CardNumber)) {
                    listError.push('Card number format is invalid.')
                    flag = false;
                }
                if (!flag) {
                    showGridMessage("Transaction", listError);
                }
                return flag;
            },
        },
        TransactionFilter: {
            type: "filter",
            init: function () {
                var currentYear = new Date().getFullYear();
                Util.InitDatepicker('#filter_Transaction .datepicker', {
                    changeYear: true,
                    showButtonPanel: true,
                    changeMonth: true,
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    dateFormat: 'dd/mm/yy',
                    yearRange: "1900:" + (currentYear)
                });
                $("#filter_Transaction #FromDate").datepicker('option', 'beforeShowDay', function (date) {
                    var endDate = $("#filter_Transaction #ToDate").datepicker('getDate');
                    return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
                });

                $("#filter_Transaction #ToDate").datepicker('option', 'beforeShowDay', function (date) {
                    var startDate = $("#filter_Transaction #FromDate").datepicker('getDate');
                    return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
                });
                var today = new Date();
                $('#filter_Transaction #ToDate').datepicker("setDate", Util.dateToStr(today, '/'));
                $('#filter_Transaction #FromDate').datepicker("setDate", Util.dateToStr(new Date(today.setDate(today.getDate() -7)), '/'));
                $("#btn_SearchTrans").click(function () {
                    loadGrid('Transaction', true);
                });
            }
        }
    },
    tab_Overview: {
        BankAccount: {
            type: "table",
            props: {
                gridId: 'tbl_BankAccount',
                colNames: ["BSB", "Account Number", "Account Name", "Purpose Type"],
                colModel: [
                    { key: false, name: "Bsb", frozen: false, width: "200", align: 'left', editable: false, sortable: false },
                    { key: false, name: "AccountNumber", align: 'left', editable: false, sortable: false },
                    { key: false, name: "AccountName", align: 'left', editable: false, sortable: false },
                    { key: false, name: "PurposeTypeName", align: 'left', editable: false, sortable: false }
                ],
                height: 'auto',
                rowNum: 10000,
                enableShrinkToFit: true,
                hasPager: false
            }
        },
        TerminalAllocation: {
            type: "table",
            props: {
                gridId: 'tbl_TerminalAllocation',
                colNames: ["Sub-business", "State", "Type", "Terminal ID", "Effective Date", "Status"],
                colModel: [
                    { key: false, name: "VehicleId", frozen: false, align: 'left', editable: false, sortable: false },
                    { key: false, name: "StateName", align: 'left', editable: false, sortable: false },
                    { key: false, name: "VehicleTypeName", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TerminalId", align: 'left', editable: false, sortable: false },
                    { key: false, name: "EffectiveDate", align: 'left', editable: false, sortable: false },
                    { key: false, name: "LatestStatus", align: 'left', editable: false, sortable: false }
                ],
                height: 'auto',
                rowNum: 10000,
                enableShrinkToFit: true,
                hasPager: false
            },
        },
        CustomFunction: function (props) {
            if (!listTable.BankAccount.loaded || !listTable.TerminalAllocation.loaded || props.forceLoad) {
                LG_ShowProgressMask();
                MemberReviewService.GetMemberOverviewByMemberKey(urlOrigin, { memberKey: getMemberkey() })
                    .done(function (result) {
                        $('#tbl_TerminalAllocation').getGrid().jqGrid('clearGridData');
                        $('#tbl_BankAccount').getGrid().jqGrid('clearGridData');
                        if (result.IsSuccess) {
                            if (result.Data) {
                                if (result.Data.BankAccount) {
                                    $('#tbl_BankAccount').getGrid()[0].addJSONData({ rows: result.Data.BankAccount });
                                } else {
                                    $('#tabsArea').hide();
                                    Util.MessageDialog("This member does not have any bank account.", "Error");
                                }
                                if (result.Data.TerminalAllocation) {
                                    $('#tbl_TerminalAllocation').getGrid()[0].addJSONData({ rows: result.Data.TerminalAllocation });
                                }
                                listTable.BankAccount.loaded = true;
                                listTable.TerminalAllocation.loaded = true;
                            } else {
                                $('#tabsArea').hide();
                                Util.MessageDialog("This member does not have any bank account.", "Error");
                            }
                        }
                        else {
                            $('#tabsArea').hide();
                            Util.MessageDialog(result.Message.split('\n'), "Error");
                        }
                        LG_HideProgressMask();
                    })
                    .fail(function (err) {
                        $('#tabsArea').hide();
                        Util.MessageDialog("Error", "Error");
                        LG_HideProgressMask();
                    });
            }
        }
    },
    tab_PaymentHistory: {
        PaymentHistory: {
            type: "table",
            props: {
                gridId: 'tbl_PaymentHistory',
                colNames: ["Pay By Office", "PMT Date", "Total Amount", "Status", "Lodgement Ref. No.", "BSB", "Account", "Account Name", "Failed Date"],
                colModel: [
                    { key: false, name: "OfficeName", width: "140", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TransactionDatetime", index: "transactiondate", width: "180",align: 'left', editable: false, sortable: true },
                    { key: false, name: "TotalAmount", index: "totalamount", firstsortorder: "desc", width: "130", align: 'center', fixed: true, editable: false, sortable: true, formatter: numericTextFormat },
                    { key: false, name: "Status", width: "90",align: 'left', editable: false, sortable: false },
                    { key: false, name: "LodgementReferenceNo", width: "170", align: 'left', editable: false, sortable: false },
                    { key: false, name: "Bsb", width: "100", align: 'left', editable: false, sortable: false },
                    { key: false, name: "AccountNumber", width: "100", align: 'left', editable: false, sortable: false },
                    { key: false, name: "AccountName", width: "180", align: 'left', editable: false, sortable: false },
                    { key: false, name: "FailedDatetime", width: "200", align: 'left', editable: false, sortable: false }
                ],
                enableShrinkToFit: true
            },
            sourceFunction: "GetPaymentHistory",
            aggregateFunction: function (data) {
                loadDataToSection("#HistoryAggregation", data.extensionObject);
            },
            searchObjectDefault: { SortField: 'transactiondate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () { return null; }
        }
    },
    tab_PaymentDetails: {
        PaymentDetail: {
            type: "table",
            props: {
                gridId: 'tbl_PaymentDetail',
                colModel: [
                    { key: false, name: "TerminalId", label: "TID", width: "90", align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchNumber", label: "Batch No.", width: "90", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TransactionDate", label: "Txn Date", index: "transactionDate", width: "165", align: 'left', editable: false },
                    { key: false, name: "FileDate", label: "File Date", index: "fileDate", width: "90", align: 'left', editable: false },
                    { key: false, name: "CardNumber", label: "Card Number", width: "170", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardType", label: "Card Type", width: "100", align: 'left', editable: false, sortable: false },
                    { key: false, name: "RateUsed", label: "Rate Used", width: "100", align: 'right', editable: false, sortable: false },
                    { key: false, name: "Amount", firstsortorder: "desc", label: "Amount", index: "amount", width: "100", align: 'center', editable: false, formatter: numericTextFormat },
                    { key: false, name: "Description", label: "Description", index: "msf", width: "300", align: 'left', editable: false, sortable: false },
                    { key: false, name: "LodgementRefNo", label: "Lodgement Ref. No.", width: "200", align: 'left', editable: false, sortable: false },
                    { key: false, name: "ProcessedDate", label: "Processed Date", width: "150", align: 'left', editable: false, sortable: false },
                    { key: false, name: "ProcessedBy", label: "Processed By", width: "140", align: 'left', editable: false, sortable: false }
                ],
            },
            sourceFunction: "GetPaymentDetails",
            searchObjectDefault: { SortField: 'transactionDate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () { return null; }
        }
    },
    tab_PendingTransaction: {
        PendingTransaction: {
            type: "table",
            props: {
                gridId: 'tbl_PendingTransaction',
                colModel: [
                    { key: false, name: "ExpectPaidDate", label: "Expect Paid Date",width: "165", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TerminalId", label: "TID", width: "90", align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchNumber", label: "Batch No.", width: "120", align: 'left', editable: false, sortable: false },
                    { key: false, name: "TransactionDate", label: "Txn Date", index: "transactionDate", width: "165", align: 'left', editable: false, sortable: true },
                    { key: false, name: "FileDate", label: "File Date", index: "fileDate", width: "90", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardNumber", label: "Card Number", width: "170", align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardType", label: "Card Type", width: "100", align: 'left', editable: false, sortable: false },
                    { key: false, name: "RateUsed", label: "Rate Used", width: "100", align: 'right', editable: false, sortable: false },
                    { key: false, name: "Amount", label: "Amount", index: "amount", width: "100", align: 'center', firstsortorder: "desc", fixed: true, editable: false, sortable: true, formatter: numericTextFormat },
                    { key: false, name: "Description", label: "Description", index: "msf", width: "300", align: 'left', editable: false, sortable: false },
                ],
                enableShrinkToFit: true
            },
            sourceFunction: "GetPendingTransaction",
            aggregateFunction: function (data) {
                loadDataToSection("#PendingTransAggregation", data.extensionObject);
            },
            searchObjectDefault: { SortField: 'transactionDate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () { return null; }
        }
    },
    tab_QuarantinedTransaction: {
        QuarantinedTransaction: {
            type: "table",
            props: {
                gridId: 'tbl_QuarantinedTransaction',
                colModel: [
                    {
                        key: false, name: "Actions", label: "Actions", width: '175', align: 'left', editable: false, sortable: false,
                        formatter: function (cellValue, option, rowObject) {
                            var buttonHtml =
                                '<button class="btn-inline' + (rowObject.IsRelease ? (' clickable" onclick="quarantineTxn(this)" docketKey="'
                                    + rowObject.DocketKey + '" action-type="release"') : ' btn-disabled"')
                                + ' style="width: 66px;" >Release</button>&nbsp&nbsp'
                                    + '<button  class="btn-inline' + (rowObject.IsRaiseReverse ? (' clickable" onclick="initDialog(this)" docketKey="'
                                    + rowObject.DocketKey + '" dialog-name="CreateReverse" + ') : ' btn-disabled"')
                                + ' >Reverse</button>';
                            return buttonHtml;
                        }
                    },
                    { key: false, name: "TerminalId", label: "TID", width: '90', align: 'left', editable: false, sortable: false },
                    { key: false, name: "TransactionDate", label: "Txn Date", width: '180', index: "transactiondate", align: 'left', editable: false },
                    { key: false, name: "PaymentStatus", label: "PMT Status", width: '100', align: 'left', editable: false, sortable: false },
                    { key: false, name: "Amount", label: "Amount", width: '110', index: "amount", align: 'right', editable: false, sortable: false },
                    { key: false, name: "Extras", label: "Extra", width: '110', align: 'right', editable: false, sortable: false },
                    { key: false, name: "TotalAmount", label: "Total Amount", firstsortorder: "desc", width: '130', index: "totalAmount", align: 'center', editable: false, formatter: numericTextFormat },
                    { key: false, name: "FileDate", label: "File Date", width: '110', align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardNumber", label: "Card Number", width: '130', align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardHolderName", label: "Card Holder", width: '190', align: 'left', editable: false, sortable: false },
                    { key: false, name: "Used", label: "Used?", width: '60', align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchId", label: "Batch ID", width: '140', align: 'left', editable: false, sortable: false },
                    { key: false, name: "BatchNumber", label: "Batch No.", width: '110', align: 'left', editable: false, sortable: false },
                    { key: false, name: "BusinessId", label: "BID", width: '180', align: 'left', editable: false, sortable: false }
                ],
            },
            validate: function (params) {
                var flag = true;
                var listError = [];
                if (params && params.TotalAmount && ! /^[+-]?((\d+(\.\d*)?)|(\.\d+))$/.test(params.TotalAmount)) {
                    listError.push('Total amount format is invalid.');
                    flag = false;
                }
                if (params && params.CardNumber && ! /^[0-9.*]+$/.test(params.CardNumber)) {
                    listError.push('Card number format is invalid.')
                    flag = false;
                }
                if (!flag) {
                    showGridMessage("QuarantinedTransaction", listError);
                }
                return flag;
            },
            sourceFunction: 'GetQuarantinedTransaction',
            searchObjectDefault: { SortField: 'transactiondate', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () {
                var filterParams = Util.getFilterParams("#filter_QuarantinedTransaction");
                filterParams.TotalAmount = filterParams.TotalAmount.replace("$", "").replace(/,/gi,"");
                return filterParams;
            }
        },
        QuarantinedTransactionFilter: {
            type: "filter",
            init: function () {
                $("#btn_SearchQuarantinedTrans").click(function () {
                    loadGrid('QuarantinedTransaction', true);
                });
            }
        }
    },
    tab_RaisedRequest: {
        RaisedRequest: {
            type: "table",
            props: {
                gridId: 'tbl_RaisedRequest',
                colModel: [
                    {
                        key: false, name: "Actions", label: "Actions", align: "left", editable: false, sortable: false, frozen: true, width: '215', title: false,
                        formatter: function (cellValue, option, rowObject) {
                            var html = '<a class="btn-inline clickable" target="_blank" href="DocketTransactionDetail?docketKey=' + rowObject.DocketKey + '&memberKey=' + getMemberkey()
                                + '" formtarget="_blank">Open original txn</a>&nbsp&nbsp'
                                + '<button class="btn-inline clickable" onclick="initDialog(this)" dialog-name="RaisedRequestDetails" RaiseRequestID="'
                                + rowObject.RaiseRequestID + '">View more</button>';
                            return html;
                        }
                    },
                    {
                        key: false, name: "RaiseRequestID", index: "requestID", label: "Request ID", width: '100', align: 'left', editable: false
                    },
                    { key: false, name: "RaiseRequestTypeName", label: "Type", width: '150', align: 'left', editable: false, sortable: false },
                    { key: false, name: "ReasonDescription", label: "Reason", width: '250', align: 'left', editable: false, sortable: false },
                    { key: false, name: "Amount", index: "Amount", label: "Amount", width: '100', align: "center", firstsortorder: "desc", editable: false, sortable: true, fixed: true, formatter: numericTextFormat },
                    { key: false, name: "Fee", label: "Fee", width: '110', align: 'right', editable: false, sortable: false },
                    { key: false, name: "RequestStatus", label: "Status", width: '110', align: 'left', editable: false, sortable: false },
                    { key: false, name: "TotalTransactionAmount", index: "transactionAmount", label: "Txn. Amount", width: '120', align: 'center', firstsortorder: "desc", editable: false, formatter: numericTextFormat },
                    { key: false, name: "TransactionDate", label: "Txn. Date", width: '165', align: 'left', editable: false, sortable: false },
                    { key: false, name: "CardNumber", label: "Card Number", width: '130', align: 'left', editable: false, sortable: false },
                    { key: false, name: "CreatedBy", label: "Created By", width: '130', align: 'left', editable: false, sortable: false },
                    { key: false, name: "CreatedDate", label: "Created Date", width: '160', align: 'left', editable: false, sortable: false },
                    { key: false, name: "ProcessedDate", label: "Processed Date", width: '165', align: 'left', editable: false, sortable: false },
                    { key: false, name: "ReferenceNumber", label: "Disputed Ref. No.", width: '180', align: 'right', editable: false, sortable: false },
                    {
                        key: false, name: "FreshdeskTicket", label: "Freshdesk Ticket", width: '130', align: 'right', editable: false, sortable: false,
                        formatter: function (cellValue, option, rowObject) {

                            return (rowObject.FreshdeskTicketURL ? ('<a class="underlined" target="_blank" href="' + rowObject.FreshdeskTicketURL + '">' + rowObject.FreshdeskTicketNo + '</a>') : '');
                        }
                    },
                    { key: false, name: "RequestNote", label: "Notes", width: '280', align: 'left', editable: false, sortable: false },
                    { key: false, name: "FailedDate", sortable: false, hidden: true },
                    { key: false, name: "LodgementReferenceNo", sortable: false, hidden: true },
                    { key: false, name: "FreshdeskTicketNo", sortable: false, hidden: true },
                    { key: false, name: "FreshdeskTicketURL", sortable: false, hidden: true },
                    { key: false, name: "IsCancelRequest", sortable: false, hidden: true },
                    { key: false, name: "IsReverseRequest", sortable: false, hidden: true },
                ],
            },
            validate: function (params) {
                if (params && params.SearchText && ! /^[0-9a-zA-Z]+$/.test(params.SearchText)) {
                    showGridMessage('RaisedRequest', 'Only number or letters allowed');
                    return false;
                }
                return true;
            },
            sourceFunction: 'GetRaisedRequest',
            searchObjectDefault: { SortField: 'requestID', SortOrder: 'desc', PageIndex: 1, PageSize: 20 },
            getFilterParams: function () {
                var filterParams = Util.getFilterParams("#filter_RaisedRequest");
                return filterParams;
            }
        },
        RaisedRequestFilter: {
            type: "filter",
            init: function () {
                $("#btn_SearchRaisedRequest").click(function () {
                    loadGrid('RaisedRequest', true);
                });
                MemberReviewService.GetRaisedRequestType(urlOrigin, {})
                    .done(function (result) {
                        if (result.IsSuccess && result.Data) {
                            var data = result.Data;
                            for (var item of data) {
                                var elHtml = '<option value="'+item.Value+'"' + (item.Selected? 'selected' : '') + '>' + item.Text + '</option>'
                                $("#filter_RaisedRequest #RequestTypeId").append(elHtml);
                            }
                        }
                    })
                    .fail(function (err) {
                        Util.MessageDialog("Error", err.statusText);
                        LG_HideProgressMask();
                    });
            }
        }
    }
}

const COMMON_TABLE_PROPS = {
    rowNum: 20,
    height: 400,
    hasPager: true,
    enableAutoWidth: true,
    allowSort: true
}

var listDialog = {
    AddNotes: {
        sourceFunction: 'AddMemberNoteDialog',
        modalProps: {
            title: "Add Notes",
            position: "center",
            width: "500px",
            modal: true
        },
        functions: {
            addNote: function () {
                $('#dialog .action-proceed').click(function () {
                    if ($('#div_AddNote form').valid()) {
                        LG_ShowProgressMask();
                        MemberReviewService.AddMemberNote(urlOrigin, getDialogParameters())
                            .done(function (result) {
                                if (result.IsSuccess) {
                                    loadGrid('MemberNotes', true);
                                    CloseDialog();
                                } else {
                                    Util.MessageDialog(result.Message.split('\n'), "Error");
                                }
                                LG_HideProgressMask();
                            })
                            .fail(function (err) {
                                Util.MessageDialog(err.statusText, "Error");
                                LG_HideProgressMask();

                            });
                    }
                });
            }
        }
    },
    CreateChargeback: {
        sourceFunction: 'CreateChargebackDialog',
        modalProps: {
            title: "Create Chargeback",
            position: "center",
            width: "1000px",
            modal: true
        },
        initParam: function (el) {
            return { docketKey: $(el).attr('docketkey') };
        },
        validate: function (first) {
            var result = true;
            var validator = $('#div_CreateChargeback form').data('validator');
            if (validator || first) {
                var reasonKey = $('#div_CreateChargeback #ChargebackReasonKey');
                var otherReason = $('#div_CreateChargeback #OtherChargebackReason');
                reasonKey.parent().find("label[for='ChargebackReasonKey']").hide();
                otherReason.parent().find("label[for='OtherChargebackReason']").hide()
                if (reasonKey.val() == 0) {
                    reasonKey.parent().find("label[for='ChargebackReasonKey']").show();
                    result = false;
                } else {
                    var reason = $('#div_CreateChargeback #ChargebackReasonKey').find("option:selected").text();
                    if (reason == "Others" && !otherReason.val()) {
                        otherReason.parent().find("label[for='OtherChargebackReason']").show();
                        result = false;
                    }
                }
            }
            return result;
        },
        functions: {
            SetupDialog: function () {
                var dialogDiv = $('#dialog');  
                var error = $('#dialog #Error').val();
                if (error) {
                    dialogDiv.empty();
                    Util.MessageDialog(error, "Error");
                    return;
                }
                $('#dialog #ChargebackReasonKey').on("change", function () {
                    var reason = $(this).find("option:selected").text();
                    $('#dialog #OtherChargebackReason').removeClass("disabled");
                    if (reason != "Others") {
                        $('#dialog #OtherChargebackReason').val('');
                        $('#dialog #OtherChargebackReason').addClass("disabled");
                    }
                    validateDialog('CreateChargeback')
                });
                $('#dialog #OtherChargebackReason').on("change", function () {
                    validateDialog('CreateChargeback')
                });
            },
            RaiseRequest: function () {
                $('#dialog .action-proceed').click(function () {
                    var a = $('#div_CreateChargeback form').data('validator');
                    var valid = $('#div_CreateChargeback form').valid()
                    var customValid = validateDialog('CreateChargeback', true);
                    if (valid && customValid) {
                        Util.ConfirmDialog("Are you sure you want to raise this chargeback? This can not be undone.").done(function () {
                            LG_ShowProgressMask();
                            MemberReviewService.CreateChargeback(urlOrigin, getDialogParameters())
                                .done(function (result) {
                                    if (result.IsSuccess) {
                                        CloseDialog();
                                        Util.MessageDialog('<span>' + result.Message + '</span>', 'Information');
                                    } else {
                                        Util.MessageDialog(result.Message.split('\n'), "Error");
                                    }
                                    loadGrid('Transaction');
                                    loadGrid('RaisedRequest', true);
                                    LG_HideProgressMask();

                                })
                                .fail(function (err) {
                                    Util.MessageDialog(err.statusText, "Error");
                                    LG_HideProgressMask();
                                });
                        })
                    }
                });
            }
        }
    },
    RaisedRequestDetails: {
        sourceFunction: 'RaisedRequestDetailsDialog',
        modalProps: {
            title: "Transaction Details",
            position: "center",
            width: 800,
            modal: true
        },
        initParam: function (el) {
            var requestId = $(el).attr('RaiseRequestID');
            $("#dialogRaisedRequestId").val(requestId);
            return {};
        },
        functions: {
            renderData: function () {
                var requestId = $("#dialogRaisedRequestId").val();
                var grid = $("#tbl_RaisedRequest").getGrid();
                var listCell = $("#tbl_RaisedRequest td[aria-describedby=tbl_RaisedRequestGrid_RaiseRequestID]");
                var rownum;
                listCell.each(function (i) {
                    if (requestId === $(listCell[i]).text()) {
                        rownum = i + 1;
                    }
                });
                var data = grid.getRowData(rownum);
                data.RequestAmount = data.Amount.replace('<p class="customedText text-right">', '').replace('</p>', '');
                data.FeeAmount = data.Fee;
                if (data) {
                    $('#dialog label.display-only').text("");
                    renderRaisedRequestData(data);
                }
            }
        }
    },
    CreateReverse: {
        sourceFunction: 'CreateReverseDialog',
        modalProps: {
            title: "Create Reverse",
            position: "center",
            width: "1000px",
            modal: true
        },
        initParam: function (el) {
            return { docketKey: $(el).attr('docketkey') };
        },
        validate: function (first) {
            var result = true;
            var validator = $('#div_CreateReverse form').data('validator');
            if (validator || first) {
                var reasonKey = $('#div_CreateReverse #ReverseReasonKey');
                var otherReason = $('#div_CreateReverse #OtherReverseReason');
                reasonKey.parent().find("label[for='ReverseReasonKey']").hide();
                otherReason.parent().find("label[for='OtherReverseReason']").hide()
                if (reasonKey.val() == 0) {
                    reasonKey.parent().find("label[for='ReverseReasonKey']").show();
                    result = false;
                } else {
                    var reason = $('#div_CreateReverse #ReverseReasonKey').find("option:selected").text();
                    if (reason == "Others" && !otherReason.val()) {
                        otherReason.parent().find("label[for='OtherReverseReason']").show();
                        result = false;
                    }
                }
            }
            return result;
        },
        functions: {
            SetupDialog: function () {
                var dialogDiv = $('#dialog');
                var error = $('#dialog #Error').val();
                if (error) {
                    dialogDiv.empty();
                    Util.MessageDialog(error, "Error");
                    return;
                }
                $('#dialog #ReverseReasonKey').on("change", function () {
                    var reason = $(this).find("option:selected").text();
                    $('#dialog #OtherReverseReason').removeClass("disabled");
                    if (reason != "Others") {
                        $('#dialog #OtherReverseReason').val('');
                        $('#dialog #OtherReverseReason').addClass("disabled");
                    }
                    validateDialog('CreateReverse')
                });
                $('#dialog #OtherReverseReason').on("change", function () {
                    validateDialog('CreateReverse')
                });
            },
            Reverse: function () {
                $('#dialog .action-proceed').click(function () {
                    var a = $('#dialog form').data('validator');
                    var valid = $('#dialog form').valid()
                    var customValid = validateDialog('CreateReverse', true);
                    if (valid && customValid) {
                        Util.ConfirmDialog("Are you sure you want to reverse this transaction? This can not be undone.<br>To complete the process, You’d need to submit the reversal request to the bank.").done(function () {
                            LG_ShowProgressMask();
                            MemberReviewService.CreateReverse(urlOrigin, getDialogParameters())
                                .done(function (result) {
                                    if (result.IsSuccess) {
                                        CloseDialog();
                                        Util.MessageDialog('<span>' + result.Message + '</span>', 'Information');
                                        loadGrid('RaisedRequest', true);
                                        loadGrid('Transaction', true);
                                    } else {
                                        Util.MessageDialog(result.Message.split('\n'), "Error");
                                    }
                                    loadGrid('QuarantinedTransaction');
                                    loadGrid('PendingTransaction', true);
                                    LG_HideProgressMask();

                                })
                                .fail(function (err) {
                                    Util.MessageDialog(err.statusText, "Error");
                                    LG_HideProgressMask();
                                });
                        })
                    }
                });
            }
        }
    }
}

function numericTextFormat(cellValue, option, rowObject) {
    return CustomCellText(cellValue, ['text-right']);
}

function CustomCellText(text, options = null) {
    if (!text) {
        text = '';
    }
    var tagClass = "customedText";
    if (options != null) {
        for (var opt of options) {
            tagClass = tagClass + " " + opt;
        }
    }
    return '<p class="' + tagClass + '">' + text + ' ' + '</p>';
}