﻿(function (MemberReviewService, $, undefined) {
    MemberReviewService.GetMemberByMemberKey = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberByMemberKey?',
            type: "GET",
            data: searchParams,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetMemberNotesByMemberKey = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberNotesByMemberKey?',
            type: "GET",
            data: params,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.AddMemberNoteDialog = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/AddMemberNote/' + params.memberKey,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.AddMemberNote = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/AddMemberNote',
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.FilterMemberTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/FilterMemberTransaction',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetMemberOverviewByMemberKey = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberOverviewByMemberKey',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetPaymentHistory = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetPaymentHistory',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetPaymentDetails = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetPaymentDetails',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetPendingTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberPendingTransaction',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetQuarantinedTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberQuarantinedTransaction',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.QuarantineTransaction = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/QuarantineTransaction',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.CreateChargebackDialog = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/CreateChargeback',
            type: "GET",
            data: params,
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.CreateChargeback = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/CreateChargeback',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetChargebackData = function (baseURL, params) {
        var data;
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetChargebackData',
            type: "GET",
            data: params,
            dataType: 'json',
            async:false,
            success: function (result) {
                data = result;
            },
            error: function (err) {
                data = err;
            }
        });
        return data;
    }

    MemberReviewService.GetRaisedRequest = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetMemberRaisedRequest',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.GetRaisedRequestType = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/GetRaisedRequestType',
            data: params,
            type: "GET",
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise();
    }

    MemberReviewService.RaisedRequestDetailsDialog = function (baseURL, params) {
        var deferred = $.Deferred();
        var html = $("#TransactionDetailDialog").html();
        deferred.resolve(html);
        return deferred.promise();
    }

    MemberReviewService.CreateReverseDialog = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/CreateReverse',
            type: "GET",
            data: params,
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    MemberReviewService.CreateReverse = function (baseURL, params) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'FraudMonitoring/CreateReverse',
            type: "POST",
            data: JSON.stringify(params),
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    return {
        GetMemberByMemberKey: MemberReviewService.GetMemberByMemberKey,
        GetMemberNotesByMemberKey: MemberReviewService.GetMemberNotesByMemberKey,
        AddMemberNoteDialog: MemberReviewService.AddMemberNoteDialog,
        AddMemberNote: MemberReviewService.AddMemberNote,
        FilterMemberTransaction: MemberReviewService.FilterMemberTransaction,
        MemberReviewService: MemberReviewService.GetMemberOverviewByMemberKey,
        GetPaymentHistory: MemberReviewService.GetPaymentHistory,
        GetPaymentDetails: MemberReviewService.GetPaymentDetails,
        GetPendingTransaction: MemberReviewService.GetPendingTransaction,
        GetQuarantinedTransaction: MemberReviewService.GetQuarantinedTransaction,
        QuarantineTransaction: MemberReviewService.QuarantineTransaction,
        CreateChargebackDialog: MemberReviewService.CreateChargebackDialog,
        CreateChargeback: MemberReviewService.CreateChargeback,
        GetRaisedRequest: MemberReviewService.GetRaisedRequest
    };

}(window.MemberReviewService = window.MemberReviewService || {}, jQuery));