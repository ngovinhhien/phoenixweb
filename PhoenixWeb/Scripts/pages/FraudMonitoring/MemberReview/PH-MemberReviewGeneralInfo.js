﻿var urlOrigin = Util.PH_UpdateURL();

$(document).ready(function () {
    $(".active").removeClass("active open");
    $('ul.page-sidebar-menu>li.FraudMonitoring').addClass("active open");
    loadMember();
});

function loadMember() {
    LG_ShowProgressMask();
    var searchParams = { memberKey: getMemberkey() };
    MemberReviewService.GetMemberByMemberKey(urlOrigin, searchParams)
        .done(function (result) {
            if (result.IsSuccess && result.Data) {
                $("h3.page-title").text('MID ' + result.Data.member.MemberId + ' - ' + result.Data.member.TradingName);
                loadDataToSection("#MerchantInfo_div", result.Data.member);
                renderAssociatedTable(result.Data.mstAssociatedMIDs, calculateHeight('#AssociatedMID_div'));
                $('.tab-selector').first().click();
            } else {
                $('#tabsArea').hide();
                Util.MessageDialog(result.Message.split('\n'), "Error");
                LG_HideProgressMask();
            }
        })
        .fail(function (err) {
            Util.MessageDialog("Error.", "Error");
            LG_HideProgressMask();
        });
}


function calculateHeight(div) {
    var neighbors = $(div).closest('.row').find('.form-body').not('#AssociatedMID_div');
    var maxHeight = 0;
    neighbors.each(function (i) {
        maxHeight = $(neighbors[i]).height() > maxHeight ? $(neighbors[i]).height() : maxHeight;
    })
    $(div).height(maxHeight);
    return maxHeight;
}

function renderAssociatedTable(data, parentHeight) {
    var tableDiv = $('#Associated_table');
    var otherChildren = tableDiv.closest('.form-body').children().not(tableDiv.parent());
    var tableDivHeight = parentHeight;
    otherChildren.each(function (i) {
        tableDivHeight -= $(otherChildren[i]).outerHeight();
    })

    if (data.length > 0) {
        Util.InitJqGrid({
            gridId: 'Associated_table',
            colNames: ["MID", "Reasons"],
            colModel: [
                {
                    key: false, name: "AssociateMemberId", index: "AssociateMemberId", width: "60", align: 'left', editable: false, sortable: false,
                    formatter: function (cellValue, option, rowObject) {
                        return '<a class="underlined clickable" target="_blank" href="MemberReview?memberKey=' + rowObject.AssociateMemberKey + '">' + cellValue + '</a>';
                    }
                },
                { key: false, name: "AssociateReason", index: "AssociateReason", align: 'left', editable: false, sortable: false }
            ],
            rowList: [],
            rowNum: 10000,
            pgbuttons: false,
            pgtext: null,
            viewrecords: false,
            height: tableDivHeight - 35,
            enableAutoWidth: true,
            enableShrinkToFit: true
        }, null, null);
        var gridData = {
            rows: data,

        }
        var grid = $('#Associated_table').getGrid()[0];
        $('#Associated_table').getGrid().jqGrid('clearGridData');
        grid.addJSONData(gridData);
    } else {
        $('#Associated_table').text('No records to display.')
    }

}
