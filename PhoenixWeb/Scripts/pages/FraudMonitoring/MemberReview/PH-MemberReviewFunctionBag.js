﻿function getMemberkey() {
    return $("#MemberKey").val();
}

function customGrid(tableName, loadGrid) {
    var gridId = 'tbl_' + tableName + 'Grid';
    var pagerId = 'tbl_' + tableName + 'GridPager';
    var table = listTable[tableName];
    if (table) {
        var frozen = false;
        if (table.props.colModel[0].frozen) {
            frozen = true;
        }
        $("#" + gridId).setGridParam({
            onPaging: function (pgButton) {
                var currentPage = $("#" + gridId).getGridParam('page');
                var rowNum = $("#" + gridId).getGridParam('rowNum');
                if (pgButton == "next_" + pagerId) {
                    currentPage = currentPage + 1;
                }
                if (pgButton == "prev_" + pagerId) {
                    if (currentPage == 1) {
                        return;
                    }
                    currentPage = currentPage - 1;
                }
                if (pgButton == "first_" + pagerId) {
                    if (currentPage == 1) {
                        return;
                    }
                    currentPage = 1;
                }
                if (pgButton == "last_" + pagerId) {
                    if ($("#" + gridId).getGridParam('lastpage') == 0) {
                        return;
                    }
                    if (currentPage == $("#" + gridId).getGridParam('lastpage')) {
                        return;
                    }
                    currentPage = $("#" + gridId).getGridParam('lastpage');
                }
                var p = $(this).jqGrid("getGridParam");
                var elTotalPage = $(p.pager).find("span[id*='" + pagerId + "']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;
                if (pgButton == "records") {
                    var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                    rowNum = newRowNum;
                    currentPage = 1;
                }
                if (pgButton == "user") {
                    var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                    if (newPage <= totalPage && newPage > 0) {
                        currentPage = newPage;
                    }
                }
                if (currentPage > totalPage) {
                    return;
                }

                if (loadGrid) {
                    if (listTable[tableName].searchObject) {
                        listTable[tableName].searchObject.PageIndex = currentPage;
                        listTable[tableName].searchObject.PageSize = rowNum;
                    }
                    loadGrid(tableName);
                }
            },
            onSortCol: function (index, columnIndex, sortOrder) {
                if ($("#" + gridId).getGridParam('lastpage') == 0) {
                    return;
                }

                if (loadGrid) {
                    if (listTable[tableName].searchObject) {
                        var colModel = $("#" + gridId).jqGrid('getGridParam', 'colModel');
                        listTable[tableName].searchObject.SortField = colModel[columnIndex].index;
                        listTable[tableName].searchObject.SortOrder = sortOrder;
                        listTable[tableName].searchObject.PageIndex = 1;
                        listTable[tableName].searchObject.PageSize = $("#" + gridId).getGridParam('rowNum');
                    }
                    loadGrid(tableName);
                }
                return 'stop';
            }
        });

        for (var col of table.props.colModel) {
            // align header
            var tableId = 'tbl_' + tableName;
            if (col.align) {
                $("#" + gridId + "_" + col.name).addClass(col.align);
                $('#' + tableId + " .frozen-div.ui-jqgrid-hdiv " + "#" + gridId + "_" + col.name).addClass(col.align);
            }
            // remove sortable on unsortable header
            if (col.sortable == false) {
                var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
                $(cellSelector).find('span.s-ico').remove();
            }
            if (frozen) {
                var cellSelector = '#' + tableId + ' .frozen-div.ui-jqgrid-hdiv th#' + gridId + '_' + col.name;
                $(cellSelector).find('span.s-ico').remove();
            }
        }
    }
}

var loadGrid = function (tableName, reset = false) {
    var table = listTable[tableName];
    if (table && !table.loading) {
        var frozen = false;
        if (table.props.colModel[0].frozen) {
            frozen = true;
        }
        if (!table.searchObject) {
            table.searchObject = $.extend(true, {}, table.searchObjectDefault);
        }
        if (reset) {
            table.searchObject.SortField = table.searchObjectDefault.SortField;
            table.searchObject.SortOrder = table.searchObjectDefault.SortOrder;
            table.searchObject.PageIndex = 1;
            table.searchObject.PageSize = $('#tbl_' + tableName).getGrid().getGridParam('rowNum');
            Util.resetGridSortIcon('#tbl_' + tableName);
        }
        removeGridMessage(tableName);
        disableGridLoad(tableName, "search-btn");
        $('#tbl_' + tableName).getGrid().jqGrid('clearGridData');
        var params = table.searchObject ? $.extend(true, {}, table.searchObject) : {};
        Util.addParams(params, table.getFilterParams ? table.getFilterParams() : {});
        params.memberKey = getMemberkey();
        if (!table.validate || (table.validate && table.validate(params))) {
            var serviceFunction = MemberReviewService[table.sourceFunction];
            if (serviceFunction) {
                table.loading = true;
                serviceFunction(urlOrigin, params)
                    .done(function (result) {
                        if (result.IsSuccess) {
                            listTable[tableName].loaded = true;
                            if (result.Data) {
                                var grid = $('#tbl_' + tableName).getGrid()[0];
                                var data = result.Data;
                                grid.addJSONData(data);
                                if (frozen) {
                                    $(grid).setGridWidth($("#wrapper").width());
                                    $(grid).resizeFrozenRows();
                                }
                                if (listTable[tableName].aggregateFunction) {
                                    listTable[tableName].aggregateFunction(result.Data)
                                }
                            } else {
                                showGridMessage(tableName, result.Message.split('\n'), "Information");
                            }
                        }
                        else {
                            showGridMessage(tableName, result.Message.split('\n'), "Error");
                        }
                        disableGridLoad(tableName, "search-btn", false);
                        table.loading = false;
                    })
                    .fail(function (err) {
                        disableGridLoad(tableName, "search-btn", false);
                        table.loading = false;
                        showGridMessage(tableName, err.statusText);
                    });
            } else {
                disableGridLoad(tableName, "search-btn", false);
                LG_HideProgressMask();
            }
        } else {
            disableGridLoad(tableName, "search-btn", false);
        }
    }
}

function addCommonProps(tableName) {
    var table = listTable[tableName];
    if (table) {
        var props = $.extend(true, {}, table.props);
        for (var prop in COMMON_TABLE_PROPS) {
            if (props[prop] == undefined) {
                props[prop] = COMMON_TABLE_PROPS[prop];
            }
        }
        return props;
    }
}

function getDialogParameters() {
    var parameters = Util.getFilterParams('#dialog');
    parameters.memberKey = getMemberkey();
    return parameters;
}

function getComponentInTabs() {
    var tables = {};
    var filters = {};
    for (var tabId in listTab) {
        for (var item in listTab[tabId]) {
            var prop = listTab[tabId][item];
            if (prop) {
                switch (prop.type) {
                    case "table":
                        tables[item] = listTab[tabId][item];
                        break;
                    case "filter":
                        filters[item] = listTab[tabId][item];
                        break;
                }
            }
        }
    }
    listFilter = filters
    listTable = tables;
}

function reloadTableByTab(tabName, forced = false) {
    var arrTableNames = []
    var fullTabName = 'tab_' + tabName;
    if (listTab[fullTabName]) {
        for (var tableName in listTab[fullTabName]) {
            arrTableNames.push(tableName);
        }
    }
    for (var tableName of arrTableNames) {
        var table = listTable[tableName];
        if (table && !table.loading && (!table.loaded || forced)) {
            $("#" + getTableDiv(tableName)).getGrid().resizeFrozenRows();
            if (listTable[tableName].sourceFunction) {
                loadGrid(tableName);

            }
        }
    }
    var customFunction = listTab[fullTabName].CustomFunction;
    if (customFunction) {
        customFunction({ forceLoad: forced});
    }
}

function getTableDiv(tableName) {
    return "tbl_" + tableName;
}

$('.tab-selector').click(function (e) {
    $('.tabcontent').hide();
    $('.tab-selector').removeClass('tab-active');
    $(this).addClass('tab-active')
    var tabName = $(this).attr('tabId');
    $('.tabcontent#' + 'tab_' + tabName).show();
    $(window).trigger('resize');
    reloadTableByTab(tabName);
});


function CloseDialog() {
    $("#dialog").dialog("close");
    $("#dialog").empty();
}

function loadDataToSection(sectionSelector, data) {
    for (var key in data) {
        var el = $(sectionSelector + " #" + key);
        if (el[0] != null) {
            if (el.hasClass("display-only")) {
                if (el.attr('type') != "checkbox") {
                    var parent = el.parent();
                    parent.append('<label id="' + key + '"></label>');
                    parent.children('label#' + key).attr("class", el.attr("class"));
                    el.remove();
                    el = parent.children('label#' + key);
                }
            }
            el.closest("div.form-group").removeClass("hidden");
            el.attr("data-original", data[key]);
            var value;
            if (el.hasClass("numeric-display")) {
                value = data[key] ? data[key] : "0";
            } else if (el.hasClass("money-display")) {
                value = data[key] ? data[key] : "$0.00";
            } else {
                value = data[key] ? data[key] : "-";
            }
            switch (el[0].nodeName) {
                case "INPUT":
                    if (el.attr('type') == "checkbox") {
                        el.prop("checked", data[key]);
                        $(el).closest("span").addClass(data[key] ? "checked" : "");
                    } else if (el.hasClass("datepicker")) {
                        el.val(data[key]);
                    } else {
                        el.val(value);
                    }
                    break;
                case "SPAN":
                    el.text(value);
                    break;
                case "LABEL":
                    el.text(value);
                    break;
                case "SELECT":
                    el.val(data[key]);
            }
        }
    }
}



function initTabComponents() {
    getComponentInTabs();
    for (var filterName in listFilter) {
        var initFunc = listFilter[filterName].init;
        if (initFunc) {
            initFunc();
        }
    }
    for (var tableName in listTable) {
        Util.InitJqGrid(addCommonProps(tableName), null, null);
        customGrid(tableName, loadGrid);
    }
}

function initDialog(el) {
    var dialogName = $(el).attr("dialog-name");
    var dialog = listDialog[dialogName];
    if (dialog) {
        LG_ShowProgressMask();
        var serviceFunction = MemberReviewService[dialog.sourceFunction];
        if (serviceFunction) {
            var params = dialog.initParam ? dialog.initParam(el) : {};
            params.memberKey = getMemberkey();
            serviceFunction(urlOrigin, params)
                .done(function (result) {
                    var dialogDiv = $("#dialog")
                    dialogDiv.empty();
                    dialogDiv.append(result);
                    var error = $('#dialog #Error').val();
                    if (error) {
                        dialogDiv.empty();
                        Util.MessageDialog(error, "Error");
                    } else {
                        if (dialog.functions) {
                            for (var func in dialog.functions) {
                                dialog.functions[func](params);
                            }
                        }
                        dialogDiv.dialog(dialog.modalProps);
                    }
                    LG_HideProgressMask();
                })
                .fail(function (err) {
                    LG_HideProgressMask();
                });
        }
    }
}

function showGridMessage(tableName, messages) {
    var html = '<div class="gridMessage">#content#</div>';
    var content = '';
    var arrMessage = Array.isArray(messages) ? messages : [messages];
    for (var mess of arrMessage) {
        mess = mess.substring(mess.length - 1) == '.' ? mess : mess + '.';
        content += '<p>' + mess + '</p>';
    }
    html = html.replace('#content#', content);
    $('#tbl_' + tableName + ' .ui-jqgrid-bdiv').first().append(html);
}
function removeGridMessage(tableName) {
    $('#tbl_' + tableName + ' .ui-jqgrid-bdiv .gridMessage').remove()
}


function quarantineTxn(el) {
    var docketKey = $(el).attr('docketkey');
    var action = $(el).attr('action-type');
    var message = action == "release" ? "Do you want to release this transaction?" : "Do you want to quarantine this transaction?";
    Util.ConfirmDialog(message).done(function () {
        LG_ShowProgressMask();
        MemberReviewService.QuarantineTransaction(urlOrigin, { docketKey: docketKey, action: action })
            .done(function (result) {
                if (!result.IsSuccess) {
                    Util.MessageDialog(result.Message.split('\n'), "Error");
                }
                loadGrid("Transaction");
                loadGrid("QuarantinedTransaction");
                loadGrid("PendingTransaction");
                LG_HideProgressMask();
            })
            .fail(function (err) {
                Util.MessageDialog(err.statusText, "Error");
                LG_HideProgressMask();
            });
    });
}

function validateDialog(dialogName, first) {
    var dialog = listDialog[dialogName];
    if (dialog && dialog.validate) {
        return dialog.validate(first);
    } else {
        return true;
    }
}

function loadRaisedRequest() {
    loadGrid("RaisedRequest");
}

function doRaisedRequestAction(el) {
    proceedAction(el, loadRaisedRequest);
}

function disableElement(area, className, isDisable = true) {
    if (!Array.isArray(className)) {
        className = [className];
    }
    for (var item of className) {
        if (isDisable) {
            $(area).find("." + item).addClass("disabled-item");
        } else {
            $(area).find("." + item).removeClass("disabled-item");
        }
    }
}


function disableGridLoad(tableName, className, isDisable = true) {
    if (isDisable) {
        disableElement($('#tbl_' + tableName).closest(".tabcontent"), "search-btn");
        Util.BlockjqGrid($('#tbl_' + tableName).getGrid(), true);
        return;
    }
    Util.UnBlockjqGrid($('#tbl_' + tableName).getGrid());
    disableElement($('#tbl_' + tableName).closest(".tabcontent"), className, false);

}