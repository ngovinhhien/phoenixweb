﻿
//Get URL Origin
var urlOrigin = Util.PH_UpdateURL();
var dataRowUnReconciledBatches = null;
var statusSuccess;
var statusFail;
$(function () {

    initGridUnReconciledBatches();
    initGridUnReconciledTransactions();

    $('#btnSearch').click(function () {

        if (!$('#frmSearchOpenBatches').valid()) {
            return;
        }

        var searchParams = {
            PageIndex: 1,
            PageSize: $('[id^=grdUnReconciledBatchesPager] select.ui-pg-selbox').val(),
            Request: {
                SerialNumber: $('#SerialNumber').val(),
                TerminalID: $('#TerminalID').val()
            }
        };
        loadGridUnReconciledBatchesData(searchParams);
        gridUnReconciledBatchesEvents(searchParams);
    });



    $(window).bind("resize", function () {
        $('#grdUnReconciledBatches').setGridWidth($('#parentUnReconciledBatchesGrid').width());
        $('#grdUnReconciledTransactions').setGridWidth($('#parentUnReconciledTransactionsGrid').width());
    }).trigger("resize");

});

function initGridUnReconciledBatches() {

    $('#grdUnReconciledBatches').jqGrid({
        url: "",
        colNames: [
            "", "Terminal ID", "Serial Number", "Batch Number", "Transaction Count", "Batch Total", "Batch Start", "Batch End", "Batch Closing Attempts", "DebitNumber",
            "DebitAmount", "CreditAmount", "CreditNumber", "DebitReversalNumber", "DebitReversalAmount", "CreditReversalNumber", "CreditReversalAmount", "SettlementNetAmount", "MerchantID"
        ],
        colModel: [

            { key: false, name: "", index: "", editable: false, sortable: false, width: 80, formatter: addOpenBatches },
            { key: false, name: "TerminalID", index: "TerminalID", editable: false, sortable: false, width: 80 },
            { key: false, name: "SerialNumber", index: "SerialNumber", editable: false, sortable: false, width: 80 },
            { key: false, name: "BatchNumber", index: "BatchNumber", editable: false, sortable: false, width: 80 },
            { key: false, name: "TransactionCount", index: "TransactionCount", editable: false, sortable: false, width: 80 },
            { key: false, name: "BatchTotal", index: "BatchTotal", editable: false, sortable: false, width: 80 },
            { key: false, name: "SOSAttempt", index: "SOSAttempt", editable: false, sortable: false, width: 150, formatter: formatDate },
            { key: false, name: "FirstEOSAttempt", index: "FirstEOSAttempt", editable: false, sortable: false, width: 150, formatter: formatDate },
            { key: false, name: "EOSAttempt", index: "EOSAttempt", editable: false, sortable: false, width: 150 },

            { key: false, name: "MerchantID", index: "MerchantID", editable: false, sortable: false, hidden: true },
            { key: false, name: "DebitNumber", index: "DebitNumber", editable: false, sortable: false, hidden: true },
            { key: false, name: "DebitAmount", index: "DebitAmount", editable: false, sortable: false, hidden: true },
            { key: false, name: "CreditAmount", index: "CreditAmount", editable: false, sortable: false, hidden: true },
            { key: false, name: "CreditNumber", index: "CreditNumber", editable: false, sortable: false, hidden: true },
            { key: false, name: "DebitReversalNumber", index: "DebitReversalNumber", editable: false, sortable: false, hidden: true },
            { key: false, name: "DebitReversalAmount", index: "DebitReversalAmount", editable: false, sortable: false, hidden: true },
            { key: false, name: "CreditReversalNumber", index: "CreditReversalNumber", editable: false, sortable: false, hidden: true },
            { key: false, name: "CreditReversalAmount", index: "CreditReversalAmount", editable: false, sortable: false, hidden: true },
            { key: false, name: "SettlementNetAmount", index: "SettlementNetAmount", editable: false, sortable: false, hidden: true },

        ],
        pager: '#grdUnReconciledBatchesPager',
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });



    function formatDate(cellValue, option, rowObject) {
        try {
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
    function addOpenBatches(cellValue, option, rowObject) {
        var tag = $('<button class="btn btn-default">Close Batch</button>');
        tag.attr({
            'data-message': rowObject.Message,
            class: 'view-detail-log',
            'onclick': 'confirmOpenBatches(this)',
            'rowid': option.rowId
        });
        return tag[0].outerHTML;
    }
}

function initGridUnReconciledTransactions() {

    $('#grdUnReconciledTransactions').jqGrid({
        url: "",
        colNames: [
            "Total Sale Amount", "Reponse Code", "Start Date Time", "Transaction Number", "Card Name", "Card Masked", "Tran Type", "Processing Type Name"
        ],
        colModel: [

            { key: false, name: "TotalSaleAmount", index: "TotalSaleAmount", editable: false, sortable: false, width: 80 },
            { key: false, name: "ResponseCode", index: "ResponseCode", editable: false, sortable: false, width: 80 },
            { key: false, name: "StartDateTime", index: "StartDateTime", editable: false, sortable: false, width: 150, formatter: formatDate },
            { key: false, name: "TransactionNumber", index: "TransactionNumber", editable: false, sortable: false, width: 80 },
            { key: false, name: "CardName", index: "CardName", editable: false, sortable: false, width: 80 },
            { key: false, name: "CardMasked", index: "CardMasked", editable: false, sortable: false, width: 80 },
            { key: false, name: "TranType", index: "TranType", editable: false, sortable: false, width: 80 },
            { key: false, name: "ProcessingTypeName", index: "ProcessingTypeName", editable: false, sortable: false, width: 80 },
        ],
        pager: '#grdUnReconciledTransactionsPager',
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });



    function formatDate(cellValue, option, rowObject) {
        try {
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
}

function gridUnReconciledBatchesEvents(searchParams) {

    $('#grdUnReconciledBatches').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdUnReconciledBatches').getGridParam('page');
            var rowNum = $('#grdUnReconciledBatches').getGridParam('rowNum');

            if (pgButton == "next_grdUnReconciledBatchesPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdUnReconciledBatchesPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdUnReconciledBatchesPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdUnReconciledBatchesPager") {
                currentPage = $('#grdUnReconciledBatches').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdUnReconciledBatchesPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page.

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridUnReconciledBatchesData(searchParams);
        }
    });


}

function gridUnReconciledTransactionsEvents(searchParams) {

    $('#grdUnReconciledTransactions').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdUnReconciledTransactions').getGridParam('page');
            var rowNum = $('#grdUnReconciledTransactions').getGridParam('rowNum');

            if (pgButton == "next_grdUnReconciledTransactionsPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdUnReconciledTransactionsPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdUnReconciledTransactionsPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdUnReconciledTransactionsPager") {
                currentPage = $('#grdUnReconciledTransactions').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdUnReconciledTransactionsPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page.

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridUnReconciledTransactionsData(searchParams);
        }
    });


}


function loadGridUnReconciledBatchesData(searchParams) {

    LG_ShowProgressMask();

    $.ajax({
        url: urlOrigin + "TransactionHost/SearchUnReconciledBatches",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        //Loading data to grid
        var gridUnReconciledBatches = $('#grdUnReconciledBatches')[0];
        $('#grdUnReconciledBatches').jqGrid('clearGridData');
        $('#grdUnReconciledTransactions').jqGrid('clearGridData'); //clear data on UnReconciledTransactions grid after load data to UnReconciledBatches grid
        gridUnReconciledBatches.addJSONData(result);

        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function loadGridUnReconciledTransactionsData(searchParams) {

    LG_ShowProgressMask();

    $.ajax({
        url: urlOrigin + "TransactionHost/SearchUnReconciledTransactions",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        //Loading data to grid
        var gridUnReconciledTransactions = $('#grdUnReconciledTransactions')[0];
        $('#grdUnReconciledTransactions').jqGrid('clearGridData');

        gridUnReconciledTransactions.addJSONData(result);

        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}

function confirmOpenBatches(selector) {
    var self = $(selector);
    var popupConfirmOpenBatches = $('#popupConfirmOpenBatches').dialog();
    var trSelected = self.closest("tr");
    var rowId = trSelected[0].id;

    popupConfirmOpenBatches.dialog('option', { 'max-width': '20%', 'resizable': false, title: 'Confirmation' });
    dataRowUnReconciledBatches = $('#grdUnReconciledBatches').jqGrid('getRowData', rowId);;
}

$(document).on("click", "#popupConfirmOpenBatches #Confirm", function () {
    LG_ShowProgressMask();
    $('#popupConfirmOpenBatches').dialog("close");
    var popUpInformation = $("#popupInformation");
    var searchParams = {
        PageIndex: 1,
        PageSize: $('[id^=grdUnReconciledBatchesPager] select.ui-pg-selbox').val(),
        Request: {
            SerialNumber: $('#SerialNumber').val(),
            TerminalID: $('#TerminalID').val()
        }
    }

    $.ajax({
        url: urlOrigin + "TransactionHost/ExecuteOpenBatches",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(dataRowUnReconciledBatches)
    }).done(function (result) {
        //Loading data to grid
        if (result.Status == statusSuccess) {
            loadGridUnReconciledBatchesData(searchParams);
        }

        popUpInformation.find("#Content").text(result.Message);
        popUpInformation.dialog();
        popUpInformation.dialog('option', { 'max-width': '20%', 'resizable': false, title: 'Information' });

        LG_HideProgressMask();
    }).fail(function (err) {
        popUpInformation.find("#Content").text("Something error, please contact with admin");
        popUpInformation.dialog();
        popUpInformation.dialog('option', { 'max-width': '20%', 'resizable': false, title: 'Information' });

        console.log(err);
        LG_HideProgressMask();
    });
})

$(document).on("dblclick", "#grdUnReconciledBatches tr", function () {
    var $thisTr = $(this);
    var rowId = $thisTr[0].id;
    var dataRow = $('#grdUnReconciledBatches').jqGrid('getRowData', rowId);

    var searchParams = {
        PageIndex: 1,
        PageSize: $('[id^=grdUnReconciledTransactionsPager] select.ui-pg-selbox').val(),
        Request: {
            TerminalID: dataRow.TerminalID,
            BatchNumber: dataRow.BatchNumber
        }
    };
    loadGridUnReconciledTransactionsData(searchParams);
    gridUnReconciledTransactionsEvents(searchParams);
});
