﻿(function (glideboxService, $, undefined) {
    glideboxService.GetGlideboxHistories = function (baseURL, endpoint, searchGlideboxHistory) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + endpoint,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchGlideboxHistory),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

}(window.glideboxService = window.glideboxService || {}, jQuery));