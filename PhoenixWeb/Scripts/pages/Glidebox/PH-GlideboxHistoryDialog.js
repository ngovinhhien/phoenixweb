﻿var searchGlideboxHistory = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;
var endpoint = "";

$(function () {
    $(window).bind("resize", function () {
        $("#gridGlideboxHistory").setGridWidth($("#parentGlideboxHistoryGrid").width());
    }).trigger("resize");

    $("#gridGlideboxHistory").jqGrid({
        url: "",
        colNames: [
            "Date", "Activity Log", "Modified By"
        ],
        colModel: [
            {
                name: "ChangedDatetime",
                index: "ChangedDatetime",
                editable: false,
                sortable: true,
                align: 'left',
                resizable: false,
                fixed: true
            },
            {
                name: "ActionLog",
                index: "ActionLog",
                editable: false,
                sortable: false,
                align: 'left',
                resizable: false
            },
            {
                name: "ChangedBy",
                index: "ChangedBy",
                editable: false,
                sortable: false,
                align: 'left',
                resizable: false,
                fixed: true
            }
        ],
        pager: "#gridGlideboxHistoryPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        width: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#gridGlideboxHistory').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#gridGlideboxHistory').getGridParam('page');
            var rowNum = $('#gridGlideboxHistory').getGridParam('rowNum');

            if (pgButton == "next_gridGlideboxHistoryPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_gridGlideboxHistoryPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_gridGlideboxHistoryPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_gridGlideboxHistoryPager") {
                if ($('#gridGlideboxHistory').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#gridGlideboxHistory').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#gridGlideboxHistory').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='gridGlideboxHistoryPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            if (!searchGlideboxHistory) {
                searchGlideboxHistory = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                };
            } else {
                searchGlideboxHistory.CurrentPage = currentPage;
                searchGlideboxHistory.PageSize = rowNum;
            }
            
            LoadGlideboxHistoryGrid();
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchGlideboxHistory) {
                searchGlideboxHistory.SortName = sortName;
                searchGlideboxHistory.SortOrder = sortOrder;
            }
            LoadGlideboxHistoryGrid();
            return 'stop';
        }
    });
});

GetGlideboxHistory = function (id, name, dataType) {
    Util.CloseDialog('#productDetailDialog');

    searchGlideboxHistory = {
        CurrentPage: 1,
        PageSize: 10,
        ID: id
    };

    if (dataType == "Product") {
        endpoint = "Product/SearchProductHistory";
    }
    else if (dataType == "Category") {
        endpoint = "Category/SearchCategoryHistory";
    }

    //Show progress mask
    LG_ShowProgressMask();

    LoadGlideboxHistoryGrid().done(function () {
        $('#glideboxHistoryDialog').dialog({
            resizable: false,
            height: "auto",
            width: "50%",
            modal: true,
            title: name + " History",
            close: function () {
                $(this).dialog("close");
                $('#textAdvanceSearchHistory').val("");
                if (dataType == "Product") {
                    loadManageProductData();
                }
                else if (dataType == "Category") {
                    loadManageCategoryData();
                }
            }
        });
        $("#gridGlideboxHistory").setGridWidth($("#parentGlideboxHistoryGrid").width());

        $("#gview_gridGlideboxHistory .ui-jqgrid-bdiv").css("max-height", "400px");
        $("#gview_gridGlideboxHistory .ui-jqgrid-bdiv").css("overflow-y", "auto");

    });
}

LoadGlideboxHistoryGrid = function () {
    var deferred = $.Deferred();

    LG_ShowProgressMask();
    glideboxService.GetGlideboxHistories(baseURL, endpoint, searchGlideboxHistory).done(function (result) {

        //Loading data to grid
        var grid = $("#gridGlideboxHistory")[0];
        $("#gridGlideboxHistory").jqGrid('clearGridData');
        grid.addJSONData(result);

        //the notice flag is using advance search
        isFiltering = false;

        var dataGridCount = $('#gridGlideboxHistory').jqGrid('getGridParam', 'reccount');
        if (dataGridCount > 0) {
            $("#filterSectionHistory").show();
        } else {
            $("#filterSectionHistory").hide();
        }

        LG_HideProgressMask();
        deferred.resolve(result);
    });
    return deferred.promise();
}

searchHistory = function () {
    var searchText = $('#textAdvanceSearchHistory').val();

    searchGlideboxHistory.SearchText = searchText;
    //check in the first time use filter search then reset CurrentPage value
    if (!isFiltering) {
        searchGlideboxHistory.CurrentPage = 1;
    }

    LG_ShowProgressMask();

    glideboxService.GetGlideboxHistories(baseURL, endpoint, searchGlideboxHistory)
        .done(function (result) {
            var grid = $('#gridGlideboxHistory')[0];
            $('#gridGlideboxHistory').jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = true;
            LG_HideProgressMask();
        });
}

$('#btnSearchGlideboxHistory').on('click', function () {
    searchHistory();
})

$('#textAdvanceSearchHistory').on('keyup', function (event) {
    if (event.which === 13) {
        searchHistory();
    }
})
