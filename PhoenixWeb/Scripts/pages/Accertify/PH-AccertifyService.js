﻿(function (AccertifyService, $, undefined) {
    AccertifyService.FilterAccertifyTransaction = function (baseURL, searchParams) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + 'Accertify/FilterAccertifyTransaction?',
            type: "GET",
            data: searchParams,
            headers: {
                "Content-Type": "application/json"
            },
            success: function (result) {
                deferred.resolve(result);
            },
            error: function (err) {
                deferred.reject(err)
            }
        });
        return deferred.promise();
    }

    return {
        FilterAccertifyTransaction: AccertifyService.FilterAccertifyTransaction
    };

}(window.AccertifyService = window.AccertifyService || {}, jQuery));