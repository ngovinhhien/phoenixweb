﻿var urlOrigin = Util.PH_UpdateURL();
var searchObject = {
    SortField: 'transactionDateTime',
    SortOrder: 'desc',
    PageIndex: '',
    PageSize: ''
};
var filterParams = {};

$(document).ready(function () {
    var currentYear = new Date().getFullYear();
    Util.InitDatepicker('.datepicker', {
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        yearRange: "1900:" + (currentYear)
    });

    $("#FromDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#ToDate").datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    $("#ToDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#FromDate").datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    Util.InitJqGrid({
        gridId: 'AccertifyTransaction',
        colNames: BuildColName(),
        colModel: BuildColModel(),
        rowNum: 20,
        height: 700,
        hasPager: true,
        allowSort: true,
        enableAutoWidth: true,
        enableShrinkToFit: true
    }, searchObject, loadDataTransactionGrid);

    adjustSortIconAndAlignment('AccertifyTransaction');
    $('#btnSearch').click(function (e) {
        e.preventDefault();
        filterParams = Util.getFilterParams("#param-area");
        searchObject.PageIndex = 1;
        searchObject.PageSize = $("#AccertifyTransaction").getPageSize();
        searchObject.SortField = 'transactionDateTime';
        searchObject.SortOrder = 'desc';
        Util.resetGridSortIcon('#AccertifyTransaction');
        loadDataTransactionGrid();
    });

    $('.btn-setDate').click(function (e) {
        e.preventDefault();
        var today = new Date();
        var backDate = parseInt($(this).attr('back-date'));
        $('#ToDate').datepicker("setDate", Util.dateToStr(today, '/'));
        $('#FromDate').datepicker("setDate", Util.dateToStr(new Date(today.setDate(today.getDate() - backDate)), '/'));
        ChangeSetDateBtnState();
    });

    $('#reset').click(function (e) {
        var d = new Date();
        if (d.getDay() == 1) {
            $('#btnLast3Days').click();
        } else {
            $('#btnYesterday').click();
        };
        $('#SearchText').val('');
        $('#AccertifyStatus').val('Review');
    });

    $('.filedate').on('change', function () {
        ChangeSetDateBtnState();
    });

    setTimeout(function () {
        var d = new Date();
        if (d.getDay() == 1) {
            $('#btnLast3Days').click();
        } else {
            $('#btnYesterday').click();
        }
    }, 500)

});

function ChangeSetDateBtnState() {
    var today = new Date();
    var setDateBtn = $('.btn-setDate');
    setDateBtn.removeClass('btn-setDate-active');
    if (Util.dateToStr($('#ToDate').datepicker('getDate'), '/') == Util.dateToStr(today, '/')) {
        var fromDateText = Util.dateToStr($('#FromDate').datepicker('getDate'), '/');
        setDateBtn.each(function (btn) {
            var backDate = $(setDateBtn[btn]).attr('back-date');
            var fromDate = new Date();
            if (Util.dateToStr(new Date(fromDate.setDate(fromDate.getDate() - backDate)), '/') == fromDateText) {
                $(setDateBtn[btn]).addClass('btn-setDate-active');
            }
        });
    }
}




function BuildColName() {
    var colNames = [
        'MID',
        "Trading Name",
        "TID",
        "Type",
        'Order No. <span class="glyphicon glyphicon-question-sign" style="color: #000" title="Order No is used to identify txn on Accertify Portal." data-placement="right"></span>',
        'Ref. No. <span class="glyphicon glyphicon-question-sign" style="color: #000" title="Ref No is used to identify txn on Payway." data-placement="right"></span>',
        "Txn Total",
        "Txn Date",
        "Txn Status",
        "Accertify Status",
        "Card Details",
        "MCC Category",
    ];

    return colNames;
}

function BuildColModel() {
    var colModel = [
        {
            key: false, name: "MemberID", index: "MemberID", align: 'left', editable: false, width: 80, sortable: true,
            formatter: function (cellValue, option, rowObject) {
                var imgURL = urlOrigin + "FraudMonitoring/MemberReview?memberKey=" + rowObject.MemberKey;
                return '<a class="underlined clickable" target="_blank" href="' + imgURL + '">' + cellValue + '</a>';
            }
        },
        { key: false, name: "TradingName", index: "TradingName", align: 'left', editable: false, sortable: true, formatter: tradingName, width: 160 },
        { key: false, name: "TerminalID", index: "TerminalID", align: 'left', editable: false, sortable: true, width: 80 },
        { key: false, name: "TransactionType", index: "TransactionType", align: 'left', editable: false, sortable: true, width: 100 },
        { key: false, name: "OrderNumber", index: "OrderNumber", align: 'left', editable: false, sortable: true, width: 170 },
        { key: false, name: "ReceiptNumber", index: "ReceiptNumber", align: 'left', editable: false, sortable: true, width: 90 },
        {
            key: false, name: "TotalAmount", firstsortorder: "desc", index: "TotalAmount", editable: false, sortable: true, width: 110, align: "right", formatter: transactionDetail
        },
        {
            key: false, name: "TransactionDateTime", index: "TransactionDateTime", align: 'left', editable: false, sortable: true, width: 100,
            formatter: function (cellValue, option, rowObject) {
                var html = '';
                cellValue = cellValue.replace(' ', ';')
                var arr = cellValue.split(';');
                for (var el of arr) {
                    html += CustomCellText(el);
                }
                return html;
            }
        },
        { key: false, name: "TransactionStatus", index: "TransactionStatus", align: 'left', editable: false, width: 110, sortable: true },
        { key: false, name: "AccertifyStatus", index: "AccertifyStatus", align: 'left', editable: false, width: 130, sortable: true },
        { key: false, name: "CardName", index: "CardName", align: 'left', editable: false, sortable: true, formatter: cardDetail, width: 100 },
        { key: false, name: "MCCCategory", index: "MCCCategory", align: 'left', editable: false, sortable: true, formatter: MCCCategory, width: 170 },
    ];

    return colModel;
}

function tradingName(cellValue, option, rowObject) {
    return CustomCellText(cellValue, ['text-bold']);
}

function transactionDetail(cellValue, option, rowObject) {
    return CustomCellText(('$' + (cellValue == null ? "0.00" : numberWithCommas(cellValue.toFixed(2)))).replace('$-', '-$'), ['text-right', 'text-bold']);
}

function numberWithCommas(num) {
    return String(num).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, '$1,');
}

function cardDetail(cellValue, option, rowObject) {
    var html = '';
    var cardName = rowObject.CardName.toUpperCase().replace(/ /g, '');
    var imgHtml = '';
    const cardImage = Util.CardImage();
    if (cardImage[cardName] != undefined) {
        imgHtml = '<img height="12" width="20" src="' + cardImage[cardName] + '"> ';
    }
    html = html + CustomCellText(imgHtml + rowObject.CardName);
    html = html + CustomCellText(rowObject.CardNumber, ['underlined']);
    return html;
}

function MCCCategory(cellValue, option, rowObject) {
    return (cellValue ? CustomCellText(cellValue) : '') + (rowObject.mccCategory ? CustomCellText(rowObject.mccCategory) : '');
}

function CustomCellText(text, options = null) {
    if (!text) {
        text = '';
    }
    var tagClass = "customedText";
    if (options != null) {
        for (var opt of options) {
            tagClass = tagClass + " " + opt;
        }
    }
    return '<p class="' + tagClass + '">' + text + ' ' + '</p>';
}

function loadDataTransactionGrid() {
    var searchParams = GatherParams();
    if (searchParams.SearchText && !/^[a-zA-Z0-9]+$/.test(searchParams.SearchText)) {
        Util.MessageDialog("Member ID or Terminal ID format is invalid.", "Error");
        return;
    }
    LG_ShowProgressMask();
    AccertifyService.FilterAccertifyTransaction(urlOrigin, searchParams)
        .done(function (result) {
            $('#AccertifyTransaction').getGrid().jqGrid('clearGridData');
            if (result.IsSuccess) {
                if (result.Data) {
                    var grid = $('#AccertifyTransaction').getGrid()[0];
                    grid.addJSONData(result.Data);
                    $("#AccertifyTransaction").getGrid().setGridWidth($("#wrapper").width());
                    $(window).trigger("resize");
                } else {
                    Util.MessageDialog('No result found', 'Information');
                }
            }
            else {
                Util.MessageDialog(result.Message.split('\n'), "Error");
            }
            LG_HideProgressMask();
        })
        .fail(function (err) {
            Util.MessageDialog("Error", "Error");
            LG_HideProgressMask();
        });
}

function GatherParams() {
    var searchParams = {};
    Util.addParams(searchParams, filterParams);
    Util.addParams(searchParams, searchObject);
    return searchParams
}

function adjustSortIconAndAlignment(tableId){
    var columns = BuildColModel();
    var gridId = tableId + 'Grid';
    for (var col of columns) {
        if (col.sortable == false) {
            var cellSelector = '#' + tableId + ' th#' + gridId + '_' + col.name;
            $(cellSelector).find('span.s-ico').remove();
            $(cellSelector).find('div.ui-jqgrid-sortable').removeClass('ui-jqgrid-sortable');
        }
        if (col.align) {
            $("#" + gridId + "_" + col.name).addClass(col.align);
        }
    }
}