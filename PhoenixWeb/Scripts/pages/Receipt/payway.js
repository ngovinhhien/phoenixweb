﻿var urlOrigin = Util.PH_UpdateURL();
var searchObject = undefined;
$(function () {
    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });

    $('.datepickerWithDayAndMonthFormat').datepicker({
        dateFormat: 'ddmm'
    });

    //validate From Date must <= To Date
    $("#StartDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#EndDate").datepicker('getDate')
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    //validate From Date must >= To Date
    $("#EndDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#StartDate").datepicker('getDate')
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    //Trigger resize payment method jqgrid when window is resizing.
    $(window).bind('resize', function () {
        $("#grdPaywayReceipt").setGridWidth($("#paywayReceipt").width() - 20);
    }).trigger('resize');

    $("#grdPaywayReceipt").jqGrid({
        url: '',
        colNames: [
            'Merchant ID',
            'Card PAN',
            'Card Expiry',
            'Order Type',
            'Amount',
            'Currency',
            'Order Number',
            'Customer Reference Number',
            'Original Order Number',
            'Response Code',
            'Response Text',
            'Receipt Number',
            'Transaction Date',
            'Status'
        ],
        colModel: [
            { key: false, name: 'merchantId', index: 'merchantId', width: "80", editable: false, sortable: false },
            { key: false, name: 'cardPAN', index: 'cardPAN', width: "150", editable: false, sortable: false },
            { key: false, name: 'cardExpiry', index: 'cardExpiry', width: "80", editable: false, sortable: false },
            { key: false, name: 'orderType', index: 'orderType', width: "80", editable: false, sortable: false },
            { key: false, name: 'amount', index: 'amount', width: "80", editable: false, sortable: false },
            { key: false, name: 'currency', index: 'currency', width: "80", editable: false, sortable: false },
            { key: false, name: 'orderNumber', index: 'orderNumber', width: "100", editable: false, sortable: false },
            { key: false, name: 'customerReferenceNumber', index: 'customerReferenceNumber', width: "150", editable: false, sortable: false },
            { key: false, name: 'originalOrderNumber', index: 'originalOrderNumber', width: "130", editable: false, sortable: false },
            { key: false, name: 'responseCode', index: 'responseCode', width: "100", editable: false, sortable: false },
            { key: false, name: 'responseText', index: 'responseText', width: "180", editable: false, sortable: false },
            { key: false, name: 'receiptNumber', index: 'receiptNumber', width: "100", editable: false, sortable: false },
            { key: false, name: 'transactionDate', index: 'transactionDate', width: "130", editable: false, sortable: false },
            { key: false, name: 'status', index: 'status', editable: false, sortable: false },
        ],
        pager: '#grdPaywayReceiptPager',
        rowNum: 10,
        loadui: 'disable',
        rowList: [10, 20, 50, 100],
        height: '100%',
        viewrecords: true,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#grdPaywayReceipt').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdPaywayReceipt').getGridParam('page');
            var rowNum = $('#grdPaywayReceipt').getGridParam('rowNum');
            var sortName = $('#grdPaywayReceipt').getGridParam('sortname');
            var sortOrder = $('#grdPaywayReceipt').getGridParam("sortorder");

            if (pgButton === "next_grdPaywayReceiptPager") {
                if (currentPage === $('#grdPaywayReceipt').getGridParam('lastpage')) {
                    return;
                }
                currentPage = currentPage + 1;
            }
            if (pgButton === "prev_grdPaywayReceiptPager") {
                if (currentPage === 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton === "first_grdPaywayReceiptPager") {
                if (currentPage === 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton === "last_grdPaywayReceiptPager") {
                if (currentPage === $('#grdPaywayReceipt').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#grdPaywayReceipt').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton === "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton === "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdPaywayReceiptPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page.

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }
            if (!searchObject) {
                searchObject = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    CustomerReferenceNumber: $("#CustomerReferenceNumber").val(),
                    OrderNumber: $("#OrderNumber").val(),
                    StartDate: $("#TransactionStartDate").val(),
                    EndDate: $("#TransactionEndDate").val()
                };
            } else {
                searchObject.CurrentPage = currentPage;
                searchObject.PageSize = rowNum;
            }
            GetPaywayReceipt();
        }
    });

    GetPaywayReceipt = function () {
        //Show progress mask
        LG_ShowProgressMask();
        $('#grdPaywayReceipt').jqGrid('clearGridData');
        $.ajax({
            url: urlOrigin + "Receipt/GetPaywayReceiptFiles",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: JSON.stringify(searchObject)
        }).done(function (result) {
            //Loading data to grid
            var grid = $('#grdPaywayReceipt')[0];
            $('#grdPaywayReceipt').jqGrid('clearGridData');
            grid.addJSONData(result);
            LG_HideProgressMask();
        }).fail(function (err) {
            console.log(err);
            LG_HideProgressMask();
        });
    };

    $("#filter").off("click").on("click",
        function () {
            var CustomerReferenceNumber = $("#CustomerReferenceNumber").val();
            var orderNumber = $("#OrderNumber").val();
            var startDate = $("#StartDate").val();
            var endDate = $("#EndDate").val();

            if (!searchObject) {
                var currentPage = $('#grdPaywayReceipt').getGridParam('page');
                var rowNum = $('#grdPaywayReceipt').getGridParam('rowNum');
                var sortName = $('#grdPaywayReceipt').getGridParam('sortname');
                var sortOrder = $('#grdPaywayReceipt').getGridParam("sortorder");
                searchObject = {
                    CurrentPage: currentPage,
                    PageSize: 10,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    CustomerReferenceNumber: $("#CustomerReferenceNumber").val(),
                    OrderNumber: $("#OrderNumber").val(),
                    StartDate: $("#StartDate").val(),
                    EndDate: $("#EndDate").val()
                };
            } else {
                searchObject.CustomerReferenceNumber = CustomerReferenceNumber;
                searchObject.OrderNumber = orderNumber;
                searchObject.StartDate = startDate;
                searchObject.EndDate = endDate;
            }
            GetPaywayReceipt();
        });

    ExportFile = function () {
        var startDate = searchObject !== undefined ? searchObject.StartDate : $("#StartDate").val();
        var endDate = searchObject !== undefined ? searchObject.EndDate : $("#EndDate").val();
        var terminalId = searchObject !== undefined ? searchObject.CustomerReferenceNumber : $("#CustomerReferenceNumber").val();
        var orderNumber = searchObject !== undefined ? searchObject.OrderNumber : $("#OrderNumber").val();

        window.location.href =
            urlOrigin +
            "Receipt/GetPaywayReceiptExportFile?" +
            "startDate=" + startDate +
            "&endDate=" + endDate +
            "&terminalId=" + terminalId +
            "&orderNumber=" + orderNumber;
    };
});