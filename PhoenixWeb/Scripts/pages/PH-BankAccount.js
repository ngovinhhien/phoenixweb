﻿$(document).ready(function () {
   
    //Get URL Origin
    var urlOrigin = Util.PH_UpdateURL();

    onDeleteBankAccount = function (bankAccountKey) {
        if (bankAccountKey > 1) {
            return;
        }

        if (confirm('Are you sure to remove this bank account?')) {
            var bankAccountData = JSON.parse($("#BankAccountDtoAsJson").val()) || [];
            var bankAccountRemains = bankAccountData.filter(function (element) {
                return element.AccountKey != bankAccountKey;
            });
            $("#BankAccountDtoAsJson").val(JSON.stringify(bankAccountRemains));
            RenderBankAccountGrid("tblBankAccount");
        }
    }

    RenderBankAccountGrid = function (tblContainName) {
        var bankAccountDatas = JSON.parse($("#BankAccountDtoAsJson").val()) || [];

        if (bankAccountDatas.length) {
            //render header bank account grid
            //var transactionPurposeList = bankAccountData[0].TransactionPurposeDtoAsJson;
            var transactionPurposeList = JSON.parse($("#PurposeTypes").val());
            var headerHtml = $("#tmplBankAccountHeader").render({ data: transactionPurposeList });

            var needToUpdateToHiddenField = false;
            $.each(bankAccountDatas,
                function (i, bankAccountData) {
                    if (typeof bankAccountData.TransactionPurposeDtoAsJson == "string") {
                        bankAccountData.TransactionPurposeDtoAsJson =
                            Util.ParseToArrayObject(bankAccountData.TransactionPurposeDtoAsJson);
                        needToUpdateToHiddenField = true;
                    }
                });

            if (needToUpdateToHiddenField) {
                $("#BankAccountDtoAsJson").val(JSON.stringify(bankAccountDatas));
            }
            //render bank account records
            var recordHtml = $("#tmplBankAccountList").render({ data: bankAccountDatas });

            $("#" + tblContainName).html(headerHtml + recordHtml);

            //Set default state for all purpose checkbox on grid
            var purposeNameList = transactionPurposeList.map(x => x.PurposeTypeName);
            SetDefaultStateForPurposeCheckbox(purposeNameList);
        } else {
            $("#" + tblContainName).html("");
        }
    }

    OnCheckPurpose = function (elCheckbox) {
        var isChecked = $(elCheckbox).prop("checked"),
            purposeId = $(elCheckbox).data("purposeid"),
            accountKey = $(elCheckbox).data("accountkey"),
            checkboxTypeName = $(elCheckbox).data("purposename"),
            elCheckboxType = $("input[data-purposename='" + checkboxTypeName + "']");

        if (isChecked) {
            //update data
            UpdateCheckedPurposeValueToJsonData(accountKey, purposeId, isChecked);

            //disable all check box of type
            var currentCheckBoxId = $(elCheckbox).attr("id");
            var shouldDisableCheckBox = elCheckboxType.filter(function (i, el) {
                var checkboxId = $(el).attr("id");
                return currentCheckBoxId != checkboxId;
            });

            $(shouldDisableCheckBox).attr("disabled", "disabled");
        } else {
            //update data
            UpdateCheckedPurposeValueToJsonData(accountKey, purposeId, isChecked);

            //enable all check box of type
            $(elCheckboxType).removeAttr("disabled");
        }
    }

    UpdateCheckedPurposeValueToJsonData = function (accountKey, purposeId, checkValue) {
        //get all bank account data from hidden field
        var bankAccountData = JSON.parse($("#BankAccountDtoAsJson").val() || null);

        //handle update bank account data base on checkbox
        $.each(bankAccountData,
            function (i, el) {
                if (el.AccountKey == accountKey) { //check match account key
                    var purposes = Util.ParseToArrayObject(el.TransactionPurposeDtoAsJson);
                    bankAccountData[i].TransactionPurposeDtoAsJson = purposes;
                    $.each(purposes,
                        function (j, element) {
                            if (element.PurposeTypeID == purposeId) { //check match purpose id
                                //update bank account data
                                bankAccountData[i].TransactionPurposeDtoAsJson[j].Checked = checkValue;
                            }
                        });
                }
            });
        //set Bank Account json data to hidden field
        $("#BankAccountDtoAsJson").val(JSON.stringify(bankAccountData));
    }

    SetDefaultStateForPurposeCheckbox = function (purposeNames) {
        $.each(purposeNames,
            function (i, val) {
                var checkBoxTypeName = val;
                //disable all check box follow type
                var checkedCheckboxType = $("input[data-purposename='" + checkBoxTypeName + "']:checked");
                var isCheckedPurposeType = checkedCheckboxType.length > 0;
                if (isCheckedPurposeType) {
                    //all checkbox follow type
                    var elCheckboxList = $("input[data-purposename='" + checkBoxTypeName + "']");

                    var checkedCheckboxTypeId = $(checkedCheckboxType).attr("id");
                    //get all checkbox of type should be disable
                    var shouldDisableCheckBox = elCheckboxList.filter(function (index, element) {
                        var checkboxId = $(element).attr("id");
                        return checkedCheckboxTypeId != checkboxId;
                    });

                    //set disabled attr to checkbox
                    $(shouldDisableCheckBox).attr("disabled", "disabled");
                }
            });
    }

    /**
     * Method for checking required purpose type already associate with any bank account or not
     * @returns {false in case not associate, true in case associate} 
     */
    IsSettingAccountForRequiredPurpose = function() {
        var bankAccountDatas = JSON.parse($("#BankAccountDtoAsJson").val()) || [];
        if (bankAccountDatas.length == 0) {
            return "";
        }

        var PurposeTypeNames = [];
        var purposeTypeList = [];
        for (var j = 0; j < bankAccountDatas.length; j++) {
            var bankAccount = bankAccountDatas[j];
            var purposeTypes = Util.ParseToArrayObject(bankAccount.TransactionPurposeDtoAsJson);

            for (var i = 0; i < purposeTypes.length; i++) {
                var purposeType = purposeTypes[i];
                if (purposeType.Checked) {
                    purposeTypeList.push(purposeType);
                }
            }
        }

        var totalPurposes = JSON.parse($("#PurposeTypes").val());
        // In case the checked items equal with the list of purpose => All purpose have been checked.
        if (purposeTypeList.length == totalPurposes.length) {
            return "";
        }

        var totalPurposeNames = totalPurposes.map(a => a.PurposeTypeName);
        var purposeTypeListNames = purposeTypeList.map(a => a.PurposeTypeName);
        //Get the list of purpose doesn't check and display to UI
        for (var k = 0; k < totalPurposeNames.length; k++) {
            if (purposeTypeListNames.indexOf(totalPurposeNames[k]) < 0) {
                PurposeTypeNames.push(totalPurposeNames[k]);
            }
        }

        return PurposeTypeNames.join(",");
    };


    SaveNewBankAccount = function() {
        var bsbValidation = $("#BsbNumberErr").text();
        var accountNumberValidation = $("#AccountNumberErr").text();
        var accountNameValidation = $("#AccountNameErr").text();
        if (!$('#form0').valid()) {
            return;
        }

        if (bsbValidation || accountNumberValidation || accountNameValidation) {
            return;
        }

        var accountKey = buildId(),
            accountName = $("#AccountName").val().replace('\t', ' ').trim(),
            accountNumber = $("#AccountNumber").val().replace('\t', ' ').trim(),
            bsbNumber = $("#BsbNumber").val().replace('\t', ' ').trim(),
            memberKey = $("#Member_key").val();

        //Need to check on client side before sending to server side for checking
        var bankAccountDatas = JSON.parse($("#BankAccountDtoAsJson").val()) || [];
        var availabled = $.grep(bankAccountDatas,
            function(element) {
                return element.AccountNumber === accountNumber && element.BSB === bsbNumber;
            });
        if (availabled.length) {
            $("#errorMessage").text("This account has already exist in your account list.");
            return;
        }

        $.ajax({
            url: urlOrigin + "/Member/CheckValidBankAccountInfo",
            type: 'GET',
            data: { bsb: bsbNumber, accountNumber: accountNumber, memberKey: memberKey },
            async: false,
            success: function(data) {
                if (!data.IsError) {
                    $("#errorMessage").text("");
                    //get all bank account data from hidden field
                    var bankAccountData = JSON.parse($("#BankAccountDtoAsJson").val()) || [];
                    var transactionPurpose = JSON.parse($("#PurposeTypes").val());

                    if (bankAccountData.length == 0) {
                        $.each(transactionPurpose,
                            function(i, element) {
                                element.Checked = 1;
                            });
                    }

                    var newAccount = {
                        AccountKey: accountKey,
                        AccountName: accountName,
                        AccountNumber: accountNumber,
                        BSB: bsbNumber,
                        TransactionPurposeDtoAsJson: transactionPurpose || null
                    };

                    bankAccountData.push(newAccount);
                    $("#BankAccountDtoAsJson").val(JSON.stringify(bankAccountData));
                    RenderBankAccountGrid("tblBankAccount");
                    CloseMe();
                } else {
                    $("#errorMessage").text(data.ErrorMessage);
                }
            }
        });
    };

    ParsePurposeToJsonDataBeforeSave = function() {
        var bankAccountData = JSON.parse($("#BankAccountDtoAsJson").val()) || [];
        $.each(bankAccountData,
            function(i, el) {
                var purposes = el.TransactionPurposeDtoAsJson;
                if (typeof purposes != "string") {
                    el.TransactionPurposeDtoAsJson = JSON.stringify(el.TransactionPurposeDtoAsJson);
                }
            }
        );
        $("#BankAccountDtoAsJson").val(JSON.stringify(bankAccountData));
    };
});