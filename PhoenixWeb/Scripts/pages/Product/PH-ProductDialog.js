﻿//@class ProductDialog
function ProductDialog() { }
var baseURL = Util.PH_UpdateURL();
var defaultImage = $('#product-image').attr('src');
var isChangedImage = false;

ProductDialog.SaveProduct = function (url, callbackMethod) {
    if ($('#ProductForm').valid()) {
        var btnChooseFile = $('#Image')[0];
        var file = btnChooseFile.files[0];

        if (btnChooseFile.files.length > 0) {
            if (!IsValidImageFile(file)) {
                return false;
            }
        }

        var formData = new FormData();
        formData.append("File", file); //file data 
        formData.append("ProductID", $('#ProductID').val() || 0);
        formData.append("ProductName", $('#ProductName').val());
        formData.append("Price", $('#Price').val());
        formData.append("Quantity", $('#Quantity').val());
        formData.append("CategoryID", $('#Category').val());
        formData.append("IsChangedImage", isChangedImage);

        LG_ShowProgressMask();
        manageProductService.SaveProduct(url, formData).done(function (message) {
            $('#productDialog').dialog('close');

            if ($('#ProductID').val() == 0) {
                Util.MessageDialog('New product successfully created.');
            }

            if (typeof callbackMethod == "function") {
                callbackMethod();
            }

            isChangedImage = false;
        }).fail(function (message) {
            LG_HideProgressMask();
            Util.MessageDialog(message);
        });
    }
}

ProductDialog.DisplayProductDialog = function (action, productID) {
    ResetValue();
    ResetValidation();
    
    switch (action) {
        case "ADD":
            $('#product-title').text("Add New Product");

            //show dialog
            Util.ShowDialog('#productDialog', '30%');
            break;
        case "EDIT":
            manageProductService.GetProductDetail(baseURL, productID).done(function (rowData) {
                $('#product-title').text("Edit Product");

                //bind data from grid to form
                $('#ProductID').val(rowData.ProductID);
                $('#ProductName').val(rowData.ProductName);
                $('#Price').val(rowData.Price);
                $('#Quantity').val(rowData.Quantity);
                if (rowData.ImageContent != "") {
                    $('#product-image').attr('src', rowData.ImageContent);
                }            
                $('#Category').val(rowData.CategoryID);

                //show dialog
                Util.ShowDialog('#productDialog', '30%');
            });

            break;
        default:
            break;
    }
};

ResetValidation = function () {
    $('.field-validation-valid').text("");
}

ResetValue = function () {
    $('#ProductForm').trigger("reset");
    $('#ProductID').val(0);
    $('#product-image').attr('src', defaultImage);
}

IsValidImageFile = function (file) {
    var fileExtension = file.name.substr((file.name.lastIndexOf('.') + 1)).toLowerCase();

    if (file.type.indexOf("image") == -1) {
        Util.MessageDialog('File not support.');
        return false;
    }

    if (!Util.IsValidExtensionImageFile(fileExtension)) {
        Util.MessageDialog('Please select image file with .png or .jpg format to upload.');
        return false;
    }

    var imageSizeMB = file.size / 1024000;
    if (imageSizeMB > $('#image-max-size').val()) {
        Util.MessageDialog('Image size too large.');
        return false;
    }

    return true;
}

$('#Image').change(function () {
    isChangedImage = true;
    Util.DisplayImageFile('#product-image', this);
});

$("#select-image").click(function () {
    $("#Image").trigger('click');
});

$("#reset-image").click(function () {
    $('#product-image').attr('src', defaultImage);
    $('#Image').val("");
    isChangedImage = true;
});