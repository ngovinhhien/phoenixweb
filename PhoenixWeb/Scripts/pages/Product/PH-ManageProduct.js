﻿var searchGlideboxProduct = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;
var defaultImage = $('#product-detail-image').attr('src');

$(function () {
    $(window).bind("resize", function () {
        $("#gridManageProduct").setGridWidth($("#manageProduct").width() - 10);
    }).trigger("resize");

    $("#gridManageProduct").jqGrid({
        url: "",
        colNames: [
            "Product Name", "Price", "Quantity", "Category", "Created by", "Date Added", "Status", "Action"
        ],
        colModel: [
            {
                name: "ProductName",
                index: "ProductName",
                editable: false,
                sortable: true,
                align: 'left',
                formatter: customViewProductDetailFormatter
            },
            {
                name: "Price",
                index: "Price",
                editable: false,
                sortable: true,
                align: 'center'
            },
            {
                name: "Quantity",
                index: "Quantity",
                editable: false,
                sortable: true,
                align: 'center'
            },
            {
                name: "CategoryName",
                index: "CategoryName",
                editable: false,
                sortable: false,
                align: 'left'
            },
            {
                name: "CreatedBy",
                index: "CreatedBy",
                editable: false,
                sortable: false,
                align: 'left'
            },
            {
                name: "CreatedDatetime",
                index: "CreatedDatetime",
                editable: false,
                sortable: true,
                align: 'center'
            },
            {
                key: false,
                name: "IsActive",
                index: "IsActive",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customStatusFormatter
            },
            {
                key: false,
                name: "ProductID",
                index: "ProductID",
                editable: false,
                sortable: false,
                align: 'center',
                formatter: customActionFormatter
            }
        ],
        pager: "#gridManageProductPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    Util.JQGridAlignLeftHeader("gridManageProduct", "ProductName");
    Util.JQGridAlignLeftHeader("gridManageProduct", "CategoryName");
    Util.JQGridAlignLeftHeader("gridManageProduct", "CreatedBy");

    $('#gridManageProduct').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#gridManageProduct').getGridParam('page');
            var rowNum = $('#gridManageProduct').getGridParam('rowNum');

            if (pgButton == "next_gridManageProductPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_gridManageProductPager") {

                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_gridManageProductPager") {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }

            if (pgButton == "last_gridManageProductPager") {
                if ($('#gridManageProduct').getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $('#gridManageProduct').getGridParam('lastpage')) {
                    return;
                }
                currentPage = $('#gridManageProduct').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='gridManageProductPager']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            if (currentPage > totalPage) {
                return;
            }

            if (!searchGlideboxProduct) {
                searchGlideboxProduct = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                };
            } else {
                searchGlideboxProduct.CurrentPage = currentPage;
                searchGlideboxProduct.PageSize = rowNum;
            }

            loadManageProductData();
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchGlideboxProduct) {
                searchGlideboxProduct.SortName = sortName;
                searchGlideboxProduct.SortOrder = sortOrder;
            }
            loadManageProductData();
            return 'stop';
        }
    });

    searchGlideboxProduct = {
        CurrentPage: 1,
        PageSize: 10,
        CategoryID: $('#CategoryID option:selected').val()
    };
    loadManageProductData();
});

loadManageProductData = function () {
    //Show progress mask
    LG_ShowProgressMask();

    var isValidCategoryID = false;
    var productCategoryID = $('#product-categoryID').val();
    $("#CategoryID option").each(function () {
        if (productCategoryID == $(this).val()) {
            isValidCategoryID = true;
        }
    });

    var isProductsMappedCategoryMode = false;
    if (!isValidCategoryID || (productCategoryID == "" || productCategoryID == "-1")) {
        ProductMode();
    }
    else {
        ProductsMappedCategoryMode();
        isProductsMappedCategoryMode = true;
    }

    manageProductService.SearchGlideboxProduct(baseURL, searchGlideboxProduct)
        .done(function (result) {
            //Loading data to grid
            var grid = $("#gridManageProduct")[0];
            $("#gridManageProduct").jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = false;

            if (isProductsMappedCategoryMode) {
                manageCategoryService.GetCategoryDetail(baseURL, productCategoryID).done(function (rowData) {
                    $('#product-header').text(rowData.CategoryName);
                });
            }

            LG_HideProgressMask();
        });
};

$('#btnSearchProduct').on('click', function () {
    searchProduct($("#textAdvanceSearch").val(), $("#CategoryID option:selected").val());
})

$('#textAdvanceSearch').on('keyup', function (event) {
    if (event.which === 13) {
        searchProduct($("#textAdvanceSearch").val(), $("#CategoryID option:selected").val());
    }
})

$('#CategoryID').on('change', function () {
    searchProduct($("#textAdvanceSearch").val(), $("#CategoryID option:selected").val());
});

searchProduct = function (searchText, categorySelected) {
    searchGlideboxProduct.SearchText = searchText;
    searchGlideboxProduct.CategoryID = categorySelected;
    //check in the first time use filter search then reset CurrentPage value
    if (!isFiltering) {
        searchGlideboxProduct.CurrentPage = 1;
    }

    LG_ShowProgressMask();

    manageProductService.SearchGlideboxProduct(baseURL, searchGlideboxProduct)
        .done(function (result) {
            var grid = $('#gridManageProduct')[0];
            $('#gridManageProduct').jqGrid('clearGridData');
            grid.addJSONData(result);

            //the notice flag is using advance search
            isFiltering = true;
            LG_HideProgressMask();
        });
}

customActionFormatter = function (productID, options, rowObject) {
    var html = "";
    if ($("#btnAdd").length) {
        if (rowObject.IsActive) {
            html = html + " <u><a title='Deactivate' onclick='onDeactivateProduct(" + rowObject.ProductID + ")'>Deactivate</a></u> ";
        }
        else {
            html = html + " <u><a title='Activate' onclick='onActivateProduct(" + rowObject.ProductID + ")'>Activate</a></u> ";
        }

        html = html + " <u><a title='Edit' onclick='onEditProduct(" + rowObject.ProductID + ")'>Edit</a></u> ";
        html = html + " <u><a title='History' onclick='GetGlideboxHistory(" + rowObject.ProductID + "," + '"' + rowObject.ProductName + '"' + "," + '"Product"' + ")'>History</a></u>";
    } else {
        html = "";
    }

    return html;
};

customViewProductDetailFormatter = function (productID, options, rowObject) {
    var html = "";
    if ($("#btnAdd").length) {
        html = html + " <a onclick='GetProductDetail(" + rowObject.ProductID + ")'>" + rowObject.ProductName +"</a> ";
    } else {
        html = "";
    }

    return html;
};

customStatusFormatter = function (isActive, options, rowObject) {
    var html = "";

    if (isActive) {
        html = html + "<i class='glyphicon glyphicon-ok' style='color: #04ac4c'></i>";
    }
    else {
        html = html + "<i class='glyphicon glyphicon-remove' style='color: red'></i>";
    }

    return html;
};

onDeactivateProduct = function (id) {
    Util.CloseDialog('#productDetailDialog');
    ActivateDeactivateProductDialog.DisplayDialog("DEACTIVATE", id);
}

onActivateProduct = function (id) {
    Util.CloseDialog('#productDetailDialog');
    ActivateDeactivateProductDialog.DisplayDialog("ACTIVATE", id);
}

$("#btnAdd").on("click",
    function () {
        ProductDialog.DisplayProductDialog("ADD");
    });

onEditProduct = function (id) {
    Util.CloseDialog('#productDetailDialog');
    ProductDialog.DisplayProductDialog("EDIT", id);
}

onSaveProduct = function () {
    ProductDialog.SaveProduct("", loadManageProductData);
}

GetProductDetail = function (productID) {
    LG_ShowProgressMask();

    manageProductService.GetProductDetail(baseURL, productID).done(function (rowData) {
        $('#product-action').html("");

        $('#product-name').text(rowData.ProductName);
        $('#product-price').text("$" + rowData.Price);
        $('#product-quantity').text(rowData.Quantity);
        $('#product-created-by').text(rowData.CreatedBy);
        $('#product-created-datetime').text(rowData.CreatedDatetime);
        $('#product-category').text(rowData.CategoryName);

        if (rowData.IsActive) {
            $('#product-status').text("Active");
            $('#product-action').append("<div class='col-md-4'><u> <a onclick='onDeactivateProduct(" + rowData.ProductID + ")'>Deactivate</a></u></div>"); 
        }
        else {
            $('#product-status').text("Deactive");
            $('#product-action').append("<div class='col-md-4'><u> <a onclick='onActivateProduct(" + rowData.ProductID + ")'>Activate</a></u></div>"); 
        }
        if (rowData.ImageContent != "") {
            $('#product-detail-image').attr('src', rowData.ImageContent);
        }
        else {
            $('#product-detail-image').attr('src', defaultImage);
        }

        $('#product-action').append("<div class='col-md-3'><u> <a onclick='onEditProduct(" + rowData.ProductID + ")'>Edit</a></u></div>"); 
        $('#product-action').append("<div class='col-md-4'><u> <a onclick='GetGlideboxHistory(" + rowData.ProductID + "," + '"' + rowData.ProductName + '"' + "," + '"Product"' + ")'>History</a></u></div>"); 
        //show dialog
        Util.ShowDialog('#productDetailDialog', '50%');

        LG_HideProgressMask();
    });
}

$("#edit-category").on("click",
    function () {
        CategoryDialog.DisplayCategoryDialog("EDIT", $('#product-categoryID').val());
    });

$("#add-product").on("click",
    function () {        
        ProductDialog.DisplayProductDialog("ADD");
        $('#Category').val($('#product-categoryID').val());        
    });

onSaveCategory = function () {
    CategoryDialog.SaveCategory("", loadManageProductData);
}

ProductMode = function () {
    $('#btnAdd').show();
    $('#CategoryID').show();
    $('#product-header').text("Product");

    $('#edit-category').hide();
    $('#add-product').hide();

    $('#products-mapped-category-mode').show();
}

ProductsMappedCategoryMode = function () {
    $('#btnAdd').hide();
    $('#CategoryID').hide();
    $('#edit-category').show();
    $('#add-product').show();
    $('#products-mapped-category-mode').hide();
}