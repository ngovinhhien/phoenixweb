﻿(function (manageProductService, $, undefined) {
    manageProductService.SearchGlideboxProduct = function (baseURL, searchGlideboxProduct) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Product/GetProducts",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchGlideboxProduct),
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

    manageProductService.ActivateDeactivateProduct = function (url, formData) {
        var deferred = $.Deferred();
        var saveURL = "";
        if (url) {
            saveURL = url;
        } else {
            saveURL = "Product/ActivateDeactivateProduct";
        }

        $.ajax({
            url: baseURL + saveURL,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(formData),
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve();
                }
            }
        });
        return deferred.promise();
    }

    manageProductService.SaveProduct = function (url, formData) {
        var deferred = $.Deferred();
        var saveURL = "";
        if (url) {
            saveURL = url;
        } else {
            saveURL = "Product/SaveProduct";
        }

        $.ajax({
            url: baseURL + saveURL,
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve();
                }
            }
        });
        return deferred.promise();
    }

    manageProductService.GetProductDetail = function (baseURL, productID) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "Product/GetProductDetail",
            type: "GET",
            data: {
                productID: productID
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }

}(window.manageProductService = window.manageProductService || {}, jQuery));