﻿//@class ActivateDeactivateProductDialog
function ActivateDeactivateProductDialog() { }
var baseURL = Util.PH_UpdateURL();
var isActivateAction = false;

ActivateDeactivateProductDialog.SaveChanges = function (action, url, callbackMethod) {
    if ($('#activateDeactivateForm').valid()) {
        if (action === "Activate") {
            isActivateAction = true;
        }

        var data = {
            ID: $('#ID').val(),
            Reason: $('#Reason').val(),
            IsActivateAction: isActivateAction
        }

        LG_ShowProgressMask();
        manageProductService.ActivateDeactivateProduct(url, data).done(function (message) {
            $('#activateDeactivateDialog').dialog('close');

            if (typeof callbackMethod == "function") {
                callbackMethod();
            }

            isActivateAction = false;
        }).fail(function (message) {
            LG_HideProgressMask();
            Util.MessageDialog(message);
        });
    }
}

ActivateDeactivateProductDialog.DisplayDialog = function (action, productID) {
    $("#activateDeactivateForm").trigger("reset");
    resetValidation();

    switch (action) {
        case "ACTIVATE":
            $('#title-action').text("Are you sure you want to activate this item?");
            $('#ID').val(productID);

            //show dialog
            showActivateDeactivateProductDialog("Activate");
            break;
        case "DEACTIVATE":
            $('#title-action').text("Are you sure you want to deactivate this item?");
            $('#ID').val(productID);

            //show dialog
            showActivateDeactivateProductDialog("Deactivate");

            break;
        default:
            break;
    }
};

resetValidation = function () {
    $('.field-validation-valid').text("");
}

showActivateDeactivateProductDialog = function (action) {
    $('#activateDeactivateDialog').dialog({
        modal: true,
        resizable: false,
        width: '30%',
        buttons: [{
            text: action,
            click: function () {
                onActivateDeactivateProduct(action);
            },
            class: 'btn blue'
        }]
    });
}

onActivateDeactivateProduct = function (action) {
    ActivateDeactivateProductDialog.SaveChanges(action, "", loadManageProductData);
}