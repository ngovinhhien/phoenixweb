﻿var urlOrigin = Util.PH_UpdateURL();
$(document).ready(function () {
    $('#AllocateDocket_EffectiveDate').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        yearRange: '2000:' + (new Date().getFullYear())
    });
    initGridUnallocateDockets();
    initGridTerminalAllocations();
    $('#btnSearch').click(function () {
        var searchParams = {
            PageIndex: 1,
            PageSize: $('[id^=grdUnallocatedDocketPager] select.ui-pg-selbox').val(),
            Request: {
                DocketBatchId: $('#DocketBatchId').val(),
                TerminalId: $('#TerminalId').val(),
                BatchNumber: $('#BatchNumber').val()
            }
        };
        loadGridUnallocateDocketData(searchParams);
        gridUnallocateDocketEvents(searchParams);
    })
});
function initGridUnallocateDockets() {

    $('#grdUnallocatedDocket').jqGrid({
        url: "",
        colNames: [
            "", "Docket Batch ID", "Terminal ID", "Vehicle ID", "Docket Date", "Docket Total", "Docket Key", "Face Value", "Extras", "Service Charge", "Gst", "Batch Number"
        ],
        colModel: [
            { key: false, name: "", index: '', editable: false, sortable: false, formatter: addLinkViewDetail, align: "center"},
            { key: false, name: "DocketBatchId", index: "DockBatchId", editable: false, sortable: false, align: "center"},
            { key: false, name: "TerminalId", index: "TerminalId", editable: false, sortable: false, align: "center"},
            { key: false, name: "VehicleId", index: "VehicleId", editable: false, sortable: false, align: "center"},
            { key: false, name: "DocketDate", index: "DocketDate", editable: false, sortable: false, width: 100, formatter: formatDate, align: "center"},
            { key: false, name: "DocketTotal", index: "DocketTotal", editable: false, align: "center", sortable: false },
            { key: false, name: "Docket_key", index: "Docket_key", editable: false, sortable: false, align: "center"},
            { key: false, name: "FaceValue", index: "FaceValue", editable: false, sortable: false, align: "center"},
            { key: false, name: "Extras", index: "Extras", editable: false, sortable: false, align: "center"},
            { key: false, name: "ServiceCharge", index: "ServiceCharge", editable: false, sortable: false, align: "center"},
            { key: false, name: "Gst", index: "Gst", editable: false, sortable: false, align: "center"},
            { key: false, name: "BatchNumber", index: "BatchNumber", editable: false, sortable: false, align: "center"},
        ],
        pager: '#grdUnallocatedDocketPager',
        rowNum: 20,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    function formatDate(cellValue, option, rowObject) {
        try {
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
    function addLinkViewDetail(cellValue, option, rowObject) {
        var tag = $('<button>Allocate Docket</button>');
        tag.attr({
            'data-docket-batch-id': rowObject.DocketBatchId,
            'data-docket-date': rowObject.DocketDate,
            'data-terminal-id': rowObject.TerminalId,
            class: 'open-allocate-docket',
            'onclick': 'openAllocateDocket(this)',
            'rowid': option.rowId
        });
        return tag[0].outerHTML;
    }
}
function loadGridUnallocateDocketData(searchParams) {
    ;
    LG_ShowProgressMask();

    $.ajax({
        url: urlOrigin + "Docket/GetUnallocatedDockets",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdUnallocatedDocket')[0];
        $('#grdUnallocatedDocket').jqGrid('clearGridData');
        grid.addJSONData(result);
        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}
function gridUnallocateDocketEvents(searchParams) {

    $('#grdUnallocatedDocket').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdUnallocatedDocket').getGridParam('page');
            var rowNum = $('#grdUnallocatedDocket').getGridParam('rowNum');

            if (pgButton == "next_grdUnallocatedDocketPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdUnallocatedDocketPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdUnallocatedDocketPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdUnallocatedDocketPager") {
                currentPage = $('#grdUnallocatedDocket').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdUnallocatedDocketPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridUnallocateDocketData(searchParams);
        }
    });
}
var popupTerminalAllocationIsOpened = false;
function openAllocateDocket(selector) {
    var self = $(selector);
    var searchParams = {
        PageIndex: 1,
        PageSize: $('[id^=grdTerminalAllocationPager] select.ui-pg-selbox').val(),
        Request: {
            TerminalId: self.attr('data-terminal-id'),
            DocketBatchId: self.attr('data-docket-batch-id'),
            DocketDate: self.attr('data-docket-date')
        }
    };
    popupTerminalAllocationIsOpened = false;
    loadGridTerminalAllocationData(searchParams);
    gridTerminalAllocationEvents(searchParams);
}
function initGridTerminalAllocations() {
    $('#grdTerminalAllocation').jqGrid({
        url: "",
        colNames: [
            "Terminal Id", "Member Id", "Vehicle Id", "Effective Date", "Is Current", "Modified By User", "Deallocate Date", "TerminalAllocation_key", "Vehicle Type Name", "EffectiveDateRaw"
        ],
        colModel: [
            { key: false, name: "TerminalId", index: "TerminalId", editable: false, sortable: false },
            { key: false, name: "MemberId", index: "MemberId", editable: false, sortable: false },
            { key: false, name: "VehicleId", index: "VehicleId", editable: false, sortable: false },
            { key: false, name: "EffectiveDate", index: "EffectiveDate", editable: false, sortable: false, formatter: formatDate },
            { key: false, name: "IsCurrent", index: "IsCurrent", editable: false, sortable: false, formatter: function (cellValue) { return cellValue ? 'Yes' : 'No'; } },
            { key: false, name: "ModifiedByUser", index: "ModifiedByUser", editable: false, sortable: false },
            { key: false, name: "UnallocatedDate", index: "UnallocatedDate", editable: false, sortable: false, formatter: formatDate },
            { key: false, name: "TerminalAllocation_key", index: "TerminalAllocation_key", editable: false, sortable: false, hidden: true },
            { key: false, name: "VehicleTypeName", index: "VehicleTypeName", editable: false, sortable: false, hidden: true },
            { key: false, name: "EffectiveDate", index: "EffectiveDate", editable: false, sortable: false, hidden: true },
        ],
        pager: '#grdTerminalAllocationPager',
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 30, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "Response",
            page: "PageIndex",
            total: "TotalPages",
            records: "TotalItems",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false,

    });

    function formatDate(cellValue, option, rowObject) {
        try {
            if (cellValue == null) {
                return '';
            }
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
}
function loadGridTerminalAllocationData(searchParams) {
    LG_ShowProgressMask();

    $.ajax({
        url: urlOrigin + "Docket/GetTerminalAllocations",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchParams)
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdTerminalAllocation')[0];
        $('#grdTerminalAllocation').jqGrid('clearGridData');
        grid.addJSONData(result);
        LG_HideProgressMask();
        if (!popupTerminalAllocationIsOpened) {
            if (result == undefined || result == null || result.length == 0) {
                var msg = 'Terminal ' + result.Request.TerminalId + ' has not allocated to member yet!';
                $('#dialogInfo').dialog();
                $('#dialogInfoContent').html(msg);
            } else {
                $('#grdTerminalAllocation').jqGrid('setSelection', 1, true)
                var popup = $('#dialogAllocateDocket').dialog();
                popup.parents('.ui-dialog').find('.ui-dialog-title').text('Allocate Docket')
                popup.dialog('option', {
                    'width': '70%',
                    'max-height': '80%'
                });
            }
            popupTerminalAllocationIsOpened = true;
        }
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
}
function gridTerminalAllocationEvents(searchParams) {
    $('#grdTerminalAllocation').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdTerminalAllocation').getGridParam('page');
            var rowNum = $('#grdTerminalAllocation').getGridParam('rowNum');

            if (pgButton == "next_grdTerminalAllocationPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdTerminalAllocationPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdTerminalAllocationPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdTerminalAllocationPager") {
                currentPage = $('#grdTerminalAllocation').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdTerminalAllocationPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0;

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }

            searchParams.PageIndex = currentPage;
            searchParams.PageSize = rowNum;

            loadGridTerminalAllocationData(searchParams);
        },
        onSelectRow: function (id, stat, e) {
            var rowData = $('#grdTerminalAllocation').getRowData(id);
            rowData.RowId = id;
            rowData.DocketBatchId = searchParams.Request.DocketBatchId;
            fillTerminalAllocation(rowData);
        }
    });

}
function fillTerminalAllocation(data) {
    var popup = $('#dialogAllocateDocket');
    popup.find('#TerminalAllocation_TerminalId').text(data.TerminalId);
    popup.find('#TerminalAllocation_DocketBatchId').text(data.DocketBatchId);
    popup.find('#TerminalAllocation_MemberId').text(data.MemberId);
    popup.find('#TerminalAllocation_VehicleType').text(data.VehicleTypeName);
    popup.find('#TerminalAllocation_IsCurrent').text(data.IsCurrent);
    popup.find('#TerminalAllocation_ModifiedByUser').text(data.ModifiedByUser);
    popup.find('#TerminalAllocation_DeallocatedDate').text(data.UnallocatedDate);
    popup.find('#AllocateDocket_Save').attr({ 'data-id': data.TerminalAllocation_key, 'row-id': data.RowId });
    var effectiveDate = new Date(data.EffectiveDate);
    popup.find('#AllocateDocket_EffectiveDate').val(effectiveDate.FormatDate());
    popup.find('#AllocateDocket_EffectiveTime').val(effectiveDate.FormatTime());
}

function checkTerminalAllocation(rowId) {
    var data = $('#grdTerminalAllocation').getRowData(rowId);
    var flg = false;
    if (data == undefined || data == null) {
        flg = false;
    } else {
        flg = data.MemberId != null && data.VehicleId != null;
    }
    if (!flg) {
        var msg = 'Terminal ' + data.TerminalId + ' has not been allocated on this Effective date yet!';
        $('#dialogInfo').dialog();
        $('#dialogInfoContent').html(msg);
    }
    return flg;
}

function allocateDocket(selector) {
    var self = $(selector);
    if (self.text() == 'Saving') {
        return;
    }
    var popup = $('#dialogAllocateDocket');
    var rowId = self.attr('row-id');
    if (!checkTerminalAllocation(rowId)) {
        return;
    }

    var eff_date = popup.find('#AllocateDocket_EffectiveDate').val(),
        eff_time = popup.find('#AllocateDocket_EffectiveTime').val();
    var arr_date = eff_date.split('/'),
        arr_time = eff_time == '' ? [0, 0] : eff_time.split(':');

    var submitParams = {
        TerminalAllocationKey: self.attr('data-id'),
        EffectiveDate: new Date(arr_date[2], parseInt(arr_date[1]) - 1, arr_date[0], arr_time[0], arr_time[1]),
        DocketBatchId: popup.find('#TerminalAllocation_DocketBatchId').text()
    };

    $.ajax({
        url: urlOrigin + "Docket/AllocateDocket",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(submitParams)
    }).done(function (result) {
        if (result == '') {
            $('#dialogInfoContent').html('Allocated docket successfully!');
            $('#dialogAllocateDocket').dialog('close');
            $('btnSearch').trigger('click');
        } else {
            $('#dialogInfoContent').html(result);
        }
        $('#dialogInfo').dialog();
        self.text('Save');
    }).fail(function (err) {
        console.log(err);
        $('#dialogAllocateDocket').dialog('close');
        self.text('Save');
    });
}