﻿$(document).ready(function () {
    $(".errorIcon").hide();
    $(".warningIcon").hide();

    LH_ChangeDebitRateDisplay = function (BusinessTypeVal) {
        var rdbDebitRatePercentage = $('#Percentage[name="rado"]');
        var rdbDebitRateDollar = $('#Dollar[name="rado"]');

        rdbDebitRatePercentage.attr('disabled', false);
        rdbDebitRateDollar.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1"://TaxiEpay
                {
                    rdbDebitRatePercentage.prop('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "18"://Live Eftpos
                {
                    rdbDebitRateDollar.prop('checked', true);
                    //Removed by PHW-1618
                    //rdbDebitRatePercentage.attr('disabled', true);
                }
                break;
            case "19"://DirectLiveEftpos
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);

                }
                break;
            case "20"://TaxiPro
                {
                    rdbDebitRateDollar.attr('checked', true);
                    rdbDebitRatePercentage.attr('disabled', true);
                }
                break;
            case "21"://TaxiApp
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "22"://LiveEftpos black
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "23"://LiveEftpos black for taxi
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "24"://Live limo
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "25"://Glide
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "26"://Live Eftpos Integrated
                {
                    rdbDebitRateDollar.prop('checked', true);
                    rdbDebitRatePercentage.attr('disabled', true);
                }
                break;
            case "27"://Live Eftpos Black Integrated
                {
                    rdbDebitRatePercentage.attr('checked', true);
                    rdbDebitRateDollar.attr('disabled', true);
                }
                break;
            case "28"://LiveSMS
                break;
            case "29"://TaxiEpay
                {
                    rdbDebitRatePercentage.prop('checked', true);
                }
                break;

        }
    };

    LH_ChangeQatasRateDisplay = function (BusinessTypeVal) {
        var rdbQatasRatePercentage = $('#Percentage[name="qantasrado"]');
        var rdbQatasRateDollar = $('#Dollar[name="qantasrado"]');

        rdbQatasRatePercentage.attr('disabled', false);
        rdbQatasRateDollar.attr('disabled', false);

        switch (BusinessTypeVal) {
            case "1": //TaxiEpay
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "18": //Live Eftpos
                {
                    rdbQatasRateDollar.attr('checked', true);
                }
                break;
            case "19": //DirectLiveEftpos
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);

                }
                break;
            case "20": //TaxiPro
                {
                    rdbQatasRateDollar.attr('checked', true);
                    rdbQatasRatePercentage.attr('disabled', true);
                }
                break;
            case "21": //TaxiApp
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "22": //LiveEftpos black
                break;
            case "23": //LiveEftpos black for taxi
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "24": //Live limo
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "25": //Glide
                {
                    rdbQatasRatePercentage.attr('checked', true);
                    rdbQatasRateDollar.attr('disabled', true);
                }
                break;
            case "26": //Live Eftpos Integrated
                {
                    rdbQatasRatePercentage.attr('checked', true);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                {
                    rdbQatasRatePercentage.attr('checked', true);
                }
                break;
            case "28": //LiveSMS
                {
                    rdbQatasRateDollar.attr('disabled', true);
                    rdbQatasRatePercentage.attr('disabled', true);
                }
                break;
            case "29"://TaxiEpay
                {
                    rdbQatasRatePercentage.prop('checked', true);
                }
                break;
        }
    };


    LH_ChangeDebitRateDisplay($("#BusinessType").val());
    LH_ChangeQatasRateDisplay($("#BusinessType").val());

    $("#submitResult").off("click").on("click",
        function () {

            $("#CRDebitRate1").trigger("focusout");
            $("#CRQantasRate1").trigger("focusout");
            $("#CRVisaRate1").trigger("focusout");
            $("#CRMasterCardRate1").trigger("focusout");
            $("#CRAmexRate1").trigger("focusout");
            $("#CRDinersRate1").trigger("focusout");
            $("#CRUnionPayRate1").trigger("focusout");
            $("#CRZipPayRate1").trigger("focusout");
            $("#CRAlipayRate1").trigger("focusout");

            var allRates = $(".cmrate");

            for (var i = 0; i < allRates.length; i++) {
                if ($(allRates[i]).hasClass("errorClass")) {
                    $(allRates[i]).focus();
                    return;
                }
            }

            //Hot fix for issue PHW-697 
            //TODO: Need to check why we need to do that.
            var isDollarChecked = $("#CRQantasRateId #Dollar").prop('checked');
            var rdbQatasRatePercentage = $('#Percentage[name="qantasrado"]');
            var rdbQatasRateDollar = $('#Dollar[name="qantasrado"]');
            if (isDollarChecked) {
                rdbQatasRateDollar.attr('checked', true);
            } else {
                rdbQatasRatePercentage.attr('checked', true);
            }

            var isDollarCheckedDebit = $("#CRBeditRateId #Dollar").prop('checked');
            var rdbDebitRatePercentage = $('#CRBeditRateId #Percentage[name="rado"]');
            var rdbDebitRateDollar = $('#CRBeditRateId #Dollar[name="rado"]');
            if (isDollarCheckedDebit) {
                rdbDebitRateDollar.attr('checked', true);
            } else {
                rdbDebitRatePercentage.attr('checked', true);
            }

            $("#form0").submit();
        });
    $("#CRDebitRate1, #CRVisaRate1, #CRMasterCardRate1, #CRAmexRate1, #CRDinersRate1, #CRUnionPayRate1, #CRZipPayRate1, #CRQantasRate1, #CRAlipayRate1 ").keypress(function (e) {
        var currentValue = $(this).val();
        var selectionStart = $(this)[0].selectionStart;
        var fullContent = PH_BuildFullContentWithKeyPressEvent(currentValue, event.key, selectionStart);
        if (isNaN(Number(fullContent))) {
            event.preventDefault();
        }
    });

    $("#CRDebitRate1").focusout(function () {

        var fieldname = "#CRDebitRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Debit rate";
        var placement = "bottom";

        var isDollarChecked = $("#CRDebitRateId #Dollar").prop('checked');

        //alert(fieldname);
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, -0.1);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, -0.1);
                } else {
                    result = validateAmount(fieldname, -5, -0.5);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, -0.5);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                //Check percentice or dollar
                //if dollar, from -2 to 0
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0, isDollarChecked);
                } else {
                    result = validateAmount(fieldname, -3, 0, isDollarChecked);
                }
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);

    });

    $("#CRVisaRate1").focusout(function () {
        var fieldname = "#CRVisaRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Visa rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRMasterCardRate1").focusout(function () {

        var fieldname = "#CRMasterCardRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Master Card rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -3, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRAmexRate1").focusout(function () {

        var fieldname = "#CRAmexRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Amex rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRDinersRate1").focusout(function () {

        var fieldname = "#CRDinersRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Diners rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRQantasRate1").focusout(function () {

        var fieldname = "#CRQantasRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Qantas rate";
        var placement = "bottom";
        var isDollarChecked = $("#CRQantasRateId #Dollar").prop('checked');
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
                //Check percentice or dollar
                //if dollar, from -2 to -0.1
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -2, 2);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
                if (isDollarChecked) {
                    result = validateAmount(fieldname, -2, 0);
                } else {
                    result = validateAmount(fieldname, -5, 0);
                }
                break;
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0, isDollarChecked);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRUnionPayRate1").focusout(function () {
        var fieldname = "#CRUnionPayRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "UnionPay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRZipPayRate1").focusout(function () {

        var fieldname = "#CRZipPayRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "ZipPay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -6, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });

    $("#CRAlipayRate1").focusout(function () {

        var fieldname = "#CRAlipayRate1";
        var val1 = $("#BusinessType").val();
        var ratename = "Alipay rate";
        var placement = "right";
        var result = false;
        switch (val1) {
            case "1": //TaxiEpay
                result = validateAmount(fieldname, 0, 5);
                break;
            case "18": //Live Eftpos
            case "19": //DirectLiveEftpos
                result = validateAmount(fieldname, -5, 0);
                break;
            case "20": //TaxiPro
                result = validateAmount(fieldname, -5, 5);
                break;
            case "21": //TaxiApp
                result = validateAmount(fieldname, 0, 5);
                break;
            case "22": //LiveEftpos black
                result = validateAmount(fieldname, -5, 0);
                break;
            case "23": //LiveEftpos black for taxi
            case "24": //Live limo
            case "25": //Glide
                result = validateAmount(fieldname, 0, 5);
                break;
            case "26": //Live Eftpos Integrated
            case "27": //Live Eftpos Black Integrated
                result = validateAmount(fieldname, -5, 0);
                break;
            case "28": //LiveSMS
                break;
            case "29": //CSA Integrated
                result = validateAmount(fieldname, -4, 0);
                break;
        }
        Util.CommissionRateValidationRule(fieldname, val1, ratename, placement, result);
    });
});