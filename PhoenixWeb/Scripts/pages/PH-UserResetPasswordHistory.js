﻿$(function () {
    var baseUrl = Util.PH_UpdateURL();
    var currentYear = new Date().getFullYear();

    var searchObject = {};

    $(".active").removeClass("active open");
    $(".Administration").addClass("active open");

    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: (currentYear - 3) + ":" + (currentYear + 3)
    });

    var currentDate = new Date();

    $('#fromDate').datepicker('setDate', new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 7));
    $('#toDate').datepicker('setDate', currentDate);

    //validate From Date must <= To Date
    $('#fromDate').datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $('#toDate').datepicker('getDate')
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    //validate From Date must >= To Date
    $('#toDate').datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $('#fromDate').datepicker('getDate')
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    $('#btnSearch').on('click', function () {
        searchObject.pageSize = 10;
        searchObject.currentPage = 1;

        searchObject.fromDate = $('#fromDate').val();
        searchObject.toDate = $('#toDate').val();
        searchObject.searchText = $('#searchText').val();

        loadSearchUserResetPasswordHistory(baseUrl, searchObject);
    });

    $(window).bind("resize", function () {
        $("#gridHistory").setGridWidth($("#resetPasswordHistory").width() - 10);
    }).trigger("resize");

    $("#gridHistory").jqGrid({
        url: "",
        colNames: [
            "User name",
            "Email address",
            "Reset date",
            "Source",
            "Reset by"
        ],
        colModel: [
            {
                name: "UserName",
                index: "UserName",
                editable: true,
                sortable: false,
            },
            {
                name: "Email",
                index: "Email",
                editable: true,
                sortable: false,
            },
            {
                name: "CreatedDate",
                index: "CreatedDate",
                editable: true,
                sortable: false,
                formatter: formatDate
            },
            {
                name: "Source",
                index: "Source",
                editable: true,
                sortable: false,
                width: 300
            },
            {
                name: "CreatedByUser",
                index: "CreatedByUser",
                editable: true,
                sortable: false,
            }
        ],
        pager: "#gridHistoryPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#gridHistory').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#gridHistory').getGridParam('page');
            var rowNum = $('#gridHistory').getGridParam('rowNum');


            if (pgButton == "next_gridHistoryPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_gridHistoryPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_gridHistoryPager") {
                currentPage = 1;
            }

            if (pgButton == "last_gridHistoryPager") {
                currentPage = $('#gridHistory').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='gridHistoryPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }

            searchObject.pageSize = rowNum;
            searchObject.currentPage = currentPage;

            loadSearchUserResetPasswordHistory(baseUrl, searchObject);
        }
    });

    function formatDate(cellValue, option, rowObject) {
        try {
            var date = new Date(cellValue);
            if (date != undefined) {
                return date.FormatFullDateTime();
            } else {
                return '';
            }
        } catch (e) {

        }
    }
});

loadSearchUserResetPasswordHistory = function (baseUrl, searchObject) {
    LG_ShowProgressMask();

    $.ajax({
        url: baseUrl + "UserResetPasswordHistory/GetHistory",
        type: 'POST',
        data: searchObject,
        success: function (result) {
            $('#gridHistory').jqGrid('clearGridData');

            if (result != null) {
                var grid = $('#gridHistory')[0];
                grid.addJSONData(result);
            }
            else {
                alert("System Error");
            }           
        }
    });

    LG_HideProgressMask();
}