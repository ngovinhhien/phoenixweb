﻿var viewModel = {}

$(function() {

   if ($("#grdQantasPointEarnedRateGrid").length) {
        $("#grdQantasPointEarnedRateGrid").jqGrid({
            url: "",
            colNames: [
                "ID", "IsUsed", "ExchangeRateTypeCode", "RegisterEffectedDays",
                "Name", "Is Default", "Type", "Earned Rate", "From Date", "To Date", "Action"
            ],
            colModel: [
                { key: true, name: "ID", index: "ID", editable: false, sortable: false, hidden: true },
                {
                    key: false,
                    name: "IsUsed",
                    index: "IsUsed",
                    editable: false,
                    sortable: false,
                    hidden: true
                },
                {
                    key: false,
                    name: "ExchangeRateTypeCode",
                    index: "ExchangeRateTypeCode",
                    editable: false,
                    sortable: false,
                    hidden: true
                },
                {
                    key: false,
                    name: "RegisterEffectedDays",
                    index: "RegisterEffectedDays",
                    editable: false,
                    sortable: false,
                    hidden: true
                },
                { key: false, name: "Name", index: "Name", editable: false, sortable: true },
                {
                    key: false,
                    name: "IsDefault",
                    index: "IsDefault",
                    editable: false,
                    sortable: true,
                    formatter: customIsDefaultFormatter
                },
                { key: false, name: "Type", index: "Type", editable: false, sortable: true, align: "center" },
                {
                    key: false,
                    name: "QantasExchangeRate",
                    index: "QantasExchangeRate",
                    editable: false,
                    sortable: true,
                    align: "center"
                },
                {
                    key: false,
                    name: "FromDate",
                    index: "FromDate",
                    editable: false,
                    sortable: true,
                    align: "center"
                },
                {
                    key: false,
                    name: "ToDate",
                    index: "ToDate",
                    editable: false,
                    sortable: true,
                    align: "center"
                },
                {
                    key: false,
                    name: "Action",
                    index: "Action",
                    editable: false,
                    sortable: false,
                    align: 'center',
                    formatter: customActionFormatter
                }
            ],
            pager: "#grdQantasPointEarnedRateGridPager",
            loadui: "disable",
            datatype: 'json',
            height: '100%',
            rowNum: 10,
            rowList: [10, 20, 50, 100],
            height: "100%",
            viewrecords: true,
            emptyrecords: "No records to display",
            jsonReader: {
                root: "rows",
                page: "page",
                total: "total",
                records: "records",
                repeatitems: false,
            },
            autowidth: true,
        });

        viewModel = {
            CurrentPage: 1,
            PageSize: 10
        };
        loadQantasPointEarnedRate();

        //set width grid
        $(window).bind("resize",
            function() {
                $("#grdQantasPointEarnedRateGrid").setGridWidth($("#parentQantasPointEarnedRateGrid").width() - 10);
            }).trigger("resize");

        $('#grdQantasPointEarnedRateGrid').setGridParam({
            onPaging: function(pgButton) {
                var currentPage = $('#grdQantasPointEarnedRateGrid').getGridParam('page');
                var rowNum = $('#grdQantasPointEarnedRateGrid').getGridParam('rowNum');
                var sortName = $('#grdQantasPointEarnedRateGrid').getGridParam('sortname');
                var sortOrder = $('#grdQantasPointEarnedRateGrid').getGridParam("sortorder");

                if (pgButton == "next_grdQantasPointEarnedRateGridPager") {
                    currentPage = currentPage + 1;
                }
                if (pgButton == "prev_grdQantasPointEarnedRateGridPager") {
                    currentPage = currentPage - 1;
                }

                if (pgButton == "first_grdQantasPointEarnedRateGridPager") {
                    currentPage = 1;
                }

                if (pgButton == "last_grdQantasPointEarnedRateGridPager") {
                    currentPage = $('#grdQantasPointEarnedRateGrid').getGridParam('lastpage');
                }
                var p = $(this).jqGrid("getGridParam");
                if (pgButton == "records") {
                    var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                    rowNum = newRowNum;
                }

                if (pgButton == "user") {
                    var elTotalPage = $(p.pager).find("span[id*='grdQantasPointEarnedRateGridPager']")[0];
                    var totalPage =
                        elTotalPage
                            ? (parseInt(elTotalPage.innerText) || 0)
                            : 0; //get total page, default value equal 0 in case not found total page element.
                    var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                    if (newPage <= totalPage && newPage > 0) {
                        currentPage = newPage;
                    }
                }
                if (!viewModel) {
                    viewModel = {
                        CurrentPage: currentPage,
                        PageSize: rowNum,
                        SortName: sortName,
                        SortOrder: sortOrder
                    };
                } else {
                    viewModel.CurrentPage = currentPage;
                    viewModel.PageSize = rowNum;
                    viewModel.SortName = sortName;
                    viewModel.SortOrder = sortOrder;
                }

                loadQantasPointEarnedRate();
            },
            onSortCol: function (index, columnIndex, sortOrder) {
                var sortName = index;
                if (viewModel) {
                    viewModel.SortName = sortName;
                    viewModel.SortOrder = sortOrder;
                }
                loadQantasPointEarnedRate();
                return 'stop';
            }
        });
    }
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
});

$("#btnAdd").on("click",
    function() {
        EarnedRateDialog.AddEarnedRateDialog();
    });

onSaveExchangeRate = function () {
    EarnedRateDialog.SaveExchangeRate("", loadQantasPointEarnedRate);
}

onEditExchangeRate = function(id) {
    var rowData = $('#grdQantasPointEarnedRateGrid').jqGrid('getRowData', id);
    EarnedRateDialog.DisplayPointEarnedRateDialog(rowData.ExchangeRateTypeCode, "EDIT_EARNED", id);
}

onDeleteExchangeRate = function (exchangeRateId) {
    var confirmation = confirm("Are you sure to delete?");
    if (confirmation && exchangeRateId) {
        LG_ShowProgressMask();
        qantasPointEarnedRateService.DeleteQantasPointEarnedRate(exchangeRateId).done(function(message) {
            alert(message);
            loadQantasPointEarnedRate();
        }).fail(function(message) {
            LG_HideProgressMask();
            alert(message);
        });
    }
}


customIsDefaultFormatter = function (cellvalue, options, rowObject) {
    var html = "";
    if (rowObject.ExchangeRateTypeCode == "DEFAULT") {
        html += "<div style='text-align: center;'><input type='checkbox' disabled='disabled' checked/></div>";
    }
    return html;
};

customActionFormatter = function (cellvalue, options, rowObject) {
    var html = "";
    var editHtml = "<img class='icon' src='../Images/edit.png' data-id=" + rowObject.ID + " onclick='onEditExchangeRate(" + rowObject.ID + ")' id='edit_" + rowObject.ID + "'/>";
    var deleteHtml = "<img class='icon' src='../Images/delete.png' onclick='onDeleteExchangeRate(" + rowObject.ID + ")' id='delete_" + rowObject.ID + "'/>";
    if (rowObject.ExchangeRateTypeCode != "GROUP") {
        html = html + editHtml;
        if (rowObject.ExchangeRateTypeCode != "DEFAULT") {
            html = html + deleteHtml;
        }
    }
    return html;
};

loadQantasPointEarnedRate = function () {

    //Show progress mask
    LG_ShowProgressMask();

    //Searching declined transaction
    $.ajax({
        url: baseURL + "QantasProcessing/GetQantasPointEarnedRate",
        type: "POST",
        data: JSON.stringify(viewModel),
        headers: {
            "Content-Type": "application/json"
        }
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdQantasPointEarnedRateGrid')[0];
        $('#grdQantasPointEarnedRateGrid').jqGrid('clearGridData');
        grid.addJSONData(result);

        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
};


