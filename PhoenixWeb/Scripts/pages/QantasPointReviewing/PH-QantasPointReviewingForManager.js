﻿var searchQBR = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;

$(function () {

    $(window).bind("resize", function () {
        $("#grdQantasPointVerificationForManager").setGridWidth($("#QantasPointReviewingForManager").width() - 10);
    }).trigger("resize");

    $("#grdQantasPointVerificationForManager").jqGrid({
        url: "",
        colNames: [
            "Entry Date", "Business Type", "Description", "Point", "Month", "Year"
            , "MemberKey", "MemberId", "Trading Name", "QBR Number", "Created By", "Status", "Comment"
        ],
        colModel: [
            { key: false, width: 80, name: "EntryDate", index: "EntryDate", align: 'center', editable: false, sortable: true, formatter: 'date', formatoptions: { srcformat: "Y-m-d", newformat: "d/m/Y" } },
            {
                key: false,
                name: "BusinessType",
                index: "BusinessType",
                align: 'center',
                editable: false,
                width: 100,
                sortable: false
            },
            {
                key: false,
                name: "Description",
                index: "Description",
                align: 'left',
                editable: false,
                width: 170,
                sortable: true
            },
            { key: false, name: "Point", index: "Point", width: 55, align: 'right', editable: false, sortable: true },
            {
                key: false,
                name: "Month",
                index: "Month",
                align: 'center',
                width: 55,
                editable: false,
                sortable: true
            },
            {
                key: false, name: "Year", index: "Year",
                width: 55,
                align: 'center', editable: false, sortable: true
            },
            { key: false, name: "MemberKey", index: "MemberKey", align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "MemberId", index: "MemberId", width: 60, align: 'left', editable: false, sortable: true },
            { key: false, name: "TradingName", index: "TradingName", width: 160, align: 'left', editable: false, sortable: true },
            { key: false, name: "QBRNumber", index: "QBRNumber", width: 100, align: 'center', editable: false, sortable: true },
            { key: false, name: "CreatedBy", index: "CreatedBy", width: 100, align: 'left', editable: false, sortable: true },
            { key: false, name: "Status", index: "Status", width: 110, align: 'left', editable: false, sortable: true },
            { key: false, name: "Comment", index: "Comment", align: 'left', editable: false, sortable: true }

        ],
        pager: "#grdQantasPointVerificationForManagerPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#grdQantasPointVerificationForManager').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdQantasPointVerificationForManager').getGridParam('page');
            var rowNum = $('#grdQantasPointVerificationForManager').getGridParam('rowNum');
            var sortName = $('#grdQantasPointVerificationForManager').getGridParam('sortname');
            var sortOrder = $('#grdQantasPointVerificationForManager').getGridParam("sortorder");
            var selectedYear = $("#Year").val();
            var selectedMonth = $("#Month").val();
            var selectedBusinessType = $("#BusinessTypeID").val();

            if (pgButton == "next_grdQantasPointVerificationForManagerPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdQantasPointVerificationForManagerPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdQantasPointVerificationForManagerPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdQantasPointVerificationForManagerPager") {
                currentPage = $('#grdQantasPointVerificationForManager').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='grdQantasPointVerificationForManagerPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (!searchQBR) {
                searchQBR = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    Year: selectedYear,
                    Month: selectedMonth,
                    BusinessTypeID: selectedBusinessType,
                    SortName: sortName,
                    SortOrder: sortOrder
                };
            } else {
                searchQBR.CurrentPage = currentPage;
                searchQBR.PageSize = rowNum;
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;
            }

            if (isFiltering) {
                $("#textAdvanceSearch").trigger("keyup");
            } else {
                loadQantasPointFoManager();
            }
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchQBR) {
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;

                if (isFiltering) {
                    $("#textAdvanceSearch").trigger("keyup");
                } else {
                    loadQantasPointFoManager();
                }
            }
            return 'stop';
        }
    });


    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
}); // end of document ready


$("#search").off("click").on("click",
    function () {
        var selectedYear = $("#Year").val();
        var selectedMonth = $("#Month").val();
        var selectedBusinessType = $("#BusinessTypeID").val();
        searchQBR = {
            Year: selectedYear,
            Month: selectedMonth,
            BusinessTypeID: selectedBusinessType,
            CurrentPage: 1,
            PageSize: 10
        };

        loadQantasPointFoManager();
    });


onManagerRejectQantasPointReward = function () {
    var dataGridCount = $('#grdQantasPointVerificationForManager').jqGrid('getGridParam', 'reccount');
    if (dataGridCount > 0) {
        var reason = prompt("Please Add a reason:", '');
        if (reason.trim()) {
            LG_ShowProgressMask();
            //add rejected reason of manager
            searchQBR.ManagerRejectedComment = reason;
            $.ajax({
                url: baseURL + "QantasProcessing/ManagerRejectQantasPoint",
                type: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(searchQBR),
                success: function (res) {
                    if (res.IsError) {
                        alert(res.ErrorMessage);
                        loadQantasPointFoManager();
                    } else {
                        loadQantasPointFoManager();
                        alert("The selected QBR point list was return to Sale/Finance to check again.");
                    }
                }
            });
        } else {
            alert("Please enter a rejection reason.");
            return;
        }
    } else {
        alert("No record is rejected. You can search with other options.");
    }
};


loadQantasPointFoManager = function () {

    //Show progress mask
    LG_ShowProgressMask();

    //Searching declined transaction
    $.ajax({
        url: baseURL + "QantasProcessing/GetQBRPointForManager",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchQBR)
    }).done(function (result) {
        //Loading data to grid
        var grid = $('#grdQantasPointVerificationForManager')[0];
        $('#grdQantasPointVerificationForManager').jqGrid('clearGridData');
        grid.addJSONData(result);
        //the notice flag is using advance search
        isFiltering = false;

        //handle display the search filter after search QBR
        var dataGridCount = $('#grdQantasPointVerificationForManager').jqGrid('getGridParam', 'reccount');
        if (dataGridCount > 0) {
            $("#filterSection").show();
        } else {
            $("#filterSection").hide();
        }

        $("#totalTransaction").text(result.records);
        $("#totalApproval").text(result.approval);
        $("#totalRejected").text(result.rejected);
        $("#totalPoint").text(result.totalPoint || 0);

        LG_HideProgressMask();
    }).fail(function (err) {
        console.log(err);
        LG_HideProgressMask();
    });
};


$("#textAdvanceSearch").keyup(function () {
    var searchText = $("#textAdvanceSearch").val();
    if (searchText.trim().length > 2) {
        searchQBR.SearchText = searchText;
        //check in the first time use filter search then reset CurrentPage value
        if (!isFiltering) {
            searchQBR.CurrentPage = 1;
            searchQBR.PageSize = 10;
        }

        LG_ShowProgressMask();
        $.ajax({
            url: baseURL + "QantasProcessing/GetQBRPointForManagerByAdvanceSearch",
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(searchQBR),
            success: function (result) {
                if (result.IsError) {
                    alert(result.ErrorMessage);
                    LG_HideProgressMask();
                } else {
                    var grid = $('#grdQantasPointVerificationForManager')[0];
                    $('#grdQantasPointVerificationForManager').jqGrid('clearGridData');
                    grid.addJSONData(result);

                    //the notice flag is using advance search
                    isFiltering = true;
                    LG_HideProgressMask();
                }
            }
        });
    } else {
        if (isFiltering) {
            loadQantasPointFoManager();
        }
    }
});


sendQBRToQantas = function () {
    var dataGridCount = $('#grdQantasPointVerificationForManager').jqGrid('getGridParam', 'reccount');
    if (dataGridCount > 0) {
        var confirms = confirm("Are you sure to send to Qantas?");
        if (confirms) {
            LG_ShowProgressMask();
            $.ajax({
                url: baseURL + "QantasProcessing/ManagerSendQBRToQantas",
                type: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(searchQBR),
                success: function (res) {
                    if (res.IsError) {
                        LG_HideProgressMask();
                        alert(res.ErrorMessage);
                    } else {
                        loadQantasPointFoManager();
                        alert("The selected QBR point list is ready to send to Qantas.");
                    }
                }
            });
        }
    } else {
        alert("No record is approved. You can search with other option");
    }
}

ExportFile = function () {
    if (searchQBR.hasOwnProperty("Month")) {
        window.location.href =
            baseURL +
            "QantasProcessing/GetQBRExportFileForManager?month=" +
            searchQBR.Month +
            "&year=" +
            searchQBR.Year +
            "&businesTypeId=" +
            searchQBR.BusinessTypeID;
    }
}
