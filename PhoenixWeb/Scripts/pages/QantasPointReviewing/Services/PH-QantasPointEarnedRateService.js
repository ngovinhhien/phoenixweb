﻿
(function (qantasPointEarnedRateService, $, undefined) {
    //function call to get all ExchangeRate Type
    qantasPointEarnedRateService.GetExchangeRateTypeList = function (includedGroupType) {
        var deferred = $.Deferred();
        includedGroupType = includedGroupType ? true : false;
        $.ajax({
            url: baseURL + "QantasProcessing/GetExchangeRateTypeList",
            type: "GET",
            data: { includedGroupType: includedGroupType },
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve(result.Data);
                }
            }
        });
        return deferred.promise();
    }

    qantasPointEarnedRateService.DeleteQantasPointEarnedRate = function (exchangeRateId) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "QantasProcessing/DeleteQantasPointEarnedRate",
            type: "POST",
            data: { id: exchangeRateId },
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve("The QBR Earned Rate has been deleted successful.");
                }
            }
        });
        return deferred.promise();
    }

    qantasPointEarnedRateService.SaveQantasPointEarnedRate = function (url, formData) {
        var deferred = $.Deferred();
        var saveURL = "";
        if (url) {
            saveURL = url;
        } else {
            saveURL = "QantasProcessing/UpdateQantasPointEarnedRate";
        }

        $.ajax({
            url: baseURL + saveURL,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(formData),
            success: function (result) {
                if (result.IsError) {
                    deferred.reject(result.ErrorMessage);
                } else {
                    deferred.resolve("The QBR Earned Rate has been saved successful.");
                }
            }
        });
        return deferred.promise();
    }

    qantasPointEarnedRateService.CheckEarnedRateDateExisting = function (startDate, endDate, typeCode) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "QantasProcessing/GetEarnedRateDateExisting",
            type: "GET",
            data: { startDate: startDate, endDate: endDate, typeCode: typeCode },
            success: function (result) {
                if (result.Data.length > 0) {
                    deferred.resolve(
                        "Warning: this Qantas Earned Rate's period conflict with the Earned Rate [" + result.Data.join() + "]'s period." +
                        " If you don't update Start Date and End Date to resolve the conflict, system will get the Earned Rate based on the effected highest  earned rate.");
                } else {
                    deferred.resolve("");
                }
            }
        });
        return deferred.promise();
    }

}(window.qantasPointEarnedRateService = window.qantasPointEarnedRateService || {}, jQuery));