﻿

$(function () {

    var urlOrigin = Util.PH_UpdateURL();
    var fileData = [];

    $(window).bind("resize", function () {
        $("#grdQantasPointUploadGrid").setGridWidth($("#QantasPointUpload").width() - 10);
    }).trigger("resize");

    $("#grdQantasPointUploadGrid").jqGrid({
        url: "",
        colNames: [
            "Entry Date", "Description", "Point", "Month", "Year"
            , "MemberId", "Created By"
        ],
        colModel: [
            { key: false, name: "EntryDateTime", index: "EntryDateTime", width: 80, align: 'center', editable: false, sortable: false, formatter: 'date', formatoptions: { srcformat: "Y-m-d", newformat: "d/m/Y" } },
            {
                key: false,
                name: "Description",
                index: "Description",
                width: 230,
                align: 'left',
                editable: false,
                sortable: false
            },
            { key: false, name: "Point", index: "Point", width: 80, align: 'right', editable: false, sortable: false },
            {
                key: false,
                name: "Month",
                index: "Month",
                width: 80,
                align: 'center',
                editable: false,
                sortable: false
            },
            { key: false, name: "Year", index: "Year", width: 80, align: 'center', editable: false, sortable: false },
            { key: false, name: "MemberId", index: "MemberId", width: 150, align: 'left', editable: false, sortable: false },
            { key: false, name: "CreatedByUser", index: "CreatedByUser", align: 'left', editable: false, sortable: false }
        ],
        pager: "#grdQantasPointUploadGridPager",
        loadui: "disable",
        datatype: 'json',
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            repeatitems: false
        },
        autowidth: true,
        loadonce: true,
    });


    $("#BusinessTypeId").on("change",
        function () {
            var businessTypeId = $("#BusinessTypeId").val();
            $.ajax({
                url: urlOrigin + "QantasProcessing/GetLoyaltyPromotion",
                data: { businessTypeId: businessTypeId },
                type: "GET",
                success: function (result) {
                    $("#LoyaltyPromotionId").empty();
                    $.each(result.Data, function (i, el) {
                        $('#LoyaltyPromotionId').append(new Option(el.Text, el.Value));
                    })
                    $("#LoyaltyPromotionId").trigger("change");
                }
            });
        });

    $("#btnLoadFile").on("click", function () {
        var btnChooseFile = $("#btnChooseFile")[0];
        var file = btnChooseFile.files[0];
        if (file) {
            var fileExtension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //validate excel file 
            if ("xlsx".indexOf(fileExtension) < 0) {
                //Show error massage
                alert('This function only allows to import "xlsx" format.');

                //empty grid data
                $('#grdQantasPointUploadGrid').jqGrid('clearGridData');
                $("#btnSave").attr("disabled", true);
                return false;
            }

            //file data 
            var formData = new FormData();
            formData.append("File", file);
            LG_ShowProgressMask();

            $.ajax({
                url: urlOrigin + "QantasProcessing/UploadQantasPointFile",
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.IsError) {
                        alert(result.ErrorMessage);
                        $('#grdQantasPointUploadGrid').jqGrid('clearGridData');
                        $("#btnSave").attr("disabled", true);
                    } else {
                        //set data display on grid
                        var grid = $('#grdQantasPointUploadGrid')[0];
                        $('#grdQantasPointUploadGrid').jqGrid('clearGridData');
                        $("#grdQantasPointUploadGrid").setGridParam({ datatype: 'json' }).trigger('reloadGrid');//set grid type to json before add data
                        var data = {
                            rows: result.Data
                        }
                        grid.addJSONData(data);
                        $("#grdQantasPointUploadGrid").setGridParam({ datatype: 'local' }).trigger('reloadGrid'); //set grid type to local offline

                        fileData = result.Data;
                        $("#btnSave").attr("disabled", false);
                    }

                    LG_HideProgressMask();
                }
            });
        } else {
            alert("Please select the QBR point list.");
        }
    });


    $("#btnSave").off("click").on("click",
        function () {
            $("#btnSave").attr("disabled", true);
            var model = {
                LoyaltyPromotionId: $("#LoyaltyPromotionId").val(),
                EntryList: JSON.stringify(fileData)
            };
            LG_ShowProgressMask();
            $.ajax({
                url: urlOrigin + "QantasProcessing/ImportQantasPointFile",
                type: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(model),
                success: function (result) {
                    if (result.IsError) {
                        alert(result.ErrorMessage);
                        $("#btnSave").attr("disabled", false)
                    } else {
                        $('#grdQantasPointUploadGrid').jqGrid('clearGridData');
                        alert("The file is imported successful.");
                        $("#btnChooseFile").val("");
                        $("#btnSave").attr("disabled", true)
                    }
                    LG_HideProgressMask();
                }

            });

        });
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
})


