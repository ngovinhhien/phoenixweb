﻿var searchQBR = {};
var baseURL = Util.PH_UpdateURL();
var isFiltering = false;

$(function () {

    $(window).bind("resize", function () {
        $("#grdQantasPointFileVerification").setGridWidth($("#qantasPointFileReviewing").width() - 10);
    }).trigger("resize");

    $("#grdQantasPointFileVerification").jqGrid({
        url: "",
        colNames: [
            "Entry Date", "Business Type", "Description", "Point", "Month", "Year"
            , "MemberKey", "MemberId", "Trading Name", "QBR Number", "Review By Sale", "Review By Manager", "Status", "Comment"
        ],
        colModel: [
            { key: false, name: "EntryDateTime", index: "EntryDateTime", width: 80, align: 'center', editable: false, sortable: true, formatter: 'date', formatoptions: { srcformat: "Y-m-d", newformat: "d/m/Y" } },
            {
                key: false,
                name: "BusinessType",
                index: "BusinessType",
                width: 100,
                align: 'center',
                editable: false,
                sortable: false
            },
            {
                key: false,
                name: "Description",
                index: "Description",
                align: 'left',
                width: 170,
                editable: false,
                sortable: true
            },
            { key: false, name: "Points", index: "Points", width: 55, align: 'right', editable: false, sortable: true },
            {
                key: false,
                name: "MonthPeriod",
                index: "MonthPeriod",
                width: 55,
                align: 'center',
                editable: false,
                sortable: true
            },
            { key: false, name: "YearPeriod", index: "YearPeriod", width: 55, align: 'center', editable: false, sortable: true },
            { key: false, name: "Member_key", index: "Member_key", width: 60, align: 'left', editable: false, sortable: false, hidden: true },
            { key: false, name: "MemberId", index: "MemberId", width: 60, align: 'left', editable: false, sortable: true },
            { key: false, name: "TradingName", index: "TradingName", width: 160, align: 'left', editable: false, sortable: true },
            { key: false, name: "QBRNumber", index: "QBRNumber", width: 100, align: 'center', editable: false, sortable: true },
            { key: false, name: "ReviewBySale", index: "ReviewBySale", width: 100, align: 'left', editable: false, sortable: true },
            { key: false, name: "ReviewByManager", index: "ReviewByManager", width: 100, align: 'left', editable: false, sortable: true },
            { key: false, name: "Status", index: "Status", width: 110, align: 'left', editable: false, sortable: true },
            { key: false, name: "Comment", index: "Comment", align: 'left', editable: false, sortable: true },
        ],
        pager: "#grdQantasPointFileVerificationPager",
        rowNum: 10,
        loadui: "disable",
        rowList: [10, 20, 50, 100],
        height: "100%",
        viewrecords: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $('#grdQantasPointFileVerification').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdQantasPointFileVerification').getGridParam('page');
            var rowNum = $('#grdQantasPointFileVerification').getGridParam('rowNum');
            var sortName = $('#grdQantasPointFileVerification').getGridParam('sortname');
            var sortOrder = $('#grdQantasPointFileVerification').getGridParam("sortorder");
            var selectedYear = $("#Year").val();
            var selectedMonth = $("#Month").val();

            if (pgButton == "next_grdQantasPointFileVerificationPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdQantasPointFileVerificationPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdQantasPointFileVerificationPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdQantasPointFileVerificationPager") {
                currentPage = $('#grdQantasPointFileVerification').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='grdQantasPointFileVerificationPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (!searchQBR) {
                searchQBR = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    Year: selectedYear,
                    Month: selectedMonth,
                    SortName: sortName,
                    SortOrder: sortOrder
                };
            } else {
                searchQBR.CurrentPage = currentPage;
                searchQBR.PageSize = rowNum;
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;
            }

            if (isFiltering) {
                var searchText = $("#textAdvanceSearch").val();
                loadQantasPointByAdvanceSearch(searchText);
            } else {
                loadQantasPointFileReward();
            }
        },
        onSortCol: function (index, columnIndex, sortOrder) {
            var sortName = index;
            if (searchQBR) {
                searchQBR.SortName = sortName;
                searchQBR.SortOrder = sortOrder;

                if (isFiltering) {
                    var searchText = $("#textAdvanceSearch").val();
                    loadQantasPointByAdvanceSearch(searchText);
                } else {
                    loadQantasPointFileReward();
                }
            }
            return 'stop';
        }
    });


    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
}); // end of document ready

$("#search").off("click").on("click",
    function () {
        var QantasFileID = $("#QantasFileID").val();
        if (!QantasFileID || QantasFileID < 1) {
            return false;
        }
        searchQBR = {
            QantasFileID: QantasFileID,
            CurrentPage: 1,
            PageSize: 10,
            SearchText: ""
        };

        loadQantasPointFileReward().done(function () {
            //handle display the search filter after search QBR
            var dataGridCount = $('#grdQantasPointFileVerification').jqGrid('getGridParam', 'reccount');
            if (dataGridCount > 0) {
                $("#filterSection").show();
            } else {
                $("#filterSection").hide();
            }
        });
    });

$("#textAdvanceSearch").keyup(function () {
    var searchText = $("#textAdvanceSearch").val();
    loadQantasPointByAdvanceSearch(searchText);
});

loadQantasPointByAdvanceSearch = function (searchText) {
    if (searchText.trim().length > 2) {
        searchQBR.SearchText = searchText;
        //check in the first time use filter search then reset CurrentPage value
        if (!isFiltering) {
            searchQBR.CurrentPage = 1;
            searchQBR.PageSize = 10;
        }

        loadQantasPointFileReward().done(function () {
            isFiltering = true;
        });

    } else {
        if (isFiltering) {
            searchQBR.SearchText = "";
            loadQantasPointFileReward().done(function () {
                isFiltering = false;
            });
        }
    }
};

$("#Month").off("change").on("change",
    function() {
        LoadQantasFileName();
    });

$("#Year").off("change").on("change",
    function () {
        LoadQantasFileName();
    });

LoadQantasFileName = function() {
    var selectedYear = $("#Year").val();
    var selectedMonth = $("#Month").val();
    $('#QantasFileID').html("");
    qantasPointFileReviewingService.LoadQantasFileName(baseURL, selectedYear, selectedMonth).done(function (result) {
        let i = 0;
        for (i; i < result.length; i++) {
            $('#QantasFileID').append(new Option(result[i].Text, result[i].Value, true));
        }
    });
}

loadQantasPointFileReward = function () {
    var deferred = $.Deferred();
    //Show progress mask
    LG_ShowProgressMask();

    //Searching declined transaction
    $.ajax({
        url: baseURL + "QantasProcessing/GetQantasPointFileContent",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: JSON.stringify(searchQBR)
    }).done(function(result) {
        //Loading data to grid
        var grid = $('#grdQantasPointFileVerification')[0];
        $('#grdQantasPointFileVerification').jqGrid('clearGridData');
        grid.addJSONData(result);

        LG_HideProgressMask();
        return deferred.resolve();
    }).fail(function(err) {
        console.log(err);
        LG_HideProgressMask();
        return deferred.reject();
        });
    return deferred.promise();
};


ExportFile = function () {
    if (searchQBR.hasOwnProperty("QantasFileID")) {
        window.location.href =
            baseURL +
            "QantasProcessing/GetQBRExportFileReviewing?fileId=" +
            searchQBR.QantasFileID;
    }
}