﻿(function (qantasPointFileReviewingService, $, undefined) {
    //function call to get all ExchangeRate Type
    qantasPointFileReviewingService.LoadQantasFileName = function (baseURL, year, month) {
        var deferred = $.Deferred();
        $.ajax({
            url: baseURL + "QantasProcessing/GetQantasFileNameBaseOnYearAndMonth",
            type: "POST",
            data: {
                year: year,
                month: month
            },
            success: function (result) {
                deferred.resolve(result);
            }
        });
        return deferred.promise();
    }
}(window.qantasPointFileReviewingService = window.qantasPointFileReviewingService || {}, jQuery));