﻿//@class Util
function Util() { }
//validate decimal number
PH_DecimalValidation = function (existingData, newKey, selectionStart, decimalNumber) {
    if ((newKey != 46 || existingData.indexOf('.') != -1) &&
        ((newKey < 48 || newKey > 57) &&
            (newKey != 0 && newKey != 8)) &&
        newKey != 45) {
        return false;
    }

    var text = existingData;
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > decimalNumber) &&
        (newKey != 0 && newKey != 8) &&
        (selectionStart >= text.length - decimalNumber)) {
        return false;
    }

    if ((newKey == 45) && ((text.indexOf('-') != -1) || (text.indexOf('-') == -1 && selectionStart != 0))) {
        return false;
    }

    return true;
};

PH_UnsignDecimalValidation = function (existingData, newKey, selectionStart, decimalNumber) {
    if ((newKey != 46 || existingData.indexOf('.') != -1) &&
        ((newKey < 48 || newKey > 57) &&
            (newKey != 0 && newKey != 8))) {
        return false;
    }

    var text = existingData;
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > decimalNumber) &&
        (newKey != 0 && newKey != 8) &&
        (selectionStart >= text.length - decimalNumber)) {
        return false;
    }

    return true;
};

PH_SignDecimalValidation = function (existingData, newKey, selectionStart, decimalNumber) {
    if ((newKey != 46 || existingData.indexOf('.') != -1) &&
        ((newKey < 48 || newKey > 57) &&
            (newKey != 0 && newKey != 8)) &&
        newKey != 45) {
        return false;
    }

    var text = existingData;
    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > decimalNumber) &&
        (newKey != 0 && newKey != 8) &&
        (selectionStart >= text.length - decimalNumber)) {
        return false;
    }


    if ((newKey == 45) && ((text.indexOf('-') != -1) || (text.indexOf('-') == -1 && selectionStart != 0))) {
        return false;
    }

    if (text.indexOf('-') == -1 && newKey != 45) {
        return false;
    }
    return true;
};

PH_BuildFullContentWithKeyPressEvent = function (currentData, key, addedIndex) {
    var fullstr = [currentData.slice(0, addedIndex), key, currentData.slice(addedIndex)].join('');
    if (fullstr.indexOf(".") == fullstr.length - 1) {
        fullstr = fullstr + "0";
    }
    //Fix issue PHW-629
    if (fullstr == "-") {
        fullstr = fullstr + "0";
    }
    return fullstr;
};

PH_BuildFullContentWithKeyPressEventWithoutValidation = function (currentData, key, addedIndex) {
    var fullstr = [currentData.slice(0, addedIndex), key, currentData.slice(addedIndex)].join('');
    return fullstr;
};

validateAmount = function (fieldname, minValue, maxValue) {
    var fieldValue = $(fieldname).val();

    if (!fieldValue) {
        return false;
    }

    var regularExpress = "^(\-?[0-9]+(.[0-9]{1,4})?)$";
    var re = new RegExp(regularExpress);

    if (fieldValue < minValue || fieldValue > maxValue || !re.test(fieldValue)) {
        var message = "Inputted amount must be between " + minValue + " to " + maxValue + " and maximum 4 digits after decimal points.";
        $(fieldname).attr("title", message);

        var errorIcon = $(fieldname).parent().find(".errorIcon");
        if (errorIcon && errorIcon.length) {
            $(errorIcon).attr("title", message);
            $(errorIcon).show();
        }
        $(fieldname).addClass("errorClass");
        return true;
    }

    return false;
};

LG_ShowProgressMask = function (message, image) {
    if (!message) {
        message = "Loading ...";
    }

    if (!image) {
        image = "";
    }

    //Check this progress bar is available or not
    var currentProgressBar = $(".loadingoverlay");
    if (!currentProgressBar.length) {
        $.LoadingOverlay("show",
            {
                image: image,
                text: message
            });
    }
};

LG_HideProgressMask = function () {
    //Check this progress bar is available or not
    var currentProgressBar = $(".loadingoverlay");
    if (currentProgressBar.length) {
        $.LoadingOverlay("hide");
    }
};

Util.LG_BuildDateTimeWithInternationalFormat = function () {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return yyyy + "-" + mm + "-" + dd;
}

LG_GetDefaultValueForCommissionRateByBusinessType = function (businessTypeID, subBusinessType, IsDollarForDebitRate, IsDollarForQantasRate) {
    var commissionRate = {
        DebitRate: 0,
        Master: 0,
        MasterDebit: 0,
        Visa: 0,
        VisaDebit: 0,
        Amex: 0,
        Diners: 0,
        UnionPay: 0,
        Qantas: 0,
        Alipay: 0,
        Zip: 0,
        JCB: 0
    };

    if (!businessTypeID) {
        return commissionRate;
    }

    switch (businessTypeID) {
        case "1": //TaxiEpay
            break;
        case "18": //Live Eftpos
            if (IsDollarForDebitRate) {
                commissionRate.DebitRate = -0.5;
            } else {
                commissionRate.DebitRate = 0;
            }

            commissionRate.Master = -2.20;
            commissionRate.Visa = -2.2;
            commissionRate.Amex = -2.5;
            commissionRate.Diners = -2.5;
            commissionRate.UnionPay = -2.5;
            break;
        case "19": //DirectLiveEftpos
            commissionRate.DebitRate = -0.5;
            commissionRate.Master = -2.20;
            commissionRate.Visa = -2.2;
            commissionRate.Amex = -2.5;
            commissionRate.Diners = -2.5;
            commissionRate.UnionPay = -2.5;
            break;
        case "20": //TaxiPro
            break;
        case "21": //TaxiApp
            break;
        case "22": //LiveEftpos black
            commissionRate.DebitRate = -2.2;
            commissionRate.Master = -2.59;
            commissionRate.Visa = -2.59;
            commissionRate.Amex = -2.59;
            commissionRate.Diners = -2.59;
            commissionRate.UnionPay = -2.59;
            break;
        case "23": //LiveEftpos black for taxi
            commissionRate.DebitRate = 1;
            commissionRate.Master = 1;
            commissionRate.Visa = 1;
            commissionRate.Amex = 1;
            commissionRate.Diners = 1;
            commissionRate.UnionPay = 1;
            break;
        case "24": //Live limo
            break;
        case "25": //Glide
            break;
        case "26": //Live Eftpos Integrated
            commissionRate.DebitRate = -0.5;
            commissionRate.Master = -2.20;
            commissionRate.Visa = -2.2;
            commissionRate.Amex = -2.5;
            commissionRate.Diners = -2.5;
            commissionRate.UnionPay = -2.5;
            break;
        case "27": //Live Eftpos Black Integrated
            commissionRate.DebitRate = -2.2;
            commissionRate.Master = -2.59;
            commissionRate.Visa = -2.59;
            commissionRate.Amex = -2.59;
            commissionRate.Diners = -2.59;
            commissionRate.UnionPay = -2.59;
            break;
        case "28": //LiveSMS
            break;
        case "29": //CSA Integrated
            commissionRate.DebitRate = 0;
            commissionRate.Master = 0;
            commissionRate.MasterDebit = 0;
            commissionRate.Visa = 0;
            commissionRate.VisaDebit = 0;
            commissionRate.Amex = 0;
            commissionRate.Diners = 0;
            commissionRate.UnionPay = 0;
            commissionRate.Zip = 0;
            commissionRate.Alipay = 0;
            commissionRate.Qantas = 0;
            commissionRate.JCB = 0;
            break;
    }
    switch (subBusinessType) {
        case "eCommerce":
            if (businessTypeID == "18" || businessTypeID == "26") {
                commissionRate.DebitRate = "";
                commissionRate.Master = -1.95;
                commissionRate.Visa = -1.95;
                commissionRate.Amex = -1.95;
                commissionRate.Diners = -1.95;
                commissionRate.UnionPay = -1.95;
                commissionRate.Zip = "";
                commissionRate.Alipay = "";
                commissionRate.Qantas = "";
                commissionRate.JCB = -1.95;
            }
            else if (businessTypeID == "22" || businessTypeID == "27") {
                commissionRate.DebitRate = "";
                commissionRate.Master = -2.65;
                commissionRate.Visa = -2.65;
                commissionRate.Amex = -2.65;
                commissionRate.Diners = -2.65;
                commissionRate.UnionPay = -2.65;
                commissionRate.Zip = "";
                commissionRate.Alipay = "";
                commissionRate.Qantas = "";
                commissionRate.JCB = -2.65;
            }
            break;
        case "Live local":
            commissionRate.DebitRate = "";
            commissionRate.Master = -2.75;
            commissionRate.Visa = -2.75;
            commissionRate.Amex = -2.75;
            commissionRate.Diners = -2.75;
            commissionRate.UnionPay = -2.75;
            commissionRate.Zip = "";
            commissionRate.Alipay = "";
            commissionRate.Qantas = "";
            commissionRate.JCB = -2.75;
            break;
        case "Live taxi black VIC":
            commissionRate.DebitRate = 1;
            commissionRate.Master = 1;
            commissionRate.Visa = 1;
            commissionRate.Amex = 0.5;
            commissionRate.Diners = 0.5;
            commissionRate.UnionPay = 0.5;
            commissionRate.Zip = "";
            commissionRate.Alipay = "";
            commissionRate.Qantas = "";
            commissionRate.JCB = "";
            break;
        default:
            break;
    }

    return commissionRate;
}

LG_GetDefaultValueForTranFeeByBusinessType = function (businessTypeID) {
    var TranFee = {
        DebitRate: 0,
        Master: 0,
        MasterDebit: 0,
        Visa: 0,
        VisaDebit: 0,
        Amex: 0,
        Diners: 0,
        UnionPay: 0,
        Qantas: 0,
        Alipay: 0,
        Zip: 0,
        JCB: 0
    };

    if (!businessTypeID) {
        return TranFee;
    }

    switch (businessTypeID) {
        case "29": //CSA Integrated
            TranFee.DebitRate = 0;
            TranFee.Master = 0;
            TranFee.MasterDebit = 0;
            TranFee.Visa = 0;
            TranFee.VisaDebit = 0;
            TranFee.Amex = 0;
            TranFee.Diners = 0;
            TranFee.UnionPay = 0;
            TranFee.Zip = 0;
            TranFee.Alipay = 0;
            TranFee.Qantas = 0;
            TranFee.JCB = 0;
            break;
    }

    return TranFee;
};

buildId = function () {
    var now = new Date();
    var array = [
        now.getDate(),
        now.getHours(),
        now.getMinutes(),
        now.getSeconds()
    ];
    return parseInt("-" + array.join(""));
};

Util.CommissionRateValidationRule = function (fieldname, businesstype, ratename, placement, wontBeRemoved) {

    var val1 = businesstype;
    // In case another function already show the message, still keep message of these.
    $(fieldname).css("background-color", "white");
    $(fieldname).tooltip('destroy');

    if (wontBeRemoved) {
        return;
    }

    $(fieldname).removeClass("errorClass");
    $(fieldname).attr("data-original-title", "");
    $(fieldname).attr("title", "");

    var errorIcon = $(fieldname).parent().find(".errorIcon");
    if (errorIcon && errorIcon.length) {
        $(errorIcon).attr("title", "");
        $(errorIcon).hide();
    }

    var warningIcon = $(fieldname).parent().find(".warningIcon");
    if (warningIcon && warningIcon.length) {
        $(warningIcon).attr("title", "");
        $(warningIcon).hide();
    }

    var fieldValue = $(fieldname).val();

    if (fieldValue == 0) {
        return;
    }

    //Warning when value is positive
    if (fieldValue > 0) {
        var message = ratename + " - if you enter positive value customer will get paid";
        switch (val1) {
            case "20":
                var warningIconTitle = $(fieldname).parent().find(".warningIcon");
                if (warningIconTitle && warningIconTitle.length) {
                    $(warningIconTitle).attr("title", message);
                    $(warningIconTitle).show();
                }
                break;

        }
    }
};

Util.PH_UpdateURL = function () {
    var iisPath = $("#urlHidden").val().trim();
    if (iisPath != "/") {
        return window.location.origin + iisPath;
    } else {
        return window.location.origin + '/';
    }
};

Util.MoveToTopOfPage = function () {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(document.body).offset().top
    },
        500);
};

Util.ParseToArrayObject = function (jsonArray) {
    if (typeof jsonArray != "string") {
        return jsonArray;
    }

    return JSON.parse(jsonArray);
};

Date.prototype.toObject = function () {
    var YY = this.getFullYear(),
        MM = this.getMonth() + 1,
        DD = this.getDate(),
        hh = this.getHours(),
        mm = this.getMinutes(),
        ss = this.getSeconds(),
        ms = this.getMilliseconds();
    if (MM < 10) MM = '0' + MM;
    if (DD < 10) DD = '0' + DD;
    if (mm < 10) mm = '0' + mm;
    if (hh < 10) hh = '0' + hh;
    if (ss < 10) ss = '0' + ss;
    if (ms < 10) ms = '00' + ms;
    else if (ms < 100) ms = '0' + ms;
    return { y: YY, M: MM, d: DD, h: hh, m: mm, s: ss, ms: ms };
};

Date.prototype.FormatFullDateTime = function () {
    var obj = this.toObject();
    return obj.d + '/' + obj.M + '/' + obj.y + ' ' + obj.h + ':' + obj.m + ':' + obj.s + '.' + obj.ms;
};


Date.prototype.FormatDateTime = function () {
    var obj = this.toObject();
    return obj.d + '/' + obj.M + '/' + obj.y + ' ' + obj.h + ':' + obj.m + ':' + obj.s;
};


Date.prototype.FormatDate = function () {
    var obj = this.toObject();
    return obj.d + '/' + obj.M + '/' + obj.y;
};

Date.prototype.FormatTime = function () {
    var obj = this.toObject();
    return obj.h + ':' + obj.m;
};


////////////////Validate///////////////

Util.disabledStartDatePicker = function (date, isUsed, endDate) {
    // validate in case used the Exchange Rate
    if (isUsed == "true") {
        return false;
    }

    //validate in case start date > today and < end date
    var today = new Date();
    if (today > date || date > endDate) {
        return false;
    } else {
        return [true];
    }
};

Util.disabledEndDateFollowStartDate = function (date, startDate) {
    //validate in case enddate > startdate 
    if (date > startDate) {
        return [true];
    } else {
        return false;
    }
};

Util.disabledStartDateFollowEndDate = function (date, endDate) {
    //validate in case enddate < startdate 
    if (endDate > date) {
        return [true];
    } else {
        return false;
    }
};

Util.disabledDateFollowOtherDate = function (firstDate, secondDate) {
    //validate in case firstDate >= secondDate
    if (firstDate >= secondDate) {
        return [true];
    } else {
        return false;
    }
};

Util.CheckStartDateFollowEndDate = function (startDateElementName, endDateElementName, errorElementName) {
    var startdate = new Date($("#" + startDateElementName).val());
    var enddate = new Date($("#" + endDateElementName).val());

    if (isNaN(startdate)) {
        $("#" + errorElementName).text("The Start Date format incorrect (dd/mm/yyyy hh:mm:ss AM|PM).");
        $("#" + startDateElementName).focus();
        return false;
    }

    if (isNaN(enddate)) {
        $("#" + errorElementName).text("The End Date format incorrect (dd/mm/yyyy hh:mm:ss AM|PM).");
        $("#" + endDateElementName).focus();
        return false;
    }

    if (startdate > enddate) {
        $("#" + errorElementName).text("The Start Date must be smaller than End Date.");
        $("#" + startDateElementName).focus();
        return false;
    }

    return true;
};

Util.ConvertStringToBool = function (value) {

    switch (typeof value) {
        case "string":
            value = value.trim().toLowerCase();
            if (value === "true") {
                return true;
            }
            return false;
        case "boolean":
            return value;
        default:
            return false;
    }
};

Util.MemberBusinessType = {
    TaxiEPay: "1",
    LiveEftPos: "18",
    DirectLiveEftPos: "19",
    TaxiPro: "20",
    TaxiApp: "21",
    LiveEftPosBlack: "22",
    LiveEftPosBlackForTaxi: "23",
    LiveLimo: "24",
    Glide: "25",
    LiveEftPosIntegrated: "26",
    LiveEftPosBlackIntegrated: "27",
    LiveSMS: "28"
};

Util.PaymentType = {
    BankTransfer: "1",
    Cash: "2"
};

Util.HandleDragDialog = function (event, ui) {
    var scrollTopPx = $(document).scrollTop();
    var positionUI = ui.position;
    positionUI.top = positionUI.top - scrollTopPx;
    $(this).closest(".ui-dialog").css("top", positionUI.top);
}

Util.MessageDialog = function (message, title, additionalFunc) {
    //remove el before build new popup
    $("#messagePopup").remove();

    var html = "";
    if (Array.isArray(message)) {
        message.forEach(function (val) {
            html += "<p>" + val + "</p>";
        });
    } else {
        html = "<p>" + message + "</p>";
    }

    $('<div id="messagePopup"></div>').appendTo('body')
        .html("<div style='word-break: break-word;'>" + html + "</div>")
        .dialog({
            modal: true,
            title: title || "Message",
            width: '400px',
            resizable: false,
            drag: function (event, ui) {
                Util.HandleDragDialog(event, ui);
            },
            buttons: {
                Cancel: {
                    click: function () {
                        $(this).dialog("close");
                        if (additionalFunc) {
                            additionalFunc();
                        }
                    },
                    "class": "btn btn-default",
                    text: "OK"
                }
            }
        });
};

Util.ConfirmDialog = function (message, title, customizeLabelYesButton, customizeLabelNoButton) {
    var deferred = $.Deferred();
    //remove el before build new popup
    $("#confirmPopup").remove();


    if (!customizeLabelYesButton) {
        customizeLabelYesButton = "Yes";
    }

    if (!customizeLabelNoButton) {
        customizeLabelNoButton = "No";
    }

    $('<div id="confirmPopup"></div>').appendTo('body')
        .html("<div style='word-break: break-word;'>" + message + "</div>")
        .dialog({
            modal: true,
            title: title || "Confirmation",
            width: '400px',
            resizable: false,
            drag: function (event, ui) {
                Util.HandleDragDialog(event, ui);
            },
            buttons: {
                Yes: {
                    click: function () {
                        $(this).remove();
                        deferred.resolve();
                    },
                    "class": "btn btn-primary",
                    text: customizeLabelYesButton
                },
                No: {
                    click: function () {
                        $(this).dialog("close");
                        deferred.reject();
                    },
                    "class": "btn btn-default",
                    text: customizeLabelNoButton
                }
            },
            close: function (event, ui) {
                deferred.reject();
            }
        });

    return deferred.promise();
};

Util.ConfirmWithReasonDialog = function (message, title) {
    var deferred = $.Deferred();
    //remove el before build new popup
    $("#confirmWithReasonPopup").remove();

    var html = "<p>" + message + "</p>";
    html += "<input type='text' id='txtReason' placeholder='Reason' maxlength='399' class='form-control' style='border-radius: 5px !important;'/>";
    html += "<span id='reasonErrorMessage' style='color: red;'></span>";

    $('<div id="confirmWithReasonPopup"></div>').appendTo('body')
        .html("<div style='word-break: break-word;'>" + html + "</div>")
        .dialog({
            modal: true,
            title: title || "Confirmation",
            width: '400px',
            resizable: false,
            drag: function (event, ui) {
                Util.HandleDragDialog(event, ui)
            },
            buttons: {
                Yes: {
                    click: function () {
                        var reason = $("#txtReason").val().trim();
                        if (reason) {
                            $(this).remove();
                            deferred.resolve(reason);
                        } else {
                            $("#reasonErrorMessage").text("Reason is required to proceed.");
                        }
                    },
                    "class": "btn btn-primary",
                    text: "Yes"
                },
                No: {
                    click: function () {
                        $(this).dialog("close");
                    },
                    "class": "btn btn-default",
                    text: "No"
                }
            },
            close: function (event, ui) {
                deferred.reject();
            }
        });

    return deferred.promise();
};

Util.IsDuplicateInArray = function (array) {
    var temp = [];
    for (var i = 0; i < array.length; i++) {
        if (temp.indexOf(array[i]) < 0) {
            temp.push(array[i]);
        } else {
            return true;
        }
    }

    return false;
};

Util.IsValidExtensionImageFile = function (extensionFile) {
    if (extensionFile == "png" || extensionFile == "jpg") {
        return true;
    }
    return false;
};

Util.DisplayImageFile = function (element, input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(element).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};

Util.ShowDialog = function (element, width) {
    $(element).dialog({
        modal: true,
        resizable: false,
        width: width
    });
};

Util.SetDataToGrid = function (gridIdName, data) {
    var grid = $('#' + gridIdName)[0];
    $("#" + gridIdName).jqGrid('clearGridData');
    grid.addJSONData(data);
};

Util.ResizeGrid = function (gridIdName, parentGridId) {
    $(window).bind("resize",
        function () {
            $("#" + gridIdName).setGridWidth($("#" + parentGridId).width());
        }).trigger("resize");
};

Util.JQGridAlignLeftHeader = function (gridIdName, columnName) {
    $("#" + gridIdName).jqGrid('setLabel', columnName, '', { 'text-align': 'left' });
};

Util.JQGridAlignRightHeader = function (gridIdName, columnName) {
    $("#" + gridIdName).jqGrid('setLabel', columnName, '', { 'text-align': 'right' });
};

Util.JQGridSetPaddingLeft = function (gridIdName, columnName) {
    $("#" + gridIdName).jqGrid('setLabel', columnName, '', { 'padding-left': "8px" });
};

Util.JQGridSetPaddingRight = function (gridIdName, columnName) {
    $("#" + gridIdName).jqGrid('setLabel', columnName, '', { 'padding-right': "12px" });
};

Util.JQGridSetPaddingRightSort = function (gridIdName, columnName) {
    $("#jqgh_" + gridIdName + "_" + columnName).css("padding-right", "11px");
};




Util.CloseDialog = function (elementDialog) {
    if ($(elementDialog).hasClass('ui-dialog-content')) {
        $(elementDialog).dialog('close');
    }
}

Util.BulkMessageDialog = function (addedMessage, errorMessage, addedList, errorList, downloadLink, title) {
    //remove el before build new popup
    $("#bulkMessagePopup").remove();

    var html = "";
    html += "There are " +
        addedList.length +
        " terminals successfully assigned and " +
        errorList.length +
        " terminals failed to assign.";
    html += "<p style='margin: 0 0 10px 0;'>Please refer to details below:</p>";

    if (addedList.length) {
        //added list
        var addedString = addedList.join();
        var subAddedString = addedString;
        if (addedList.length > 5) {
            subAddedString = addedString.substring(0, addedString.indexOf(addedList[5]) - 1);
            subAddedString += "....";
        }

        var addedRow = "<p style='margin: 0'>- " + addedMessage + ": " + subAddedString + "</p>"
        html += addedRow;
    }

    if (errorList.length) {
        //error list
        html += "<p style='margin: 0px;'>- " + errorMessage + ":</p>"
        var table = "";
        table += "<table class='table table-bordered'>";
        table += "<tr><th>Terminal ID</th><th>Reason</th><tr>"
        var maxErrorCount = errorList.length > 5 ? 5 : errorList.length;
        for (var i = 0; i < maxErrorCount; i++) {
            table += "<tr><td>" + errorList[i].Key + "</td><td>" + errorList[i].Value + "</td></tr>";
        }

        table += "</table>";
        html += table;
    }

    html += "<a style='color: blue;text-decoration: underline;' href='" + downloadLink + "'>Download CSV file</a>";

    $('<div id="bulkMessagePopup"></div>').appendTo('body')
        .html("<div style='word-break: break-word;'>" + html + "</div>")
        .dialog({
            modal: true,
            title: title || "Message",
            width: '650px',
            resizable: false,
            drag: function (event, ui) {
                Util.HandleDragDialog(event, ui);
            },
            buttons: {
                Cancel: {
                    click: function () {
                        $(this).dialog("close");
                    },
                    "class": "btn btn-default",
                    text: "Close"
                }
            }
        });
};
Util.CheckEmptyField = function (selector, errorList, fieldDisplayname) {
    var field = $(selector);
    if (field == null || field == undefined) {
        return false;
    }
    if (field.val() == null || field.val() == undefined || field.val() == '') {
        errorList.push('Please input ' + fieldDisplayname + '!');
        return false;
    }
    return true;
};

Util.CheckNotEmptyValue = function (value, fieldDisplayname, valObj) {
    if (valObj.errors == null || valObj.errors == undefined)
        valObj.errors = [];
    if (value == null || value == undefined || value == '') {
        valObj.errors.push('Please input ' + fieldDisplayname + '!');
        valObj.flag = false;
        return false;
    }
    return true;
};

Util.DisableControlById = function (listButtonId, isDisabled = true) {
    for (var i of listButtonId) {
        $("#" + i).prop("disabled", isDisabled);
    }
};
Util.CheckDirty = function (e, callback) {
    var inputId = e.target.id;
    var newValue = $(e.target).val();
    if (newValue != oldData[inputId]) {
        callback();
    }
};
Util.ConvertDateToStringDMY = function (date) {
    var options = { day: 'numeric', month: 'numeric', year: 'numeric' };
    var parts = date.toLocaleDateString(options).substring(0, 10).split('/');
    if (parts[0].length == 1)
        parts[0] = "0" + parts[0];
    if (parts[1].length == 1)
        parts[1] = "0" + parts[1];
    return parts[1] + '-' + parts[0] + '-' + parts[2];
}
Util.ConvertDMYStringToYMDString = function (str) {
    var parts = str.split('-');
    return parts[2] + '-' + parts[1] + '-' + parts[0];
}
Util.CheckMaxLength = function (value, maxLength, fieldDisplayname, valObj) {
    if (valObj.errors == null || valObj.errors == undefined)
        valObj.errors = [];
    if (value.length > maxLength) {
        valObj.errors.push('Invalid ' + fieldDisplayname + '!');
        valObj.flag = false;
        return false;
    }
    return true;
};
Util.ShowActiveMenu = function () {
    var pathname = window.location.pathname;
    var currentMenu = ($("ul.page-sidebar-menu li a[href='" + pathname + "']")).closest("ul.page-sidebar-menu>li");
    $(".active").removeClass("active open");
    $(currentMenu).addClass("active open");
}

Util.CheckItemInlist = function (item, list, existDo, notExistDo) {
    if (Array.isArray(list)) {
        var index = list.indexOf(item);
        if (index != -1) {
            if (existDo != null && existDo != undefined) {
                existDo();
            }
        } else {
            if (notExistDo != null && notExistDo != undefined) {
                notExistDo();
            }
        }
    }
}

Util.InitializeDialog = function (elementDialog) {
    $(elementDialog).dialog({
        draggable: true,
        drag: function (event, ui) {
            var scrollTopPx = $(document).scrollTop();
            var positionUI = ui.position;
            positionUI.top = positionUI.top - scrollTopPx;
            $(this).closest(".ui-dialog").css("top", positionUI.top);
        }
    });
    $(elementDialog).dialog("close");
}


Util.isNumberKey = function(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
};

overrideGoToToday = function() {
    var _gotoToday = $.datepicker._gotoToday;
    $.datepicker._gotoToday = function(a) {
        var target = $(a);
        var inst = this._getInst(target[0]);
        _gotoToday.call(this, a);
        $.datepicker._selectDate(a,
            $.datepicker._formatDate(inst, inst.selectedDay, inst.selectedMonth, inst.selectedYear));
        target.blur();
    };
};

Util.DisableCheckbox = function (elementID) {
    $("#" + elementID).removeClass("form-control").addClass("check-box");
    $("#" + elementID).attr("disabled", "disabled");
    $("#" + elementID).parent().parent().addClass("disabled");
}

Util.EnableCheckbox = function (elementID) {
    $("#" + elementID).removeClass("check-box").addClass("form-control");
    $("#" + elementID).removeAttr("disabled");
    $("#" + elementID).parent().parent().removeClass("disabled");
}
Util.getFilterParams = function (area) {
    var listEl = $(area + " .search-param");
    var params = {};
    for (var elName in listEl) {
        if (!isNaN(parseInt(elName))) {
            var el = listEl[elName];
            var key = el.id;
            if (key) {
                var val = '';
                if (el.className.includes('datepicker')) {
                    var splitter = el.value.indexOf('-') != -1 ? '-' : el.value.indexOf('/') != -1 ? '/' : '.';
                    val = el.value.split(splitter).join("-");
                } else if (el.type == "checkbox") {
                    val = el.checked.toString();
                } else {
                    val = el.value;
                }
                params[key] = val.trim();
            }
        }
    }
    return params;
}


Util.addParams = function (params, objToAdd) {
    for (var elName in objToAdd) {
        if (objToAdd[elName] != null && objToAdd[elName] != undefined) {
            params[elName] = objToAdd[elName];
        }
    }
}

Util.dateToStr = function(date, splitter = '-') {
    if (!date) {
        date = new Date();
    }
    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var currentDate = day + splitter + (month) + splitter + date.getFullYear();
    return currentDate;
};


Util.InitDatepicker = function (selector, options) {
    overrideGoToToday();
    $(selector).datepicker(options);
    $(selector).datepicker("option", "buttonImage", "../Images/iconfinder_calendar_115762.png");
    $(selector).datepicker("option", "showOn", "both");
    $(selector).parent().css("display", "flex");
    if (options.allowClear) {
        cleanDatepicker();
    }
};

function cleanDatepicker() {
    var old_fn = $.datepicker._updateDatepicker;

    $.datepicker._updateDatepicker = function (inst) {
        old_fn.call(this, inst);

        var buttonPane = $(this).datepicker("widget").find(".ui-datepicker-buttonpane");

        $(
            "<button type='button' class='ui-datepicker-clean ui-state-default ui-priority-primary ui-corner-all'>Clear</button>")
            .appendTo(buttonPane).click(function (ev) {
                $.datepicker._clearDate(inst.input);
                inst.input.val("");
            });
    };
}

Util.ValidateDateFromTo = function (fromElement, toElement) {
    fromElement.datepicker('option', 'beforeShowDay', function (date) {
        var endDate = toElement.datepicker('getDate');
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });
    toElement.datepicker('option', 'beforeShowDay', function (date) {
        var startDate = fromElement.datepicker('getDate');
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });
}

Util.ReloadDataJqgrid = function (gridId, data, additionalFunc) {
    var grid = $(gridId);
    grid.jqGrid('clearGridData');
    if (data && data.rows.length > 0) {
        grid[0].addJSONData(data);
    }
    else {
        Util.MessageDialog('No result found', 'Information');
    }
    additionalFunc();
}

Util.InitializeDropdownList = function (elementId, selectAllText = null, data, valueField, textField) {
    if (selectAllText) {
        $(elementId).append('<option value="">' + selectAllText + '</option>');
    }
    if (data) {
        var options = '';
        $.each(data,
            function (i, item) {
                options += '<option value="' + item[valueField] + '">' + item[textField] + '</option>'
            });
        $(elementId).append(options);
    }
}


Util.isBlank = function(str) {
    return (!str || $.trim(str) === "");
};

Util.InitJqGrid = function (
    props,
    searchObject,
    loadGrid) {
    // props calculation before initiation
    var initTag = $("#" + props.gridId);
    if (initTag.prop("tagName") == 'DIV') {
        var outerDiv = props.gridId;
        props.gridId = initTag.attr('id') + "Grid";
        $("#" + outerDiv).append('<table id="' + props.gridId + '"></table>');
        if (props.hasPager) {
            props.pagerId = initTag.attr('id') + "GridPager";
            $("#" + outerDiv).append('<div id="' + props.pagerId + '"></div>');
        }
    }
    $.fn.getGrid = function () {
        if (this.prop("tagName") == 'TABLE') {
            return this;
        }
        return $(this).find("#" + this.attr('id') + "Grid");
    };
    $.fn.getPageSize = function () {
        return parseInt($(this).find('select.ui-pg-selbox').val());
    };
    $.fn.resizeFrozenRows = function () {
        var grid = this[0];
        if (grid.p.frozenColumns) {
            resizeColumnHeader.call(grid);
            fixPositionsOfFrozenDivs.call(grid);
            fixGboxHeight.call(grid);
            fixFrozenColumnWidth.call(grid);
        }
    };
    props.isFrozenColumn = props.colModel[0].frozen ? true : false;
    // init grid
    $("#" + props.gridId).jqGrid({
        colNames: props.colNames,
        colModel: props.colModel,
        pager: "#" + props.pagerId ? props.pagerId : '',
        rowNum: props.rowNum ? props.rowNum : 20,
        loadui: "disable",
        rowList: props.rowList ? props.rowList : [20, 50, 100, 200],
        height: props.height != null && props.height != undefined ? props.height : "100%",
        pgbuttons: props.pgbuttons ? props.pgbuttons : true,
        viewrecords: props.viewrecords ? props.viewrecords : true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        gridComplete: props.gridComplete ? props.gridComplete : null,
        autowidth: props.enableAutoWidth != null && props.enableAutoWidth != undefined ? props.enableAutoWidth : false,
        shrinkToFit: props.enableShrinkToFit != null && props.enableShrinkToFit != undefined ? props.enableShrinkToFit : false,
        multiselect: props.enableMultiselect != null && props.enableMultiselect != undefined ? props.enableMultiselect : false,
        onSelectRow: props.onSelectRow ? props.onSelectRow : null,
        ondblClickRow: props.ondblClickRow ? props.ondblClickRow : null,
        loadtext: props.loadtext ? props.loadtext : null,
        resizeStop: props.resizeStop ? props.resizeStop : null,
        autoencode: props.autoencode ? props.autoencode : false,
        datatype: props.datatype ? props.datatype : 'local',

    });
    //reference to: "http://www.trirand.com/blog/?page_id=393/help/jqgrid-frozen-column-is-not-working-with-toggle-filter"
    var fixPositionsOfFrozenDivs = function () {
        var $rows;
        if (this.grid === undefined) {
            return;
        }
        if (this.grid.fbDiv !== undefined) {
            $rows = $('>div>table.ui-jqgrid-btable>tbody>tr', this.grid.bDiv);
            $('>table.ui-jqgrid-btable>tbody>tr', this.grid.fbDiv).each(function (i) {
                var rowHeight = $($rows[i]).height(), rowHeightFrozen = $(this).height();
                if ($(this).hasClass("jqgrow")) {
                    $(this).height(rowHeight);
                    rowHeightFrozen = $(this).height();
                    if (rowHeight !== rowHeightFrozen) {
                        $(this).height(rowHeight + (rowHeight - rowHeightFrozen));
                    }
                }
            });
            $(this.grid.fbDiv).height(this.grid.bDiv.clientHeight + 1);
            $(this.grid.fbDiv).css($(this.grid.bDiv).position());
        }
        if (this.grid.fhDiv !== undefined) {
            $rows = $('>div>table.ui-jqgrid-htable>thead>tr', this.grid.hDiv);
            $('>table.ui-jqgrid-htable>thead>tr', this.grid.fhDiv).each(function (i) {
                var rowHeight = $($rows[i]).height(), rowHeightFrozen = $(this).height();
                $(this).height(rowHeight);
                rowHeightFrozen = $(this).height();
                if (rowHeight !== rowHeightFrozen) {
                    $(this).height(rowHeight + (rowHeight - rowHeightFrozen));
                }
            });
            $(this.grid.fhDiv).height(this.grid.hDiv.clientHeight);
            $(this.grid.fhDiv).css($(this.grid.hDiv).position());
        }
    }
    var fixGboxHeight = function () {
        var gviewHeight = $("#gview_" + $.jgrid.jqID(this.id)).outerHeight();
        var pagerHeight = 0;
        if (this.p.pager) {
            pagerHeight = $(this.p.pager).outerHeight();
        }
        $("#gbox_" + $.jgrid.jqID(this.id)).height(gviewHeight + pagerHeight);
        gviewHeight = $("#gview_" + $.jgrid.jqID(this.id)).outerHeight();
        if (this.p.pager) {
            pagerHeight = $(this.p.pager).outerHeight();
        }
        $("#gbox_" + $.jgrid.jqID(this.id)).height(gviewHeight + pagerHeight);
    };
    var resizeColumnHeader = function () {
        var rowHeight, resizeSpanHeight,
            // get the header row which contains
            headerRow = $(this).closest("div.ui-jqgrid-view")
                .find("table.ui-jqgrid-htable>thead>tr.ui-jqgrid-labels");
        // reset column height
        headerRow.find("span.ui-jqgrid-resize").each(function () {
            this.style.height = '';
        });
        // increase the height of the resizing span
        resizeSpanHeight = 'height: ' + headerRow.height() + 'px !important; cursor: col-resize;';
        headerRow.find("span.ui-jqgrid-resize").each(function () {
            this.style.cssText = resizeSpanHeight;
        });
        // set position of the dive with the column header text to the middle
        rowHeight = headerRow.height();
        headerRow.find("div.ui-jqgrid-sortable").each(function () {
            var $div = $(this);
            $div.css('top', (rowHeight - $div.outerHeight()) / 2 + 'px');
        });
    }
    var fixFrozenColumnWidth = function () {
        var gview = $("#gview_" + $.jgrid.jqID(this.id));
        var hTableRowColList = $(gview).find('.ui-jqgrid-hdiv').not('.frozen-div').find('tr th');
        var hFTableRowColList = $(gview).find('.frozen-div.ui-jqgrid-hdiv tr th');
        var countFrozenColumn = $(hFTableRowColList).length;
        for (var i = 0; i < countFrozenColumn; i++) {
            var colWidth = $(hTableRowColList[i]).width();
            $(hFTableRowColList[i]).width(colWidth);
            $(gview).find('.frozen-bdiv.ui-jqgrid-bdiv tr td:nth-child(' + (i + 1).toString() + ')').width(colWidth);
        }
    }
    if (props.isFrozenColumn) {
        var $grid = $("#" + props.gridId);
        $grid.setGridWidth($($grid).closest('.grid-wrapper').width());
        $grid.jqGrid('setFrozenColumns');
        resizeColumnHeader.call($grid[0]);
        $grid.triggerHandler("jqGridAfterGridComplete");
        fixPositionsOfFrozenDivs.call($grid[0]);
        fixGboxHeight.call($grid[0]);
    }
    $(window).bind('resize', props.gridId, function () {
        $("#" + props.gridId).setGridWidth($("#" + props.gridId).closest('.grid-wrapper').width());
        var grid = $("#" + props.gridId)[0];
        if (grid.p.frozenColumns) {
            $("#" + props.gridId).resizeFrozenRows();
        }
    }).trigger('resize');
    //check the grid include paging
    $("#" + props.gridId).setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $("#" + props.gridId).getGridParam('page');
            var rowNum = $("#" + props.gridId).getGridParam('rowNum');
            if (pgButton == "next_" + props.pagerId) {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_" + props.pagerId) {
                if (currentPage == 1) {
                    return;
                }
                currentPage = currentPage - 1;
            }
            if (pgButton == "first_" + props.pagerId) {
                if (currentPage == 1) {
                    return;
                }
                currentPage = 1;
            }
            if (pgButton == "last_" + props.pagerId) {
                if ($("#" + props.gridId).getGridParam('lastpage') == 0) {
                    return;
                }
                if (currentPage == $("#" + props.gridId).getGridParam('lastpage')) {
                    return;
                }
                currentPage = $("#" + props.gridId).getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            var elTotalPage = $(p.pager).find("span[id*='" + props.pagerId + "']")[0];
            var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText.replace(/,/g, '')) || 0) : 0;
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }
            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                if (newPage <= totalPage && newPage > 0) {
                    currentPage = newPage;
                }
            }
            if (currentPage > totalPage) {
                return;
            }
            if (loadGrid) {
                if (searchObject) {
                    searchObject.PageIndex = currentPage;
                    searchObject.PageSize = rowNum;
                }
                loadGrid();
            }
        }
    });
    if (props.allowSort) {
        $('#' + props.gridId).setGridParam({
            onSortCol: function (index, columnIndex, sortOrder) {
                if ($("#" + props.gridId).getGridParam('lastpage') == 0) {
                    return;
                }
                if (loadGrid) {
                    if (searchObject) {
                        searchObject.SortField = index;
                        searchObject.SortOrder = sortOrder;
                        searchObject.PageIndex = 1;
                        searchObject.PageSize = $("#" + props.gridId).getGridParam('rowNum');
                    }
                    loadGrid();
                }
                return 'stop';
            }
        });
    }
};

Util.BlockjqGrid = function (jqgridId, applyZindex = false) {
    if (applyZindex) {
        $(jqgridId).closest(".ui-jqgrid").block({
            message: "<h5>Being processed...</h5>",
            baseZ: 0,
            css: {"z-index": 0}
        });
        return;
    }
    $(jqgridId).closest(".ui-jqgrid").block({
        message: "<h5>Being processed...</h5>"
    });
};

Util.UnBlockjqGrid = function (jqgridId) {
    $(jqgridId).closest(".ui-jqgrid").unblock();
};

Util.DisableCheckbox = function(elementID) {
    $("#" + elementID).removeClass("form-control").addClass("check-box");
    $("#" + elementID).attr("disabled", "disabled");
    $("#" + elementID).parent().parent().addClass("disabled");
};

Util.EnableCheckbox = function(elementID) {
    $("#" + elementID).removeClass("check-box").addClass("form-control");
    $("#" + elementID).removeAttr("disabled");
    $("#" + elementID).parent().parent().removeClass("disabled");
};

Util.resetGridSortIcon = function(gridSelector) {
    var sortIcons = $(gridSelector + ' span.ui-grid-ico-sort');
    sortIcons.removeClass('ui-state-disabled');
    sortIcons.addClass('ui-state-disabled');
};

Util.AutoCorrectText = function (selector) {
    $(selector).on('focusout', function (event) {
        if (this.pattern) {
            this.value = this.value.replace(new RegExp(this.pattern, 'g'), '');
        }
    });
}
Util.CardImage = function () {
    const CARD_IMAGE = {
        MASTERCARD: '../assets/global/img/mastercard.svg',
        MAESTRO: '../assets/global/img/maestro.svg',
        PAYPAL: '../assets/global/img/paypal.svg',
        UNIONPAY: '../assets/global/img/unionpay.svg',
        VISA: '../assets/global/img/visa.svg',
        JCB: '../assets/global/img/jcb.svg',
        DISCOVER: '../assets/global/img/discover.svg',
        DINER: '../assets/global/img/diner.svg',
        AMEX: '../assets/global/img/amex.svg',
        ALIPAY: '../assets/global/img/mastercard.svg',
    }
    return CARD_IMAGE;
}
Util.ShowGridMessage = function (tableName, messages) {
    var html = '<div class="gridMessage">#content#</div>';
    var content = '';
    var arrMessage = Array.isArray(messages) ? messages : [messages];
    for (var mess of arrMessage) {
        mess = mess.substring(mess.length - 1) == '.' ? mess : mess + '.';
        content += '<p>' + mess + '</p>';
    }
    html = html.replace('#content#', content);
    $('#' + tableName + ' .ui-jqgrid-bdiv').first().append(html);
}
Util.RemoveGridMessage = function (tableName) {
    $('#' + tableName + ' .ui-jqgrid-bdiv .gridMessage').remove()
}

Util.initUploadZone = function(dragZoneId, config, successFunction, errorFunction){
    var previewTemplate = '<div class=\"dz-preview dz-file-preview\">'
        + '<div class=\"dz-details\">'
        + '<div class=\"dz-filename\">'
        + '<img src="../Images/File-Excel.svg"><br/>'
        + '<span data-dz-name></span>'
        + '</div>'
        + '<div class=\"dz-size\" data-dz-size></div>'
        + '</div>'
        + '</div>';
    
    var dragDropTemplate = '<form class=\"dropzone\" id=\"DragAndDropUploadForm\">'
        + '<div class=\"dz-message\">'
        + '<img src=\"../Images/icon-cloud upload.svg\"/><br/><br/>'
        + 'Drag and drop .CSV file here <br/> OR <br/>'
        + '<button type=\"button\" class=\"btn btn-default\" id=\"SelectFile\">Select file</button>'
        + '</div>'
        + '</form>'
        + '<button type=\"button\" id=\"UploadFile\" class=\"btn blue\">Upload</button>'
        + '<style>'
        + '#UploadFile { width: 100%; margin-top: 10px; border-radius: 3px!important; }'
        + '#SelectFile { border-radius: 3px!important; }'
        + '.dropzone .dz-preview {width: 100%;}'
        + '.dropzone .dz-preview .dz-details .dz-filename:hover span { border: unset }'
        + '</style>';

    $(dragZoneId).html(dragDropTemplate);

    var dropZone = new Dropzone('#DragAndDropUploadForm',{ url: config.uploadURL,
        paramName: config.paramName !== undefined ? config.paramName : "file", // The name that will be used to transfer the file
        autoProcessQueue: config.autoProcessQueue !== undefined ? config.autoProcessQueue : true,
        previewTemplate: config.previewTemplate !== undefined ? config.previewTemplate : previewTemplate,
        clickable: config.clickable !== undefined ? config.clickable : '#SelectFile',
        acceptedFiles: config.acceptedFiles !== undefined ? config.acceptedFiles : null,
        uploadMultiple: config.maxFiles === undefined || config.maxFiles === null ? true : (config.maxFiles !== 1),
        maxFiles: (config.maxFiles === undefined || config.maxFiles === null) ? null : config.maxFiles,
        timeout: (config.timeout === undefined || config.timeout === null) ? 30000 : config.timeout,
        init: function() {
            var submitButton = $('#UploadFile');
            var myDropZone = this;
            submitButton.on('click', function() {
                if (myDropZone.files.length === 0)
                {
                    return;
                }
                LG_ShowProgressMask("Uploading...");
                myDropZone.processQueue();
                
            });
            
            
            myDropZone.on('success', function (file, result){
                LG_HideProgressMask();
                
                if (successFunction != null)
                    successFunction(file, result);
            })

            myDropZone.on('error', function (file, result, xhr){
                LG_HideProgressMask();

                if (errorFunction != null)
                    errorFunction(file, result, xhr);
            })

            myDropZone.on("addedfile", function(file) {
                if (this.files.length > this.options.maxFiles)
                {
                    this.removeFile(this.files[0]);
                }

                if (config.acceptedFiles != undefined) {
                    var fileExt = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
                    if (config.acceptedFiles.split(',').indexOf(fileExt) === -1) {
                        Util.MessageDialog("Invalid file format. Please select another file.", "Error");
                        this.removeFile(file);
                    }
                }
            })
        }
    });

    return dropZone;
}