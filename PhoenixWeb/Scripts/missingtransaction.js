﻿$(function () {
    var currentYear = new Date().getFullYear();
    $('.datepicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
        yearRange: "1900:" + (currentYear)
    });

    $('.datepickerWithDayAndMonthFormat').datepicker({
        dateFormat: 'ddmm'
    });

    //validate From Date must <= To Date
    $("#TransactionStartDate").datepicker('option', 'beforeShowDay', function (date) {
        var endDate = $("#TransactionEndDate").datepicker('getDate')
        return endDate ? Util.disabledDateFollowOtherDate(endDate, date) : [true];
    });

    //validate From Date must >= To Date
    $("#TransactionEndDate").datepicker('option', 'beforeShowDay', function (date) {
        var startDate = $("#TransactionStartDate").datepicker('getDate')
        return startDate ? Util.disabledDateFollowOtherDate(date, startDate) : [true];
    });

    //disable future date to "File Date" field
    $("#FileDate").datepicker('option', 'maxDate', new Date())

    $("#BatchSequence").off("focusout").on("focusout",
        function() {
            var currentValue = $("#BatchSequence").val();
            if (isNaN(Number(currentValue)) || currentValue < 0 || currentValue > 99) {
                //Validate errorClass
                $("#BatchSequence").addClass("errorClass");
                $("#BatchSequence").attr("title", "Batch Sequence only allow number between 0 and 99");
                return false;
            }
            $("#BatchSequence").removeClass("errorClass");
            $("#BatchSequence").attr("title", "");

        });

    $('.specialDatePicker').datepicker({
        changeYear: true,
        showButtonPanel: true,
        changeMonth: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: new Date(),
        dateFormat: 'ddmmyy',
        yearRange: "1900:" + (currentYear)
    });

    var searchObject = undefined;
    var searchObjectMissingFromWestPac = undefined;
    selectedDeclinedTransaction = [];
    selectedMissingTransactionFromWestPac = [];

    //Trigger resize payment method jqgrid when window is resizing.
    $(window).bind('resize', function () {
        $("#grdMissingTransaction").setGridWidth($("#declinedTransaction").width() - 20);
        $("#grdMissingTransactionFromWestPac").setGridWidth($("#MissingTransactionFromWestPac").width() - 20);
    }).trigger('resize');

    $("#grdMissingTransaction").jqGrid({
        url: '',
        colNames: [
            'Select', 'Terminalid', 'Start shift', 'End shift', 'Transaction number', 'Transaction Date Time', 'Transaction Total'
            , 'Amount', 'Masked PAN', 'Batch Number', 'File date','Status'
        ],
        colModel: [
            {
                key: true,
                name: 'id',
                index: 'id',
                editable: true,
                sortable: false,
                formatter: customCheckboxFormatter
            },
            { key: false, name: 'terminalid', index: 'terminalid', editable: false, sortable: false },
            {
                key: false,
                name: 'startshift',
                index: 'startshift',
                editable: false,
                sortable: false
            },
            {
                key: false,
                name: 'endshift',
                index: 'endshift',
                editable: false,
                sortable: false
            },
            { key: false, name: 'transid1', index: 'transid1', editable: false, sortable: false },
            {
                key: false,
                name: 'transstart',
                index: 'transstart',
                editable: false,
                sortable: false
            },
            { key: false, name: 'transtotalamount', index: 'transtotalamount', editable: false, sortable: false },
            { key: false, name: 'transfare', index: 'transfare', editable: false, sortable: false },
            { key: false, name: 'transpan', index: 'transpan', editable: false, sortable: false },
            { key: false, name: 'batchnumber', index: 'batchnumber', editable: false, sortable: false },
            { key: false, name: 'filedate', index: 'filedate', editable: false, sortable: false },
            { key: false, name: 'trans_status', index: 'trans_status', editable: false, sortable: false }
        ],
        pager: '#grdMissingTransactionPager',
        rowNum: 10,
        loadui: 'disable',
        rowList: [10, 20, 50, 100],
        height: '100%',
        viewrecords: true,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
        },
        autowidth: true,
        multiselect: false
    });

    $("#grdMissingTransactionFromWestPac").jqGrid({
        url: '',
        colNames: [
            'Select', 'Start Shift', 'End Shift', 'Terminal ID', 'Masked PAN', 'Transaction Date Time', 'Amount', 'TransExtras', 'Total Amount'
        ],
        colModel: [
            {
                key: true,
                name: 'id',
                index: 'id',
                editable: true,
                sortable: false,
                formatter: customCheckboxFormatterForMissingTransactionFromWestPac,
                width: 80
            },
            { key: false, name: 'Startshift', index: 'Startshift', editable: false, sortable: false, width: 120 },
            { key: false, name: 'endshift', index: 'endshift', editable: false, sortable: false, width: 120 },
            { key: false, name: 'terminalnumber', index: 'terminalnumber', editable: false, sortable: false, width: 80 },
            { key: false, name: 'transpan', index: 'transpan', editable: false, sortable: false, width: 100 },
            {
                key: false,
                name: 'transstart',
                index: 'transstart',
                editable: false,
                sortable: false,
                width: 120
            },
            { key: false, name: 'TransFare', index: 'TransFare', editable: false, sortable: false, width: 80 },
            { key: false, name: 'TransExtras', index: 'TransExtras', editable: false, sortable: false, width: 80 },
            { key: false, name: 'TransTotalAmount', index: 'TransTotalAmount', editable: false, sortable: false }
        ],
        viewrecords: true,
        pager: '#grdMissingTransactionFromWestPacPager',
        rowNum: 10,
        loadui: 'disable',
        rowList: [10, 20, 50, 100],
        height: '100%',
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false
        },
        autowidth: true,
        multiselect: false
    });

    function customCheckboxFormatter(cellvalue, options, rowObject) {
        var html = "";
        html += "<input type='checkbox' data-declined-id='" + cellvalue + "' onclick='SaveSelected(this)'/>";
        return html;
    }

    function customCheckboxFormatterForMissingTransactionFromWestPac(cellvalue, options, rowObject) {
        var html = "";
        html += "<input type='checkbox' data-missing-id='" + cellvalue + "' onclick='keepMissingTransactionId(this)'/>";
        return html;
    }

    keepMissingTransactionId = function (checkbox) {
        var isChecked = $(checkbox).is(":checked");
        var currentMissingTransactionID = $(checkbox).attr("data-missing-id");
        if (isChecked) {
            selectedMissingTransactionFromWestPac.push(currentMissingTransactionID);
        } else {
            selectedMissingTransactionFromWestPac.splice($.inArray(currentMissingTransactionID, selectedMissingTransactionFromWestPac), 1);
        }
    };

    SaveSelected = function (checkbox) {
        var isChecked = $(checkbox).is(":checked");
        var currentDeclinedTransactionID = $(checkbox).attr("data-declined-id");
        if (isChecked) {
            selectedDeclinedTransaction.push(currentDeclinedTransactionID);
        } else {
            selectedDeclinedTransaction.splice($.inArray(currentDeclinedTransactionID, selectedDeclinedTransaction), 1);
        }
    };

    $('#grdMissingTransaction').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdMissingTransaction').getGridParam('page');
            var rowNum = $('#grdMissingTransaction').getGridParam('rowNum');
            var sortName = $('#grdMissingTransaction').getGridParam('sortname');
            var sortOrder = $('#grdMissingTransaction').getGridParam("sortorder");

            if (pgButton == "next_grdMissingTransactionPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdMissingTransactionPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdMissingTransactionPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdMissingTransactionPager") {
                currentPage = $('#grdMissingTransaction').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
                currentPage = 1;
            }

            if (pgButton == "user") {
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());
                var elTotalPage = $(p.pager).find("span[id*='grdMissingTransactionPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page.

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }
            if (!searchObject) {
                searchObject = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    TerminalID: $("#TerminalID").val(),
                    TransactionStartDate: $("#TransactionStartDate").val(),
                    TransactionEndDate: $("#TransactionEndDate").val()
                };
            } else {
                searchObject.CurrentPage = currentPage;
                searchObject.PageSize = rowNum;
            }
            SearchMissingTransaction();
        }
    });

    $('#grdMissingTransactionFromWestPac').setGridParam({
        onPaging: function (pgButton) {
            var currentPage = $('#grdMissingTransactionFromWestPac').getGridParam('page');
            var rowNum = $('#grdMissingTransactionFromWestPac').getGridParam('rowNum');
            var sortName = $('#grdMissingTransactionFromWestPac').getGridParam('sortname');
            var sortOrder = $('#grdMissingTransactionFromWestPac').getGridParam("sortorder");

            if (pgButton == "next_grdMissingTransactionFromWestPacPager") {
                currentPage = currentPage + 1;
            }
            if (pgButton == "prev_grdMissingTransactionFromWestPacPager") {
                currentPage = currentPage - 1;
            }

            if (pgButton == "first_grdMissingTransactionFromWestPacPager") {
                currentPage = 1;
            }

            if (pgButton == "last_grdMissingTransactionFromWestPacPager") {
                currentPage = $('#grdMissingTransactionFromWestPac').getGridParam('lastpage');
            }
            var p = $(this).jqGrid("getGridParam");
            if (pgButton == "records") {
                var newRowNum = parseInt($(p.pager).find(".ui-pg-selbox").val());
                rowNum = newRowNum;
            }

            if (pgButton == "user") {
                var elTotalPage = $(p.pager).find("span[id*='grdMissingTransactionFromWestPacPager']")[0];
                var totalPage = elTotalPage ? (parseInt(elTotalPage.innerText) || 0) : 0; //get total page, default value equal 0 in case not found total page element.
                var newPage = parseInt($(p.pager).find(".ui-pg-input").val());

                if (newPage <= totalPage) {
                    currentPage = newPage;
                }
            }
            if (!searchObjectMissingFromWestPac) {
                searchObjectMissingFromWestPac = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    TerminalID: $("#TerminalID").val(),
                    TransactionStartDate: $("#TransactionStartDate").val(),
                    TransactionEndDate: $("#TransactionEndDate").val()
                };
            } else {
                searchObjectMissingFromWestPac.CurrentPage = currentPage;
                searchObjectMissingFromWestPac.PageSize = rowNum;
            }
            SearchMissingTransactionFromWestPac();
        }
    });

    SearchMissingTransaction = function () {
        //Show progress mask
        LG_ShowProgressMask();
        $('#grdMissingTransaction').jqGrid('clearGridData');
        //Searching declined transaction
        $.ajax({
            url: "MissingTransaction/GetDeclinedTransaction",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: JSON.stringify(searchObject)
        }).done(function (result) {
            //Loading data to grid
            var grid = $('#grdMissingTransaction')[0];
            $('#grdMissingTransaction').jqGrid('clearGridData');
            grid.addJSONData(result);
            //Clear global variable (keeping selected transaction ids)
            selectedDeclinedTransaction = [];
            LG_HideProgressMask();
        }).fail(function (err) {
            console.log(err);
            LG_HideProgressMask();
        });
    };

    $("#search").off("click").on("click",
        function () {
            var terminalID = $("#TerminalID").val();
            var transactionStartDate = $("#TransactionStartDate").val();
            var transactionEndDate = $("#TransactionEndDate").val();

            if (!terminalID) {
                alert("Please input Terminal ID");
                return;
            }

            if (!searchObject) {
                var currentPage = $('#grdMissingTransaction').getGridParam('page');
                var rowNum = $('#grdMissingTransaction').getGridParam('rowNum');
                var sortName = $('#grdMissingTransaction').getGridParam('sortname');
                var sortOrder = $('#grdMissingTransaction').getGridParam("sortorder");
                searchObject = {
                    CurrentPage: currentPage,
                    PageSize: 10,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    TerminalID: $("#TerminalID").val(),
                    TransactionStartDate: $("#TransactionStartDate").val(),
                    TransactionEndDate: $("#TransactionEndDate").val()
                };
            } else {
                searchObject.TerminalID = terminalID;
                searchObject.TransactionStartDate = transactionStartDate;
                searchObject.TransactionEndDate = transactionEndDate;
            }
            SearchMissingTransaction();
        });

    $("#approveDeclinedTransaction").off("click").on("click",
        function () {
            if (!selectedDeclinedTransaction || selectedDeclinedTransaction.length === 0) {
                return;
            }
            LG_ShowProgressMask();
            var selectedDeclinedTransactionStr = selectedDeclinedTransaction.join();

            var terminalID = $("#TerminalID").val();
            var transactionStartDate = $("#TransactionStartDate").val();
            var transactionEndDate = $("#TransactionEndDate").val();

            $.ajax({
                url: "MissingTransaction/ApprovedTransaction",
                type: "POST",
                data: {
                    declinedTransactions: selectedDeclinedTransactionStr,
                    terminalID: searchObject.TerminalID,
                    transactionStartDate: searchObject.TransactionStartDate,
                    transactionEndDate: searchObject.TransactionEndDate
                }
            }).done(function (result) {
                if (result.IsApproved) {
                    SearchMissingTransaction();
                    selectedDeclinedTransaction = [];
                    LG_HideProgressMask();
                } else {
                    alert(result.Message);
                    LG_HideProgressMask();
                }
            }).fail(function (err) {
                console.log(err);
            });

        });


    SearchMissingTransactionFromWestPac = function () {
        $('#grdMissingTransactionFromWestPac').jqGrid('clearGridData');
        LG_ShowProgressMask();

        $.ajax({
            url: "MissingTransactionFromWestPac/GetMissingTransactionFromWestPac",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: JSON.stringify(searchObjectMissingFromWestPac)
        }).done(function (result) {
            var grid = $('#grdMissingTransactionFromWestPac')[0];
            $('#grdMissingTransactionFromWestPac').jqGrid('clearGridData');
            selectedMissingTransactionFromWestPac = [];
            grid.addJSONData(result);
            LG_HideProgressMask();

        }).fail(function (err) {
            console.log(err);
            LG_HideProgressMask();
        });
    };

    $("#searchMissingTransactionFromWestPac").off("click").on("click",
        function () {
            var terminalID = $("#TerminalID").val();
            var transactionStartDate = $("#TransactionStartDate").val();
            var transactionEndDate = $("#TransactionEndDate").val();

            if (!terminalID || !transactionStartDate || !transactionEndDate) {
                alert("Please select search condition");
                return;
            }

            if (!searchObjectMissingFromWestPac) {

                var currentPage = $('#grdMissingTransactionFromWestPac').getGridParam('page');
                var rowNum = $('#grdMissingTransactionFromWestPac').getGridParam('rowNum');
                var sortName = $('#grdMissingTransactionFromWestPac').getGridParam('sortname');
                var sortOrder = $('#grdMissingTransactionFromWestPac').getGridParam("sortorder");

                searchObjectMissingFromWestPac = {
                    CurrentPage: currentPage,
                    PageSize: rowNum,
                    SortName: sortName,
                    SortOrder: sortOrder,
                    TerminalID: $("#TerminalID").val(),
                    BatchSequence: $("#BatchSequence").val(),
                    BatchID: $("#BatchID").val(),
                    FileDate: $("#FileDate").val(),
                    TransactionStartDate: $("#TransactionStartDate").val(),
                    TransactionEndDate: $("#TransactionEndDate").val()
                };
            } else {
                searchObjectMissingFromWestPac.TerminalID = terminalID;
                searchObjectMissingFromWestPac.TransactionStartDate = transactionStartDate;
                searchObjectMissingFromWestPac.TransactionEndDate = transactionEndDate;
                searchObjectMissingFromWestPac.BatchSequence = $("#BatchSequence").val();
                searchObjectMissingFromWestPac.BatchID = $("#BatchID").val();
                searchObjectMissingFromWestPac.FileDate = $("#FileDate").val();
            }
            SearchMissingTransactionFromWestPac();
            
        });


    $("#approveMissingTransactionFromWestpac").off("click").on("click",
        function () {
            if (!selectedMissingTransactionFromWestPac || selectedMissingTransactionFromWestPac.length === 0) {
                return;
            }

            var inputElements = $("#MissingTransactionFromWestPac").find("input");
            var valid = true;
            for (var i = 0; i < inputElements.length; i++) {
                if ($(inputElements[i]).hasClass("errorClass")) {
                    valid = false;
                    return false;
                }
            }

            if (!valid) {
                return false;
            }

            LG_ShowProgressMask();
            var selectedDeclinedTransactionStr = selectedMissingTransactionFromWestPac.join();
            searchObjectMissingFromWestPac.SelectedTransactionIds = selectedDeclinedTransactionStr;
            searchObjectMissingFromWestPac.BatchSequence = $("#BatchSequence").val();
            searchObjectMissingFromWestPac.BatchID = $("#BatchID").val();
            searchObjectMissingFromWestPac.FileDate = $("#FileDate").val();
            $.ajax({
                url: "MissingTransactionFromWestPac/ApprovedMissingTransactionFromWestPac",
                type: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(searchObjectMissingFromWestPac)
            }).done(function (result) {
                if (result.IsApproved) {
                    SearchMissingTransactionFromWestPac();
                    selectedMissingTransactionFromWestPac = [];
                    LG_HideProgressMask();
                } else {
                    alert(result.Message);
                    LG_HideProgressMask();
                }
            }).fail(function (err) {
                console.log(err);
            });

        });

});