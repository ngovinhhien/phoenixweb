﻿$(function () {
    $("#grdQbrNumberHistory").jqGrid({
        url: 'GetQbrNumberHistory',
        datatype: 'json',
        postData:
        {
            memberKey: $('#Member_key').val()
        },
        mtype: 'Get',
        colNames: ['Id', 'QBR Number', 'Changed Date'],
        colModel: [
            { key: true, hidden: true, name: 'MemberReferenceAuditKey', index: 'MemberReferenceAuditKey', editable: false },            
            { key: false, name: 'MemberReferenceNumber', index: 'MemberReferenceNumber', editable: false, },
            { key: false, name: 'CreatedDate', index: 'CreatedDate', editable: false, formatter: 'date', formatoptions: { newformat: 'd/m/Y' } },
        ],
        pager: jQuery('#grdQbrNumberHistoryPager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: '',
        emptyrecords: 'No records to display',
        sord: "desc",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false,
        width: '100%'
    }).navGrid('#grdQbrPointHistoryPager', { edit: false, add: false, del: false, search: true, refresh: true }
    //    ,
    //    {
    //        // edit options
    //        zIndex: 100,
    //        url: '/member/Edit',
    //        closeOnEscape: true,
    //        closeAfterEdit: true,
    //        recreateForm: true,
    //        afterComplete: function (response) {
    //            if (response.responseText) {
    //                alert(response.responseText);
    //            }
    //        }
    //    },
    //    {
    //        // add options
    //        zIndex: 100,
    //        url: "/member/Create",
    //        closeOnEscape: true,
    //        closeAfterAdd: true,
    //        afterComplete: function (response) {
    //            if (response.responseText) {
    //                alert(response.responseText);
    //            }
    //        }
    //    },
    //    {
    //        // delete options
    //        zIndex: 100,
    //        url: "/member/Delete",
    //        closeOnEscape: true,
    //        closeAfterDelete: true,
    //        recreateForm: true,
    //        msg: "Are you sure you want to delete this task?",
    //        afterComplete: function (response) {
    //            if (response.responseText) {
    //                alert(response.responseText);
    //            }
    //        }
        //    }
    );
});