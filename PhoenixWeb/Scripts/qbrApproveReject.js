﻿$(function () {
    $("#grdQbrPointHistory").jqGrid({
        url: 'GetFileEntries',
        datatype: 'json',
        postData:
            {
                'exportFileEntry': function () { return $('#QantasExportFileKey').val() },
                'onlyRejected': function () { return !$("#showApproveReject").val() },
                sortorder: "desc"
            },
        mtype: 'Get',
        colNames: ['Actions', 'Id', 'Member Key', 'Member ID', 'Member', 'Description', 'Status', 'Points', 'Status/Reason', 'Qbr Number'],
        colModel: [
            { name: 'act', index: 'act', width: 120, sortable: false },
            { key: true, hidden: true, name: 'MemberReferenceEntryKey', index: 'MemberReferenceEntryKey', editable: false },
            { key: false, hidden: true, name: 'MemberReferenceEntry.Member_Key', index: 'MemberReferenceEntry.Member_Key', editable: false },
            { key: false, name: 'MemberReferenceEntry.Member.MemberId', index: 'MemberReferenceEntry.Member.MemberId', editable: false, },
            { key: false, name: 'MemberReferenceEntry.Member.TradingName', index: 'MemberReferenceEntry.Member.TradingName', editable: false, },
            { key: false, name: 'MemberReferenceEntry.Description', index: 'MemberReferenceEntry.Description', editable: false, },
            { key: false, name: 'QantasExportFileEntryStatus.Name', index: 'QantasExportFileEntryStatus.Name', editable: false, },
            { key: false, name: 'Points', index: 'Points', editable: false, align: "right", formatter: 'number', formatoptions: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 0, prefix: "" } },
            { key: false, name: 'UploadStatus', index: 'UploadStatus', editable: false, },
            { key: false, name: 'QBRNumber', index: 'QBRNumber', editable: false, },
        ],
        pager: jQuery('#grdQbrPointHistoryPager'),
        rowNum: 50,
        rowList: [10, 25, 50, 100],
        height: '100%',
        viewrecords: true,
        caption: 'Qantas Points File',
        emptyrecords: 'No records to display',
        sord: "desc",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        beforeRequest: function () {
           
        },
        gridComplete: function () {
            if ($("#showApproveReject").val() === "true") {
                $('#fileAction').show();
                $('#approveRejectLineItem').show();
                $('#approveRejectFile').show()
                var ids = jQuery("#grdQbrPointHistory").getDataIDs();
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    be = "<input type='button' value='Approve' onclick=onApprove(" + cl + "); /> &nbsp;";
                    se = "<input type='button' value='Reject' onclick=onReject(" + cl + "); />";
                    jQuery("#grdQbrPointHistory").setRowData(ids[i], { act: be + se })
                }
            }
            $.LoadingOverlay("hide");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            alert('HTTP status code: ' + jqXHR.status + '\n' +
                'textStatus: ' + textStatus + '\n' +
                'errorThrown: ' + errorThrown);
            alert('HTTP message body (jqXHR.responseText): ' + '\n' + jqXHR.responseText);
        },
        autowidth: true,
        multiselect: false,
        editurl: 'edit',
    }).navGrid('#grdQbrPointHistoryPager', { edit: false, add: false, del: false, search: true, refresh: true }
    );
});

function onApprove(id, mk) {
    updateQantasFileEntryStatus('approve', id, false, jQuery("#grdQbrPointHistory").getRowData(id)["MemberReferenceEntry.Member_Key"]);
}

function onReject(id, mk) {
    updateQantasFileEntryStatus('reject', id, false, jQuery("#grdQbrPointHistory").getRowData(id)["MemberReferenceEntry.Member_Key"]);
}

function reloadGrid() {
    $.LoadingOverlay("show", {
        image: "",
        text: "load'..."
    });
    $("#grdQbrPointHistory").jqGrid("setGridParam", { datatype: "json" })
        .trigger("reloadGrid", [{ current: true }]);

    $.LoadingOverlay("hide", {
        image: "",
        text: "load'..."
    });
}

function updateQantasFileEntryStatus(status, id, udpateAll, mk) {
    
    $.ajax({
       
        type: "POST",
        url: "UpdateQantasFileEntryStatus",
        dataType: "json",
        data: JSON.stringify({ qbrFileEntryKey: id, updateStatusWith: status, updateAll: udpateAll, memberKey: mk, qantasExportFilekey: $('#QantasExportFileKey').val() }),
        contentType: "application/json",
        beforeSend: function () {
            $('#messageQBR').html('please wait...');
        },
        success: function (data) {
            $('#messageQBR').html(data);
            reloadGrid();
        },
        error: function (jqXHR, textStatus, errorThrown, data) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status === 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status === 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#messageQBR').html("An error occured while connecting to server - " + jqXHR.message + "\nDetails: " + msg);
        }
    });
}

function updateQantasFileStatus(status, qantasExportFileKey) {    
    $.ajax({
        async: false,
        type: "POST",
        url: "UpdateQantasFileStatus",
        dataType: "json",
        data: JSON.stringify({ updateStatusWith: status, qantasExportFilekey: qantasExportFileKey }),
        contentType: "application/json",
        beforeSend: function () {
            $('#messageQBR').html('please wait...');
        },
        success: function (data) {
            $('#messageQBR').html(data);            
        },        
        error: function (jqXHR, textStatus, errorThrown, data) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status === 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status === 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#messageQBR').html("An error occured while connecting to server - " + jqXHR.message + "\nDetails: " + msg);
        }
    });
}