﻿using log4net.Config;
using Phoenix_BusLog;
using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PhoenixWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            FileInfo info = new FileInfo(Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "log4net.config"));
            XmlConfigurator.ConfigureAndWatch(info);
            ActionFilters.FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            Caching.ReloadMemoryCache();
        }
    }
}