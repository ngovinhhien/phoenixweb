﻿using Phoenix_BusLog.Data;
using Phoenix_BusLog.Excel;
using PhoenixObjects.Financial;
using PhoenixObjects.Transaction;
using PhoenixWeb.ActionFilters;
using PhoenixWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using PhoenixWeb.Helper;
using System.Data;
using ExcelDataReader;
using PhoenixObjects.Rewards;
using PhoenixWeb.Models.ViewModel;
using System.Data.Entity.Core;
using Newtonsoft.Json;
using Phoenix_BusLog;
using PhoenixObjects.Common;
using PhoenixObjects.Enums;
using PhoenixWeb.ReportingWebService;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class TransactionController : PhoenixWebController
    {
        //
        // GET: /Transaction/
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult Index()
        {
            return View();
        }

        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult PrintDocket()
        {

            PrintDocketSearchModel model = new PrintDocketSearchModel();
            model.EndDateString = DateTime.Now.ToString("s");
            model.StartDateString = DateTime.Now.ToString("s");
            model.ApproveStatusBool = true;
            model.DocketPaidStatusChecked = true;
            model.DocketPendingStatusChecked = true;
            model.DocketUnknownStatusChecked = true;
            DrawDropDownConfig();
            return View(model);
        }

        public ActionResult ShowMember(string id)
        {
            MemberDA mda = new MemberDA();
            int mkey = mda.GetMemberKey(id);
            return RedirectToAction("Edit", "Member", new { member_key = mkey });
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult PrintDocket(PrintDocketSearchModel model, FormCollection c)
        {
            try
            {
                DrawDropDownConfig();
                TransactionDA tda = new TransactionDA();
                DateTime StartDate = default(DateTime);
                DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
                if (StartDate != null)
                {
                    model.StartDate = StartDate;
                }

                DateTime EndDate = default(DateTime);
                DateTime.TryParse(c["EndDate"].ToString(), out EndDate);
                if (EndDate != null)
                {
                    model.EndDate = EndDate;
                }

                if (model.BothStatusBool == true)
                {
                    model.ApproveStatusBool = true;
                    model.DeclineStatusBool = true;
                }

                model.StartDateString = StartDate.ToString("s");
                model.EndDateString = EndDate.ToString("s");
                if (model.FileDateFrom != null)
                {
                    model.FileDateFromString = model.FileDateFrom.GetValueOrDefault(DateTime.Now).ToString("yyyy-MM-dd");
                }

                if (model.FileDateTo != null)
                {
                    model.FileDateToString = model.FileDateTo.GetValueOrDefault(DateTime.Now).ToString("yyyy-MM-dd");
                }

                List<TransactionSearch_ResultModel> transactionResult = tda.TransactionSearch(model);
                model.TransactionSearch_ResultModel = transactionResult;

                Session["TransactionSearch_SearchCondition_Report"] = model;
            }
            catch (Exception e)
            {
                if (e.InnerException != null && e.InnerException.Message.Contains("timeout"))
                {
                    ViewBag.ErrorMessage = "Timeout Error. Please select more filters condition because of this filters contain many rows.";
                    LogError(e.Message);
                }
                else
                {
                    ViewBag.ErrorMessage = "System Error.";
                    LogError(e.Message);
                }
            }
            return View(model);
        }

        private void DrawDropDownConfig()
        {
            //build Card Type dropdown list
            TransactionDA tda = new TransactionDA();
            var cardTypeList = tda.GetCadmusinCardTypes();
            ViewBag.CardType = cardTypeList.AsSelectListItem();

            var BusinessTypeList = new BusinessTypeDA().Get(true).ToList();
            BusinessTypeList.Add(new BusinessTypeModel()
            {
                BusinessTypeKey = 0,
                BusinessTypeName = "All",
            });
            ViewBag.BusinessTypeList = BusinessTypeList.Select(item => new SelectListItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString()
            }).OrderBy(n=>n.Value).ToList();

            List<string> ApprovalStatus = new List<string>();
            ApprovalStatus.Add("");
            ApprovalStatus.Add("Approved");
            ApprovalStatus.Add("Declined");

            ViewBag.ApprovalStatus = ApprovalStatus.Select(item => new SelectListItem
            {
                Text = item,
                Value = item
            }).ToList();
        }

        [RBAC(Permission = "Transaction_TransactionSearch_PrintDocket")]
        public ActionResult Print(string TransactionID, int IsApproved)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            UserDA uda = new UserDA();
            string ReportName = "/LTEPAdminReports/GenerateReceipt";

            IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

            parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "TransactionID", Value = TransactionID });
            if (IsApproved == 1)
            {
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "IsApproved", Value = "true" });
            }
            else
            {
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "IsApproved", Value = "false" });
            }

            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
            return new FileContentResult(output, mimeType);

        }

        [Authorize]
        [RBAC(Permission = "Transaction_TransactionSearch_ExporttDocketSearch")]
        public ActionResult ExportToExcel()
        {
            try
            {
                LogInfo("Start Export Transaction");
                byte[] output;
                string extension, mimeType, encoding;
                Warning[] warnings;
                string[] streamIds;
                var model = (PrintDocketSearchModel)Session["TransactionSearch_SearchCondition_Report"];
                LogInfo($"Search Obj = {JsonConvert.SerializeObject(model)}");
                var MemberID = string.IsNullOrEmpty(model.MemberID) ? null : $"{model.MemberID}";
                var TerminalID = string.IsNullOrEmpty(model.TerminalID) ? null : $"{model.TerminalID}";
                var TaxiID = string.IsNullOrEmpty(model.TaxiID) ? null : $"{model.TaxiID}";
                var BatchNumber = string.IsNullOrEmpty(model.BatchNumber) ? null : $"{model.BatchNumber}";
                var DocketFrom = model.DocketFrom == null ? null : $"{(float) model.DocketFrom}";
                var DocketTo = model.DocketTo == null ? null : $"{(float) model.DocketTo}";
                var CardType = string.IsNullOrEmpty(model.CardType) ? "ALL" : $"{model.CardType.ToUpper()}";
                var CardMask = string.IsNullOrEmpty(model.CardMask) ? null : $"{model.CardMask}";
                var ReceiptNo = string.IsNullOrEmpty(model.ReceiptNo) ? null : $"{model.ReceiptNo}";
                var AmexMID = string.IsNullOrEmpty(model.AmexMerchantID) ? null : $"{model.AmexMerchantID}";
                var WestpacMID = string.IsNullOrEmpty(model.WestPacMerchantID) ? null : $"{model.WestPacMerchantID}";
                var startDate = model.StartDate?.ToString("yyyy-MM-dd HH:mm:ss");
                var endDate = model.EndDate?.ToString("yyyy-MM-dd HH:mm:ss");

                var filedateStart = model.FileDateFrom?.ToString("yyyy-MM-dd HH:mm:ss");
                var filedateEnd = model.FileDateTo?.ToString("yyyy-MM-dd HH:mm:ss");

                IList<ParameterValue> parameters = new List<ParameterValue>();
                var ReportName =
                    $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}PHW_TransactionSearch";
                parameters = new List<ParameterValue>();
                parameters.Add(new ParameterValue { Name = "startdate", Value = startDate });
                parameters.Add(new ParameterValue { Name = "enddate", Value = endDate });
                parameters.Add(new ParameterValue { Name = "memberid", Value = MemberID });
                parameters.Add(new ParameterValue { Name = "terminalid", Value = TerminalID });
                parameters.Add(new ParameterValue { Name = "taxiid", Value = TaxiID });
                parameters.Add(new ParameterValue { Name = "BatchNumber", Value = BatchNumber });
                parameters.Add(new ParameterValue { Name = "Fare", Value = null });
                parameters.Add(new ParameterValue { Name = "CardNumber", Value = null });
                parameters.Add(new ParameterValue { Name = "InvRoc", Value = null });
                parameters.Add(new ParameterValue { Name = "CardType", Value = CardType });
                parameters.Add(new ParameterValue
                { Name = "ApprovedIncluded", Value = model.ApproveStatusBool.ToString() });
                parameters.Add(new ParameterValue
                { Name = "DeclinedIncluded", Value = model.DeclineStatusBool.ToString() });
                parameters.Add(new ParameterValue { Name = "FullCardNumber", Value = CardMask });
                parameters.Add(new ParameterValue { Name = "ReceiptNo", Value = ReceiptNo });
                parameters.Add(new ParameterValue { Name = "CompanyKey", Value = model.CompanyKey.ToString() });
                parameters.Add(new ParameterValue { Name = "AmexMID", Value = AmexMID });
                parameters.Add(new ParameterValue { Name = "FileDateStart", Value = filedateStart });
                parameters.Add(new ParameterValue { Name = "FileDateEnd", Value = filedateEnd });

                parameters.Add(new ParameterValue { Name = "WestpacMID", Value = WestpacMID });
                parameters.Add(new ParameterValue { Name = "TranTotalAmountFrom", Value = DocketFrom });
                parameters.Add(new ParameterValue { Name = "TranTotalAmountTo", Value = DocketTo });
                parameters.Add(new ParameterValue
                { Name = "LoadPaidDocketStatus", Value = model.DocketPaidStatusChecked.ToString() });
                parameters.Add(new ParameterValue
                { Name = "LoadPendingDocketStatus", Value = model.DocketPendingStatusChecked.ToString() });
                parameters.Add(new ParameterValue
                { Name = "LoadUnknownDocketStatus", Value = model.DocketUnknownStatusChecked.ToString() });
                string format = "EXCELOPENXML";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType,
                    out encoding, out warnings, out streamIds);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = $"TransactionList_{DateTime.Now.ToString("ddMMyyyy")}.xlsx",
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(output, mimeType);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return File(new byte[] { }, "application/xlsx");
            }
            finally
            {
                LogInfo("End Export Transaction");
            }
        }

        [Authorize]
        [RBAC(Permission = "Transaction_TransactionSearch_ExporttDocketSearch")]
        public ActionResult ExportToCSV()
        {
            try
            {
                LogInfo("Start Export Transaction");
                byte[] output;
                string extension, mimeType, encoding;
                Warning[] warnings;
                string[] streamIds;
                var model = (PrintDocketSearchModel)Session["TransactionSearch_SearchCondition_Report"];
                LogInfo($"Search Obj = {JsonConvert.SerializeObject(model)}");
                var MemberID = string.IsNullOrEmpty(model.MemberID) ? null : $"{model.MemberID}";
                var TerminalID = string.IsNullOrEmpty(model.TerminalID) ? null : $"{model.TerminalID}";
                var TaxiID = string.IsNullOrEmpty(model.TaxiID) ? null : $"{model.TaxiID}";
                var BatchNumber = string.IsNullOrEmpty(model.BatchNumber) ? null : $"{model.BatchNumber}";
                var DocketFrom = model.DocketFrom == null ? null : $"{(float)model.DocketFrom}";
                var DocketTo = model.DocketTo == null ? null : $"{(float)model.DocketTo}";
                var CardType = string.IsNullOrEmpty(model.CardType) ? "ALL" : $"{model.CardType.ToUpper()}";
                var CardMask = string.IsNullOrEmpty(model.CardMask) ? null : $"{model.CardMask}";
                var ReceiptNo = string.IsNullOrEmpty(model.ReceiptNo) ? null : $"{model.ReceiptNo}";
                var AmexMID = string.IsNullOrEmpty(model.AmexMerchantID) ? null : $"{model.AmexMerchantID}";
                var WestpacMID = string.IsNullOrEmpty(model.WestPacMerchantID) ? null : $"{model.WestPacMerchantID}";
                var startDate = model.StartDate?.ToString("yyyy-MM-dd HH:mm:ss");
                var endDate = model.EndDate?.ToString("yyyy-MM-dd HH:mm:ss");

                var filedateStart = model.FileDateFrom?.ToString("yyyy-MM-dd HH:mm:ss");
                var filedateEnd = model.FileDateTo?.ToString("yyyy-MM-dd HH:mm:ss");

                IList<ParameterValue> parameters = new List<ParameterValue>();
                var ReportName =
                    $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}PHW_TransactionSearch";
                parameters = new List<ParameterValue>();
                parameters.Add(new ParameterValue { Name = "startdate", Value = startDate });
                parameters.Add(new ParameterValue { Name = "enddate", Value = endDate });
                parameters.Add(new ParameterValue { Name = "memberid", Value = MemberID });
                parameters.Add(new ParameterValue { Name = "terminalid", Value = TerminalID });
                parameters.Add(new ParameterValue { Name = "taxiid", Value = TaxiID });
                parameters.Add(new ParameterValue { Name = "BatchNumber", Value = BatchNumber });
                parameters.Add(new ParameterValue { Name = "Fare", Value = null });
                parameters.Add(new ParameterValue { Name = "CardNumber", Value = null });
                parameters.Add(new ParameterValue { Name = "InvRoc", Value = null });
                parameters.Add(new ParameterValue { Name = "CardType", Value = CardType });
                parameters.Add(new ParameterValue
                { Name = "ApprovedIncluded", Value = model.ApproveStatusBool.ToString() });
                parameters.Add(new ParameterValue
                { Name = "DeclinedIncluded", Value = model.DeclineStatusBool.ToString() });
                parameters.Add(new ParameterValue { Name = "FullCardNumber", Value = CardMask });
                parameters.Add(new ParameterValue { Name = "ReceiptNo", Value = ReceiptNo });
                parameters.Add(new ParameterValue { Name = "CompanyKey", Value = model.CompanyKey.ToString() });
                parameters.Add(new ParameterValue { Name = "AmexMID", Value = AmexMID });
                parameters.Add(new ParameterValue { Name = "FileDateStart", Value = filedateStart });
                parameters.Add(new ParameterValue { Name = "FileDateEnd", Value = filedateEnd });

                parameters.Add(new ParameterValue { Name = "WestpacMID", Value = WestpacMID });
                parameters.Add(new ParameterValue { Name = "TranTotalAmountFrom", Value = DocketFrom });
                parameters.Add(new ParameterValue { Name = "TranTotalAmountTo", Value = DocketTo });
                parameters.Add(new ParameterValue
                { Name = "LoadPaidDocketStatus", Value = model.DocketPaidStatusChecked.ToString() });
                parameters.Add(new ParameterValue
                { Name = "LoadPendingDocketStatus", Value = model.DocketPendingStatusChecked.ToString() });
                parameters.Add(new ParameterValue
                { Name = "LoadUnknownDocketStatus", Value = model.DocketUnknownStatusChecked.ToString() });
                string format = "CSV";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType,
                    out encoding, out warnings, out streamIds);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = $"TransactionList_{DateTime.Now.ToString("ddMMyyyy")}.csv",
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(output, mimeType);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return File(new byte[] { }, "csv");
            }
            finally
            {
                LogInfo("End Export Transaction");
            }
        }

        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult ReconcileMissing()
        {
            MissingTransactionSearchModel model = new MissingTransactionSearchModel();
            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.EndDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.StartDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.ApproveStatusBool = true;


            DrawDropDownConfig();
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult ReconcileMissing(MissingTransactionSearchModel model, FormCollection c)
        {
            TransactionDA tda = new TransactionDA();

            DateTime StartDate = default(DateTime);
            DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
            if (StartDate != null)
                model.StartDate = StartDate;

            DateTime EndDate = default(DateTime);
            DateTime.TryParse(c["EndDate"].ToString(), out EndDate);
            if (EndDate != null)
                model.EndDate = EndDate;

            var approve = model.ApproveStatusBool;
            var decline = model.DeclineStatusBool;
            var both = model.BothStatusBool;

            if (model.BothStatusBool == true)
            {
                model.ApproveStatusBool = true;
                model.DeclineStatusBool = true;
            }
            model.StartDateString = StartDate.ToString("s");
            model.EndDateString = EndDate.ToString("s");


            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MissingTransactionSearchModel, PrintDocketSearchModel>()
                        .ForMember(x => x.ProcessBy, opt => opt.Ignore());
            });

            IMapper mapper = config.CreateMapper();
            var pmodel = mapper.Map<MissingTransactionSearchModel, PrintDocketSearchModel>(model);

            List<TransactionSearch_ResultModel> transactionResult = tda.TransactionSearch(pmodel);
            model.CadMusIn = transactionResult;

            DrawDropDownConfig();
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        [ValidateAntiForgeryToken]
        public ActionResult Import(MissingTransactionSearchModel model)
        {


            foreach (BankFileModel bankfilerecod in model.BankFile)
            {
                var a = bankfilerecod.IsChecked;
            }
            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.EndDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.StartDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.ApproveStatusBool = true;


            DrawDropDownConfig();
            return View("ReconcileMissing", model);
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        [ValidateAntiForgeryToken]
        public ActionResult BankFileUpload(HttpPostedFileBase file, MissingTransactionSearchModel model)
        {
            List<BankFileModel> bankFileModel = new List<BankFileModel>();
            if (file != null && file.ContentLength > 0 && file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                if (file.ContentLength > 0)
                {
                    //create an XML object and load it in                  
                    Stream fileStream = file.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;
                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return PartialView("_BankFile", bankFileModel);
                    }

                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    reader.Close();

                    foreach (DataRow row in result.Tables[0].Rows)
                    {
                        bankFileModel.Add(new BankFileModel()
                        {
                            MerchantID = row["Merchant ID"].ToString(),
                            TerminalId = row["Terminal ID"].ToString(),
                            SettlementDate = row["Settlement Date"].ToString(),
                            TransactionDate = row["Transaction Date"].ToString(),
                            TransactionTime = row["Transaction Time"].ToString(),
                            STAN = row["STAN"].ToString(),
                            CardIssuer = row["Card Issuer"].ToString(),
                            CardNumber = row["Card Number"].ToString(),
                            Account = row["Account"].ToString(),
                            Expiry = row["Expiry"].ToString(),
                            Amount = row["Amount"].ToString(),
                            TransType = row["Trans Type"].ToString(),
                            Approved = row["Approved"].ToString(),
                            Reversal = row["Reversal"].ToString(),
                        });
                    };


                }
            }
            // your existing code here
            TempData["Success"] = true;

            model.BankFile = bankFileModel;
            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.EndDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.StartDateString = DateTime.Now.ToString("s");// dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.ApproveStatusBool = true;


            DrawDropDownConfig();
            return View("ReconcileMissing", model);
        }





        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult Upload()
        {
            using (RewardsDA rewardsData = new RewardsDA())
            {
                return View(new LoyaltyUploadModel()
                {
                    //OrganisationforLoyaltyModel = organisationforLoyaltyModel,
                });
            }

        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult Import(FormCollection c, LoyaltyUploadModel model)
        {
            List<LoyaltyUploadFileLineItemModel> loyaltyUploadFileLineItemModel = new List<LoyaltyUploadFileLineItemModel>();
            model.FileRecords = (List<LoyaltyUploadFileModel>)Session["loyaltyUploadFileModel"];
            if (model.AllowImport)
            {
                foreach (LoyaltyUploadFileModel m in model.FileRecords)
                {
                    loyaltyUploadFileLineItemModel.Add(new LoyaltyUploadFileLineItemModel()
                    {
                        LoyaltyNumber = m.LoyaltyNumber,
                        MemberId = m.MemberId,
                        Name = m.Name,
                        Points = int.TryParse(m.Points, out var points) ? points : 0,
                    });
                }

                using (RewardsDA rewardsData = new RewardsDA())
                {
                    try
                    {
                        rewardsData.Save(loyaltyUploadFileLineItemModel, "PhoenixWeb", "Test", int.TryParse(model.SelectionOrganisationId, out var points) ? points : 0);


                        foreach (LoyaltyUploadFileModel m in model.FileRecords)
                        {
                            m.Status = "Imported In Phoenix";
                        }
                        model.AllowImport = false;
                    }
                    catch (Exception ex)
                    {

                    }
                    finally { }
                }


            }


            using (RewardsDA rewardsData = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
            }
            return View("Upload", model);
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        [ValidateAntiForgeryToken]
        public ActionResult LoyaltyRewardsFileUpload(HttpPostedFileBase file, LoyaltyUploadModel model)
        {
            bool allGood = false;
            List<LoyaltyUploadFileModel> loyaltyUploadFileModels = new List<LoyaltyUploadFileModel>();
            if (file != null && file.ContentLength > 0 && file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                if (file.ContentLength > 0)
                {
                    //create an XML object and load it in                  
                    Stream fileStream = file.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;
                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        using (RewardsDA rewardsData = new RewardsDA())
                        {
                            IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                            model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                            model.AllowImport = allGood;
                            return View("Upload", model);
                        }
                    }

                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    reader.Close();

                    DataTable dataTable = result.Tables["Loyalty Data"];
                    string[] columns = { "Member Id", "Points", "Business Name", "Loyalty Number", "Load", "Record Date" };
                    string[] columnNames = dataTable.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    var unmatched = columns.Except(columnNames).Concat(columnNames.Except(columns)).Skip(1).ToArray();

                    if (unmatched.Count() > 0)
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        using (RewardsDA rewardsData = new RewardsDA())
                        {
                            IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                            model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                            model.AllowImport = allGood;
                            return View("Upload", model);
                        }
                    }

                    foreach (DataRow row in dataTable.Select("[Load] = 'Y'"))
                    {
                        LoyaltyUploadFileModel loyaltyUploadFileModel = new LoyaltyUploadFileModel()
                        {
                            MemberId = row["Member Id"].ToString(),
                            Points = row["Points"].ToString(),
                            Name = row["Business Name"].ToString(),
                            LoyaltyNumber = row["Loyalty Number"].ToString(),
                            Load = row["Load"].ToString(),
                            RecordDate = row["Record Date"].ToString(),
                            Status = "Not Imported In Phoenix",
                        };
                        loyaltyUploadFileModel.Verify();
                        loyaltyUploadFileModels.Add(loyaltyUploadFileModel);
                    };

                    model.FileRecords = loyaltyUploadFileModels;
                    if (file == null || file.FileName == null)
                    {
                        ViewBag.Message = "Please upload file.";
                    }
                    else
                    {
                        model.FileName = file.FileName;
                    }
                    model.SelectionOrganisationId = model.SelectionOrganisationId;
                    ViewBag.UploadError = loyaltyUploadFileModels.IsValid().Split('|');

                    Session["loyaltyUploadFileModel"] = loyaltyUploadFileModels;

                    allGood = true;
                }
            }



            using (RewardsDA rewardsData = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                model.AllowImport = allGood;
                return View("Upload", model);
            }
        }

    }
}
