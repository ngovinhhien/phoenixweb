﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LGHelper.PhoenixWebAPI;
using PhoenixObjects.Base;
using PhoenixWeb.Models.ViewModel.FraudMonitoring;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class FraudMonitoringController : Controller
    {
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult FraudTransaction()
        {
            FraudTransactionFilter filterModel = new FraudTransactionFilter();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();

            filterModel.ListBusinessType = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_BUSINESS_TYPE).Data;
            filterModel.BusinessTypeKey = int.Parse(filterModel.ListBusinessType.Where(i => i.Selected == true).FirstOrDefault().Value);
            filterModel.ListFraudAlert = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_FRAUD_ALERT).Data;
            filterModel.FraudAlertKey = int.Parse(filterModel.ListFraudAlert.Where(i => i.Selected == true).FirstOrDefault().Value);
            filterModel.ListQuarantineStaus = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_QUARANTINE_STATUS).Data;
            filterModel.QuarantineStatus = filterModel.ListQuarantineStaus.Where(i => i.Selected == true).FirstOrDefault().Value;
            filterModel.ListTransactionStatus = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_TRANSACTION_STATUS).Data;
            filterModel.TransactionStatus = filterModel.ListTransactionStatus.Where(i => i.Selected == true).FirstOrDefault().Value;
            return View(filterModel);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult FilterFraudTransaction(FraudTransactionFilter model)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["searchText"] = model.SearchText;
            query["fromDate"] = model.FromDate;
            query["toDate"] = model.ToDate;
            query["businessTypeKey"] = model.BusinessTypeKey.ToString();
            query["quarantineStatus"] = model.QuarantineStatus;
            query["fraudAlertKey"] = model.FraudAlertKey.ToString();
            query["transactionStatus"] = model.TransactionStatus;
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<DataSearchResponse<FraudTransaction>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_FRAUD_TRANSACTION + uriBuilder.Query.ToString());

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult QuarantineTransaction(int docketKey, string action)
        {
            var parameters = new
            {
                docketKey = docketKey,
                actionType = action,
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Post<object>(PhoenixWebAPIEndpoint.FRAUDMONITORING_FRAUD_TRANSACTION, parameters);

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult MemberReview(int memberKey)
        {
            var model = new Member { MemberKey = memberKey };
            return View(model);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberByMemberKey(int memberKey)
        {
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<MemberReviewVM>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER + "/" + memberKey.ToString() + "?phwUserName=" + User.Identity.Name);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberNotesByMemberKey(FilterModel model, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponse<MemberNoteVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_NOTE + "/" + memberKey.ToString() + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult AddMemberNote()
        {
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult AddMemberNote(MemberNoteVM model)
        {
            var parameters = new
            {
                memberKey = model.MemberKey,
                noteDescription = model.NoteDesciption,
                isHighPriority = bool.Parse(model.NoteType),
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Post<DataSearchResponse<MemberNoteVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_NOTE, parameters);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult FilterMemberTransaction(MemberTransactionFilterVM model, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["fromDate"] = model.FromDate;
            query["toDate"] = model.ToDate;
            query["terminalId"] = model.TerminalId;
            query["totalAmount"] = string.IsNullOrEmpty(model.TotalAmount) ? string.Empty : model.TotalAmount;
            query["cardNumber"] = model.CardNumber;
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<DataSearchResponse<MemberTransactionVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_TRANSACTIONS + "/" + memberKey.ToString() + uriBuilder.Query.ToString());

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberOverviewByMemberKey(int memberKey)
        {
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<MemberOverviewVM>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_OVERVIEW + "/" + memberKey.ToString() + "?phwUserName=" + User.Identity.Name);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetPaymentHistory(FilterModel model, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponseWithExtensionObject<MemberPaymentHistoryVM, Latestpayment>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_PAYMENT_HISTORY + "/" + memberKey.ToString() + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetPaymentDetails(FilterModel model, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponse<MemberPaymentDetailVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_PAYMENT_DETAILS + "/" + memberKey.ToString() + uriBuilder.Query.ToString(), 90000);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberPendingTransaction(FilterModel model, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponseWithExtensionObject<MemberPendingTransactionVM, PendingTransactionTotal>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_PENDING_TRANSACTION + "/" + memberKey.ToString() + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberQuarantinedTransaction(FilterModel model, string totalAmount, string cardNumber, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["totalAmount"] = totalAmount;
            query["cardNumber"] = cardNumber;
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponse<MemberQuarantinedTransactionVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_QUARANTINED_TRANSACTION + "/" + memberKey.ToString() + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult CreateChargeback(int memberKey, int docketKey)
        {
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            var res = client.Get<ChargebackData>(PhoenixWebAPIEndpoint.FRAUDMONITORING_CHARGEBACK_DATA + "/" + memberKey + "/" + docketKey + uriBuilder.Query.ToString());
            var model = new ChargebackTransactionDetail();
            if (res.IsSuccess && res.Data != null)
            {
                model = res.Data.ChargebackTransactionDetail;
                model.ChargebackReasons = res.Data.ChargebackReasons;
                model.Member = model.MemberId + " - " + model.TradingName;
                if (model.ChargebackAmount == 0)
                {
                    model.Error = "Maximum amount for chargebacks has been reached.";
                }
            }
            else
            {
                model.Error = res.Message;
            }
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult CreateChargeback(ChargebackTransactionDetail model)
        {
            var parameters = new
            {
                docketKey = model.DocketKey,
                memberKey = model.MemberKey,
                chargebackAmount = model.ChargebackAmount,
                chargebackFee = model.ChargebackFee,
                chargebackReasonKey = model.ChargebackReasonKey,
                otherChargebackReason = model.OtherChargebackReason,
                disputeReferenceNo = model.DisputeReferenceNo,
                freshdeskTicketNo = model.FreshdeskTicketNo,
                chargebackNote = model.ChargebackNote,

                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Post<object>(PhoenixWebAPIEndpoint.FRAUDMONITORING_CHARGEBACK_TRANSACTION, parameters);

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetMemberRaisedRequest(FilterModel model, string searchText, int memberKey, string requestTypeId)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["searchText"] = searchText;
            query["requestTypeId"] = requestTypeId;
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DataSearchResponse<MemberRaisedRequestVM>>(PhoenixWebAPIEndpoint.FRAUDMONITORING_MEMBER_RAISED_REQUEST + "/" + memberKey.ToString() + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult DocketTransactionDetail(int docketKey, int memberKey)
        {
            var model = new DocketTransactionDetailVM { DocketKey = docketKey, MemberKey = memberKey};
            return View(model);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult DocketTransactionDetailData(int docketKey, int memberKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<DocketTransactionDetailVM>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_GET_MEMBER_TRANSACTION_PAYMENT_DETAIL + "/" + memberKey + "/" + docketKey + uriBuilder.Query.ToString());
            if (res.Data != null)
            {
                res.Data.Member = res.Data.MemberId + " - " + res.Data.TradingName;
                if (res.Data.MemberType == "LE")
                {
                    res.Data.DisplayFields = "Member,DocketKey,MemberKey,MemberId,TradingName,CompanyKey,BusinessType,MemberType,TerminalId,TransactionDate,TransactionResponse,CardType,CardNumber,CardHolderName,CardMode,MSF,TotalPaidAmount,PaymentStatus,ProcessedDate,FailedDate,LodgementReferenceNo,TransactionAmountLE";
                }
                else if (res.Data.MemberType == "Taxi")
                {
                    res.Data.DisplayFields = "Member,DocketKey,MemberKey,MemberId,TradingName,CompanyKey,BusinessType,MemberType,TerminalId,TransactionDate,TransactionResponse,CardType,CardNumber,CardHolderName,CardMode,MSF,TotalPaidAmount,PaymentStatus,ProcessedDate,FailedDate,LodgementReferenceNo,FareAmountTaxi,ExtrasAmountTaxi,AutoTipAmountTaxi,TotalFareTaxi,AutoTipFeeAmountTaxi,LevyChargeAmountTaxi";
                }

                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ResultModel<object> model = new ResultModel<object>
                {
                    Message = res.Message,
                    IsSuccess = res.IsSuccess,
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult TransactionNote(int docketKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<List<TransactionNote>>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_GET_TRANSACTION_NOTES + "/" + docketKey + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult RelatedTransaction(int docketKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<List<RelatedTransaction>>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_GET_RELATED_TRANSACTIONS + "/" + docketKey + uriBuilder.Query.ToString());
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult AddTransNote()
        {
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult AddTransNote(TransactionNote model)
        {
            var parameters = new
            {
                docketKey = model.DocketKey,
                noteDescription = model.NoteDescription,
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Post<object>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_GET_TRANSACTION_NOTES, parameters);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult GetRaisedRequestType()
        {
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_GET_RAISED_REQUEST_TYPES);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult CancelChargeback(RelatedTransaction model)
        {
            var parameters = new
            {
                RequestID = model.RaiseRequestID,
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Post<object>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_CANCEL_CHARGEBACK, parameters);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult ReverseChargeback(RelatedTransaction model)
        {
            var parameters = new
            {
                RequestID = model.RaiseRequestID,
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var res = client.Post<object>(PhoenixWebAPIEndpoint.FRAUD_MONITORING_CREATE_REVERSE_CHARGEBACK, parameters);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult CreateReverse(int memberKey, int docketKey)
        {
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            var res = client.Get<ReverseData>(PhoenixWebAPIEndpoint.FRAUDMONITORING_REVERSE_DATA + "/" + memberKey + "/" + docketKey + uriBuilder.Query.ToString());
            var model = new ReverseTransactionDetail();
            if (res.IsSuccess && res.Data != null)
            {
                model = res.Data.ReverseTransactionDetail;
                model.ReverseAmount = res.Data.ReverseTransactionDetail.TotalAmount;
                model.ReverseReasons = res.Data.ReverseReasons;
                model.Member = model.MemberId + " - " + model.TradingName;
            }
            else
            {
                model.Error = res.Message;
            }

            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult CreateReverse(ReverseTransactionDetail model)
        {
            var parameters = new
            {
                docketKey = model.DocketKey,
                memberKey = model.MemberKey,
                reverseReasonKey = model.ReverseReasonKey,
                otherReverseReason = model.OtherReverseReason,
                freshdeskTicketNo = model.FreshdeskTicketNo,
                note = model.ReverseNote,
                phwUserName = User.Identity.Name,
            };
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Post<object>(PhoenixWebAPIEndpoint.FRAUDMONITORING_REVERSE_TRANSACTION, parameters);

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

    }
}