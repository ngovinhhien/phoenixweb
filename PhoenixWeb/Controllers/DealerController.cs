﻿using Phoenix_BusLog.Data;
using Phoenix_BusLog.Excel;
using Phoenix_Service;
using PhoenixObjects.Member;
using PhoenixObjects.Terminal;
using PhoenixObjects.Transaction;
using PhoenixWeb.Models.ViewModel;
using PhoenixWeb.ReportingWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PhoenixWeb.Controllers
{
    public class DealerController : PhoenixWebController
    {
        //
        // GET: /Dealer/              
        public ActionResult Index()
        {
            Session["count"] = 0;
            MemberDA mdaa = new MemberDA();


            DealerDashboardVM model = new DealerDashboardVM();
             //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);
            OperatorDA opDA = new OperatorDA();
            var OptCount = opDA.GetOperatorCountDealerKey(u.Dealerkey.GetValueOrDefault(0));
            model.OperatorCount = OptCount;
            model.Dealer = mdaa.GetMemeber(u.Dealerkey.GetValueOrDefault(0));

            TerminalDA tda = new TerminalDA();
            model.TerminalCount = tda.GetDealersOperatorTerminalCount(u.Dealerkey.GetValueOrDefault(0));

            DealerDA dda = new DealerDA();
            model.WeeklyTopPerformers = dda.GetTop5OperatorPerformer_Weekly(u.Dealerkey.GetValueOrDefault(0));
            model.WeeklyLeastPerformers = dda.GetLeast5OperatorPerformer_Weekly(u.Dealerkey.GetValueOrDefault(0));
            model.MonthlyTopPerformers = dda.GetTop5OperatorPerformer_Monthly(u.Dealerkey.GetValueOrDefault(0));
            model.MonthlyLeastPerformers = dda.GetLeast5OperatorPerformer_Monthly(u.Dealerkey.GetValueOrDefault(0));
            model.NoTransactionIn7Days = dda.GetNoTransactionIn7Days(u.Dealerkey.GetValueOrDefault(0));
            model.NoTransactionIn30Days = dda.GetNoTransactionIn30Days(u.Dealerkey.GetValueOrDefault(0));

            model.WeeklyTop5PerformersByTerminal = dda.GetTop5OperatorPerformerByTerminal_Weekly(u.Dealerkey.GetValueOrDefault(0));
            model.MonthlyTop5PerformersByTerminal = dda.GetTop5OperatorPerformerByTerminal_Monthly(u.Dealerkey.GetValueOrDefault(0));
            model.WeeklyLeast5PerformersByTerminal = dda.GetLeast5OperatorPerformerByTerminal_Weekly(u.Dealerkey.GetValueOrDefault(0));
            model.MonthlyLeast5PerformersByTerminal = dda.GetLeast5OperatorPerformerByTerminal_Monthly(u.Dealerkey.GetValueOrDefault(0));


             
            return View(model);
        }


     
        public ActionResult Operator()
        {
            DealerDA tda = new DealerDA();
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            model = tda.GetDealerOperators(u.Dealerkey.GetValueOrDefault(0));
            model.MemberList = model.MemberList.OrderBy(n => n.TradingName).ToList();
            return View(model);
        }

        public ActionResult Terminals()
        {
            DealerDA tda = new DealerDA();
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);
            List<OperatorTerminalModel> model = new List<OperatorTerminalModel>();
            model = tda.GetDealerOperatorsTerminals(u.Dealerkey.GetValueOrDefault(0));
            return View(model);
        }

        public ActionResult OperatorPrint(int PayRun_Key, string Type, string lodge, int memberkey)
        {
            byte[] output;
            string extension, mimeType, encoding;
            ReportingWebService.Warning[] warnings;
            string[] streamIds;
            string PayRunId = string.Empty;
            string MemberId = string.Empty;
            string format = "PDF";
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var payrun = (from p in con.PayRuns
                              where p.PayRun_key == PayRun_Key
                              select p).FirstOrDefault();
                var memberid = (from p in con.Members
                                where p.Member_key == memberkey
                                select p).FirstOrDefault();

                MemberId = memberid.MemberId;

                if (payrun != null)
                {
                    PayRunId = payrun.PayRunId;
                }
            }
            if (Type == "Bank")
            {
              

                string ReportName = "/LTEPAdminReports/Operator Pay Run Report (Detail)";

                IList<ReportingWebService.ParameterValue> parameters = new List<ParameterValue>();
                parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = MemberId });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayRunId });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "ShowMessage", Value = "false" });

                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return new FileContentResult(output, mimeType);
            }
            else
            {
                string ReportName = "/LTEPAdminReports/Operator Cashing Report";

                IList<ReportingWebService.ParameterValue> parameters = new List<ParameterValue>();
                parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = MemberId });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "RefNumber", Value = lodge });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });


                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return new FileContentResult(output, mimeType);
            }
        }

        public ActionResult Print(int PayRun_Key)
        {
            byte[] output;
            string extension, mimeType, encoding;
            ReportingWebService.Warning[] warnings;
            string[] streamIds;
            string PayRunId = string.Empty;

            DealerDA tda = new DealerDA();
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);


            MemberDA md = new MemberDA();
            var mid = md.GetMemberId(u.Dealerkey.GetValueOrDefault(0));

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var payrun = (from p in con.PayRuns
                              where p.PayRun_key == PayRun_Key
                              select p).FirstOrDefault();
                if (payrun != null)
                {
                    PayRunId = payrun.PayRunId;
                }
            }          
            
                string ReportName = "/LTEPAdminReports/Dealer Pay Report Detail";

                IList<ReportingWebService.ParameterValue> parameters = new List<ParameterValue>();
                parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = mid });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayRunId });

                string format = "PDF";

                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                return new FileContentResult(output, mimeType);            
        }
     
        public ActionResult OperatorPayments(int Operator)
        {
            MemberDA mda = new MemberDA();

            @ViewBag.Operator = Operator;
            return View();
        }
       

        [Authorize]
        public ActionResult OperatorPaymentList(int Operator)
        {
            @ViewBag.Operator = Operator;
            TransactionDA tda = new TransactionDA();           
            List<TransactionModel> model = new List<TransactionModel>();
            model = tda.GetDealerTransactionPayments(Operator);
            return View(model);
        }

        [Authorize]
        public ActionResult OperatorSubList(int transaction_key)
        {
            List<oTransactionDetailsModel> model = new List<oTransactionDetailsModel>();

            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                var sTra = from k in con.Portal_TransactionDetails_TaxiEpay_GetByMemberKey_Paid(transaction_key)
                           select k;

                foreach (var st in sTra)
                {
                    oTransactionDetailsModel mod = new oTransactionDetailsModel();
                    mod.Amount = st.FaceValue;
                    mod.MSF = st.OperatorCommission;
                    mod.CardType = st.CardName;
                    mod.CardNumber = st.CardNumberMasked;
                    mod.TransType = st.transtype;
                    mod.Date = st.ItemDate.Value;
                    mod.TransactionDate = st.ItemDate.Value;
                    mod.Total = st.TotalPaid.Value;
                    mod.RateUsed = st.OperatorCommissionRate;
                    decimal rate = Math.Abs(st.OperatorCommissionRate) * 100;
                    int rateint = Convert.ToInt32(rate);
                    if (rateint > 10)
                    {
                        string per = "-$";
                        string perSub = String.Format("{0:###,##0.00}", (rate / 100));

                        string pers = string.Empty;

                        if (perSub.Length <= 4)
                        {
                            pers = perSub.Substring(0, 4);
                        }
                        else
                        {
                            pers = perSub.Substring(0, 5);
                        }
                        mod.RateString = per + pers;
                    }
                    else
                    {
                        string per = "%";
                        string perSub = String.Format("{0:###,##0.00}", (mod.RateUsed * 100));
                        string pers = string.Empty;

                        if (perSub.Length <= 4)
                        {
                            pers = perSub.Substring(0, 4);
                        }
                        else
                        {
                            pers = perSub.Substring(0, 5);
                        }
                        mod.RateString = pers + per;
                    }
                    model.Add(mod);
                }
            }
            Session["OperatorSubList"] = model;
            return View(model);
        }

        public ActionResult Payment()
        {           
            return View();
        }
        [Authorize]
        public ActionResult PaymentList()
        {
            TransactionDA tda = new TransactionDA();
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);
            List<TransactionModel> model = new List<TransactionModel>();
            model = tda.GetDealerTransactionPayments(u.Dealerkey.GetValueOrDefault(0));
            return View(model);
        }
        [Authorize]
        public ActionResult SubList(int transaction_key)
        {        
            List<TransactionDetailsModel> model = new List<TransactionDetailsModel>();
           
                using (TaxiEpayEntities con = new TaxiEpayEntities())
                {
                    var sTra = from k in con.Dealer_TransactionDetails_GetByTransactionkey_Paid(transaction_key)
                                select k;
                
                    foreach (var st in sTra)
                    {
                        TransactionDetailsModel mod = new TransactionDetailsModel();
                        mod.Amount = st.Amount.Value;
                        mod.Description = st.Description;
                        mod.ItemDate = st.ItemDate.Value;
                        mod.RateUsed = st.RateUsed;
                        mod.Fare = st.Fare;
                        mod.Extras = st.Extras;
                        mod.TerminalId = st.TerminalId;
                        mod.VehicleId = st.VehicleId;
                        decimal rate = Math.Abs(st.RateUsed.GetValueOrDefault(0)) * 100;
                        int rateint = Convert.ToInt32(rate);
                        if (rateint > 10)
                        {
                            string per = "-$";
                            string perSub = String.Format("{0:###,##0.00}", (rate / 100));

                            string pers = string.Empty;

                            if (perSub.Length <= 4)
                            {
                                pers = perSub.Substring(0, 4);
                            }
                            else
                            {
                                pers = perSub.Substring(0, 5);
                            }
                            mod.RateString = per + pers;
                        }
                        else
                        {
                            string per = "%";
                            string perSub = String.Format("{0:###,##0.00}", (mod.RateUsed * 100));
                            string pers = string.Empty;

                            if (perSub.Length <= 4)
                            {
                                pers = perSub.Substring(0, 4);
                            }
                            else
                            {
                                pers = perSub.Substring(0, 5);
                            }
                            mod.RateString = pers + per;
                        }
                        model.Add(mod);
                    }
                }         
            Session["SubList"] = model;
            return View(model);
        }

        [Authorize]
        public ActionResult ExportPayment()
        {
            List<TransactionDetailsModel> model = new List<TransactionDetailsModel>();
            List<PaymentPackage> PackageModel = new List<PaymentPackage>();
            model = (List<TransactionDetailsModel>)Session["SubList"];
            if (model != null && model.Count() > 0)
            {
                foreach (var st in model)
                {
                    PaymentPackage mod = new PaymentPackage();
                    mod.Date = st.ItemDate.Value.ToShortDateString() + ":" + st.ItemDate.Value.ToShortTimeString();
                    mod.Amount = st.Amount.GetValueOrDefault(0);
                    mod.RateUsed = st.RateString;

                    mod.Fare = st.Fare.ToString();
                    mod.Extras = st.Extras.ToString();
                    mod.TerminalId = st.TerminalId.ToString();
                    mod.VehicleId = st.VehicleId;
                    //mod.MSF = st.;
                    //mod.Total = st;
                  
                    PackageModel.Add(mod);
                }

                if (PackageModel.Count() > 0)
                {
                    try
                    {                
                        List<string> headerNames =
                           new List<string> { "Transaction Date", 
                   "Dealer Commission", "Dealer Rate","FareValue","Extras","TerminalId","VehicleId" };

                        ExcelHelper excelFacade = new ExcelHelper();
                        excelFacade.Create<PaymentPackage>(@"C:\temp\Payments.xlsx",
                                    PackageModel, "Payments", headerNames);
                    }
                    catch { }
                }
                return File(@"C:\temp\Payments.xlsx", "application/xlsx", "Payments.xlsx");
            }
            else
            {
                return RedirectToActionPermanent("Index", "Payments");

            }

        }

        [Authorize]
        public ActionResult OperatorExportPayment()
        {
            List<oTransactionDetailsModel> model = new List<oTransactionDetailsModel>();
            List<oPaymentPackage> PackageModel = new List<oPaymentPackage>();
            model = (List<oTransactionDetailsModel>)Session["OperatorSubList"];
            if (model != null && model.Count() > 0)
            {
                foreach (var st in model)
                {
                    oPaymentPackage mod = new oPaymentPackage();
                    mod.Date = st.Date.ToShortDateString() + ":" + st.Date.ToShortTimeString();
                    mod.Amount = st.Amount;
                    mod.MSF = st.MSF;
                    mod.Total = st.Total;
                    mod.RateUsed = st.RateString;

                    PackageModel.Add(mod);
                }


                if (PackageModel.Count() > 0)
                {
                    try
                    {
                        List<string> headerNames =
                           new List<string> { "Transaction Date", 
                   "Amount", "Commission", 
                   "Total", "Rate Used" };

                        ExcelHelper excelFacade = new ExcelHelper();
                        excelFacade.Create<oPaymentPackage>(@"C:\temp\Payments.xlsx",
                                    PackageModel, "Payments", headerNames);
                    }
                    catch { }
                }
                return File(@"C:\temp\Payments.xlsx", "application/xlsx", "Payments.xlsx");
            }
            else
            {
                return RedirectToActionPermanent("Index", "Payments");

            }

        }

        public ActionResult Transaction()
        {
            return View();
        }
        public JsonResult GetTansData()
        {
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);
            TransactionDA gda = new TransactionDA();
            var data1 = gda.GetTransactionData(u.Dealerkey.GetValueOrDefault(0)).ToList();
            data1.Reverse();
            var data = data1.Take(8);
            return Json(data.Reverse(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult OpDetails(string Id)
        {
             MemberDA mda = new MemberDA();
            var mkey = mda.GetMemberKey(Id);
            return RedirectToAction("Details", new { Id = mkey });
        }

        public ActionResult Details(int Id)
        {
            MemberDA mda = new MemberDA();
            MemberModel model = new MemberModel();
            model = mda.GetMemeber(Id);
            if (model.DOB != null)
            {
                string dobDate = model.DOB.Value.ToShortDateString();
                string[] dobDateArry = dobDate.Split('/');
                model.DOBstring = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            }
            if (model.PhotoIDExpiry != null)
            {
                string ExpiryDate = model.PhotoIDExpiry.Value.ToShortDateString();
                string[] ExpiryDateArry = ExpiryDate.Split('/');
                model.PhotoIDExpiryString = ExpiryDateArry[2].PadLeft(4, '0') + "-" + ExpiryDateArry[1].PadLeft(2, '0') + "-" + ExpiryDateArry[0].PadLeft(2, '0');
            }
            Session["Member_key"] = Id;
            Session["Company_key"] = model.CompanyKey;
            model.RentalMemberRentalTemplateKey = 7;
            TerminalRentalRateDA trda = new TerminalRentalRateDA();

            List<TerminalRentalRateModel> rmodel = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value);

            model.TerminalRentalRateList = rmodel.OrderBy(n => n.TerminalRentalRate_key).ToList();

            DrawDropDownConfig(model.CompanyKey);
            return View(model);
        }
        private void DrawDropDownConfig(int company_key)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();

            ViewBag.MemberRentalTemplateList = MemberRentalTemplateList.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.MemberRentalTemplateKey.ToString()
            }).ToList();
            List<string> PaymentType = new List<string>();
            PaymentType.Add("Cash");
            PaymentType.Add("Bank");
            ViewBag.PaymentTypeList = PaymentType.Select(item => new SelectListItem
            {
                Text = item,
                Value = item
            }).ToList();

            BusinessTypeDA bda = new BusinessTypeDA();
            var BusinessTypeList = bda.Get().ToList();
            ViewBag.BusinessTypeList = BusinessTypeList.Select(item => new SelectListItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString()
            }).ToList();

            DealerDA dda = new DealerDA();
            var DealerList = dda.GetDealersByCompanykey(company_key).ToList();
            ViewBag.Dealers = DealerList.Select(item => new SelectListItem
            {
                Text = item.DealerName,
                Value = item.Dealer_key.ToString()
            }).ToList();

        }

        [Authorize]
        public ActionResult Search(decimal? amountFrom, decimal? amountTo, DateTime? dateFrom, DateTime? dateTo, string TerminalId, string Batchno, long? DriverId, string TaxiId)
        {
            List<sTransactionDetailsModel> modelReport = new List<sTransactionDetailsModel>();
            if (TerminalId == "")
                TerminalId = null;
            if (Batchno == "")
                Batchno = null;
            if (TaxiId == "")
                TaxiId = null;

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA userda = new UserDA();
            var u = userda.GetUsers(uId);          
            List<sTransactionParentModel> modelResult = new List<sTransactionParentModel>();
            List<Dealer_TransactionDetails_SearchTransactionsByTerminalIdBatch_Result> ResultList = new List<Dealer_TransactionDetails_SearchTransactionsByTerminalIdBatch_Result>();
            using (TaxiEpayEntities con = new TaxiEpayEntities())
            {
                if (amountFrom == null && amountTo == null && dateFrom == null && dateTo == null && TerminalId == null && Batchno == null && DriverId == null && TaxiId == null)
                {
                    ResultList = (from k in con.Dealer_TransactionDetails_SearchTransactionsByTerminalIdBatch(u.Dealerkey.GetValueOrDefault(0), dateFrom, dateTo, amountFrom, amountTo, TerminalId, Batchno, DriverId, TaxiId)
                                  select k).Take(20).ToList();
                }
                else
                {
                    ResultList = (from k in con.Dealer_TransactionDetails_SearchTransactionsByTerminalIdBatch(u.Dealerkey.GetValueOrDefault(0), dateFrom, dateTo, amountFrom, amountTo, TerminalId, Batchno, DriverId, TaxiId)
                                  select k).ToList();
                }

                 //int cn = (int)Session["count"];
                 //if (cn == 0)
                 //{
                 //    ResultList = ResultList.Take(10).ToList();
                 //    cn++;
                 //    Session["count"] = cn;
                 //}
                foreach (var st in ResultList)
                {                    
                    
                    sTransactionDetailsModel mod = new sTransactionDetailsModel();
                    mod.Amount = st.FaceValue.Value;
                    mod.MSF = st.OperatorCommission;
                    mod.CardType = st.CardName;
                    mod.CardNumber = st.CardNumberMasked;
                    mod.TransType = st.transtype;
                    mod.Date = st.ItemDate.Value;
                    mod.TransactionDate = st.ItemDate.Value;
                    mod.Total = st.TotalPaid.Value;
                    mod.TerminalId = st.Terminalid;
                    mod.TransactionDescription = st.TransactionDescription;
                    mod.Tkey = st.Tkey;
                    mod.Batchno = st.BatchNumber;
                    mod.DriverId = st.Driverid;
                    mod.TaxiId = st.TaxiId;
                    mod.EndOfShift = st.EndOfShift;
                    if (st.PayStatus == "Paid")
                    {
                        var tran = (from t in con.TransactionDetails
                                    where t.TransactionDetail_key == st.Tkey
                                    select t).SingleOrDefault();

                        //  mod.TransactionDate = tran.Transaction.TransactionDate.Value;
                        mod.Status = st.PayStatus + " (" + mod.TransactionDate + ")";
                    }
                    else
                    {
                        mod.Status = st.PayStatus;
                    }
                    mod.Total = st.TotalPaid.Value;
                    modelReport.Add(mod);

                }

                Session["Report"] = modelReport;

                foreach (var st in ResultList)
                {
                    sTransactionParentModel model1 = new sTransactionParentModel();
                    model1 = (from n in modelResult
                              where n.Batchno == st.BatchNumber && n.TerminalId == st.Terminalid
                              select n).FirstOrDefault();
                    if (model1 == null)
                    {
                        sTransactionParentModel model = new sTransactionParentModel();
                        model.TotalAmount = model.TotalAmount + st.FaceValue.Value;
                        model.TotalMSF = model.TotalMSF + st.OperatorCommission;
                        model.Total = model.Total + st.TotalPaid.Value;
                        model.Batchno = st.BatchNumber;
                        model.TerminalId = st.Terminalid;
                        model.Id = st.Terminalid + "" + st.BatchNumber;

                        sTransactionDetailsModel mod = new sTransactionDetailsModel();
                        mod.Amount = st.FaceValue.Value;
                        mod.MSF = st.OperatorCommission;
                        mod.CardType = st.CardName;
                        mod.CardNumber = st.CardNumberMasked;
                        mod.TransType = st.transtype;
                        mod.Date = st.ItemDate.Value;
                        mod.TransactionDate = st.ItemDate.Value;
                        mod.Total = st.TotalPaid.Value;
                        mod.TerminalId = st.Terminalid;
                        mod.TransactionDescription = st.TransactionDescription;
                        mod.Tkey = st.Tkey;
                        mod.Batchno = st.BatchNumber;
                        mod.DriverId = st.Driverid;
                        mod.TaxiId = st.TaxiId;
                        mod.EndOfShift = st.EndOfShift;
                        if (st.PayStatus == "Paid")
                        {
                            var tran = (from t in con.TransactionDetails
                                        where t.TransactionDetail_key == st.Tkey
                                        select t).SingleOrDefault();
                        
                            mod.Status = st.PayStatus + " (" + mod.TransactionDate + ")";
                        }
                        else
                        {
                            mod.Status = st.PayStatus;
                        }
                        mod.Total = st.TotalPaid.Value;
                        model.Details.Add(mod);

                        modelResult.Add(model);
                    }
                    else
                    {
                        modelResult.Remove(model1);
                        sTransactionDetailsModel ChildModel = new sTransactionDetailsModel();
                        ChildModel.Amount = st.FaceValue.Value;
                        ChildModel.MSF = st.OperatorCommission;
                        ChildModel.CardType = st.CardName;
                        ChildModel.CardNumber = st.CardNumberMasked;
                        ChildModel.TransType = st.transtype;
                        ChildModel.Date = st.ItemDate.Value;
                        ChildModel.TransactionDate = st.ItemDate.Value;
                        ChildModel.Total = st.TotalPaid.Value;
                        ChildModel.TerminalId = st.Terminalid;
                        ChildModel.TransactionDescription = st.TransactionDescription;
                        ChildModel.Tkey = st.Tkey;
                        ChildModel.Batchno = st.BatchNumber;
                        ChildModel.DriverId = st.Driverid;
                        ChildModel.TaxiId = st.TaxiId;
                        ChildModel.EndOfShift = st.EndOfShift;
                        if (st.PayStatus == "Paid")
                        {
                            var tran = (from t in con.TransactionDetails
                                        where t.TransactionDetail_key == st.Tkey
                                        select t).SingleOrDefault();
                         
                            ChildModel.Status = st.PayStatus + " (" + ChildModel.TransactionDate + ")";
                        }
                        else
                        {
                            ChildModel.Status = st.PayStatus;
                        }
                        ChildModel.Total = st.TotalPaid.Value;
                        model1.Details.Add(ChildModel);
                        model1.TotalAmount = model1.TotalAmount + st.FaceValue.Value;
                        model1.TotalMSF = model1.TotalMSF + st.OperatorCommission;
                        model1.Total = model1.Total + st.TotalPaid.Value;
                        model1.Batchno = st.BatchNumber;
                        modelResult.Add(model1);
                    }
                }
            }

            return View("List", modelResult);
        }
        [Authorize]
        public ActionResult Export()
        {
            List<sTransactionDetailsModel> sTranDetails = new List<sTransactionDetailsModel>();
            List<TransactionPackage> PackageModel = new List<TransactionPackage>();

            sTranDetails = (List<sTransactionDetailsModel>)Session["Report"];

            foreach (var st in sTranDetails)
            {
                TransactionPackage mod = new TransactionPackage();
                mod.Date = st.Date.ToShortDateString() + " " + st.Date.ToShortTimeString();
                mod.Amount = st.Amount;
                mod.MSF = st.MSF;
                mod.Total = st.Total;
                mod.CardNumber = st.CardNumber;
                mod.CardType = st.CardType;
                mod.TransType = st.TransType;
                mod.Status = st.Status;
                mod.TerminalId = st.TerminalId;
                mod.PaymentReference = st.PaymentReference;
                mod.DriverId = st.DriverId;
                mod.TaxiId = st.TaxiId;
                mod.EndOfShift = st.EndOfShift;
                PackageModel.Add(mod);
            }

            if (PackageModel.Count() > 0)
            {
                try
                {
                    List<string> headerNames =
                       new List<string> { "Transaction Date", "TerminalId", 
                   "Amount", "Commission", 
                   "Total", "Card Number","Card Type", "Transaction Type", "Payment Status", "PaymentReference", "DriverId","TaxiId","EndOfShiftDate"};

                    ExcelHelper excelFacade = new ExcelHelper();
                    excelFacade.Create<TransactionPackage>(@"C:\temp\transactions.xlsx",
                                PackageModel, "Transactions", headerNames);

                    return File(@"C:\temp\transactions.xlsx", "application/xlsx", "transactions.xlsx");
                }
                catch { }
            }
            else
            { }
            return RedirectToAction("Index");
        }
    }
}
