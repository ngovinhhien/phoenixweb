﻿using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using PhoenixObjects.Enums;
using PhoenixObjects.Glidebox;
using PhoenixWeb.Models.Contants;
using PhoenixWeb.Models.ViewModel.Glidebox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        public ActionResult Index(int? categoryID)
        {
            ViewBag.CategoryID = categoryID;
            var categoryDA = new CategoriesDA();

            #region Manage product page
            var selectListCategoryItem = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Selected = true,
                    Text = "Select Category",
                    Value = "-1"
                }
            };

            selectListCategoryItem.AddRange(categoryDA.GetAllCategory().OrderBy(x => x.CategoryName)
                                                                    .Select(c => new SelectListItem
                                                                    {
                                                                        Value = c.CategoryID.ToString(),
                                                                        Text = c.CategoryName
                                                                    }));
            ViewBag.Categories = selectListCategoryItem;
            #endregion

            #region Add/Edit product
            var selectListItem = new List<SelectListItem>();
            var categorySelectList = categoryDA.GetAllCategory().Where(x => x.IsActive == true)
                                                                .OrderBy(x => x.CategoryName)
                                                                .Select(c => new SelectListItem
                                                                {
                                                                    Value = c.CategoryID.ToString(),
                                                                    Text = c.CategoryName
                                                                });
            selectListItem.AddRange(categorySelectList);
            ViewBag.ProductCategories = selectListItem;

            int.TryParse(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.GlideboxProductImageMaxSize.ToString()), out int imageMaxSize);
            ViewBag.GlideboxProductImageMaxSize = imageMaxSize;
            #endregion

            return View();
        }

        public JsonResult GetProducts(ProductVM search)
        {
            var productDA = new ProductDA();
            var jsonData = productDA.GetProducts(search.PageSize, search.CurrentPage, search.CategoryID, search.SearchText, search.SortName, search.SortOrder);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActivateDeactivateProduct(ActivateDeactivateProductModel model)
        {
            var errorMessage = string.Empty;
            var modelErrors = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (var modelError in modelState.Errors)
                        {
                            modelErrors.Add(modelError.ErrorMessage);
                        }
                    }
                    errorMessage = string.Join("<br>", modelErrors);
                }
                else
                {
                    var productDA = new ProductDA();

                    var isSuccess = productDA.ActivateDeactivateProduct(model, User.Identity.Name);
                    if (!isSuccess)
                    {
                        errorMessage = ErrorMessageConstant.SystemError;
                    }
                }
            }
            catch (Exception)
            {
                errorMessage = ErrorMessageConstant.SystemError;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProduct()
        {
            var errorMessage = string.Empty;
            var modelErrors = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (var modelError in modelState.Errors)
                        {
                            modelErrors.Add(modelError.ErrorMessage);
                        }
                    }
                    errorMessage = string.Join("<br>", modelErrors);
                }
                else
                {
                    int.TryParse(Request.Params["ProductID"], out int productID);
                    var productName = Request.Params["ProductName"];
                    decimal.TryParse(Request.Params["Price"], out decimal price);
                    int.TryParse(Request.Params["Quantity"], out int quantity);
                    int.TryParse(Request.Params["CategoryID"], out int categoryID);
                    bool.TryParse(Request.Params["IsChangedImage"], out bool isChangedImage);

                    byte[] imageByteArray = null;
                    if (Request.Files.Count > 0)
                    {
                        var imageFile = Request.Files[0];
                        var extension = Path.GetExtension(imageFile.FileName).ToLower();
                        var imageFileSizeMB = (double)imageFile.ContentLength / 1024000;

                        var isValidGlideboxProductImageSizeConfig = int.TryParse(
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.GlideboxProductImageMaxSize.ToString()), out int imageMaxSize);
                        if (!isValidGlideboxProductImageSizeConfig)
                        {
                            return Json(new
                            {
                                IsError = true,
                                ErrorMessage = GlideBoxProductMessageConstant.InvalidConfigGlideboxImageMaxSize
                            }, JsonRequestBehavior.AllowGet);
                        }

                        if (!imageFile.ContentType.Contains("image"))
                        {
                            errorMessage = GlideBoxProductMessageConstant.InvalidImageFile;
                        }
                        else if (extension != ".png" && extension != ".jpg")
                        {
                            errorMessage = GlideBoxProductMessageConstant.InvalidExtensionImageFile;
                        }
                        else if (imageFileSizeMB > imageMaxSize)
                        {
                            errorMessage = GlideBoxProductMessageConstant.ImageSizeTooLarge;
                        }

                        if (string.IsNullOrEmpty(errorMessage))
                        {
                            imageByteArray = new BinaryReader(imageFile.InputStream).ReadBytes(imageFile.ContentLength);
                        }
                    }

                    var model = new ProductModel
                    {
                        ProductID = productID,
                        ProductName = productName,
                        Price = price,
                        Quantity = quantity,
                        ImageContent = imageByteArray,
                        Category = categoryID,
                        IsChangedImage = isChangedImage
                    };

                    var productDA = new ProductDA();

                    if (productDA.IsExistProductName(model))
                    {
                        errorMessage = GlideBoxProductMessageConstant.ProductNameExists;
                    }

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        if (model.ProductID == 0)
                        {
                            productDA.SaveProduct(model, User.Identity.Name);
                        }
                        else
                        {
                            var isSuccess = productDA.EditProduct(model, User.Identity.Name);

                            if (!isSuccess)
                            {
                                errorMessage = ErrorMessageConstant.SystemError;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ErrorMessageConstant.SystemError;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProductDetail(int productID)
        {
            var productDA = new ProductDA();
            var jsonData = productDA.GetProductByID(productID);
            //The product image too large than normal json result so need to response as Content result to client.
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(jsonData),
                ContentType = "application/json"
            };
            return result;
        }

        public JsonResult SearchProductHistory(GlideboxHistoryVM model)
        {
            var productDA = new ProductDA();
            var jsonData = productDA.SearchProductHistory(model.ID, model.PageSize, model.CurrentPage, model.SearchText, model.SortName, model.SortOrder);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}