﻿using Phoenix_BusLog.TransactionHostLog.BusLog;
using Phoenix_BusLog.TransactionHostLog.Model;
using PhoenixObjects.TransactionHostLog;
using PhoenixWeb.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    [RBAC(Permission = "ViewPhoenixWebLog")]
    public class TransactionHostLogController : PhoenixWebController
    {
        //
        // GET: /HostLog/

        public ActionResult Index()
        {
            using (var logDA = new LogDA())
            {
                ViewBag.LevelList = logDA.GetLevel();
            }
            var model = new SearchLogModel
            {
                LoggedDateFrom = DateTime.Now,
                LoggedDateTo = DateTime.Now
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult GetLog(PhoenixObjects.Common.GridModel<SearchLogModel, List<LogModel>> model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var logDA = new LogDA())
                    {
                        logDA.GetWithPaging(model);
                    }
                }

                return new CustomJsonResult() {Data = model};
            }
            catch (SqlException e)
            {
                if (e.Number == -2) //Need specify time out message
                {
                    model.Message = "Timeout error. Please select more filters condition because of this filters contain many rows.";
                }
                else
                {
                    model.Message = "System Error";
                }
                model.IsError = true;
                
                return new CustomJsonResult() {Data = model};
            }
            catch (Exception e)
            {
                model.IsError = true;
                model.Message = "System Error";
                return new CustomJsonResult() { Data = model};
            }
        }
        [HttpGet]
        public ActionResult GetLogger()
        {
            var data = new List<LoggerTypeModel>();
            using (var logDA = new LogDA())
            {
                data = logDA.GetLogger();
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
