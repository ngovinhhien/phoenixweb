﻿using Phoenix_BusLog.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Phoenix_BusLog;
using PhoenixObjects.Enums;
using PhoenixObjects.GlideBoxCategory;
using PhoenixWeb.Models.ViewModel;
using PhoenixWeb.Models.Contants;
using PhoenixWeb.Models.ViewModel.Glidebox;
using DotNetOpenAuth.Messaging;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        //
        // GET: /Category/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCategories(VMBaseWithPaging basicSearch)
        {
            var categoriesDA = new CategoriesDA();
            var jsonData = categoriesDA.GetCategories(basicSearch.PageSize, basicSearch.CurrentPage,
                basicSearch.SearchText, basicSearch.SortOrder, basicSearch.SortName);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BulkAssignCategory()
        {
            ViewBag.MaxCategoryAssignedInTerminal =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MaxCategoryAssignedInTerminal
                    .ToString());

            return View();
        }

        [HttpGet]
        public ActionResult GetActiveCategoryListToAssign(string categoryName, string terminalId)
        {
            try
            {
                var categoryInTerminal = new List<CategoryOrder>();
                var categoryDA = new CategoriesDA();
                var categories = categoryDA.GetAllActiveCategoriesList(categoryName);

                if (!string.IsNullOrEmpty(terminalId))
                {
                    categoryInTerminal = categoryDA.GetCategoryInTerminal(terminalId);
                }

                return Json(new { IsError = false, Message = string.Empty, data = categories, assignData = categoryInTerminal }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { IsError = true, Message = ErrorMessageConstant.SystemError, InnerMessage = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveAssignCategoryToTerminal(BulkAssignCategoryModel bulkData)
        {
            try
            {
                var categoryDA = new CategoriesDA();
                StringBuilder error = new StringBuilder();
                //get the max number of the category assigned to the terminal
                var isValidMaxCategoryConfig = int.TryParse(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MaxCategoryAssignedInTerminal.ToString()), out int maxCategory);
                if (!isValidMaxCategoryConfig)
                {
                    return Json(new { IsError = true, Message = GlideBoxCategoryMessageConstant.InvalidConfigMaxCategoryCanAssign });
                }

                //get all terminal id from client by split via ;
                var terminalIdList = bulkData.TerminalIds.Split(';').Select(x => x.Trim())
                    .Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();
                var categoryList = bulkData.CategoryOrders;

                //check terminal Id is empty
                if (terminalIdList.Count < 1)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.InvalidTerminalBulkAssign);
                }

                //check category Id is empty or lager max category number
                if (categoryList.Count < 1 || categoryList.Count > maxCategory)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategory);
                }

                //check order number is invalid from client
                var hasInvalidOrder = categoryList.Exists(x => x.Order <= 0 || x.Order > maxCategory);
                if (hasInvalidOrder)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategoryOrder);
                }

                //check order number is duplicate from client
                var hasDuplicateOrder = categoryList.GroupBy(x => new {x.Order}).Where(x => x.Skip(1).Any()).Any();
                if (hasDuplicateOrder)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.DuplicateCategoryOrder);
                }

                //check category id is duplicate from client
                var hasDuplicateCategoryId = categoryList.GroupBy(x => new { x.CategoryID }).Where(x => x.Skip(1).Any()).Any();
                if (hasDuplicateCategoryId)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.DuplicateCategoryId);
                }

                //check category id from client is match in DB
                var categorySys =
                    categoryDA.GetCategoryByCategoryList(categoryList);

                if (categorySys.Count != categoryList.Count)
                {
                    error.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategory);
                }

                if (error.Length > 0)
                {
                    return Json(new { IsError = true, Message = error.ToString() });
                }

                //handle create TerminalCategory
                var terminalIdAdded = new List<string>();
                var terminalIdInvalid = new List<string>();
                var terminalIdExist = new List<string>();
                var terminalIdNoSupport = new List<string>();
                var result = categoryDA.MapCategoryBelongToTerminal(terminalIdList, categorySys,
                    out terminalIdAdded, out terminalIdInvalid, out terminalIdExist, out terminalIdNoSupport, User.Identity.Name, SourceContants.BulkAssignCategory);

                //save data to DB in case terminal Id correct
                if (result.Item1.Count > 0)
                {
                    categoryDA.SaveTerminalCategory(result);
                }

                //list of terminal Id correct
                Session["TerminalIdAdded"] = new List<string>();
                if (terminalIdAdded.Count > 0)
                {
                    Session["TerminalIdAdded"] = terminalIdAdded;
                }

                var terminalError = new Dictionary<string, string>();
                //list of terminal Id invalid
                if (terminalIdInvalid.Count > 0)
                {
                    terminalError.AddRange(terminalIdInvalid.Select(x => x).ToDictionary(x => x, x => GlideBoxCategoryMessageConstant.MessageInvalidTerminalList));
                }

                //list of terminal Id have already existed categories
                if (terminalIdExist.Count > 0)
                {
                    terminalError.AddRange(terminalIdExist.Select(x => x).ToDictionary(x => x, x => GlideBoxCategoryMessageConstant.MessageTerminalExistCategories));
                }

                //list of terminal Id belong to Live eftpos
                if (terminalIdNoSupport.Count > 0)
                {
                    terminalError.AddRange(terminalIdNoSupport.Select(x => x).ToDictionary(x => x, x => GlideBoxCategoryMessageConstant.MessageTerminalNoSupport));
                }

                Session["TerminalIdError"] = new Dictionary<string, string>();
                if (terminalError.Count > 0)
                {
                    Session["TerminalIdError"] = terminalError;
                }

                return Json(new { IsError = false, TerminalError = terminalError.OrderBy(x=>x.Key).ToList(), TerminalAdded = terminalIdAdded });
            }
            catch (Exception e)
            {
                return Json(new { IsError = true, Message = ErrorMessageConstant.SystemError, InnerMessage = e.Message });
            }
        }

        public JsonResult UpdateCategoryIndividualTerminal(IndividualAssignCategoryModel individualData)
        {
            try
            {
                StringBuilder errors = new StringBuilder();
                var categoryDA = new CategoriesDA();
                var termialId = individualData.TerminalId;
                var categoryList = individualData.CategoryOrders;

                var isValidMaxCategoryConfig = int.TryParse(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MaxCategoryAssignedInTerminal.ToString()), out int maxCategory);
                if (!isValidMaxCategoryConfig)
                {
                    return Json(new { IsError = true, Message = GlideBoxCategoryMessageConstant.InvalidConfigMaxCategoryCanAssign });
                }

                //check terminal Id is empty or doesn't exist
                if (string.IsNullOrEmpty(termialId.Trim()) || !categoryDA.IsExistTerminal(termialId))
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.InvalidTerminalInvidiualAssign);
                }

                //check category Id is empty or lager max category number
                if (categoryList.Count < 1 || categoryList.Count > maxCategory)
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategoryList);
                }

                //check order number is invalid from client
                var hasInvalidOrder = categoryList.Exists(x => x.Order <= 0 || x.Order > maxCategory);
                if (hasInvalidOrder)
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategoryOrder);
                }

                //check order number is duplicate from client
                var hasDuplicateOrder = categoryList.GroupBy(x => new { x.Order }).Where(x => x.Skip(1).Any()).Any();
                if (hasDuplicateOrder)
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.DuplicateCategoryOrder);
                }

                //check category id is duplicate from client
                var hasDuplicateCategoryId = categoryList.GroupBy(x => new { x.CategoryID }).Where(x => x.Skip(1).Any()).Any();
                if (hasDuplicateCategoryId)
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.DuplicateCategoryId);
                }

                //check category id from client is match in DB
                var categorySys =
                    categoryDA.GetCategoryByCategoryList(categoryList);

                if (categorySys.Count != categoryList.Count)
                {
                    errors.AppendLine(GlideBoxCategoryMessageConstant.InvalidCategory);
                }

                if (errors.Length > 0)
                {
                    return Json(new { IsError = true, Message = errors.ToString() });
                }

                categoryDA.UpdateCategoryInterminal(termialId, categoryList, User.Identity.Name, SourceContants.IndividualAssignCategory);

                return Json(new { IsError = false, Message = "" });
            }
            catch (Exception e)
            {
                return Json(new { IsError = true, Message = ErrorMessageConstant.SystemError, InnerMessage = e.Message });
            }
        }

        [HttpGet]
        public JsonResult GetCategoryDetailInTerminal(string terminalId)
        {
            try
            {
                var categoryDA = new CategoriesDA();
                var result = categoryDA.GetCategoryDetailInTerminal(terminalId);

                return Json(new { IsError = false, Message = string.Empty, data = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { IsError = true, Message = ErrorMessageConstant.SystemError, InnerMessage = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ResetCategoryInTerminal(string terminalId)
        {
            try
            {
                var categoryDA = new CategoriesDA();
                categoryDA.DeleteCategoryInTerminal(terminalId, User.Identity.Name, SourceContants.IndividualAssignCategory);

                return Json(new { IsError = false, Message = string.Empty });
            }
            catch (Exception e)
            {
                return Json(new { IsError = true, Message = ErrorMessageConstant.SystemError, InnerMessage = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SaveCategory(CategoryModel model)
        {
            var errorMessage = string.Empty;
            var modelErrors = new List<string>();
            var categoriesDA = new CategoriesDA();

            try
            {
                //check valid the config "max category users can create"
                var isValidMaxCategoryCanCreateConfig = int.TryParse(
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MaxTotalGlideboxCategoryCanCreate.ToString()), out int maxCategory);
                if (!isValidMaxCategoryCanCreateConfig)
                {
                    return Json( new
                    {
                        IsError = true, ErrorMessage = GlideBoxCategoryMessageConstant.InvalidConfigMaxCategoryCanCreate
                    }, JsonRequestBehavior.AllowGet);
                }

                //check number of category in system smaller than the config
                if (model.IsCreateCategoryAction && maxCategory > 0)
                {
                    var totalCategory = categoriesDA.GetTotalCategory();
                    if (totalCategory >= maxCategory)
                    {
                        return Json( new
                            {
                                IsError = true, ErrorMessage = $"{GlideBoxCategoryMessageConstant.CreateCategoryLargerConfig}"
                            }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (!ModelState.IsValid)
                {
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (var modelError in modelState.Errors)
                        {
                            modelErrors.Add(modelError.ErrorMessage);
                        }
                    }
                    errorMessage = string.Join("<br>", modelErrors);  
                }
                else
                {
                    //check category name is exist in system
                    if (categoriesDA.IsExistCategoryName(model))
                    {
                        modelErrors.Add(GlideBoxCategoryMessageConstant.CategoryNameExists);
                    }

                    //check category code is exist in system
                    if (categoriesDA.IsExistCategoryCode(model))
                    {
                        modelErrors.Add(GlideBoxCategoryMessageConstant.CategoryCodeExists);
                    }

                    //check the price smaller than commission amount
                    if (model.PriceCategory <= model.CommissionAmount)
                    {
                        modelErrors.Add(GlideBoxCategoryMessageConstant.PriceLargerCommissionAmount);
                    }

                    errorMessage = string.Join("<br>", modelErrors);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        if (model.IsCreateCategoryAction)
                        {
                            categoriesDA.CreateCategory(model, User.Identity.Name);
                        }
                        else
                        {
                            if (!categoriesDA.EditCategory(model, User.Identity.Name))
                            {
                                errorMessage = ErrorMessageConstant.SystemError;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                errorMessage = ErrorMessageConstant.SystemError;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCategoryDetail(int categoryID)
        {
            var categoriesDA = new CategoriesDA();
            var jsonData = categoriesDA.GetCategoryByID(categoryID);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsCurrentCategoryAssignedToTerminals(int categoryID)
        {
            var warningMessage = string.Empty;
            var categoriesDA = new CategoriesDA();
            var isAssigned = categoriesDA.IsCurrentCategoryAssignedToTerminals(categoryID);

            if (isAssigned)
            {
                warningMessage = GlideBoxCategoryMessageConstant.CategoryAssignedToTerminals;
            }

            var jsonData = new
            {
                IsAssigned = !string.IsNullOrEmpty(warningMessage),
                WarningMessage = warningMessage,
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveChanges(ActivateDeactivateCategoryModel model)
        {
            var errorMessage = string.Empty;
            var modelErrors = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (var modelError in modelState.Errors)
                        {
                            modelErrors.Add(modelError.ErrorMessage);
                        }
                    }
                    errorMessage = string.Join("<br>", modelErrors);
                }
                else
                {
                    var categoriesDA = new CategoriesDA();

                    if (!model.IsActivateAction)
                    {
                        var isAssigned = categoriesDA.IsCurrentCategoryAssignedToTerminals(model.ID);

                        if (isAssigned)
                        {
                            errorMessage = GlideBoxCategoryMessageConstant.CategoryAssignedToTerminals;
                        }
                    }

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        var isSuccess = categoriesDA.ActivateDeactivateCategory(model, User.Identity.Name);

                        if (!isSuccess)
                        {
                            errorMessage = ErrorMessageConstant.SystemError;
                        }
                    }
                }
            }
            catch (Exception)
            {
                errorMessage = ErrorMessageConstant.SystemError;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchCategoryHistory(GlideboxHistoryVM model)
        {
            var categoriesDA = new CategoriesDA();
            var jsonData = categoriesDA.SearchCategoryHistory(model.ID, model.PageSize, model.CurrentPage, model.SearchText, model.SortOrder, model.SortName);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileResult DownloadTerminalList()
        {
            try
            {
                var fileContent = new StringBuilder();
                var terminalErrorList = (Dictionary<string, string>)Session["TerminalIdError"];

                fileContent.AppendLine("TerminalID,Status,Reason");
                foreach (var row in terminalErrorList)
                {
                    fileContent.AppendLine(row.Key + "," + "Error" + "," + row.Value);
                }

                var terminalAddedList = (List<string>)Session["TerminalIdAdded"];
                foreach (var row in terminalAddedList)
                {
                    fileContent.AppendLine(row + "," + "Success" + "," + "");
                }

                return File(new UTF8Encoding().GetBytes(fileContent.ToString()), "text/csv", "AssignCategoryResult.csv");
            }
            catch (Exception e)
            {
                return File(new UTF8Encoding().GetBytes(e.Message), "text/csv", "export.csv");
            }
        }

    }
}
