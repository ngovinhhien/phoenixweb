﻿using System.Collections.Specialized;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace PhoenixWeb.Controllers
{
    public abstract class PhoenixController : Controller
    {
        protected MembershipProvider membershipProvider => GetMembershipProviderByName("AspNetSqlMembershipProvider");

        protected MembershipProvider GetMembershipProviderByName(string name)
        {
            var provider = new SqlMembershipProvider();
            string configPath = "~/web.config";
            Configuration config = WebConfigurationManager.OpenWebConfiguration(configPath);
            MembershipSection section = (MembershipSection)config.GetSection("system.web/membership");
            ProviderSettingsCollection settings = section.Providers;
            NameValueCollection membershipParams = settings[name].Parameters;
            provider.Initialize(name, membershipParams);

            return provider;
        }

    }
}
