﻿using Phoenix_BusLog.Data;
using PhoenixObjects.Financial;
using PhoenixObjects.Member;
using PhoenixWeb.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class MemberSignUpController : PhoenixWebController
    {
        //
        // GET: /MemberSignUp/

        public ActionResult Index()
        {
            return View("Menu");
        }
      
        //
        // GET: /MemberSignUp/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /MemberSignUp/Create
        public ActionResult AutoCompleteEftTerminalId(string term)
        {
            List<string> m = new List<string>();
            TerminalDA cda = new TerminalDA();
            var model = cda.GetEftTerminals(term).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create()
        {
            MemberModel model = new MemberModel();
            DrawDropDownConfig(model.CompanyKey);
            DrawDropDownConfigAllocateTerminal();
            return View(model);
        }
        private void DrawDropDownConfigAllocateTerminal()
        {
            StateDA sda = new StateDA();
            var states = sda.GetStateAustralia().ToList();
            ViewBag.StateList = states.Select(item => new SelectListItem
            {
                Text = item.StateName,
                Value = item.StateKey.ToString()
            }).ToList();

            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get().ToList();
            ViewBag.VehicleTypeList = VehicleType.Select(item => new SelectListItem
            {

                Text = item.Name,
                Value = item.VehicleType_key.ToString()
            }).ToList();
        }
        private void DrawDropDownConfig(int company_key)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();

            ViewBag.MemberRentalTemplateList = MemberRentalTemplateList.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.MemberRentalTemplateKey.ToString()
            }).ToList();

            BusinessTypeDA bda = new BusinessTypeDA();
            var BusinessTypeList = bda.Get().ToList();
            ViewBag.BusinessTypeList = BusinessTypeList.Select(item => new SelectListItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString()
            }).ToList();
        }
        //
        // POST: /MemberSignUp/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MemberSignUp/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /MemberSignUp/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MemberSignUp/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MemberSignUp/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
