﻿using PhoenixWeb.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class MyWebController : PhoenixWebController
    {
        //
        // GET: /MyWeb/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPendingDetailsRequest()
        {

            return View();
        }
        //
        // GET: /MyWeb/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }
        //
        // GET: /MyWeb/Create

        public ActionResult Create()
        {
            return View();
        }
        //
        // POST: /MyWeb/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MyWeb/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }
        //
        // POST: /MyWeb/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MyWeb/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MyWeb/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
