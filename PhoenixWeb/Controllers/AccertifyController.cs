﻿using LGHelper.PhoenixWebAPI;
using Phoenix_BusLog;
using PhoenixObjects.Enums;
using PhoenixWeb.Models.ViewModel.Accertify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class AccertifyController : Controller
    {
        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult AccertifyReview()
        {
            AccertifyTransactionFilter filterModel = new AccertifyTransactionFilter();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            string accertifyLink = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.AccertifyApp.ToString());
            filterModel.ListSubBusinessType = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.ACCERTIFY_GET_SUBBUSINESS_TYPE).Data;
            filterModel.SubBusinessType = filterModel.ListSubBusinessType.FirstOrDefault().Value;
            filterModel.ListAccertifyStatus = client.Get<List<SelectListItem>>(PhoenixWebAPIEndpoint.ACCERTIFY_GET_STATUS).Data;
            filterModel.AccertifyStatus = filterModel.ListAccertifyStatus.Where(i => i.Selected == true).FirstOrDefault().Value;

            ViewBag.AccertifyLink = accertifyLink;

            return View(filterModel);
        }

        [RBAC(Permission = "FraudMonitoringViewReport")]
        public ActionResult FilterAccertifyTransaction(AccertifyTransactionFilter model)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["searchText"] = model.SearchText;
            query["fromDate"] = model.FromDate;
            query["toDate"] = model.ToDate;
            query["subBusinessType"] = model.SubBusinessType.ToString();
            query["AccertifyStatus"] = model.AccertifyStatus;
            query["sortField"] = model.SortField;
            query["sortOrder"] = model.SortOrder;
            query["pageSize"] = model.PageSize.ToString();
            query["pageIndex"] = model.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<DataSearchResponse<AccertifyTransactionVM>>(PhoenixWebAPIEndpoint.ACCERTIFY_GET_TRANSACTION + uriBuilder.Query.ToString());

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }
    }
}