﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix_BusLog.Data;
using AutoMapper;
using PhoenixObjects.Rewards;
using PhoenixWeb.Models.ViewModel;
using ExcelDataReader;
using System.IO;
using System.Data;

namespace PhoenixWeb.Controllers
{
    public class RewardsController : Controller
    {    
        public ActionResult QBRApprove()
        {
            RewardsDA rewardsDA = new RewardsDA();
            IEnumerable<QantasExportFileModel> qantasExportFileModel = rewardsDA.GetPending();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<QantasExportFileModel, QantasExportFileVM>();
                cfg.CreateMap<QantasExportFileApproverModel, QantasExportFileApproverVM>();
                cfg.CreateMap<QantasExportFileApproverStatusModel, QantasExportFileApproverStatusVM>();
                cfg.CreateMap<QantasExportFileStatusModel, QantasExportFileStatusVM>();
            });

            IMapper mapper = config.CreateMapper();
            var source = qantasExportFileModel;
            var model = mapper.Map<IEnumerable<QantasExportFileModel>, IEnumerable<QantasExportFileVM>>(source);


            List<SelectListItem> qantasFiles = model.Select(n => new SelectListItem { Value = n.QantasExportFileKey.ToString(), Text = n.FileName }).ToList();

            var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" }; qantasFiles.Insert(0, qantasFile);

            return View(new RewardsViewModel() { FileNames = qantasFiles });
        }

        public JsonResult GetFileEntries(string sid, string sord, int page, int rows, string exportFileEntry, string sortorder, bool onlyRejected = false)
        {

            if (String.IsNullOrEmpty(exportFileEntry) || exportFileEntry == "undefined") { return Json("", JsonRequestBehavior.AllowGet); }
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                int count;
                var todoListsResults = rewardsDA.GetExportFileEntries(Convert.ToInt32(exportFileEntry), out count, pageIndex * pageSize, pageSize, onlyRejected);
                int totalRecords = count; //todoListsResults.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults = todoListsResults
                            .OrderByDescending(order => order.QantasFileEntryKey).ToList();
                    //todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults = todoListsResults
                            .OrderBy(order => order.QantasFileEntryKey).ToList();
                    //todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = todoListsResults
                };

                JsonResult json = Json(jsonData, JsonRequestBehavior.AllowGet);

                return json;
            }

        }

        public JsonResult UpdateQantasFileEntryStatus(int qbrFileEntryKey, string updateStatusWith, bool updateAll, int memberKey, int qantasExportFilekey)
        {

            using (RewardsDA rewardsDA = new RewardsDA())
            {

                if (updateAll)
                {
                    rewardsDA.UpdateExportFileEntryStatusAll(
                                    qantasExportFilekey,
                                    updateStatusWith == "approve" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);
                }
                else
                {


                    rewardsDA.UpdateExportFileEntryStatus(memberKey,
                                                        qantasExportFilekey,
                                                        updateStatusWith == "approve" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);
                }
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateQantasFileStatus(string updateStatusWith, int qantasExportFilekey)
        {

            using (RewardsDA rewardsDA = new RewardsDA())
            {
                rewardsDA.UpdateExportFileStatus(qantasExportFilekey,
                                                    updateStatusWith.ToLower() == "ready to be send to qbr" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);

            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Rejected()
        {
            RewardsDA rewardsDA = new RewardsDA();
            IEnumerable<QantasExportFileModel> qantasExportFileModel = rewardsDA.Get();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<QantasExportFileModel, QantasExportFileVM>();
                cfg.CreateMap<QantasExportFileApproverModel, QantasExportFileApproverVM>();
                cfg.CreateMap<QantasExportFileApproverStatusModel, QantasExportFileApproverStatusVM>();
                cfg.CreateMap<QantasExportFileStatusModel, QantasExportFileStatusVM>();
            });

            IMapper mapper = config.CreateMapper();
            var source = qantasExportFileModel;
            var model = mapper.Map<IEnumerable<QantasExportFileModel>, IEnumerable<QantasExportFileVM>>(source);

            List<SelectListItem> qantasFiles = model.Select(n => new SelectListItem { Value = n.QantasExportFileKey.ToString(), Text = n.FileName + " | " + n.FileDate.Value.ToString("dd-MMM-yyyy") }).ToList();

            var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" }; qantasFiles.Insert(0, qantasFile);

            return View(new RewardsViewModel() { FileNames = qantasFiles });
        }

        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult Upload()
        {
            using (RewardsDA rewardsData = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                return View(new LoyaltyUploadModel()
                {
                    OrganisationforLoyaltyModel = organisationforLoyaltyModel,
                });
            }

        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        public ActionResult Import(FormCollection c, LoyaltyUploadModel model)
        {
            List<LoyaltyUploadFileLineItemModel> loyaltyUploadFileLineItemModel = new List<LoyaltyUploadFileLineItemModel>();
            model.FileRecords = (List<LoyaltyUploadFileModel>)Session["loyaltyUploadFileModel"];
            if (model.AllowImport)
            {
                foreach (LoyaltyUploadFileModel m in model.FileRecords)
                {
                    loyaltyUploadFileLineItemModel.Add(new LoyaltyUploadFileLineItemModel()
                    {
                        LoyaltyNumber = m.LoyaltyNumber,
                        MemberId = m.MemberId,
                        Name = m.Name,
                        Points = int.TryParse(m.Points, out var points) ? points : 0,
                    });
                }

                using (RewardsDA rewardsData = new RewardsDA())
                {
                    try
                    {
                        rewardsData.Save(loyaltyUploadFileLineItemModel, "PhoeixWeb", "Test", int.TryParse(model.SelectionOrganisationId, out var points) ? points : 0);


                        foreach (LoyaltyUploadFileModel m in model.FileRecords)
                        {
                            m.Status = "Imported In Phoenix";
                        }
                        model.AllowImport = false;
                    }
                    catch (Exception ex)
                    {

                    }
                    finally { }
                }


            }


            using (RewardsDA rewardsData = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
            }
            return View("Upload", model);
        }

        [HttpPost]
        [RBAC(Permission = "Transaction_TransactionSearch_SearchDocket")]
        [ValidateAntiForgeryToken]
        public ActionResult LoyaltyRewardsFileUpload(HttpPostedFileBase file, LoyaltyUploadModel model)
        {
            bool allGood = false;
            List<LoyaltyUploadFileModel> loyaltyUploadFileModels = new List<LoyaltyUploadFileModel>();
            if (file != null && file.ContentLength > 0 && file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {

                if (file.ContentLength > 0)
                {
                    //create an XML object and load it in                  
                    Stream fileStream = file.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;
                    if (file.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                    }
                    else if (file.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        using (RewardsDA rewardsData = new RewardsDA())
                        {
                            IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                            model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                            model.AllowImport = allGood;
                            return View("Upload", model);
                        }
                    }

                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    reader.Close();

                    DataTable dataTable = result.Tables["Loyalty Data"];
                    string[] columns = { "Member Id", "Points", "Business Name", "Loyalty Number", "Load", "Record Date" };
                    string[] columnNames = dataTable.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    var unmatched = columns.Except(columnNames).Concat(columnNames.Except(columns)).Skip(1).ToArray();

                    if (unmatched.Count() > 0)
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        using (RewardsDA rewardsData = new RewardsDA())
                        {
                            IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                            model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                            model.AllowImport = allGood;
                            return View("Upload", model);
                        }
                    }

                    foreach (DataRow row in dataTable.Select("[Load] = 'Y'"))
                    {
                        LoyaltyUploadFileModel loyaltyUploadFileModel = new LoyaltyUploadFileModel()
                        {
                            MemberId = row["Member Id"].ToString(),
                            Points = row["Points"].ToString(),
                            Name = row["Business Name"].ToString(),
                            LoyaltyNumber = row["Loyalty Number"].ToString(),
                            Load = row["Load"].ToString(),
                            RecordDate = row["Record Date"].ToString(),
                            Status = "Not Imported In Phoenix",
                        };
                        loyaltyUploadFileModel.Verify();
                        loyaltyUploadFileModels.Add(loyaltyUploadFileModel);
                    };

                    model.FileRecords = loyaltyUploadFileModels;
                    if (file == null || file.FileName == null)
                    {
                        ViewBag.Message = "Please upload file.";
                    }
                    else
                    {
                        model.FileName = file.FileName;
                    }
                    model.SelectionOrganisationId = model.SelectionOrganisationId;
                    ViewBag.UploadError = loyaltyUploadFileModels.IsValid().Split('|');

                    Session["loyaltyUploadFileModel"] = loyaltyUploadFileModels;

                    allGood = true;
                }
                else
                {
                    ModelState.AddModelError("File", "This file format is not supported");
                    using (RewardsDA rewardsData = new RewardsDA())
                    {
                        IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                        model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                        model.AllowImport = allGood;
                        return View("Upload", model);
                    }
                }

            }
            else
            {
                ModelState.AddModelError("File", "This file format is not supported");
                using (RewardsDA rewardsData = new RewardsDA())
                {
                    IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                    model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                    model.AllowImport = allGood;
                    return View("Upload", model);
                }
            }


            using (RewardsDA rewardsData = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsData.GetOrganisationforLoyalties();
                model.OrganisationforLoyaltyModel = organisationforLoyaltyModel;
                model.AllowImport = allGood;
                return View("Upload", model);
            }
        }

        public ActionResult LoyaltyApproval()
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsDA.GetOrganisationforLoyalties();
                List<SelectListItem> qantasFiles = organisationforLoyaltyModel.Select(n => new SelectListItem { Value = n.OrganisationforLoyaltyId.ToString(), Text = n.Name }).ToList();
                var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" };
                qantasFiles.Insert(0, qantasFile);
                return View(new RewardsViewModel() { FileNames = new List<SelectListItem>() { new SelectListItem() { Value = null, Text = "--- Select ---" } }, Organisations = qantasFiles });
            }
        }


        public JsonResult GetPendingForLoyalties(int id)
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                IEnumerable<LoyaltyExportFileModel> qantasExportFileModel = rewardsDA.GetPendingforLoyalty(id);
                List<SelectListItem> qantasFiles = qantasExportFileModel.Select(n => new SelectListItem { Value = n.LoyaltyExportFileId.ToString(), Text = n.FileName + " | " + n.FileDate.Value.ToString("dd-MMM-yyyy") }).ToList();
                var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" }; qantasFiles.Insert(0, qantasFile);
                return Json(qantasFiles, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLoyaltyFileEntries(string sid, string sord, int page, int rows, string exportFileEntry, string sortorder)
        {

            if (String.IsNullOrEmpty(exportFileEntry) || exportFileEntry == "undefined") { return Json("", JsonRequestBehavior.AllowGet); }
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                var todoListsResults = rewardsDA.GetLoyaltyExportFileEntries(Convert.ToInt32(exportFileEntry));
                int totalRecords = todoListsResults.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults = todoListsResults
                            .OrderByDescending(order => order.LoyaltyExportFileLineItemId).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults = todoListsResults
                            .OrderBy(order => order.LoyaltyExportFileLineItemId).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = todoListsResults
                };

                JsonResult json = Json(jsonData, JsonRequestBehavior.AllowGet);

                return json;
            }

        }

        public JsonResult UpdateLoyaltyFileEntryStatus(int qbrFileEntryKey, string updateStatusWith, bool updateAll, string memberKey, int loyaltyExportFileId)
        {

            using (RewardsDA rewardsDA = new RewardsDA())
            {

                if (updateAll)
                {
                    rewardsDA.UpdateLoyaltyExportFileEntryStatusAll(
                                    loyaltyExportFileId,
                                    updateStatusWith == "approve" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);
                }
                else
                {


                    rewardsDA.UpdateLoyaltyExportFileEntryStatus(memberKey,
                                                        loyaltyExportFileId,
                                                        updateStatusWith == "approve" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);
                }
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateLoyaltyFileStatus(string updateStatusWith, int loyaltyExportFileId)
        {

            if (String.IsNullOrEmpty( updateStatusWith)) { return Json("Success", JsonRequestBehavior.AllowGet); }

            using (RewardsDA rewardsDA = new RewardsDA())
            {
                rewardsDA.UpdateLoyaltyExportFileStatus(loyaltyExportFileId,
                                                    updateStatusWith.ToLower() == "ready to be send to qbr" ? RewardsDA.QantasExportFileEntryStatus.Approve : RewardsDA.QantasExportFileEntryStatus.Rejected);

            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }


        public ActionResult UploadedFiles()
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                IEnumerable<OrganisationforLoyaltyModel> organisationforLoyaltyModel = rewardsDA.GetOrganisationforLoyalties();
                List<SelectListItem> qantasFiles = organisationforLoyaltyModel.Select(n => new SelectListItem { Value = n.OrganisationforLoyaltyId.ToString(), Text = n.Name }).ToList();
                var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" };
                qantasFiles.Insert(0, qantasFile);
                return View(new RewardsViewModel() { FileNames = new List<SelectListItem>() { new SelectListItem() { Value = null, Text = "--- Select ---" } }, Organisations = qantasFiles });
            }
        }

        [HttpPost]
        public JsonResult GetUploadedFiles(int id)
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                IEnumerable<LoyaltyExportFileModel> qantasExportFileModel = rewardsDA.GetUploadedFiles(id);
                List<SelectListItem> qantasFiles = qantasExportFileModel                      
                        .Select(n => new SelectListItem { Value = n.LoyaltyExportFileId.ToString(), Text = n.FileName + " | " + n.FileDate.Value.ToString("dd-MMM-yyyy") }).ToList();
                var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" };
                                qantasFiles.Insert(0, qantasFile);
                return Json(qantasFiles, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetPendingUploadedFiles(int id)
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                IEnumerable<LoyaltyExportFileModel> qantasExportFileModel = rewardsDA.GetUploadedFiles(id);
                List<SelectListItem> qantasFiles = qantasExportFileModel
                        .Where(where => where.QantasExportFileStatusId == 2)
                        .Select(n => new SelectListItem { Value = n.LoyaltyExportFileId.ToString(), Text = n.FileName + " | " + n.FileDate.Value.ToString("dd-MMM-yyyy") }).ToList();
                var qantasFile = new SelectListItem() { Value = null, Text = "--- Select ---" };
                qantasFiles.Insert(0, qantasFile);
                return Json(qantasFiles, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetUploadFileDetails(int id)
        {
            using (RewardsDA rewardsDA = new RewardsDA())
            {
                var model = rewardsDA.GetUploadedFile(id);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
