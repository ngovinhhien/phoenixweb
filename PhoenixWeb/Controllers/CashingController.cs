﻿using log4net;
using Phoenix_BusLog.Data;
using Phoenix_BusLog.TransactionHost.BusLogs;
using Phoenix_Service;
using PhoenixObjects.Cashing;
using PhoenixObjects.Common;
using PhoenixObjects.Financial;
using PhoenixObjects.GlideBoxCategory;
using PhoenixObjects.Member;
using PhoenixObjects.Transaction;
using PhoenixWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class CashingController : PhoenixWebController
    {
        private ILog logger = LogManager.GetLogger("CashingController");
        public CashingController() : base()
        { }

        Dictionary<string, string> ScannedDockets;
        //
        // GET: /Cashing/
        public List<string> ScannedDocketList = new List<string>();

        public ActionResult Index()
        {
            Session.Clear();
            Session.RemoveAll();
            Session["DocketCheckSummary"] = 0;
            Session["ModelList"] = null;
            Session["MemberRateModel"] = null;
            Session["MemberKey"] = 0;
            Session["LodgmentNo"] = 0;
            Session["Paid"] = "false";
            Session["Print"] = "false";
            Session["ScannedDocketList"] = null;
            Session["officekey"] = null;
            Session["commissionRate"] = null;
            Session["MemberRateModel"] = null;
            Session["TopBC"] = null;
            Session["BottomBC"] = null;
            Session["ValidScanCount"] = null;
            Session["HasChargeBack"] = "false";
            Session["HasChargeBackInsurance"] = "false";
            Session["HasAuthorize"] = "false";
            Session["MemberNotes"] = "false";
            Session["Notes"] = null;
            Session["RewardPoints"] = null;
            Session["TerminalAllocationLst"] = null;
            Session["AuthorisedMemberKey"] = 0;
            ScannedDockets = new Dictionary<string, string>();
            //return RedirectToAction("IndexScan");
            WebCashingResult model = new WebCashingResult();
            int officekey = 0;
            double commissionRate = 0.0F;
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;

            UserDA uda = new UserDA();
            officekey = uda.GetUserOfficeKey(uId);
            OfficeDA oda = new OfficeDA();
            commissionRate = oda.GetOfficeCommission(officekey);
            Session["officekey"] = officekey;
            Session["commissionRate"] = commissionRate;
            Session["UserId"] = uId;
            return View(model);

        }

        public ActionResult IndexScan()
        {
            Session["LodgmentNo"] = 0;
            Session["Paid"] = "false";
            Session["Print"] = "false";
            Session["ScannedDocketList"] = null;
            Session["officekey"] = null;
            Session["commissionRate"] = null;
            Session["MemberRateModel"] = null;
            WebCashingResult model = new WebCashingResult();
            try
            {
                int officekey = 0;
                double commissionRate = 0.0F;
                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;

                UserDA uda = new UserDA();
                officekey = uda.GetUserOfficeKey(uId);
                OfficeDA oda = new OfficeDA();
                commissionRate = oda.GetOfficeCommission(officekey);
                Session["officekey"] = officekey;
                Session["commissionRate"] = commissionRate;
                Session["UserId"] = uId;

                if (Session["ModelList"] != null)
                {
                    model = (WebCashingResult)Session["ModelList"];
                }
                else
                {
                    model = null;
                }

                if (model != null && model.BatchStatus != "Cash Customer")
                {
                    ModelState.AddModelError("Error", "Invalid payment type. Cannot continue with cashing.");
                    return View(model);
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        [RBAC(Permission = "Cashing/IndexMassScan")]
        public ActionResult IndexMassScan()
        {
            Session.Clear();
            Session.RemoveAll();
            Session["MemberKey"] = 0;
            Session["DocketCheckSummary"] = 0;
            Session["ModelList"] = null;
            Session["LodgmentNo"] = 0;
            Session["Paid"] = "false";
            Session["Print"] = "false";
            Session["ScannedDocketList"] = null;
            Session["officekey"] = null;
            Session["commissionRate"] = null;
            Session["UserId"] = null;
            try
            {
                int officekey = 0;
                double commissionRate = 0.0F;
                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                UserDA uda = new UserDA();
                officekey = uda.GetUserOfficeKey(uId);
                OfficeDA oda = new OfficeDA();
                commissionRate = oda.GetOfficeCommission(officekey);
                Session["officekey"] = officekey;
                Session["commissionRate"] = commissionRate;
                Session["UserId"] = uId;
                return View();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View();
            }
        }
        [RBAC(Permission = "CashingGenerateReport")]
        public ActionResult GenerateReport(FormCollection c)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                PhoenixWeb.ReportingWebService.Warning[] warnings;
                string[] streamIds;

                DateTime sDate = Convert.ToDateTime(c["StartDate"]);
                DateTime eDate = Convert.ToDateTime(c["EndDate"]);
                UserDA uda = new UserDA();
                string ReportName = "/Cashing Reports/Daily Cashing Summary";

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                int officekey = uda.GetUserOfficeKey(uId);
                string state = uda.GetUserOfficeKeyState(officekey);
                string user = User.Identity.Name;
                IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "StartDate", Value = sDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "EndDate", Value = eDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "Office", Value = officekey.ToString() });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "State", Value = state });
                //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
                string format = "PDF";
                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                //return new ReportsResult(output, mimeType, format); this downloads report
                return new FileContentResult(output, mimeType);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }
        [RBAC(Permission = "CashingGenerateReport")]
        public ActionResult Reports()
        {
            try
            {
                CashingReportVM model = new CashingReportVM();
                string dobDate = DateTime.Now.ToShortDateString();
                string time = DateTime.Now.ToShortTimeString();
                string[] dobDateArry = dobDate.Split('/');

                model.StartDate = DateTime.Now.Date.ToString("s");
                model.EndDate = DateTime.Now.ToString("s");
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }
        [RBAC(Permission = "CashingGenerateReport")]
        public ActionResult GenerateManualVoucherReports(FormCollection c)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                PhoenixWeb.ReportingWebService.Warning[] warnings;
                string[] streamIds;

                DateTime sDate = Convert.ToDateTime(c["StartDate"]);
                DateTime eDate = Convert.ToDateTime(c["EndDate"]);
                UserDA uda = new UserDA();
                string ReportName = "/Cashing Reports/Manual Vouchers By Office";

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                int officekey = uda.GetUserOfficeKey(uId);
                string state = uda.GetUserOfficeKeyState(officekey);
                string user = User.Identity.Name;
                IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "startDate", Value = sDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "endDate", Value = eDate.ToString("yyyy-MM-dd hh:mm:ss tt") });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "Office", Value = officekey.ToString() });
                //parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "State", Value = state });
                //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
                string format = "EXCEL";
                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                //return new ReportsResult(output, mimeType, format); this downloads report
                return new FileContentResult(output, mimeType);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }
        [RBAC(Permission = "CashingGenerateReport")]
        public ActionResult ManualVoucherReports()
        {
            try
            {
                ManualVoucherReportVM model = new ManualVoucherReportVM();
                string dobDate = DateTime.Now.ToShortDateString();
                string time = DateTime.Now.ToShortTimeString();
                string[] dobDateArry = dobDate.Split('/');

                model.StartDate = DateTime.Now.Date.ToString("s");
                model.EndDate = DateTime.Now.ToString("s");
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }
        [RBAC(Permission = "CashingSearchMember")]
        public ActionResult SearchMember()
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchMember(FormCollection c)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            try
            {
                MemberModel modl = new MemberModel();
                MemberDA mda = new MemberDA();

                if (c["DriverSearchField"] != "")
                {
                    string term = c["DriverSearchField"].ToString();
                    model.MemberList = mda.GetMemberDriverAuthSearch(term);
                }
                else if (c["MemberSearchField"] != "")
                {
                    string term = c["MemberSearchField"].ToString();

                    if (term.IndexOf(",") > 0)
                    {
                        string memberid = string.Empty;
                        string[] memberlist = term.Split(',');
                        memberid = memberlist[0];
                        if (memberid.Length > 1)
                        {
                            var mkey = mda.GetMemberKey(memberid);
                            model.MemberList.Add(mda.GetMemeber(mkey));
                        }
                    }
                    else if (term.Length > 6 && term.Length < 9 && term.Substring(0, 1) == "7")
                    {
                        var mkey = mda.GetMemberKeyByTerminalId(term);
                        model.MemberList.Add(mda.GetMemeber(mkey));
                    }
                    else
                    {
                        //List<MemberModel> memlist = new List<MemberModel>();
                        //memlist = mda.GetMemberGeneralSearch(term);
                        //model.MemberList = memlist;

                        List<MemberModelSimple> memlist = new List<MemberModelSimple>();
                        memlist = mda.GetMemberGeneralSearch(term);
                        model.MemberList = memlist;
                    }
                }
                else
                {
                    model = null;
                }

                Session["searchList"] = null;
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }

        }

        [RBAC(Permission = "CashingSearchMember")]
        public ActionResult SelectMemberAccount(string Id, string IsAuthorised = "false")
        {
            try
            {
                Session.Clear();
                Session.RemoveAll();
                Session["DocketCheckSummary"] = 0;
                Session["ModelList"] = null;
                Session["MemberRateModel"] = null;
                Session["MemberKey"] = 0;
                Session["LodgmentNo"] = 0;
                Session["Paid"] = "false";
                Session["Print"] = "false";
                Session["ScannedDocketList"] = null;
                Session["officekey"] = null;
                Session["commissionRate"] = null;
                Session["MemberRateModel"] = null;
                Session["TopBC"] = null;
                Session["BottomBC"] = null;
                Session["ValidScanCount"] = null;
                Session["HasChargeBack"] = "false";
                Session["HasChargeBackInsurance"] = "false";
                if (IsAuthorised == "true")
                {
                    Session["HasAuthorize"] = "true";
                }
                else
                {
                    Session["HasAuthorize"] = "false";
                }

                Session["MemberNotes"] = "false";
                Session["Notes"] = null;
                Session["RewardPoints"] = null;
                Session["TerminalAllocationLst"] = null;
                Session["AuthorisedMemberKey"] = 0;
                MemberDA mda = new MemberDA();
                MemberModel membermodel = new MemberModel();
                string memberid = Id;
                int mkey = mda.GetMemberKey(memberid);
                membermodel = mda.GetMemeber(mkey);

                WebCashingResult model = new WebCashingResult();
                model.ABN = membermodel.ABN;
                model.BatchNumber = "";
                model.BatchStatus = "";
                model.BatchTotal = 0;
                model.CommissionTotal = 0;
                model.Count = 0;
                model.DocketCheckSummaryKey = 0;
                model.DocketTotal = 0;
                model.ExtraTotal = 0;
                model.FaceValueTotal = 0;
                model.GrandTotal = 0;
                model.LodgementReferenceNo = "";
                model.Member_key = membermodel.Member_key;
                model.memberid = membermodel.MemberId;
                model.Mobile = membermodel.Mobile;
                model.Name = membermodel.FirstName + " " + membermodel.LastName;
                model.Phone = membermodel.Telephone;
                model.TradingName = membermodel.TradingName;
                model.PhotoID = membermodel.PhotoID;
                model.WebCashing_MassScanModelList = null;
                model.ResultList = null;
                model.LastCashedAmt = membermodel.LastCashedAmt;
                model.LastCashedDateTime = membermodel.LastCashedDateTime;
                model.DriverCertificateId = membermodel.DriverCertificateId;
                if (membermodel.CRDebitRate != null)
                    model.Debit = membermodel.CRDebitRate.GetValueOrDefault();
                model.Visa = membermodel.CRVisaRate.GetValueOrDefault();
                model.Master = membermodel.CRMasterCardRate.GetValueOrDefault();
                model.Diners = membermodel.CRDinersRate.GetValueOrDefault();
                model.Amex = membermodel.CRAmexRate.GetValueOrDefault();
                model.DebitPercentage = membermodel.CRDebitRatePercentage.GetValueOrDefault();

                if (membermodel.CRQantasRate != null)
                    model.Qantas = membermodel.CRQantasRate.GetValueOrDefault();
                model.UnionPay = membermodel.CRUnionPayRate.GetValueOrDefault();
                model.ZipPay = membermodel.CRZipPayRate.GetValueOrDefault();
                model.QantasPercentage = membermodel.CRQantasRatePercentage.GetValueOrDefault();
                model.Alipay = membermodel.CRAlipayRate.GetValueOrDefault();



                model.Companykey = membermodel.CompanyKey;
                OperatorDA oda1 = new OperatorDA();
                OperatorModel opt = new OperatorModel();
                opt = oda1.Get(membermodel.Member_key);
                if (opt.AllowCash == false)
                {
                    model.BatchStatus = "Bank Customer";
                }
                else
                {
                    model.BatchStatus = "Cash Customer";
                }

                if (opt.PaymentType.GetValueOrDefault() == 3)
                {
                    model.BatchStatus = "Card Customer";
                }


                Session["ModelList"] = model;
                Session["MemberKey"] = model.Member_key;
                Session["ScanType"] = "Scan";
                return RedirectToAction("IndexScan");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }

        [RBAC(Permission = "CashingMemberDetails")]
        public ActionResult MemberDetails(string memberId)
        {
            MemberDA mda = new MemberDA();
            int memberkey = mda.GetMemberKey(memberId);
            //return RedirectToAction("Edit", "Member", new { member_key = memberkey });
            return RedirectToAction("Details", "Member", new { member_key = memberkey });
        }


        [RBAC(Permission = "Cashing/NoReceipt")]
        public ActionResult NoReceipt(string Id)
        {
            List<WebCashing_GetPendingBatchesHeaderByMemberKey> model = new List<WebCashing_GetPendingBatchesHeaderByMemberKey>();
            try
            {
                MemberDA mda = new MemberDA();
                int memberkey = mda.GetMemberKey(Id);
                Session["MemberKey"] = memberkey;
                int dockey = 0;

                PendingTransactionDetailDA ptda = new PendingTransactionDetailDA();

                var Val1 = Session["DocketCheckSummary"];
                if (Val1 != null)
                    dockey = Convert.ToInt32(Val1);
                if (this.HasPermission("CashingShowChargeBack"))
                {
                    // model = ptda.GetPendingTransaction(memberkey, dockey);
                    model = ptda.GetPendingBatchesDetailByMemberKey(memberkey, dockey);
                }
                else if (this.HasPermission("CashingNoReceiptSixMonths"))
                {
                    model = ptda.GetPendingBatchesDetailByMemberKey(memberkey, dockey, 6);
                }
                Session["NoRecieptModel"] = model;

                //foreach (var m in model)
                //{
                //    m.MemberId = Id;
                //}
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        [RBAC(Permission = "Cashing/NoReceipt")]
        public ActionResult AddNoReceipt(FormCollection c)
        {
            try
            {
                MemberDA mda = new MemberDA();
                int memberkey = Convert.ToInt32(Session["MemberKey"].ToString());
                int GCount = 0;
                int Countt = 0;
                PendingTransactionDetailDA ptda = new PendingTransactionDetailDA();
                WebCashingResult model = new WebCashingResult();
                List<WebCashing_GetPendingBatchesHeaderByMemberKey> modelP = new List<WebCashing_GetPendingBatchesHeaderByMemberKey>();
                // modelP = ptda.GetPendingTransaction(memberkey, dockey);

                modelP = (List<WebCashing_GetPendingBatchesHeaderByMemberKey>)Session["NoRecieptModel"];// ptda.GetPendingBatchesDetailByMemberKey(memberkey, dockey);


                List<string> TODOList = new List<string>();
                foreach (var m1 in modelP)
                {

                    var isther = c[m1.count];

                    if (isther != null)
                    {
                        var dockeyChecked = (c[m1.count]).ToString();

                        if (dockeyChecked == "on")
                        {

                            TODOList.Add(m1.DocketBatchId);
                        }
                    }
                }
                foreach (var m1 in TODOList)
                {

                    string paid = Session["Paid"].ToString();

                    if (paid == "false")
                    {
                        var Val1 = Session["DocketCheckSummary"];
                        int DocketCheckSummary = 0;
                        if (Val1 != null)
                            DocketCheckSummary = (int)Session["DocketCheckSummary"];

                        Guid uId = (Guid)Session["UserId"];

                        int officekey = (int)Session["officekey"];
                        double commissionRate = (double)Session["commissionRate"];

                        WebCashingResult model1 = new WebCashingResult();
                        model1 = (WebCashingResult)Session["ModelList"];

                        DocketCheckDA dcd = new DocketCheckDA(logger);

                        var addthis = modelP.Where(m => m.DocketBatchId == m1).FirstOrDefault();
                        GCount = addthis.WebCashing_GetPendingBatchesDetailsByMemberKey.Count();

                        foreach (var a in addthis.WebCashing_GetPendingBatchesDetailsByMemberKey)
                        {

                            DocketCheckSummary = (int)Session["DocketCheckSummary"];


                            WebCashingResult mm = null;
                            int BatchYear = Convert.ToInt32(addthis.BatchYear);

                            Session["ScanType"] = "";
                            string ScanType = "d";
                            if (Countt < GCount - 1)
                            {
                                model = dcd.DocketCheck(addthis.TerminalID, addthis.BatchNumber, User.Identity.Name, DocketCheckSummary, uId, ScanType, "False", string.Empty, commissionRate, officekey, false, BatchYear, memberkey, null, true, a.Docket_key);
                                Countt = Countt + 1;
                            }
                            else
                            {
                                model = dcd.DocketCheck(addthis.TerminalID, addthis.BatchNumber, User.Identity.Name, DocketCheckSummary, uId, ScanType, "False", string.Empty, commissionRate, officekey, true, BatchYear, memberkey, null, true, a.Docket_key);
                                Countt = Countt + 1;
                                //return RedirectToAction("Search", new { TopBC = addthis.TerminalID, BottomBC = addthis.BatchNumber, ScanType = "", mm, Year = addthis.BatchYear });
                            }
                            if (model.Member_key != null)
                            {
                                Session["DocketCheckSummary"] = model.DocketCheckSummaryKey;
                                Session["LodgmentNo"] = model.LodgementReferenceNo;
                                Session["MemberKey"] = model.Member_key;
                                Session["TopBC"] = addthis.TerminalID;
                                Session["BottomBC"] = addthis.BatchNumber;
                                Session["ValidScanCount"] = model.Count;
                                Session["ModelList"] = model;
                            }
                        }
                    }
                }
                return RedirectToAction("IndexScan");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }

        [RBAC(Permission = "CashingRemoveManualVoucher")]
        public ActionResult RemoveManualVoucher(int Id)
        {
            CommissionRate_MemberRateModelList model = new CommissionRate_MemberRateModelList();
            model = (CommissionRate_MemberRateModelList)Session["MemberRateModel"];
            try
            {

                List<CommissionRate_AddedMemberRateModel> AddedRateList = new List<CommissionRate_AddedMemberRateModel>();
                AddedRateList = model.AddedMemberRateModelList;
                var model1 = (from n in AddedRateList
                              where n.Id == Id
                              select n).FirstOrDefault();
                model.AddedMemberRateModelList.Remove(model1);

                decimal RemoveCardTotal = Convert.ToDecimal(model1.Total);
                int RemoveCardQty = 1;

                List<CommissionRate_AddedMemberRateModelTotals> AddedtotalsList = new List<CommissionRate_AddedMemberRateModelTotals>();
                AddedtotalsList = model.AddedMemberRateTotalModelList;
                var RemovedCard = (from n in AddedtotalsList
                                   where n.CardName == model1.Name
                                   select n).FirstOrDefault();

                decimal RemoveCardTotalFaceValue = Convert.ToDecimal(RemovedCard.Total) - Convert.ToDecimal(RemoveCardTotal);
                int RemoveCardTotalQty = RemovedCard.Qty - RemoveCardQty;


                decimal Grandtotal = 0.0M;
                decimal TotalFaceValue = 0.0M;
                int GrandQty = 0;
                Grandtotal = Convert.ToDecimal(AddedtotalsList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Total);
                TotalFaceValue = Convert.ToDecimal(AddedtotalsList.Where(n => n.CardName == "Total FaceValue").FirstOrDefault().Total);
                GrandQty = AddedtotalsList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Qty;

                Grandtotal = Grandtotal - Convert.ToDecimal(RemoveCardTotal);
                TotalFaceValue = TotalFaceValue - Convert.ToDecimal(model1.FaceValue);
                GrandQty = GrandQty - 1;


                AddedtotalsList.Remove(RemovedCard);
                RemovedCard.Total = Convert.ToDouble(RemoveCardTotalFaceValue);
                RemovedCard.Qty = RemoveCardTotalQty;
                AddedtotalsList.Add(RemovedCard);


                model.AddedMemberRateTotalModelList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Qty = GrandQty;
                model.AddedMemberRateTotalModelList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Total = Math.Round(Convert.ToDouble(Grandtotal), 2);
                model.AddedMemberRateTotalModelList.Where(n => n.CardName == "Total FaceValue").FirstOrDefault().Total = Math.Round(Convert.ToDouble(TotalFaceValue), 2);
                // model.AddedMemberRateTotalModelList.Where(n => n.CardName == RemovedCard.CardName).FirstOrDefault().Qty = model.AddedMemberRateTotalModelList.Where(n => n.CardName == RemovedCard.CardName).FirstOrDefault().Qty - 1;
                //  model.AddedMemberRateTotalModelList.Where(n => n.CardName == RemovedCard.CardName).FirstOrDefault().Total = model.AddedMemberRateTotalModelList.Where(n => n.CardName == RemovedCard.CardName).FirstOrDefault().Total - RemovedCard.Total;

                if (model.AddedMemberRateTotalModelList.Where(n => n.CardName == RemovedCard.CardName).FirstOrDefault().Qty == 0)
                {
                    model.AddedMemberRateTotalModelList.Remove(RemovedCard);
                }
                if (model.AddedMemberRateTotalModelList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Total <= 0)
                {
                    model.AddedMemberRateTotalModelList.RemoveAll(n => n.CardName == "Grand Total");
                }
                if (model.AddedMemberRateTotalModelList.Where(n => n.CardName == "Total FaceValue").FirstOrDefault().Total <= 0)
                {
                    model.AddedMemberRateTotalModelList.RemoveAll(n => n.CardName == "Total FaceValue");
                }
                return View("ManualVoucher", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("ManualVoucher", model);
            }
        }

        [RBAC(Permission = "CashingManualVoucher")]
        public ActionResult ManualVoucherCheckBusinessType(string Id)
        {
            MemberDA mda = new MemberDA();
            int mkey = mda.GetMemberKey(Id);
            CommissionRateDA crda = new CommissionRateDA();
            ManualCashing model = new ManualCashing();
            model = crda.GetCurrentMemberBusinessTypeList(mkey);
            model.MemberId = Id;

            if (model.ServiceFeeRadioList.Count == 0)
            {

                ViewBag.Msg = "No member vehicle created";
                return View("CommonConfirmedNormal");
            }
            if (model.ServiceFeeRadioList.Count == 1)
            {
                string bus = model.ServiceFeeRadioList.FirstOrDefault().BusinessType;
                decimal rate = model.ServiceFeeRadioList.FirstOrDefault().ServiceFeeRate;
                model.ServiceFeeRateId = rate;
                model.BusinessTypeId = bus;
                Session["ServiceRateModel"] = model;
                return RedirectToAction("ManualVoucher", new { m = model });
            }
            else
            {
                return View(model);
            }
        }

        public JsonResult SetServicefee(decimal Servicefee)
        {
            Session["ServiceFee"] = Servicefee;
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        //[RBAC(Permission = "CashingManualVoucher")]
        public ActionResult ManualVoucher(ManualCashing m)
        {
            Session["MemberRateModel"] = null;
            if (m == null)
            {
                if (Session["ServiceRateModel"] != null)
                {
                    m = (ManualCashing)Session["ServiceRateModel"];
                }
            }
            else
            {
                MemberDA mda = new MemberDA();
                int mkey = mda.GetMemberKey(m.MemberId);
                CommissionRateDA crda = new CommissionRateDA();
                ManualCashing modelc = new ManualCashing();
                modelc = crda.GetCurrentMemberBusinessTypeList(mkey);
                var f = Session["ServiceFee"];
                decimal rf = Convert.ToDecimal(f);
                var ff = (from f1 in modelc.ServiceFeeRadioList
                          where f1.ServiceFeeRate == rf
                          select f1.BusinessType).FirstOrDefault();
                m.ServiceFeeRateId = rf;
                m.BusinessTypeId = ff;

            }
            CommissionRate_MemberRateModelList model = new CommissionRate_MemberRateModelList();
            try
            {
                MemberDA mda = new MemberDA();
                int mkey = mda.GetMemberKey(m.MemberId);
                CommissionRateDA crda = new CommissionRateDA();
                model.MemberRateModelList = crda.GetCurrentMemberRate(mkey, m.BusinessTypeId);
                model.ServiceFeeRate = m.ServiceFeeRateId;
                model.BusinessType = m.BusinessTypeId;
                if (model.MemberRateModelList.Count != 0)
                {
                    ViewBag.LastCursor = model.MemberRateModelList.FirstOrDefault().DocketPayMethod_key.Value;
                }
                Session["MemberRateModel"] = model;
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        [HttpPost]
        //[RBAC(Permission = "CashingManualVoucher")]
        public ActionResult ManualVoucher(FormCollection c)
        {
            CommissionRate_MemberRateModelList model = new CommissionRate_MemberRateModelList();
            List<CommissionRate_AddedMemberRateModelTotals> AddedtotalsList = new List<CommissionRate_AddedMemberRateModelTotals>();

            //if there is a total's list already get that 
            model = (CommissionRate_MemberRateModelList)Session["MemberRateModel"];
            try
            {

                AddedtotalsList = model.AddedMemberRateTotalModelList;
                decimal TotalFaceValue = 0.0M;
                decimal Grandtotal = 0.0M;
                int GrandQty = 0;
                int Count = 0;
                if (AddedtotalsList.Count > 0)
                {
                    Grandtotal = Convert.ToDecimal(AddedtotalsList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Total);
                    TotalFaceValue = Convert.ToDecimal(AddedtotalsList.Where(n => n.CardName == "Total FaceValue").FirstOrDefault().Total);
                    GrandQty = AddedtotalsList.Where(n => n.CardName == "Grand Total").FirstOrDefault().Qty;
                    Count = model.AddedMemberRateModelList.OrderByDescending(n => n.Id).FirstOrDefault().Id;
                }

                foreach (var ml in model.MemberRateModelList)
                {
                    CommissionRate_AddedMemberRateModel newAddedRate = new CommissionRate_AddedMemberRateModel();
                    var facevalue = (c[ml.DocketPayMethod_key.Value.ToString()].ToString()).TrimEnd(',');
                    if (facevalue != string.Empty)
                    {
                        ViewBag.LastCursor = ml.DocketPayMethod_key.Value;
                        Count = Count + 1;
                        var CardName = ml.Name;
                        newAddedRate.Id = Count;
                        newAddedRate.DocketPayMethod_key = ml.DocketPayMethod_key;
                        newAddedRate.FaceValue = Math.Round(Convert.ToDouble(facevalue), 2);
                        newAddedRate.Name = CardName;
                        newAddedRate.ManualCommissionRate = ml.ManualCommissionRate;
                        decimal hc = 0;
                        if (ml.FlatAmount == true)
                        {
                            newAddedRate.HandlingCharge = ml.HandlingAmount.ToString();
                            hc = ml.HandlingAmount.Value;
                        }
                        else
                        {
                            newAddedRate.HandlingCharge = (Convert.ToDouble(facevalue) * Convert.ToDouble(ml.HandlingRate)).ToString();
                            hc = Convert.ToDecimal(Convert.ToDouble(facevalue) * Convert.ToDouble(ml.HandlingRate));
                        }
                        double y = Convert.ToDouble(facevalue) * Convert.ToDouble(ml.ManualCommissionRate / 100);
                        double x = Convert.ToDouble(hc);
                        double z = Convert.ToDouble(facevalue) + y + x;
                        newAddedRate.Total = Math.Round(z, 2);
                        model.AddedMemberRateModelList.Add(newAddedRate);


                        //adding Totals
                        Grandtotal = Grandtotal + Convert.ToDecimal(z);
                        TotalFaceValue = TotalFaceValue + Convert.ToDecimal(facevalue);
                        GrandQty = GrandQty + 1;
                        var model1 = (from n in AddedtotalsList
                                      where n.CardName == newAddedRate.Name
                                      select n).FirstOrDefault();
                        if (model1 == null)
                        {
                            CommissionRate_AddedMemberRateModelTotals Addedtotals = new CommissionRate_AddedMemberRateModelTotals();
                            Addedtotals.CardName = CardName;
                            Addedtotals.Qty = 1;
                            Addedtotals.Total = Math.Round(z, 2);
                            AddedtotalsList.Add(Addedtotals);
                        }
                        else
                        {
                            AddedtotalsList.Remove(model1);
                            model1.Qty = model1.Qty + 1;
                            double a = model1.Total.Value + z;
                            model1.Total = Math.Round(a, 2);
                            AddedtotalsList.Add(model1);
                        }

                        model.AddedMemberRateTotalModelList = AddedtotalsList;
                    }
                }

                //Adding GrandTotal
                var mode2 = (from n in AddedtotalsList
                             where n.CardName == "Grand Total"
                             select n).FirstOrDefault();
                var mode3 = (from n in AddedtotalsList
                             where n.CardName == "Total FaceValue"
                             select n).FirstOrDefault();
                if (mode2 == null)
                {
                    CommissionRate_AddedMemberRateModelTotals Grandtotals = new CommissionRate_AddedMemberRateModelTotals();
                    Grandtotals.CardName = "Grand Total";
                    Grandtotals.Qty = GrandQty;
                    Grandtotals.Total = Math.Round(Convert.ToDouble(Grandtotal), 2);
                    model.AddedMemberRateTotalModelList.Add(Grandtotals);
                    CommissionRate_AddedMemberRateModelTotals TotalFace = new CommissionRate_AddedMemberRateModelTotals();
                    TotalFace.CardName = "Total FaceValue";
                    TotalFace.Total = Math.Round(Convert.ToDouble(TotalFaceValue), 2);
                    model.AddedMemberRateTotalModelList.Add(TotalFace);
                }
                else
                {
                    AddedtotalsList.Remove(mode2);
                    AddedtotalsList.Remove(mode3);
                    //add grand total
                    mode2.Qty = GrandQty;
                    mode2.Total = Math.Round(Convert.ToDouble(Grandtotal), 2);
                    AddedtotalsList.Add(mode2);
                    //add totalface value
                    mode3.Total = Math.Round(Convert.ToDouble(TotalFaceValue), 2);
                    AddedtotalsList.Add(mode3);

                }
                var servicefeeRate = Convert.ToDecimal(c["ServiceFeeRate"].ToString());
                model.ServiceFeeRate = servicefeeRate;
                Session["MemberRateModel"] = model;
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }
        [RBAC(Permission = "CashingManualVoucher")]
        public ActionResult AddManualVoucher()
        {
            CommissionRate_MemberRateModelList model = new CommissionRate_MemberRateModelList();
            WebCashingResult MainModel = new WebCashingResult();

            model = (CommissionRate_MemberRateModelList)Session["MemberRateModel"];
            DocketCheckDA dcd = new DocketCheckDA();

            Guid uId = (Guid)Session["UserId"];

            int officekey = (int)Session["officekey"];
            double commissionRate = (double)Session["commissionRate"];
            int DocketCheckSummary = 0;
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            bool GenerateAccordin = false;
            int count = 0;
            if (model.AddedMemberRateModelList.Count > 0)
            {
                int Recordcount = model.AddedMemberRateModelList.Count - 1;
                //StreamWriter sw = new StreamWriter(@"C:\Vensys-Consulting\PhoenixTest.txt");
                //DateTime start = DateTime.Now;
                //sw.WriteLine("start - " + start);
                foreach (var mod in model.AddedMemberRateModelList)
                {
                    if (count == Recordcount)
                    {
                        GenerateAccordin = true;
                    }
                    if (Session["DocketCheckSummary"] != null)
                    {
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    }

                    //commissionRate = Convert.ToDouble(mod.ManualCommissionRate/100);
                    //sw.WriteLine(User.Identity.Name + "-Dc" + DocketCheckSummary + "-memberkey" + model1.Member_key.Value + "val" + Convert.ToDecimal(mod.FaceValue) + "-docpaymethod" + mod.DocketPayMethod_key.Value + "-comrate" + commissionRate + "-offkey" + officekey);
                    //DateTime dd = DateTime.Now;
                    ////Write a line of text
                    //sw.WriteLine(count);
                    //sw.WriteLine(dd);
                    var rate = model.ServiceFeeRate / 100;
                    MainModel = dcd.ManualDocketCheck(User.Identity.Name, DocketCheckSummary, model1.Member_key.Value, Convert.ToDecimal(mod.FaceValue), mod.DocketPayMethod_key.Value, commissionRate, officekey, GenerateAccordin, rate);
                    count = count + 1;
                    Session["DocketCheckSummary"] = MainModel.DocketCheckSummaryKey;

                    ////Write a second line of text
                    //DateTime dt = DateTime.Now;
                    //sw.WriteLine(dt);
                    //TimeSpan diffq = dt - dd;
                    //sw.WriteLine("Diff - " + diffq);
                    ////Close the file


                }
                //DateTime end = DateTime.Now;
                //TimeSpan diff = end - start;
                //sw.WriteLine("end - " + end);
                //sw.WriteLine("Diff - " + diff);
                //sw.Close();
                Session["MemberKey"] = model1.Member_key.Value;
            }
            Session["ModelList"] = MainModel;
            Session["MemberRateModel"] = null;
            Session["ServiceRateModel"] = null;
            return RedirectToAction("IndexScan");

        }


        public ActionResult Menu()
        {
            return View();
        }
        public class ReportsResult : ActionResult
        {
            public ReportsResult(byte[] data, string mineType, string format)
            {
                this.Data = data;
                this.MineType = mineType;
                this.Format = format;
            }

            public byte[] Data { get; set; }
            public string MineType { get; set; }
            public string Format { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                if (Format == "PDF")
                {
                    MemoryStream ms = new MemoryStream(Data);
                    context.HttpContext.Response.ContentType = "application/pdf";
                    context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename=Report.pdf");
                    context.HttpContext.Response.Buffer = true;
                    ms.WriteTo(context.HttpContext.Response.OutputStream);
                }
                if (Format == "HTML4.0")
                {
                    if (Data == null)
                    {
                        new EmptyResult().ExecuteResult(context);
                        return;
                    }
                    context.HttpContext.Response.ContentType = MineType;

                    using (MemoryStream ms = new MemoryStream(Data))
                    {
                        ms.Position = 0;
                        using (StreamReader sr = new StreamReader(ms))
                        {
                            context.HttpContext.Response.Output.Write(sr.ReadToEnd());
                        }
                    }
                }
            }
        }


        public ActionResult PrintPaidOne(string LogNo)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                PhoenixWeb.ReportingWebService.Warning[] warnings;
                string[] streamIds;

                string RefNumber = LogNo;// Session["LodgmentNo"].ToString();
                string ReportName = string.Empty;
                IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();
                string ScanType1 = Session["ScanType"].ToString();

                ReportName = "/Cashing Reports/Operator Cashing Report";
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "RefNumber", Value = RefNumber });
                parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });

                string format = "PDF";
                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                //Session["MemberKey"] = 0;
                return new FileContentResult(output, mimeType);
            }
            catch (Exception ex)
            {
                int DocketCheckSummary = (int)Session["DocketCheckSummary"];
                ViewBag.Error = ex.Message;
                if (DocketCheckSummary != 0)
                {
                    DocketCheckDA dcd = new DocketCheckDA();
                    dcd.CancelDocketCheck(DocketCheckSummary);
                    return RedirectToAction("IndexScan");
                }
            }
            ViewBag.Error = "Error Printing";
            WebCashingResult model1 = new WebCashingResult();
            return View("PayDone", model1);
        }

        [RBAC(Permission = "CashingPrintStatement")]
        public ActionResult Print()
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                PhoenixWeb.ReportingWebService.Warning[] warnings;
                string[] streamIds;

                string RefNumber = Session["LodgmentNo"].ToString();
                string DocketCheckSummary = Session["DocketCheckSummary"].ToString();
                string ReportName = string.Empty;
                IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();
                string ScanType1 = Session["ScanType"].ToString();
                if (ScanType1 == "MassScan")
                {
                    ReportName = "/LTEPAdminReports/MassScanningReport";
                    parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "DocketCheckSummaryKey", Value = DocketCheckSummary });
                }
                else
                {
                    ReportName = "/Cashing Reports/Operator Cashing Report";
                    parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "RefNumber", Value = RefNumber });
                    parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });
                }

                // string format = "HTML4.0";
                string format = "PDF";

                PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                Session["MemberKey"] = 0;
                //Session["DocketCheckSummary"] = 0;
                //Session["ModelList"] = 0;
                return new FileContentResult(output, mimeType);

                // return new ReportsResult(output, mimeType, format);

            }
            catch (Exception ex)
            {
                //int DocketCheckSummary = (int)Session["DocketCheckSummary"];
                //ViewBag.Error = ex.Message;
                //if (DocketCheckSummary != 0)
                //{
                //    DocketCheckDA dcd = new DocketCheckDA();
                //    dcd.CancelDocketCheck(DocketCheckSummary);
                //    return RedirectToAction("IndexScan");
                //}
                //ViewBag.Error = "Error Printing";
                //WebCashingResult model11 = new WebCashingResult();
                //return View("PayDone", model11);

                return Content("<h4 style='color:red'>An Error Occured, Cannot Printing statement!</h4>");
            }
            //ViewBag.Error = "Error Printing";
            //WebCashingResult model1 = new WebCashingResult();
            //return View("PayDone", model1);
        }

        [RBAC(Permission = "CashingViewMemberNotes")]
        public ActionResult ViewMemberNotes(string Id)
        {
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(Id);

            MemberModel mobj = new MemberModel();
            mobj = mda.GetMemeberLowVersion(member_key);

            MemberNotesDA mdna = new MemberNotesDA();
            IEnumerable<MemberNotesModel> mnm;
            mnm = mdna.GetMemberNotes(member_key);
            Session["MemeberKey"] = member_key;
            Session["MemeberId"] = Id;
            MemberNoteVM model = new MemberNoteVM();
            model.MemberId = mobj.MemberId;
            model.FirstName = mobj.FirstName;
            model.LastName = mobj.LastName;
            model.TradingName = mobj.TradingName;
            model.MemberNotes = mnm.ToList();

            return View("MemberNotes", model);
        }

        [HttpPost]
        [RBAC(Permission = "CashingCreateMemberNotes")]
        public ActionResult CreateMemberNotes(FormCollection c, MemberNoteVM m)
        {
            MemberNotesDA mda = new MemberNotesDA();
            MemberNotesModel model = new MemberNotesModel();
            model.NoteDate = DateTime.Now;
            var t = c["IsWarning"];
            if (t == "on")
                model.Warning = true;
            else
                model.Warning = false;

            model.Note = c["Note"].ToString();

            int member_key = (int)Session["MemeberKey"];
            string Id = "0";
            if (member_key != 0)
            {
                Id = Session["MemeberId"].ToString();

                model.Member_key = member_key;
                model.Modified_dttm = DateTime.Now;
                model.ModifiedByUser = User.Identity.Name;

                mda.Add(model);
            }
            return RedirectToAction("IndexScan");
            //  return RedirectToAction("ViewMemberNotes", Id);
        }

        [RBAC(Permission = "CashingShowPhoto")]
        public ActionResult GetPhoto(string memberId)
        {
            MemberDA mda = new MemberDA();
            byte[] photo = mda.GetMemberPhotoByMemberId(memberId);

            return File(photo, "image/jpeg");
        }

        [RBAC(Permission = "CashingWithdrawal")]
        public ActionResult Withdrawal()
        {
            MemberBalanceModel m = (MemberBalanceModel)Session["CurrentWMember"];
            MemberBalanceModel model = new MemberBalanceModel();
            ViewBag.MsgConfirm = "";
            if (m != null)
            {
                decimal Amount = Convert.ToDecimal(Session["Amount"]);
                ViewBag.MsgConfirm = "Balance amount updated! (Amount withdrawn: $" + Amount + ")";
                model = m;
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult Withdrawal(string TopBC)
        {
            MemberBalanceModel model = new MemberBalanceModel();
            if (TopBC != null)
            {

                if (TopBC.Length > 7)
                {
                    string TerminalId = TopBC.Substring(0, 8);
                    Session["TopBC"] = TerminalId;
                    Session["WMemberID"] = 0;
                    Session["Amount"] = 0;
                    MemberDA mda = new MemberDA();
                    int member_key = mda.GetMemberKeyByTerminalId(TerminalId);
                    model = mda.GetMemberBalance(member_key);
                }
                else
                {
                    MemberBalanceModel modelNull = new MemberBalanceModel();
                    ViewBag.Error = "length of Top Bar Code should be atleast 8 number";
                    return View(modelNull);
                }
            }
            return View(model);
        }

        public ActionResult PayWithdrawalConfirm(string memberId, decimal Balance)
        {
            @ViewBag.MemberId = memberId;
            @ViewBag.Balance = Balance;
            Session["WMemberID"] = memberId;
            return View();
        }


        public ActionResult PayWithdrawalMsg(FormCollection c)
        {
            decimal amt = Convert.ToDecimal(c["Amount"]);
            decimal bal = Convert.ToDecimal(c["Balance"]);
            string mID = Session["WMemberID"].ToString();
            Session["Amount"] = amt;

            @ViewBag.MemberId = mID;
            @ViewBag.Balance = bal;
            @ViewBag.Amount = amt;
            if (amt > bal)
            {
                ViewBag.Error = "Withdrawal Amount should be less then balance amount";
                return View("PayWithdrawalConfirm");
            }
            if (amt < 0)
            {
                ViewBag.Error = "Amount cannot be less than zero dollar";
                return View("PayWithdrawalConfirm");
            }
            else
            {
                return View();
            }
        }

        public ActionResult PayWithdrawal(FormCollection c)
        {
            UserDA uda = new UserDA();
            MemberAccountEntryDA maeda = new MemberAccountEntryDA();
            MemberAccountDA mada = new MemberAccountDA();
            MemberAccountEntryModel model = new MemberAccountEntryModel();
            string MemberId = Session["WMemberID"].ToString();
            if (MemberId == "0")
            {
                MemberBalanceModel nullmodel = new MemberBalanceModel();
                return RedirectToAction("Withdrawal", new { nullmodel });// View("Withdrawal", nullmodel);
            }
            decimal Amount = Convert.ToDecimal(Session["Amount"]);// Convert.ToDecimal(c["Amount"].ToString());
            string TerminalId = Session["TopBC"].ToString();
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKeyByTerminalId(TerminalId);
            MemberBalanceModel m = mda.GetMemberBalance(member_key);
            ViewBag.Error = "";
            try
            {
                decimal bal = mda.GetMemberBalance(member_key).Balance;

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;

                model.MemberAccountKey = mada.Get(member_key);
                model.Member_key = member_key;
                model.Transaction_key = null;
                model.EntryDateTime = DateTime.Now;
                model.Description = "Widthdrawal over the counter by " + User.Identity.Name;
                model.CreditAmount = 0.0M;
                model.DebitAmount = Amount;
                model.BalanceAmount = 0.0M;
                model.ReversedMemberAccountEntryKey = null;
                model.Office_key = uda.GetUserOfficeKey(uId);
                model.CreatedByUser = User.Identity.Name;
                model.CreatedDateTime = DateTime.Now;
                model.UpdatedByUser = User.Identity.Name;
                model.UpdatedDateTime = DateTime.Now;

                maeda.Add(model);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Withdrawal", m);
            }
            //Session["WMemberID"] = 0;
            Amount = Math.Round(Amount, 2);
            m = mda.GetMemberBalance(member_key);
            Session["CurrentWMember"] = m;
            Session["Amount"] = Amount;
            //return View("Withdrawal", m);
            return RedirectToAction("Withdrawal");
        }

        public ActionResult QuickScan()
        {
            Session["QuickScan"] = 0;
            List<QuickScanVM> model = new List<QuickScanVM>();
            return View(model);
        }

        //[HttpPost]
        public ActionResult QuickScannning(string QSTopBC, string QSBottomBC)
        {
            List<QuickScanVM> model = new List<QuickScanVM>();
            int newId = 1;
            bool IsPresent = false;
            if (QSTopBC != null && QSBottomBC != null)
            {
                if (Session["QuickScan"] != null && Session["QuickScan"].ToString() != "0")
                {
                    model = (List<QuickScanVM>)Session["QuickScan"];
                    if (model.Count == 0)
                    {
                        newId = 1;
                    }
                    else
                    {
                        newId = model.OrderByDescending(n => n.Id).FirstOrDefault().Id + 1;
                    }
                    var rr = (from r in model
                              where r.TopBarCode == QSTopBC && r.BottomBarCode == QSBottomBC
                              select r).FirstOrDefault();
                    if (rr != null)
                    {
                        IsPresent = true;
                    }
                }
                else
                {
                    model = new List<QuickScanVM>();
                }

                if (IsPresent == false)
                {
                    QuickScanVM newModel = new QuickScanVM();
                    string amt = QSBottomBC.Substring(6, 6);
                    string a1 = string.Empty;
                    if (amt.Substring(0, 1) == "0")
                    {
                        a1 = amt.Substring(1, 3);
                    }
                    else
                    {
                        a1 = amt.Substring(0, 4);
                    }

                    string a2 = amt.Substring(4, 2);
                    string a3 = "$" + a1 + "." + a2;
                    newModel.Id = newId;
                    newModel.TopBarCode = QSTopBC;
                    newModel.BottomBarCode = QSBottomBC;
                    newModel.Amount = a3;
                    Session["QuickScan"] = model;
                    model.Add(newModel);
                }

            }
            return View("QuickScan", model);
        }

        public ActionResult RemoveQuick(int Id)
        {
            List<QuickScanVM> model = new List<QuickScanVM>();

            if (Session["QuickScan"] != null && Session["QuickScan"].ToString() != "0")
            {
                model = (List<QuickScanVM>)Session["QuickScan"];
                QuickScanVM ToRemove = model.FirstOrDefault(n => n.Id == Id);
                model.Remove(ToRemove);
                Session["QuickScan"] = model;
            }
            return View("QuickScan", model);
        }


        public ActionResult ProcessQuickScan()
        {
            List<QuickScanVM> model = new List<QuickScanVM>();
            WebCashingResult MainModel = new WebCashingResult();

            model = (List<QuickScanVM>)Session["QuickScan"];
            DocketCheckDA dcd = new DocketCheckDA();

            Guid uId = (Guid)Session["UserId"];

            int officekey = (int)Session["officekey"];
            double commissionRate = (double)Session["commissionRate"];
            int DocketCheckSummary = 0;
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            bool GenerateAccordin = false;
            int count = 0;
            if (model.Count > 0)
            {
                int Recordcount = model.Count - 1;
                foreach (var mod in model)
                {
                    if (count == Recordcount)
                    {
                        GenerateAccordin = true;
                    }
                    if (Session["DocketCheckSummary"] != null)
                    {
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    }
                    try
                    {
                        MainModel = dcd.DocketCheck(mod.TopBarCode, mod.BottomBarCode, User.Identity.Name, DocketCheckSummary, uId, "MassScanNot", "False", string.Empty, commissionRate, officekey, GenerateAccordin, 0);
                        count = count + 1;
                        Session["TopBC"] = mod.TopBarCode;
                        Session["BottomBC"] = mod.BottomBarCode;
                        Session["DocketCheckSummary"] = MainModel.DocketCheckSummaryKey;
                    }
                    catch
                    {

                    }
                }

                Session["LodgmentNo"] = MainModel.LodgementReferenceNo;
                Session["MemberKey"] = MainModel.Member_key;
                Session["ModelList"] = MainModel;

                //Session["MemberKey"] = model1.Member_key.Value;
            }
            Session["ModelList"] = MainModel;
            return RedirectToAction("IndexScan");
        }

        [RBAC(Permission = "CashingScanDocket")]
        public ActionResult Search(string TopBC, string BottomBC, string ScanType, WebCashingResult DelModel, int? Year)
        {
            this.LogInfo(String.Format("Scan docket top barcode: {0}, bottom barcode: {1}, Year : {2} ", TopBC, BottomBC, Year.GetValueOrDefault(0).ToString()));
            if (TopBC != null && BottomBC != null)
            {
                int day = Convert.ToInt32(BottomBC.Substring(0, 2));
                int month = Convert.ToInt32(BottomBC.Substring(2, 2));
                int member_key;
                try
                {
                    string DocketMonth = BottomBC.Substring(2, 2);
                    string DocketDay = BottomBC.Substring(0, 2);
                    CommonDA com = new CommonDA();
                    int year = com.GetDocketYear(DocketMonth, DocketDay);
                    this.LogInfo(String.Format("Get Docket Year: {0}", year.ToString()));
                    if (Year == null)
                    {
                        Year = year;
                    }

                    DateTime Date = new DateTime(year, month, day);
                    if (Convert.ToInt32(Session["MemberKey"].ToString()) == 0)
                    {
                        string TerminalId = TopBC.Substring(0, 8);
                        DateTime Date1 = new DateTime(year, month, day);
                        MemberDA mda = new MemberDA();
                        member_key = mda.GetMemberKeyByTerminalIdByEffecDate(TerminalId, Date1);
                        this.LogInfo(String.Format("Get Member Key By TerminalID By Effect Date, memberkey: {0}", member_key.ToString()));
                        Session["MemberKey"] = member_key;
                    }
                }
                catch (Exception ex)
                {
                    WebCashingResult model = new WebCashingResult();
                    if (Session["ModelList"] != null)
                    {
                        model = (WebCashingResult)Session["ModelList"];
                    }
                    model.ErrorMsg = "Please check bottom barcode, seems invalid";
                    Session["ModelList"] = model;
                    //ViewBag.Error = "Please check bottom barcode, seems invalid";
                    if (ScanType == "MassScan")
                    {

                        return View("ListMassScan", model);
                    }
                    else
                    {
                        return RedirectToAction("IndexScan", model);
                    }
                }

            }

            string paid = Session["Paid"].ToString();

            if (paid == "false")
            {
                WebCashingResult model = new WebCashingResult();
                if (TopBC != null && BottomBC != null && DelModel.Member_key == null)
                {

                    DocketCheckDA dcd = new DocketCheckDA(logger);
                    var Val1 = Session["DocketCheckSummary"];
                    int DocketCheckSummary = 0;
                    if (Val1 != null)
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];

                    Guid uId = (Guid)Session["UserId"];

                    int officekey = (int)Session["officekey"];
                    double commissionRate = (double)Session["commissionRate"];

                    WebCashingResult model1 = new WebCashingResult();
                    model1 = (WebCashingResult)Session["ModelList"];

                    this.LogInfo(String.Format("Start checking docket"));
                    if (ScanType == "MassScan")
                    {
                        Session["ScanType"] = "MassScan";
                        model = dcd.MassDocketCheck(TopBC, BottomBC, User.Identity.Name, DocketCheckSummary, commissionRate, officekey, model1);
                    }
                    else
                    {
                        Session["ScanType"] = "";
                        if (Year == null)
                        {
                            Year = 0;
                        }
                        int mkey = (int)Session["AuthorisedMemberKey"];
                        int? mkey1;
                        if (mkey > 0)
                        {

                            mkey1 = mkey;
                            model = dcd.DocketCheck(TopBC,
                                BottomBC,
                                User.Identity.Name,
                                DocketCheckSummary,
                                uId, ScanType,
                                "False",
                                string.Empty,
                                commissionRate, officekey, true, Year.Value, mkey1, false);

                        }
                        else
                        {
                            mkey1 = Convert.ToInt32(Session["MemberKey"].ToString());
                            model = dcd.DocketCheck(TopBC, BottomBC, User.Identity.Name, DocketCheckSummary, uId, ScanType, "False", string.Empty, commissionRate, officekey, true, Year.Value, mkey1);
                        }

                        this.LogInfo(String.Format("finish checking docket"));
                        Session["ModelList"] = model;
                    }
                }

                if (Session["DelModel"] != null)
                {
                    DelModel = (WebCashingResult)Session["DelModel"];
                    if (DelModel != null && DelModel.Member_key != null)
                    {
                        model = DelModel;
                        Session["DelModel"] = null;
                    }
                }

                Session["DocketCheckSummary"] = model.DocketCheckSummaryKey;
                Session["LodgmentNo"] = model.LodgementReferenceNo;
                Session["ModelList"] = model;

                if (TopBC != null)
                {
                    Session["TopBC"] = TopBC;
                }

                if (BottomBC != null)
                {
                    Session["BottomBC"] = BottomBC;
                }

                Session["ValidScanCount"] = model.Count;
                ViewBag.Error = "";
                if (ScanType == "MassScan")
                {
                    LogInfo(string.Format("Finish Scan docket top barcode: {0}, bottom barcode: {1}, Year : {2} ", TopBC, BottomBC, Year.GetValueOrDefault(0).ToString()));
                    return View("ListMassScan", model);
                }

                LogInfo(string.Format("Finish Scan docket top barcode: {0}, bottom barcode: {1}, Year : {2} ", TopBC, BottomBC, Year.GetValueOrDefault(0).ToString()));
                return RedirectToAction("IndexScan");
            }
            else
            {
                WebCashingResult model = new WebCashingResult();
                if (Session["ModelList"] != null)
                {
                    model = (WebCashingResult)Session["ModelList"];
                }
                model.ErrorMsg = "Please click New Batch button to start scanning new batch";
                Session["ModelList"] = model;
                if (ScanType == "MassScan")
                {
                    LogInfo(String.Format("Finish Scan docket top barcode: {0}, bottom barcode: {1}, Year : {2} ", TopBC, BottomBC, Year.GetValueOrDefault(0).ToString()));
                    return View("ListMassScan", model);
                }
                LogInfo(String.Format("Finish Scan docket top barcode: {0}, bottom barcode: {1}, Year : {2} ", TopBC, BottomBC, Year.GetValueOrDefault(0).ToString()));
                return RedirectToAction("IndexScan");
            }
        }

        public ActionResult AcknowledgeChargeBack()
        {
            Session["HasChargeBack"] = "true";
            return RedirectToAction("IndexScan");
        }

        public ActionResult AcknowledgeInsuranceChargeBack()
        {
            Session["HasChargeBackInsurance"] = "true";
            return RedirectToAction("IndexScan");
        }

        public ActionResult AcknowledgeMemberNotesConfirm()
        {
            Session["MemberNotes"] = "true";
            return RedirectToAction("IndexScan");
        }
        public ActionResult AcknowledgeAuthoriseConfirm()
        {
            Session["HasAuthorize"] = "true";
            return RedirectToAction("IndexScan");
        }

        public ActionResult MemberNotesConfirm()
        {
            if (Session["HasAuthorize"].ToString() == "false"
                || Session["MemberNotes"].ToString() == "true"
                || !this.HasPermission("CashingChangeNoteWarning"))
            {
                return Json(new
                {
                    Result = "error"
                }, JsonRequestBehavior.AllowGet);
            }

            WebCashingResult model = new WebCashingResult();
            model = (WebCashingResult)Session["ModelList"];
            if (model != null)
            {
                MemberDA mda = new MemberDA();
                int mkey = mda.GetMemberKey(model.memberid);
                MemberNotesDA mds = new MemberNotesDA();
                bool has = mds.DoesMemberHaveWarning(mkey);
                MemberNotesDA mdna = new MemberNotesDA();
                IEnumerable<MemberNotesModel> drt = null;

                if (model.ResultList == null)
                {
                    if (has)
                    {
                        drt = mdna.GetMemberNotes(mkey);

                        return Json(new
                        {
                            Result = "success",
                            Data = drt
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            Result = "error"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (model.ResultList != null && model.ResultList.Count == 1)
                {
                    if (has)
                    {
                        drt = mdna.GetMemberNotes(mkey);

                        return Json(new
                        {
                            Result = "success",
                            Data = drt
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            Result = "error"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new
            {
                Result = "error"
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChargeBackConfirm()
        {
            if (!this.HasPermission("CashingShowChargeBack"))
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (Session["HasAuthorize"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (Session["MemberNotes"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

            if (Session["HasChargeBack"].ToString() == "true")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            else
            {

                CommonDA com = new CommonDA();
                if ((int)Session["MemberKey"] != 0)
                {
                    int mkey = (int)Session["MemberKey"];
                    TransactionDA dda = new TransactionDA();
                    bool HasChargeBack = false;
                    if (this.HasPermission("CashingAddInsuranceChargeBack"))
                    {
                        HasChargeBack = dda.HasGetChargeBack(mkey, true);
                    }
                    else
                    {
                        HasChargeBack = dda.HasGetChargeBack(mkey, false);
                    }
                    if (HasChargeBack == true)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }

            return Json("error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChargeBack()
        {


            WebCashingResult MainModel = new WebCashingResult();
            DocketCheckDA dcd = new DocketCheckDA();
            TransactionDA tda = new TransactionDA();
            Guid uId = (Guid)Session["UserId"];

            int officekey = (int)Session["officekey"];
            double commissionRate = (double)Session["commissionRate"];
            int DocketCheckSummary = 0;
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            MainModel = model1;
            bool GenerateAccordin = false;
            int count = 0;
            int mkey = model1.Member_key.Value;
            List<WebCashing_GetChargeBackModel> GetChargeBackModel = null;
            if (this.HasPermission("CashingAddInsuranceChargeBack"))
            {
                GetChargeBackModel = tda.GetChargeBack(mkey, true);
            }
            else
            {
                GetChargeBackModel = tda.GetChargeBack(mkey, false);
            }

            if (GetChargeBackModel.Count > 0)
            {
                int Recordcount = GetChargeBackModel.Count - 1;

                foreach (var mod in GetChargeBackModel)
                {
                    if (count == Recordcount)
                    {
                        GenerateAccordin = true;
                    }
                    if (Session["DocketCheckSummary"] != null)
                    {
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    }
                    try
                    {
                        MainModel = dcd.ChargeBackDocketCheck(mod.Description, mod.docket_key.Value, User.Identity.Name, model1.DocketCheckSummaryKey, mkey, GenerateAccordin);

                        count = count + 1;
                        Session["DocketCheckSummary"] = MainModel.DocketCheckSummaryKey;
                        Session["HasChargeBack"] = "true";
                    }
                    catch
                    {

                    }
                }
                Session["MemberKey"] = MainModel.Member_key.Value;
            }
            Session["ModelList"] = MainModel;
            return RedirectToAction("IndexScan");
            //  return View("IndexScan", MainModel);
        }


        /// <summary>
        /// Yes/No Dialog Box to ask user if they want to add this docket as sameday cashing since its not in the pending
        /// </summary>
        /// <returns>success if terminal allocated show dialogbox else error dont show dialogbox</returns>

        public ActionResult SamedayConfirm()
        {
            /*
            if (Session["HasAuthorize"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (Session["MemberNotes"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
             * */
            WebCashingResult model = new WebCashingResult();
            model = (WebCashingResult)Session["ModelList"];
            if (model != null)
            {
                if (model.ResultList != null)
                {
                    if (model.ResultList.Count > 0)
                    {
                        if (!this.HasPermission("CashingShowSameday"))
                        {
                            return Json("error", JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            //#if DEBUG
                            //                            return Json("success", JsonRequestBehavior.AllowGet);

                            //#endif
                            string TopBC = string.Empty;
                            string BottomBC = string.Empty;
                            if (Session["TopBC"] != null)
                            {
                                TopBC = Session["TopBC"].ToString();
                            }
                            if (Session["BottomBC"] != null)
                            {
                                BottomBC = Session["BottomBC"].ToString();
                            }

                            if (TopBC.Length < 12 && BottomBC.Length < 12)
                            {
                                return Json("error", JsonRequestBehavior.AllowGet);
                            }

                            int monthToday = DateTime.Now.Month;
                            string DocketMonth = BottomBC.Substring(2, 2);
                            string DocketDay = BottomBC.Substring(0, 2);
                            CommonDA com = new CommonDA();
                            int year = com.GetDocketYear(DocketMonth, DocketDay);




                            string tempbatch = TopBC.Substring(0, 8).ToString() + BottomBC.Substring(0, 6).ToString() + year.ToString();
                            string batchno = BottomBC.Substring(0, 6).ToString();
                            string termnialid = TopBC.Substring(0, 8).ToString();
                            var status = (from nm in model.ResultList
                                              //where nm.BatchNumber == batchno && nm.TerminalId == termnialid
                                          select nm.BatchStatus).FirstOrDefault();

                            if (status.ToLower() == "invalid batch")
                            {
                                TerminalDA tda = new TerminalDA();
                                MemberDA mda = new MemberDA();
                                DocketTableDA dda = new DocketTableDA();
                                //should not exist already
                                bool BatchIDExists = dda.DoesBatchExists(tempbatch);

                                int mkey = mda.GetMemberKey(model.memberid);
                                //terminal should exists
                                var tmodelExists = tda.CheckTerminalExists(termnialid);

                                //terminal should be allocated to member
                                var tmodelAllocated = tda.IsTerminalAllocatedToThisMember(termnialid, mkey);
                                var IsCashCustomer = mda.IsCashCustomer(mkey);

                                if (tmodelExists != null && tmodelAllocated == true && BatchIDExists == false && IsCashCustomer == true)
                                {
                                    return Json("success", JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json("error", JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            return Json("error", JsonRequestBehavior.AllowGet);

        }


        /// <summary>
        /// DialogBox to show cards types with rates, allows user to enter total amt and quantity
        /// </summary>
        /// <param name="Id">not used</param>
        /// <param name="Businesstype">business type of this member eg taxi,lomo</param>
        /// <returns>DialogBox to show cards types with rates, allows user to enter total amt and quantity</returns>
        public ActionResult Sameday(string Id, string Businesstype)
        {
            Session["MemberRateSameDayModel"] = null;
            CommissionRate_MemberRateModelSameList model = new CommissionRate_MemberRateModelSameList();

            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];

            string TopBC = Session["TopBC"].ToString();
            string BottomBC = Session["BottomBC"].ToString();

            MemberDA mda = new MemberDA();
            int mkey = mda.GetMemberKey(model1.memberid);

            VehicleTypeDA vda = new VehicleTypeDA();
            Businesstype = vda.GetVehicleTypeByTerminal(TopBC.Substring(0, 8).ToString());

            CommissionRateDA crda = new CommissionRateDA();
            model.MemberRateModelList = crda.GetCurrentMemberSameDayRate(mkey, Businesstype);

            TerminalHostDA terminalHostDA = new TerminalHostDA();
            if (terminalHostDA.IsAllowAccessToGlidebox(mkey))
            {
                CategoriesDA categoriesDA = new CategoriesDA();
                model.GlideboxCommissionList = categoriesDA.GetGlideboxCategoriesCommissionForSameDay();
            }

            decimal Amt = Convert.ToDecimal(BottomBC.Substring(6, 6)) / 100;


            Session["MemberRateSameDayModel"] = model;
            model.TopBC = TopBC;
            model.BottomBC = BottomBC;
            model.BatchAmount = Amt.ToString();
            ViewBag.BatchAmount = "$" + Amt.ToString();

            return View(model);
        }

        [HttpPost]
        public ActionResult Sameday(FormCollection c)
        {
            CommissionRate_MemberRateModelSameList model = new CommissionRate_MemberRateModelSameList();
            model = (CommissionRate_MemberRateModelSameList)Session["MemberRateSameDayModel"];
            if (model.AddedMemberRateSameTotalModelList == null)
            {
                model.AddedMemberRateSameTotalModelList = new CommissionRate_AddedMemberRateModelSameTotals();
            }
            if (model.AddedMemberRateSameModelList.Count == 0)
            {
                model.AddedMemberRateSameModelList = new List<CommissionRate_AddedMemberRateSameModel>();
            }

            decimal GrandFaceValue = 0.0M;
            decimal Grandtotal = 0.0M;
            decimal GrandComm = 0.0M;
            decimal GrandHChrg = 0.0M;
            int GrandQty = 0;
            int Count = 0;

            string TopBC = string.Empty;
            string BottomBC = string.Empty;
            if (Session["TopBC"] != null)
            {
                TopBC = Session["TopBC"].ToString();
            }
            if (Session["BottomBC"] != null)
            {
                BottomBC = Session["BottomBC"].ToString();
            }
            decimal batchAmount = Convert.ToDecimal(BottomBC.Substring(6, 6)) / 100;
            ViewBag.BatchAmount = "$" + batchAmount.ToString();

            foreach (var ml in model.MemberRateModelList)
            {

                CommissionRate_AddedMemberRateSameModel newAddedRate = new CommissionRate_AddedMemberRateSameModel();
                var facevalue = (c[ml.DocketPayMethod_key.Value.ToString()].ToString()).TrimEnd(',');
                var Quantity = (c[ml.DocketPayMethod_key1.Value.ToString()].ToString()).TrimEnd(',');
                if (facevalue != string.Empty)
                {
                    if (Quantity == null || Quantity == "")
                    {
                        Quantity = "1";
                    }
                    Count = Count + 1;
                    var CardName = ml.Name;
                    newAddedRate.Id = Count;
                    newAddedRate.DocketPayMethod_key = ml.DocketPayMethod_key;
                    newAddedRate.Quantity = Convert.ToInt32(Quantity);
                    newAddedRate.FaceValue = Math.Round(Convert.ToDouble(facevalue), 2);
                    newAddedRate.Name = CardName;
                    newAddedRate.ManualCommissionRate = ml.ManualCommissionRate;
                    newAddedRate.CommissionRate = ml.OperatorCommissionRate;
                    decimal hc = 0;
                    if (ml.FlatAmount == true)
                    {
                        newAddedRate.HandlingCharge = ml.HandlingAmount.ToString();
                        hc = ml.HandlingAmount.Value;
                    }
                    else
                    {
                        newAddedRate.HandlingCharge = (Convert.ToDouble(facevalue) * Convert.ToDouble(ml.HandlingRate)).ToString();
                        hc = Convert.ToDecimal(Convert.ToDouble(facevalue) * Convert.ToDouble(ml.HandlingRate));
                    }
                    double y = Convert.ToDouble(facevalue) * Convert.ToDouble(ml.OperatorCommissionRate / 100);
                    double x = Convert.ToDouble(hc);
                    double z = Convert.ToDouble(facevalue) + y + x;
                    newAddedRate.Total = Math.Round(z, 2);
                    newAddedRate.Commission = Math.Round(y, 2);



                    var mm = (from m in model.AddedMemberRateSameModelList
                              where m.Name == ml.Name
                              select m).FirstOrDefault();

                    if (mm == null)
                    {
                        int cot = 0;
                        if (model.AddedMemberRateSameModelList.Count > 0)
                        {
                            cot = (model.AddedMemberRateSameModelList.OrderBy(n => n.Id).LastOrDefault().Id) + 1;
                        }
                        else
                        {
                            cot = 1;
                        }

                        newAddedRate.Id = cot;
                        model.AddedMemberRateSameModelList.Add(newAddedRate);
                        //adding Totals
                        GrandFaceValue = Convert.ToDecimal(model.AddedMemberRateSameTotalModelList.FaceValue) + Convert.ToDecimal(newAddedRate.FaceValue);
                        Grandtotal = Grandtotal + Convert.ToDecimal(z);
                        GrandQty = GrandQty + Convert.ToInt32(newAddedRate.Quantity);
                        GrandComm = GrandComm + Convert.ToDecimal(newAddedRate.Commission);
                        GrandHChrg = GrandHChrg + Convert.ToDecimal(x);
                    }
                    else
                    {
                        model.AddedMemberRateSameModelList.Remove(mm);
                        mm.Quantity = mm.Quantity + Convert.ToInt32(Quantity);
                        mm.FaceValue = mm.FaceValue + Math.Round(Convert.ToDouble(facevalue), 2);
                        model.AddedMemberRateSameModelList.Add(mm);
                        //newAddedRate.FaceValue = mm.FaceValue;

                        GrandFaceValue = Convert.ToDecimal(Convert.ToDouble(facevalue)) + Convert.ToDecimal(model.AddedMemberRateSameTotalModelList.FaceValue);
                        Grandtotal = 0;
                        GrandQty = 0;
                        GrandComm = 0;
                        GrandHChrg = 0;
                    }

                    CommissionRate_AddedMemberRateModelSameTotals Addedtotals = new CommissionRate_AddedMemberRateModelSameTotals();
                    Addedtotals.FaceValue = Convert.ToDouble(GrandFaceValue);
                    Addedtotals.Commission = Convert.ToDouble(GrandComm);
                    Addedtotals.Total = Convert.ToDouble(Grandtotal);
                    Addedtotals.HandlingChrg = Convert.ToDouble(GrandHChrg);

                    model.AddedMemberRateSameTotalModelList = Addedtotals;
                }
            }

            if (model.AddedMemberRateSameModelList.Sum(x => x.FaceValue) == Convert.ToDouble(batchAmount))
            {
                ViewBag.ShowProcess = "true";
            }
            else
            {
                ViewBag.ShowProcess = "false";
            }

            foreach (var item in model.GlideboxCommissionList)
            {
                if (c[item.CategoryID.ToString()] != null)
                {
                    int.TryParse(c[item.CategoryID.ToString()].ToString(), out int quantity);
                    if (quantity > 0)
                    {
                        var glideboxCategory = model.AddedGlideboxCommissionList.FirstOrDefault(x => x.CategoryID == item.CategoryID);

                        if (glideboxCategory == null)
                        {
                            GlideboxCategoriesCommissionForSameDayCashingModel glideboxCommission = new GlideboxCategoriesCommissionForSameDayCashingModel
                            {
                                CategoryID = item.CategoryID,
                                ShortName = item.ShortName,
                                Price = item.Price,
                                Quantity = quantity,
                                CommissionAmount = item.CommissionAmount,
                                TotalCommissionAmount = quantity * item.CommissionAmount
                            };
                            model.AddedGlideboxCommissionList.Add(glideboxCommission);
                        }
                        else
                        {
                            glideboxCategory.Quantity += quantity;
                            glideboxCategory.TotalCommissionAmount = glideboxCategory.Quantity * item.CommissionAmount;
                        }
                    }
                }
            }

            Session["MemberRateSameDayModel"] = model;
            return View(model);
        }

        public ActionResult AddSameDayCashing()
        {
            CommissionRate_MemberRateModelSameList model = new CommissionRate_MemberRateModelSameList();
            WebCashingResult MainModel = new WebCashingResult();

            model = (CommissionRate_MemberRateModelSameList)Session["MemberRateSameDayModel"];
            DocketCheckDA dcd = new DocketCheckDA();
            string TopBC = string.Empty;
            string BottomBC = string.Empty;
            if (Session["TopBC"] != null)
            {
                TopBC = Session["TopBC"].ToString();
            }
            if (Session["BottomBC"] != null)
            {
                BottomBC = Session["BottomBC"].ToString();
            }

            Guid uId = (Guid)Session["UserId"];

            int officekey = (int)Session["officekey"];
            double commissionRate = (double)Session["commissionRate"];
            int DocketCheckSummary = 0;
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            int count = 0;
            bool accordin = false;
            if (model.AddedMemberRateSameModelList.Count > 0)
            {
                int Recordcount = model.AddedMemberRateSameModelList.Count - 1;

                DataTable glideboxCommissionTable = new DataTable("dbo.TVPGlideboxCommissionForSameDayCashing");
                glideboxCommissionTable.Columns.Add("CategoryID", typeof(int));
                glideboxCommissionTable.Columns.Add("ShortName", typeof(string));
                glideboxCommissionTable.Columns.Add("Price", typeof(decimal));
                glideboxCommissionTable.Columns.Add("Quantity", typeof(int));
                glideboxCommissionTable.Columns.Add("CommissionAmount", typeof(decimal));
                glideboxCommissionTable.Columns.Add("TotalCommissionAmount", typeof(decimal));
                glideboxCommissionTable.Columns.Add("IsPercentage", typeof(bool));

                foreach (var item in model.AddedGlideboxCommissionList)
                {
                    glideboxCommissionTable.Rows.Add(item.CategoryID, item.ShortName, item.Price, item.Quantity, item.CommissionAmount, item.TotalCommissionAmount, false);
                }

                foreach (var mod in model.AddedMemberRateSameModelList)
                {
                    if (Recordcount == count)
                    {
                        accordin = true;
                    }
                    //commissionRate = Convert.ToDouble(mod.CommissionRate/100);
                    if (Session["DocketCheckSummary"] != null)
                    {
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    }
                    MainModel = dcd.SameDayDocketCheck(TopBC, BottomBC, User.Identity.Name, DocketCheckSummary, model1.Member_key.Value, mod.Quantity.Value, Convert.ToDecimal(mod.FaceValue), mod.DocketPayMethod_key.Value, commissionRate, officekey, accordin, glideboxCommissionTable);

                    Session["DocketCheckSummary"] = MainModel.DocketCheckSummaryKey;
                    Session["MemberKey"] = model1.Member_key.Value;
                    count = count + 1;

                }
            }

            Session["ModelList"] = MainModel;
            return RedirectToAction("IndexScan");

        }

        public ActionResult RemoveSamedayDocket(int Id)
        {
            CommissionRate_MemberRateModelSameList model = new CommissionRate_MemberRateModelSameList();
            model = (CommissionRate_MemberRateModelSameList)Session["MemberRateSameDayModel"];

            decimal GrandFaceValue = 0.0M;

            var mm = (from m in model.AddedMemberRateSameModelList
                      where m.Id == Id
                      select m).FirstOrDefault();

            if (mm != null)
            {
                model.AddedMemberRateSameModelList.Remove(mm);
                GrandFaceValue = Convert.ToDecimal(model.AddedMemberRateSameTotalModelList.FaceValue) - Convert.ToDecimal(mm.FaceValue);
                model.AddedMemberRateSameTotalModelList.FaceValue = Convert.ToDouble(GrandFaceValue);
            }

            if (Session["BottomBC"] != null)
            {
                string BottomBC = Session["BottomBC"].ToString();
                decimal batchAmount = Convert.ToDecimal(BottomBC.Substring(6, 6)) / 100;
                ViewBag.BatchAmount = "$" + batchAmount.ToString();

                if (model.AddedMemberRateSameModelList.Sum(x => x.FaceValue) == Convert.ToDouble(batchAmount))
                {
                    ViewBag.ShowProcess = "true";
                }
                else
                {
                    ViewBag.ShowProcess = "false";
                }
            }

            var glideboxCommision = model.AddedGlideboxCommissionList.FirstOrDefault(x => x.CategoryID == Id);
            if (glideboxCommision != null)
            {
                model.AddedGlideboxCommissionList.Remove(glideboxCommision);
            }

            Session["MemberRateSameDayModel"] = model;
            return View("Sameday", model);

        }

        [RBAC(Permission = "CashingPayDocket")]
        public ActionResult PayNow(string BtnType)
        {
            return View("PayNow");
        }

        public ActionResult PayDone(string BtnType, string ScanType)
        {
            int DocketCheckSummary = 0;
            if (Session["DocketCheckSummary"] != null)
                DocketCheckSummary = (int)Session["DocketCheckSummary"];
            if (BtnType == "Pay")
            {
                Session["ScanType"] = "None";
                ViewBag.Msg = "Do you want to pay batch?";
                if (ScanType == "MassScan")
                {
                    Session["ScanType"] = ScanType;
                }
                return View();
            }
            if (BtnType == "Pay1")
            {
                string paid = Session["Paid"].ToString();
                if (paid == "false" && DocketCheckSummary != 0)
                {
                    Session["Print"] = "false";
                    //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                    int memberkey = (int)Session["MemberKey"];
                    string print = Session["Print"].ToString();
                    WebCashing_PayDockets_Result modelPay_result = new WebCashing_PayDockets_Result();
                    if (paid == "false" && print == "false")
                    {
                        try
                        {
                            DocketCheckDA dcd = new DocketCheckDA();
                            WebCashing_PayDockets modelPay = new WebCashing_PayDockets();
                            //if(Session["ScanType"] != null)
                            string ScanType1 = Session["ScanType"].ToString();
                            if (ScanType1 == "MassScan")
                            {
                                WebCashing_PayMassScanning_Result modelPayMass_result = dcd.PayMassDocket(DocketCheckSummary, uId, User.Identity.Name);
                                modelPay_result.LodgementReferenceNo = modelPayMass_result.LodgementReferenceNo;
                                modelPay_result.TotalPaid = modelPayMass_result.TotalPaid;
                                modelPay_result.TransactionKey = modelPayMass_result.TransactionKey;
                            }
                            else
                            {
                                if (paid == "false" && print == "false" && memberkey == 0)
                                {
                                    ViewBag.Msg = "An Error has occurred, Could not pay this Batch";
                                    return View("PayError");
                                }
                                else
                                {
                                    modelPay_result = dcd.PayDocket(DocketCheckSummary, memberkey, uId, User.Identity.Name);
                                    if (modelPay_result.TransactionKey != null)
                                    {
                                        dcd.SaveNoReceiptLogs((WebCashingResult)Session["ModelList"], (List<WebCashing_GetPendingBatchesHeaderByMemberKey>)Session["NoRecieptModel"], User.Identity.Name);
                                    }
                                }
                            }

                            modelPay.LodgementReferenceNo = modelPay_result.LodgementReferenceNo;
                            modelPay.TotalPaid = modelPay_result.TotalPaid;
                            modelPay.TransactionKey = modelPay_result.TransactionKey;
                            Session["LodgmentNo"] = modelPay.LodgementReferenceNo;
                            //Session["DocketCheckSummary"] = 0;
                            Session["MemberKey"] = 0;
                            Session["Paid"] = "True";
                            WebCashingResult model = new WebCashingResult();
                            model = (WebCashingResult)Session["ModelList"];

                            decimal GDiff = model.GrandTotal.Value - modelPay.TotalPaid.Value;

                            ViewBag.Msg = "Batch Paid. Please print the statement.GrandTotal: $" + model.GrandTotal.Value + "| Total Paid: $" + modelPay.TotalPaid + "| Differance: $" + GDiff;
                            return View("PayNow");

                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = ex.Message;
                            if (DocketCheckSummary != 0)
                            {
                                DocketCheckDA dcd = new DocketCheckDA();
                                dcd.CancelDocketCheck(DocketCheckSummary);
                                ViewBag.Msg = "An Error has occurred, Could not pay this Batch";
                                return View("PayError");
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Msg = "An Error has occurred, Could not pay this Batch";
                        return View("PayError");

                    }
                    return View();
                }
            }
            if (BtnType == "Print")
            {
                Session["Print"] = "True";
                return RedirectToAction("Print");
            }
            ViewBag.Msg = "Cannot Pay. Batch already Paid.";
            return View("PayError");

        }

        public ActionResult NewBatch(string BtnType, string ScanType)
        {
            if (BtnType == "Confirm")
            {
                string paid = Session["Paid"].ToString();

                WebCashingResult model = new WebCashingResult();
                model = (WebCashingResult)Session["ModelList"];

                if (paid == "false" && model.GrandTotal > 0)
                {
                    ViewBag.Msg = "Looks like this batch is not paid, Please try cancelling";
                    return View("PayError");
                }
                else
                {
                    Session["ScanType"] = "None";
                    ViewBag.Msg = "Do you want to Start New batch?";
                    if (ScanType == "MassScan")
                    {
                        Session["ScanType"] = ScanType;
                    }
                    return View();
                }
            }
            if (BtnType == "New")
            {
                int DocketCheckSummary = 0;
                var CheckSummary = Session["DocketCheckSummary"];
                if (CheckSummary != null)
                    DocketCheckSummary = (int)Session["DocketCheckSummary"];

                var pstatus = Session["Paid"];
                string paid = string.Empty;
                if (pstatus != null)
                    paid = Session["Paid"].ToString();
                if (paid == "false")
                {
                    DocketCheckDA dcd = new DocketCheckDA();
                    dcd.CancelDocketCheck(DocketCheckSummary);

                }
                string ScanType1 = Session["ScanType"].ToString();
                if (ScanType1 == "MassScan")
                {

                    return RedirectToAction("IndexMassScan");
                }
                else
                {
                    Session["ModelList"] = null;
                    Session["DocketCheckSummary"] = 0;
                    Session["MemberRateModel"] = null;
                    Session["MemberKey"] = 0;
                    Session["TopBC"] = null;
                    Session["BottomBC"] = null;
                    Session["HasChargeBack"] = "false";
                    Session["HasAuthorize"] = "false";
                    Session["MemberNotes"] = "false";
                    Session["HasChargeBackInsurance"] = "false";
                    Session["Notes"] = null;
                    Session["RewardPoints"] = null;
                    Session["TerminalAllocationLst"] = null;
                    Session["AuthorisedMemberKey"] = 0;
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Msg = "Error";
            return View("PayError");
        }
        public ActionResult NewBatchDirect(string ScanType)
        {
            int DocketCheckSummary = 0;
            var CheckSummary = Session["DocketCheckSummary"];
            if (CheckSummary != null)
                DocketCheckSummary = (int)Session["DocketCheckSummary"];

            var pstatus = Session["Paid"];
            string paid = string.Empty;
            if (pstatus != null)
                paid = Session["Paid"].ToString();
            if (paid == "false")
            {
                DocketCheckDA dcd = new DocketCheckDA();
                dcd.CancelDocketCheck(DocketCheckSummary);

            }
            string ScanType1 = string.Empty;
            if (Session["ScanType"] != null)
                ScanType1 = Session["ScanType"].ToString();
            if (ScanType1 == "MassScan")
            {
                return RedirectToAction("IndexMassScan");
            }
            else
            {
                Session["DocketCheckSummary"] = 0;
                Session["ModelList"] = null;
                Session["MemberRateModel"] = null;
                Session["MemberKey"] = 0;
                Session["TopBC"] = null;
                Session["BottomBC"] = null;
                Session["HasChargeBack"] = "false";
                Session["HasChargeBackInsurance"] = "false";
                Session["HasAuthorize"] = "false";
                Session["MemberNotes"] = "false";
                Session["Notes"] = null;
                Session["RewardPoints"] = null;
                Session["TerminalAllocationLst"] = null;
                Session["AuthorisedMemberKey"] = 0;

                return RedirectToAction("Index");
            }
        }

        public ActionResult Cancel(string ScanType, string BtnType)
        {
            if (BtnType == "Confirm")
            {
                Session["ScanType"] = "None";
                ViewBag.Msg = "Do you want to Cancel Batch?";
                if (ScanType == "MassScan")
                {
                    Session["ScanType"] = ScanType;
                }
                return View();
            }
            if (BtnType == "Cancel")
            {
                int DocketCheckSummary = 0;
                var CheckSummary = Session["DocketCheckSummary"];
                if (CheckSummary != null)
                    DocketCheckSummary = (int)Session["DocketCheckSummary"];

                var pstatus = Session["Paid"];
                string paid = string.Empty;
                if (pstatus != null)
                    paid = Session["Paid"].ToString();
                if (paid == "false")
                {
                    DocketCheckDA dcd = new DocketCheckDA();
                    dcd.CancelDocketCheck(DocketCheckSummary);

                }
                string ScanType1 = Session["ScanType"].ToString();
                if (ScanType1 == "MassScan")
                {
                    return RedirectToAction("IndexMassScan");
                }
                else
                {
                    Session["ModelList"] = null;
                    Session["DocketCheckSummary"] = 0;
                    Session["MemberRateModel"] = null;
                    Session["MemberKey"] = 0;
                    Session["TopBC"] = null;
                    Session["BottomBC"] = null;
                    Session["HasChargeBack"] = "false";
                    Session["HasChargeBackInsurance"] = "false";
                    Session["MemberNotes"] = "false";
                    Session["HasAuthorize"] = "false";

                    Session["Notes"] = null;
                    Session["RewardPoints"] = null;
                    Session["TerminalAllocationLst"] = null;
                    Session["AuthorisedMemberKey"] = 0;
                    ScannedDockets = new Dictionary<string, string>();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Msg = "Error";
            return View("PayError");
        }

        [RBAC(Permission = "CashingRemoveBatch")]
        public ActionResult RemoveBatch(string BatchID)
        {
            Guid uId = new Guid();
            int DocketCheckSummary = 0;
            WebCashingResult model = new WebCashingResult();
            var CheckSummary = Session["DocketCheckSummary"];
            if (CheckSummary != null)
                DocketCheckSummary = (int)Session["DocketCheckSummary"];

            var pstatus = Session["Paid"];
            string paid = string.Empty;
            if (pstatus != null)
                paid = Session["Paid"].ToString();
            if (paid == "false")
            {
                int mem_key = 0;
                if (Convert.ToInt32(Session["MemberKey"].ToString()) != 0)
                {
                    mem_key = (Convert.ToInt32(Session["MemberKey"]));
                }

                WebCashingResult model1 = new WebCashingResult();
                model1 = (WebCashingResult)Session["ModelList"];

                var doc = (from d in model1.ResultList
                           where d.BatchID == BatchID
                           select d).FirstOrDefault();

                DocketCheckDA dcd = new DocketCheckDA();
                if (doc.BatchNumber == null && doc.TerminalId == null)
                {
                    int do1 = (from d in doc.ResultListChild
                               select d.docket_key).FirstOrDefault();

                    model = dcd.DocketCheck("Remove", "Remove", "Remove", DocketCheckSummary, uId, null, "True", BatchID, 0.0, 0, true, 0, mem_key, false, false, do1);

                }
                else
                {
                    model = dcd.DocketCheck("Remove", "Remove", "Remove", DocketCheckSummary, uId, null, "True", BatchID, 0.0, 0, true, 0, mem_key, false);

                }

                Session["DocketCheckSummary"] = model.DocketCheckSummaryKey;
                Session["LodgmentNo"] = model.LodgementReferenceNo;
                Session["MemberKey"] = model.Member_key;
                Session["ModelList"] = model;
            }
            string ScanType1 = Session["ScanType"].ToString();
            if (ScanType1 == "MassScan")
            {
                return View("ListMassScan", model);
            }
            else
            {
                Session["DelModel"] = model;
                return RedirectToAction("Search", new { TopBC = string.Empty, BottomBC = string.Empty, ScanType = "singleScan", DelModel = model, Year = 0 });
            }
        }

        public ActionResult Remove(string BtnType, string ScanType, string BatchID)
        {
            if (BtnType == "Confirm")
            {
                Session["ScanType"] = "None";
                Session["BatchID"] = BatchID;
                ViewBag.Msg = "Do you want to remove this batch?";
                if (ScanType == "MassScan")
                {
                    Session["ScanType"] = ScanType;
                }
                return View();
            }
            if (BtnType == "Remove")
            {
                Guid uId = new Guid();
                int DocketCheckSummary = 0;
                WebCashingResult model = new WebCashingResult();
                var CheckSummary = Session["DocketCheckSummary"];
                if (CheckSummary != null)
                    DocketCheckSummary = (int)Session["DocketCheckSummary"];

                var pstatus = Session["Paid"];
                string paid = string.Empty;
                if (pstatus != null)
                    paid = Session["Paid"].ToString();
                if (paid == "false")
                {
                    string BatchID1 = Session["BatchID"].ToString();
                    string TopBC = Session["TopBC"].ToString();
                    string BottomBC = Session["BottomBC"].ToString();

                    DocketCheckDA dcd = new DocketCheckDA();
                    model = dcd.DocketCheck(TopBC, BottomBC, "Remove", DocketCheckSummary, uId, null, "True", BatchID1, 0.0, 0, true, 0);

                    Session["DocketCheckSummary"] = model.DocketCheckSummaryKey;
                    Session["LodgmentNo"] = model.LodgementReferenceNo;
                    Session["MemberKey"] = model.Member_key;
                    Session["ModelList"] = model;
                }
                string ScanType1 = Session["ScanType"].ToString();
                if (ScanType1 == "MassScan")
                {
                    return View("ListMassScan", model);
                }
                else
                {
                    return RedirectToAction("Search", new { TopBC = string.Empty, BottomBC = string.Empty, ScanType = "singlescan", DelModel = model, Year = 0 });
                    //  (string TopBC, string BottomBC, string ScanType, WebCashingResult DelModel)
                    //return View("List", model);
                }
            }
            ViewBag.Msg = "Error";
            return View("PayError");
        }

        [RBAC(Permission = "CashingRedeemVouchers")]
        public ActionResult RedeemVouchers()
        {
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];


            RewardPointDA cda = new RewardPointDA();
            RewardsPointVM drt = new RewardsPointVM();
            RewardPointsModel re = cda.Get(model1.Member_key.Value);
            drt.Total = re.TotalTrans.ToString();
            drt.TotalAvail = re.TotalPoints.Value.ToString();
            drt.TotalUsed = re.TotalPointsUsed.ToString();


            MemberDA mda = new MemberDA();
            var ckey = mda.GetMemberCompanyKey(model1.Member_key.Value);

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;

            UserDA uda = new UserDA();
            var officekey = uda.GetUserOfficeKey(uId);
            DrawDropDownConfig(ckey, officekey);
            RedeemVoucherVM model = new RedeemVoucherVM();
            VoucherDA vda = new VoucherDA();
            var hist = vda.GetAssignedVouchers_ByMember(model1.Member_key.Value);
            model.HistoryVouchers = hist.ToList();
            model.Name = model1.Name;
            model.MemberId = model1.memberid;
            model.Member_key = model1.Member_key.Value;

            if (re.TotalTrans != null)
                model.TotalPointEarned = re.TotalTrans;
            if (re.TotalPoints != null)
                model.TotalPointAvailable = re.TotalPoints.Value;
            model.TotalPointUsed = re.TotalPointsUsed;
            model.Override = false;

            return View(model);
        }

        [HttpPost]
        public ActionResult RedeemVouchers(FormCollection c, RedeemVoucherVM model, string Command)
        {

#if DEBUG
            model.TotalPointAvailable = 20000;

#endif
            VoucherDA vda = new VoucherDA();
            Boolean CanRedeem = false;
            VoucherTypeModel vmodel = new VoucherTypeModel();
            vmodel = vda.GetVoucherType(model.VoucherTypekey);
#if DEBUG

            vmodel.ConversionValue = 100;
#endif
            if (model.Override == true)
            {

                var lim = vmodel.OverrideLimit + model.TotalPointAvailable;
                if (lim >= vmodel.ConversionValue)
                {
                    CanRedeem = true;
                }
            }
            else if (model.TotalPointAvailable < vmodel.ConversionValue)
            {
                return Content("Error occured -Not enought points to redeem vouchers ");
            }
            else
            {
                CanRedeem = true;
            }

            if (CanRedeem == true)
            {
                string voucherNumber = string.Empty;
                if (Command == "Redeem Vouchers")
                {
                    voucherNumber = vda.RedeemVoucher(model.VoucherTypekey, model.Member_key, false, User.Identity.Name);
                }
                else
                {
                    voucherNumber = vda.RedeemVoucher(model.VoucherTypekey, model.Member_key, true, User.Identity.Name);
                }
                if (voucherNumber != string.Empty)
                {
                    Session["Voucher"] = voucherNumber;
                    byte[] output;
                    string extension, mimeType, encoding;
                    ReportingWebService.Warning[] warnings;
                    string[] streamIds;

                    string ReportName = "/LTEPAdminReports/VoucherGeneration";

                    IList<ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = model.Member_key.ToString() });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "VoucherNumber", Value = voucherNumber });

                    string format = "PDF";
                    PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                    return new FileContentResult(output, mimeType);
                }
            }
            else
            {
                return Content("Error occured -Not enought points to redeem vouchers ");
            }
            return View();

        }

        public ActionResult PrintVoucher(string id, int member)
        {
            Session["Voucher"] = id;
            byte[] output;
            string extension, mimeType, encoding;
            ReportingWebService.Warning[] warnings;
            string[] streamIds;

            string ReportName = "/LTEPAdminReports/VoucherGeneration";

            IList<ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();
            parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = member.ToString() });
            parameters.Add(new ReportingWebService.ParameterValue { Name = "VoucherNumber", Value = id });

            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            return new FileContentResult(output, mimeType);

        }

        private void DrawDropDownConfig(int company_key, int office_key)
        {
            VoucherDA cda = new VoucherDA();
            var VoucherType = cda.GetVoucherType_ByCompanyOffice(company_key, office_key).ToList();

            ViewBag.VoucherType = VoucherType.Select(item => new SelectListItem
            {
                Text = item.vouchername,
                Value = item.VoucherType_key.ToString()
            }).ToList();
        }

        [RBAC(Permission = "CashingUseVouchers")]
        public ActionResult UseVouchers()
        {
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            MemberDA mda = new MemberDA();
            var ckey = mda.GetMemberCompanyKey(model1.Member_key.Value);

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UseVoucherVM model = new UseVoucherVM();
            model.Member_key = model1.Member_key.Value;
            model.MemberId = model1.memberid;
            model.Name = model1.Name;
            return View(model);
        }


        [HttpPost]
        public ActionResult UseVouchers(FormCollection c)
        {
            try
            {
                if (c["VoucherNumber"] != string.Empty)
                {
                    var voucher = c["VoucherNumber"].ToString();
                    VoucherDA vda = new VoucherDA();
                    string res = vda.IsVoucherScannable(voucher);
                    if (res == "ok")
                    {
                        vda.UseVoucher(voucher, User.Identity.Name);
                        ViewBag.Msg = "Voucher updated";
                        return View("CommonConfirmedNormal");
                    }
                    else
                    {
                        ViewBag.Err = res;
                        ViewBag.Title = "Error";
                        ViewBag.Button = "Close";
                        return View("ComErrorAjax");
                    }


                }
                else
                {
                    ViewBag.Action = "UseVouchers";
                    ViewBag.Button = "Re-scan";
                    ViewBag.Err = "Voucher Number can not be null";
                    return View("CommonErrorAjax");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Button = "Re-scan";
                ViewBag.Action = "UseVouchers";
                ViewBag.Err = "An Error Occurred" + ex.Message;
                return View("CommonErrorAjax");
            }
        }

        public ActionResult UseVoucher(string voucher)
        {
            try
            {
                VoucherDA vda = new VoucherDA();
                string res = vda.IsVoucherScannable(voucher);
                if (res == "ok")
                {
                    vda.UseVoucher(voucher, User.Identity.Name);
                    ViewBag.Err = "Voucher updated";
                    return View("ComErrorAjax");
                }
                else
                {
                    ViewBag.Err = "Voucher can not be scanned : " + res;
                    return View("ComErrorAjax");
                }

            }
            catch (Exception ex)
            {
                ViewBag.Msg = ex.Message;
                return View("ComErrorAjax");
            }
        }

        [RBAC(Permission = "CashingAllMemberNotes")]
        public ActionResult AllMemberNotes()
        {
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];

            MemberNotesDA mda = new MemberNotesDA();
            var noteList = mda.GetAllMemberNotes(model1.Member_key.Value).ToList();
            AllMemberNoteVM model = new AllMemberNoteVM();
            model.MemberNotes = noteList.ToList();
            return View(model);
        }
        [RBAC(Permission = "CashingChangeNoteWarning")]
        public ActionResult ChangeNoteWarning(int Id, bool type)
        {

            MemberNotesDA mda = new MemberNotesDA();
            if (type == false)
            {
                mda.ChangeNoteWarningStatus(Id, type);
                ViewBag.Msg = "Note Warning acknolodged, you will not see this warning in your next scan";
            }
            else
            {
                mda.ChangeNoteWarningStatus(Id, type);
                ViewBag.Msg = "Note Warning acknolodged, you will see this note as warning in your next scan";
            }
            return View("CommonConfirmedNormal");

        }

        [RBAC(Permission = "CashingChangeNoteWarning")]
        public ActionResult ChangeNoteWarningPopUp(int Id)
        {
            bool type = false;
            MemberNotesDA mda = new MemberNotesDA();
            mda.ChangeNoteWarningStatus(Id, type);
            WebCashingResult model = new WebCashingResult();
            model = (WebCashingResult)Session["ModelList"];
            return RedirectToAction("IndexScan", model);
        }


        public JsonResult GetRewardPoints(int Id)
        {
            RewardPointDA cda = new RewardPointDA();
            RewardsPointVM drt = new RewardsPointVM();
            RewardPointsModel re = cda.Get(Id);
            drt.Total = re.TotalTrans.ToString();
            drt.TotalAvail = re.TotalPoints.GetValueOrDefault(0).ToString();
            drt.TotalUsed = re.TotalPointsUsed.ToString();

            return Json(drt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCommissionRate(int Id)
        {
            MemberDA mda = new MemberDA();
            var comk = mda.GetMemberCompanyKey(Id);
            CommissionRateDA cda = new CommissionRateDA();
            VehicleTypeDA vda0 = new VehicleTypeDA();
            int vkey0 = vda0.GetVehicleTypeKey(comk);
            var defaultRateModel = cda.GetMemberCommissionRate(vkey0, Id, comk);

            DefaultRateVMModel drt = new DefaultRateVMModel();
            drt.Amex = " " + String.Format("{0:N4}%", defaultRateModel.Amex.GetValueOrDefault(0.0m));
            drt.Visa = " " + String.Format("{0:N4}%", defaultRateModel.Visa.GetValueOrDefault(0.0m));
            drt.Master = " " + String.Format("{0:N4}%", defaultRateModel.Master.GetValueOrDefault(0.0m));
            drt.Diners = " " + String.Format("{0:N4}%", defaultRateModel.Diners.GetValueOrDefault(0.0m));
            drt.UnionPay = " " + String.Format("{0:N4}%", defaultRateModel.UnionPay.GetValueOrDefault(0.0m));
            drt.Zip = " " + String.Format("{0:N4}%", defaultRateModel.Zip.GetValueOrDefault(0.0m));
            drt.Alipay = " " + String.Format("{0:N4}%", defaultRateModel.Alipay.GetValueOrDefault(0.0m));

            if (defaultRateModel.DebitIsFixed)
            {
                drt.Debit = String.Format("${0:N4}", defaultRateModel.Debit.GetValueOrDefault(0.0m));
            }
            else
            {
                drt.Debit = String.Format("{0:N4}%", defaultRateModel.DebitPercentage.GetValueOrDefault(0.0m));
            }

            if (defaultRateModel.QantasIsFixed)
            {
                drt.Qantas = String.Format("${0:N4}", defaultRateModel.Qantas.GetValueOrDefault(0.0m));
            }
            else
            {
                drt.Qantas = String.Format("{0:N4}%", defaultRateModel.QantasPercentage.GetValueOrDefault(0.0m));
            }


            return Json(drt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTerminals(int Id)
        {
            TerminalAllocationDA cda = new TerminalAllocationDA();
            List<TerminalAllocationModel> drt1 = new List<TerminalAllocationModel>();
            drt1 = cda.GetTerminalAllocationList(Id);
            List<string> drt = new List<string>();
            foreach (var t in drt1)
            {
                string dr = "";
                if (drt1.Count == 1)
                {
                    dr = "<span style='font-weight:700'>" + t.EftTerminalId + "</span>" + " - " + t.VehicleModel.VehicleId + " - " + t.VehicleModel.RegisteredState;
                }
                else
                {
                    dr = "<span style='font-weight:700'>" + t.EftTerminalId + "</span>" + " - " + t.VehicleModel.VehicleId + " - " + t.VehicleModel.RegisteredState + " | ";
                }

                drt.Add(dr);
            }

            return Json(drt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLastCashing(int Id)
        {

            TransactionDA trda = new TransactionDA();
            LastCashingVM drt = new LastCashingVM();
            var trans = trda.GetLastTransactionByMemberkey(Id);
            drt.LastCashedAmt = trans.TotalAmount.ToString();
            if (trans.TransactionDate != null)
            {
                drt.LastCashedDateTime = trans.TransactionDate.Value.ToLongDateString() + ":" + trans.TransactionDate.Value.ToLongTimeString();
            }
            return Json(drt, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetNotes(int Id)
        {
            IEnumerable<MemberNotesModel> drt = null;

            MemberNotesDA mdna = new MemberNotesDA();
            drt = mdna.GetMemberNotes(Id).Take(15).ToList();

            return Json(drt, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AuthorisedMembersConfirm(string Id)
        {
            if (Session["HasAuthorize"].ToString() == "true")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (!this.HasPermission("CashingAuthorisedMembers"))
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

            WebCashingResult model = new WebCashingResult();
            model = (WebCashingResult)Session["ModelList"];
            if (model != null)
            {

                MemberDA mda = new MemberDA();
                int mkey = mda.GetMemberKey(model.memberid);
                Session["MemberKey"] = mkey;
                AuthorisedMemberDA mdaa = new AuthorisedMemberDA();
                List<AuthorisedMemberModel> modelA = mdaa.GetAllByMemberKey(mkey);
                if (modelA.Count > 0)
                {
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Session["HasAuthorize"] = "true";
                    //return RedirectToAction("AcknowledgeAuthoriseConfirm");
                    return Json("error", JsonRequestBehavior.AllowGet);
                }

            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }


        [RBAC(Permission = "CashingAuthorisedMembers")]
        public ActionResult AuthorisedMembers(int? Id)
        {
            int mkey = 0;
            if (Id == null)
            {
                mkey = Convert.ToInt32(Session["MemberKey"]);
            }
            else
            {
                mkey = Id.Value;
            }

            AuthorisedMemberDA mda = new AuthorisedMemberDA();
            List<AuthorisedMemberModel> model = mda.GetAllByMemberKey(mkey);
            return View(model);

        }
        [RBAC(Permission = "CashingAuthorisedMembers")]
        public ActionResult ConfirmAuthorisedMember(string Id)
        {
            int mkey = Convert.ToInt32(Id);
            Session["AuthorisedMemberKey"] = mkey;
            ViewBag.Button = "Switch";
            ViewBag.Action = "SelectAuthorisedMember";
            ViewBag.Err = "Are you sure you want to switch to authorised member?";
            return View("CommonNonAjax");
        }
        [RBAC(Permission = "CashingAuthorisedMembers")]
        public ActionResult SelectAuthorisedMember(FormCollection c)
        {
            try
            {

                MemberDA mda = new MemberDA();
                MemberModel membermodel = new MemberModel();
                int mkey = (int)Session["AuthorisedMemberKey"];
                Session["MemberKey"] = mkey;
                membermodel = mda.GetMemeber(mkey);

                DocketCheckDA dcd = new DocketCheckDA();
                WebCashingResult model = new WebCashingResult();
                int DocketCheckSummary = 0;
                DocketCheckSummary = (int)Session["DocketCheckSummary"];

                Guid uId = (Guid)Session["UserId"];

                int officekey = (int)Session["officekey"];
                double commissionRate = (double)Session["commissionRate"];

                WebCashingResult model1 = new WebCashingResult();
                model1 = (WebCashingResult)Session["ModelList"];

                string TopBC = "0";
                string BottomBC = "0";
                if (DocketCheckSummary != 0)
                {
                    TopBC = Session["TopBC"].ToString();
                    BottomBC = Session["BottomBC"].ToString();
                    model = dcd.DocketCheck(TopBC, BottomBC, User.Identity.Name, DocketCheckSummary, uId, "Scan", "False", string.Empty, commissionRate, officekey, true, 0, mkey, true);

                }
                else
                {
                    string mid = mda.GetMemberId(mkey);
                    return RedirectToAction("SelectMemberAccount", new { Id = mid, IsAuthorised = "true" });
                }

                Session["HasAuthorize"] = "true";

                OperatorDA oda1 = new OperatorDA();
                OperatorModel opt = new OperatorModel();
                opt = oda1.Get(membermodel.Member_key);
                if (opt.AllowCash == false)
                {
                    model.BatchStatus = "Bank Customer";
                }
                else
                {
                    model.BatchStatus = "Cash Customer";
                }


                //IEnumerable<MemberNotesModel> mnm = null;             

                //if (Session["Notes"] == null)
                //{
                //    MemberNotesDA mdna = new MemberNotesDA();
                //    mnm = mdna.GetMemberNotes(model.Member_key.Value).Take(15).ToList();
                //}
                //else
                //{
                //    mnm = (IEnumerable<MemberNotesModel>)Session["Notes"];
                //}
                //model.MemberNotes = mnm;
                //Session["Notes"] = mnm;

                Session["ModelList"] = model;
                return RedirectToAction("IndexScan");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return View("Err");
            }
        }

        public ActionResult InsuranceConfirm()
        {
            if (Session["HasAuthorize"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            if (Session["MemberNotes"].ToString() == "false")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }

            if (!this.HasPermission("CashingAddInsuranceChargeBack"))
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            //if (Session["TopBC"] == null && Session["BottomBC"] == null)
            //{
            //    return Json("error", JsonRequestBehavior.AllowGet);
            //}
            //WebCashingResult model = new WebCashingResult();
            //model = (WebCashingResult)Session["ModelList"];
            //if (model != null)
            //{
            //    if (model.ResultList != null)
            //    {
            //        if (model.ResultList.Count > 0)
            //        {
            //#if DEBUG
            //                        return Json("success", JsonRequestBehavior.AllowGet);

            //#endif
            //if (((int)Session["ValidScanCount"]) == 1)
            //{
            if (Session["HasChargeBackInsurance"].ToString() == "true")
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                if ((int)Session["MemberKey"] != 0)
                {

                    int mkey = (int)Session["MemberKey"];
                    TransactionDA dda = new TransactionDA();
                    bool HasChargeBackInsurance = false;

                    HasChargeBackInsurance = dda.HasGetInsuranceChargeBack(mkey);
                    if (HasChargeBackInsurance == true)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                }

                //MemberDA mda = new MemberDA();
                //int mkey = mda.GetMemberKey(model.memberid);

                //TransactionDA dda = new TransactionDA();
                //bool HasChargeBack = false;

                //HasChargeBack = dda.HasGetInsuranceChargeBack(mkey);

                //if (HasChargeBack == true)
                //{
                //    return Json("success", JsonRequestBehavior.AllowGet);
                //}
            }
            //            }
            //        }
            //    }
            // }
            return Json("error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsuranceChargeBack()
        {

            WebCashingResult MainModel = new WebCashingResult();
            DocketCheckDA dcd = new DocketCheckDA();
            TransactionDA tda = new TransactionDA();
            Guid uId = (Guid)Session["UserId"];

            int officekey = (int)Session["officekey"];
            double commissionRate = (double)Session["commissionRate"];
            int DocketCheckSummary = 0;
            WebCashingResult model1 = new WebCashingResult();
            model1 = (WebCashingResult)Session["ModelList"];
            MainModel = model1;
            bool GenerateAccordin = false;
            int count = 0;
            int mkey = model1.Member_key.Value;
            List<WebCashing_GetTerminalInsuranceModel> GetChargeBackModel = null;

            GetChargeBackModel = tda.GetInsuranceChargeBack(mkey);

            if (GetChargeBackModel.Count > 0)
            {
                int Recordcount = GetChargeBackModel.Count - 1;

                foreach (var mod in GetChargeBackModel)
                {
                    if (count == Recordcount)
                    {
                        GenerateAccordin = true;
                    }
                    if (Session["DocketCheckSummary"] != null)
                    {
                        DocketCheckSummary = (int)Session["DocketCheckSummary"];
                    }
                    try
                    {
                        MainModel = dcd.ChargeBackDocketCheck(mod.Description, mod.docket_key.Value, User.Identity.Name, model1.DocketCheckSummaryKey, mkey, GenerateAccordin);

                        count = count + 1;
                        Session["DocketCheckSummary"] = MainModel.DocketCheckSummaryKey;
                        Session["HasChargeBackInsurance"] = "true";
                    }
                    catch
                    {

                    }
                }
                Session["MemberKey"] = model1.Member_key.Value;
            }
            Session["ModelList"] = MainModel;
            return RedirectToAction("IndexScan");
            //  return View("IndexScan", MainModel);
        }

        public ActionResult ApplyLevyCharge(string LevyCharge)
        {
            CommissionRate_MemberRateModelSameList model = new CommissionRate_MemberRateModelSameList();
            model = (CommissionRate_MemberRateModelSameList)Session["MemberRateSameDayModel"];
            decimal GrandFaceValue = 0.0M;
            double levyChargeValue = 0;
            double totalLevy = 0; double levyByquantity = 0;
            foreach (CommissionRate_AddedMemberRateSameModel m in model.AddedMemberRateSameModelList)
            {
                m.LevyCharge = double.TryParse(LevyCharge, out levyChargeValue) ? levyChargeValue : 0;
                levyByquantity = (m.LevyCharge * (m.Quantity.HasValue ? m.Quantity.Value : 0));
                totalLevy += levyByquantity;
                if (m.Total.HasValue)
                {
                    m.Total += m.Total.HasValue
                            ? levyByquantity
                            : (double?)null;
                }
            };

            GrandFaceValue = Convert.ToDecimal(model.AddedMemberRateSameTotalModelList.FaceValue) + Convert.ToDecimal(totalLevy);
            model.AddedMemberRateSameTotalModelList.FaceValue = Convert.ToDouble(GrandFaceValue);
            Session["MemberRateSameDayModel"] = model;
            return View("Sameday", model);
        }
    }
}

