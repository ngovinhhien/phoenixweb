﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using Phoenix_Service;
using PhoenixObjects.Base;
using PhoenixObjects.ImportFee;
using PhoenixWeb.Models;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    [RBAC(Permission = "ImportInterchangeAndSchemeFees")]
    public class ImportController : PhoenixWebController
    {
        public ActionResult FeesImport()
        {
            return View("InterchangeAndSchemeFee/Index");
        }

        [HttpPost]
        public ActionResult GetRecentFeesImport(BasePagingModel searchModel)
        {
            var result = new ImportFeeDA().GetFeesImportList(searchModel);

            return Json(new { isSuccess = true, Data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FeesImport(HttpPostedFileBase file)
        {
            string fileContent = string.Empty;

            try
            {
                List<string> listString;
                var lstFees = new List<InterchangeSchemeFeeModel>();
                var orgFileName = file.FileName;
                var fileSize = file.ContentLength;

                using (StreamReader reader = new StreamReader(file.InputStream))
                {
                    fileContent = reader.ReadToEnd();
                    listString = fileContent.Split(new[] { "\r\n", "\r" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                if (listString != null)
                {
                    if (!IsHeaderCorrect(listString[0]))
                    {
                        return Json(
                            new
                            {
                                isSuccess = true,
                                isHeaderError = true,
                                isFileError = true,
                            }, JsonRequestBehavior.DenyGet);
                    }

                    var csvIsError = false;
                    listString.RemoveAt(0); // remove header
                    listString.ForEach(line =>
                    {
                        var lineArray = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                        if (lineArray.Length != 9)
                        {
                            LogInfo(line);
                            csvIsError = true;
                            return;
                        }

                        lstFees.Add(new InterchangeSchemeFeeModel
                        {
                            MID = lineArray[0].Trim(),
                            Month = lineArray[1].Trim(),
                            Year = lineArray[2].Trim(),
                            CardType = lineArray[3].Trim(),
                            FeeType = lineArray[4].Trim(),
                            Description = lineArray[5].Trim(),
                            Txns_No = lineArray[6].Trim(),
                            Rate = lineArray[7].Trim(),
                            FeeValue = lineArray[8].Trim(),
                        });
                    });

                    if (csvIsError)
                    {
                        return Json(new { isSuccess = false }, JsonRequestBehavior.DenyGet);
                    }

                    var validatedData = ValidateDatas(lstFees);

                    var guidKey = Guid.NewGuid().ToString().Replace("-", string.Empty);

                    if (validatedData.Any(r => !string.IsNullOrWhiteSpace(r.ErrorDescription)))
                    {
                        Session[guidKey] = validatedData;
                        return Json(new { isSuccess = true, isHeaderError = false, isFileError = true, key = guidKey }, JsonRequestBehavior.DenyGet);
                    }

                    var submitData = new Dictionary<string, object>
                    {
                        { "fileName", orgFileName },
                        { "fileSize", fileSize },
                        { "data", lstFees },
                    };
                    Session[guidKey] = submitData;

                    var reviewData = validatedData.GroupBy(f => new { f.MID, f.TradingName })
                        .Select(g => new
                        {
                            g.Key.TradingName,
                            g.Key.MID,
                            TotalFee = g.Sum(p => Convert.ToDecimal(p.FeeValue)).ToString("#,##0.00"),
                        });

                    return Json(
                        new
                        {
                            isSuccess = true,
                            isFileError = false,
                            key = guidKey,
                            data = reviewData,
                        },
                        JsonRequestBehavior.DenyGet);
                }

                return Json(new { isSuccess = false }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception e)
            {
                LogError(e);
                LogInfo(fileContent);
                return Json(new { isSuccess = false }, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult DownloadErrorFile(string key)
        {
            if (Session[key] == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var arrData = (Session[key] as List<InterchangeSchemeFeeModel>).Select(r =>
                r.MID + "," + r.Month + "," + r.Year + "," + r.CardType + "," + r.FeeType + "," + r.Description + "," +
                r.Txns_No + "," + r.Rate + "," + r.FeeValue + ",\"" + r.ErrorDescription + "\"").ToArray();
            var header = "MID,Month,Year,Visa_Master_Debit,Type,Description,txns_no,rate,fee_value,Error_Description";
            var csv = header + Environment.NewLine + string.Join(Environment.NewLine, arrData);

            return File(new System.Text.UTF8Encoding().GetBytes(csv), "application/vnd.ms-excel", "InterchangeSchemeFeeError.csv");
        }

        [HttpGet]
        public ActionResult DownloadImportedFile(string key)
        {
            var s3BucketName = Caching.GetSystemConfigurationByKey("S3FeeImportBucket");
            var fileContent = LGHelper.S3.GetFileFromS3AndResponseToString(key, s3BucketName);

            if (string.IsNullOrWhiteSpace(fileContent))
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return File(new System.Text.UTF8Encoding().GetBytes(fileContent), "application/vnd.ms-excel", key.Substring(0, key.LastIndexOf(".")));
        }

        [HttpPost]
        public ActionResult SubmitFees(string key)
        {
            try
            {
                LogInfo("Start submit Fees");
                if (Session[key] != null)
                {

                    var submitData = Session[key] as Dictionary<string, object>;
                    var orgFileName = submitData["fileName"].ToString();
                    var orgFileSize = int.Parse(submitData["fileSize"].ToString());
                    var data = submitData["data"] as List<InterchangeSchemeFeeModel>;
                    var arrData = (data ?? new List<InterchangeSchemeFeeModel>()).Select(r =>
                        r.MID + "," + r.Month + "," + r.Year + "," + r.CardType + "," + r.FeeType + "," +
                        r.Description + "," +
                        r.Txns_No + "," + r.Rate + "," + r.FeeValue).ToArray();
                    var header = Caching.GetSystemConfigurationByKey("InterchangeSchemeFeeHeaderTemplate");
                    var csv = header + Environment.NewLine + string.Join(Environment.NewLine, arrData);
                    LogInfo("Upload file to S3 in SubmitFees");
                    var uploadedFileName = UploadToS3(csv, orgFileName);
                    LogInfo($"End upload file to S3 in SubmitFees -- {uploadedFileName}");
                    var createSuccess = new ImportFeeDA().AddFee(data, orgFileName, orgFileSize, uploadedFileName, User.Identity.Name);

                    return Json(new
                    {
                        isSuccess = createSuccess
                    });
                }

                return Json(new
                {
                    isSuccess = false
                });
            }
            catch (Exception e)
            {
                LogError(e);
                return Json(new
                {
                    isSuccess = false
                });
            }
            finally
            {
                LogInfo("End submit Fees");
            }
        }

        private string UploadToS3(string fileContent, string orgFileName)
        {
            var s3BucketName = Caching.GetSystemConfigurationByKey("S3FeeImportBucket");

            return LGHelper.S3.UploadFile(fileContent, orgFileName, s3BucketName);
        }

        private bool IsHeaderCorrect(string header)
        {
            return header.ToLower()
                .Equals(Caching.GetSystemConfigurationByKey("InterchangeSchemeFeeHeaderTemplate").ToLower());
        }

        private List<InterchangeSchemeFeeModel> ValidateDatas(List<InterchangeSchemeFeeModel> data)
        {
            data = ValidateMID(data)
                .ValidateFees(ValidateMonth)
                .ValidateFees(ValidateYear)
                .ValidateFees(ValidateMonthYear)
                .ValidateFees(ValidateCardType)
                .ValidateFees(ValidateFeeType)
                .ValidateFees(ValidateDescription)
                .ValidateFees(ValidateTXNNo)
                .ValidateFees(ValidateRate)
                .ValidateFees(ValidateFeeValue)
                .ValidateFees(ValidateDuplicateCharge);

            return data;
        }

        private List<InterchangeSchemeFeeModel> ValidateMID(List<InterchangeSchemeFeeModel> data)
        {
            LogInfo("Start validate MID");
            data.ForEach(r =>
            {
                if (string.IsNullOrWhiteSpace(r.MID))
                {
                    r.ErrorDescriptionList.Add("MID is required");
                }

                if (!new Regex("^[A-Za-z0-9]{1,10}$").IsMatch(r.MID))
                {
                    r.ErrorDescriptionList.Add("MID is in invalid format");
                }
            });

            var lstMemberId = data.GroupBy(r => r.MID)
                .Select(g => g.Key)
                .ToList();

            List<InterchangeSchemeFeeMIDValidateResultModel> sqlReturn;
            using (var context = new TaxiEpayEntities())
            {
                sqlReturn = context.Members.Where(m => lstMemberId.Contains(m.MemberId))
                                            .Select(r =>
                                            new InterchangeSchemeFeeMIDValidateResultModel
                                            {
                                                Company_key = r.Company_key,
                                                MemberKey = r.Member_key,
                                                MemberId = r.MemberId,
                                                TradingName = r.TradingName,
                                            }).ToList();
            }

            if (sqlReturn != null)
            {
                data.ForEach(d =>
                {
                    var objResult = sqlReturn.FirstOrDefault(r => r.MemberId == d.MID);
                    if (objResult == null)
                    {
                        d.ErrorDescriptionList.Add("MID does not exist");
                        return;
                    }

                    if (objResult.Company_key != 29)
                    {
                        d.ErrorDescriptionList.Add("Cannot import fees for this business type");
                    }

                    d.TradingName = objResult.TradingName;
                    d.MKey = objResult.MemberKey;
                });
            }
            LogInfo("End validate MID");
            return data;
        }

        private List<InterchangeSchemeFeeModel> ValidateMonth(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate Month");

            var grpData = dataObject.GroupBy(d => d.Month);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Month is required");
                }

                if (!new Regex("^(0[1-9]|1[0-2])$").IsMatch(g.Key))
                {
                    errMess.Add("Month is in invalid format");
                }

                dataObject.Where(d => d.Month == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate Month");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateYear(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate Year");

            var grpData = dataObject.GroupBy(d => d.Year);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Year is required");
                }

                if (!new Regex(@"^\d{4}$").IsMatch(g.Key))
                {
                    errMess.Add("Year is in invalid format");
                }

                dataObject.Where(d => d.Month == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate Year");
            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateMonthYear(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate Month/Year");

            var grpData = dataObject.GroupBy(d => new { d.Month, d.Year });

            grpData.ToList().ForEach(g =>
            {
                int.TryParse(g.Key.Year, out var dataYear);
                int.TryParse(g.Key.Month, out var dataMonth);

                if (dataYear <= 1 || dataMonth <= 1)
                {
                    return;
                }

                var currentDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var dataDate = new DateTime(dataYear, dataMonth, 1);

                LogInfo($"CurrentDate = {currentDate}, DataDate = {dataDate}");

                if (currentDate < dataDate || currentDate > dataDate.AddMonths(6))
                {
                    dataObject.Where(d => d.Month == g.Key.Month && d.Year == g.Key.Year)
                        .ToList()
                        .ForEach(d => d.ErrorDescriptionList.Add("Month and Year cannot be later than current date or earlier than 6 months ago"));
                }
            });

            LogInfo("End validate Month/Year");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateCardType(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate card type");

            var cardType = new[] { "vs", "mc", "debit" };
            var grpData = dataObject.GroupBy(d => d.CardType);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Visa_Master_Debit is required");
                }

                if (!new Regex("^[A-Za-z]{2,5}$").IsMatch(g.Key))
                {
                    errMess.Add("Visa_Master_Debit is in invalid format");
                }

                if (!cardType.Contains(g.Key.ToLower()))
                {
                    errMess.Add("This value is not allowed. Please use VS for VISA, MC for Mastercard or Debit for Debit.");
                }


                dataObject.Where(d => d.CardType == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate card type");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateFeeType(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate fee type");

            var feeType = new[] { "interchange", "scheme" };
            var grpData = dataObject.GroupBy(d => d.FeeType);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Type is required");
                }

                if (!new Regex("^[A-Za-z]{6,15}$").IsMatch(g.Key))
                {
                    errMess.Add("Type is in invalid format");
                }

                if (!feeType.Contains(g.Key.ToLower()))
                {
                    errMess.Add("This value is not allowed. Please use Interchange or Scheme.");
                }

                dataObject.Where(d => d.FeeType == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate fee type");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateDescription(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate description");

            var grpData = dataObject.GroupBy(d => d.Description);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Description is required");
                }

                if (!new Regex(@"^[A-Za-z0-9\s]{1,50}$").IsMatch(g.Key))
                {
                    errMess.Add("Description is in invalid format");
                }

                dataObject.Where(d => d.Description == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate description");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateTXNNo(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate Txns_No");

            var grpData = dataObject.GroupBy(d => new { d.Txns_No, d.FeeType });

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (g.Key.FeeType.ToLower() == "interchange" && string.IsNullOrWhiteSpace(g.Key.Txns_No))
                {
                    errMess.Add("Txns_No is required");
                }

                if (!new Regex("^[0-9]{0,7}$").IsMatch(g.Key.Txns_No))
                {
                    errMess.Add("Txns_No is in invalid format");
                }

                dataObject.Where(d => d.Txns_No == g.Key.Txns_No && d.FeeType == g.Key.FeeType)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate Txns_No");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateRate(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate rate");

            var grpData = dataObject.GroupBy(d => d.Rate);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("Rate is required");
                }

                if (!new Regex(@"^\d{1}[.]\d{0,4}$").IsMatch(g.Key))
                {
                    errMess.Add("Rate is in invalid format");
                }

                decimal.TryParse(g.Key, out var dRate);

                if (dRate > 9.9999M)
                {
                    errMess.Add("Cannot import values greater than 9.9999");
                }

                dataObject.Where(d => d.Rate == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate rate");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateFeeValue(List<InterchangeSchemeFeeModel> dataObject)
        {
            LogInfo("Start validate fee_value");

            var grpData = dataObject.GroupBy(d => d.FeeValue);

            grpData.ToList().ForEach(g =>
            {
                var errMess = new List<string>();

                if (string.IsNullOrWhiteSpace(g.Key))
                {
                    errMess.Add("fee_value is required");
                }

                if (!new Regex(@"^(?!1000\.\d|0+(?:\.0+)?$)\d{1,4}(?:\.\d{1,2})?$").IsMatch(g.Key))
                {
                    errMess.Add("fee_value is in invalid format");
                }

                decimal.TryParse(g.Key, out var feeValue);

                if (feeValue > 1000.0M)
                {
                    errMess.Add("Cannot import values greater than $1000.00");
                }

                dataObject.Where(d => d.FeeValue == g.Key)
                    .ToList()
                    .ForEach(d => d.ErrorDescriptionList.AddRange(errMess));
            });

            LogInfo("End validate fee_value");

            return dataObject;
        }

        private List<InterchangeSchemeFeeModel> ValidateDuplicateCharge(List<InterchangeSchemeFeeModel> data)
        {
            var lstDataDistinct = data.GroupBy(
                    r => new
                    {
                        r.MID,
                        r.Month,
                        r.Year,
                        FeeType = r.FeeType.ToLower(),
                        CardType = r.CardType.ToLower(),
                        Description = r.Description.ToLower(),
                    })
                .Select(g => new InterchangeSchemeFeeModel
                {
                    NumOfDuplicated = g.Count(),
                    MID = g.Key.MID,
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    FeeType = g.Key.FeeType,
                    CardType = g.Key.CardType,
                    Description = g.Key.Description,
                })
                .ToList();

            var lstNotDuplicated = lstDataDistinct.Where(r => r.NumOfDuplicated == 1)
                .Select(r => r);

            var listMID = lstNotDuplicated.Select(r => r.MID).Distinct().ToList();

            List<FeeImportedDetail> lstDataDuplicatedOnDb = new List<FeeImportedDetail>();
            try
            {
                using (var context = new TaxiEpayEntities())
                {
                    var detailList = context.FeeImportedDetails
                        .Where(r => listMID.Contains(r.MemberID))
                        .Select(f =>
                            f.MemberID + "|" + f.Month + "|" + f.Year + "|" + f.CardType + "|" + f.FeeType + "|" + f.Description);

                    lstDataDuplicatedOnDb = lstDataDuplicatedOnDb.Where(l =>
                            detailList.Contains((l.MemberID + "|" + l.Month + "|" + l.Year + "|" + l.CardType + "|" + l.FeeType +
                                       "|" + l.Description).ToLower()))
                        .Select(r => r)
                        .ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            data.ForEach(d =>
            {
                if (!string.IsNullOrWhiteSpace(d.ErrorDescription))
                {
                    return;
                }

                if (lstDataDistinct.Where(o => o.NumOfDuplicated > 1)
                    .Any(r => r.MID == d.MID
                              && r.Month == d.Month
                              && r.Year == d.Year
                              && string.Compare(r.FeeType, d.FeeType, StringComparison.InvariantCultureIgnoreCase) == 0
                              && string.Compare(r.CardType, d.CardType, StringComparison.InvariantCultureIgnoreCase) == 0))
                {
                    d.ErrorDescriptionList.Add("Duplicated charges");
                }
                else if (lstDataDuplicatedOnDb.Any(r => r.MemberID == d.MID
                                                        && r.Month == Convert.ToInt32(d.Month)
                                                        && r.Year == Convert.ToInt32(d.Year)
                                                        && string.Compare(r.FeeType, d.FeeType, StringComparison.InvariantCultureIgnoreCase) == 0
                                                        && string.Compare(r.CardType, d.CardType, StringComparison.InvariantCultureIgnoreCase) == 0))
                {
                    d.ErrorDescriptionList.Add("Duplicated charges");
                }
            });

            return data;
        }
    }

    public static class ExtensionMethod
    {
        public static T ValidateFees<T>(this T source, Func<T, T> validateFunc)
        {
            return validateFunc(source);
        }
    }
}
