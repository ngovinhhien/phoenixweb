﻿using Phoenix_BusLog.Data;
using PhoenixWeb.Common;
using PhoenixWeb.Models.ViewModel;
using System;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    public class UserResetPasswordHistoryController : PhoenixWebController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHistory(ResetPasswordHistoryModel searchObject)
        {
            try
            {
                UserResetPasswordHistoryDA historyDA = new UserResetPasswordHistoryDA();
                var jsonData = historyDA.GetUserResetPasswordHistory(searchObject.FromDate, searchObject.ToDate, searchObject.SearchText, searchObject.PageSize, searchObject.CurrentPage);

                return new CustomJsonResult() { Data = jsonData };
            }
            catch (Exception e)
            {
                LogError(e.Message);

                return new CustomJsonResult() { Data = null };
            }
        }
    }
}