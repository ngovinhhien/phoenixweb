﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Phoenix_BusLog.Data;
using PhoenixObjects.UserAuthentication;
using PhoenixObjects.Salesforce;
using PhoenixObjects.Administrator;
using PhoenixWeb.ActionFilters;
using System.Security.Principal;
using PhoenixWeb.Models.Contants;
using PhoenixWeb.Models.ViewModel;
using Phoenix_BusLog;
using PhoenixObjects.Enums;
using PhoenixWeb.Common;
using PhoenixObjects.Enums;

namespace PhoenixWeb.Controllers
{

    public class AdministrationController : PhoenixWebController
    {
        public ActionResult ForceReloadRBAUsers()
        {
            RBAC_ExtendedMethods.ForceReloadRBAUsers();
            var data = new
            {
                Status = "Ok"
            };
            return new JsonResult()
            {
                Data = data,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //
        // GET: /Administration/

        public ActionResult Index()
        {
            //string username = Session["Username"].ToString();
            //var nameT =User.Identity.Name;
            //var r = User.IsInRole("Administrator");
            UserDA uda = new UserDA();
            // int HasRole = uda.IsUserInRoles(nameT, "Administrator");

            return RedirectToAction("Menu");
        }
        [RBAC(Permission = "AdminChangeSalesforceCredential")]
        public ActionResult SalesforceCred()
        {
            SalesforceDA sda = new SalesforceDA();
            SalesforceDetailModel cred = new SalesforceDetailModel();
            cred = sda.GetCredentials();
            return View(cred);
        }

        [HttpPost]
        [RBAC(Permission = "AdminChangeSalesforceCredential")]
        public ActionResult SalesforceCred(SalesforceDetailModel Model)
        {
            SalesforceDA sda = new SalesforceDA();
            sda.AddCredentials(Model);
            return View(Model);
        }

        [RBAC(Permission = "AdminAddFreshDeskCredential")]
        public ActionResult FreshDeskCred()
        {
            return View();
        }
        [RBAC(Permission = "AdminAddFreshDeskCredential")]
        public ActionResult SaveFreshDeskCred(string UserName, string PhoeUserName, string Email)
        {
            FreshDeskCredDA fda = new FreshDeskCredDA();
            Guid Id = Guid.Parse(PhoeUserName);
            bool done = fda.UpdateUserFreshDeskCred(Id, UserName, Email);
            if (done)
            {
                ViewBag.Msg = "Changes Saved!";
            }
            else
            {
                ViewBag.Err = "Error occured!";
            }
            return View("FreshDeskCred");
        }

        public ActionResult UserProfile(Guid id)
        {
            UserDA uda = new UserDA();
            UserProfileModel model = uda.GetUsers(id);
            return View(model);
        }
        public ActionResult Menu()
        {
            return View();
        }
        [RBAC(Permission = "AdminCreateNewRoles")]
        public ActionResult CreateRoles()
        {
            RolesDA rda = new RolesDA();
            List<RoleModel> model = rda.GetRoles();
            return View(model);
        }
        [HttpPost]
        [RBAC(Permission = "AdminCreateNewRoles")]
        public ActionResult CreateRoles(FormCollection c)
        {
            string RoleName = c["RoleName"].ToString();
            Roles.CreateRole(RoleName);
            return RedirectToAction("Menu");
        }
        [RBAC(Permission = "AdminAddUserRoles")]
        public ActionResult AddRoles()
        {
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "AdminAddUserRoles")]
        public ActionResult AddRoles(FormCollection c)
        {
            Guid userid = Guid.Parse(c["UserName"].ToString());
            Guid roleid = Guid.Parse(c["Role"].ToString());

            UserDA uda = new UserDA();
            try
            {
                ViewBag.Message = "Role Saved";
                uda.AddUserToRole(userid, roleid);
            }
            catch
            {
                ViewBag.Message = "Error";
            }

            return View();
        }

        [RBAC(Permission = "AdminCreateUser")]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "AdminCreateUser")]
        public ActionResult Register(FormCollection c)
        {
            string username = c["UserName"].ToString();
            string email = c["Email"].ToString();
            string password = c["Password"].ToString();
            int officekey = Convert.ToInt32(c["Office"].ToString());
            Guid RoleKey = Guid.Parse(c["Role"].ToString());

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(username, password, email, passwordQuestion: null, passwordAnswer: null, isApproved: true, providerUserKey: null, status: out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    Guid uId = (Guid)membershipProvider.GetUser(username, false).ProviderUserKey;
                    UserDA uda = new UserDA();
                    if (officekey != 0)
                    {
                        uda.AddUserOffice(uId, officekey);
                    }
                    if (RoleKey != Guid.Empty)
                    {
                        uda.AddUserToRole(uId, RoleKey);
                    }

                    ViewBag.Success = "User Added Successfully";
                    return View("Success");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                    return View();
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Success");
        }
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        [RBAC(Permission = "AdminAddPermission")]
        public ActionResult AddPermission()
        {
            RolesPermissionVM model = new RolesPermissionVM();

            RolesDA rda = new RolesDA();
            model.RoleList = rda.GetRoles();

            //  PermissionsDA pda = new PermissionsDA();

            //  model.PermissionList = pda.GetAllPermissionListByUser(User).OrderBy(n => n.PermissionName).ToList();

            return View(model);
        }


        public ActionResult AddPermissionToRole()
        {
            PermissionModel model = new PermissionModel();
            Guid RoleId = (Guid)Session["PerRoleId"];
            PermissionsDA pda = new PermissionsDA();
            var lst = pda.GetAllPermissionNotRole(RoleId).OrderBy(n => n.PermissionName).ToList();
            if (lst.Count > 0)
            {
                ViewBag.PermissionList = lst.Select(item => new SelectListItem
                {
                    Text = item.PermissionName,
                    Value = item.PermissionId.ToString()
                }).ToList();

            }
            else
            {
                ViewBag.PermissionList = lst.Select(item => new SelectListItem
                {
                    Text = "None",
                    Value = "0"
                }).ToList();
            }
            model.PermissionId = lst.First().PermissionId;
            model.PermissionName = lst.First().PermissionName;
            return View(model);
        }
        [HttpPost]
        public ActionResult AddPermissionToRole(PermissionModel model)
        {
            PermissionsDA pda = new PermissionsDA();
            Guid RoleId = (Guid)Session["PerRoleId"];
            pda.AddPermissionToRole(RoleId, model.PermissionId);
            ViewBag.BtnName = "Ok";
            ViewBag.ContAction = "AddPermission";
            // ViewBag.PraValus = m.MemberKey;
            ViewBag.Msg = "Permission added";
            return View("CommonMsg1");
        }
        //private void DrawDropDownConfig()
        //{
        //    RolesDA cda = new RolesDA();
        //    var rolesList = cda.GetRoles().ToList();

        //    ViewBag.RolesList = rolesList.Select(item => new SelectListItem
        //    {
        //        Text = item.RoleName,
        //        Value = item.RoleId.ToString()
        //    }).ToList();

        //}

        public ActionResult ViewAllUsers()
        {
            List<UserProfileModel> model = new List<UserProfileModel>();
            UserDA da = new UserDA();

            model = da.GetUsers();

            return View(model);
            //PhoenixObjects.UserAuthentication.PhoenixUser user

        }

        public ActionResult ChangePassword(Guid Id)
        {
            UserProfileModel model;
            UserDA da = new UserDA();
            model = da.GetUsers(Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePasswordSaved(FormCollection c, UserProfileModel m)
        {
            UserProfileModel model;
            UserDA da = new UserDA();
            model = da.GetUsers(m.UserId);
            try
            {
                //string OldPassword = c["OldPassword"].ToString();
                string NewPassword = c["NewPassword"].ToString();
                string ConfirmPassword = c["ConfirmPassword"].ToString();
                if (ConfirmPassword != NewPassword)
                {
                    ViewBag.Error = "New Passwords don't match";
                    return View();
                }

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                membershipProvider.UnlockUser(model.UserName);
                string resetedPassword = membershipProvider.ResetPassword(model.UserName, string.Empty);
                bool IsChanged = membershipProvider.ChangePassword(model.UserName, resetedPassword, NewPassword);

                if (IsChanged)
                {
                    //log reset password history after change password successful
                    da.SaveResetPasswordHistory(model.UserName, ResetPasswordSourceConstants.SourceNameOfChangePasswordInViewAllUsersScreen, User.Identity.Name);
                }

                //if (!IsChanged)
                //{
                //    return RedirectToAction("ChangePasswordMain");
                //}
                //return RedirectToAction("Index", "AdminHome");

                return RedirectToAction("ViewAllUsers");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }
        public ActionResult ResetPasswordRequests()
        {
            using (UserDA userDA = new UserDA())
            {
                IEnumerable<UserResetPasswordRequestModel> userResetPasswordRequestModels = userDA.GetResetPasswordRequests();
                //dirty       
                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                List<UserResendTokenViewModel> userResendTokenViewModel = new List<UserResendTokenViewModel>();
                foreach (UserResetPasswordRequestModel userResetPasswordRequestModel in userResetPasswordRequestModels)
                {
                    UserProfileModel userProfileModel = userDA.GetUsers(userResetPasswordRequestModel.UserId);
                    userResendTokenViewModel.Add(new UserResendTokenViewModel()
                    {
                        CreatedBy = userResetPasswordRequestModel.CreatedBy,
                        CreatedOn = userResetPasswordRequestModel.CreatedOn,
                        Email = membershipProvider.GetUser(userProfileModel.UserName, false).Email,
                        ExpiresBy = userResetPasswordRequestModel.ExpiresBy,
                        IsActive = userResetPasswordRequestModel.IsActive,
                        IsDeleted = userResetPasswordRequestModel.IsDeleted,
                        ResetPasswordToken = userResetPasswordRequestModel.ResetPasswordToken,
                        LastEmailUsed = userResetPasswordRequestModel.LastEmailUsed,
                        UpdatedBy = userResetPasswordRequestModel.UpdatedBy,
                        UpdatedOn = userResetPasswordRequestModel.UpdatedOn,
                        UserId = userResetPasswordRequestModel.UserId,
                        Username = userProfileModel.UserName,
                        
                        UserResetPasswordRequestId = userResetPasswordRequestModel.UserResetPasswordRequestId
                    });
                }
                return View(userResendTokenViewModel);
            };
        }

        public ActionResult ResendResetPasswordToken(Guid Id)
        {
            using (UserDA userDA = new UserDA())
            {
                UserResetPasswordRequestModel userResetPasswordRequestModel = userDA.GetResetPasswordRequest(Id);
                UserProfileModel userProfileModel = userDA.GetUsers(Id);

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                UserResendTokenViewModel userResendTokenViewModel = new UserResendTokenViewModel()
                {
                    CreatedBy = userResetPasswordRequestModel.CreatedBy,
                    CreatedOn = userResetPasswordRequestModel.CreatedOn,
                    Email = membershipProvider.GetUser(userProfileModel.UserName, false).Email,
                    ExpiresBy = userResetPasswordRequestModel.ExpiresBy,
                    IsActive = userResetPasswordRequestModel.IsActive,
                    IsDeleted = userResetPasswordRequestModel.IsDeleted,
                    ResetPasswordToken = userResetPasswordRequestModel.ResetPasswordToken,
                    UpdatedBy = userResetPasswordRequestModel.UpdatedBy,
                    UpdatedOn = userResetPasswordRequestModel.UpdatedOn,
                    UserId = userResetPasswordRequestModel.UserId,
                    Username = userProfileModel.UserName,
                    UserResetPasswordRequestId = userResetPasswordRequestModel.UserResetPasswordRequestId
                };
                return View(userResendTokenViewModel);
            };
        }


        //Common Code?
        protected string GetUser_IP()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Request.UserHostAddress;
            }
            return VisitorsIPAddr;
        }
        
        [HttpPost]
        public ActionResult ResendResetPasswordToken(FormCollection c, UserResendTokenViewModel m)
        {
            if (m.IsExpiredToken)
            {
                using (UserDA userDA = new UserDA())
                {
                    UserResetPasswordRequestModel userResetPasswordRequestModel = userDA.SaveResetPasswordRequest(m.UserId, m.Email, User.Identity.Name, GetUser_IP());
                    m.CreatedBy = userResetPasswordRequestModel.CreatedBy;
                    m.CreatedOn = userResetPasswordRequestModel.CreatedOn;
                    m.ExpiresBy = userResetPasswordRequestModel.ExpiresBy;
                    m.IsActive = userResetPasswordRequestModel.IsActive;
                    m.IsDeleted = userResetPasswordRequestModel.IsDeleted;
                    m.ResetPasswordToken = userResetPasswordRequestModel.ResetPasswordToken;
                    m.UpdatedBy = userResetPasswordRequestModel.UpdatedBy;
                    m.UpdatedOn = userResetPasswordRequestModel.UpdatedOn;
                    m.UserId = userResetPasswordRequestModel.UserId;
                    m.UserResetPasswordRequestId = userResetPasswordRequestModel.UserResetPasswordRequestId;
                }
            }
            SendVerificationEmail(m.UserId, m, m.Email);
            ViewBag.Message = "Successfully resend reset password token";
            return RedirectToAction("ResetPasswordRequests", "Administration");
        }

        public ActionResult DeactivateResetPasswordToken(Guid Id, Guid token)
        {
            using (UserDA userDA = new UserDA())
            {
                userDA.DeactivateResetPasswordToken(Id, token);
            }
            return RedirectToAction("ResetPasswordRequests", "Administration");
        }

        //common code
        private void SendVerificationEmail(Guid userid, UserResetPasswordRequestModel model, string email)
        {
            UserDA userda = new UserDA();
            var u = userda.GetUsers(userid);

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];           
            string subject = "Reset Password Resend";
            string body = "<b>Hi " + u.UserName + ",</b><br/><br/>";
            body += "<b>You have requested a password reset.</b><br/>";

            body += "<b>Please find your verification link below. Once Logged in you can change your password by going to the my profile section on your portal</b><br/><br/>";
            body += "<b>UserName: " + u.UserName + "<b><br/><br/>";
            body += $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebDNS.ToString())}/Account/ResetPassword?id=" + model.ResetPasswordToken + " <br/><br/>";

            body += "<b>Regards, <br/>PhoenixWeb Support Team<b>";

            try
            {
                List<String> _to = new List<string>();
                _to.Add(email);

                if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true, "it@livegroup.com.au"))
                {
                    ModelState.AddModelError("", "Reset successful, mail Sent.");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured while sending email.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
            }
        }

        public ActionResult MFAEnabler()
        {
            using (MultiFactorAuthenticatorDA multiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
            {
                return View(multiFactorAuthenticatorDA.GetUserMFAConfigurationList());
            };
        }
        [HttpPost]
        public ActionResult Toggle(string userId, bool enable)
        {
            using (MultiFactorAuthenticatorDA multiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
            {
                multiFactorAuthenticatorDA.ToggleUserMFA(new Guid(userId), enable, User.Identity.Name);
                return Json(new { Success = true, message = "Successfully record updated." }, JsonRequestBehavior.AllowGet);
            };
        }

        public ActionResult Configuration()
        {
            try
            {
                SystemConfigurationDA configDA = new SystemConfigurationDA();
                var systemID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SystemID"]);
                var configurations = configDA.GetSystemConfigurations(systemID);

                var result = configurations
                    .Select(n => new SystemConfigurationVM {
                        Key = n.ConfigurationKey,
                        Value = n.ConfigurationValue.Length <= 4 ? (n.ConfigurationValue + "**********") : (n.ConfigurationValue.Substring(0, 4) + "*********"),
                        Description = n.Description
                    })
                    .ToList();
                return View(result);
            }
            catch (Exception e)
            {
                LogError(e.Message);
                return View();
            }
        }

        public ActionResult ReloadConfiguration()
        {
            Caching.ReloadMemoryCache();
            return RedirectToAction("Configuration");
        }
    }
}
