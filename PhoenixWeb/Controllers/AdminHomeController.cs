﻿using Phoenix_BusLog.Data;
using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PhoenixWeb.ActionFilters;
using System.Text;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using PhoenixWeb.Common;
namespace PhoenixWeb.Controllers
{
    [Authorize]   
     public class AdminHomeController : PhoenixWebController
    {
        //
        // GET: /Administrator/AdminHome/
       
        public ActionResult Index()
        {
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            try
            {
                              
              //  Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                DateTime LastPWChangedDate = membershipProvider.GetUser(User.Identity.Name, false).LastPasswordChangedDate;
                double t = DateTime.Now.Subtract(LastPWChangedDate).Days;
                LoginModel model = new LoginModel();
                model.UserName = User.Identity.Name;               
                if (t > 90)
                {
                return RedirectToAction("ChangePasswordMain", "Account", model);
                }
                ViewBag.Expiring = 0;
                if (t < 90 && t>76)
                {
                    var tt = 90 - t;
                    ViewBag.Expiring = Math.Round(tt,2);
                }

                Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                UserDA userda = new UserDA();
                var u = userda.GetUsers(uId);
                if (u.Dealerkey != null)
                {
                    return RedirectToAction("Index", "Dealer");
                }
                //UserDA rda = new UserDA();
                //UserProfileModel user = rda.GetUsers(uId);
                //if (user.OfficeType_Key == 4 || user.OfficeType_Key == 3)
                //{
                //    ViewBag.CanMassSacn = true;
                //}
                //else
                //{
                //    if (User.IsInRole("Administrator"))
                //    {
                //        ViewBag.CanMassSacn = true;
                //    }
                //    else
                //    {
                //        ViewBag.CanMassSacn = false;
                //    }
                //}
            }
            catch
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        //
        // GET: /Administrator/AdminHome/Details/5

        //
        // GET: /Administrator/AdminHome/Create

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Permission()
        {
            Session.Abandon();
            return View();
        }


        public ActionResult Error()
        {
            return View();
        }
        //
        // POST: /Administrator/AdminHome/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administrator/AdminHome/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Administrator/AdminHome/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Administrator/AdminHome/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Administrator/AdminHome/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        public ActionResult FreshDeskLogin()
        {

            FreshDeskCredDA fda = new FreshDeskCredDA();
            FreshDeskCredModel model = new FreshDeskCredModel();
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            model = fda.GetUserFreshDeskCred(uId);

            string key = "4fe7df9302aa983ad987ad1a1cbcfb0a";
            const string pathTemplate = "https://livegroup.freshdesk.com/login/sso?name={0}&email={1}&timestamp={2}&hash={3}";

            var username = model.Username;// "Vnguyen";
            var email = model.Email;// "vu.nguyen@livegroup.com.au";

            string timems = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString();
            var hash = GetHash(key, username, email, timems);
            var path = String.Format(pathTemplate, Server.UrlEncode(username), Server.UrlEncode(email), timems, hash);

            return Redirect(path);
        }
        private static string GetHash(string secret, string name, string email, string timems)
        {
            string input = name + email + timems;
            var keybytes = Encoding.Default.GetBytes(secret);
            var inputBytes = Encoding.Default.GetBytes(input);

            var crypto = new HMACMD5(keybytes);
            byte[] hash = crypto.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                string hexValue = b.ToString("X").ToLower(); // Lowercase for compatibility on case-sensitive systems
                sb.Append((hexValue.Length == 1 ? "0" : "") + hexValue);
            }
            return sb.ToString();
        }
    }
}
