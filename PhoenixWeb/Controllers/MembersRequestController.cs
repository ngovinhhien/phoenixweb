﻿using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using PhoenixObjects.Common;
using PhoenixObjects.Member;
using PhoenixObjects.MyWeb;
using PhoenixWeb.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class MembersRequestController : PhoenixWebController
    {
        //
        // GET: /MembersRequest/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DetailChange()
        {
            MyWebChangeRequest_DetailsDA mDa = new MyWebChangeRequest_DetailsDA();
            List<DetailChangeRequestModel> model = new List<DetailChangeRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name);

            return View(model);
        }

        public ActionResult BankChange()
        {
            MyWebChangeRequest_BankDA mDa = new MyWebChangeRequest_BankDA();
            List<BankChangeRequestModel> model = new List<BankChangeRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name);

            return View(model);
        }

        public ActionResult PaperRollRequest()
        {
            MyWebChangeRequest_PaperRollOrderDA mDa = new MyWebChangeRequest_PaperRollOrderDA();
            List<PaperRollRequestModel> model = new List<PaperRollRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name);

            return View(model);
        }

        public ActionResult ViewDetailRequest(int RequestID)
        {
            MyWebChangeRequest_DetailsDA mDa = new MyWebChangeRequest_DetailsDA();
            DetailChangeRequestModel model = new DetailChangeRequestModel();
            model = mDa.GetRequest(RequestID);
            return View(model);
        }
        public ActionResult ViewBankRequest(int RequestID)
        {
            MyWebChangeRequest_BankDA mDa = new MyWebChangeRequest_BankDA();
            BankChangeRequestModel model = new BankChangeRequestModel();
            model = mDa.GetRequest(RequestID);
            return View(model);
        }
        public ActionResult ViewPaperRollRequest(int RequestID)
        {
            MyWebChangeRequest_PaperRollOrderDA mDa = new MyWebChangeRequest_PaperRollOrderDA();
            PaperRollRequestModel model = new PaperRollRequestModel();
            model = mDa.GetRequest(RequestID);
            return View(model);
        }

        public ActionResult SupportDocumentImage(int RequestID, int TypeID)
        {
            MyWebChangeRequest_DetailsDA mDa = new MyWebChangeRequest_DetailsDA();
            DetailChgReqImageModel imgModel = new DetailChgReqImageModel();

            imgModel = mDa.GetDocument(RequestID, TypeID);
            //return new FileContentResult(output, "image/jpeg");
            if (imgModel.Extension == ".jpg")
            {
                //Session["JpegByte"] = imgModel.SupportDoc;
               // return View("ImageDocJpeg");
                return new FileContentResult(imgModel.SupportDoc, "image/jpeg");
            }
            if (imgModel.Extension == ".pdf")
            {
                return new FileContentResult(imgModel.SupportDoc, "application/pdf");
            }
            return new FileContentResult(imgModel.SupportDoc, "application/pdf");
        }
        public ActionResult SupportDocumentImage_Bank(int RequestID, int TypeID)
        {
            MyWebChangeRequest_BankDA mDa = new MyWebChangeRequest_BankDA();
            BankChgReqImageModel imgModel = new BankChgReqImageModel();

            imgModel = mDa.GetDocument(RequestID, TypeID);
            //return new FileContentResult(output, "image/jpeg");
            if (imgModel.Extension == ".jpg")
            {
                //Session["JpegByte"] = imgModel.SupportDoc;
                // return View("ImageDocJpeg");
                return new FileContentResult(imgModel.SupportDoc, "image/jpeg");
            }
            if (imgModel.Extension == ".pdf")
            {
                return new FileContentResult(imgModel.SupportDoc, "application/pdf");
            }
            return new FileContentResult(imgModel.SupportDoc, "application/pdf");
        }

     
        public ActionResult ConfirmDetailRequest(DetailChangeRequestModel modelData)
        {
            int id = modelData.Member_key.Value;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    MemberAddressDA mda = new MemberAddressDA();
                    AddressListModel m = new AddressListModel();
                   
                    MemberDA mda1 = new MemberDA();
                    if (modelData.New_PostCode != null)
                    {
                        m.PostCode = modelData.New_PostCode.ToString();
                    }
                    if (modelData.New_State != null)
                    {
                        m.State = modelData.New_State;
                    }
                    if (modelData.New_Suburb != null)
                    {
                        m.Suburb = modelData.New_Suburb;
                    }
                    m.Country = "Australia";

                    m.StreetName = modelData.New_Street;
                    m.StreetNumber = modelData.New_StreetNo;
                    m.AddressType = "Primary";
                    mda.Add(m, id);                  

                    MyWebChangeRequest_DetailsDA myDa = new MyWebChangeRequest_DetailsDA();
                    modelData.CompleteBy = User.Identity.Name;
                    modelData.CompletedDate = DateTime.Now;
                    modelData.Status = TEnum.MyWebChangeRequestStatus.APPROVED.ToString();
                    myDa.UpdateStatusComplete(modelData);
                   
                    tran.Complete();
                }
                catch
                {
                    ViewBag.Msg = "Details Not Saved, An Error Occurred!";
                    return View("ErrorConfirmed");
                }
            }


            MemberDA mda_send = new MemberDA();
            MemberModel mem_send = new MemberModel();
            mem_send = mda_send.GetMemeber(id);

            //send an email to the customer

            string subject = "Change of contact details(TESTING-Ritesh)";
            string body = "<b>Success!</b><br/><br/>";
            body += "<b>We've made the requested changes to your contact details.</b><br/>";

            body += "<b>Your new contact details are:</b><br/><br/>";
            body += "Email Address: " + mem_send.Email + "<br/>";
            body += "Fax: " + mem_send.Fax + "<br/>";
            body += "Mobile: " + mem_send.Mobile + "<br/>";
            body += "Phone: " + mem_send.Telephone + "<br/>";
            body += "Street: " + mem_send.Address + "<br/>";
            body += "Suburb: " + mem_send.City + "<br/>";
            body += "PostCode: " + mem_send.Postcode + "<br/>";
            body += "State: " + mem_send.State + "<br/><br/>";

            if (mem_send.CompanyKey == 1)
            {
                body += "If this is incorrect please email support@livetaxiepay.com.au or call us on 1300 883 703" + "<br/><br/>";
            }
            if (mem_send.CompanyKey == 18)
            {
                body += "If this is incorrect please email support@liveeftpos.com.au or call us on 1300 780 788" + "<br/><br/>";
            }
            body += "<b>Happy transacting!<b>" + "<br/><br/>";

            try
            {
                List<String> _to = new List<string>();
                String _from = string.Empty;
                _to.Add(mem_send.Email);
              
                if (mem_send.CompanyKey == 1)
                {
                    _from = "support@livetaxiepay.com.au";
                }
                if (mem_send.CompanyKey == 18)
                {
                    _from = "support@liveeftpos.com.au";
                }

                if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true, _from))
                {
                    ModelState.AddModelError("", "Reset successful, mail Sent.");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured while sending email.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
            }

            ViewBag.Msg = "Reset successful, mail Sent.";
            return View("DetailRequestConfirmed");
        }


        public ActionResult ConfirmBankRequest(BankChangeRequestModel modelData)
        {
            int id = modelData.Member_key.Value;
            string accname = string.Empty;
            string bsb = string.Empty;
            string accnumber = string.Empty;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    AccountDA ada = new AccountDA();
                    BankAccountModel accModel = new BankAccountModel();

                    accModel.Member_Key = modelData.Member_key.Value;
                    accModel.UserName = User.Identity.Name;
                   

                    if (modelData.New_AccountName != null)
                    {
                        accModel.AccountName = modelData.New_AccountName.ToString();
                        accname = accModel.AccountName;
                    }
                    if (modelData.New_AccountNumber != null)
                    {
                        accModel.AccountNumber = modelData.New_AccountNumber;
                        accnumber = accModel.AccountNumber;
                    }
                    if (modelData.New_BSB != null)
                    {
                        accModel.BSB = modelData.New_BSB;
                        bsb = accModel.BSB;
                    }
                  
                    if (modelData.Account_key != null)
                    {
                        accModel.AccountKey = modelData.Account_key.Value;
                        accModel.Weight = Convert.ToInt32(modelData.Old_Weight);
                        ada.Edit(accModel);
                    }
                    else
                    {
                        accModel.Weight = 100;
                        ada.Add(accModel);
                    }                 
                
                
                    MyWebChangeRequest_BankDA myDa = new MyWebChangeRequest_BankDA();
                    modelData.CompleteBy = User.Identity.Name;
                    modelData.CompletedDate = DateTime.Now;
                    modelData.Status = TEnum.MyWebChangeRequestStatus.APPROVED.ToString();
                    myDa.UpdateStatusComplete(modelData);
                   
                    tran.Complete();
                }
                catch
                {
                    ViewBag.Msg = "Details Not Saved, An Error Occurred!";
                    return View("ErrorConfirmed");
                }
            }

            MemberDA mda = new MemberDA();
            MemberModel mem = new MemberModel();
            mem = mda.GetMemeber(id);

           //send an email to the customer
       
            string subject = "Change in bank details(TESTING-Ritesh)";
            string body = "<b>Success!</b><br/><br/>";
            body += "<b>Your bank account details have been modified as per your request.</b><br/>";

            body += "<b>Your new bank details are:</b><br/><br/>";
            body += "Account Name: " + accname + "<br/>";
            body += "BSB: " + bsb + "<br/>";
            body += "Account number: " + accnumber + "<br/>";

            if (mem.CompanyKey == 1)
            {
                body += "If this is incorrect please email support@livetaxiepay.com.au or call us on 1300 883 703" + "<br/><br/>";
            }
            if (mem.CompanyKey == 18)
            {
                body += "If this is incorrect please email support@liveeftpos.com.au or call us on 1300 780 788" + "<br/><br/>";
            }

            body += "<b>Happy transacting!<b>" + "<br/><br/>";

            try
            {
                List<String> _to = new List<string>();
                String _from = string.Empty;
                _to.Add(mem.Email);
            
                if (mem.CompanyKey == 1)
                {
                    _from = "support@livetaxiepay.com.au";
                }
                if (mem.CompanyKey == 18)
                {
                    _from = "support@liveeftpos.com.au";
                }

                if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true, _from))
                {
                    ModelState.AddModelError("", "Reset successful, mail Sent.");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured while sending email.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
            }

            ViewBag.Msg = "Reset successful, mail Sent.";
            return View("BankRequestConfirmed");
        }

        public ActionResult ConfirmPaperRollRequest(PaperRollRequestModel modelData, FormCollection c)
        {
            int id = modelData.Member_key.Value;
            string trackingno = string.Empty;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    trackingno = c["trackingno"].ToString();
                    MyWebChangeRequest_PaperRollOrderDA myDa = new MyWebChangeRequest_PaperRollOrderDA();
                    modelData.CompleteBy = User.Identity.Name;
                    modelData.CompletedDate = DateTime.Now;
                    modelData.Status = TEnum.MyWebChangeRequestStatus.DISPATCHED.ToString();
                    modelData.TrackingNo = trackingno;
                    myDa.UpdateStatusComplete(modelData);

                    tran.Complete();
                }
                catch
                {
                    ViewBag.Msg = "Details Not Saved, An Error Occurred!";
                    return View("ErrorConfirmed");
                }
            }

            MemberDA mda = new MemberDA();
            MemberModel mem = new MemberModel();
            mem = mda.GetMemeber(id);

           
            //send an email to the customer

            string subject = "Paper roll Request(Testing -Ritesh- DONT SEND)";
            string body = "<b>Thanks for your order!</b><br/><br/>";
            body += "<b>Your paper rolls have been dispatched and will be with you in 1-2 days.</b><br/><br/>";

            body += "<b>Your paper roll order details:</b><br/><br/>";
            body += "No. of Paper rolls: " + modelData.QuantityRequested + "<br/>";
            body += "Mailing Address: " + modelData.Street + "<br/><br/>";
            body += "<b>Tracking Number: </b>" + trackingno + "<br/><br/><br/>";   

            if (mem.CompanyKey == 1)
            {
                body += "If this is incorrect please call us immediately on 1300 883 703" + "<br/><br/>";
                body += "<b>Live taxiepay<b>" + "<br/><br/>";
            }
            if (mem.CompanyKey == 18)
            {
                body += "If this is incorrect please call us immediately on 1300 780 788" + "<br/><br/>";
                body += "<b>Live eftpos<b>" + "<br/><br/>";
            }                    

            try
            {
                List<String> _to = new List<string>();
                String _from = string.Empty;
                _to.Add(mem.Email);
             
                if (mem.CompanyKey == 1)
                {
                    _from = "support@livetaxiepay.com.au";
                }
                if (mem.CompanyKey == 18)
                {
                    _from = "support@liveeftpos.com.au";
                }

                if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true, _from))
                {
                    ModelState.AddModelError("", "Reset successful, mail Sent.");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured while sending email.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
            }

            ViewBag.Msg = "Reset successful, mail Sent.";
            return View("PaperRollRequestConfirmed");
        }

        public ActionResult DetailsNotification()
        {
            MyWebChangeRequest_DetailsDA mDa = new MyWebChangeRequest_DetailsDA();
            List<DetailChangeRequestModel> model = new List<DetailChangeRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name).ToList();

            return View(model);
        }
        public ActionResult BankNotification()
        {
            MyWebChangeRequest_BankDA mDa = new MyWebChangeRequest_BankDA();
            List<BankChangeRequestModel> model = new List<BankChangeRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name).ToList();

            return View(model);
        }
        public ActionResult PaperRollNotification()
        {
            MyWebChangeRequest_PaperRollOrderDA mDa = new MyWebChangeRequest_PaperRollOrderDA();
            List<PaperRollRequestModel> model = new List<PaperRollRequestModel>();
            model = mDa.GetAllPending(User.Identity.Name).ToList();

            return View(model);
        }

    }
}
