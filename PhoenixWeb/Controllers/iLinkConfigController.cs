﻿using System;
using System.Linq;
using System.Web.Mvc;
using PhoenixWeb.Models.ViewModel;
using Phoenix_BusLog.Data;
using log4net;
using Newtonsoft.Json;
using System.IO;
using ExcelDataReader;
using System.Data;
using System.Collections.Generic;
using Phoenix_BusLog.TransactionHostLog.BusLog;
using Phoenix_BusLog.TransactionHostLog.Model;
using PhoenixObjects.WebAppLog;
using PhoenixObjects.Common;
using Phoenix_Service;
using System.Data.SqlClient;
using Phoenix_BusLog;
using PhoenixWeb.Models.ViewModel.iLink;
using PhoenixWeb.Common;
using PhoenixObjects.Financial;
using Phoenix_BusLog.Excel;
using PhoenixObjects.Enums;
using System.Web;
using PhoenixWeb.Models.iLinkMannualyImport;
using Crypto.AES;
using System.Globalization;
using System.Text.RegularExpressions;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon;
using Amazon.S3.Model;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class iLinkConfigController : Controller
    {
        public ActionResult iLinkCredentialConfig()
        {
            return View();
        }

        public ActionResult GetiLinkCredentials()
        {
            iLinkConfigDA ilcDA = new iLinkConfigDA();
            var jsonData = ilcDA.GetiLinkCredentials();
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateiLinkCredentials(iLinkCredentialVM model)
        {
            iLinkConfigDA ilcDA = new iLinkConfigDA();
            model.IsActive = true;
            model.UpdatedBy = User.Identity.Name;
            model.CreatedBy = User.Identity.Name;
            var result = ilcDA.UpdateiLinkCredentials(model.toiLinkCredential());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult iLinkAutomationConfig()
        {
            return View();
        }

        public ActionResult GetiLinkAutomationConfig()
        {
            iLinkConfigDA ilcDA = new iLinkConfigDA();
            var jsonData = ilcDA.GetDownloadAutomationConfig();
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateiLinkAutomationConfig(iLinkAutomationConfigVM model)
        {
            iLinkConfigDA ilcDA = new iLinkConfigDA();
            model.iLinkAllowedFileExtensions = model.iLinkAllowedFileExtensions.Replace(" ", "");
            model.iLinkAutomaticErrorReceivers = model.iLinkAutomaticErrorReceivers.Replace(" ", "");
            model.iLinkAutomaticResultReceivers = model.iLinkAutomaticResultReceivers.Replace(" ", "");
            var result = ilcDA.UpdateDownloadAutomationConfig(model.toDictionary(), User.Identity.Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Log()
        {
            using (var logDA = new LogDA())
            {
                var listLevel = logDA.GetLevel();
                ViewBag.LevelList = listLevel.Select(item => new SelectListItem
                {
                    Value = item,
                    Text = item
                });
            }

            return View();
        }

        [HttpPost]
        public ActionResult SearchiLinkLog(GridModel<SearchLogModel, List<iLink_SearchLog_Result>> model)
        {
            iLinkConfigDA ilcDA = new iLinkConfigDA();
            ilcDA.SearchiLinkLog(model);
            return new CustomJsonResult
            {
                Data = model
            };
        }

        public ActionResult FileManagement()
        {
            iLinkConfigDA configDA = new iLinkConfigDA();

            List<iLinkFileStatu> statuses = configDA.GetiLinkFileStatuses();
            ViewBag.Statuses = statuses.Select(n => new SelectListItem()
            {
                Text = n.StatusName,
                Value = n.StatusCode
            }).ToList();

            List<iLinkFileType> fileTypes = configDA.GetiLinkFileTypes();
            ViewBag.FileTypes = fileTypes.Select(n => new SelectListItem()
            {
                Text = n.Name,
                Value = n.ID.ToString()
            }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult GetiLinkFiles(GridModel<ILinkFileManagementSearchVM, List<iLinkFileVM>> model)
        {
            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                var fromDate = model.Request.StartDate.ToString("yyyy-MM-dd");
                var endDate = model.Request.EndDate.ToString("yyyy-MM-dd");

                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("@RowIDFrom", model.PageIndexFrom));
                sqlParameters.Add(new SqlParameter("@RowIDTo", model.PageIndexTo));
                sqlParameters.Add(new SqlParameter("@FromDate", fromDate + " 00:00:00.000"));
                sqlParameters.Add(new SqlParameter("@ToDate", endDate + " 23:59:59.000"));
                sqlParameters.Add(new SqlParameter("@SortOrder", model.Request.SortOrder));
                sqlParameters.Add(new SqlParameter("@FileName", model.Request.FileName ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@FileStatus", model.Request.Status ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@FileType", model.Request.FileType ?? 0));
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output
                };
                sqlParameters.Add(totalItemsParam);
                try
                {
                    var response = taxiEpay.Database.SqlQuery<iLinkFileVM>("EXEC GetiLinkFiles " +
                  "@FromDate, " +
                  "@ToDate, " +
                  "@FileName, " +
                  "@FileStatus, " +
                  "@FileType, " +
                  "@SortOrder, " +
                  "@RowIDFrom, " +
                  "@RowIDTo, " +
                  "@TotalItems OUT", sqlParameters.ToArray()).ToList();

                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    model.TotalItems = total;
                    model.Response = response;

                    return new CustomJsonResult()
                    {
                        Data = model
                    };
                }
                catch (Exception e)
                {
                    model.IsError = true;
                    model.Message = e.Message;
                    return new CustomJsonResult()
                    {
                        Data = model
                    };
                }
                //var response = taxiEpay.Database.SqlQuery<iLinkFileVM>("EXEC GetiLinkFiles " +
                //    "@FromDate, " +
                //    "@ToDate, " +
                //    "@FileName, " +
                //    "@FileStatus, " +
                //    "@FileType, " +
                //    "@SortOrder, " +
                //    "@RowIDFrom, " +
                //    "@RowIDTo, " +
                //    "@TotalItems OUT", sqlParameters.ToArray()).ToList();

                //int total = 0;
                //int.TryParse(totalItemsParam.Value.ToString(), out total);
                //int totalPages = (int)Math.Ceiling((float)total / (float)model.PageSize);

                //var jsonData = new
                //{
                //    total = totalPages,
                //    page = model.PageIndex,
                //    records = total,
                //    rows = response
                //};

                //return new CustomJsonResult()
                //{
                //    Data = jsonData
                //};
            }
        }
        public ActionResult TransactionManagement()
        {
            using (var taxiEpay = new TaxiEpayEntities())
            {
                var cardType = taxiEpay.SystemConfigurations.Where(s => s.ConfigurationKey == "WestPacCardTypes").FirstOrDefault().ConfigurationValue;
                ViewBag.CardType = cardType.Split('|').Select(n => new SelectListItem()
                {
                    Text = n,
                    Value = n
                }).ToList();
                var transactionType = taxiEpay.SystemConfigurations.Where(s => s.ConfigurationKey == "iLinkTransactionType").FirstOrDefault().ConfigurationValue;
                ViewBag.TransactionType = transactionType.Split('|').Select(s => s.Split(';')).Select(n => new SelectListItem()
                {
                    Text = n[1],
                    Value = n[0]
                }).ToList();
            }
            ILinkTransactionManagementSearchVM model = new ILinkTransactionManagementSearchVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult GetImportedTransaction(GridModel<ILinkTransactionManagementSearchVM, List<ILinkTransactionVM>> model)
        {
            if (model.Request.TransactionDateFrom != null)
            {
                model.Request.TransactionDateFrom = model.Request.TransactionDateFrom.Substring(6) + model.Request.TransactionDateFrom.Substring(3, 2) + model.Request.TransactionDateFrom.Substring(0, 2);

            }

            if (model.Request.TransactionDateTo != null)
            {
                model.Request.TransactionDateTo = model.Request.TransactionDateTo.Substring(6) + model.Request.TransactionDateTo.Substring(3, 2) + model.Request.TransactionDateTo.Substring(0, 2);
            }

            if (model.Request.ImportedDateFrom != null)
            {
                model.Request.ImportedDateFrom = model.Request.ImportedDateFrom.Substring(6) + "-" + model.Request.ImportedDateFrom.Substring(3, 2) + "-" + model.Request.ImportedDateFrom.Substring(0, 2);
            }

            if (model.Request.ImportedDateTo != null)
            {
                model.Request.ImportedDateTo = model.Request.ImportedDateTo.Substring(6) + "-" + model.Request.ImportedDateTo.Substring(3, 2) + "-" + model.Request.ImportedDateTo.Substring(0, 2);
            }

            using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
            {
                taxiEpay.Database.CommandTimeout = int.MaxValue;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@TransactionDateFrom", model.Request.TransactionDateFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TransactionDateTo", model.Request.TransactionDateTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@ImportedDateFrom", model.Request.ImportedDateFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@ImportedDateTo", model.Request.ImportedDateTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TransactionNumber", model.Request.TransactionNumber ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AuthorisationNumber", model.Request.AuthorisationNumber ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@MemberID", model.Request.MemberID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TerminalID", model.Request.TerminalID ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@CardNumber", model.Request.CardNumber ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@CardType", model.Request.CardType ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AmountFrom", model.Request.AmountFrom ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@AmountTo", model.Request.AmountTo ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@BatchNumber", model.Request.BatchNumber ?? string.Empty));
                if (model.Request.Approved == model.Request.Declined)
                {
                    sqlParameters.Add(new SqlParameter("@Status", ""));
                }
                else
                {
                    sqlParameters.Add(new SqlParameter("@Status", model.Request.Approved ? "Approved" : "Declined"));
                }

                sqlParameters.Add(new SqlParameter("@SourceFile", model.Request.SourceFile ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@TransactionType", model.Request.TransactionType ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@IsVoid", model.Request.IsVoid ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortField", model.Request.SortField ?? string.Empty));
                sqlParameters.Add(new SqlParameter("@SortOrder", model.Request.SortOrder ?? string.Empty));

                sqlParameters.Add(new SqlParameter("@PageSize", model.PageSize));
                sqlParameters.Add(new SqlParameter("@PageIndex", model.PageIndex));

                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@TotalItems",
                    Value = 0,
                    Direction = ParameterDirection.Output
                };
                sqlParameters.Add(totalItemsParam);
                try
                {
                    var response = taxiEpay.Database.SqlQuery<ILinkTransactionVM>("EXEC GetImportedTransactions "
                    + "  @TransactionDateFrom"
                    + ", @TransactionDateTo"
                    + ", @ImportedDateFrom"
                    + ", @ImportedDateTo"
                    + ", @TransactionNumber"
                    + ", @AuthorisationNumber"
                    + ", @MemberID"
                    + ", @TerminalId"
                    + ", @CardNumber"
                    + ", @CardType"
                    + ", @AmountFrom"
                    + ", @AmountTo"
                    + ", @BatchNumber"
                    + ", @Status"
                    + ", @SourceFile"
                    + ", @TransactionType"
                    + ", @IsVoid"
                    + ", @SortField"
                    + ", @SortOrder"
                    + ", @PageSize"
                    + ", @PageIndex"
                    + ", @TotalItems OUT", sqlParameters.ToArray()).ToList();

                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    model.TotalItems = total;
                    model.Response = response;

                    return new CustomJsonResult()
                    {
                        Data = model
                    };
                }
                catch (Exception e)
                {
                    model.IsError = true;
                    model.Message = e.Message;
                    return new CustomJsonResult()
                    {
                        Data = model
                    };
                }
            }
        }

        public ActionResult ExportImportedTransaction(string TransactionDateFrom
            , string TransactionDateTo
            , string ImportedDateFrom
            , string ImportedDateTo
            , string CardNumber
            , string TransactionNumber
            , string AuthorisationNumber
            , string MemberID
            , string TerminalID
            , string CardType
            , string AmountFrom
            , string AmountTo
            , string BatchNumber
            , bool Approved
            , bool Declined
            , string SourceFile
            , string TransactionType
            , string IsVoid)
        {
            if (!string.IsNullOrEmpty(TransactionDateFrom))
            {
                TransactionDateFrom = TransactionDateFrom.Substring(6) + TransactionDateFrom.Substring(3, 2) + TransactionDateFrom.Substring(0, 2);
            }

            if (!string.IsNullOrEmpty(TransactionDateTo))
            {
                TransactionDateTo = TransactionDateTo.Substring(6) + TransactionDateTo.Substring(3, 2) + TransactionDateTo.Substring(0, 2);
            }

            if (!string.IsNullOrEmpty(ImportedDateFrom))
            {
                ImportedDateFrom = ImportedDateFrom.Substring(6) + "-" + ImportedDateFrom.Substring(3, 2) + "-" + ImportedDateFrom.Substring(0, 2);
            }

            if (!string.IsNullOrEmpty(ImportedDateTo))
            {
                ImportedDateTo = ImportedDateTo.Substring(6) + "-" + ImportedDateTo.Substring(3, 2) + "-" + ImportedDateTo.Substring(0, 2);
            }

            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}iLinkTransactionExport";

                IList<ReportingWebService.ParameterValue> reportparam = new List<ReportingWebService.ParameterValue>();

                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionDateFrom", Value = TransactionDateFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionDateTo", Value = TransactionDateTo ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "ImportedDateFrom", Value = ImportedDateFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "ImportedDateTo", Value = ImportedDateTo ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionNumber", Value = TransactionNumber ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "AuthorisationNumber", Value = AuthorisationNumber ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "MemberID", Value = MemberID ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TerminalID", Value = TerminalID ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CardNumber", Value = CardNumber ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CardType", Value = CardType ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "AmountFrom", Value = AmountFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "AmountTo", Value = AmountTo ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "BatchNumber", Value = BatchNumber ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SourceFile", Value = SourceFile ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionType", Value = TransactionType ?? "" });
                if (Approved == Declined)
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "Status", Value = "" });
                }
                else
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "Status", Value = Approved ? "Approved" : "Declined" });
                }
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "IsVoid", Value = IsVoid ?? "" });


                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, reportparam, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                return File(new byte[] { }, "application/xlsx");
            }
        }

        [HttpPost]
        public ActionResult ManuallyImportTransaction()
        {
            try
            {
                ConfigurationModel sysConfig = LoadConfiguration(3);
                List<iLinkFileType> fileTypes;
                HttpPostedFileBase manuallyFile = Request.Files[0];
                iLinkFile iLinkFile;
                var gridModel = new GridModel<ILinkFileManagementSearchVM, List<RejectedTransaction>>() { PageSize = 50, PageIndex = 1 };
                string errorMess;

                using (var taxiEpayDbContext = new TaxiEpayEntities())
                {
                    fileTypes = taxiEpayDbContext.iLinkFileTypes.ToList();
                }

                bool isAcceptFile = IsAcceptableFile(manuallyFile.FileName, sysConfig.iLinkFilenameConvention, sysConfig.iLinkAllowedFileExtensions, sysConfig.iLinkFileReleaseDate, fileTypes);

                if (!isAcceptFile)
                {
                    gridModel.IsError = true;
                    gridModel.Message = "Invalid file format. Please choose another file.";

                    return new CustomJsonResult()
                    {
                        Data = gridModel
                    };
                }

                using (var taxiEpayDbContext = new TaxiEpayEntities())
                {
                    iLinkFile = taxiEpayDbContext.iLinkFiles.FirstOrDefault(f => f.Filename == manuallyFile.FileName);
                }

                if (iLinkFile != null)
                {
                    gridModel = new GridModel<ILinkFileManagementSearchVM, List<RejectedTransaction>>()
                    {
                        IsError = true,
                        Message = "The file had already been imported. Please use another file."
                    };

                    return new CustomJsonResult()
                    {
                        Data = gridModel
                    };
                }

                bool isUploadS3Successfully = true;
                iLinkFileModel fileForUpload;

                fileForUpload = CreateDownloadedFile(manuallyFile, sysConfig.DownloadedStatusCode, fileTypes);

                try
                {
                    fileForUpload.URL = UploadFileAsync(fileForUpload.FileContent, fileForUpload.FileName, sysConfig);
                }
                catch (Exception e)
                {
                    fileForUpload.ErrorMessage = e.InnerException.Message;
                    isUploadS3Successfully = false;
                }

                if (isUploadS3Successfully)
                {
                    int iLinkFileId = SaveiLinkFile(fileForUpload);

                    iLinkFile = GetDownloadedFile(iLinkFileId);

                    int index = 1;
                    List<WestPacINModel> westPacs = new List<WestPacINModel>();

                    fileForUpload.FileContent.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                                                .ToList()
                                                .ForEach(l => CreateWestPac(index++, l, iLinkFile, ref westPacs));

                    if (westPacs.Count == 0)
                    {
                        using (var taxiEpay = new TaxiEpayEntities())
                        {
                            var ilinkFile = taxiEpay.iLinkFiles.FirstOrDefault(n => n.ID == iLinkFile.ID);
                            ilinkFile.StatusCode = sysConfig.FailedStatusCode;
                            taxiEpay.SaveChanges();
                        }
                    }
                    else
                    {
                        errorMess = SaveDataToWestPac(westPacs, iLinkFile.ID, sysConfig.ImportedStatusCode);

                        //import error by sql side
                        if (!string.IsNullOrWhiteSpace(errorMess))
                        {
                            gridModel.IsError = true;
                            gridModel.Message = errorMess;
                        }
                        //some transaction rejected
                        else if (westPacs.Any(r => !string.IsNullOrWhiteSpace(r.RejectedReason)))
                        {
                            var tranReject = westPacs.Where(r => !string.IsNullOrWhiteSpace(r.RejectedReason))
                                                        .Select(r =>
                                                        {
                                                            return new RejectedTransaction()
                                                            {
                                                                AccountType = r.accounttype,
                                                                AuthorisationNumber = r.authorisationnumber,
                                                                CardEntryMethod = r.tracktwo,
                                                                CardNumber = r.cardnumber,
                                                                CardType = r.cardtype,
                                                                CashoutOrTipAmount = r.cashoutortipamount,
                                                                IsVoid = r.voidtransactionflag,
                                                                MerchantBSBAccountNumber = r.merchantbsbaccnumber,
                                                                MerchantLocation = r.merchantlocation,
                                                                MerchantName = r.merchantname,
                                                                ProcessingDate = r.processingdatetype,
                                                                TerminalNumber = r.terminalnumber,
                                                                TransactionAmount = r.transactionamount,
                                                                TransactionDate = r.transactiondate,
                                                                TransactionNumber = r.transactionnumber,
                                                                TransactionTime = r.transactiontime,
                                                                TransactionType = r.transactiontype
                                                            };
                                                        })
                                                        .ToList();

                            gridModel.IsError = true;
                            gridModel.TotalItems = tranReject.Count;
                            gridModel.Response = tranReject;
                        }
                        //all transaction imported success
                        else
                        {
                            gridModel.TotalItems = westPacs.Count;
                        }
                    }
                }
                else
                {
                    gridModel = new GridModel<ILinkFileManagementSearchVM, List<RejectedTransaction>>()
                    {
                        IsError = true,
                        Message = fileForUpload.ErrorMessage
                    };
                }

                return new CustomJsonResult() { Data = gridModel };
            }
            catch (Exception ex)
            {
                return new CustomJsonResult() { 
                    Data = new GridModel<ILinkFileManagementSearchVM, List<RejectedTransaction>>() { IsError = true, Message = ex.Message }
                };
            }
        }

        private static ConfigurationModel LoadConfiguration(int systemID)
        {
            var configuration = new ConfigurationModel();
            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                var configs = taxiEpayDbContext.SystemConfigurations.Where(n => n.SystemID == systemID).ToList();

                var iLinkFilenameConventionObj = configs.FirstOrDefault(n => n.ConfigurationKey == "iLinkFilenameConvention");
                if (iLinkFilenameConventionObj == null || string.IsNullOrEmpty(iLinkFilenameConventionObj.ConfigurationValue))
                {
                    return null;
                }

                configuration.iLinkFilenameConvention = iLinkFilenameConventionObj.ConfigurationValue;

                var S3BucketAccessKeyObj = configs.FirstOrDefault(n => n.ConfigurationKey == "S3BucketAccessKey");
                if (S3BucketAccessKeyObj == null || string.IsNullOrEmpty(S3BucketAccessKeyObj.ConfigurationValue))
                {
                    return null;
                }

                configuration.S3BucketAccessKey = S3BucketAccessKeyObj.ConfigurationValue;

                var S3BucketSecretAccessKeyObj = configs.FirstOrDefault(n => n.ConfigurationKey == "S3BucketSecretAccessKey");
                if (S3BucketSecretAccessKeyObj == null || string.IsNullOrEmpty(S3BucketSecretAccessKeyObj.ConfigurationValue))
                {
                    return null;
                }
                using (AES aes = new AES(configuration.S3BucketAccessKey))
                {
                    configuration.S3BucketSecretAccessKey = aes.Decrypt(S3BucketSecretAccessKeyObj.ConfigurationValue);
                }

                var S3BucketNameObj = configs.FirstOrDefault(n => n.ConfigurationKey == "S3BucketName");
                if (S3BucketNameObj == null || string.IsNullOrEmpty(S3BucketNameObj.ConfigurationValue))
                {
                    return null;
                }

                configuration.S3BucketName = S3BucketNameObj.ConfigurationValue;

                var iLinkAllowedFileExtensionsObj = configs.FirstOrDefault(n => n.ConfigurationKey == "iLinkAllowedFileExtensions");
                if (iLinkAllowedFileExtensionsObj == null || string.IsNullOrEmpty(iLinkAllowedFileExtensionsObj.ConfigurationValue))
                {
                    return null;
                }
                configuration.iLinkAllowedFileExtensions = iLinkAllowedFileExtensionsObj.ConfigurationValue;

                var iLinkFileReleaseDateObj = configs.FirstOrDefault(n => n.ConfigurationKey == "iLinkFileReleaseDate");
                if (iLinkFileReleaseDateObj == null || string.IsNullOrEmpty(iLinkFileReleaseDateObj.ConfigurationValue))
                {
                    return null;
                }

                if (!DateTime.TryParseExact(iLinkFileReleaseDateObj.ConfigurationValue, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime iLinkFileReleasedDate))
                {
                    return null;
                }
                configuration.iLinkFileReleaseDate = iLinkFileReleasedDate;


                var DownloadedStatusCodeObj = configs.FirstOrDefault(n => n.ConfigurationKey == "DownloadedStatusCode");
                if (DownloadedStatusCodeObj == null || string.IsNullOrEmpty(DownloadedStatusCodeObj.ConfigurationValue))
                {
                    return null;
                }

                if (taxiEpayDbContext.iLinkFileStatus.Any(n =>
                    n.StatusCode == DownloadedStatusCodeObj.ConfigurationValue))
                {
                    configuration.DownloadedStatusCode = DownloadedStatusCodeObj.ConfigurationValue;
                }
                else
                {
                    return null;
                }


                var ImportedStatusCodeObj = configs.FirstOrDefault(n => n.ConfigurationKey == "ImportedStatusCode");
                if (ImportedStatusCodeObj == null || string.IsNullOrEmpty(ImportedStatusCodeObj.ConfigurationValue))
                {
                    return null;
                }


                if (taxiEpayDbContext.iLinkFileStatus.Any(n =>
                    n.StatusCode == ImportedStatusCodeObj.ConfigurationValue))
                {
                    configuration.ImportedStatusCode = ImportedStatusCodeObj.ConfigurationValue;
                }
                else
                {
                    return null;
                }

                var FailedStatusCodeObj = configs.FirstOrDefault(n => n.ConfigurationKey == "FailedStatusCode");
                if (FailedStatusCodeObj == null || string.IsNullOrEmpty(FailedStatusCodeObj.ConfigurationValue))
                {
                    return null;
                }

                if (taxiEpayDbContext.iLinkFileStatus.Any(n =>
                    n.StatusCode == FailedStatusCodeObj.ConfigurationValue))
                {
                    configuration.FailedStatusCode = FailedStatusCodeObj.ConfigurationValue;
                }
                else
                {
                    return null;
                }


            }
            return configuration;
        }

        private static bool IsAcceptableFile(
                                    string fileName,
                                    string iLinkFilenameConvention,
                                    string iLinkAllowedFileExtensions,
                                    DateTime iLinkFileReleaseDateConfig,
                                    List<iLinkFileType> fileTypes)
        {
            var fullRegex = $"^{iLinkFilenameConvention}\\w+{iLinkAllowedFileExtensions}$";

            if (Regex.IsMatch(fileName, fullRegex))
            {
                var isSuccess = fileTypes
                    .Select(ft => Regex.Match(fileName, ft.ReconigzableExpression))
                    .Any(a => a.Success);

                if (isSuccess)
                {
                    var fileNameItem = fileName.Split('_');

                    if (fileNameItem.Length > 2
                        && DateTime.TryParseExact(fileNameItem[2], "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime iLinkFileReleasedDate)
                        && iLinkFileReleasedDate >= iLinkFileReleaseDateConfig)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static iLinkFileModel CreateDownloadedFile(HttpPostedFileBase file, string downloadedStatusCode, List<iLinkFileType> fileTypes)
        {
            iLinkFileModel fileDownloaded;
            var dataLen = (float)file.ContentLength;
            string data = new StreamReader(file.InputStream).ReadToEnd();

            fileDownloaded = new iLinkFileModel()
            {
                FileName = file.FileName,
                FileSize = (float)Math.Round(dataLen / 1048576, 2),
                StatusCode = downloadedStatusCode,
                CreatedDate = DateTime.ParseExact(file.FileName.Split('_')[2], "yyyyMMdd", CultureInfo.InvariantCulture),
                FileTypeID = fileTypes.Where(ft => Regex.IsMatch(file.FileName, ft.ReconigzableExpression))
                                        .Select(ft => ft.ID)
                                        .SingleOrDefault(),
                DownloadedDate = DateTime.Now,
                FileContent = data,
            };

            return fileDownloaded;
        }

        private static string UploadFileAsync(string content, string filename, ConfigurationModel config)
        {
            //Build S3 config
            var s3Client = new AmazonS3Client(config.S3BucketAccessKey, config.S3BucketSecretAccessKey, RegionEndpoint.APSoutheast2);
            var fileTransferUtility = new TransferUtility(s3Client);
            var fileTransferUtilityRequest = new TransferUtilityUploadRequest();

            //Build file stream
            var fileToUpload = GenerateStreamFromString(content);

            // Specify advanced settings. Upload data from a type of System.IO.Stream.
            fileTransferUtilityRequest = new TransferUtilityUploadRequest
            {
                BucketName = config.S3BucketName,
                InputStream = fileToUpload,
                StorageClass = S3StorageClass.Standard,
                Key = filename
            };
            fileTransferUtility.UploadAsync(fileTransferUtilityRequest).Wait();

            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
            {
                BucketName = config.S3BucketName,
                Key = fileTransferUtilityRequest.Key,
                Expires = DateTime.Now.AddYears(10),
                Protocol = Protocol.HTTPS
            };

            //Get URL
            return s3Client.GetPreSignedURL(request);

            return string.Empty;
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private static int SaveiLinkFile(iLinkFileModel fileModel)
        {
            int returnedId = -1;

            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                //logger.Info("Saving file " + fileModel.FileName);

                iLinkFile file = new iLinkFile();
                file.CreatedDate = fileModel.CreatedDate;
                file.DownloadedDate = fileModel.DownloadedDate;
                file.Filename = fileModel.FileName;
                file.FileSize = fileModel.FileSize;
                file.FileTypeID = fileModel.FileTypeID;
                file.StatusCode = fileModel.StatusCode;
                file.URL = fileModel.URL;
                taxiEpayDbContext.iLinkFiles.Add(file);
                taxiEpayDbContext.SaveChanges();
                returnedId = file.ID;

                //logger.Info("Finish saving file " + fileModel.FileName);
            }

            return returnedId;
        }

        private static iLinkFile GetDownloadedFile(int Id)
        {
            //logger.Info($"Start to get all file ready for importing");

            using (var taxiEpayDbContext = new TaxiEpayEntities())
            {
                iLinkFile iLinkFiles = taxiEpayDbContext.iLinkFiles
                        .FirstOrDefault(f => f.ID == Id);

                //logger.Info("Finished getting all files");

                return iLinkFiles;
            }
        }

        private static void CreateWestPac(int index, string line, iLinkFile file, ref List<WestPacINModel> listWestPac)
        {
            WestPacINModel westPac = new WestPacINModel
            {
                iLinkFileID = file.ID,
                sourcefile = file.Filename,
                recordcreated = DateTime.Now
            };

            try
            {
                var recordType = line[0].ToString();
                if (recordType == "1")
                {
                    if (line.Length != 270)
                    {
                        string error = $"Line {index} of file {file.Filename} is incorrect format";
                        westPac.RejectedReason = error;
                        listWestPac.Add(westPac);
                    }
                    else
                    {
                        string terminalId = line.Substring(1, 16).Trim();
                        string transactionNumber = line.Substring(52, 12).Trim();
                        string transactionType = line.Substring(33, 19).Trim();
                        string transactionDate = line.Substring(25, 8).Trim();
                        string transactionTime = line.Substring(64, 4).Trim();
                        string transactionAmount = line.Substring(68, 15).Trim();
                        string cardNumber = line.Substring(83, 19).Trim();
                        string cardType = line.Substring(102, 4).Trim();
                        string authorisationNumber = line.Substring(109, 8).Trim();

                        var westPacData = listWestPac
                                            .Any(wp => wp.terminalnumber == terminalId &&
                                                        wp.transactionnumber == Convert.ToInt32(transactionNumber) &&
                                                        wp.transactiontype == transactionType &&
                                                        wp.transactiondate == transactionDate &&
                                                        wp.transactiontime == transactionTime &&
                                                        wp.transactionamount == transactionAmount &&
                                                        wp.cardnumber == cardNumber &&
                                                        wp.cardtype == cardType &&
                                                        wp.authorisationnumber == authorisationNumber);

                        if (westPacData)
                        {
                            string error = $"Duplicate terminal number '{terminalId}', ";
                            error += $"transaction number '{transactionNumber}', ";
                            error += $"transaction type '{transactionType}', ";
                            error += $"transaction date '{transactionDate}', ";
                            error += $"transaction time '{transactionTime}', ";
                            error += $"transaction amount '{transactionAmount}', ";
                            error += $"card number '{cardNumber}', ";
                            error += $"card type '{cardType}', ";
                            error += $"authorisation number '{authorisationNumber}', ";

                            westPac.terminalnumber = terminalId;
                            westPac.transactionnumber = int.Parse(transactionNumber);
                            westPac.transactiontype = transactionType;
                            westPac.transactiondate = transactionDate;
                            westPac.transactiontime = transactionTime;
                            westPac.transactionamount = transactionAmount;
                            westPac.cardnumber = cardNumber;
                            westPac.cardtype = cardType;
                            westPac.authorisationnumber = authorisationNumber;
                            westPac.RejectedReason = error;

                            listWestPac.Add(westPac);
                        }
                        else
                        {
                            westPac.terminalnumber = terminalId;
                            westPac.processingdatetype = line.Substring(17, 8).Trim();
                            westPac.transactiondate = line.Substring(25, 8).Trim();
                            westPac.transactiontype = transactionType;
                            westPac.transactiontime = line.Substring(64, 4).Trim();
                            westPac.transactionamount = line.Substring(68, 15).Trim();
                            westPac.cardnumber = line.Substring(83, 19).Trim();
                            westPac.cardtype = line.Substring(102, 4).Trim();
                            westPac.accounttype = line.Substring(106, 3).Trim();
                            westPac.authorisationnumber = line.Substring(109, 8).Trim();
                            westPac.doubtfulltransactionflag = line.Substring(117, 1).Trim();
                            westPac.voidtransactionflag = line.Substring(118, 1).Trim();
                            westPac.tracktwo = line.Substring(119, 6).Trim();
                            westPac.merchantbsbaccnumber = line.Substring(125, 40).Trim();
                            westPac.merchantname = line.Substring(165, 20).Trim();
                            westPac.merchantlocation = line.Substring(185, 20).Trim();
                            westPac.screenphoneinformation = line.Substring(205, 50).Trim();
                            westPac.cashoutortipamount = line.Substring(255, 15).Trim();
                            westPac.status = "Approved";
                            westPac.transactionnumber = int.Parse(transactionNumber);
                            westPac.RejectedReason = null;

                            listWestPac.Add(westPac);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                westPac.RejectedReason = $"Error when parsing transaction number on line {index} of file {file.Filename}";
                listWestPac.Add(westPac);
            }
        }

        private static string SaveDataToWestPac(List<WestPacINModel> westPacs, int iLinkFileID, string statusCode)
        {
            try
            {
                using (var con = new TaxiEpayEntities())
                {
                    con.Database.CommandTimeout = int.MaxValue;

                    var westpacINTable = new DataTable("dbo.WestpacINTableType");
                    westpacINTable.Columns.Add("status", typeof(string));
                    westpacINTable.Columns.Add("terminalnumber", typeof(string));
                    westpacINTable.Columns.Add("processingdatetype", typeof(string));
                    westpacINTable.Columns.Add("transactiondate", typeof(string));
                    westpacINTable.Columns.Add("transactiontype", typeof(string));
                    westpacINTable.Columns.Add("transactionnumber", typeof(int));
                    westpacINTable.Columns.Add("transactiontime", typeof(string));
                    westpacINTable.Columns.Add("transactionamount", typeof(string));
                    westpacINTable.Columns.Add("cardnumber", typeof(string));
                    westpacINTable.Columns.Add("cardtype", typeof(string));
                    westpacINTable.Columns.Add("accounttype", typeof(string));
                    westpacINTable.Columns.Add("authorisationnumber", typeof(string));
                    westpacINTable.Columns.Add("doubtfulltransactionflag", typeof(string));
                    westpacINTable.Columns.Add("voidtransactionflag", typeof(string));

                    westpacINTable.Columns.Add("tracktwo", typeof(string));
                    westpacINTable.Columns.Add("merchantbsbaccnumber", typeof(string));
                    westpacINTable.Columns.Add("merchantname", typeof(string));
                    westpacINTable.Columns.Add("merchantlocation", typeof(string));
                    westpacINTable.Columns.Add("screenphoneinformation", typeof(string));
                    westpacINTable.Columns.Add("cashoutortipamount", typeof(string));
                    westpacINTable.Columns.Add("sourcefile", typeof(string));
                    westpacINTable.Columns.Add("recordcreated", typeof(DateTime));
                    westpacINTable.Columns.Add("docket_key", typeof(int));
                    westpacINTable.Columns.Add("amount", typeof(decimal));
                    westpacINTable.Columns.Add("ReconciledDate", typeof(DateTime));
                    westpacINTable.Columns.Add("ReconciledBy", typeof(string));
                    westpacINTable.Columns.Add("BankReconReasonKey", typeof(int));
                    westpacINTable.Columns.Add("iLinkFileID", typeof(int));
                    westpacINTable.Columns.Add("RejectedReason", typeof(string));


                    foreach (var item in westPacs)
                    {
                        westpacINTable.Rows.Add(item.status,
                                                item.terminalnumber,
                                                item.processingdatetype,
                                                item.transactiondate,
                                                item.transactiontype,
                                                item.transactionnumber,
                                                item.transactiontime,
                                                item.transactionamount,
                                                item.cardnumber,
                                                item.cardtype,
                                                item.accounttype,
                                                item.authorisationnumber,
                                                item.doubtfulltransactionflag,
                                                item.voidtransactionflag,

                                                item.tracktwo,
                                                item.merchantbsbaccnumber,
                                                item.merchantname,
                                                item.merchantlocation,
                                                item.screenphoneinformation,
                                                item.cashoutortipamount,
                                                item.sourcefile,
                                                item.recordcreated,
                                                item.docket_key,
                                                item.amount,
                                                item.ReconciledDate,
                                                item.ReconciledBy,
                                                item.BankReconReasonKey,
                                                item.iLinkFileID,
                                                item.RejectedReason
                                                );
                    }

                    List<SqlParameter> sqlParameters = new List<SqlParameter>
                {
                    new SqlParameter("@ILinkFileID", iLinkFileID),
                    new SqlParameter("@StatusCode", statusCode),
                    new SqlParameter("@WestpacINTVP", SqlDbType.Structured)
                    {
                        Value = westpacINTable,
                        TypeName = "dbo.WestpacINTableType"
                    }
                };

                    var errorMessage = con.Database.SqlQuery<string>("EXEC usp_job_iLink_ImportFile @ILinkFileID, @StatusCode, @WestpacINTVP", sqlParameters.ToArray()).ToList();
                    return errorMessage?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}