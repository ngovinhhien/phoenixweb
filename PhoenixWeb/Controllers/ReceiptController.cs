﻿using log4net;
using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using Phoenix_Service;
using PhoenixObjects.Enums;
using PhoenixWeb.Common;
using PhoenixWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    [RBAC(Permission = "ViewPhoenixWebPaywayReceipt")]
    public class ReceiptController : PhoenixWebController
    {
        private ILog logger = LogManager.GetLogger("ReceiptController");

        public ActionResult Payway()
        {
            PaywayReceiptViewModel model = new PaywayReceiptViewModel();

            model.StartDate = DateTime.Now.ToString("dd-MM-yyyy");
            model.EndDate = DateTime.Now.ToString("dd-MM-yyyy");
            return View("Payway", model);
        }

        public JsonResult GetPaywayReceiptFiles([System.Web.Http.FromBody] SearchPaywayReceiptViewModel searchObject) 
        {
            var firstDateStr = searchObject.StartDate;
            var dateformat = "dd-MM-yyyy";
            var transStart = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB"));
            var endDateStr = searchObject.EndDate;
            var transEnd = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).AddDays(1);

            using (var entities = new TaxiEpayEntities())
            {
                entities.SetCommandTimeOut(1800);

                var paywayReceiptFiles = entities.PayWayReceiptFiles
                    .Where(prf => prf.TransactionDateTime <= transEnd &&
                                  prf.TransactionDateTime >= transStart &&
                                  (string.IsNullOrEmpty(searchObject.CustomerReferenceNumber) || prf.CustomerReferenceNumber.Contains(searchObject.CustomerReferenceNumber)) &&
                                  (string.IsNullOrEmpty(searchObject.OrderNumber) || prf.OrderNumber == searchObject.OrderNumber))
                    .OrderByDescending(prf => prf.TransactionDateTime)
                    .ToList();

                int totalRecords = paywayReceiptFiles.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                int skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;

                var result = paywayReceiptFiles.Skip(skip).Take(searchObject.PageSize);
                var res = result.Select(receipt => new
                {
                    id = receipt.PayWayReceiptFileID,
                    merchantId = receipt.MerchantId,
                    cardPAN = receipt.CardPAN,
                    cardExpiry = receipt.CardExpiry,
                    orderType = receipt.OrderType,
                    principalAmount = receipt.PrincipalAmount,
                    surchargeAmount = receipt.SurchargeAmount,
                    amount = receipt.Amount,
                    currency = receipt.Currency,
                    orderNumber = receipt.OrderNumber,
                    customerReferenceNumber = receipt.CustomerReferenceNumber,
                    originalOrderNumber = receipt.OriginalOrderNumber,
                    responseCode = receipt.ResponseCode,
                    responseText = receipt.ResponseText,
                    receiptNumber = receipt.ReceiptNumber,
                    transactionDate = receipt.TransactionDateTime.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    status = receipt.Status
                });

                var jsonData = new
                {
                    total = totalPages,
                    page = searchObject.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult GetPaywayReceiptExportFile(string startDate, string endDate, string terminalId, string orderNumber)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                MemberDA mda = new MemberDA();

                var firstDateStr = startDate;
                var dateformat = "dd-MM-yyyy";
                var transStart = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB"));
                var endDateStr = endDate;
                var transEnd = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).AddDays(1);

                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}Payway Receipt File";

                IList<ReportingWebService.ParameterValue> parameters = new List<ReportingWebService.ParameterValue>();

                parameters.Add(new ReportingWebService.ParameterValue { Name = "StartDate", Value = transStart.ToShortDateString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "EndDate", Value = transEnd.ToShortDateString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "TerminalId", Value = terminalId });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "OrderNumber", Value = orderNumber });

                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                logger.Fatal("Error generate excel:" + e.Message, e);
                return File(new byte[] { }, "application/vnd.ms-excel", "Payway Receipt File.xls");
            }

        }
    }
}