﻿using CsvHelper;
using Newtonsoft.Json;
using Phoenix_BusLog.Data;
using Phoenix_BusLog.TransactionHost.BusLogs;
using Phoenix_BusLog.TransactionHost.Model;
using Phoenix_Service;
using Phoenix_TransactionHostUI;
using PhoenixObjects.Search;
using PhoenixWeb.Common;
using PhoenixWeb.Models;
using PhoenixWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using PhoenixWeb.Helper;
using TerminalType = Phoenix_TransactionHostUI.TerminalType;
using System.Data.SqlClient;
using System.Data;
using PhoenixWeb.Models.Contants;
using Phoenix_BusLog;
using PhoenixObjects.Enums;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class TerminalController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [RBAC(Permission = "VAATerminalSearch")]
        public ActionResult Search()
        {
            List<TerminalModel> model = new List<TerminalModel>();
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "VAATerminalSearch")]
        public ActionResult Search(FormCollection c)
        {
            List<TerminalModel> model = new List<TerminalModel>();
            string termnialid = c["SearchKey11"].ToString();
            string serialno = c["SearchKey12"].ToString();
            TerminalHostDA tda = new TerminalHostDA();
            // model = tda.Terminal_ByID(termnialid);

            if (termnialid != "")
            {
                model = tda.Terminal_ByID(termnialid);
            }
            if (serialno != "")
            {
                model = tda.Terminal_BySerial(serialno);
            }

            ViewBag.MaxCategoryAssignedInTerminal =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MaxCategoryAssignedInTerminal
                    .ToString());

            return View(model);
        }



        public ActionResult SearchTID(string terminalId)
        {
            List<TerminalModel> model = new List<TerminalModel>();

            TerminalHostDA tda = new TerminalHostDA();
            // model = tda.Terminal_ByID(termnialid);

            if (terminalId != "")
            {
                model = tda.Terminal_ByID(terminalId);
            }

            return View("Search", model);
        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalConfigAllow")]
        public ActionResult Config(int Key)
        {
            //TerminalModel terminal = getEntity.Client<TerminalModel>(Key);
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            TerminalDA terDA = new TerminalDA();

            terminal = tda.Terminal_ByKey(Key).FirstOrDefault();

            if (terminal.DefaultBackgroundColour != null && terminal.DefaultBackgroundColour.Length > 0)
            {
                terminal.DefaultBackgroundColour = "#" + terminal.DefaultBackgroundColour;
            }
            if (terminal.EnableNetworkReceiptPrint == 1)
            {
                terminal.EnableNetworkReceiptPrintB = true;
            }
            else
            {
                terminal.EnableNetworkReceiptPrintB = false;
            }

            if (terminal.EnableMerchantLogoPrint == 1)
            {
                terminal.EnableMerchantLogoPrintB = true;
            }
            else
            {
                terminal.EnableMerchantLogoPrintB = false;
            }

            if (terminal.ClearOverrideSettings == true)
            {
                terminal.ClearOverrideSettingsB = true;
            }
            else
            {
                terminal.ClearOverrideSettingsB = false;
            }

            if (terminal.IsPreTranPing == true)
            {
                terminal.IsPreTranPingB = true;
            }
            else
            {
                terminal.IsPreTranPingB = false;
            }

            if (terminal.NetworkTableStatus == true)
            {
                terminal.NetworkTableStatusB = true;
            }
            else
            {
                terminal.NetworkTableStatusB = false;
            }
            if (terminal.EnableRefundTran == true)
            {
                terminal.EnableRefundTranB = true;
            }
            else
            {
                terminal.EnableRefundTranB = false;
            }

            if (terminal.EnableAuthTran == true)
            {
                terminal.EnableAuthTranB = true;
            }
            else
            {
                terminal.EnableAuthTranB = false;
            }
            if (terminal.EnableTipTran == true)
            {
                terminal.EnableTipTranB = true;
            }
            else
            {
                terminal.EnableTipTranB = false;
            }

            if (terminal.EnableCashOutTran == true)
            {
                terminal.EnableCashOutTranB = true;
            }
            else
            {
                terminal.EnableCashOutTranB = false;
            }
            if (terminal.Sound == true)
            {
                terminal.SoundB = true;
            }
            else
            {
                terminal.SoundB = false;
            }
            if (terminal.EnableMOTO == true)
            {
                terminal.EnableMOTOB = true;
            }
            else
            {
                terminal.EnableMOTOB = false;
            }



            string term = string.Empty;
            if (terminal.EOSTime != null)
            {
                term = terminal.EOSTime.Trim();
                string t1 = string.Empty;
                string t = term;
                string t2 = string.Empty;
                string tt = string.Empty;

                if (t.Length >= 2)
                {
                    if (t.Length == 3)
                    {
                        t1 = "0" + t.Substring(0, 1);
                        t2 = t.Substring(1, 2);
                    }
                    else if (t.Length == 2)
                    {
                        t1 = "00";
                        t2 = t.Substring(0, 2);
                    }
                    else
                    {
                        t1 = t.Substring(0, 2);
                    }

                    if (t.Length > 3)
                    {
                        t2 = t.Substring(2, 2);
                    }

                    if (t2 == null || t2 == "" || t2 == " " || t2 == string.Empty)
                    {
                        t2 = "00";
                    }
                    tt = t1 + ":" + t2;
                    terminal.EOSTimeDecimal = tt;
                    terminal.IsManualEOS = false;
                }
                else if (t.Length == 1)
                {
                    if (Convert.ToInt32(t) > 0)
                    {
                        tt = "00" + ":0" + t;
                        terminal.EOSTimeDecimal = tt;
                        terminal.IsManualEOS = false;
                    }
                    else
                    {
                        terminal.EOSTimeDecimal = "0";
                        terminal.IsManualEOS = true;
                    }
                }
                else
                {
                    terminal.EOSTimeDecimal = "0";
                    terminal.IsManualEOS = true;
                }
            }
            else
            {
                term = null;
                terminal.IsManualEOS = true;
                terminal.EOSTimeDecimal = "0";
            }

            if (terminal.DefaultServiceFee != null)
            {
                string dt1 = string.Empty;
                string dt2 = string.Empty;
                decimal dt = terminal.DefaultServiceFee.Value;

                decimal s1 = Math.Round(dt / 100, 2);

                if (dt > 0)
                {
                    terminal.DefaultServiceFeeDecimal = s1.ToString();
                    terminal.IsDefaultServiceFeeZero = false;
                }
                else
                {
                    terminal.DefaultServiceFeeDecimal = "0.00";
                    terminal.IsDefaultServiceFeeZero = true;
                }
            }
            else
            {
                terminal.IsDefaultServiceFeeZero = true;
                terminal.DefaultServiceFeeDecimal = "0.00";
            }

            terminal.MinimumCharge = Math.Round(terminal.MinimumCharge.Value, 2);
            terminal.MaximumCharge = Math.Round(terminal.MaximumCharge.Value, 2);

            terminal.PaymentType = 0;

            if (terminal.MemberKey != 0)
            {
                OperatorDA opl = new OperatorDA();
                var op = opl.Get(terminal.MemberKey);
                if (op.PayByEFT.GetValueOrDefault(false) == true && op.Operator_key != 0)
                {
                    terminal.PaymentType = 1;
                }
            }
            DrawDropDownConfig();

            return View(terminal);
        }


        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalConfigAllow")]
        [ValidateInput(false)]
        public ActionResult Config(TerminalModel cEntity, FormCollection c)
        {
            try
            {
                int dt4 = 0;
                var t3 = string.Empty;
                // setEntity.UserName = User.Identity.Name;
                string t = c["EOSTimeDecimalId"].ToString();


                DateTime dtx;
                bool res = DateTime.TryParse(t, out dtx);

                string color = c["DefaultBackgroundColour"].ToString();
                string txtABNSetting = c["txtABNSetting"].ToString();
                decimal d = Convert.ToDecimal(txtABNSetting);
                decimal d11 = d * 100;
                int d1 = Convert.ToInt32(d11);
                string d2 = string.Empty;
                if (d1 == 0)
                {
                    d2 = "000000";
                }
                else
                {
                    if (d1 < 10)
                    {
                        d2 = "00000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 2)
                    {
                        d2 = "0000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 3)
                    {
                        d2 = "000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 4)
                    {
                        d2 = "00" + d1.ToString();
                    }
                    if (d1.ToString().Length == 5)
                    {
                        d2 = "0" + d1.ToString();
                    }
                    if (d1.ToString().Length == 6)
                    {
                        d2 = d1.ToString();
                    }
                }

                cEntity.ABNSetting = d2;
                string colc = color.Substring(1, color.Length - 1);

                if (t != "")
                {
                    var t2 = string.Empty;
                    var t1 = string.Empty;
                    if (t.Length >= 2)
                    {
                        t1 = t.Substring(0, 2);
                    }
                    if (t.Length >= 4)
                    {
                        t2 = t.Substring(3, 2);
                        t3 = t1 + t2;
                    }
                    else
                    {
                        t3 = null;
                    }
                }

                string dt = c["DefaultServiceFeeId"].ToString();
                if (dt != "")
                {
                    decimal g1 = Convert.ToDecimal(dt) * 100;
                    dt4 = Convert.ToInt32(g1);
                }


                ModelState.Remove("UploadTimer");

                //TerminalModel terminal = getEntity.Client<TerminalModel>(cEntity.TerminalKey);
                TerminalModel terminal = new TerminalModel();
                TerminalHostDA tda = new TerminalHostDA();
                terminal = tda.Terminal_ByKey(cEntity.TerminalKey).FirstOrDefault();
                ClientModel clientEntity = (ClientModel)terminal;

                if (ModelState.IsValid)
                {
                    if (cEntity.EnableMerchantLogoPrintB == true)
                    {
                        cEntity.EnableMerchantLogoPrint = 1;
                    }
                    else
                    {
                        cEntity.EnableMerchantLogoPrint = 0;
                    }
                    if (cEntity.EnableNetworkReceiptPrintB == true)
                    {
                        cEntity.EnableNetworkReceiptPrint = 1;
                    }
                    else
                    {
                        cEntity.EnableNetworkReceiptPrint = 0;
                    }

                    if (cEntity.TranAdviceSendMode != 2)
                    {
                        cEntity.IdleTimer = 0;
                        cEntity.IdleTransactions = 0;
                    }

                    //if (User.IsInPermission("VAATerminalConfigAdmin"))
                    if (this.HasPermission("VAATerminalConfigAdmin"))
                    {
                        using (TransactionHostEntities con = new TransactionHostEntities())
                        {
                            Terminal rModelToServer = ConvertToTerminalServer(terminal);

                            var tt = (from r in con.Terminals
                                      where r.TerminalKey == rModelToServer.TerminalKey
                                      select r).FirstOrDefault();
                            tt.VersionNo = tt.VersionNo + 1;
                            tt.InputTimeoutSec = cEntity.InputTimeoutSec;
                            tt.BacklightSec = cEntity.BacklightSec;
                            tt.SleepModeMin = cEntity.SleepModeMin;
                            tt.ScreenBrightness = Convert.ToInt32(c["ScreenBrightness"].ToString());// cEntity.ScreenBrightness;
                            tt.Sound = cEntity.SoundB;
                            tt.RemeberMe = cEntity.RemeberMe;
                            tt.ABNSetting = cEntity.ABNSetting;
                            tt.MinimumCharge = Convert.ToDecimal(c["MinimumCharge"].ToString());  //cEntity.MinimumCharge;
                            tt.MaximumCharge = Convert.ToDecimal(c["MaximumCharge"].ToString());// cEntity.MaximumCharge;
                            tt.EOSTime = t3;// cEntity.EOSTime;
                            tt.LoginMode = cEntity.LoginMode;
                            tt.UserIdConfig = cEntity.UserIdConfig;
                            tt.BusinessIdConfig = cEntity.BusinessIdConfig;
                            tt.ABNConfig = cEntity.ABNConfig;
                            tt.GPRSReconnectTimer = Convert.ToInt16(c["GPRSReconnectTimer"].ToString()); //cEntity.GPRSReconnectTimer;
                            tt.TNNetworkKey = cEntity.TNNetworkKey;
                            tt.StateKey = cEntity.StateKey;
                            tt.TerminalNetworkKey = cEntity.TerminalNetworkKey;
                            tt.AppRespCode = cEntity.AppRespCode;
                            tt.AppSignRespCode = cEntity.AppSignRespCode;
                            tt.IsPreTranPing = cEntity.IsPreTranPingB;
                            tt.TranAdviceSendMode = cEntity.TranAdviceSendMode;
                            tt.EnableMOTO = cEntity.EnableMOTOB;
                            tt.DefaultBackgroundColour = colc;// cEntity.DefaultBackgroundColour;


                            tt.UserID = c["UserIDName"].ToString();// cEntity.UserID;
                            tt.BusinessID = c["BusinessIDName"].ToString(); //cEntity.BusinessID;
                            tt.ABN = c["ABNName"].ToString();// cEntity.ABN;
                            tt.DefaultServiceFee = dt4;// cEntity.DefaultServiceFee;

                            tt.ServiceFeeMode = cEntity.ServiceFeeMode;
                            tt.CustomerPaymentType = cEntity.CustomerPaymentType;
                            tt.EnableNetworkReceiptPrint = cEntity.EnableNetworkReceiptPrint;
                            tt.EnableMerchantLogoPrint = cEntity.EnableMerchantLogoPrint;
                            tt.SignatureVerificationTimeout = Convert.ToInt16(c["SignatureVerificationTimeout"].ToString());// cEntity.SignatureVerificationTimeout;
                            tt.AutoLogonHour = Convert.ToByte(c["AutoLogonHour"].ToString());// cEntity.AutoLogonHour;
                            tt.AutoLogonMode = cEntity.AutoLogonMode;
                            tt.EnableRefundTran = cEntity.EnableRefundTranB;
                            tt.EnableAuthTran = cEntity.EnableAuthTranB;
                            tt.EnableTipTran = cEntity.EnableTipTranB;
                            tt.EnableCashOutTran = cEntity.EnableCashOutTranB;

                            tt.MerchantPassword = c["MerchantPasswordId"].ToString();// cEntity.MerchantPassword;
                            tt.BankLogonRespCode = cEntity.BankLogonRespCode;
                            tt.NetworkTableStatus = cEntity.NetworkTableStatusB;
                            tt.BusinessTypeKey = cEntity.BusinessTypeKey;
                            tt.RebootHours = cEntity.RebootHours;
                            //tt.EnableAutoTip = cEntity.EnableAutoTip;
                            tt.EnableMOTORefund = false; //implement ticket PHW-865: always default value = false
                            tt.AutoEOSMode = cEntity.AutoEOSMode;
                            tt.EnableDualAPNMode = cEntity.EnableDualAPNMode;
                            tt.ClearOverrideSettings = cEntity.ClearOverrideSettingsB;

                            tt.EnableReceiptReference = cEntity.EnableReceiptReference;
                            tt.EnableQantasReward = cEntity.EnableQantasReward;
                            tt.EnableQantasPointsRedemption = cEntity.EnableQantasPointsRedemption;
                            tt.EnableBusinessLogo = cEntity.EnableBusinessLogo;
                            tt.EnablePaperRollOrder = cEntity.EnablePaperRollOrder;
                            tt.EnableSupportCallBack = cEntity.EnableSupportCallBack;
                            tt.RefundTransMax = cEntity.RefundTransMax;
                            tt.RefundCumulativeMax = cEntity.RefundCumulativeMax;
                            tt.EnableZip = cEntity.EnableZip;

                            tt.AddLevyChargeToAmount = cEntity.AddLevyChargeToAmount;
                            tt.LevyCharge = Convert.ToInt32(cEntity.LevyCharge * 100);
                            tt.LevyLabel = cEntity.LevyLabel;

                            tt.CustomFooter = cEntity.CustomFooter;
                            tt.CustomFooterHtml = cEntity.CustomFooterHtml;


                            tt.UpdatedBy = new Guid();
                            tt.UpdatedDate = DateTime.Now;
                            tt.UpdatedByUser = User.Identity.Name;
                            tt.IdleTimer = cEntity.IdleTimer;
                            tt.IdleTransactions = cEntity.IdleTransactions;
                            tt.RecordingMode = cEntity.RecordingMode;
                            tt.UploadTimer = cEntity.RecordingMode == 2 ? cEntity.UploadTimer : 0;
                            tt.CardNotPresentTransactionLimit = cEntity.CardNotPresentTransactionLimit;
                            tt.CardPresentTransactionLimit = cEntity.CardPresentTransactionLimit;

                            cEntity.EnablePrintSettlement = cEntity.PaymentType == 0 ? true : cEntity.EnablePrintSettlement;

                            con.SaveChanges();

                            if (cEntity.Inactive != true)
                            {
                                int receiptKey =
                                        tt.ReceiptKey.HasValue ? tt.ReceiptKey.Value
                                        : con.Apps.Where(where => where.AppKey == tt.AppKey).FirstOrDefault().ReceiptKey;

                                Receipt receipt =
                                    con.Receipts.Where(where => where.ReceiptKey == receiptKey).FirstOrDefault();
                                if (receipt.HeaderLineOne != cEntity.TerminalHeaderFooter.HeaderLineOne
                                    || receipt.HeaderLineTwo != cEntity.TerminalHeaderFooter.HeaderLineTwo
                                    || receipt.HeaderLineThree != cEntity.TerminalHeaderFooter.HeaderLineThree
                                    || receipt.HeaderLineFour != cEntity.TerminalHeaderFooter.HeaderLineFour
                                    || receipt.FooterLineOne != cEntity.TerminalHeaderFooter.FooterLineOne
                                    || receipt.FooterLineTwo != cEntity.TerminalHeaderFooter.FooterLineTwo
                                    || receipt.FooterLineThree != cEntity.TerminalHeaderFooter.FooterLineThree
                                    || receipt.FooterLineFour != cEntity.TerminalHeaderFooter.FooterLineFour
                                    || receipt.EnablePrintCustomerReceiptFirst != cEntity.EnablePrintCustomerReceiptFirst
                                    || receipt.EnablePrintSettlement != (!cEntity.EnablePrintSettlement)
                                    || receipt.PrintReceiptMode != cEntity.PrintReceiptMode

                                    )
                                {
                                    con.usp_UpdateReceiptData(
                                                                        cEntity.TerminalHeaderFooter.FooterLineOne,
                                                                        cEntity.TerminalHeaderFooter.FooterLineTwo,
                                                                        cEntity.TerminalHeaderFooter.FooterLineThree,
                                                                        cEntity.TerminalHeaderFooter.FooterLineFour,
                                                                        cEntity.TerminalHeaderFooter.HeaderLineOne,
                                                                        cEntity.TerminalHeaderFooter.HeaderLineTwo,
                                                                        cEntity.TerminalHeaderFooter.HeaderLineThree,
                                                                        cEntity.TerminalHeaderFooter.HeaderLineFour,
                                                                        cEntity.EnablePrintCustomerReceiptFirst,
                                                                        !cEntity.EnablePrintSettlement,
                                                                        cEntity.PrintReceiptMode,
                                                                        tt.TerminalID);
                                }
                            }
                        }
                    }

                    else if (this.HasPermission("VAATerminalConfigOpts"))
                    {
                        using (TransactionHostEntities con = new TransactionHostEntities())
                        {
                            Terminal rModelToServer = ConvertToTerminalServer(terminal);

                            var tt = (from r in con.Terminals
                                      where r.TerminalKey == rModelToServer.TerminalKey
                                      select r).FirstOrDefault();
                            tt.VersionNo = tt.VersionNo + 1;
                            tt.BacklightSec = cEntity.BacklightSec;
                            tt.SleepModeMin = cEntity.SleepModeMin;
                            tt.ScreenBrightness = Convert.ToInt32(c["ScreenBrightness"].ToString());// cEntity.ScreenBrightness;
                            tt.UserIdConfig = cEntity.UserIdConfig;
                            tt.BusinessIdConfig = cEntity.BusinessIdConfig;
                            tt.TNNetworkKey = cEntity.TNNetworkKey;
                            tt.StateKey = cEntity.StateKey;
                            tt.TerminalNetworkKey = cEntity.TerminalNetworkKey;
                            tt.DefaultBackgroundColour = colc;// cEntity.DefaultBackgroundColour;
                            tt.Sound = cEntity.SoundB;
                            tt.UserID = c["UserIDName"].ToString();// cEntity.UserID;
                            tt.BusinessID = c["BusinessIDName"].ToString(); //cEntity.BusinessID;
                            tt.ABN = c["ABNName"].ToString();// cEntity.ABN;
                            tt.MinimumCharge = Convert.ToDecimal(c["MinimumCharge"].ToString());  //cEntity.MinimumCharge;
                            tt.MaximumCharge = Convert.ToDecimal(c["MaximumCharge"].ToString());// cEntity.MaximumCharge;
                            tt.ABNConfig = cEntity.ABNConfig;
                            tt.DefaultServiceFee = dt4;// cEntity.DefaultServiceFee;
                            tt.EOSTime = t3;// cEntity.EOSTime;
                            tt.EnableRefundTran = cEntity.EnableRefundTranB;
                            tt.EnableAuthTran = cEntity.EnableAuthTranB;
                            tt.EnableTipTran = cEntity.EnableTipTranB;
                            tt.EnableCashOutTran = cEntity.EnableCashOutTranB;
                            tt.MerchantPassword = c["MerchantPasswordId"].ToString();// cEntity.MerchantPassword;
                            tt.EnableNetworkReceiptPrint = cEntity.EnableNetworkReceiptPrint;
                            tt.EnableMerchantLogoPrint = cEntity.EnableMerchantLogoPrint;
                            tt.SignatureVerificationTimeout = Convert.ToInt16(c["SignatureVerificationTimeout"].ToString());// cEntity.SignatureVerificationTimeout;
                            tt.EnableMOTO = cEntity.EnableMOTOB;
                            tt.ABNSetting = cEntity.ABNSetting;
                            tt.BusinessTypeKey = cEntity.BusinessTypeKey;
                            tt.RebootHours = cEntity.RebootHours;
                            //tt.EnableAutoTip = cEntity.EnableAutoTip;
                            tt.EnableMOTORefund = cEntity.EnableMOTORefund;
                            tt.AutoEOSMode = cEntity.AutoEOSMode;
                            tt.EnableDualAPNMode = cEntity.EnableDualAPNMode;
                            tt.UpdatedBy = new Guid();
                            tt.UpdatedDate = DateTime.Now;
                            tt.UpdatedByUser = User.Identity.Name;
                            tt.ClearOverrideSettings = cEntity.ClearOverrideSettingsB;
                            con.SaveChanges();

                        }
                    }
                    else if (this.HasPermission("VAATerminalConfigCSO"))
                    {

                        using (TransactionHostEntities con = new TransactionHostEntities())
                        {
                            Terminal rModelToServer = ConvertToTerminalServer(terminal);

                            var tt = (from r in con.Terminals
                                      where r.TerminalKey == rModelToServer.TerminalKey
                                      select r).FirstOrDefault();
                            tt.VersionNo = tt.VersionNo + 1;
                            tt.BacklightSec = cEntity.BacklightSec;
                            tt.ScreenBrightness = Convert.ToInt32(c["ScreenBrightness"].ToString());// cEntity.ScreenBrightness;
                            tt.Sound = cEntity.SoundB;
                            tt.UserIdConfig = cEntity.UserIdConfig;
                            tt.BusinessIdConfig = cEntity.BusinessIdConfig;
                            tt.TNNetworkKey = cEntity.TNNetworkKey;
                            tt.TerminalNetworkKey = cEntity.TerminalNetworkKey;
                            tt.DefaultBackgroundColour = colc;// cEntity.DefaultBackgroundColour;
                            tt.ABNConfig = cEntity.ABNConfig;
                            tt.UserID = c["UserIDName"].ToString();// cEntity.UserID;
                            tt.BusinessID = c["BusinessIDName"].ToString(); //cEntity.BusinessID;
                            tt.ABN = c["ABNName"].ToString();// cEntity.ABN;
                            tt.UpdatedBy = new Guid();
                            tt.UpdatedDate = DateTime.Now;
                            tt.UpdatedByUser = User.Identity.Name;

                            con.SaveChanges();

                        }
                    }

                    if (this.HasPermission("ManageGlideboxSalesTerminalConfiguration"))
                    {
                        using (TransactionHostEntities con = new TransactionHostEntities())
                        {
                            Terminal rModelToServer = ConvertToTerminalServer(terminal);

                            var tt = (from r in con.Terminals
                                      where r.TerminalKey == rModelToServer.TerminalKey
                                      select r).FirstOrDefault();

                            //handle log history whrn user enable or disable glidebox sale in terminal
                            tda.CreateTerminalAssignHistory(con, cEntity, tt, User.Identity.Name, SourceContants.TerminalConfig);

                            tt.EnableGlidebox = cEntity.EnableGlidebox;
                            tt.UpdatedBy = new Guid();
                            tt.UpdatedDate = DateTime.Now;
                            tt.UpdatedByUser = User.Identity.Name;

                            con.SaveChanges();
                        }
                    }
                }

                var allErrors = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage));

                TerminalModel tm = new TerminalModel();
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    Terminal rModelToServer = ConvertToTerminalServer(terminal);

                    var tt = (from r in con.Terminals
                              where r.TerminalKey == rModelToServer.TerminalKey
                              select r).FirstOrDefault();
                    tm = ConvertToClient(tt);
                }
                DrawDropDownConfig();

                // return View(tm);
                // return RedirectToAction("Config", new { Key = tm.TerminalKey });
            }
            catch
            {
            }
            finally
            {
                ViewBag.Msg = "Changes saved";

            }
            return View("CommonAjax");
        }

        private void DrawDropDownConfig()
        {
            TerminalHostDA tda = new TerminalHostDA();

            var loginMode = tda.GetView_LoginModeList();
            var states = tda.GetStateList();


            ViewBag.LoginMode = loginMode.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.Code.ToString()
            }).ToList();

            ViewBag.States = states.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.StateKey.ToString()
            }).OrderBy(item => item.Text).ToList();

            //build Recording Mode value list
            List<SelectListItem> RecordingModes = new List<SelectListItem>();
            RecordingModes.Add(new SelectListItem() { Text = "No Recording", Value = "0" });
            RecordingModes.Add(new SelectListItem()
            { Text = "Recording before deep sleep", Value = "1" });
            RecordingModes.Add(new SelectListItem()
            { Text = " Recordings on idle every 'Upload Timer' minutes", Value = "2" });
            ViewBag.RecordingModes = RecordingModes;

            List<SelectListItem> printReceiptModes =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PrintReceiptMode.ToString())
                .Split('-')
                .Select(m => new SelectListItem
                {
                    Text = m.Split(',')[1],
                    Value = m.Split(',')[0]
                }).ToList();
            ViewBag.PrintReceiptModes = printReceiptModes;
        }

        [OutputCache(Duration = 1)]
        [Authorize]
        public ActionResult TerminalNetworkSelect(int Key, int Default)
        {
            TerminalHostDA tda = new TerminalHostDA();
            var terminalNetworks = tda.GetTerminalNetworksList(Key);

            ViewBag.terminalNetworks = terminalNetworks.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.TerminalNetworkKey.ToString()
            }).OrderBy(item => item.Text).ToList();

            var terminalNetwork = tda.GetTerminalNetworks(Default);

            return View(terminalNetwork);
        }

        [OutputCache(Duration = 1)]
        [Authorize]
        public ActionResult NetworkSelect(int Key, int Default)
        {
            TerminalHostDA tda = new TerminalHostDA();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                List<TerminalNetworkDetail> terminalNetworkDetails = new List<TerminalNetworkDetail>();
                var t = (from tnd in con.TerminalNetworkDetails
                         where tnd.TerminalNetworkKey.Equals(Key)
                         select tnd).ToList();

                if (t != null)
                {
                    terminalNetworkDetails = t.ToList();
                }


                terminalNetworkDetails.Add(new TerminalNetworkDetail());

                ViewBag.terminalNetworkDetails = terminalNetworkDetails.Select(item => new SelectListItem
                {
                    Text = (item.Network == null ? "" : item.Network.Name + "(" + item.Network.Prefix + ")"),
                    Value = item.TNNetworkKey.ToString()
                }).OrderBy(item => item.Text).ToList();

            }
            var terminalNetworkDetail = tda.GetTerminalNetworkDetails(Default);

            return View(terminalNetworkDetail);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalManageAllow")]
        public ActionResult Manage(int Key)
        {
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(Key).FirstOrDefault();

            DrawDropDownManage();

            return View(terminal);

        }

        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalManageAllow")]
        public ActionResult Manage(TerminalModel cEntity)
        {

            TerminalModel terminal1 = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal1 = tda.Terminal_ByKey(cEntity.TerminalKey).FirstOrDefault();
            ClientModel clientEntity = (ClientModel)terminal1;
            ModelState.Remove("UploadTimer");
            if (ModelState.IsValid)
            {
                if (this.HasPermission("VAATerminalManageAdmin"))
                {

                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        var terminal = (from t in con.Terminals
                                        where t.TerminalKey == cEntity.TerminalKey
                                        select t).FirstOrDefault();
                        terminal.VersionNo = terminal.VersionNo + 1;
                        terminal.TerminalID = cEntity.TerminalID;
                        terminal.MerchantKey = cEntity.MerchantKey;
                        terminal.TerminalTypeKey = cEntity.TerminalTypeKey;
                        terminal.AppKey = cEntity.AppKey;
                        terminal.TerminalBINRangeKey = cEntity.TerminalBINRangeKey;
                        terminal.DiscountApplied = cEntity.DiscountApplied;
                        terminal.Inactive = cEntity.Inactive;
                        if (cEntity.Inactive == false || cEntity.Inactive == null)
                        {
                            //deactive old active terminals because only one active terminal in the same time
                            tda.DisableTerminal(con, terminal.TerminalID, terminal.TerminalKey);
                        }

                        terminal.ConnectToTMS = cEntity.ConnectToTMS;
                        terminal.UpdatedBy = new Guid();
                        terminal.UpdatedDate = DateTime.Now;
                        terminal.UpdatedByUser = User.Identity.Name;

                        con.SaveChanges();
                        ViewBag.Msg = "Changes saved";
                        return View("CommonAjax");
                    }

                }
                else
                {
                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        var terminal = (from t in con.Terminals
                                        where t.TerminalKey == cEntity.TerminalKey
                                        select t).FirstOrDefault();
                        terminal.VersionNo = terminal.VersionNo + 1;
                        terminal.AppKey = cEntity.AppKey;
                        terminal.TerminalBINRangeKey = cEntity.TerminalBINRangeKey;
                        terminal.DiscountApplied = cEntity.DiscountApplied;
                        terminal.Inactive = cEntity.Inactive;
                        terminal.MerchantKey = cEntity.MerchantKey;
                        terminal.ConnectToTMS = cEntity.ConnectToTMS;

                        terminal.UpdatedBy = new Guid();
                        terminal.UpdatedDate = DateTime.Now;
                        terminal.UpdatedByUser = User.Identity.Name;

                        con.SaveChanges();
                        ViewBag.Msg = "Changes saved";
                        return View("CommonAjax");
                    }
                }
            }
            TerminalModel tm = new TerminalModel();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {
                var tt = (from r in con.Terminals
                          where r.TerminalKey == cEntity.TerminalKey
                          select r).FirstOrDefault();
                tm = ConvertToClient(tt);
            }
            DrawDropDownManage();

            return View(tm);

        }



        private void DrawDropDownManage()
        {
            List<TerminalType> terminalType = new List<TerminalType>();// LiveGroup.BusLog.GetEntity<TerminalType>().ServerAll().ToList();

            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from tt in con.TerminalTypes
                         orderby tt.Model ascending
                         select tt).ToList();

                if (t != null)
                {
                    terminalType = t;
                }
            }

            List<Merchant> merchant = new List<Merchant>();// LiveGroup.BusLog.GetEntity<Merchant>().ServerAll().ToList();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from m in con.Merchants
                         orderby m.MerchantName ascending
                         select m).ToList();

                if (t != null)
                {
                    merchant = t;
                }
            }
            List<App> app = new List<App>();// LiveGroup.BusLog.GetEntity<App>().ServerAll().ToList();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from a in con.Apps
                         orderby a.Description ascending
                         select a).ToList();

                if (t != null)
                {
                    app = t;
                }
            }
            List<TerminalBINRange> terminalBINRange = new List<TerminalBINRange>();// LiveGroup.BusLog.GetEntity<TerminalBINRange>().ServerAll().ToList();
            using (TransactionHostEntities con = new TransactionHostEntities())
            {

                var t = (from tbr in con.TerminalBINRanges
                         select tbr).ToList();

                if (t != null)
                {
                    terminalBINRange = t;
                }
            }
            terminalType.Add(new TerminalType());
            merchant.Add(new Merchant());
            app.Add(new App());
            terminalBINRange.Add(new TerminalBINRange());

            ViewBag.TerminalType = terminalType.Select(item => new SelectListItem
            {
                Text = item.Model,
                Value = item.TerminalTypeKey.ToString()
            }).OrderBy(item => item.Text).ToList();

            ViewBag.Merchant = merchant.Select(item => new SelectListItem
            {
                Text = item.MerchantID,
                Value = item.MerchantKey.ToString()
            }).OrderBy(item => item.Text).ToList();

            ViewBag.App = app.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.AppKey.ToString()
            }).OrderBy(item => item.Text).ToList();


            ViewBag.TerminalBINRange = terminalBINRange.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.TerminalBINRangeKey.ToString()
            }).OrderBy(item => item.Text).ToList();

        }



        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalSwapAllow")]
        public ActionResult Swap(int Key)
        {
            // TerminalModel terminal = getEntity.Client<TerminalModel>(Key);
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(Key).FirstOrDefault();
            SwapTerminalModel swapTerminal = new SwapTerminalModel();

            Swap(ref swapTerminal, terminal);
            if (this.HasPermission("NUVAATerminalSwapAdmin") || this.HasPermission("VAATerminalSwapOpts"))
            //if (User.IsInPermission("NUVAATerminalSwapAdmin") || ViewContext.Controller.HasPermission("VAATerminalSwapOpts"))
            {
                return View(swapTerminal);
            }

            ViewBag.Msg = "Only Administrator or Operations can Swap Terminals";
            return View("CommonAjax");
        }

        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalSwapAllow")]
        public ActionResult Swap(SwapTerminalModel cEntity)
        {
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(cEntity.TerminalKey).FirstOrDefault();

            ClientModel clientEntity = (ClientModel)terminal;
            ModelState.Remove("UploadTimer");
            if (ModelState.IsValid)
            {

                Terminal rModelToServer = ConvertToTerminalServer(terminal);
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var tt = (from t in con.Terminals
                              where t.TerminalKey == rModelToServer.TerminalKey
                              select t).FirstOrDefault();

                    Terminal Newt = new Terminal();
                    Newt = tt;
                    Newt.UpdatedBy = new Guid();
                    Newt.UpdatedDate = DateTime.Now;
                    Newt.UpdatedByUser = User.Identity.Name;
                    Newt.TerminalID = "";
                    con.Terminals.Add(Newt);
                    con.SaveChanges();
                }

                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var tt = (from t in con.Terminals
                              where t.TerminalKey == rModelToServer.TerminalKey
                              select t).FirstOrDefault();
                    tt.SerialNumber = cEntity.SwapSerialNumber;
                    tt.UpdatedBy = new Guid();
                    tt.UpdatedDate = DateTime.Now;
                    tt.UpdatedByUser = User.Identity.Name;
                    con.SaveChanges();
                }
                clientEntity = ConvertToClient(rModelToServer);
            }

            //Swap(ref cEntity, terminal);
            //return View(cEntity);

            ViewBag.Msg = "Terminal Swapped";
            return View("CommonAjax");
        }



        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalViewSettlementLogs")]
        public ActionResult SettlementLogs(int Key, string search)
        {
            List<View_SettlementLog> settlementLogs = null;
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(Key).FirstOrDefault();



            if (terminal != null)
            {
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var tra = (from t1 in con.View_SettlementLog
                               where t1.TerminalID == terminal.TerminalID
                               orderby t1.SettlementLogKey descending
                               select t1).ToList();

                    if (!String.IsNullOrEmpty(search))
                    {
                        tra = tra.Where(s => s.Batchnumber.Contains(search)).OrderBy(orderby => orderby.SettlementLogKey).ToList();
                    }

                    settlementLogs = tra;
                }
            }
            ViewBag.TID = terminal.TerminalID;

            return View(settlementLogs);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalViewSettlementLogs")]
        public JsonResult SaveBatchRecon(string tid, string batch, string merchantId)
        {
            string message = String.Empty;

            try
            {
                using (TerminalHostDA terminalHostDA = new TerminalHostDA())
                {
                    terminalHostDA.AddBatchReconRequest(new BatchReconRequestModel()
                    {
                        Batchnumber = batch,
                        CreatedByUser = User.Identity.Name,
                        MerchantID = merchantId,
                        TerminalID = tid,
                    });
                }
                message = String.Format("Successfully added your request for batch # {0} to reconcile.", batch);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalViewTransactions")]
        public ActionResult Transactions(int key)
        {
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(key).FirstOrDefault();

            TransactionDA ta = new TransactionDA();
            var cardTypeList = ta.GetCadmusinCardTypes();
            ViewBag.CardType = cardTypeList.AsSelectListItem();

            if (terminal != null)
            {
                ViewBag.terminalID = terminal.TerminalID;
            }
            TerminalTransactionLogPagingViewModel model = new TerminalTransactionLogPagingViewModel();
            model.ApproveStatusBool = true;
            model.DeclineStatusBool = false;
            return View(model);
        }

        [HttpPost]
        public JsonResult GetTransactionLogs(PhoenixObjects.Common.GridModel<SearchTransactionLogModel, List<View_Transaction>> model)
        {
            List<View_Transaction> transactions = new List<View_Transaction>();
            TerminalModel terminal = new TerminalModel();

            TerminalHostDA terminalHost = new TerminalHostDA();
            terminal = terminalHost.Terminal_ByKey(model.Request.TerminalKey).FirstOrDefault();


            if (terminal != null)
            {
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var fromDate = model.Request.StartDate.ToString("yyyy-MM-dd");
                    var endDate = model.Request.EndDate.ToString("yyyy-MM-dd");
                    int isApproved = model.Request.ApproveStatusBool ? 1 : 0;
                    int isDeclined = model.Request.ApproveStatusBool ? 1 : 0;

                    List<SqlParameter> sqlParameters = new List<SqlParameter>();

                    sqlParameters.Add(new SqlParameter("@RowIDFrom", model.PageIndexFrom));
                    sqlParameters.Add(new SqlParameter("@RowIDTo", model.PageIndexTo));
                    sqlParameters.Add(new SqlParameter("@TerminalID", terminal.TerminalID ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@FromDate", fromDate + " 00:00:00.000"));
                    sqlParameters.Add(new SqlParameter("@ToDate", endDate + " 23:59:59.000"));
                    sqlParameters.Add(new SqlParameter("@BatchNumber", model.Request.BatchNumber ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@CardName", model.Request.CardType ?? string.Empty));
                    sqlParameters.Add(new SqlParameter("@MinAmount", model.Request.MinAmount ?? -1));
                    sqlParameters.Add(new SqlParameter("@MaxAmount", model.Request.MaxAmount ?? -1));
                    sqlParameters.Add(new SqlParameter("@IsApproved", model.Request.ApproveStatusBool));
                    sqlParameters.Add(new SqlParameter("@IsDeclined", model.Request.DeclineStatusBool));
                    sqlParameters.Add(new SqlParameter("@OnlyDisplayGlideboxTransaction", model.Request.OnlyDisplayGlideboxTransaction));
                    var totalItemsParam = new SqlParameter
                    {
                        ParameterName = "@TotalItems",
                        Value = 0,
                        Direction = ParameterDirection.Output
                    };
                    sqlParameters.Add(totalItemsParam);

                    var response = con.Database.SqlQuery<View_Transaction>("EXEC GetTransactionHistoryOfTerminal " +
                        "@TerminalID, " +
                        "@FromDate, " +
                        "@ToDate, " +
                        "@BatchNumber, " +
                        "@CardName, " +
                        "@MinAmount, " +
                        "@MaxAmount, " +
                        "@IsApproved, " +
                        "@IsDeclined, " +
                        "@RowIDFrom, " +
                        "@RowIDTo, " +
                        "@TotalItems OUT, " +
                        "@OnlyDisplayGlideboxTransaction", sqlParameters.ToArray()).ToList();

                    int total = 0;
                    int.TryParse(totalItemsParam.Value.ToString(), out total);
                    int totalPages = (int)Math.Ceiling((float)total / (float)model.PageSize);

                    ViewBag.TID = terminal.TerminalID;
                    var jsonData = new
                    {
                        total = totalPages,
                        page = model.PageIndex,
                        records = total,
                        rows = response
                    };

                    return new CustomJsonResult()
                    {
                        Data = jsonData
                    };

                }
            }

            return new CustomJsonResult
            {
                Data = model
            };
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalBulkUpdate")]
        public ActionResult BulkUpdate(int Key)
        {


            //TerminalModel terminal = getEntity.Client<TerminalModel>(Key);
            TerminalModel terminal = new TerminalModel();

            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(Key).FirstOrDefault();

            DrawDropDownManage();
            if (terminal.DefaultBackgroundColour != null && terminal.DefaultBackgroundColour.Length > 0)
            {
                terminal.DefaultBackgroundColour = "#" + terminal.DefaultBackgroundColour;
            }
            if (terminal.EnableNetworkReceiptPrint == 1)
            {
                terminal.EnableNetworkReceiptPrintB = true;
            }
            else
            {
                terminal.EnableNetworkReceiptPrintB = false;
            }

            if (terminal.EnableMerchantLogoPrint == 1)
            {
                terminal.EnableMerchantLogoPrintB = true;
            }
            else
            {
                terminal.EnableMerchantLogoPrintB = false;
            }


            if (terminal.IsPreTranPing == true)
            {
                terminal.IsPreTranPingB = true;
            }
            else
            {
                terminal.IsPreTranPingB = false;
            }

            if (terminal.NetworkTableStatus == true)
            {
                terminal.NetworkTableStatusB = true;
            }
            else
            {
                terminal.NetworkTableStatusB = false;
            }
            if (terminal.EnableRefundTran == true)
            {
                terminal.EnableRefundTranB = true;
            }
            else
            {
                terminal.EnableRefundTranB = false;
            }

            if (terminal.EnableAuthTran == true)
            {
                terminal.EnableAuthTranB = true;
            }
            else
            {
                terminal.EnableAuthTranB = false;
            }
            if (terminal.EnableTipTran == true)
            {
                terminal.EnableTipTranB = true;
            }
            else
            {
                terminal.EnableTipTranB = false;
            }

            if (terminal.EnableCashOutTran == true)
            {
                terminal.EnableCashOutTranB = true;
            }
            else
            {
                terminal.EnableCashOutTranB = false;
            }
            if (terminal.Sound == true)
            {
                terminal.SoundB = true;
            }
            else
            {
                terminal.SoundB = false;
            }
            if (terminal.EnableMOTO == true)
            {
                terminal.EnableMOTOB = true;
            }
            else
            {
                terminal.EnableMOTOB = false;
            }

            string term = string.Empty;
            if (terminal.EOSTime != null)
            {
                term = terminal.EOSTime.Trim();
                string t1 = string.Empty;
                string t = term;
                string t2 = string.Empty;
                string tt = string.Empty;
                if (t.Length >= 2)
                {
                    if (t.Length == 3)
                    {
                        t1 = "0" + t.Substring(0, 1);
                        t2 = t.Substring(1, 2);
                    }
                    else if (t.Length == 2)
                    {
                        t1 = "00";
                        t2 = t.Substring(0, 2);
                    }
                    else
                    {
                        t1 = t.Substring(0, 2);
                    }

                    if (t.Length > 3)
                    {
                        t2 = t.Substring(2, 2);
                    }

                    if (t2 == null || t2 == "" || t2 == " " || t2 == string.Empty)
                    {
                        t2 = "00";
                    }
                    tt = t1 + ":" + t2;
                    terminal.EOSTimeDecimal = tt;
                    terminal.IsManualEOS = false;
                }
                else if (t.Length == 1)
                {
                    tt = "00" + ":0" + t;
                    terminal.EOSTimeDecimal = tt;
                    terminal.IsManualEOS = false;
                }
                else
                {
                    terminal.EOSTimeDecimal = "0";
                    terminal.IsManualEOS = true;
                }
            }
            else
            {
                term = null;
                terminal.IsManualEOS = true;
                terminal.EOSTimeDecimal = "0";
            }

            if (terminal.DefaultServiceFee != null)
            {
                string dt1 = string.Empty;
                string dt2 = string.Empty;
                decimal dt = terminal.DefaultServiceFee.Value;

                decimal s1 = Math.Round(dt / 100, 2);

                if (dt > 0)
                {
                    terminal.DefaultServiceFeeDecimal = s1.ToString();
                    terminal.IsDefaultServiceFeeZero = false;
                }
                else
                {
                    terminal.DefaultServiceFeeDecimal = "0.00";
                    terminal.IsDefaultServiceFeeZero = true;
                }
            }
            else
            {
                terminal.IsDefaultServiceFeeZero = true;
                terminal.DefaultServiceFeeDecimal = "0.00";
            }

            terminal.MinimumCharge = Math.Round(terminal.MinimumCharge.Value, 2);
            terminal.MaximumCharge = Math.Round(terminal.MaximumCharge.Value, 2);

            terminal.PaymentType = 0;
            if (terminal.MemberKey != 0)
            {
                OperatorDA opl = new OperatorDA();
                var op = opl.Get(terminal.MemberKey);
                if (op.PayByEFT.GetValueOrDefault(false) == true && op.Operator_key != 0)
                {
                    terminal.PaymentType = 1;
                }
            }

            DrawDropDownConfig();


            return View(terminal);


        }

        [HttpPost]
        //[Authorize]
        //[RBAC(Permission = "VAATerminalBulkUpdate")]
        public ActionResult BulkUpdate(TerminalModel cEntity, FormCollection c)
        {
            List<string> updated = new List<string>();
            List<string> updatedSuccess = new List<string>();
            List<string> hasError = new List<string>();

            TerminalModel oldterminal = new TerminalModel();

            //explicitly set the merchantkey for the terminal that we use to copy the config to avoid the case when it's null
            TerminalHostDA tda = new TerminalHostDA();
            oldterminal = tda.Terminal_ByKey(cEntity.TerminalKey).FirstOrDefault();

            string[] allTerminal = c["BulkTxt"].ToString().Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            //validate Upload Timer must greater than 1 when mode = 2
            if (cEntity.RecordingMode == 2 && cEntity.UploadTimer < 1)
            {
                ViewBag.Msg = "Upload Timer must greater than or equal to 1.";
                return View("CommonAjax");
            }

            if (oldterminal == null)
            {
                ViewBag.Msg = $"Terminal Key {cEntity.TerminalKey} does not exists.";
                return View("CommonAjax");
            }

            if (allTerminal.Count() == 0)
            {
                ViewBag.Msg = $"Please enter terminal id.";
                return View("CommonAjax");
            }
            else
            {
                allTerminal = allTerminal.Select(x => x.Trim()).ToArray();
            }

            using (TransactionHostEntities transactionHostEntities = new TransactionHostEntities())
            {
                hasError = allTerminal.Except(transactionHostEntities.Terminals.Where(x => x.TerminalID.Trim().Length == 8 && x.TerminalID.Trim() != oldterminal.TerminalID.Trim())
                                                                                .Distinct()
                                                                                .Select(x => x.TerminalID.Trim())).ToList();

                updated = allTerminal.Except(hasError).ToList();
            }

            foreach (string ter in updated)
            {
                int dt4 = 0;
                var t3 = string.Empty;

                string t = c["EOSTimeDecimalId"].ToString();
                string color = c["DefaultBackgroundColour"].ToString();
                string txtABNSetting = c["txtABNSetting"].ToString();
                decimal d = Convert.ToDecimal(txtABNSetting);
                decimal d11 = d * 100;
                int d1 = Convert.ToInt32(d11);
                string d2 = string.Empty;
                if (d1 == 0)
                {
                    d2 = "000000";
                }
                else
                {
                    if (d1 < 10)
                    {
                        d2 = "00000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 2)
                    {
                        d2 = "0000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 3)
                    {
                        d2 = "000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 4)
                    {
                        d2 = "00" + d1.ToString();
                    }
                    if (d1.ToString().Length == 5)
                    {
                        d2 = "0" + d1.ToString();
                    }
                    if (d1.ToString().Length == 6)
                    {
                        d2 = d1.ToString();
                    }
                }

                cEntity.ABNSetting = d2;
                string colc = color.Substring(1, color.Length - 1);

                if (t != "")
                {
                    var t2 = string.Empty;
                    var t1 = string.Empty;
                    if (t.Length >= 2)
                    {
                        t1 = t.Substring(0, 2);
                    }
                    if (t.Length >= 4)
                    {
                        t2 = t.Substring(3, 2);
                        t3 = t1 + t2;
                    }
                    else
                    {
                        t3 = null;
                    }
                }

                string dt = c["DefaultServiceFeeId"].ToString();
                if (dt != "")
                {
                    decimal g1 = Convert.ToDecimal(dt) * 100;
                    dt4 = Convert.ToInt32(g1);
                }


                ModelState.Remove("UploadTimer");

                if (ModelState.IsValid)
                {
                    if (cEntity.EnableMerchantLogoPrintB == true)
                    {
                        cEntity.EnableMerchantLogoPrint = 1;
                    }
                    else
                    {
                        cEntity.EnableMerchantLogoPrint = 0;
                    }
                    if (cEntity.EnableNetworkReceiptPrintB == true)
                    {
                        cEntity.EnableNetworkReceiptPrint = 1;
                    }
                    else
                    {
                        cEntity.EnableNetworkReceiptPrint = 0;
                    }
                    if (cEntity.TranAdviceSendMode != 2)
                    {
                        cEntity.IdleTimer = 0;
                        cEntity.IdleTransactions = 0;
                    }

                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        var terminal = con.Terminals.Where(x => x.TerminalID.Trim() == ter && x.Inactive != true).FirstOrDefault();

                        if (terminal != null)
                        {
                            //update data from oldterminal
                            terminal.AppKey = oldterminal.AppKey;
                            terminal.ReceiptLogoKey = oldterminal.ReceiptLogoKey;
                            terminal.PromptKey = oldterminal.PromptKey;
                            terminal.TerminalTypeKey = oldterminal.TerminalTypeKey;
                            terminal.MerchantKey = oldterminal.MerchantKey;

                            //update data from TerminalModel
                            terminal.VersionNo = terminal.VersionNo + 1;
                            terminal.InputTimeoutSec = cEntity.InputTimeoutSec;
                            terminal.BacklightSec = cEntity.BacklightSec;
                            terminal.SleepModeMin = cEntity.SleepModeMin;
                            terminal.ScreenBrightness = Convert.ToInt32(c["ScreenBrightness"].ToString());// cEntity.ScreenBrightness;
                            terminal.Sound = cEntity.SoundB;
                            terminal.RemeberMe = cEntity.RemeberMe;
                            terminal.ABNSetting = cEntity.ABNSetting;
                            terminal.MinimumCharge = Convert.ToDecimal(c["MinimumCharge"].ToString());  //cEntity.MinimumCharge;
                            terminal.MaximumCharge = Convert.ToDecimal(c["MaximumCharge"].ToString());// cEntity.MaximumCharge;
                            terminal.EOSTime = t3;// cEntity.EOSTime;
                            terminal.LoginMode = cEntity.LoginMode;
                            terminal.UserIdConfig = cEntity.UserIdConfig;
                            terminal.BusinessIdConfig = cEntity.BusinessIdConfig;
                            terminal.ABNConfig = cEntity.ABNConfig;
                            terminal.GPRSReconnectTimer = Convert.ToInt16(c["GPRSReconnectTimer"].ToString()); //cEntity.GPRSReconnectTimer;
                            terminal.TNNetworkKey = cEntity.TNNetworkKey;
                            terminal.StateKey = cEntity.StateKey;
                            terminal.TerminalNetworkKey = cEntity.TerminalNetworkKey;
                            terminal.AppRespCode = cEntity.AppRespCode;
                            terminal.AppSignRespCode = cEntity.AppSignRespCode;
                            terminal.IsPreTranPing = cEntity.IsPreTranPingB;
                            terminal.TranAdviceSendMode = cEntity.TranAdviceSendMode;
                            terminal.EnableMOTO = cEntity.EnableMOTOB;
                            terminal.DefaultBackgroundColour = colc;// cEntity.DefaultBackgroundColour;                     
                            terminal.UserID = c["UserIDName"].ToString();// cEntity.UserID;
                            terminal.BusinessID = c["BusinessIDName"].ToString(); //cEntity.BusinessID;
                            terminal.ABN = c["ABNName"].ToString();// cEntity.ABN;
                            terminal.DefaultServiceFee = dt4;// cEntity.DefaultServiceFee;
                            terminal.ServiceFeeMode = cEntity.ServiceFeeMode;
                            terminal.CustomerPaymentType = cEntity.CustomerPaymentType;
                            terminal.EnableNetworkReceiptPrint = cEntity.EnableNetworkReceiptPrint;
                            terminal.EnableMerchantLogoPrint = cEntity.EnableMerchantLogoPrint;
                            terminal.SignatureVerificationTimeout = Convert.ToInt16(c["SignatureVerificationTimeout"].ToString());// cEntity.SignatureVerificationTimeout;
                            terminal.AutoLogonHour = Convert.ToByte(c["AutoLogonHour"].ToString());// cEntity.AutoLogonHour;
                            terminal.AutoLogonMode = cEntity.AutoLogonMode;
                            terminal.EnableRefundTran = cEntity.EnableRefundTranB;
                            terminal.EnableAuthTran = cEntity.EnableAuthTranB;
                            terminal.EnableTipTran = cEntity.EnableTipTranB;
                            terminal.EnableCashOutTran = cEntity.EnableCashOutTranB;
                            terminal.MerchantPassword = c["MerchantPasswordId"].ToString();// cEntity.MerchantPassword;
                            terminal.BankLogonRespCode = cEntity.BankLogonRespCode;
                            terminal.NetworkTableStatus = cEntity.NetworkTableStatusB;
                            terminal.BusinessTypeKey = cEntity.BusinessTypeKey;
                            terminal.RebootHours = cEntity.RebootHours;
                            terminal.EnableAutoTip = cEntity.EnableAutoTip;
                            terminal.EnableMOTORefund = false; //implement ticket PHW-865: always default value = false
                            terminal.AutoEOSMode = cEntity.AutoEOSMode;
                            terminal.EnableDualAPNMode = cEntity.EnableDualAPNMode;
                            terminal.Inactive = cEntity.Inactive;
                            terminal.ConnectToTMS = cEntity.ConnectToTMS;
                            terminal.UpdatedBy = new Guid();
                            terminal.UpdatedDate = DateTime.Now;
                            terminal.UpdatedByUser = User.Identity.Name;
                            terminal.EnableReceiptReference = cEntity.EnableReceiptReference;
                            terminal.EnableQantasReward = cEntity.EnableQantasReward;
                            terminal.EnableQantasPointsRedemption = cEntity.EnableQantasPointsRedemption;
                            terminal.EnableBusinessLogo = cEntity.EnableBusinessLogo;
                            terminal.EnablePaperRollOrder = cEntity.EnablePaperRollOrder;
                            terminal.EnableSupportCallBack = cEntity.EnableSupportCallBack;
                            terminal.RefundCumulativeMax = cEntity.RefundCumulativeMax;
                            terminal.RefundTransMax = cEntity.RefundTransMax;
                            terminal.AddLevyChargeToAmount = cEntity.AddLevyChargeToAmount;
                            terminal.LevyCharge = Convert.ToInt32(cEntity.LevyCharge * 100);
                            terminal.LevyLabel = cEntity.LevyLabel;

                            terminal.IdleTimer = cEntity.IdleTimer;
                            terminal.IdleTransactions = cEntity.IdleTransactions;
                            terminal.EnableZip = cEntity.EnableZip;

                            terminal.RecordingMode = cEntity.RecordingMode;
                            terminal.UploadTimer = cEntity.RecordingMode == 2 ? cEntity.UploadTimer : 0;

                            terminal.CardNotPresentTransactionLimit = cEntity.CardNotPresentTransactionLimit;
                            terminal.CardPresentTransactionLimit = cEntity.CardPresentTransactionLimit;

                            var memberkey = tda.GetMemberKeyAllocatedInTerminal(terminal.TerminalID);
                            if (memberkey > 0)
                            {
                                OperatorDA opl = new OperatorDA();
                                var op = opl.Get(memberkey);
                                if (op.AllowCash.GetValueOrDefault(false) == true && op.Operator_key > 0)
                                {
                                    cEntity.EnablePrintSettlement = true;
                                }
                            }

                            if (this.HasPermission("ManageGlideboxSalesTerminalConfiguration"))
                            {
                                var childrenMemberKey = tda.GetMemberKeyAllocatedInTerminal(terminal.TerminalID);
                                var isAllowGlideBoxForChildren = childrenMemberKey != 0 ? tda.IsAllowAccessToGlidebox(childrenMemberKey) : true;
                                var originalEnableGlidebox = cEntity.EnableGlidebox;
                                cEntity.EnableGlidebox = isAllowGlideBoxForChildren ? cEntity.EnableGlidebox : false; //auto disable glide box if children is live eftpos

                                //handle log history when user enable or disable glidebox sale in terminal
                                tda.CreateTerminalAssignHistory(con, cEntity, terminal, User.Identity.Name, SourceContants.TerminalBulkUpdate);

                                terminal.EnableGlidebox = cEntity.EnableGlidebox;
                                cEntity.EnableGlidebox = originalEnableGlidebox;
                            }

                            con.SaveChanges();

                            updatedSuccess.Add(ter);

                            var receiptKey =
                                terminal.ReceiptKey.HasValue ? terminal.ReceiptKey.Value
                                : con.Apps.FirstOrDefault(where => where.AppKey == terminal.AppKey)?.ReceiptKey;

                            if (receiptKey != null)
                            {
                                Receipt receipt =
                                    con.Receipts.FirstOrDefault(where => where.ReceiptKey == receiptKey);

                                if (receipt.EnablePrintCustomerReceiptFirst != cEntity.EnablePrintCustomerReceiptFirst
                                    || receipt.EnablePrintSettlement != !cEntity.EnablePrintSettlement
                                    || receipt.PrintReceiptMode != cEntity.PrintReceiptMode)
                                {
                                    con.usp_UpdateReceiptData(
                                        receipt.FooterLineOne,
                                        receipt.FooterLineTwo,
                                        receipt.FooterLineThree,
                                        receipt.FooterLineFour,
                                        receipt.HeaderLineOne,
                                        receipt.HeaderLineTwo,
                                        receipt.HeaderLineThree,
                                        receipt.HeaderLineFour,
                                        cEntity.EnablePrintCustomerReceiptFirst,
                                        !cEntity.EnablePrintSettlement,
                                        cEntity.PrintReceiptMode,
                                        terminal.TerminalID);
                                }
                            }
                        }
                        else
                        {
                            hasError.Add(ter);
                        }
                    }
                }
            }

            ViewBag.InvalidOrNotUpdated = hasError.ToArray();
            ViewBag.Updated = updatedSuccess.ToArray();
            ViewBag.ActionType = "updated";
            ViewBag.Msg = "Terminals Updated";

            return View("CommonAjax");
        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalBulkAdd")]
        public ActionResult BulkAdd(int Key)
        {


            BulkAddTerminalVM bulkAddTerminalVM = new BulkAddTerminalVM();

            DrawDropDownManage();

            //TerminalModel terminal = getEntity.Client<TerminalModel>(Key);
            TerminalModel terminal = this.GetTerminal(Key);

            terminal.PaymentType = 0;
            if (terminal.MemberKey != 0)
            {
                OperatorDA opl = new OperatorDA();
                var op = opl.Get(terminal.MemberKey);
                if (op.PayByEFT.GetValueOrDefault(false) == true && op.Operator_key != 0)
                {
                    terminal.PaymentType = 1;
                }
            }
            DrawDropDownConfig();

            bulkAddTerminalVM.Terminal = terminal;

            return View(bulkAddTerminalVM);

        }


        private TerminalModel GetTerminal(int key)
        {

            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(key).FirstOrDefault();

            if (terminal.DefaultBackgroundColour != null && terminal.DefaultBackgroundColour.Length > 0)
            {
                terminal.DefaultBackgroundColour = "#" + terminal.DefaultBackgroundColour;
            }
            if (terminal.EnableNetworkReceiptPrint == 1)
            {
                terminal.EnableNetworkReceiptPrintB = true;
            }
            else
            {
                terminal.EnableNetworkReceiptPrintB = false;
            }

            if (terminal.EnableMerchantLogoPrint == 1)
            {
                terminal.EnableMerchantLogoPrintB = true;
            }
            else
            {
                terminal.EnableMerchantLogoPrintB = false;
            }


            if (terminal.IsPreTranPing == true)
            {
                terminal.IsPreTranPingB = true;
            }
            else
            {
                terminal.IsPreTranPingB = false;
            }

            if (terminal.NetworkTableStatus == true)
            {
                terminal.NetworkTableStatusB = true;
            }
            else
            {
                terminal.NetworkTableStatusB = false;
            }
            if (terminal.EnableRefundTran == true)
            {
                terminal.EnableRefundTranB = true;
            }
            else
            {
                terminal.EnableRefundTranB = false;
            }

            if (terminal.EnableAuthTran == true)
            {
                terminal.EnableAuthTranB = true;
            }
            else
            {
                terminal.EnableAuthTranB = false;
            }
            if (terminal.EnableTipTran == true)
            {
                terminal.EnableTipTranB = true;
            }
            else
            {
                terminal.EnableTipTranB = false;
            }

            if (terminal.EnableCashOutTran == true)
            {
                terminal.EnableCashOutTranB = true;
            }
            else
            {
                terminal.EnableCashOutTranB = false;
            }
            if (terminal.Sound == true)
            {
                terminal.SoundB = true;
            }
            else
            {
                terminal.SoundB = false;
            }
            if (terminal.EnableMOTO == true)
            {
                terminal.EnableMOTOB = true;
            }
            else
            {
                terminal.EnableMOTOB = false;
            }

            string term = string.Empty;
            if (terminal.EOSTime != null)
            {
                term = terminal.EOSTime.Trim();
                string t1 = string.Empty;
                string t = term;
                string t2 = string.Empty;
                string tt = string.Empty;
                if (t.Length >= 2)
                {
                    if (t.Length == 3)
                    {
                        t1 = "0" + t.Substring(0, 1);
                        t2 = t.Substring(1, 2);
                    }
                    else if (t.Length == 2)
                    {
                        t1 = "00";
                        t2 = t.Substring(0, 2);
                    }
                    else
                    {
                        t1 = t.Substring(0, 2);
                    }

                    if (t.Length > 3)
                    {
                        t2 = t.Substring(2, 2);
                    }

                    if (t2 == null || t2 == "" || t2 == " " || t2 == string.Empty)
                    {
                        t2 = "00";
                    }
                    tt = t1 + ":" + t2;
                    terminal.EOSTimeDecimal = tt;
                    terminal.IsManualEOS = false;
                }
                else if (t.Length == 1)
                {
                    tt = "00" + ":0" + t;
                    terminal.EOSTimeDecimal = tt;
                    terminal.IsManualEOS = false;
                }
                else
                {
                    terminal.EOSTimeDecimal = "0";
                    terminal.IsManualEOS = true;
                }
            }
            else
            {
                term = null;
                terminal.IsManualEOS = true;
                terminal.EOSTimeDecimal = "0";
            }

            if (terminal.DefaultServiceFee != null)
            {
                string dt1 = string.Empty;
                string dt2 = string.Empty;
                decimal dt = terminal.DefaultServiceFee.Value;

                decimal s1 = Math.Round(dt / 100, 2);

                if (dt > 0)
                {
                    terminal.DefaultServiceFeeDecimal = s1.ToString();
                    terminal.IsDefaultServiceFeeZero = false;
                }
                else
                {
                    terminal.DefaultServiceFeeDecimal = "0.00";
                    terminal.IsDefaultServiceFeeZero = true;
                }
            }
            else
            {
                terminal.IsDefaultServiceFeeZero = true;
                terminal.DefaultServiceFeeDecimal = "0.00";
            }

            terminal.MinimumCharge = Math.Round(terminal.MinimumCharge.Value, 2);
            terminal.MaximumCharge = Math.Round(terminal.MaximumCharge.Value, 2);

            return terminal;
        }

        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [Authorize]
        [RBAC(Permission = "VAATerminalBulkAdd")]
        [ValidateInput(false)]
        public ActionResult BulkAdd(BulkAddTerminalVM cEntity, FormCollection c)
        {
            int OriginalTerId = cEntity.Terminal.TerminalKey;
            bool isEnableGlideBox = cEntity.Terminal.EnableGlidebox;

            List<string> updated = new List<string>();
            List<string> updatedSuccess = new List<string>();
            List<string> hasError = new List<string>();

            TerminalHostDA tda = new TerminalHostDA();
            TerminalModel oldterminal = new TerminalModel();
            oldterminal = tda.Terminal_ByKey(OriginalTerId).FirstOrDefault();

            if (String.IsNullOrEmpty(cEntity.Terminal.TerminalID))
            {
                cEntity.Terminal = this.GetTerminal(cEntity.Terminal.TerminalKey);
            }

            cEntity.Terminal.MerchantKey = oldterminal.MerchantKey;//explicitly set the merchantkey for the terminal that we use to copy the config to avoid the case when it's null
            cEntity.Terminal.EnableGlidebox = isEnableGlideBox;

            string[] allTerminal = cEntity.TerminalId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            //validate Upload Timer must greater than 1 when mode = 2
            if (cEntity.Terminal.RecordingMode == 2 && cEntity.Terminal.UploadTimer < 1)
            {
                ViewBag.Msg = "Upload Timer must greater than or equal to 1.";
                return View("CommonAjax");
            }

            if (oldterminal == null)
            {
                ViewBag.Msg = $"Terminal Key {cEntity.Terminal.TerminalID} does not exists.";
                return View("CommonAjax");
            }

            if (allTerminal.Count() == 0)
            {
                ViewBag.Msg = $"Please enter terminal id.";
                return View("CommonAjax");
            }
            else
            {
                allTerminal = allTerminal.Select(x => x.Trim()).ToArray();
            }

            using (TransactionHostEntities transactionHostEntities = new TransactionHostEntities())
            {
                updated = allTerminal.Where(x => x.Length == 8).Except(transactionHostEntities.Terminals.Where(x => x.TerminalID.Trim().Length == 8).Distinct().Select(x => x.TerminalID.Trim())).ToList();
                hasError = allTerminal.Except(updated).ToList();
            }

            foreach (string ter in updated)
            {
                int dt4 = 0;
                var t3 = string.Empty;
                string t = c["EOSTimeDecimalId"].ToString();
                string color = c["DefaultBackgroundColour"].ToString();
                string txtABNSetting = c["txtABNSetting"].ToString();
                decimal d = Convert.ToDecimal(txtABNSetting);
                decimal d11 = d * 100;
                int d1 = Convert.ToInt32(d11);
                string d2 = string.Empty;
                if (d1 == 0)
                {
                    d2 = "000000";
                }
                else
                {
                    if (d1 < 10)
                    {
                        d2 = "00000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 2)
                    {
                        d2 = "0000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 3)
                    {
                        d2 = "000" + d1.ToString();
                    }
                    if (d1.ToString().Length == 4)
                    {
                        d2 = "00" + d1.ToString();
                    }
                    if (d1.ToString().Length == 5)
                    {
                        d2 = "0" + d1.ToString();
                    }
                    if (d1.ToString().Length == 6)
                    {
                        d2 = d1.ToString();
                    }
                }

                cEntity.Terminal.ABNSetting = d2;
                string colc = color.Substring(1, color.Length - 1);

                if (t != "")
                {
                    var t2 = string.Empty;
                    var t1 = string.Empty;
                    if (t.Length >= 2)
                    {
                        t1 = t.Substring(0, 2);
                    }
                    if (t.Length >= 4)
                    {
                        t2 = t.Substring(3, 2);
                        t3 = t1 + t2;
                    }
                    else
                    {
                        t3 = null;
                    }
                }

                string dt = c["DefaultServiceFeeId"].ToString();
                if (dt != "")
                {
                    decimal g1 = Convert.ToDecimal(dt) * 100;
                    dt4 = Convert.ToInt32(g1);
                }

                ModelState.Remove("Terminal.UploadTimer");

                if (ModelState.IsValid)
                {
                    if (cEntity.Terminal.EnableMerchantLogoPrintB == true)
                    {
                        cEntity.Terminal.EnableMerchantLogoPrint = 1;
                    }
                    else
                    {
                        cEntity.Terminal.EnableMerchantLogoPrint = 0;
                    }
                    if (cEntity.Terminal.EnableNetworkReceiptPrintB == true)
                    {
                        cEntity.Terminal.EnableNetworkReceiptPrint = 1;
                    }
                    else
                    {
                        cEntity.Terminal.EnableNetworkReceiptPrint = 0;
                    }

                    if (cEntity.Terminal.TranAdviceSendMode != 2)
                    {
                        cEntity.Terminal.IdleTimer = 0;
                        cEntity.Terminal.IdleTransactions = 0;
                    }

                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        Terminal terminal = new Terminal();
                        terminal.VersionNo = terminal.VersionNo + 1;

                        terminal.MerchantKey = cEntity.Terminal.MerchantKey;
                        terminal.AppKey = cEntity.Terminal.AppKey;
                        terminal.VAAVersion = cEntity.Terminal.VAAVersion;
                        terminal.OSVersion = cEntity.Terminal.OSVersion;
                        //terminal.TNNetworkKey = cEntity.Terminal.TNNetworkKey;

                        terminal.TerminalID = ter.Trim();
                        terminal.TerminalKey = 0;
                        terminal.SerialNumber = "";
                        terminal.InputTimeoutSec = cEntity.Terminal.InputTimeoutSec;
                        terminal.BacklightSec = cEntity.Terminal.BacklightSec;
                        terminal.SleepModeMin = cEntity.Terminal.SleepModeMin;
                        terminal.ScreenBrightness = Convert.ToInt32(c["ScreenBrightness"].ToString());// cEntity.Terminal.ScreenBrightness;
                        terminal.Sound = cEntity.Terminal.SoundB;
                        terminal.RemeberMe = cEntity.Terminal.RemeberMe;
                        terminal.ABNSetting = cEntity.Terminal.ABNSetting;
                        terminal.MinimumCharge = Convert.ToDecimal(c["MinimumCharge"].ToString());  //cEntity.Terminal.MinimumCharge;
                        terminal.MaximumCharge = Convert.ToDecimal(c["MaximumCharge"].ToString());// cEntity.Terminal.MaximumCharge;
                        terminal.EOSTime = t3;// cEntity.Terminal.EOSTime;
                        terminal.LoginMode = cEntity.Terminal.LoginMode;
                        terminal.UserIdConfig = cEntity.Terminal.UserIdConfig;
                        terminal.BusinessIdConfig = cEntity.Terminal.BusinessIdConfig;
                        terminal.ABNConfig = cEntity.Terminal.ABNConfig;
                        terminal.GPRSReconnectTimer = Convert.ToInt16(c["GPRSReconnectTimer"].ToString()); //cEntity.Terminal.GPRSReconnectTimer;
                        terminal.TNNetworkKey = cEntity.Terminal.TNNetworkKey;
                        terminal.StateKey = cEntity.Terminal.StateKey;
                        terminal.TerminalNetworkKey = cEntity.Terminal.TerminalNetworkKey;
                        terminal.AppRespCode = cEntity.Terminal.AppRespCode;
                        terminal.AppSignRespCode = cEntity.Terminal.AppSignRespCode;
                        terminal.IsPreTranPing = cEntity.Terminal.IsPreTranPingB;
                        terminal.TranAdviceSendMode = cEntity.Terminal.TranAdviceSendMode;
                        terminal.EnableMOTO = cEntity.Terminal.EnableMOTOB;
                        terminal.DefaultBackgroundColour = colc;// cEntity.Terminal.DefaultBackgroundColour;
                        terminal.UpdatedByUser = User.Identity.Name;
                        terminal.UpdatedDate = DateTime.Now;
                        terminal.UserID = c["UserIDName"].ToString();// cEntity.Terminal.UserID;
                        terminal.BusinessID = c["BusinessIDName"].ToString(); //cEntity.Terminal.BusinessID;
                        terminal.ABN = c["ABNName"].ToString();// cEntity.Terminal.ABN;
                        terminal.DefaultServiceFee = dt4;// cEntity.Terminal.DefaultServiceFee;
                        terminal.ServiceFeeMode = cEntity.Terminal.ServiceFeeMode;
                        terminal.CustomerPaymentType = cEntity.Terminal.CustomerPaymentType;
                        terminal.EnableNetworkReceiptPrint = cEntity.Terminal.EnableNetworkReceiptPrint;
                        terminal.EnableMerchantLogoPrint = cEntity.Terminal.EnableMerchantLogoPrint;
                        terminal.SignatureVerificationTimeout = Convert.ToInt16(c["SignatureVerificationTimeout"].ToString());// cEntity.Terminal.SignatureVerificationTimeout;
                        terminal.AutoLogonHour = Convert.ToByte(c["AutoLogonHour"].ToString());// cEntity.Terminal.AutoLogonHour;
                        terminal.AutoLogonMode = cEntity.Terminal.AutoLogonMode;
                        terminal.EnableRefundTran = cEntity.Terminal.EnableRefundTranB;
                        terminal.EnableAuthTran = cEntity.Terminal.EnableAuthTranB;
                        terminal.EnableTipTran = cEntity.Terminal.EnableTipTranB;
                        terminal.EnableCashOutTran = cEntity.Terminal.EnableCashOutTranB;
                        terminal.MerchantPassword = c["MerchantPasswordId"].ToString();// cEntity.Terminal.MerchantPassword;
                        terminal.BankLogonRespCode = cEntity.Terminal.BankLogonRespCode;
                        terminal.NetworkTableStatus = cEntity.Terminal.NetworkTableStatusB;
                        terminal.BusinessTypeKey = cEntity.Terminal.BusinessTypeKey;
                        terminal.RebootHours = cEntity.Terminal.RebootHours;
                        terminal.EnableAutoTip = cEntity.Terminal.EnableAutoTip;
                        terminal.EnableMOTORefund = false; //implement ticket PHW-865: always default value = false
                        terminal.AutoEOSMode = cEntity.Terminal.AutoEOSMode;
                        terminal.EnableDualAPNMode = cEntity.Terminal.EnableDualAPNMode;

                        terminal.MerchantKey = cEntity.Terminal.MerchantKey;
                        terminal.TerminalTypeKey = cEntity.Terminal.TerminalTypeKey;
                        terminal.AppKey = cEntity.Terminal.AppKey;
                        terminal.Inactive = cEntity.Terminal.Inactive;
                        terminal.ConnectToTMS = cEntity.Terminal.ConnectToTMS;
                        terminal.CreatedDate = DateTime.Now;
                        terminal.CreatedBy = new Guid();
                        terminal.CreatedByUser = User.Identity.Name;

                        terminal.EnableReceiptReference = cEntity.Terminal.EnableReceiptReference;
                        terminal.EnableQantasReward = cEntity.Terminal.EnableQantasReward;
                        terminal.EnableQantasPointsRedemption = cEntity.Terminal.EnableQantasPointsRedemption;
                        terminal.EnableBusinessLogo = cEntity.Terminal.EnableBusinessLogo;
                        terminal.EnablePaperRollOrder = cEntity.Terminal.EnablePaperRollOrder;
                        terminal.EnableSupportCallBack = cEntity.Terminal.EnableSupportCallBack;
                        Int32.TryParse(c["Terminal.RefundCumulativeMax"], out var refundCumulativeMax);
                        terminal.RefundCumulativeMax = refundCumulativeMax;
                        Int32.TryParse(c["Terminal.RefundTransMax"], out var refundTransMax);
                        terminal.RefundTransMax = refundTransMax;
                        terminal.EnableZip = cEntity.Terminal.EnableZip;

                        terminal.AddLevyChargeToAmount = cEntity.Terminal.AddLevyChargeToAmount;
                        terminal.LevyCharge = Convert.ToInt32(cEntity.Terminal.LevyCharge * 100);
                        terminal.LevyLabel = cEntity.Terminal.LevyLabel;

                        terminal.CustomFooter = cEntity.Terminal.CustomFooter;
                        terminal.CustomFooterHtml = cEntity.Terminal.CustomFooterHtml;

                        terminal.IdleTimer = cEntity.Terminal.IdleTimer;
                        terminal.IdleTransactions = cEntity.Terminal.IdleTransactions;

                        terminal.RecordingMode = cEntity.Terminal.RecordingMode;
                        terminal.UploadTimer = cEntity.Terminal.RecordingMode == 2 ? cEntity.Terminal.UploadTimer : 0;
                        terminal.EnableGlidebox = false;//set default value for only create history in case enable
                        terminal.CardNotPresentTransactionLimit = cEntity.Terminal.CardNotPresentTransactionLimit;
                        terminal.CardPresentTransactionLimit = cEntity.Terminal.CardPresentTransactionLimit;

                        var memberkey = tda.GetMemberKeyAllocatedInTerminal(terminal.TerminalID);
                        if (memberkey > 0)
                        {
                            OperatorDA opl = new OperatorDA();
                            var op = opl.Get(memberkey);
                            if (op.AllowCash.GetValueOrDefault(false) == true && op.Operator_key > 0)
                            {
                                cEntity.Terminal.EnablePrintSettlement = true;
                            }
                        }

                        try
                        {
                            if (this.HasPermission("ManageGlideboxSalesTerminalConfiguration"))
                            {
                                //handle log history when user enable glidebox sale in terminal
                                tda.CreateTerminalAssignHistory(con, cEntity.Terminal, terminal, User.Identity.Name, SourceContants.TerminalBulkAdd);

                                terminal.EnableGlidebox = cEntity.Terminal.EnableGlidebox;
                            }

                            con.Terminals.Add(terminal);
                            con.SaveChanges();

                            updatedSuccess.Add(ter);

                            var receiptKey =
                                terminal.ReceiptKey.HasValue ? terminal.ReceiptKey.Value
                                : con.Apps.FirstOrDefault(where => where.AppKey == terminal.AppKey)?.ReceiptKey;

                            if (receiptKey != null)
                            {
                                Receipt receipt =
                                    con.Receipts.FirstOrDefault(where => where.ReceiptKey == receiptKey);

                                if (receipt.HeaderLineOne != cEntity.Terminal.TerminalHeaderFooter.HeaderLineOne
                                    || receipt.HeaderLineTwo != cEntity.Terminal.TerminalHeaderFooter.HeaderLineTwo
                                    || receipt.HeaderLineThree != cEntity.Terminal.TerminalHeaderFooter.HeaderLineThree
                                    || receipt.HeaderLineFour != cEntity.Terminal.TerminalHeaderFooter.HeaderLineFour
                                    || receipt.FooterLineOne != cEntity.Terminal.TerminalHeaderFooter.FooterLineOne
                                    || receipt.FooterLineTwo != cEntity.Terminal.TerminalHeaderFooter.FooterLineTwo
                                    || receipt.FooterLineThree != cEntity.Terminal.TerminalHeaderFooter.FooterLineThree
                                    || receipt.FooterLineFour != cEntity.Terminal.TerminalHeaderFooter.FooterLineFour
                                    || receipt.EnablePrintCustomerReceiptFirst != cEntity.Terminal.EnablePrintCustomerReceiptFirst
                                    || receipt.EnablePrintSettlement != (!cEntity.Terminal.EnablePrintSettlement)
                                    || receipt.PrintReceiptMode != cEntity.Terminal.PrintReceiptMode

                                    )
                                {
                                    con.usp_UpdateReceiptData(
                                        cEntity.Terminal.TerminalHeaderFooter.FooterLineOne,
                                        cEntity.Terminal.TerminalHeaderFooter.FooterLineTwo,
                                        cEntity.Terminal.TerminalHeaderFooter.FooterLineThree,
                                        cEntity.Terminal.TerminalHeaderFooter.FooterLineFour,
                                        cEntity.Terminal.TerminalHeaderFooter.HeaderLineOne,
                                        cEntity.Terminal.TerminalHeaderFooter.HeaderLineTwo,
                                        cEntity.Terminal.TerminalHeaderFooter.HeaderLineThree,
                                        cEntity.Terminal.TerminalHeaderFooter.HeaderLineFour,
                                        cEntity.Terminal.EnablePrintCustomerReceiptFirst,
                                        !cEntity.Terminal.EnablePrintSettlement,
                                        cEntity.Terminal.PrintReceiptMode,
                                        terminal.TerminalID);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            hasError.Add(ter);
                        }
                    }
                }
            }

            DrawDropDownManage();
            DrawDropDownConfig();

            cEntity.InvalidOrNotUpdated = hasError.ToArray();
            cEntity.Updated = updatedSuccess.ToArray();

            ViewBag.InvalidOrNotUpdated = hasError.ToArray();
            ViewBag.Updated = updatedSuccess.ToArray();
            ViewBag.ActionType = "created";
            ViewBag.Msg = "Terminals Added";
            return View("CommonAjax");
        }



        [OutputCache(Duration = 1)]
        [Authorize]
        [RBAC(Permission = "VAATerminalDiffSurcharge")]
        public ActionResult DiffSurchargeConfirm(int key)
        {


            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            terminal = tda.Terminal_ByKey(key).FirstOrDefault();
            if (terminal.EnableAutoTip == true)
            {
                return RedirectToAction("DiffSurcharge", new { key = key });
            }
            else
            {
                DiffSurChargeModel model = new DiffSurChargeModel();
                model.Terminal = tda.Terminal_ByKey(key).FirstOrDefault();
                if (model.Terminal.EnableAutoTip == true)
                {
                    model.EnableAutoTip = "Enable";
                }
                else
                {
                    model.EnableAutoTip = "Disable";
                }
                DrawDropDownDiffSurchargeConfig();

                //return View("DiffSurchargeConfirm", model);
                return View("DiffSurcharge", model);
            }

            // return RedirectToAction("Index");

        }
        [OutputCache(Duration = 1)]
        [Authorize]
        [HttpPost]
        [RBAC(Permission = "VAATerminalDiffSurcharge")]
        public ActionResult DiffSurchargeConfirm(DiffSurChargeModel model)
        {


            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            model.Terminal = tda.Terminal_ByKey(model.Terminal.TerminalKey).FirstOrDefault();

            bool enable = false;
            if (model.EnableAutoTip == "Enable")
            {
                enable = true;
            }

            TerminalLogoDA tda1 = new TerminalLogoDA();
            tda1.EnableAutoTip(model.Terminal.TerminalKey, enable);
            if (model.EnableAutoTip == "Enable")
            {
                return RedirectToAction("DiffSurcharge", new { key = model.Terminal.TerminalKey });
            }
            else
            {
                ViewBag.Msg = "AutoTip is disable";
                DrawDropDownDiffSurchargeConfig();
                //return View("DiffSurchargeConfirm", model);
                return View("DiffSurcharge", model);
            }



            //   return RedirectToAction("Index");

        }

        [OutputCache(Duration = 1)]
        [Authorize]
        [RBAC(Permission = "VAATerminalDiffSurcharge")]
        public ActionResult DiffSurcharge(int key)
        {

            DiffSurChargeModel model = new DiffSurChargeModel();
            TerminalModel terminal = new TerminalModel();
            TerminalHostDA tda = new TerminalHostDA();
            model.Terminal = tda.Terminal_ByKey(key).FirstOrDefault();

            PromptModel propm = new PromptModel();
            TerminalLogoDA tda1 = new TerminalLogoDA();
            propm = tda1.GetTerminalPrompt(key);

            if (propm.AutoTipEOSReportOption == "D")
            {
                model.AutoTipEOSReportOption = "D";
            }
            else if (propm.AutoTipEOSReportOption == "G")
            {
                model.AutoTipEOSReportOption = "G";
            }
            else if (propm.AutoTipEOSReportOption == "S")
            {
                model.AutoTipEOSReportOption = "S";
            }


            if (propm.AutoTipDisplayOption == "A")
            {
                model.AutoTipDisplayOption = "A";
            }
            else if (propm.AutoTipDisplayOption == "B")
            {
                model.AutoTipDisplayOption = "B";
            }
            else if (propm.AutoTipDisplayOption == "P")
            {
                model.AutoTipDisplayOption = "P";
            }


            model.AutoTipTitle = propm.AutoTipTitle;
            model.AutoTipPriorAmountLabel = propm.AutoTipPriorAmountLabel;
            model.AutoTipEOSReportMessgae = propm.AutoTipEOSReportMessage;
            model.AutoTipAountLabel = propm.AutoTipAmountLabel;

            if (propm.EnableAutoTipAmountDisplay == true)
            {
                model.EnableAutoTipAmountDisplay = "true";
            }
            else
            {
                model.EnableAutoTipAmountDisplay = "false";
            }



            if (propm.EnableAutoTipTotalDisplay == true)
            {
                model.EnablrAutoTipTotalDisplay = "true";
            }
            else
            {
                model.EnablrAutoTipTotalDisplay = "false";
            }


            if (propm.AutoTipEnterConfirmation == true)
            {
                model.AutoTipEnterConfirmation = "true";
            }
            else
            {
                model.AutoTipEnterConfirmation = "false";
            }


            //if (propm.AutoTipFeeIncluded == true)
            //{
            //    model.AutoTipFeeIncluded = "true";
            //}
            //else
            //{
            //    model.AutoTipFeeIncluded = "false";
            //}


            var one = propm.AutoTipIsPercentOne.ToString();
            var two = propm.AutoTipIsPercentTwo.ToString();
            var three = propm.AutoTipIsPercentThree.ToString();
            var four = propm.AutoTipIsPercentFour.ToString();
            if (model.Terminal.EnableAutoTip == true)
            {
                model.EnableAutoTip = "Enable";
            }
            else
            {
                model.EnableAutoTip = "Disable";
            }

            if (one == "True")
            {
                model.Type1 = "Percent";
            }
            else
            {
                model.Type1 = "Fixed";
            }
            if (two == "True")
            {
                model.Type2 = "Percent";
            }
            else
            {
                model.Type2 = "Fixed";
            }
            if (three == "True")
            {
                model.Type3 = "Percent";
            }
            else
            {
                model.Type3 = "Fixed";
            }
            if (four == "True")
            {
                model.Type4 = "Percent";
            }
            else
            {
                model.Type4 = "Fixed";
            }

            //model.Type2 = model.Terminal.AutoTipIsPercentTwo.ToString();
            //model.Type3 = model.Terminal.AutoTipIsPercentThree.ToString();
            //model.Type4 = model.Terminal.AutoTipIsPercentFour.ToString();

            if (propm.AutoTipNumOne != null)
            {
                model.AutoTip1 = (propm.AutoTipNumOne.Value);
            }
            if (propm.AutoTipNumTwo != null)
            {
                model.AutoTip2 = (propm.AutoTipNumTwo.Value);
            }
            if (propm.AutoTipNumThree != null)
            {
                model.AutoTip3 = (propm.AutoTipNumThree.Value);
            }
            if (propm.AutoTipNumFour != null)
            {
                model.AutoTip4 = (propm.AutoTipNumFour.Value);
            }

            if (model.Type1 == "Percent")
            {
                model.AutoTip1s = (model.AutoTip1 / 100).ToString() + "%";
            }
            else
            {
                model.AutoTip1s = "$ " + (model.AutoTip1 / 100).ToString();
            }
            if (model.Type2 == "Percent")
            {
                model.AutoTip2s = (model.AutoTip2 / 100).ToString() + "%";
            }
            else
            {
                model.AutoTip2s = "$ " + (model.AutoTip2 / 100).ToString();
            }
            if (model.Type3 == "Percent")
            {
                model.AutoTip3s = (model.AutoTip3 / 100).ToString() + "%";
            }
            else
            {
                model.AutoTip3s = "$ " + (model.AutoTip3 / 100).ToString();
            }
            if (model.Type4 == "Percent")
            {
                model.AutoTip4s = (model.AutoTip4 / 100).ToString() + "%";
            }
            else
            {
                model.AutoTip4s = "$ " + (model.AutoTip4 / 100).ToString();
            }

            model.AutoTip1ss = (model.AutoTip1 / 100).ToString();
            model.AutoTip2ss = (model.AutoTip2 / 100).ToString();
            model.AutoTip3ss = (model.AutoTip3 / 100).ToString();
            model.AutoTip4ss = (model.AutoTip4 / 100).ToString();


            model.AutoCardType1 = propm.AutoTipButtonOneID.ToString();
            model.AutoCardType2 = propm.AutoTipButtonTwoID.ToString();
            model.AutoCardType3 = propm.AutoTipButtonThreeID.ToString();
            model.AutoCardType4 = propm.AutoTipButtonFourID.ToString();
            model.AutoTipDefaultButtonID = propm.AutoTipDefaultButtonID;



            //Screen Text
            model.AutoTipScreenText = propm.AutoTipScreenText;

            model.OptionTwo = PopulateDiffChargeOptionTwoModel(model, propm);


            DrawDropDownDiffSurchargeConfig();
            return View(model);

        }


        [OutputCache(Duration = 1)]
        [Authorize]
        [HttpPost]
        [RBAC(Permission = "VAATerminalDiffSurcharge")]
        public ActionResult DiffSurcharge(DiffSurChargeModel model, FormCollection c)
        {
            TerminalHostDA tda1 = new TerminalHostDA();
            model.Terminal = tda1.Terminal_ByKey(model.Terminal.TerminalKey).FirstOrDefault();


            if (model.EnableAutoTip != "Enable")
            {
                bool enable = false;
                TerminalLogoDA tda = new TerminalLogoDA();
                tda.EnableAutoTip(model.Terminal.TerminalKey, enable);
                ViewBag.Msg = "AutoTip is disabled";
                DrawDropDownDiffSurchargeConfig();
            }
            else
            {
                TerminalLogoDA terminalLogoDA = new TerminalLogoDA();
                if (!model.Terminal.EnableAutoTip)
                {
                    terminalLogoDA.EnableAutoTip(model.Terminal.TerminalKey, true);
                    ViewBag.Msg = "AutoTip is enabled";
                    DrawDropDownDiffSurchargeConfig();
                }
                else
                {
                    int AutoTipDefaultButtonID = model.AutoTipDefaultButtonID.GetValueOrDefault();
                    if (c["optionsRadios"] != null)
                    {
                        AutoTipDefaultButtonID = Convert.ToInt32(c["optionsRadios"]?.ToString());
                    }

                    bool AutoTrip1TypeBool = model.Type1 == "Fixed" ? false : true;
                    bool AutoTrip2TypeBool = model.Type2 == "Fixed" ? false : true;
                    bool AutoTrip3TypeBool = model.Type3 == "Fixed" ? false : true;
                    bool AutoTrip4TypeBool = model.Type4 == "Fixed" ? false : true;

                    int AutoTrip1 = 0;
                    if (!string.IsNullOrEmpty(c["AutoTrip1"]))
                    {
                        decimal autotipdec = 0.0m;
                        Decimal.TryParse(c["AutoTrip1"].ToString(), out autotipdec);
                        AutoTrip1 = Convert.ToInt32(autotipdec * 100);
                    }

                    int AutoTrip2 = 0;
                    if (!string.IsNullOrEmpty(c["AutoTrip2"]))
                    {
                        decimal autotipdec = 0.0m;
                        Decimal.TryParse(c["AutoTrip2"].ToString(), out autotipdec);
                        AutoTrip2 = Convert.ToInt32(autotipdec * 100);
                    }
                    int AutoTrip3 = 0;
                    if (!string.IsNullOrEmpty(c["AutoTrip3"]))
                    {
                        decimal autotipdec = 0.0m;
                        Decimal.TryParse(c["AutoTrip3"].ToString(), out autotipdec);
                        AutoTrip3 = Convert.ToInt32(autotipdec * 100);
                    }
                    int AutoTrip4 = 0;
                    if (!string.IsNullOrEmpty(c["AutoTrip4"]))
                    {
                        decimal autotipdec = 0.0m;
                        Decimal.TryParse(c["AutoTrip4"].ToString(), out autotipdec);
                        AutoTrip4 = Convert.ToInt32(autotipdec * 100);
                    }

                    var AutoTrip1CardType = model.AutoCardType1;
                    var AutoTrip2CardType = model.AutoCardType2;
                    var AutoTrip3CardType = model.AutoCardType3;
                    var AutoTrip4CardType = model.AutoCardType4;

                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        PromptModel propm = new PromptModel();
                        //TerminalLogoDA tda = new TerminalLogoDA();
                        propm = terminalLogoDA.GetTerminalPrompt(model.Terminal.TerminalKey);

                        var terminal = (from r in con.Terminals
                                        where r.TerminalKey == model.Terminal.TerminalKey
                                        select r).FirstOrDefault();
                        terminal.VersionNo = terminal.VersionNo + 1;

                        terminal.AutoTipNumOne = propm.AutoTipNumOne != AutoTrip1 ? AutoTrip1 : terminal.AutoTipNumOne;
                        terminal.AutoTipNumTwo = propm.AutoTipNumTwo != AutoTrip2 ? AutoTrip2 : terminal.AutoTipNumTwo;
                        terminal.AutoTipNumThree = propm.AutoTipNumThree != AutoTrip3 ? AutoTrip3 : terminal.AutoTipNumThree;
                        terminal.AutoTipNumFour = propm.AutoTipNumFour != AutoTrip4 ? AutoTrip4 : terminal.AutoTipNumFour;

                        //******************************

                        terminal.AutoTipIsPercentOne = propm.AutoTipIsPercentOne != AutoTrip1TypeBool ? AutoTrip1TypeBool : terminal.AutoTipIsPercentOne;
                        terminal.AutoTipIsPercentTwo = propm.AutoTipIsPercentTwo != AutoTrip2TypeBool ? AutoTrip2TypeBool : terminal.AutoTipIsPercentTwo;
                        terminal.AutoTipIsPercentThree = propm.AutoTipIsPercentThree != AutoTrip3TypeBool ? AutoTrip3TypeBool : terminal.AutoTipIsPercentThree;
                        terminal.AutoTipIsPercentFour = propm.AutoTipIsPercentFour != AutoTrip4TypeBool ? AutoTrip4TypeBool : terminal.AutoTipIsPercentFour;

                        //****************

                        terminal.AutoTipButtonOneID = propm.AutoTipButtonOneID != Convert.ToInt32(AutoTrip1CardType) ? Convert.ToInt32(AutoTrip1CardType) : terminal.AutoTipButtonOneID;
                        terminal.AutoTipButtonTwoID = propm.AutoTipButtonTwoID != Convert.ToInt32(AutoTrip2CardType) ? Convert.ToInt32(AutoTrip2CardType) : terminal.AutoTipButtonTwoID;
                        terminal.AutoTipButtonThreeID = propm.AutoTipButtonThreeID != Convert.ToInt32(AutoTrip3CardType) ? Convert.ToInt32(AutoTrip3CardType) : terminal.AutoTipButtonThreeID;
                        terminal.AutoTipButtonFourID = propm.AutoTipButtonFourID != Convert.ToInt32(AutoTrip4CardType) ? Convert.ToInt32(AutoTrip4CardType) : terminal.AutoTipButtonFourID;
                        terminal.AutoTipDefaultButtonID = model.AutoTipDefaultButtonID;

                        terminal.UpdatedBy = new Guid();
                        terminal.UpdatedDate = DateTime.Now;
                        terminal.UpdatedByUser = User.Identity.Name;
                        terminal.VersionNo = terminal.VersionNo + 1;
                        con.SaveChanges();
                    }

                    //******************************
                    //Vu ask not to update prompt and Apps table just directly save on the terminal table
                    //******************************
                    //**************************************
                    //vu asked to uncomment this
                    //*************************************
                    using (TransactionHostEntities con = new TransactionHostEntities())
                    {
                        var terminal = (from r in con.Terminals
                                        where r.TerminalKey == model.Terminal.TerminalKey
                                        select r).FirstOrDefault();

                        if (terminal.PromptKey != null)
                        {
                            var prompt = (from r in con.Prompts
                                          where r.PromptKey == terminal.PromptKey
                                          select r).FirstOrDefault();

                            prompt.VersionNo = prompt.VersionNo + 1;
                            prompt.AutoTipEOSReportOption = model.AutoTipEOSReportOption;
                            prompt.AutoTipDisplayOption = model.AutoTipDisplayOption;

                            prompt.AutoTipTitle = c["AutoTipTitle"].ToString();
                            prompt.AutoTipAmountLabel = c["AutoTipAountLabel"].ToString();
                            prompt.AutoTipEOSReportMessage = c["AutoTipEOSReportMessgae"].ToString();
                            prompt.AutoTipPriorAmountLabel = c["AutoTipPriorAmountLabel"].ToString();
                            prompt.AutoTipScreenText = c["AutoTipScreenText"].ToString();
                            prompt.AutoTipDefaultButtonID = AutoTipDefaultButtonID;

                            prompt.EnableAutoTipAmountDisplay = Convert.ToBoolean(model.EnableAutoTipAmountDisplay);
                            prompt.EnableAutoTipTotalDisplay = Convert.ToBoolean(model.EnablrAutoTipTotalDisplay);
                            prompt.AutoTipEnterConfirmation = Convert.ToBoolean(model.AutoTipEnterConfirmation);
                            prompt.AutoTipFeeIncluded = Convert.ToBoolean(model.AutoTipFeeIncluded);
                        }
                        else
                        {
                            if (terminal.AppKey != null)
                            {
                                using (TransactionHostEntities con1 = new TransactionHostEntities())
                                {
                                    var app = (from a in con1.Apps
                                               where a.AppKey == terminal.AppKey
                                               select a).FirstOrDefault();

                                    if (app != null)
                                    {
                                        var prompt = (from p in con1.Prompts
                                                      where p.PromptKey == app.PromptKey
                                                      select p).FirstOrDefault();

                                        if (prompt != null)
                                        {

                                            Prompt prom = new Prompt();
                                            prom.UserId = prompt.UserId;
                                            prom.BusinessId = prompt.BusinessId;
                                            prom.AmountOne = prompt.AmountOne;
                                            prom.AmountTwo = prompt.AmountTwo;
                                            prom.AmountThree = prompt.AmountThree;
                                            prom.AmountFour = prompt.AmountFour;
                                            prom.AmountFive = prompt.AmountFive;
                                            prom.AmountSix = prompt.AmountSix;
                                            prom.AmountSeven = prompt.AmountSeven;
                                            prom.AmountEight = prompt.AmountEight;
                                            prom.AmountNine = prompt.AmountNine;
                                            prom.Inactive = prompt.Inactive;
                                            prom.LocationOnePrompt = prompt.LocationOnePrompt;
                                            prom.LocationTwoPrompt = prompt.LocationTwoPrompt;
                                            prom.DisableLocationOne = prompt.DisableLocationOne;
                                            prom.DisableLocationTwo = prompt.DisableLocationTwo;
                                            prom.EOSButtonText = prompt.EOSButtonText;
                                            prom.EOSApprovedText = prompt.EOSApprovedText;
                                            prom.EOSConfirmText = prompt.EOSConfirmText;
                                            prom.EOSPriorText = prompt.EOSPriorText;
                                            prom.EOSNotFoundText = prompt.EOSNotFoundText;
                                            prom.EOSBatchOpenText = prompt.EOSBatchOpenText;
                                            prom.AutoTipDisplayOption = prompt.AutoTipDisplayOption;
                                            prom.EnableAutoTipAmountDisplay = prompt.EnableAutoTipAmountDisplay;
                                            prom.EnableAutoTipTotalDisplay = prompt.EnableAutoTipTotalDisplay;
                                            prom.AutoTipTitle = prompt.AutoTipTitle;
                                            prom.AutoTipPriorAmountLabel = prompt.AutoTipPriorAmountLabel;
                                            prom.AutoTipScreenText = prompt.AutoTipScreenText;
                                            prom.AutoTipEnterConfirmation = prompt.AutoTipEnterConfirmation;
                                            prom.AutoTipEOSReportOption = prompt.AutoTipEOSReportOption;
                                            prom.AutoTipButtonOneFeeCents = prompt.AutoTipButtonOneFeeCents;
                                            prom.AutoTipButtonOneFeePercentage = prompt.AutoTipButtonOneFeePercentage;
                                            prom.AutoTipButtonTwoFeeCents = prompt.AutoTipButtonTwoFeeCents;
                                            prom.AutoTipButtonTwoFeePercentage = prompt.AutoTipButtonTwoFeePercentage;
                                            prom.AutoTipButtonThreeFeeCents = prompt.AutoTipButtonThreeFeeCents;
                                            prom.AutoTipButtonThreeFeePercentage = prompt.AutoTipButtonThreeFeePercentage;
                                            prom.AutoTipButtonFourFeeCents = prompt.AutoTipButtonFourFeeCents;
                                            prom.AutoTipButtonFourFeePercentage = prompt.AutoTipButtonFourFeePercentage;
                                            prom.AutoTipManualEntryFeeCents = prompt.AutoTipManualEntryFeeCents;
                                            prom.AutoTipManualEntryFeePercentage = prompt.AutoTipManualEntryFeePercentage;
                                            prom.AutoTipAmountLabel = prompt.AutoTipAmountLabel;
                                            prom.AutoTipEOSReportMessage = prompt.AutoTipEOSReportMessage;
                                            prom.AutoTipFeeIncluded = prompt.AutoTipFeeIncluded;
                                            prom.VersionNo = 1;
                                            prom.CreatedBy = new Guid();
                                            prom.CreatedDate = DateTime.Now;
                                            prom.UpdatedDate = DateTime.Now;
                                            prom.UpdatedBy = new Guid();

                                            prom.AutoTipDefaultButtonID = AutoTipDefaultButtonID;

                                            prom.AutoTipTwoEOSReportOption = model.OptionTwo.AutoTipEOSReportOption;
                                            prom.AutoTipTwoDisplayOption = model.OptionTwo.AutoTipDisplayOption;

                                            prom.AutoTipTitle = c["AutoTipTitle"].ToString();
                                            prom.AutoTipAmountLabel = c["AutoTipAountLabel"].ToString();
                                            prom.AutoTipEOSReportMessage = c["AutoTipEOSReportMessgae"].ToString();
                                            prom.AutoTipPriorAmountLabel = c["AutoTipPriorAmountLabel"].ToString();
                                            prom.AutoTipScreenText = c["AutoTipScreenText"].ToString();

                                            prom.EnableAutoTipTwoAmountDisplay = Convert.ToBoolean(model.OptionTwo.EnableAutoTipAmountDisplay);
                                            prom.EnableAutoTipTwoTotalDisplay = Convert.ToBoolean(model.OptionTwo.EnablrAutoTipTotalDisplay);
                                            prom.AutoTipTwoEnterConfirmation = Convert.ToBoolean(model.OptionTwo.AutoTipEnterConfirmation);
                                            prom.AutoTipTwoFeeIncluded = Convert.ToBoolean(model.OptionTwo.AutoTipFeeIncluded);

                                            prom.AutoTipNumOne = AutoTrip1;
                                            prom.AutoTipNumTwo = AutoTrip2;
                                            prom.AutoTipNumThree = AutoTrip3;
                                            prom.AutoTipNumFour = AutoTrip4;

                                            prom.AutoTipIsPercentOne = AutoTrip1TypeBool;
                                            prom.AutoTipIsPercentTwo = AutoTrip2TypeBool;
                                            prom.AutoTipIsPercentThree = AutoTrip3TypeBool;
                                            prom.AutoTipIsPercentFour = AutoTrip4TypeBool;

                                            prom.AutoTipButtonOneID = Convert.ToInt32(AutoTrip1CardType);
                                            prom.AutoTipButtonTwoID = Convert.ToInt32(AutoTrip2CardType);
                                            prom.AutoTipButtonThreeID = Convert.ToInt32(AutoTrip3CardType);
                                            prom.AutoTipButtonFourID = Convert.ToInt32(AutoTrip4CardType);

                                            con1.Prompts.Add(prom);
                                            con1.SaveChanges();
                                            terminal.PromptKey = prom.PromptKey;
                                        }
                                    }
                                }
                            }
                        }
                        con.SaveChanges();
                    }
                }
            }
            DiffChargeOptionTwoSave(model, c);

            ViewBag.Msg = "Saved!";
            return RedirectToAction("DiffSurcharge", new { key = model.Terminal.TerminalKey });
        }


        private void DiffChargeOptionTwoSave(DiffSurChargeModel model, FormCollection c)
        {
            TerminalHostDA terminalHostDA = new TerminalHostDA();
            model.Terminal = terminalHostDA.Terminal_ByKey(model.Terminal.TerminalKey).FirstOrDefault();
            TerminalLogoDA terminalLogoDA = new TerminalLogoDA();

            if (model.OptionTwo.EnableAutoTip != "Enable")
            {
                bool enable = false;
                terminalLogoDA.EnableAutoTipTwo(model.Terminal.TerminalKey, enable);
                DrawDropDownDiffSurchargeConfig();
            }
            else
            {
                int AutoTipDefaultButtonID = 0;
                if (c["AutoTip2optionsRadios"] != null)
                {
                    AutoTipDefaultButtonID = Convert.ToInt32(c["AutoTip2optionsRadios"]?.ToString());
                }

                bool AutoTrip1TypeBool = model.OptionTwo.Type1 == "Fixed" ? false : true;
                bool AutoTrip2TypeBool = model.OptionTwo.Type2 == "Fixed" ? false : true;
                bool AutoTrip3TypeBool = model.OptionTwo.Type3 == "Fixed" ? false : true;
                bool AutoTrip4TypeBool = model.OptionTwo.Type4 == "Fixed" ? false : true;


                int AutoTrip1 = 0;
                if (!string.IsNullOrEmpty(c["AutoTip2Tip1String"]))
                {
                    decimal autotipdec = 0.0m;
                    Decimal.TryParse(c["AutoTip2Tip1String"].ToString(), out autotipdec);
                    AutoTrip1 = Convert.ToInt32(autotipdec * 100);
                }

                int AutoTrip2 = 0;
                if (!string.IsNullOrEmpty(c["AutoTip2Tip2String"]))
                {
                    decimal autotipdec = 0.0m;
                    Decimal.TryParse(c["AutoTip2Tip2String"].ToString(), out autotipdec);
                    AutoTrip2 = Convert.ToInt32(autotipdec * 100);
                }
                int AutoTrip3 = 0;
                if (!string.IsNullOrEmpty(c["AutoTip2Tip3String"]))
                {
                    decimal autotipdec = 0.0m;
                    Decimal.TryParse(c["AutoTip2Tip3String"].ToString(), out autotipdec);
                    AutoTrip3 = Convert.ToInt32(autotipdec * 100);
                }
                int AutoTrip4 = 0;
                if (!string.IsNullOrEmpty(c["AutoTip2Tip4String"]))
                {
                    decimal autotipdec = 0.0m;
                    Decimal.TryParse(c["AutoTip2Tip4String"].ToString(), out autotipdec);
                    AutoTrip4 = Convert.ToInt32(autotipdec * 100);
                }

                var AutoTrip1CardType = model.OptionTwo.AutoCardType1;// Convert.ToInt32(c["AutoTrip1CardType"].ToString());
                var AutoTrip2CardType = model.OptionTwo.AutoCardType2; //Convert.ToInt32(c["AutoTrip2CardType"].ToString());
                var AutoTrip3CardType = model.OptionTwo.AutoCardType3; //Convert.ToInt32(c["AutoTrip3CardType"].ToString());
                var AutoTrip4CardType = model.OptionTwo.AutoCardType4; //Convert.ToInt32(c["AutoTrip4CardType"].ToString());


                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    PromptModel propm = new PromptModel();
                    //TerminalLogoDA tda = new TerminalLogoDA();
                    propm = terminalLogoDA.GetTerminalPrompt(model.Terminal.TerminalKey);

                    var terminal = (from r in con.Terminals
                                    where r.TerminalKey == model.Terminal.TerminalKey
                                    select r).FirstOrDefault();
                    terminal.VersionNo = terminal.VersionNo + 1;

                    terminal.AutoTipTwoNumOne = propm.AutoTipTwoNumOne != AutoTrip1 ? AutoTrip1 : terminal.AutoTipTwoNumOne;
                    terminal.AutoTipTwoNumTwo = propm.AutoTipTwoNumTwo != AutoTrip2 ? AutoTrip2 : terminal.AutoTipTwoNumTwo;
                    terminal.AutoTipTwoNumThree = propm.AutoTipTwoNumThree != AutoTrip3 ? AutoTrip3 : terminal.AutoTipTwoNumThree;
                    terminal.AutoTipTwoNumFour = propm.AutoTipTwoNumFour != AutoTrip4 ? AutoTrip4 : terminal.AutoTipTwoNumFour;

                    terminal.EnableAutoTipTwo = model.OptionTwo.EnableAutoTip.ToLower() == "enable";

                    //******************************

                    terminal.AutoTipTwoIsPercentOne = propm.AutoTipTwoIsPercentOne != AutoTrip1TypeBool ? AutoTrip1TypeBool : terminal.AutoTipTwoIsPercentOne;
                    terminal.AutoTipTwoIsPercentTwo = propm.AutoTipTwoIsPercentTwo != AutoTrip2TypeBool ? AutoTrip2TypeBool : terminal.AutoTipTwoIsPercentTwo;
                    terminal.AutoTipTwoIsPercentThree = propm.AutoTipTwoIsPercentThree != AutoTrip3TypeBool ? AutoTrip3TypeBool : terminal.AutoTipTwoIsPercentThree;
                    terminal.AutoTipTwoIsPercentFour = propm.AutoTipTwoIsPercentFour != AutoTrip4TypeBool ? AutoTrip4TypeBool : terminal.AutoTipTwoIsPercentFour;

                    //****************

                    terminal.AutoTipTwoButtonOneID = propm.AutoTipTwoButtonOneID != Convert.ToInt32(AutoTrip1CardType) ? Convert.ToInt32(AutoTrip1CardType) : terminal.AutoTipTwoButtonOneID;
                    terminal.AutoTipTwoButtonTwoID = propm.AutoTipTwoButtonTwoID != Convert.ToInt32(AutoTrip2CardType) ? Convert.ToInt32(AutoTrip2CardType) : terminal.AutoTipTwoButtonTwoID;
                    terminal.AutoTipTwoButtonThreeID = propm.AutoTipTwoButtonThreeID != Convert.ToInt32(AutoTrip3CardType) ? Convert.ToInt32(AutoTrip3CardType) : terminal.AutoTipTwoButtonThreeID;
                    terminal.AutoTipTwoButtonFourID = propm.AutoTipTwoButtonFourID != Convert.ToInt32(AutoTrip4CardType) ? Convert.ToInt32(AutoTrip4CardType) : terminal.AutoTipTwoButtonFourID;

                    terminal.AutoTipTwoDefaultButtonID = model.OptionTwo.AutoTipDefaultButtonID;

                    terminal.UpdatedBy = new Guid();
                    terminal.UpdatedDate = DateTime.Now;
                    terminal.UpdatedByUser = User.Identity.Name;
                    terminal.VersionNo = terminal.VersionNo + 1;
                    con.SaveChanges();
                }

                //******************************
                //Vu ask not to update prompt and Apps table just directly save on the terminal table
                //******************************
                //**************************************
                //vu asked to uncomment this
                //*************************************
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var terminal = (from r in con.Terminals
                                    where r.TerminalKey == model.Terminal.TerminalKey
                                    select r).FirstOrDefault();

                    if (terminal.PromptKey != null)
                    {
                        var prompt = (from r in con.Prompts
                                      where r.PromptKey == terminal.PromptKey
                                      select r).FirstOrDefault();

                        prompt.VersionNo = prompt.VersionNo + 1;
                        prompt.AutoTipTwoEOSReportOption = model.OptionTwo.AutoTipEOSReportOption;
                        prompt.AutoTipTwoDisplayOption = model.OptionTwo.AutoTipDisplayOption;

                        prompt.AutoTipTwoTitle = c["OptionTwo.AutoTipTitle"].ToString();
                        prompt.AutoTipTwoAmountLabel = c["OptionTwo.AutoTipAountLabel"].ToString();
                        prompt.AutoTipTwoEOSReportMessage = c["OptionTwo.AutoTipEOSReportMessgae"].ToString();
                        prompt.AutoTipTwoPriorAmountLabel = c["OptionTwo.AutoTipPriorAmountLabel"].ToString();
                        prompt.AutoTipTwoScreenText = c["OptionTwo.AutoTipScreenText"].ToString();
                        prompt.AutoTipTwoDefaultButtonID = AutoTipDefaultButtonID;

                        prompt.EnableAutoTipTwoAmountDisplay = Convert.ToBoolean(model.OptionTwo.EnableAutoTipAmountDisplay);
                        prompt.EnableAutoTipTwoTotalDisplay = Convert.ToBoolean(model.OptionTwo.EnablrAutoTipTotalDisplay);
                        prompt.AutoTipTwoEnterConfirmation = Convert.ToBoolean(model.OptionTwo.AutoTipEnterConfirmation);
                        prompt.AutoTipTwoFeeIncluded = Convert.ToBoolean(model.OptionTwo.AutoTipFeeIncluded);
                    }
                    else
                    {
                        if (terminal.AppKey != null)
                        {
                            using (TransactionHostEntities con1 = new TransactionHostEntities())
                            {
                                var app = (from a in con1.Apps
                                           where a.AppKey == terminal.AppKey
                                           select a).FirstOrDefault();

                                if (app != null)
                                {
                                    var prompt = (from p in con1.Prompts
                                                  where p.PromptKey == app.PromptKey
                                                  select p).FirstOrDefault();

                                    if (prompt != null)
                                    {

                                        Prompt prom = new Prompt();
                                        prom.UserId = prompt.UserId;
                                        prom.BusinessId = prompt.BusinessId;
                                        prom.AmountOne = prompt.AmountOne;
                                        prom.AmountTwo = prompt.AmountTwo;
                                        prom.AmountThree = prompt.AmountThree;
                                        prom.AmountFour = prompt.AmountFour;
                                        prom.AmountFive = prompt.AmountFive;
                                        prom.AmountSix = prompt.AmountSix;
                                        prom.AmountSeven = prompt.AmountSeven;
                                        prom.AmountEight = prompt.AmountEight;
                                        prom.AmountNine = prompt.AmountNine;
                                        prom.Inactive = prompt.Inactive;
                                        prom.LocationOnePrompt = prompt.LocationOnePrompt;
                                        prom.LocationTwoPrompt = prompt.LocationTwoPrompt;
                                        prom.DisableLocationOne = prompt.DisableLocationOne;
                                        prom.DisableLocationTwo = prompt.DisableLocationTwo;
                                        prom.EOSButtonText = prompt.EOSButtonText;
                                        prom.EOSApprovedText = prompt.EOSApprovedText;
                                        prom.EOSConfirmText = prompt.EOSConfirmText;
                                        prom.EOSPriorText = prompt.EOSPriorText;
                                        prom.EOSNotFoundText = prompt.EOSNotFoundText;
                                        prom.EOSBatchOpenText = prompt.EOSBatchOpenText;
                                        prom.AutoTipDisplayOption = prompt.AutoTipDisplayOption;
                                        prom.EnableAutoTipAmountDisplay = prompt.EnableAutoTipAmountDisplay;
                                        prom.EnableAutoTipTotalDisplay = prompt.EnableAutoTipTotalDisplay;
                                        prom.AutoTipTitle = prompt.AutoTipTitle;
                                        prom.AutoTipPriorAmountLabel = prompt.AutoTipPriorAmountLabel;
                                        prom.AutoTipEnterConfirmation = prompt.AutoTipEnterConfirmation;
                                        prom.AutoTipEOSReportOption = prompt.AutoTipEOSReportOption;
                                        prom.AutoTipButtonOneFeeCents = prompt.AutoTipButtonOneFeeCents;
                                        prom.AutoTipButtonOneFeePercentage = prompt.AutoTipButtonOneFeePercentage;
                                        prom.AutoTipButtonTwoFeeCents = prompt.AutoTipButtonTwoFeeCents;
                                        prom.AutoTipButtonTwoFeePercentage = prompt.AutoTipButtonTwoFeePercentage;
                                        prom.AutoTipButtonThreeFeeCents = prompt.AutoTipButtonThreeFeeCents;
                                        prom.AutoTipButtonThreeFeePercentage = prompt.AutoTipButtonThreeFeePercentage;
                                        prom.AutoTipButtonFourFeeCents = prompt.AutoTipButtonFourFeeCents;
                                        prom.AutoTipButtonFourFeePercentage = prompt.AutoTipButtonFourFeePercentage;
                                        prom.AutoTipManualEntryFeeCents = prompt.AutoTipManualEntryFeeCents;
                                        prom.AutoTipManualEntryFeePercentage = prompt.AutoTipManualEntryFeePercentage;
                                        prom.AutoTipAmountLabel = prompt.AutoTipAmountLabel;
                                        prom.AutoTipEOSReportMessage = prompt.AutoTipEOSReportMessage;
                                        prom.AutoTipFeeIncluded = prompt.AutoTipFeeIncluded;
                                        prom.VersionNo = 1;
                                        prom.CreatedBy = new Guid();
                                        prom.CreatedDate = DateTime.Now;
                                        prom.UpdatedDate = DateTime.Now;
                                        prom.UpdatedBy = new Guid();

                                        prom.AutoTipDefaultButtonID = AutoTipDefaultButtonID;

                                        prom.AutoTipEOSReportOption = model.OptionTwo.AutoTipEOSReportOption;
                                        prom.AutoTipDisplayOption = model.OptionTwo.AutoTipDisplayOption;

                                        prom.AutoTipTitle = c["AutoTipTitle"].ToString();
                                        prom.AutoTipAmountLabel = c["AutoTipAountLabel"].ToString();
                                        prom.AutoTipEOSReportMessage = c["AutoTipEOSReportMessgae"].ToString();
                                        prom.AutoTipPriorAmountLabel = c["AutoTipPriorAmountLabel"].ToString();
                                        prom.AutoTipTwoScreenText = c["OptionTwo.AutoTipScreenText"].ToString();

                                        prom.EnableAutoTipTwoAmountDisplay = Convert.ToBoolean(model.OptionTwo.EnableAutoTipAmountDisplay);
                                        prom.EnableAutoTipTwoTotalDisplay = Convert.ToBoolean(model.OptionTwo.EnablrAutoTipTotalDisplay);
                                        prom.AutoTipTwoEnterConfirmation = Convert.ToBoolean(model.OptionTwo.AutoTipEnterConfirmation);
                                        prom.AutoTipTwoFeeIncluded = Convert.ToBoolean(model.OptionTwo.AutoTipFeeIncluded);

                                        prom.AutoTipNumOne = AutoTrip1;
                                        prom.AutoTipNumTwo = AutoTrip2;
                                        prom.AutoTipNumThree = AutoTrip3;
                                        prom.AutoTipNumFour = AutoTrip4;

                                        prom.AutoTipIsPercentOne = AutoTrip1TypeBool;
                                        prom.AutoTipIsPercentTwo = AutoTrip2TypeBool;
                                        prom.AutoTipIsPercentThree = AutoTrip3TypeBool;
                                        prom.AutoTipIsPercentFour = AutoTrip4TypeBool;

                                        prom.AutoTipButtonOneID = Convert.ToInt32(AutoTrip1CardType);
                                        prom.AutoTipButtonTwoID = Convert.ToInt32(AutoTrip2CardType);
                                        prom.AutoTipButtonThreeID = Convert.ToInt32(AutoTrip3CardType);
                                        prom.AutoTipButtonFourID = Convert.ToInt32(AutoTrip4CardType);

                                        prom.AutoTipTwoScreenText = prompt.AutoTipTwoScreenText;

                                        con1.Prompts.Add(prom);
                                        con1.SaveChanges();
                                        terminal.PromptKey = prom.PromptKey;

                                    }
                                }
                            }
                        }
                    }
                    con.SaveChanges();
                }
            }
        }

        public DiffChargeOptionTwoModel PopulateDiffChargeOptionTwoModel(DiffSurChargeModel model, PromptModel propm)
        {
            model.OptionTwo.AutoTipEOSReportOption =
                                propm.AutoTipTwoEOSReportOption;

            model.OptionTwo.AutoTipDisplayOption =
                                propm.AutoTipTwoDisplayOption;

            model.OptionTwo.AutoTipTitle = propm.AutoTipTwoTitle;
            model.OptionTwo.AutoTipPriorAmountLabel = propm.AutoTipTwoPriorAmountLabel;
            model.OptionTwo.AutoTipEOSReportMessgae = propm.AutoTipTwoEOSReportMessage;
            model.OptionTwo.AutoTipAountLabel = propm.AutoTipTwoAmountLabel;


            model.OptionTwo.EnableAutoTipAmountDisplay = propm.EnableAutoTipTwoAmountDisplay.ToString();
            model.OptionTwo.EnablrAutoTipTotalDisplay = propm.EnableAutoTipTwoTotalDisplay.ToString();
            model.OptionTwo.AutoTipEnterConfirmation = propm.AutoTipTwoEnterConfirmation.ToString();

            var one = propm.AutoTipTwoIsPercentOne.ToString();
            var two = propm.AutoTipTwoIsPercentTwo.ToString();
            var three = propm.AutoTipTwoIsPercentThree.ToString();
            var four = propm.AutoTipTwoIsPercentFour.ToString();

            if (model.Terminal.EnableAutoTipTwo == true)
            {
                model.OptionTwo.EnableAutoTip = "Enable";
            }
            else
            {
                model.OptionTwo.EnableAutoTip = "Disable";
            }

            if (one == "True")
            {
                model.OptionTwo.Type1 = "Percent";
            }
            else
            {
                model.OptionTwo.Type1 = "Fixed";
            }
            if (two == "True")
            {
                model.OptionTwo.Type2 = "Percent";
            }
            else
            {
                model.OptionTwo.Type2 = "Fixed";
            }
            if (three == "True")
            {
                model.OptionTwo.Type3 = "Percent";
            }
            else
            {
                model.OptionTwo.Type3 = "Fixed";
            }
            if (four == "True")
            {
                model.OptionTwo.Type4 = "Percent";
            }
            else
            {
                model.OptionTwo.Type4 = "Fixed";
            }

            //model.OptionTwo.Type2 = model.OptionTwo.Terminal.AutoTipIsPercentTwo.ToString();
            //model.OptionTwo.Type3 = model.OptionTwo.Terminal.AutoTipIsPercentThree.ToString();
            //model.OptionTwo.Type4 = model.OptionTwo.Terminal.AutoTipIsPercentFour.ToString();

            if (propm.AutoTipTwoNumOne != null)
            {
                model.OptionTwo.AutoTip1 = (propm.AutoTipTwoNumOne.Value);
            }
            if (propm.AutoTipTwoNumTwo != null)
            {
                model.OptionTwo.AutoTip2 = (propm.AutoTipTwoNumTwo.Value);
            }
            if (propm.AutoTipTwoNumThree != null)
            {
                model.OptionTwo.AutoTip3 = (propm.AutoTipTwoNumThree.Value);
            }
            if (propm.AutoTipTwoNumFour != null)
            {
                model.OptionTwo.AutoTip4 = (propm.AutoTipTwoNumFour.Value);
            }

            if (model.OptionTwo.Type1 == "Percent")
            {
                model.OptionTwo.AutoTip1s = (model.OptionTwo.AutoTip1 / 100).ToString() + "%";
            }
            else
            {
                model.OptionTwo.AutoTip1s = "$ " + (model.OptionTwo.AutoTip1 / 100).ToString();
            }
            if (model.OptionTwo.Type2 == "Percent")
            {
                model.OptionTwo.AutoTip2s = (model.OptionTwo.AutoTip2 / 100).ToString() + "%";
            }
            else
            {
                model.OptionTwo.AutoTip2s = "$ " + (model.OptionTwo.AutoTip2 / 100).ToString();
            }
            if (model.OptionTwo.Type3 == "Percent")
            {
                model.OptionTwo.AutoTip3s = (model.OptionTwo.AutoTip3 / 100).ToString() + "%";
            }
            else
            {
                model.OptionTwo.AutoTip3s = "$ " + (model.OptionTwo.AutoTip3 / 100).ToString();
            }
            if (model.OptionTwo.Type4 == "Percent")
            {
                model.OptionTwo.AutoTip4s = (model.OptionTwo.AutoTip4 / 100).ToString() + "%";
            }
            else
            {
                model.OptionTwo.AutoTip4s = "$ " + (model.OptionTwo.AutoTip4 / 100).ToString();
            }

            model.OptionTwo.AutoTip1ss = (model.OptionTwo.AutoTip1 / 100).ToString();
            model.OptionTwo.AutoTip2ss = (model.OptionTwo.AutoTip2 / 100).ToString();
            model.OptionTwo.AutoTip3ss = (model.OptionTwo.AutoTip3 / 100).ToString();
            model.OptionTwo.AutoTip4ss = (model.OptionTwo.AutoTip4 / 100).ToString();


            model.OptionTwo.AutoCardType1 = propm.AutoTipTwoButtonOneID.ToString();
            model.OptionTwo.AutoCardType2 = propm.AutoTipTwoButtonTwoID.ToString();
            model.OptionTwo.AutoCardType3 = propm.AutoTipTwoButtonThreeID.ToString();
            model.OptionTwo.AutoCardType4 = propm.AutoTipTwoButtonFourID.ToString();
            model.OptionTwo.AutoTipDefaultButtonID = propm.AutoTipTwoDefaultButtonID;

            model.OptionTwo.AutoTipScreenText = propm.AutoTipTwoScreenText;

            return model.OptionTwo;
        }


        public JsonResult GetAutoTipDefaultButton(int key)
        {
            PromptModel propm = new PromptModel();
            TerminalLogoDA tda = new TerminalLogoDA();
            propm = tda.GetTerminalPrompt(key);

            int AutoTipDefaultButton = 0;

            if (propm.AutoTipDefaultButtonID != null)
            {
                AutoTipDefaultButton = propm.AutoTipDefaultButtonID.Value;
            }


            return Json(AutoTipDefaultButton, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 1)]
        [Authorize]
        [RBAC(Permission = "VAATerminalLogo")]
        public ActionResult Logo(int key)
        {
            TerminalLogoDA tda = new TerminalLogoDA();
            TerminalLogoModel model = new TerminalLogoModel();
            TerminalHostDA tda1 = new TerminalHostDA();
            model.Terminal = tda1.Terminal_ByKey(key).FirstOrDefault();
            // model = tda.GetLogo(key);
            model.HasPhoto = tda.GetPhotoRecieptDA(model);
            model.HasFooterLogoPhoto = tda.GetFooterLogo(model);

            return View(model);

        }

        private bool IsValidLogo(HttpPostedFileBase footerLogo)
        {
            if (footerLogo == null)
            {
                return false;
            }
            else
            {
                int MaxContentLength = 2048 * 10;
                var isValid = Regex.IsMatch(footerLogo.FileName, "(.*?)\\.(LGO|lgo)$");
                if (!isValid)
                {

                    return false;
                }
                else if (footerLogo.ContentLength == 0)
                {
                    return false;
                }
                else if (footerLogo.ContentLength > MaxContentLength)
                {
                    return false;
                }
                return true;
            }
        }

        private bool UpdateFooterLogo(FormCollection formCollection, HttpPostedFileBase footerLogo, TerminalLogoModel logoModel, TerminalLogoDA logoDA)
        {
            logoModel.TerminalKey = logoModel.Terminal.TerminalKey;
            logoModel.UpdatedBy = new Guid();
            logoModel.UpdatedByUser = User.Identity.Name;
            logoModel.UpdatedDate = DateTime.Now;
            byte[] photoid = new byte[1];

            bool isValid = IsValidLogo(footerLogo);
            if (isValid)
            {
                BinaryReader reader = new BinaryReader(footerLogo.InputStream);
                photoid = reader.ReadBytes((int)footerLogo.InputStream.Length);
                logoModel.FooterLogoModel = new FooterLogoModel();
                logoModel.FooterLogoModel.LogoImage = photoid;
                logoModel.FooterLogoModel.Description = formCollection["footerLogoDescription"].ToString();
                logoDA.SaveLogo(logoModel);
            }
            return isValid;
        }

        [OutputCache(Duration = 1)]
        [Authorize]
        [HttpPost]
        [RBAC(Permission = "VAATerminalLogo")]
        public ActionResult Logo(FormCollection c, HttpPostedFileBase file1, HttpPostedFileBase file2, HttpPostedFileBase footerLogo, TerminalLogoModel m)
        {
            var isRemoveLogo = c["RemoveLogo"];
            var isRemoveFooterLogo = c["removeFooterLogo"];

            TerminalHostDA terminalHostDA = new TerminalHostDA();
            TerminalLogoDA logoDA = new TerminalLogoDA();

            if (isRemoveFooterLogo == null)
            {
                if(footerLogo != null)
                {
                    bool updateSuccessful = UpdateFooterLogo(c, footerLogo, m, logoDA);
                    if (!updateSuccessful)
                    {
                        TerminalLogoModel model = new TerminalLogoModel();
                        model = logoDA.GetLogo(m.TerminalKey);

                        model.Terminal = terminalHostDA.Terminal_ByKey(m.TerminalKey).FirstOrDefault();
                        ViewBag.Error = "Your file is too large, maximum allowed size is: 20 KB";
                        return View(model);
                    }
                }
            }

            if (isRemoveLogo == null)
            {
                var forceUpdate = false;
                m.TerminalKey = m.Terminal.TerminalKey;
                m.UpdatedBy = new Guid();
                m.UpdatedByUser = User.Identity.Name;
                m.UpdatedDate = DateTime.Now;
                byte[] photoid = new byte[1];
                if (file1 != null)
                {
                    bool validFile = Regex.IsMatch(file1.FileName, "(.*?)\\.(LGO|lgo)$");
                    if (file1.ContentLength > 0 && validFile)
                    {
                        int MaxContentLength = 2048 * 10;
                        if (file1.ContentLength > MaxContentLength)
                        {
                            TerminalLogoModel model = new TerminalLogoModel();
                            model = logoDA.GetLogo(m.TerminalKey);

                            model.Terminal = terminalHostDA.Terminal_ByKey(m.TerminalKey).FirstOrDefault();
                            ViewBag.Error = "Your file is too large, maximum allowed size is: 20 KB";
                            return View(model);
                        }

                        BinaryReader reader = new BinaryReader(file1.InputStream);
                        photoid = reader.ReadBytes((int)file1.InputStream.Length);
                        m.ReceiptLogoModel = new ReceiptLogoModel();
                        m.ReceiptLogoModel.LogoImage = photoid;
                        m.ReceiptLogoModel.Description = c["RecieptDescription"].ToString();
                        forceUpdate = true;
                    }
                }

                byte[] photoid1 = new byte[1];
                if (file2 != null)
                {
                    bool validFile1 = Regex.IsMatch(file2.FileName, "(.*?)\\.(JPG|PNG|JPEG|jpg|png|jpeg)$");
                    if (file2.ContentLength > 0 && validFile1)
                    {
                        int MaxContentLength = 2048 * 10; //3 MB
                        if (file2.ContentLength > MaxContentLength)
                        {
                            TerminalLogoModel model = new TerminalLogoModel();
                            model = logoDA.GetLogo(m.TerminalKey);
                            model.Terminal = terminalHostDA.Terminal_ByKey(m.TerminalKey).FirstOrDefault();
                            ViewBag.Error = "Your file is too large, maximum allowed size is: 20 KB";
                            return View(model);
                        }
                        BinaryReader reader = new BinaryReader(file2.InputStream);
                        photoid1 = reader.ReadBytes((int)file2.InputStream.Length);
                        m.ScreenLogoModel = new ScreenLogoModel();
                        m.ScreenLogoModel.LogoImage = photoid1;
                        m.ScreenLogoModel.Description = c["ScreenDescription"].ToString();
                        forceUpdate = true;
                    }
                }

                if (forceUpdate)
                {
                    logoDA.SaveLogo(m);
                }
            }
            if (isRemoveLogo == "on")
            {
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var screen = (from c1 in con.Terminals
                                  where c1.TerminalKey == m.Terminal.TerminalKey
                                  select c1).FirstOrDefault();

                    if (screen != null)
                    {
                        screen.VersionNo = screen.VersionNo + 1;
                        screen.ReceiptLogoKey = null;
                        con.SaveChanges();
                    }
                }
            }

            if (isRemoveFooterLogo == "on")
            {
                RemoveFooterLogo(m, isRemoveFooterLogo);
            }

            ViewBag.Error = "Saved!";
            TerminalLogoDA tda11 = new TerminalLogoDA();
            TerminalLogoModel model1 = new TerminalLogoModel();
            model1 = tda11.GetLogo(m.Terminal.TerminalKey);
            model1.Terminal = terminalHostDA.Terminal_ByKey(m.Terminal.TerminalKey).FirstOrDefault();
            // return View(model1);
            // return View(m);
            return RedirectToAction("Logo", new { key = m.Terminal.TerminalKey });
        }

        private void RemoveFooterLogo(TerminalLogoModel m, string isRemoveFooterLogo)
        {
            if (isRemoveFooterLogo == "on")
            {
                using (TransactionHostEntities con = new TransactionHostEntities())
                {
                    var terminal = (from c1 in con.Terminals
                                    where c1.TerminalKey == m.Terminal.TerminalKey
                                    select c1).FirstOrDefault();

                    if (terminal != null)
                    {
                        terminal.VersionNo = terminal.VersionNo + 1;
                        terminal.FooterLogoKey = null;
                        con.SaveChanges();
                    }
                }
            }
        }

        public ActionResult GetPhotoScreen(int key)
        {
            byte[] photo = null;
            TerminalLogoDA tda = new TerminalLogoDA();
            TerminalLogoModel model = new TerminalLogoModel();
            TerminalHostDA tda1 = new TerminalHostDA();
            model = tda.GetLogo(key);
            model.Terminal = tda1.Terminal_ByKey(key).FirstOrDefault();
            photo = tda.GetPhotoScreenDA(model);//model.ScreenLogoModel.LogoImage;
            return File(photo, "image/jpeg");
        }

        private List<TerminalModel> ConvertToClientlist(List<Terminal> term)
        {
            List<TerminalModel> model = new List<TerminalModel>();
            foreach (var Terminal in term)
            {
                TerminalModel terminal = new TerminalModel();
                terminal.TerminalKey = Terminal.TerminalKey;
                terminal.MerchantKey = Terminal.MerchantKey;
                terminal.TerminalGroupKey = Terminal.TerminalGroupKey;
                terminal.AppKey = Terminal.AppKey;
                terminal.ReceiptLogoKey = Terminal.ReceiptLogoKey;
                terminal.ScreenLogoKey = Terminal.ScreenLogoKey;
                terminal.PromptKey = Terminal.PromptKey;
                terminal.TerminalConfigKey = Terminal.TerminalConfigKey;
                terminal.TerminalBINRangeKey = Terminal.TerminalBINRangeKey;
                terminal.TerminalNetworkKey = Terminal.TerminalNetworkKey;
                terminal.StateKey = Terminal.StateKey;
                terminal.ReceiptKey = Terminal.ReceiptKey;
                terminal.TerminalTypeKey = Terminal.TerminalTypeKey;
                terminal.TNNetworkKey = Terminal.TNNetworkKey;

                terminal.TerminalID = Terminal.TerminalID;
                terminal.SerialNumber = Terminal.SerialNumber;
                terminal.VAAVersion = Terminal.VAAVersion;
                terminal.OSVersion = Terminal.OSVersion;
                terminal.FSVersionNO = Terminal.FSVersionNO;
                terminal.DiscountApplied = Terminal.DiscountApplied;
                terminal.UserID = Terminal.UserID;
                terminal.BusinessID = Terminal.BusinessID;
                terminal.ABN = Terminal.ABN;

                terminal.VersionNo = Terminal.VersionNo;
                terminal.InputTimeoutSec = Terminal.InputTimeoutSec;
                terminal.BacklightSec = Terminal.BacklightSec;
                terminal.SleepModeMin = Terminal.SleepModeMin;
                terminal.ScreenBrightness = Terminal.ScreenBrightness;
                terminal.Sound = Terminal.Sound;
                terminal.RemeberMe = Terminal.RemeberMe;
                string p = string.Empty;
                if (Terminal.ABNSetting != null)
                {
                    string o = Terminal.ABNSetting.Trim();
                    int d = 0;
                    try
                    {
                        d = Convert.ToInt32(o);
                    }
                    catch
                    {
                    }
                    p = String.Format("{0,0:N2}", d / 100.0);
                    terminal.ABNSettingB = Convert.ToDecimal(p);
                }

                terminal.ABNSetting = p;// Terminal.ABNSetting;
                terminal.MinimumCharge = Terminal.MinimumCharge;
                terminal.MaximumCharge = Terminal.MaximumCharge;
                terminal.EOSTime = Terminal.EOSTime;
                terminal.LoginMode = Terminal.LoginMode;
                terminal.UserIdConfig = Terminal.UserIdConfig;
                terminal.BusinessIdConfig = Terminal.BusinessIdConfig;
                terminal.ABNConfig = Terminal.ABNConfig;
                terminal.FullVersionNo = Terminal.TerminalKey + "." + Terminal.VersionNo;
                terminal.GPRSReconnectTimer = Terminal.GPRSReconnectTimer;
                terminal.NetworkTableStatus = Terminal.NetworkTableStatus;
                terminal.AppSignRespCode = Terminal.AppSignRespCode;
                terminal.AppRespCode = Terminal.AppRespCode;

                terminal.Inactive = Terminal.Inactive;
                terminal.CreatedDate = Terminal.CreatedDate;
                terminal.CreatedBy = Terminal.CreatedBy;
                terminal.UpdatedDate = Terminal.UpdatedDate;
                terminal.UpdatedBy = Terminal.UpdatedBy;
                terminal.CreatedByUser = Terminal.CreatedByUser;
                terminal.UpdatedByUser = Terminal.UpdatedByUser;
                terminal.IsPreTranPing = Terminal.IsPreTranPing;
                terminal.TranAdviceSendMode = Terminal.TranAdviceSendMode;
                terminal.ConnectToTMS = Terminal.ConnectToTMS;
                terminal.SimSerialNo = Terminal.SimSerialNo;
                terminal.SimMobileNumber = Terminal.SimMobileNumber;
                terminal.IMEI = Terminal.IMEI;
                terminal.PTID = Terminal.PTID;

                terminal.DefaultServiceFee = Terminal.DefaultServiceFee;
                terminal.ServiceFeeMode = Terminal.ServiceFeeMode;
                terminal.CustomerPaymentType = Terminal.CustomerPaymentType;
                terminal.EnableNetworkReceiptPrint = Terminal.EnableNetworkReceiptPrint;
                terminal.EnableMerchantLogoPrint = Terminal.EnableMerchantLogoPrint;
                terminal.SignatureVerificationTimeout = Terminal.SignatureVerificationTimeout;
                terminal.AutoLogonHour = Terminal.AutoLogonHour;
                terminal.AutoLogonMode = Terminal.AutoLogonMode;
                terminal.EnableRefundTran = Terminal.EnableRefundTran;
                terminal.EnableAuthTran = Terminal.EnableAuthTran;
                terminal.EnableTipTran = Terminal.EnableTipTran;
                terminal.EnableCashOutTran = Terminal.EnableCashOutTran;
                terminal.MerchantPassword = Terminal.MerchantPassword;
                terminal.BankLogonRespCode = Terminal.BankLogonRespCode;

                terminal.MainRegionKey = Terminal.MainRegionKey;
                terminal.TipTrickKey = Terminal.TipTrickKey;
                terminal.TerminalLocationKey = Terminal.TerminalLocationKey;
                terminal.EnableMOTO = Terminal.EnableMOTO;
                terminal.DefaultBackgroundColour = Terminal.DefaultBackgroundColour;


                if (Terminal.BusinessTypeKey != null)
                {
                    terminal.BusinessTypeKey = Terminal.BusinessTypeKey.Value;
                }
                if (Terminal.RebootHours != null)
                {
                    terminal.RebootHours = Terminal.RebootHours.Value;
                }
                if (Terminal.EnableAutoTip == null)
                {
                    terminal.EnableAutoTip = true;
                }
                else
                {
                    terminal.EnableAutoTip = Terminal.EnableAutoTip.Value;
                }
                if (Terminal.EnableMOTORefund == null)
                {
                    terminal.EnableMOTORefund = true;
                }
                else
                {
                    terminal.EnableMOTORefund = Terminal.EnableMOTORefund.Value;
                }

                if (Terminal.AutoEOSMode != null)
                {
                    terminal.AutoEOSMode = Terminal.AutoEOSMode.Value;
                }
                if (Terminal.EnableDualAPNMode != null)
                {
                    terminal.EnableDualAPNMode = Terminal.EnableDualAPNMode.Value;
                }
                model.Add(terminal);
            }
            return model;
        }

        private void Swap(ref SwapTerminalModel swapterminal, TerminalModel terminal)
        {
            swapterminal.TerminalKey = terminal.TerminalKey;
            swapterminal.TerminalID = terminal.TerminalID;
            swapterminal.SerialNumber = terminal.SerialNumber;
            swapterminal.CreatedBy = terminal.CreatedBy;
            swapterminal.CreatedDate = terminal.CreatedDate;
            swapterminal.UpdatedBy = terminal.UpdatedBy;
            swapterminal.UpdatedDate = terminal.UpdatedDate;
            swapterminal.CreatedByUser = terminal.CreatedByUser;
            swapterminal.UpdatedByUser = terminal.UpdatedByUser;
            swapterminal.SwapSerialNumber = terminal.SerialNumber;

        }

        private void DrawDropDownDiffSurchargeConfig()
        {
            List<TypeMode1> AutoTipDisplayOption = new List<TypeMode1>();
            TypeMode1 AutoTipDisplayOption1 = new TerminalController.TypeMode1();
            TypeMode1 AutoTipDisplayOption2 = new TerminalController.TypeMode1();
            TypeMode1 AutoTipDisplayOption3 = new TerminalController.TypeMode1();

            AutoTipDisplayOption1.ShortDescription = "P-Percentage Only";
            AutoTipDisplayOption1.key = "P";

            AutoTipDisplayOption2.ShortDescription = "A-Amount Only";
            AutoTipDisplayOption2.key = "A";

            AutoTipDisplayOption3.ShortDescription = "B-Both Percentage and Amount";
            AutoTipDisplayOption3.key = "B";

            AutoTipDisplayOption.Add(AutoTipDisplayOption1);
            AutoTipDisplayOption.Add(AutoTipDisplayOption2);
            AutoTipDisplayOption.Add(AutoTipDisplayOption3);

            ViewBag.AutoTipDisplayOption1 = AutoTipDisplayOption.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();
            //*************************************************************************
            List<TypeMode> EnableTrueFalse = new List<TypeMode>();
            TypeMode EnableTrueFalse1 = new TerminalController.TypeMode();
            TypeMode EnableTrueFalse2 = new TerminalController.TypeMode();
            EnableTrueFalse1.ShortDescription = "true";
            EnableTrueFalse1.key = "true";

            EnableTrueFalse2.ShortDescription = "false";
            EnableTrueFalse2.key = "false";


            EnableTrueFalse.Add(EnableTrueFalse1);
            EnableTrueFalse.Add(EnableTrueFalse2);
            ViewBag.EnableTrueFalse = EnableTrueFalse.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();
            //***************************************************************************
            List<TypeMode> Con = new List<TypeMode>();
            TypeMode con1 = new TerminalController.TypeMode();
            TypeMode con2 = new TerminalController.TypeMode();
            con1.ShortDescription = "Need Confirmation";
            con1.key = "true";

            con2.ShortDescription = "No Confirmation";
            con2.key = "false";


            Con.Add(con1);
            Con.Add(con2);
            ViewBag.EnableCon = Con.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();

            //*************************************************************************
            List<TypeMode> AmtDisplay = new List<TypeMode>();
            TypeMode Amt1 = new TerminalController.TypeMode();
            TypeMode Amt2 = new TerminalController.TypeMode();
            Amt1.ShortDescription = "Show Amount Line";
            Amt1.key = "true";

            Amt2.ShortDescription = "Omit Amount Line";
            Amt2.key = "false";


            AmtDisplay.Add(Amt1);
            AmtDisplay.Add(Amt2);
            ViewBag.AmountDisplay = AmtDisplay.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();

            //*************************************************************************
            List<TypeMode> TotDisplay = new List<TypeMode>();
            TypeMode Tot1 = new TerminalController.TypeMode();
            TypeMode Tot2 = new TerminalController.TypeMode();
            Tot1.ShortDescription = "Show Total Line";
            Tot1.key = "true";

            Tot2.ShortDescription = "Omit Total Line";
            Tot2.key = "false";


            TotDisplay.Add(Tot1);
            TotDisplay.Add(Tot2);
            ViewBag.TotalDisplay = TotDisplay.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();
            //*************************************************************************

            List<TypeMode> AutoTipEOSReportOption = new List<TypeMode>();
            TypeMode AutoTipEOSReportOption1 = new TerminalController.TypeMode();
            TypeMode AutoTipEOSReportOption2 = new TerminalController.TypeMode();
            TypeMode AutoTipEOSReportOption3 = new TerminalController.TypeMode();

            AutoTipEOSReportOption1.ShortDescription = "G-Gross Tip Amount Only";
            AutoTipEOSReportOption1.key = "G";

            AutoTipEOSReportOption2.ShortDescription = "S-Net Tip Amount Only";
            AutoTipEOSReportOption2.key = "S";

            AutoTipEOSReportOption3.ShortDescription = "D-Net Tip and Gross Tip";
            AutoTipEOSReportOption3.key = "D";

            AutoTipEOSReportOption.Add(AutoTipEOSReportOption1);
            AutoTipEOSReportOption.Add(AutoTipEOSReportOption2);
            AutoTipEOSReportOption.Add(AutoTipEOSReportOption3);

            ViewBag.AutoTipEOSReportOption1 = AutoTipEOSReportOption.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();



            //**************************************************************************
            List<TypeMode3> EnableMode = new List<TypeMode3>();
            TypeMode3 EnableType1 = new TerminalController.TypeMode3();
            TypeMode3 EnableType2 = new TerminalController.TypeMode3();

            EnableType1.ShortDescription = "Enable";
            EnableType1.key = "Enable";

            EnableType2.ShortDescription = "Disable";
            EnableType2.key = "Disable";

            EnableMode.Add(EnableType1);
            EnableMode.Add(EnableType2);

            ViewBag.EnableMode = EnableMode.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();



            List<TypeMode2> TypeMode = new List<TypeMode2>();
            TypeMode2 Type1 = new TerminalController.TypeMode2();
            TypeMode2 Type2 = new TerminalController.TypeMode2();

            Type1.ShortDescription = "Percent";
            Type1.key = "Percent";

            Type2.ShortDescription = "Fixed";
            Type2.key = "Fixed";

            TypeMode.Add(Type1);
            TypeMode.Add(Type2);

            ViewBag.TypeMode1 = TypeMode.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();

            List<TypeMode> CardTypeMode = new List<TypeMode>();
            TypeMode CardType1 = new TerminalController.TypeMode();
            TypeMode CardType2 = new TerminalController.TypeMode();
            TypeMode CardType3 = new TerminalController.TypeMode();
            TypeMode CardType4 = new TerminalController.TypeMode();
            TypeMode CardType5 = new TerminalController.TypeMode();
            TypeMode CardType6 = new TerminalController.TypeMode();
            TypeMode CardType7 = new TerminalController.TypeMode();
            TypeMode CardType8 = new TerminalController.TypeMode();
            TypeMode CardType9 = new TerminalController.TypeMode();
            TypeMode CardType10 = new TerminalController.TypeMode();
            TypeMode CardType11 = new TerminalController.TypeMode();
            TypeMode CardType12 = new TerminalController.TypeMode();
            TypeMode CardType13 = new TerminalController.TypeMode();
            TypeMode CardType14 = new TerminalController.TypeMode();
            TypeMode CardType15 = new TerminalController.TypeMode();
            TypeMode CardType16 = new TerminalController.TypeMode();
            TypeMode CardType17 = new TerminalController.TypeMode();
            TypeMode CardType18 = new TerminalController.TypeMode();
            TypeMode unionPayCardType = new TerminalController.TypeMode();

            CardType1.ShortDescription = "AMEX";
            CardType1.key = "1";
            CardType2.ShortDescription = "Basic Card";
            CardType2.key = "2";
            CardType3.ShortDescription = "Diners";
            CardType3.key = "3";
            CardType4.ShortDescription = "Diners/AMEX";
            CardType4.key = "4";
            CardType5.ShortDescription = "Eftpos";
            CardType5.key = "5";
            CardType6.ShortDescription = "Gold Cards";
            CardType6.key = "6";
            CardType7.ShortDescription = "Little Tip";
            CardType7.key = "7";
            CardType8.ShortDescription = "Massive Tip";
            CardType8.key = "8";
            CardType9.ShortDescription = "Master Card";
            CardType9.key = "9";
            CardType10.ShortDescription = "Medium Tip";
            CardType10.key = "10";
            CardType11.ShortDescription = "Okay Tip";
            CardType11.key = "11";
            CardType12.ShortDescription = "Silver Card";
            CardType12.key = "12";
            CardType13.ShortDescription = "VISA";
            CardType13.key = "13";
            CardType14.ShortDescription = "VISA/Master";
            CardType14.key = "14";
            CardType15.ShortDescription = "NZ Flag";
            CardType15.key = "15";
            CardType16.ShortDescription = "USA Flag";
            CardType16.key = "16";
            CardType17.ShortDescription = "Australia Flag";
            CardType17.key = "17";
            CardType18.ShortDescription = "UK Flag";
            CardType18.key = "18";
            unionPayCardType.ShortDescription = "Union Pay";
            unionPayCardType.key = "19";

            CardTypeMode.Add(CardType1);
            CardTypeMode.Add(CardType2);
            CardTypeMode.Add(CardType3);
            CardTypeMode.Add(CardType4);
            CardTypeMode.Add(CardType5);
            CardTypeMode.Add(CardType6);
            CardTypeMode.Add(CardType7);
            CardTypeMode.Add(CardType8);
            CardTypeMode.Add(CardType9);
            CardTypeMode.Add(CardType10);
            CardTypeMode.Add(CardType11);
            CardTypeMode.Add(CardType12);
            CardTypeMode.Add(CardType13);
            CardTypeMode.Add(CardType14);
            CardTypeMode.Add(CardType15);
            CardTypeMode.Add(CardType16);
            CardTypeMode.Add(CardType17);
            CardTypeMode.Add(CardType18);
            CardTypeMode.Add(unionPayCardType);

            ViewBag.CardTypeMode = CardTypeMode.Select(item => new SelectListItem
            {
                Text = item.ShortDescription,
                Value = item.key.ToString()
            }).OrderBy(item => item.Text).ToList();

        }
        class TypeMode
        {
            public string ShortDescription { get; set; }
            public string key { get; set; }
        }
        class TypeMode1
        {
            public string ShortDescription { get; set; }
            public string key { get; set; }
        }
        class TypeMode3
        {
            public string ShortDescription { get; set; }
            public string key { get; set; }
        }
        class TypeMode2
        {
            public string ShortDescription { get; set; }
            public string key { get; set; }
        }

        public ActionResult TMSConnect()
        {

            return View(new TerminalTMSConnectVM());
        }


        [HttpPost]
        public ActionResult TMSConnect(TerminalTMSConnectVM model)
        {

            List<string> updated = new List<string>();
            IEnumerable<string> delta = new List<string>();

            if (!String.IsNullOrEmpty(model.TerminalId))
            {
                using (TransactionHostEntities entities = new TransactionHostEntities())
                {
                    List<Terminal> terminals = terminals = entities.Terminals.Where(where => model.TerminalId.Contains(where.TerminalID)).ToList();
                    foreach (Terminal terminal in terminals)
                    {
                        //prevent updates when ConnectToTMS is already set to true
                        //TODO: Optimize code
                        if (model.TMSConnect)
                        {
                            if (terminal.ConnectToTMS == null || (terminal.ConnectToTMS.HasValue && !terminal.ConnectToTMS.Value))
                            {
                                terminal.ConnectToTMS = true;
                                terminal.UpdatedDate = DateTime.Now;
                                terminal.UpdatedByUser = User.Identity.Name;
                                updated.Add(terminal.TerminalID);
                            }
                        }
                        else
                        {
                            if (terminal.ConnectToTMS == null || (terminal.ConnectToTMS.HasValue && terminal.ConnectToTMS.Value))
                            {
                                terminal.ConnectToTMS = false;
                                terminal.UpdatedDate = DateTime.Now;
                                terminal.UpdatedByUser = User.Identity.Name;
                                updated.Add(terminal.TerminalID);
                            }
                        }
                    }
                    entities.SaveChanges();
                }
                delta = model.TerminalId.Split(',').ToList().Except(updated);
            }
            model.InvalidOrNotUpdated = delta.ToArray();
            model.Updated = updated.ToArray();
            return View(model);
        }


        public Terminal ConvertToTerminalServer(TerminalModel cTerminal)
        {
            //
            Terminal sTerminal = new Terminal();
            sTerminal.TerminalKey = cTerminal.TerminalKey;
            sTerminal.MerchantKey = cTerminal.MerchantKey;

            sTerminal.TerminalGroupKey = cTerminal.TerminalGroupKey;
            sTerminal.AppKey = cTerminal.AppKey;

            sTerminal.ReceiptLogoKey = cTerminal.ReceiptLogoKey;
            sTerminal.ScreenLogoKey = cTerminal.ScreenLogoKey;
            sTerminal.PromptKey = cTerminal.PromptKey;

            sTerminal.TerminalConfigKey = cTerminal.TerminalConfigKey;

            sTerminal.TerminalBINRangeKey = cTerminal.TerminalBINRangeKey;
            sTerminal.TerminalNetworkKey = cTerminal.TerminalNetworkKey;

            sTerminal.StateKey = cTerminal.StateKey;
            sTerminal.ReceiptKey = cTerminal.ReceiptKey;
            sTerminal.TerminalTypeKey = cTerminal.TerminalTypeKey;
            sTerminal.TNNetworkKey = cTerminal.TNNetworkKey;

            sTerminal.VersionNo = cTerminal.VersionNo;
            sTerminal.InputTimeoutSec = cTerminal.InputTimeoutSec;
            sTerminal.BacklightSec = cTerminal.BacklightSec;
            sTerminal.SleepModeMin = cTerminal.SleepModeMin;
            sTerminal.ScreenBrightness = cTerminal.ScreenBrightness;
            sTerminal.Sound = cTerminal.Sound;
            sTerminal.RemeberMe = cTerminal.RemeberMe;
            sTerminal.ABNSetting = cTerminal.ABNSetting;
            sTerminal.MinimumCharge = cTerminal.MinimumCharge;
            sTerminal.MaximumCharge = cTerminal.MaximumCharge;
            sTerminal.EOSTime = cTerminal.EOSTime;
            sTerminal.LoginMode = cTerminal.LoginMode;
            sTerminal.UserIdConfig = cTerminal.UserIdConfig;
            sTerminal.BusinessIdConfig = cTerminal.BusinessIdConfig;
            sTerminal.ABNConfig = cTerminal.ABNConfig;
            sTerminal.GPRSReconnectTimer = cTerminal.GPRSReconnectTimer;
            sTerminal.NetworkTableStatus = cTerminal.NetworkTableStatus;
            sTerminal.Inactive = cTerminal.Inactive;
            sTerminal.UserID = cTerminal.UserID;
            sTerminal.BusinessID = cTerminal.BusinessID;
            sTerminal.ABN = cTerminal.ABN;

            sTerminal.TerminalID = cTerminal.TerminalID;
            sTerminal.SerialNumber = cTerminal.SerialNumber;
            sTerminal.VAAVersion = cTerminal.VAAVersion;
            sTerminal.OSVersion = cTerminal.OSVersion;
            sTerminal.FSVersionNO = cTerminal.FSVersionNO;
            sTerminal.DiscountApplied = cTerminal.DiscountApplied;
            sTerminal.AppRespCode = cTerminal.AppRespCode;
            sTerminal.AppSignRespCode = cTerminal.AppSignRespCode;
            sTerminal.IsPreTranPing = cTerminal.IsPreTranPing;
            sTerminal.TranAdviceSendMode = cTerminal.TranAdviceSendMode;
            sTerminal.ConnectToTMS = cTerminal.ConnectToTMS;
            sTerminal.SimSerialNo = cTerminal.SimSerialNo;
            sTerminal.SimMobileNumber = cTerminal.SimMobileNumber;
            sTerminal.IMEI = cTerminal.IMEI;
            sTerminal.PTID = cTerminal.PTID;
            sTerminal.DefaultBackgroundColour = cTerminal.DefaultBackgroundColour;
            sTerminal.EnableMOTO = cTerminal.EnableMOTO;

            sTerminal.DefaultServiceFee = cTerminal.DefaultServiceFee;
            sTerminal.ServiceFeeMode = cTerminal.ServiceFeeMode;
            sTerminal.CustomerPaymentType = cTerminal.CustomerPaymentType;
            sTerminal.EnableNetworkReceiptPrint = cTerminal.EnableNetworkReceiptPrint;
            sTerminal.EnableMerchantLogoPrint = cTerminal.EnableMerchantLogoPrint;
            sTerminal.SignatureVerificationTimeout = cTerminal.SignatureVerificationTimeout;
            sTerminal.AutoLogonHour = cTerminal.AutoLogonHour;
            sTerminal.AutoLogonMode = cTerminal.AutoLogonMode;
            sTerminal.EnableRefundTran = cTerminal.EnableRefundTran;
            sTerminal.EnableAuthTran = cTerminal.EnableAuthTran;
            sTerminal.EnableTipTran = cTerminal.EnableTipTran;
            sTerminal.EnableCashOutTran = cTerminal.EnableCashOutTran;
            sTerminal.MerchantPassword = cTerminal.MerchantPassword;
            sTerminal.BankLogonRespCode = cTerminal.BankLogonRespCode;
            sTerminal.MainRegionKey = cTerminal.MainRegionKey;
            sTerminal.TipTrickKey = cTerminal.TipTrickKey;
            sTerminal.TerminalLocationKey = cTerminal.TerminalLocationKey;
            sTerminal.BusinessTypeKey = cTerminal.BusinessTypeKey;
            sTerminal.RebootHours = cTerminal.RebootHours;
            sTerminal.EnableAutoTip = cTerminal.EnableAutoTip;
            sTerminal.EnableMOTORefund = cTerminal.EnableMOTORefund;
            sTerminal.AutoEOSMode = cTerminal.AutoEOSMode;
            sTerminal.EnableDualAPNMode = cTerminal.EnableDualAPNMode;
            sTerminal.ClearOverrideSettings = cTerminal.ClearOverrideSettings;

            return sTerminal;
        }
        private TerminalModel ConvertToClient(Terminal Terminal)
        {

            TerminalModel terminal = new TerminalModel();
            terminal.TerminalKey = Terminal.TerminalKey;
            terminal.MerchantKey = Terminal.MerchantKey;
            terminal.TerminalGroupKey = Terminal.TerminalGroupKey;
            terminal.AppKey = Terminal.AppKey;
            terminal.ReceiptLogoKey = Terminal.ReceiptLogoKey;
            terminal.ScreenLogoKey = Terminal.ScreenLogoKey;
            terminal.PromptKey = Terminal.PromptKey;
            terminal.TerminalConfigKey = Terminal.TerminalConfigKey;
            terminal.TerminalBINRangeKey = Terminal.TerminalBINRangeKey;
            terminal.TerminalNetworkKey = Terminal.TerminalNetworkKey;
            terminal.StateKey = Terminal.StateKey;
            terminal.ReceiptKey = Terminal.ReceiptKey;
            terminal.TerminalTypeKey = Terminal.TerminalTypeKey;
            terminal.TNNetworkKey = Terminal.TNNetworkKey;

            terminal.TerminalID = Terminal.TerminalID;
            terminal.SerialNumber = Terminal.SerialNumber;
            terminal.VAAVersion = Terminal.VAAVersion;
            terminal.OSVersion = Terminal.OSVersion;
            terminal.FSVersionNO = Terminal.FSVersionNO;
            terminal.DiscountApplied = Terminal.DiscountApplied;
            terminal.UserID = Terminal.UserID;
            terminal.BusinessID = Terminal.BusinessID;
            terminal.ABN = Terminal.ABN;

            terminal.VersionNo = Terminal.VersionNo;
            terminal.InputTimeoutSec = Terminal.InputTimeoutSec;
            terminal.BacklightSec = Terminal.BacklightSec;
            terminal.SleepModeMin = Terminal.SleepModeMin;
            terminal.ScreenBrightness = Terminal.ScreenBrightness;
            terminal.Sound = Terminal.Sound;
            terminal.RemeberMe = Terminal.RemeberMe;
            string p = string.Empty;
            if (Terminal.ABNSetting != null)
            {
                string o = Terminal.ABNSetting.Trim();
                int d = 0;
                try
                {
                    d = Convert.ToInt32(o);
                }
                catch
                {
                }
                p = String.Format("{0,0:N2}", d / 100.0);
                terminal.ABNSettingB = Convert.ToDecimal(p);
            }

            terminal.ABNSetting = p;// Terminal.ABNSetting;
            terminal.MinimumCharge = Terminal.MinimumCharge;
            terminal.MaximumCharge = Terminal.MaximumCharge;
            terminal.EOSTime = Terminal.EOSTime;
            terminal.LoginMode = Terminal.LoginMode;
            terminal.UserIdConfig = Terminal.UserIdConfig;
            terminal.BusinessIdConfig = Terminal.BusinessIdConfig;
            terminal.ABNConfig = Terminal.ABNConfig;
            terminal.FullVersionNo = Terminal.TerminalKey + "." + Terminal.VersionNo;
            terminal.GPRSReconnectTimer = Terminal.GPRSReconnectTimer;
            terminal.NetworkTableStatus = Terminal.NetworkTableStatus;
            terminal.AppSignRespCode = Terminal.AppSignRespCode;
            terminal.AppRespCode = Terminal.AppRespCode;

            terminal.Inactive = Terminal.Inactive;
            terminal.CreatedDate = Terminal.CreatedDate;
            terminal.CreatedBy = Terminal.CreatedBy;
            terminal.UpdatedDate = Terminal.UpdatedDate;
            terminal.UpdatedBy = Terminal.UpdatedBy;
            terminal.CreatedByUser = Terminal.CreatedByUser;
            terminal.UpdatedByUser = Terminal.UpdatedByUser;
            terminal.IsPreTranPing = Terminal.IsPreTranPing;
            terminal.TranAdviceSendMode = Terminal.TranAdviceSendMode;
            terminal.ConnectToTMS = Terminal.ConnectToTMS;
            terminal.SimSerialNo = Terminal.SimSerialNo;
            terminal.SimMobileNumber = Terminal.SimMobileNumber;
            terminal.IMEI = Terminal.IMEI;
            terminal.PTID = Terminal.PTID;

            terminal.DefaultServiceFee = Terminal.DefaultServiceFee;
            terminal.ServiceFeeMode = Terminal.ServiceFeeMode;
            terminal.CustomerPaymentType = Terminal.CustomerPaymentType;
            terminal.EnableNetworkReceiptPrint = Terminal.EnableNetworkReceiptPrint;
            terminal.EnableMerchantLogoPrint = Terminal.EnableMerchantLogoPrint;
            terminal.SignatureVerificationTimeout = Terminal.SignatureVerificationTimeout;
            terminal.AutoLogonHour = Terminal.AutoLogonHour;
            terminal.AutoLogonMode = Terminal.AutoLogonMode;
            terminal.EnableRefundTran = Terminal.EnableRefundTran;
            terminal.EnableAuthTran = Terminal.EnableAuthTran;
            terminal.EnableTipTran = Terminal.EnableTipTran;
            terminal.EnableCashOutTran = Terminal.EnableCashOutTran;
            terminal.MerchantPassword = Terminal.MerchantPassword;
            terminal.BankLogonRespCode = Terminal.BankLogonRespCode;

            terminal.MainRegionKey = Terminal.MainRegionKey;
            terminal.TipTrickKey = Terminal.TipTrickKey;
            terminal.TerminalLocationKey = Terminal.TerminalLocationKey;
            terminal.EnableMOTO = Terminal.EnableMOTO;
            terminal.DefaultBackgroundColour = Terminal.DefaultBackgroundColour;
            terminal.AutoTipNumOne = Terminal.AutoTipNumOne;
            terminal.AutoTipNumTwo = Terminal.AutoTipNumTwo;
            terminal.AutoTipNumThree = Terminal.AutoTipNumThree;
            terminal.AutoTipNumFour = Terminal.AutoTipNumFour;

            terminal.AutoTipIsPercentOne = Terminal.AutoTipIsPercentOne;
            terminal.AutoTipIsPercentTwo = Terminal.AutoTipIsPercentTwo;
            terminal.AutoTipIsPercentThree = Terminal.AutoTipIsPercentThree;
            terminal.AutoTipIsPercentFour = Terminal.AutoTipIsPercentFour;

            terminal.AutoTipButtonOneID = Terminal.AutoTipButtonOneID;
            terminal.AutoTipButtonTwoID = Terminal.AutoTipButtonTwoID;
            terminal.AutoTipButtonThreeID = Terminal.AutoTipButtonThreeID;
            terminal.AutoTipButtonFourID = Terminal.AutoTipButtonFourID;
            terminal.AutoTipDefaultButtonID = Terminal.AutoTipDefaultButtonID;
            terminal.ClearOverrideSettings = Terminal.ClearOverrideSettings;


            if (Terminal.BusinessTypeKey != null)
            {
                terminal.BusinessTypeKey = Terminal.BusinessTypeKey.Value;
            }
            if (Terminal.RebootHours != null)
            {
                terminal.RebootHours = Terminal.RebootHours.Value;
            }
            if (Terminal.EnableAutoTip == null)
            {
                terminal.EnableAutoTip = true;
            }
            else
            {
                terminal.EnableAutoTip = Terminal.EnableAutoTip.Value;
            }
            if (Terminal.EnableMOTORefund == null)
            {
                terminal.EnableMOTORefund = true;
            }
            else
            {
                terminal.EnableMOTORefund = Terminal.EnableMOTORefund.Value;
            }

            if (Terminal.AutoEOSMode != null)
            {
                terminal.AutoEOSMode = Terminal.AutoEOSMode.Value;
            }
            if (Terminal.EnableDualAPNMode != null)
            {
                terminal.EnableDualAPNMode = Terminal.EnableDualAPNMode.Value;
            }
            return terminal;
        }

        [OutputCache(Duration = 1)]
        [Authorize]
        [RBAC(Permission = "VAATerminalLogo")]
        public ActionResult SetEOS(string terminalId, bool isSetEOS)
        {
            try
            {
                using (var dbContext = new TransactionHostEntities())
                {
                    if (isSetEOS)
                    {
                        dbContext.Database.ExecuteSqlCommand($"EXEC usp_UnSetEOSTransactionSummaryForTerminal '{terminalId}'");
                    }
                    else
                    {
                        dbContext.Database.ExecuteSqlCommand($"EXEC usp_SetEOSTransactionSummaryForTerminal '{terminalId}'");
                    }
                }
            }
            catch (Exception ex)
            {
                //Somehow EF does not refresh the usp changes against to usp_SetEOS entity, hence the error of EOS SP result error. 
                var result = new
                {
                    IsError = true,
                    ErrorMessage = "Cannot set settlement receipt to summary"
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsError = false, ErrorMessage = string.Empty, IsSetEOS = !isSetEOS }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loading ImportTerminal page
        /// </summary>
        /// <returns></returns>
        public ActionResult ImportTerminals()
        {
            var model = new ImportTerminalPageVM();
            var businessTypeDA = new BusinessTypeDA();
            model.BusinessType = businessTypeDA.GetBusinessTypes().AsSelectListItems();

            return View(model);
        }

        /// <summary>
        /// Loading Terminal from file and response back json to client.
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadTerminal()
        {
            try
            {
                if (Request.Files == null || Request.Files.Count == 0)
                {
                    var result = new
                    {
                        IsError = true,
                        ErrorMessage = "Please select terminal file"
                    };
                    var json = Json(result, JsonRequestBehavior.AllowGet);
                    return json;
                }
                //get file data
                var terminalFile = Request.Files[0];
                var contentType = terminalFile.InputStream;
                using (CsvReader csvReader = new CsvReader(new StreamReader(contentType)))
                {
                    csvReader.Configuration.HasHeaderRecord = true;
                    var records = csvReader.GetRecords<ImportTerminalVM>().ToList();

                    //format terminal
                    if (records.Any())
                    {
                        foreach (var terminal in records)
                        {
                            terminal.Status = terminal.Status.Trim().ToUpper();
                            terminal.TerminalType = terminal.TerminalType.RemoveMultipleSpaces().ToUpper();
                        }
                    }

                    var terminalIds = records.Where(x => !string.IsNullOrEmpty(x.TerminalID)).Select(n => n.TerminalID.Trim()).ToList();
                    var duplicatedTerminalIds = terminalIds.GroupBy(n => n).Where(n => n.Count() > 1).Select(n => n.Key);
                    var errors = new StringBuilder();

                    //validate missing TerminalID in upload files
                    if (records.Exists(x => string.IsNullOrEmpty(x.TerminalID.Trim())))
                    {
                        errors.AppendLine("Some records don't have TerminalID.");
                    }

                    //validate missing Status in upload files
                    if (records.Exists(x => string.IsNullOrEmpty(x.Status.Trim())))
                    {
                        errors.AppendLine("Some records don't have Status.");
                    }

                    //validate missing TerminalType in upload files
                    if (records.Exists(x => string.IsNullOrEmpty(x.TerminalType.Trim())))
                    {
                        errors.AppendLine("Some records don't have TerminalType.");
                    }

                    //validate duplicate TerminalID in upload files
                    if (duplicatedTerminalIds.Any())
                    {
                        errors.AppendLine(
                            $"The list of TerminalIds = [{string.Join(",", duplicatedTerminalIds)}] have been duplicated in import file.");
                    }


                    using (var entities = new TaxiEpayEntities())
                    {
                        //validate TerminalID existed in system
                        var terminalIdAvailables = entities.EftTerminals.Where(n => terminalIds.Contains(n.TerminalId)).Select(n => n.TerminalId);
                        if (terminalIdAvailables.Any())
                        {
                            errors.AppendLine(
                                $"The list of TerminalIds = [{string.Join(",", terminalIdAvailables)}] existed in system.");
                        }

                        //validate Terminal Type invalid in system
                        var terminalFromClient = records.Select(n => n.TerminalType.ToUpper().Trim()).Distinct().ToList();
                        var validList = entities.TerminalTypes.Where(n => n.AllowImport == true)
                           .Select(n => n.Name.ToUpper())
                           .Distinct().ToList();
                        var invalidList = terminalFromClient.Except(validList).ToList();

                        if (invalidList != null && invalidList.Any())
                        {
                            errors.AppendLine($"Invalid Terminal Type: [{string.Join(",", invalidList.Select(n => n))}]");
                        }

                        //validate Status not existing in system
                        var terminalStatusFromClient = records.Select(n => n.Status.ToUpper().Trim()).Distinct().ToList();
                        var statusAvailabledList = entities.TerminalStatus
                            .Where(x => terminalStatusFromClient.Contains(x.Status.ToUpper())).Select(x => x.Status.ToUpper()).Distinct()
                            .ToList();

                        if (terminalStatusFromClient.Count != statusAvailabledList.Count) //Status not match
                        {
                            terminalStatusFromClient.RemoveAll(n => statusAvailabledList.Contains(n) || string.IsNullOrEmpty(n));
                            if (terminalStatusFromClient.Count > 0)
                            {
                                errors.AppendLine($"Status not existing: [{string.Join(",", terminalStatusFromClient.Select(n => n))}]");
                            }
                        }
                    }

                    var result = new
                    {
                        IsError = errors.Length != 0,
                        ErrorMessage = errors.ToString(),
                        Data = records.ToList()
                    };

                    var json = Json(result, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = Int32.MaxValue; //set max lenght json data  2147483647

                    return json;
                }
            }
            catch (Exception e)
            {
                var result = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ImportTerminalToDatabase(ImportTerminalPageVM model)
        {
            try
            {
                var entryList =
                    JsonConvert.DeserializeObject<List<ImportTerminalVM>>(model.TerminalList);

                var lstEftTerminal = new List<EftTerminal>();

                var typesFromClient = entryList.Select(n => n.TerminalType.ToLower().Trim()).Distinct();
                var types = new List<Phoenix_Service.TerminalType>();
                var statusList = new List<string>();
                using (var entities = new TaxiEpayEntities())
                {
                    types = entities.TerminalTypes.Where(n => typesFromClient.Contains(n.Name.ToLower())).ToList();
                    statusList = entities.TerminalStatus.Select(x => x.Status).ToList();
                }
                foreach (var terminal in entryList)
                {
                    var eftTerminal = terminal.ToEftTerminal();
                    eftTerminal.Company_key = model.BusinessTypeId;
                    eftTerminal.ModifiedByUser = User.Identity.Name;
                    eftTerminal.Modified_dttm = DateTime.Now;
                    eftTerminal.Status = statusList.First(x => x.ToUpper() == terminal.Status.ToUpper().Trim());
                    eftTerminal.TerminalType =
                        types.FirstOrDefault(n => n.Name.ToLower() == terminal.TerminalType.ToLower().Trim()).TerminalType_key;
                    lstEftTerminal.Add(eftTerminal);
                }

                EftTerminalDA eftTerminalDA = new EftTerminalDA();
                eftTerminalDA.AddListOfTerminal(lstEftTerminal);

                var result = new
                {
                    IsError = false
                };

                return Json(result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                var result = new
                {
                    IsError = true,
                    Message = e.Message
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GeneratePassword(string terminalId)
        {
            try
            {
                TerminalHostDA tda = new TerminalHostDA();
                var password = tda.GenerateTerminalPassword(terminalId);
                var result = new
                {
                    IsError = false,
                    Message = $"Password for Terminal {terminalId} is {password}"
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsError = true,
                    ErrorMessage = "Cannot generate password"
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
