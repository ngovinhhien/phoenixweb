﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using PhoenixWeb.Helper;
using PhoenixWeb.Models.ViewModel;
using Phoenix_Service;

namespace PhoenixWeb.Controllers
{
    public class MissingTransactionController : PhoenixWebController
    {
        //
        // GET: /MissingTransaction/

        public ActionResult Index()
        {
            DeclinedTransactionVM model = new DeclinedTransactionVM();
#if DEBUG
            model.TerminalID = "72879247";
#endif
            model.TransactionStartDate = DateTime.Now.ToString("dd-MM-yyyy");
            model.TransactionEndDate = DateTime.Now.ToString("dd-MM-yyyy");
            return View("DeclinedTransaction", model);
        }


        public JsonResult GetDeclinedTransaction([FromBody] SearchDeclinedTransactionVM searchObject)  //Gets the QBR history
        {
            var firstDateStr = searchObject.TransactionStartDate;
            var dateformat = "dd-MM-yyyy";
            var transStart = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB"));
            var endDateStr = searchObject.TransactionEndDate;
            var transEnd = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).AddDays(1);

            using (var entities = new TaxiEpayEntities())
            {
                entities.SetCommandTimeOut(1800);

                var declinedTransactions =
                    entities.f_RPT_DeclinedTransactionSearch(transStart, transEnd, searchObject.TerminalID)
                    .OrderBy(n => n.transstart).ToList();

                int totalRecords = declinedTransactions.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                int skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;

                var result = declinedTransactions.Skip(skip).Take(searchObject.PageSize);
                var res = result.Select(transaction => new
                {
                    id = transaction.id,
                    batchid = transaction.batchid,
                    transaction.batchstatus,
                    transaction.merchantid,
                    transaction.merchantname,
                    startshift = transaction.startshift.ToString("dd/MM/yyyy HH:mm:ss"),
                    endshift = transaction.endshift.ToString("dd/MM/yyyy HH:mm:ss"),
                    transaction.trans_status,
                    transstart = transaction.transstart.ToString("dd/MM/yyyy HH:mm:ss"),
                    transend = transaction.transend.ToString("dd/MM/yyyy HH:mm:ss"),
                    transaction.transfare,
                    transaction.transtotalamount,
                    transaction.terminalid,
                    transaction.transpan,
                    transaction.cardname,
                    transaction.transid1,
                    transaction.batchnumber,
                    filedate = transaction.filedate.Value.ToString("dd/MM/yyyy")
                });

                var jsonData = new
                {
                    total = totalPages,
                    page = searchObject.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ApprovedTransaction(string declinedTransactions, string terminalID, string transactionStartDate, string transactionEndDate)
        {
            if (string.IsNullOrEmpty(declinedTransactions))
            {
                var jsonData = new
                {
                    IsApproved = false,
                    Message = "Please select declined transaction"
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

            var trans = declinedTransactions.Split(',');
            var firstDateStr = transactionStartDate;
            var dateformat = "dd-MM-yyyy";
            var transStart = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB"));
            var endDateStr = transactionEndDate;
            var transEnd = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).AddDays(1);
            using (var entities = new TaxiEpayEntities())
            {
                entities.SetCommandTimeOut(1800);
                var cadMusDeclineds = entities.CadmusDeclineds
                                                .Where(n => trans.Contains(n.id.ToString()) &&
                                                            n.terminalid == terminalID &&
                                                            n.transstart >= transStart &&
                                                            n.transend <= transEnd)
                                                .ToList();
                if (!cadMusDeclineds.Any())
                {
                    var jsonData = new
                    {
                        IsApproved = false,
                        Message = "Please select declined transaction"
                    };
                    return Json(jsonData, JsonRequestBehavior.AllowGet);
                }

                foreach (var cadMusDeclined in cadMusDeclineds)
                {
                    var cadmusIn = cadMusDeclined.ToCadmusIN();
                    entities.CadmusINs.Add(cadmusIn);
                }

                entities.SaveChanges();
            }
            var result = new
            {
                IsApproved = true,
                Message = ""
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
