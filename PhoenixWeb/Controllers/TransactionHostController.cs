﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhoenixObjects.TransactionHost;
using PhoenixWeb.Common;
using Phoenix_BusLog.TransactionHost.BusLogs;


namespace PhoenixWeb.Controllers
{
    [Authorize]
    [RBAC(Permission = "CloseBatches")]
    public class TransactionHostController : Controller
    {
        //
        // GET: /TransactionHost/

        public ActionResult OpenBatches()
        {
            SearchUnReconciledBatches model = new SearchUnReconciledBatches();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchUnReconciledBatches(PhoenixObjects.Common.GridModel<SearchUnReconciledBatches, List<UnReconciledBatchesModel>> model)
        {
            if (ModelState.IsValid)
            {
                using (var transHostDA = new TransactionHostDA())
                {
                    transHostDA.GetUnReconciledBatchesWithPaging(model);
                }
            }
            return new CustomJsonResult() { Data = model };
        }

        [HttpPost]
        public ActionResult SearchUnReconciledTransactions(PhoenixObjects.Common.GridModel<SearchUnReconciledTransactions, List<UnReconciledTransactionsModel>> model)
        {
            if (ModelState.IsValid)
            {
                using (var transHostDA = new TransactionHostDA())
                {
                    transHostDA.GetUnReconciledTransactionsWithPaging(model);
                }
            }
            return new CustomJsonResult() { Data = model };
        }

        [HttpPost]
        public ActionResult ExecuteOpenBatches(UnReconciledBatchesModel model)
        {
            var reponse = new PhoenixObjects.Common.Response();
            if (Math.Abs((decimal)model.SettlementNetAmount - model.BatchTotal) > 1)
            {
                reponse.Status = (int)PhoenixObjects.Enums.StatusResponseEnum.Fail;
                reponse.Message = String.Format("Cannot open this batch. ABS(Settlement Net Amount: {0} - Batch Total: {1}) > 1", model.SettlementNetAmount, model.BatchTotal);
                return Json(reponse);
            }

            using (var transHostDA = new TransactionHostDA())
            {
                var settlementLog = transHostDA.FromUnReconciledBatchesToSettlementLog(model);
                transHostDA.AddNewSettlementLog(settlementLog, 3);

                var settlementAmount = transHostDA.FromUnReconciledBatchesToSettlementAmount(model);
                settlementLog.SettlementAmounts.Add(settlementAmount);

                bool checkCommit = transHostDA.SaveChanges();
                if (!checkCommit)
                {
                    reponse.Status = (int)PhoenixObjects.Enums.StatusResponseEnum.Fail;
                    reponse.Message = "Cannot open this batch. Please try it again!";
                    return Json(reponse);
                }

                var checkAdd = transHostDA.Call_SP_ToAddSettlementData(model.TerminalID, model.MerchantID, model.BatchNumber, model.BatchTotal);

                var settlementhistory = transHostDA.GetSettlementHistoryBy(model.TerminalID, model.MerchantID, model.BatchNumber);
                if (settlementhistory != null && settlementhistory.SettlementHistoryKey > 0)
                {
                    var checkPush = transHostDA.Call_SP_PushBatchtoPhoenixes(settlementhistory.SettlementHistoryKey, true);
                }
            }

            reponse.Status = (int)PhoenixObjects.Enums.StatusResponseEnum.Success;
            reponse.Message = "Successfully! This batch will take an hour to transfer to Phoenix Web System";
            return Json(reponse);
        }
    }
}
