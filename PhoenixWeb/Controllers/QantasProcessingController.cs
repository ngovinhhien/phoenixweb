﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Phoenix_BusLog.TransactionHost.Model;
using System.Web.Http;
using PhoenixWeb.Models.ViewModel;
using System.Web.Mvc;
using log4net;
using PhoenixWeb.Helper;
using PhoenixWeb.Models;
using Phoenix_BusLog.Data;
using Phoenix_Service;
using System.Dynamic;
using System.Text;
using DotNetOpenAuth.Messaging;
using OfficeOpenXml;
using Newtonsoft.Json;
using PhoenixWeb.Common;
using Phoenix_BusLog;
using PhoenixObjects.Enums;

namespace PhoenixWeb.Controllers
{
    public class QantasProcessingController : Controller
    {
        private ILog logger = LogManager.GetLogger("QantasProcessingController");

        public const string SaleAndFinRejectedStatus = "SF_REJECTED";
        public const string SaleAndFinApprovalStatus = "SF_APPROVAL";
        public const string ManagerRejectedStatus = "MNG_REJECTED";


        //
        // GET: /QantasProcessing/

        public ActionResult Index(bool isManager)
        {
            var model = new QantasPointApprovalScreenVM();
            model.Years = Enumerable.Range(DateTime.Now.Year - 9, 10).ToList().AsSelectListItems();
            model.Year = DateTime.Now.Year;
            model.Months = DateTimeFormatInfo.CurrentInfo.MonthNames
                .Where(n => !string.IsNullOrEmpty(n))
                .Select((n, i) => new SelectListItem()
                {
                    Text = n,
                    Value = (i + 1).ToString(),
                    Selected = (i + 1) == DateTime.Now.Month
                });
            model.Month = DateTime.Now.Month;

            var companyDA = new CompanyDA();
            var companyList = companyDA.GetCompanyAssociateWithOrganisationforLoyalty();
            model.BusinessTypes = companyList.AsSelectListItems();
            var firstCompany = companyList.FirstOrDefault();
            model.BusinessTypeID = firstCompany == null ? 0 : firstCompany.Company_key;

            if (isManager)
            {
                return View("QantasReviewingForManager", model);

            }
            else
            {
                return View("QantasReviewingForSaleFinance", model);
            }
        }


        public ActionResult QantasPointUpload()
        {
            var model = new QantasPointUploadVM();
            var companyDA = new CompanyDA();
            var companyList = companyDA.GetCompanyAssociateWithOrganisationforLoyalty().OrderBy(x => x.Company_key);
            model.BusinessTypes = companyList.ToList().AsSelectListItems();

            var firstCompany = companyList.FirstOrDefault();
            model.LoyaltyPromotions = firstCompany.LoyaltyPromotions.ToList().AsSelectListItems();

            return View(model);
        }

        public ActionResult QantasPointEarnedRate()
        {
            return View();
        }

        public JsonResult GetExchangeRateTypeList(bool includedGroupType = false)
        {
            IEnumerable<SelectListItem> typeList = null;
            var errorMessage = string.Empty;
            try
            {
                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    if (includedGroupType)
                    {
                        typeList = taxiEpayDbContext.ExchangeRateTypes
                            .Where(x => x.IsActive)
                            .ToList()
                            .AsSelectListItems("GROUP");
                    }
                    else
                    {
                        typeList = taxiEpayDbContext.ExchangeRateTypes
                            .Where(x => x.IsActive && x.ExchangeRateTypeCode != "GROUP")
                            .ToList()
                            .AsSelectListItems();
                    }

                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
                Data = typeList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQantasPointEarnedRate(QantasPointEarnedRateVM model)
        {
            using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
            {
                var items = taxiEpayDbContext.QBRExchangeRates
                    .Include(x => x.ExchangeRateType)
                    .Include(x => x.QBRPoints)
                    .Where(x => x.IsActive);

                //for sorting
                var qantasServices = new QantasProcessingDA();
                var itemSorter = qantasServices.SortDataListForPointEarnedRate(items, model.SortName, model.SortOrder);

                int totalRecords = itemSorter.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)model.PageSize);
                var skip = (model.CurrentPage - 1) * model.PageSize;
                var res = itemSorter.Skip(skip).Take(model.PageSize).ToList();

                var data = res.Select(x => new
                {
                    ID = x.ID,
                    Name = x?.Name,
                    QantasExchangeRate = x?.QantasExchangeRate,
                    Type = x.ExchangeRateType?.ExchangeRateTypeName,
                    FromDate = x.ExchangeRateTypeCode.Equals("NEW_MERCHANT") ? x?.RegisterStartDate?.ToString("dd/MM/yyyy") ?? String.Empty : x?.CampaignStartDate?.ToString("dd/MM/yyyy") ?? String.Empty,
                    ToDate = x.ExchangeRateTypeCode.Equals("NEW_MERCHANT") ? x?.RegisterEndDate?.ToString("dd/MM/yyyy") ?? String.Empty : x?.CampaignEndDate?.ToString("dd/MM/yyyy") ?? String.Empty,
                    x?.ExchangeRateTypeCode,
                    x?.RegisterEffectedDays,
                    IsUsed = x.QBRPoints.Any()
                });

                var jsonData = new
                {
                    total = totalPages,
                    page = model.CurrentPage,
                    records = totalRecords,
                    rows = data
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult UpdateQantasPointEarnedRate(QantasPointEarnedRateVM model)
        {
            var errorMessage = string.Empty;
            try
            {
                if (model == null)
                {
                    throw new Exception("Model is invalid");
                }
                var id = 0;
                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    var qbrExchangeRate = model.ToQbrExchangeRate();

                    var qantasServices = new QantasProcessingDA();
                    id = qantasServices.SaveQantasPointEarnedRate(taxiEpayDbContext, qbrExchangeRate, User.Identity.Name);

                    if (model.ExchangeRateTypeCode == "DEFAULT")
                    {
                        Trace.TraceInformation(id.ToString());
                        taxiEpayDbContext.Database.ExecuteSqlCommand(
                            $"UPDATE dbo.QBRExchangeRate SET IsActive = 0 WHERE ExchangeRateTypeCode = 'DEFAULT' AND ID NOT IN ({id})");
                        taxiEpayDbContext.Database.ExecuteSqlCommand(
                            $"UPDATE dbo.QBRExchangeRate SET IsActive = 1 WHERE ExchangeRateTypeCode = 'DEFAULT' AND ID IN ({id})");
                    }

                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteQantasPointEarnedRate(int id)
        {
            var errorMessage = string.Empty;
            try
            {
                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    var isExisting = taxiEpayDbContext.QBRExchangeRates.Any(x => x.ID == id);
                    if (isExisting)
                    {
                        var qantasServices = new QantasProcessingDA();
                        qantasServices.DeActiveQantasPointEarnedRate(taxiEpayDbContext, id, User.Identity.Name);

                        taxiEpayDbContext.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("The Exchange Rate doesn't exist.");
                    }
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEarnedRateDateExisting(string startDate, string endDate, string typeCode)
        {
            var errorMessage = string.Empty;
            List<string> typeNameList = new List<string>();
            try
            {
                if (startDate != null && endDate != null && !string.IsNullOrEmpty(typeCode))
                {
                    using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                    {
                        CultureInfo culture = new CultureInfo("en-US");
                        var startDateTime = DateTime.ParseExact(startDate, "dd/MM/yyyy", culture);
                        var endtDateTime = DateTime.ParseExact(endDate, "dd/MM/yyyy", culture);

                        typeNameList = taxiEpayDbContext.QBRExchangeRates.Where(x =>
                            x.ExchangeRateTypeCode.Equals(typeCode) && x.IsActive == true &&
                            ((x.RegisterStartDate <= startDateTime && x.RegisterEndDate >= endtDateTime)
                             || (x.CampaignStartDate <= startDateTime && x.CampaignEndDate >= endtDateTime)))
                            .Select(x => x.Name).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            var jsonData = new
            {
                IsError = !string.IsNullOrEmpty(errorMessage),
                ErrorMessage = errorMessage,
                Data = typeNameList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UploadQantasPointFile()
        {
            var isError = false;
            var errorMessage = String.Empty;
            List<QantasPointUploadModel> data = null;
            var qantasServices = new QantasProcessingDA();
            var importDate = DateTime.UtcNow.Date;

            try
            {
                //get file data
                var httpPostedFileBase = Request.Files[0];
                var contentType = httpPostedFileBase.ContentType;
                var fileType = qantasServices.GetFileType(contentType);

                //check excel type
                if (fileType.Equals("EXCEL"))
                {
                    var excelPackage = new ExcelPackage(httpPostedFileBase.InputStream);
                    //get first worksheet in excel file
                    ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.First();

                    int totalColumn = excelWorksheet.Dimension.End.Column;
                    int totalRow = excelWorksheet.Dimension.End.Row;

                    if (totalRow < 1 || totalRow > 5001)
                    {
                        throw new Exception($"The upload file should be minimum 1 record and maximum 5000 record.");
                    }

                    //get coulmn format of upload file
                    var columns = "Description|Point|Month|Year|Member Id";
                    var columnList = columns.Split('|').ToList();

                    //validate header row
                    List<string> headerValueList = new List<string>();
                    var headerRow = excelWorksheet.Cells[excelWorksheet.Dimension.Start.Row,
                        excelWorksheet.Dimension.Start.Column, 1, totalColumn];
                    foreach (var cell in headerRow)
                    {
                        headerValueList.Add(cell.Text.Trim());
                    }
                    bool isHeaderMatch = qantasServices.IsHeaderMatch(columnList, headerValueList);

                    //check column data is correct
                    if (totalColumn == columnList.Count && isHeaderMatch)
                    {
                        int lineNumber = 1;
                        List<QantasPointUploadModel> entryList = new List<QantasPointUploadModel>();
                        StringBuilder rowErrors = new StringBuilder(); //contain error list about data type in excel file

                        while (lineNumber < totalRow)
                        {
                            lineNumber++;
                            Dictionary<string, string> rowValueWithColumnName = new Dictionary<string, string>();
                            for (int i = 1; i <= totalColumn; i++)
                            {
                                rowValueWithColumnName.Add(headerValueList[i - 1], excelWorksheet.Cells[lineNumber, i].Text?.Trim());
                            }

                            //create entry entity of every row data
                            var lineError = String.Empty;
                            var entry = qantasServices.CreateEntryBaseOnRowData(rowValueWithColumnName, User.Identity.Name, out lineError, importDate);
                            if (!String.IsNullOrEmpty(lineError)) //in case this row data incorrect
                            {
                                rowErrors.Append(lineNumber.ToString() + ",");
                            }
                            else //in case this row data correct
                            {
                                entryList.Add(entry);
                            }
                        }


                        var validDataError = qantasServices.ValidateImportDataValidWithSystem(entryList);
                        var dataTypeError = !String.IsNullOrEmpty(rowErrors.ToString()) ? $"Error data in rows: {rowErrors.ToString()}  please check and upload again. " : String.Empty;

                        if (!String.IsNullOrEmpty(dataTypeError) || !String.IsNullOrEmpty(validDataError)) // if any row data incorrect then suggest user check file and upload again.
                        {
                            throw new Exception(dataTypeError + validDataError);
                        }
                        else // if not error then return all rows to display.
                        {
                            if (entryList.Count < 1)
                            {
                                throw new Exception($"No one record in file!");
                            }
                            data = entryList;
                        }
                    }
                    else
                    {
                        throw new Exception($"File format isn't correct. Please select the file with {columnList.Count} columns: {columns}");
                    }
                }
                else
                {
                    throw new Exception("This function only allows to import xlsx format");
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }


            var result = new
            {
                IsError = isError,
                ErrorMessage = errorMessage,
                Data = data
            };

            var json = Json(result, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = Int32.MaxValue; //set max lenght json data  2147483647

            return json;
        }

        public JsonResult ImportQantasPointFile(QantasPointUploadVM importData)
        {
            var isError = false;
            var errorMessage = String.Empty;

            try
            {
                if (importData.LoyaltyPromotionId == 0)
                {
                    throw new Exception("Loyalty Promotion is missing.");
                }
                if (importData.EntryList.Length < 4)
                {
                    throw new Exception("Data File is missing.");
                }

                var entryList =
                       JsonConvert.DeserializeObject<List<QantasPointUploadModel>>(importData.EntryList);

                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    var qantasServices = new QantasProcessingDA();
                    qantasServices.HandleSaveQantasPoint(taxiEpayDbContext, entryList, importData.LoyaltyPromotionId);

                    //taxiEpayDbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }


            var result = new
            {
                IsError = isError,
                ErrorMessage = errorMessage,
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLoyaltyPromotion(string businessTypeId)
        {
            var isError = false;
            var errorMessage = String.Empty;
            IEnumerable<SelectListItem> loyaltyListItems = null;
            try
            {
                int companyKey;
                if (!int.TryParse(businessTypeId, out companyKey))
                {
                    throw new Exception("Can not found Business Type, please contact admin!");
                }
                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    var loyaltyList = taxiEpayDbContext.LoyaltyPromotions.Where(x => x.CompanyKey == companyKey)
                        .ToList();
                    loyaltyListItems = loyaltyList.AsSelectListItems();
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }

            var result = new
            {
                IsError = isError,
                ErrorMessage = errorMessage,
                Data = loyaltyListItems
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetQBRPoint([FromBody] QantasPointApprovalScreenVM searchObject)
        {

            using (var entities = new TaxiEpayEntities())
            {
                entities.SetCommandTimeOut(int.MaxValue);
                var items = entities
                    .Database
                    .SqlQuery<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result>
                        ($"EXEC GetQBRPointForSaleFinByYearMonthAndBusinessType @Year={searchObject.Year}, @Month={searchObject.Month},@BusinessTypeID={searchObject.BusinessTypeID} ");
                //var items = entities
                //    .Database
                //    .SqlQuery<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result>
                //        ($"EXEC GetQBRPointForSaleFinByYearMonthAndBusinessType @Year={searchObject.Year}, @Month={searchObject.Month},@BusinessTypeID={searchObject.BusinessTypeID} ").ToList();

                //if (searchObject.RejectedInforObject != null && searchObject.RejectedInforObject.Count > 0)
                //{
                //    foreach(var item in searchObject.RejectedInforObject)
                //    {
                //        items.FirstOrDefault(x => x.ID == item.ID).Status = item.Status;
                //        items.FirstOrDefault(x => x.ID == item.ID).Comment = item.Comment;
                //    }
                //}
                var qantasServices = new QantasProcessingDA();
                IEnumerable<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> itemsSorter = qantasServices.SortDataListForSaleFin(items, searchObject.SortName, searchObject.SortOrder);

                int totalRecords = itemsSorter.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                var skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;
                var res = itemsSorter.Skip(skip).Take(searchObject.PageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = searchObject.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetQBRPointByAdvanceSearch(QantasPointApprovalScreenVM searchObject)
        {
            var isError = false;
            var errorMessage = String.Empty;
            var searchText = searchObject.SearchText?.Trim();
            try
            {
                if (searchText.Length > 2)
                {
                    using (var entities = new TaxiEpayEntities())
                    {
                        entities.SetCommandTimeOut(int.MaxValue);

                        var items = entities
                            .Database
                            .SqlQuery<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result>
                                ($"EXEC GetQBRPointForSaleFinByYearMonthAndBusinessType @Year={searchObject.Year}, @Month={searchObject.Month},@BusinessTypeID={searchObject.BusinessTypeID} ");

                        List<string> rejectedIdList = null;
                        List<string> rejectedIdListFilter = null;
                        IEnumerable<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> itemsFilter;

                        //search in reject entry list
                        if (searchObject.RejectedInforObject != null && searchObject.RejectedInforObject.Count > 0)
                        {
                            rejectedIdList = searchObject.RejectedInforObject.Select(x => x.ID).ToList();
                            rejectedIdListFilter = searchObject.RejectedInforObject
                                .Where(x => x.Comment.ToLower().Contains(searchText.ToLower()) || x.Status.ToLower().Contains(searchText.ToLower()))
                                .Select(x => x.ID).ToList();
                        }

                        if (rejectedIdListFilter != null)//in case include reject object
                        {
                            itemsFilter = items.Where(x =>
                                x.MemberId.ToString().ToLower().Contains(searchText.ToLower()) || x.QBRNumber.ToLower().Contains(searchText.ToLower()) ||
                                (x.Status.ToLower().Contains(searchText.ToLower()) && !rejectedIdList.Contains(x.ID)) ||
                                (x.Comment.ToLower().Contains(searchText.ToLower()) && !rejectedIdList.Contains(x.ID)) ||
                                x.TradingName.ToLower().Contains(searchText.ToLower()) ||
                                rejectedIdListFilter.Contains(x.ID));
                        }
                        else
                        {
                            itemsFilter = items.Where(x =>
                                x.MemberId.ToString().ToLower().Contains(searchText.ToLower()) || x.QBRNumber.ToLower().Contains(searchText.ToLower()) ||
                                x.Status.ToLower().Contains(searchText.ToLower()) || x.Comment.ToLower().Contains(searchText.ToLower()) || x.TradingName.ToLower().Contains(searchText.ToLower()));
                        }

                        var qantasServices = new QantasProcessingDA();
                        var itemsSorter = qantasServices.SortDataListForSaleFin(itemsFilter, searchObject.SortName, searchObject.SortOrder);

                        int totalRecords = itemsSorter.Count();
                        int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                        var skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;
                        var res = itemsSorter.Skip(skip).Take(searchObject.PageSize);

                        var jsonData = new
                        {
                            total = totalPages,
                            page = searchObject.CurrentPage,
                            records = totalRecords,
                            rows = res.ToList(),
                            IsError = false,
                            ErrorMessage = ""
                        };

                        return Json(jsonData, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    isError = true;
                    errorMessage = "The search text should be least 3 characters.";
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }


            var result = new
            {
                IsError = isError,
                ErrorMessage = errorMessage
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetQBRPointForManagerByAdvanceSearch(QantasPointApprovalScreenVM searchObject)
        {
            var isError = false;
            var errorMessage = String.Empty;
            var searchText = searchObject.SearchText?.Trim();
            try
            {
                if (searchText.Length > 2)
                {
                    using (var entities = new TaxiEpayEntities())
                    {
                        entities.SetCommandTimeOut(int.MaxValue);

                        var items = entities.View_GetQBRPointForManager
                        .Where(x => x.SendMonth == searchObject.Month
                        && x.SendYear == searchObject.Year
                        && x.BusinessTypeID == searchObject.BusinessTypeID
                        && (x.StatusCode == SaleAndFinApprovalStatus || x.StatusCode == SaleAndFinRejectedStatus));

                        var itemsFilter = items.Where(x =>
                               x.MemberId.ToString().ToLower().Contains(searchText.ToLower())
                               || x.QBRNumber.ToLower().Contains(searchText.ToLower())
                               || x.Status.ToLower().Contains(searchText.ToLower())
                               || x.Comment.ToLower().Contains(searchText.ToLower())
                               || x.TradingName.ToLower().Contains(searchText.ToLower()));

                        var qantasServices = new QantasProcessingDA();
                        var itemsSorter = qantasServices.SortDataListForManager(itemsFilter, searchObject.SortName, searchObject.SortOrder);

                        int totalRecords = itemsSorter.Count();
                        int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                        var skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;
                        var res = itemsSorter.Skip(skip).Take(searchObject.PageSize);

                        var jsonData = new
                        {
                            total = totalPages,
                            page = searchObject.CurrentPage,
                            records = totalRecords,
                            rows = res.ToList()
                        };

                        return Json(jsonData, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    isError = true;
                    errorMessage = "The search text should be least 3 characters.";
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }


            var result = new
            {
                IsError = isError,
                ErrorMessage = errorMessage
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GetQBRPointForManager([FromBody] QantasPointApprovalScreenVM searchObject)
        {
            using (var entities = new TaxiEpayEntities())
            {

                var items = entities.View_GetQBRPointForManager
                    .Where(x => x.SendMonth == searchObject.Month
                    && x.SendYear == searchObject.Year
                    && x.BusinessTypeID == searchObject.BusinessTypeID
                    && (x.StatusCode == SaleAndFinApprovalStatus || x.StatusCode == SaleAndFinRejectedStatus));

                var qantasServices = new QantasProcessingDA();
                var itemsSorter = qantasServices.SortDataListForManager(items, searchObject.SortName, searchObject.SortOrder);

                int totalRecords = itemsSorter.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchObject.PageSize);
                var skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;
                var res = itemsSorter.Skip(skip).Take(searchObject.PageSize);

                var totalRejected = itemsSorter.Count(x => x.StatusCode == SaleAndFinRejectedStatus);
                var totalApproval = itemsSorter.Count(x => x.StatusCode == SaleAndFinApprovalStatus);
                var totalPoint = itemsSorter.Sum(n => n.Point);
                var jsonData = new
                {
                    total = totalPages,
                    page = searchObject.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList(),
                    rejected = totalRejected,
                    approval = totalApproval,
                    totalPoint = totalPoint
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult ManagerRejectQantasPoint([FromBody] QantasPointApprovalScreenVM searchObject)
        {
            var isError = false;
            var errorMessage = String.Empty;
            try
            {
                using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                {
                    var reasonRejected = searchObject.ManagerRejectedComment;
                    //var items = taxiEpayDbContext.MemberReferenceEntryForManagerApprovals.Where(x =>
                    //    x.SendMonth == searchObject.Month && x.SendYear == searchObject.Year &&
                    //    x.BusinessTypeID == searchObject.BusinessTypeID && x.StatusCode != ManagerRejectedStatus);

                    var items = taxiEpayDbContext.MemberReferenceEntryForManagerApprovals
                        .Include(x => x.Member)
                        .Join(taxiEpayDbContext.Companies,
                            x => x.BusinessTypeID,
                            company => company.Company_key,
                            (x, company) => new { MemberReferenceEntryForManagerApproval = x, Company = company })
                        .Where(x =>
                            x.MemberReferenceEntryForManagerApproval.SendMonth == searchObject.Month
                            && x.MemberReferenceEntryForManagerApproval.SendYear == searchObject.Year
                            && x.MemberReferenceEntryForManagerApproval.BusinessTypeID == searchObject.BusinessTypeID
                            && (x.MemberReferenceEntryForManagerApproval.StatusCode == SaleAndFinApprovalStatus
                                || x.MemberReferenceEntryForManagerApproval.StatusCode == SaleAndFinRejectedStatus))
                        .Select(n => n.MemberReferenceEntryForManagerApproval);


                    if (items.Any())
                    {
                        //update all QBR to manager rejected state
                        foreach (var item in items)
                        {
                            item.StatusCode = ManagerRejectedStatus;
                            item.Comment = reasonRejected;
                            item.ModifiedDate = DateTime.Now;
                            item.ModifiedBy = ControllerContext.HttpContext.User.Identity.Name;
                        }

                        taxiEpayDbContext.SaveChanges();

                        //hadle send email to sale to notify
                        string sparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
                        string recipient = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.SaleRecipient.ToString());
                        string sparkPostEmailTemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.RejectSparkPostTemplateID.ToString());

                        dynamic emailDto = new ExpandoObject();
                        emailDto.TemplateID = sparkPostEmailTemplateID;
                        emailDto.SparkPostAPIKey = sparkPostAPIKey;
                        emailDto.Recipient = recipient;
                        var dynamicData = new Dictionary<string, object>()
                        {
                            {"Month", searchObject.Month},
                            {"Year", searchObject.Year},
                            {"ManagerName", ControllerContext.HttpContext.User.Identity.Name}
                        };
                        emailDto.CustomMetaData = dynamicData;

                        Common.Common.SendEmail(emailDto);
                    }
                    else
                    {
                        isError = true;
                        errorMessage = "No one Qantas Point records to Reject, please check. ";
                    }
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }

            var data = new
            {
                IsError = isError,
                ErrorMessage = errorMessage
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public FileResult GetQBRExportFileForManager(int month, int year, int businesTypeId)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                

                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}Qantas Point Generate List Manager Review";
                IList<ReportingWebService.ParameterValue> parameters = new List<ReportingWebService.ParameterValue>();

                parameters.Add(new ReportingWebService.ParameterValue { Name = "Month", Value = month.ToString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "Year", Value = year.ToString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "BusinessTypeID", Value = businesTypeId.ToString() });

                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                logger.Fatal("Error generate excel:" + e.Message, e);
                return File(new byte[] { }, "application/vnd.ms-excel", "Qantas Point Generate List Manager Review.xls");
            }

        }

        private static object Lock = new object();
        public JsonResult SendQantasPointToApproval([FromBody] QantasPointApprovalScreenVM QantasPointData)
        {
            var isError = false;
            var errorMessage = String.Empty;
            try
            {
                //Forced using this method only one at the time.
                lock (Lock)
                {
                    using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                    {
                        //get Qantas point data from view base on search filter
                        var QantasPointFromSearch = taxiEpayDbContext
                            .Database
                            .SqlQuery<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result>
                                ($"EXEC GetQBRPointForSaleFinByYearMonthAndBusinessType @Year={QantasPointData.Year}, @Month={QantasPointData.Month},@BusinessTypeID={QantasPointData.BusinessTypeID} ")
                            .ToList();

                        var rejectedItemsList = QantasPointData.RejectedInforObject?.Select(x => x.ID);
                        var lstqbrPoint = QantasPointFromSearch.Select(n => n.ID);

                        // This is prevent concurrent user, in case 2 users perform reject records at the same times, the data will be different.
                        if (!QantasPointFromSearch.Any() || (rejectedItemsList != null && rejectedItemsList.Any(n => !lstqbrPoint.Contains(n))))
                        {
                            var dataRes = new
                            {
                                IsError = true,
                                ErrorMessage = "Data has been processing by another member. Please refresh the page and try again."
                            };
                            return Json(dataRes, JsonRequestBehavior.AllowGet);
                        }

                        foreach (var point in QantasPointFromSearch)
                        {
                            var entryApprovalKey = taxiEpayDbContext
                                .Database
                                .SqlQuery<int>($"EXEC GetMemberEntryForManagerApprovalKey @pointID = '{point.ID}', @businessTypeID = {point.BusinessTypeID}")
                                .FirstOrDefault();

                            //get rejected information for current Qantas point from search filter
                            var rejectedItem = QantasPointData.RejectedInforObject?.FirstOrDefault(x =>
                                point.ID.Equals(x.ID));

                            //check the entry was calculate before
                            if (entryApprovalKey > 0) //is existing
                            {
                                var entryApproval = taxiEpayDbContext.MemberReferenceEntryForManagerApprovals
                                    .FirstOrDefault(
                                        n => n.MemberReferenceEntryForManagerApprovalKey == entryApprovalKey);

                                if (rejectedItem == null)// is approval by sale 
                                {
                                    entryApproval.StatusCode = SaleAndFinApprovalStatus;
                                    entryApproval.Comment = String.Empty;
                                }
                                else //is rejected by sale
                                {
                                    entryApproval.Comment = rejectedItem.Comment;
                                    entryApproval.StatusCode = SaleAndFinRejectedStatus;
                                }

                                entryApproval.SendMonth = QantasPointData.Month;
                                entryApproval.SendYear = QantasPointData.Year;
                            }
                            else //add new entry approval
                            {
                                //create new entry approval
                                var qantasApprovalObject = point.ToMemberReferenceEntryForManagerApproval();
                                qantasApprovalObject.CreatedBy = qantasApprovalObject.ModifiedBy =
                                    ControllerContext.HttpContext.User.Identity.Name;
                                if (rejectedItem == null)// is approval by sale 
                                {
                                    qantasApprovalObject.StatusCode = SaleAndFinApprovalStatus;
                                }
                                else //is rejected by sale
                                {
                                    qantasApprovalObject.Comment = rejectedItem.Comment;
                                    qantasApprovalObject.StatusCode = SaleAndFinRejectedStatus;
                                }
                                qantasApprovalObject.SendMonth = QantasPointData.Month;// update the month is send to Qantas
                                qantasApprovalObject.SendYear = QantasPointData.Year;

                                // link entry with new entry approval
                                var lst = point.MemberReferenceEntryKeyList.Split(',').ToList();
                                var entryList = taxiEpayDbContext.MemberReferenceEntries.Where(x =>
                                    lst.Contains(x.MemberReferenceEntryKey.ToString()));
                                qantasApprovalObject.MemberReferenceEntries.AddRange(entryList);

                                taxiEpayDbContext.MemberReferenceEntryForManagerApprovals.Add(qantasApprovalObject);
                            }
                        }

                        taxiEpayDbContext.SaveChanges();

                        //handle send email to manager to notify
                        string sparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
                        string recipient = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ManagerRecipient.ToString());
                        string sparkPostEmailTemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ApprovedSparkPostTemplateID.ToString());

                        dynamic emailDto = new ExpandoObject();
                        emailDto.TemplateID = sparkPostEmailTemplateID;
                        emailDto.SparkPostAPIKey = sparkPostAPIKey;
                        emailDto.Recipient = recipient;
                        var dynamicData = new Dictionary<string, object>()
                                {
                                    {"Month", QantasPointData.Month},
                                    {"Year", QantasPointData.Year},
                                };
                        emailDto.CustomMetaData = dynamicData;

                        Common.Common.SendEmail(emailDto);
                    }
                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }

            var data = new
            {
                IsError = isError,
                ErrorMessage = errorMessage
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ManagerSendQBRToQantas(QantasPointApprovalScreenVM searchObject)
        {
            var isError = false;
            var errorMessage = String.Empty;
            try
            {
                lock (Lock)
                {
                    using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
                    {
                        var items = taxiEpayDbContext.MemberReferenceEntryForManagerApprovals
                            .Include(n => n.MemberReferenceEntries)
                            .Join(taxiEpayDbContext.Companies,
                                x => x.BusinessTypeID,
                                company => company.Company_key,
                                (x, company) => new { MemberReferenceEntryForManagerApproval = x, Company = company })
                            .Where(x =>
                                x.MemberReferenceEntryForManagerApproval.SendMonth == searchObject.Month
                                && x.MemberReferenceEntryForManagerApproval.SendYear == searchObject.Year
                                && x.MemberReferenceEntryForManagerApproval.BusinessTypeID == searchObject.BusinessTypeID
                                && (x.MemberReferenceEntryForManagerApproval.StatusCode == SaleAndFinApprovalStatus
                                    || x.MemberReferenceEntryForManagerApproval.StatusCode == SaleAndFinRejectedStatus))
                            .Select(n => n.MemberReferenceEntryForManagerApproval)
                            .ToList();

                        if (items.Any())
                        {
                            var qantasServices = new QantasProcessingDA();
                            qantasServices.PrepareEntryDataToSendQBR(taxiEpayDbContext, items, User.Identity.Name);

                            taxiEpayDbContext.SaveChanges();
                        }
                        else
                        {
                            isError = true;
                            errorMessage = "Data has been processing by another member. Please refresh the page and try again.";
                        }
                    }

                }
            }
            catch (Exception e)
            {
                isError = true;
                errorMessage = e.Message;
            }

            var data = new
            {
                IsError = isError,
                ErrorMessage = errorMessage
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RenderReport(string idList)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;

                MemberDA mda = new MemberDA();

                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}Qantas Point Detail";
                IList<ReportingWebService.ParameterValue> parameters = new List<ReportingWebService.ParameterValue>();

                parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberReferenceEntryKeyList", Value = idList });
                string format = "PDF";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                return new FileContentResult(output, mimeType);
            }
            catch (Exception e)
            {
                logger.Fatal(e.Message, e);
                return new FileContentResult(new byte[] { }, "application/pdf");
            }



        }


        public FileResult GetQBRExportFile(int month, int year, int businesTypeId)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                MemberDA mda = new MemberDA();
                

                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}Qantas Point Generate List";

                IList<ReportingWebService.ParameterValue> parameters = new List<ReportingWebService.ParameterValue>();

                parameters.Add(new ReportingWebService.ParameterValue { Name = "Month", Value = month.ToString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "Year", Value = year.ToString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "BusinessTypeID", Value = businesTypeId.ToString() });

                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                logger.Fatal("Error generate excel:" + e.Message, e);
                return File(new byte[] { }, "application/xlsx");
            }

        }

        public ActionResult QantasPointReview()
        {
            var model = new QantasPointFileReviewingVM()
            {
                Month = DateTime.Now.Month,
                Year = DateTime.Now.Year
            };

            model.Years = Enumerable.Range(DateTime.Now.Year - 9, 10).ToList().AsSelectListItems();
            model.Months = DateTimeFormatInfo.CurrentInfo.MonthNames
                .Where(n => !string.IsNullOrEmpty(n))
                .Select((n, i) => new SelectListItem()
                {
                    Text = n,
                    Value = (i + 1).ToString(),
                    Selected = (i + 1) == DateTime.Now.Month
                });
            var qantasProcessingDA = new QantasProcessingDA();
            model.QantasFileNameList = qantasProcessingDA.GetQantasExportFileListForMonthAndYear(model.Month, model.Year).AsSelectListItem();
            return View(model);
        }

        public ActionResult GetQantasFileNameBaseOnYearAndMonth(int year, int month)
        {
            var qantasProcessingDA = new QantasProcessingDA();
            var data = qantasProcessingDA.GetQantasExportFileListForMonthAndYear(month, year).AsSelectListItem();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQantasPointFileContent(QantasPointFileReviewingVM model)
        {
            using (TaxiEpayEntities taxiEpayDbContext = new TaxiEpayEntities())
            {
                var items = taxiEpayDbContext.View_QantasPointFileReviewingDetail
                    .Where(n => n.QantasExportFileKey == model.QantasFileID);

                //for advance search
                IQueryable<View_QantasPointFileReviewingDetail> itemsFilter;
                var searchText = model.SearchText?.Trim();
                if (!string.IsNullOrEmpty(searchText))
                {
                    itemsFilter = items.Where(x =>
                               x.MemberId.ToLower().ToString().Contains(searchText.ToLower())
                               || x.QBRNumber.ToLower().Contains(searchText.ToLower())
                               || x.Status.ToLower().Contains(searchText.ToLower())
                               || x.Comment.ToLower().Contains(searchText.ToLower())
                               || x.TradingName.ToLower().Contains(searchText.ToLower()));
                }
                else
                {
                    itemsFilter = items;
                }

                //handle sorting 
                var qantasProcessingDA = new QantasProcessingDA();
                var itemSorter = qantasProcessingDA.SortDataListForReviewing(itemsFilter, model.SortName, model.SortOrder);

                int totalRecords = itemSorter.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)model.PageSize);
                var skip = (model.CurrentPage - 1) * model.PageSize;
                var res = itemSorter.Skip(skip).Take(model.PageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = model.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }


        public FileResult GetQBRExportFileReviewing(int fileId)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}Qantas Point Generate List File Reviewing";

                IList<ReportingWebService.ParameterValue> parameters = new List<ReportingWebService.ParameterValue>();

                parameters.Add(new ReportingWebService.ParameterValue { Name = "QantasFileID", Value = fileId.ToString() });

                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                logger.Fatal("Error generate excel:" + e.Message, e);
                return File(new byte[] { }, "application/xlsx");
            }

        }

    }



}
