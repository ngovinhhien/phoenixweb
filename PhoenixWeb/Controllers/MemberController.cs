﻿using CustomerPortalDataAccess;
using CustomerPortalDataAccess.Entities;
using LGHelper;
using LGMail;
using LGMail.Interface;
using LiveEncryption;
using Newtonsoft.Json;
using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using Phoenix_BusLog.Salesforce;
using Phoenix_BusLog.TransactionHost.BusLogs;
using Phoenix_Service;
using Phoenix_Service.Extension;
using PhoenixObjects.CommissionRate;
using PhoenixObjects.Common;
using PhoenixObjects.Customer;
using PhoenixObjects.Enums;
using PhoenixObjects.Fee;
using PhoenixObjects.Financial;
using PhoenixObjects.LiveSMS;
using PhoenixObjects.Member;
using PhoenixObjects.Member.CommissionRate;
using PhoenixObjects.Member.Search;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Salesforce;
using PhoenixObjects.Terminal;
using PhoenixObjects.User;
using PhoenixObjects.UserAuthentication;
using PhoenixWeb.Common;
using PhoenixWeb.Helper;
using PhoenixWeb.Models.APIModels;
using PhoenixWeb.Models.Contants;
using PhoenixWeb.Models.ViewModel;
using PhoenixWeb.Models.ViewModel.Member.Search;
using PhoenixWeb.ReportingWebService;
using RestSharp;
using SalesforceSharp;
using SalesforceSharp.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PhoenixWeb.Models.ViewModel.PricingPlan;
using PhoenixWeb.Models.ViewModel.DriverCard;
using LGHelper.PhoenixWebAPI;
using Phoenix_TransactionHostUI;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class MemberController : PhoenixWebController
    {
        private int minimumAgeAllow = int.Parse(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MandatoryMinimumAgeAllow.ToString()).ToString());

        private List<Dictionary<string, object>> PayMethodList = new List<Dictionary<string, object>>()
            {
                new Dictionary<string, object>() { { "elemId", "Debit" }, { "CartType", "Debit" }, { "Rate", 0 }, { "RateOption", 0 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "Visa" }, { "CartType", "Visa" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "VisaDebit" }, { "CartType", "Visa Debit" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 1 } },
                new Dictionary<string, object>() { { "elemId", "MasterCard" }, { "CartType", "Mastercard" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "MasterCardDebit" }, { "CartType", "Mastercard Debit" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 1 } },
                new Dictionary<string, object>() { { "elemId", "Amex" }, { "CartType", "AMEX" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "Diners" }, { "CartType", "Diners" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "UnionPay" }, { "CartType", "UnionPay" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "ZipPay" }, { "CartType", "Zip" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "Qantas" }, { "CartType", "Qantas" }, { "Rate", 0 }, { "RateOption", 0 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "Alipay" }, { "CartType", "Alipay" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
                new Dictionary<string, object>() { { "elemId", "JCB" }, { "CartType", "JCB" }, { "Rate", 0 }, { "RateOption", 1 }, { "TransactionFee", 0 }, { "IsNewPayMethod", 0 } },
            };

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Menu()
        {
            Session.Clear();
            return View();
        }
        public List<SAccount> recordAccountGlobalList = new List<SAccount>();
        public List<SContact> recordLeadGlobalList = new List<SContact>();
        public List<SLead> recordOpportGlobalList = new List<SLead>();

        private List<SAccount> SalesForceConnect(string sfName, string type)
        {
            var client = new SalesforceClient();
            //Setup > Personal Setup > My Personal Information > Reset My Security Token.
            SalesforceDA sda = new SalesforceDA();
            SalesforceDetailModel cred = new SalesforceDetailModel();
            cred = sda.GetCredentials();


            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var authFlow = new UsernamePasswordAuthenticationFlow(cred.ClientID, cred.Secret, cred.UserName, cred.TokenPassword);
            client.Authenticate(authFlow);
            IList<SAccount> records = null;
            if (!String.IsNullOrEmpty(sfName))
            {
                string name = sfName;
                if (sfName.IndexOf("'") >= 0)
                {
                    name = sfName.Remove(sfName.IndexOf("'"));
                }
                if (type == "1")
                {
                    //order is important
                    //string name = sfName.Remove(sfName.IndexOf("'"));
                    string query = String.Format("SELECT Id, AccountNumber,Name,Description,Industry,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,Phone FROM Account where Name like '{0}%'", name);
                    records = client.Query<SAccount>(query);
                }
                if (type == "2")
                {
                    string query = String.Format("SELECT Id, AccountNumber,Name,Description,Industry,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,Phone FROM Account where Phone like '%{0}%'", name);
                    records = client.Query<SAccount>(query);
                    //records = client.Query<SAccount>("SELECT Id, AccountNumber,Name,Description,Industry,ShippingCity,ShippingCountry,ShippingState,ShippingPostalCode,ShippingStreet,Phone FROM Account where Phone like '%" + name + "%'");
                }
                foreach (var r in records)
                {
                    SAccount a = new SAccount();
                    a.Id = r.Id;
                    a.AccountNumber = r.AccountNumber;
                    a.Name = r.Name;
                    a.Description = r.Description;
                    a.Industry = r.Industry;
                    a.ShippingCity = r.ShippingCity;
                    a.ShippingCountry = r.ShippingCountry;
                    a.ShippingState = r.ShippingState;
                    a.ShippingPostalCode = r.ShippingPostalCode;
                    a.ShippingStreet = r.ShippingStreet;
                    a.Phone = r.Phone;

                    recordAccountGlobalList.Add(a);
                }
            }
            Session["recordAccountGlobalList"] = recordAccountGlobalList;
            return recordAccountGlobalList;
        }

        private void SalesForceConnect2(string AccountId)
        {
            var client = new SalesforceClient();
            //Setup > Personal Setup > My Personal Information > Reset My Security Token.
            SalesforceDA sda = new SalesforceDA();
            SalesforceDetailModel cred = new SalesforceDetailModel();
            cred = sda.GetCredentials();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var authFlow = new UsernamePasswordAuthenticationFlow(cred.ClientID, cred.Secret, cred.UserName, cred.TokenPassword);
            client.Authenticate(authFlow);

            IList<SContact> leadRecords = null;
            IList<SLead> lead = null;
            leadRecords = client.Query<SContact>("SELECT AccountId, FirstName, LastName,Email,MasterRecordId,Name,Birthdate,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet,MobilePhone,Fax,Phone FROM Contact where AccountId = '" + AccountId + "'");
            //lead = client.Query<SLead>("SELECT AccountId, Amex_Diners__c,Average_transaction__c,Diners__c,Debit__c,EFTPOS_turnover__c,Terminal_Rental__c,Visa_Mastercard__c,Monthly_Rental__c,Flat_Rate__c,Quoted_by__c FROM Opportunity where AccountId = '" + AccountId + "'");
            lead = client.Query<SLead>("SELECT AccountId, Amex_Diners__c,Average_transaction__c,Diners__c,Debit__c,EFTPOS_turnover__c,Terminal_Rental__c,Visa_Mastercard__c,Unionpay__c,Zip__c,Alipay__c,Qantas__c,Monthly_Rent__c,Flat_Rate__c,Quoted_by__c, New_Free_Rental__c FROM Opportunity where AccountId = '" + AccountId + "'");

            foreach (var r in leadRecords)
            {
                SContact a = new SContact();
                a.AccountId = r.AccountId;
                a.FirstName = r.FirstName;
                a.LastName = r.LastName;
                a.Email = r.Email;
                a.MasterRecordId = r.MasterRecordId;
                a.Name = r.Name;
                a.MailingCity = r.MailingCity;
                a.MailingCountry = r.MailingCountry;
                a.MailingPostalCode = r.MailingPostalCode;
                a.MailingState = r.MailingState;
                a.MailingStreet = r.MailingStreet;
                a.MobilePhone = r.MobilePhone;
                a.Phone = r.Phone;
                a.Fax = r.Fax;
                recordLeadGlobalList.Add(a);
            }
            foreach (var r in lead)
            {
                SLead a = new SLead();
                a.AccountId = r.AccountId;
                a.Amex_Diners__c = -r.Amex_Diners__c;
                a.Diners__c = -r.Diners__c;
                a.Average_transaction__c = r.Average_transaction__c;
                a.Debit__c = -r.Debit__c;
                a.EFTPOS_turnover__c = r.EFTPOS_turnover__c;
                //turn over range
                a.Terminal_Rental__c = r.Terminal_Rental__c;
                a.Visa_Mastercard__c = -r.Visa_Mastercard__c;
                // a.Sliding_Scale__c = r.Sliding_Scale__c;
                //a.Monthly_Rental__c = r.Monthly_Rental__c;
                a.Monthly_Rent__c = r.Monthly_Rent__c;
                a.Flat_Rate__c = r.Flat_Rate__c;
                a.Quoted_by__c = r.Quoted_by__c;
                a.New_Free_Rental__c = r.New_Free_Rental__c;
                a.Unionpay__c = r.Unionpay__c;
                a.Zip__c = r.Zip__c;
                a.Alipay__c = r.Alipay__c;
                a.Qantas__c = r.Qantas__c;
                recordOpportGlobalList.Add(a);
            }
        }
        public ActionResult SalesforceAddMemeber(string Id)
        {
            recordAccountGlobalList = (List<SAccount>)Session["recordAccountGlobalList"];
            SAccount ac = recordAccountGlobalList.Find(r => r.Id == Id);

            List<SForceModel> modelSF = new List<SForceModel>();
            SContact ld = new SContact();
            SLead led = new SLead();
            SalesForceConnect2(Id);

            if (ac != null)
            {
                SForceModel sforceModel = new SForceModel();
                ld = recordLeadGlobalList.Find(r => r.AccountId == Id);
                led = recordOpportGlobalList.Find(r => r.AccountId == Id);

                MemberModel mem = new MemberModel();
                mem.SalesforceId = Id;
                mem.CompanyKey = 18;

                if (ld != null)
                {
                    if (ld.FirstName != null)
                        mem.FirstName = ld.FirstName.Substring(0, 1).ToUpper() + ld.FirstName.Substring(1);
                    if (ld.LastName != null)
                        mem.LastName = ld.LastName.Substring(0, 1).ToUpper() + ld.LastName.Substring(1);
                    mem.Email = ld.Email;
                    if (ld.Phone != null)
                    {
                        string preMob = ld.Phone.Substring(0, 2);

                        if (preMob == "04")
                        {
                            mem.Mobile = ld.Phone.Trim();
                        }
                        else
                        {
                            mem.Telephone = ld.Phone.Trim();
                        }

                    }
                    if (ld.MobilePhone != null)
                        mem.Mobile = ld.MobilePhone.Trim();
                    mem.TradingName = ac.Name;
                    mem.Fax = ld.Fax;

                    if (Convert.ToDateTime(ld.Birthdate) != DateTime.MinValue)
                    {
                        mem.DOB = Convert.ToDateTime(ld.Birthdate);
                    }
                }

                if (led != null)
                {
                    if (led.Debit__c != null)
                        mem.CRDebitRate = led.Debit__c.Value;
                    if (led.Debit__c != null)
                        mem.CRDebitRatePercentage = led.Debit__c.Value;
                    if (led.Visa_Mastercard__c != null)
                        mem.CRVisaRate = led.Visa_Mastercard__c.Value;
                    if (led.Visa_Mastercard__c != null)
                        mem.CRMasterCardRate = led.Visa_Mastercard__c.Value;
                    if (led.Amex_Diners__c != null)
                        mem.CRAmexRate = led.Amex_Diners__c.Value;
                    if (led.Diners__c != null)
                        mem.CRDinersRate = led.Diners__c.Value;
                    if (led.Unionpay__c != null)
                        mem.CRUnionPayRate = led.Unionpay__c.Value;
                    if (led.Zip__c != null)
                        mem.CRZipPayRate = led.Zip__c.Value;
                    if (led.Qantas__c != null)
                        mem.CRQantasRate = led.Qantas__c.Value;
                    if (led.Qantas__c != null)
                        mem.CRQantasRatePercentage = led.Qantas__c.Value;
                    if (led.Alipay__c != null)
                        mem.CRAlipayRate = led.Alipay__c.Value;
                    if (led.Average_transaction__c != null)
                        mem.AverageTransaction = led.Average_transaction__c.Value;

                    if (led.EFTPOS_turnover__c != null)
                        mem.AverageTurnover = led.EFTPOS_turnover__c.Value;

                    //string mRental = led.Monthly_Rental__c;
                    string mRental = null; //New Salesforce field doesn't have rental template anymore.
                    if (mRental != null)
                    {
                        MemberRentalTemplateDA mrda = new MemberRentalTemplateDA();
                        mem.RentalMemberRentalTemplateKey = mrda.GetKeyByName(mRental);
                    }
                    else
                    {
                        MemberRentalTemplateDA mrda = new MemberRentalTemplateDA();
                        mem.RentalMemberRentalTemplateKey = mrda.GetKeyByName("LiveEftpos Default");
                    }

                    if (led.Flat_Rate__c != null)
                    {
                        mem.IsFlatRate = led.Flat_Rate__c;
                    }
                    else
                    {
                        mem.IsFlatRate = false;
                    }

                    if (led.Monthly_Rent__c != null)
                    {
                        //mem.RentalAmountFromSalesforce = led.Terminal_Rental__c;
                        mem.RentalAmountFromSalesforce = led.Monthly_Rent__c;
                    }

                    var range = led.EFTPOS_turnover__c;
                    RentalTurnOverDA rda = new RentalTurnOverDA();
                    if (range != null)
                        mem.RentalTurnoverRangeKeyFromSalesforce = rda.GetTurnOverRangeKey(range.Value);


                    if (led.Quoted_by__c != null)
                    {
                        EmployeeDA eda = new EmployeeDA();
                        int emp_key = eda.GetEmployeeByName(led.Quoted_by__c);
                        mem.Employee_key = emp_key;
                    }

                    if (!String.IsNullOrEmpty(led.New_Free_Rental__c))
                    {
                        int freerentmonth = 0;
                        if (Int32.TryParse(led.New_Free_Rental__c, out freerentmonth))
                        {
                            if (freerentmonth > 0)
                            {
                                DateTime dt = DateTime.Today;
                                mem.RentalRentFree = true;
                                mem.RentalStartDate = new DateTime(dt.Year, dt.Month, 1); //get the first day of current month
                                mem.RentalEndDate = new DateTime(dt.AddMonths(freerentmonth).Year, dt.AddMonths(freerentmonth).Month, 1).AddDays(-1); //get the last day of the coming months
                                Session["RentalStartDateString"] = mem.RentalStartDateString;
                                Session["RentalEndDateString"] = mem.RentalEndDateString;
                                //mem.MemberRentalModelList = new List<MemberRentalModel>();
                                //MemberRentalModel mrm = new MemberRentalModel();
                                //mrm.RentFree = true;

                                //mrm.StartDate = new DateTime(dt.Year, dt.Month, 1); //get the first day of current month
                                //mrm.EndDate = new DateTime(dt.AddMonths(freerentmonth).Year, dt.AddMonths(freerentmonth).Month, 1).AddDays(-1); //get the last day of the coming months
                                //mrm.Modified_dttm = DateTime.Now;
                                //mrm.ModifiedByUser = User.Identity.Name;
                                //mem.MemberRentalModelList.Add(mrm);
                            }

                        }
                    }
                }
                else
                {
                    //no opportunities found in the salesforce so use default
                    CommissionRateDA cda = new CommissionRateDA();
                    DefaultRateModel drt = new DefaultRateModel();
                    drt = cda.GetDefaultCommissionRate(18, 19);

                    mem.CRDebitRate = drt.Debit;
                    mem.CRDebitRatePercentage = drt.DebitPercentage;
                    mem.CRVisaRate = drt.Visa;
                    mem.CRMasterCardRate = drt.Master;
                    mem.CRAmexRate = drt.Amex;
                    mem.CRDinersRate = drt.Diners;
                    mem.CRUnionPayRate = drt.UnionPay;
                    mem.CRZipPayRate = drt.Zip;
                    mem.CRQantasRate = drt.Qantas;
                    mem.CRQantasRatePercentage = drt.QantasPercentage;
                    mem.CRAlipayRate = drt.Alipay;
                    mem.RentalMemberRentalTemplateKey = 7;
                }

                sforceModel.memberModel = mem;
                // sforceModel.
                if (ld != null)
                {
                    if (ld.MailingStreet != null)
                    {
                        string[] str = ld.MailingStreet.Split(null);
                        AddressListModel addrModel = new AddressListModel();
                        addrModel.Member = mem;

                        if (str[0] != null)
                        {
                            addrModel.StreetNumber = str[0];
                        }
                        if (str.Length > 1)
                        {
                            if (str[1] != null)
                            {
                                addrModel.StreetName = str[1];
                            }
                        }
                        sforceModel.addrModel = addrModel;
                    }
                }
                modelSF.Add(sforceModel);
            }


            Session["SalesforceMember"] = modelSF;
            MemberDA mda = new MemberDA();
            Session["AdrList"] = null;
            Session["BAccountList"] = null;
            Session["MemberRentalModelList"] = null;
            Session["TerminalAllocationList"] = null;
            //bool IsMember = mda.GetMemeberBySalesforceId(Id);
            //if (IsMember == false)
            //{
            List<SForceModel> SFmodel = (List<SForceModel>)Session["SalesforceMember"];
            SForceModel sforceModel1 = new SForceModel();
            MemberModel model = new MemberModel();
            sforceModel1 = SFmodel.Where(r => r.memberModel.SalesforceId == Id).FirstOrDefault();
            model = sforceModel1.memberModel;
            Session["SalesforceMemberAddress"] = sforceModel1.addrModel;
            Session["SalesforceMemberModel"] = sforceModel1.memberModel;
            Session["SalesforceRentalModel"] = sforceModel1.memberRentalModel;

            model.IsBankCustomer = true;
            if (model.DOB != null)
            {
                string dobDate = model.DOB.Value.ToShortDateString();
                string[] dobDateArry = dobDate.Split('/');
                model.DOBstring = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            }
            return RedirectToAction("Create", model);
        }



        public ActionResult GetSFName()
        {
            return Json(recordAccountGlobalList.OrderBy(c => c.Name), JsonRequestBehavior.AllowGet);
        }
        [RBAC(Permission = "MemberSForceSearch")]
        public ActionResult SalesforceSearch()
        {
            List<SAccount> model = new List<SAccount>();

            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberSForceSearch")]
        public ActionResult SalesforceSearch(FormCollection c)
        {
            var name = c["Name"].ToString();
            var type = c["Type"].ToString();
            List<SAccount> model = new List<SAccount>();
            model = SalesForceConnect(name, type);
            return View(model);
        }

        public ActionResult AutoCompleteEftTerminalId(string term)
        {
            List<string> m = new List<string>();
            TerminalDA cda = new TerminalDA();
            var model = cda.EftTerminalsAutoComplate(term).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompletePostCode(string term)
        {
            List<string> m = new List<string>();
            PostCodeDA cda = new PostCodeDA();
            var model = cda.GetStatePostCodeLocality(term).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteDealer(string term)
        {
            List<string> dealerList = new List<string>();
            DealerDA cda = new DealerDA();
            dealerList = cda.GetDealerAutoComplete(term).ToList();

            return Json(dealerList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string term)
        {
            List<string> memberList = new List<string>();
            EmployeeDA cda = new EmployeeDA();
            memberList = cda.GetEmployeeAutoComplete(term).ToList();

            return Json(memberList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteSearchTypeNumber(string term)
        {
            List<string> m = new List<string>();
            m.Add("MemberId");
            m.Add("TerminalId");
            m.Add("Mobile");
            m.Add("BusinessId");
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteSearchTypeString(string term)
        {
            List<string> m = new List<string>();
            m.Add("First Name");
            m.Add("Last Name");
            m.Add("Email");
            m.Add("Trading Name");
            m.Add("State");
            m.Add("Suburb");
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        private void DrawDropDownConfigAllocateTerminal(int companyKey = 0, int subBusinessTypeKey = 0)
        {
            StateDA sda = new StateDA();
            var states = sda.GetStateAustralia().ToList();
            ViewBag.StateList = states.Select(item => new SelectListItem
            {
                Text = item.StateName,
                Value = item.StateKey.ToString()
            }).ToList();

            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get(companyKey).ToList();
            ViewBag.VehicleTypeList = VehicleType.Select(item => new CustomHelpers.CustomSelectItem
            {
                Text = item.Name,
                Value = item.VehicleType_key.ToString(),
                Selected = item.VehicleType_key == subBusinessTypeKey
            }).ToList();

            EmployeeDA eda = new EmployeeDA();
            var EmployeeList = eda.Get().ToList();
            EmployeeModel emodel = new EmployeeModel();
            emodel.FirstName = " ";
            emodel.Employee_key = 0;
            EmployeeList.Add(emodel);
            ViewBag.EmployeeList = EmployeeList.Select(item => new SelectListItem
            {
                Text = item.FirstName + " " + item.LastName,
                Value = item.Employee_key.ToString()
            }).ToList().OrderBy(n => n.Text);
        }

        public ActionResult AutoCompleteBSBSearch(string term)
        {
            List<string> m = new List<string>();
            BsbListDA bda = new BsbListDA();
            List<BSBListModel> model = new List<BSBListModel>();
            m = bda.GetSerach(term);
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        private void DrawDropDownBsb()
        {
            BsbListDA bda = new BsbListDA();
            List<BSBListModel> model = new List<BSBListModel>();
            model = bda.Get();

            ViewBag.Bsb = model.Select(item => new SelectListItem
            {
                Text = item.Bank + " " + item.BranchName + " " + item.State,
                Value = item.BSB
            }).ToList();


        }
        private void DrawDropDownConfig(int company_key, bool isFilterBusinessType = false, bool isCreateMemberScreen = false, bool isExcludeDriverCard = false, bool isFeeFree = false)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();

            if (!isFeeFree)
            {
                int.TryParse(Caching.GetSystemConfigurationByKey("FeefreeRentalTemplateKey"),
                    out int feeFreeRentalTemplateKey);
                MemberRentalTemplateList = MemberRentalTemplateList.Where(r =>
                    r.MemberRentalTemplateKey != feeFreeRentalTemplateKey).ToList();
            }
            
            ViewBag.MemberRentalTemplateList = MemberRentalTemplateList.Select(item => new SelectListItem
            {
                Text = item.Description,
                Value = item.MemberRentalTemplateKey.ToString()
            }).ToList();

            using (var taxiEpay = new TaxiEpayEntities())
            {
                if (isExcludeDriverCard)
                {
                    ViewBag.PaymentTypeList = taxiEpay.SystemConfigurations
                           .Where(s => s.ConfigurationKey == "PaymentTypes")
                           .FirstOrDefault()
                           .ConfigurationValue
                           .Split(',')
                           .Where(n => n != "Card")
                           .Select(pt => new SelectListItem
                           {
                               Text = pt,
                               Value = pt,
                           })
                           .ToList();
                }
                else
                {
                    ViewBag.PaymentTypeList = taxiEpay.SystemConfigurations
                        .Where(s => s.ConfigurationKey == "PaymentTypes")
                        .FirstOrDefault()
                        .ConfigurationValue
                        .Split(',')
                        .Select(pt => new SelectListItem
                        {
                            Text = pt,
                            Value = pt,
                        })
                        .ToList();
                }

            }

            BusinessTypeDA bda = new BusinessTypeDA();
            var BusinessTypeList = bda.Get(isFilterBusinessType).ToList();
            VehicleTypeDA vtda = new VehicleTypeDA();
            var vehicles = vtda.GetAllVehiclesForAllBusiness(BusinessTypeList.Select(x => x.BusinessTypeKey).ToList());
            ViewBag.VehicleDtoAsJson = JsonConvert.SerializeObject(vehicles);
            if (isCreateMemberScreen)
            {
                var merchantIdDefaultForEachBusinessTypeConfiguration = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MerchantIdDefaultForEachBusinessType.ToString());
                if (merchantIdDefaultForEachBusinessTypeConfiguration != null)
                {
                    var businessTypeModels = JsonConvert.DeserializeObject<List<BusinessTypeModel>>(merchantIdDefaultForEachBusinessTypeConfiguration);
                    foreach (var item in businessTypeModels)
                    {
                        var businessType = BusinessTypeList.FirstOrDefault(x => x.BusinessTypeKey == item.BusinessTypeKey);
                        if (businessType != null)
                        {
                            businessType.MerchantId = item.MerchantId;
                        }
                    }
                }

                ViewBag.BusinessTypeList = Extensions.AsSelectListItemCustom(BusinessTypeList);
            }
            else
            {
                ViewBag.BusinessTypeList = Extensions.AsSelectListItem(BusinessTypeList, true);
            }

            DealerDA dda = new DealerDA();
            var DealerList = dda.GetDealersByCompanykey(company_key).ToList();
            ViewBag.Dealers = DealerList.Select(item => new SelectListItem
            {
                Text = item.DealerName,
                Value = item.Dealer_key.ToString()
            }).ToList();

            FeeDA fda = new FeeDA();
            var fees = fda.GetFees();
            ViewBag.Fees = fees.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.ID.ToString()
            });
            ViewBag.ListEOMMSFPaymentEnableBusinessType = Caching.GetSystemConfigurationByKey("EOMMSFPaymentEnableType");
        }

        public ActionResult SalesforceDulpicateAddDialog(string Id)
        {
            MemberDA mda = new MemberDA();
            bool IsMember = mda.GetMemeberBySalesforceId(Id);
            if (IsMember == true)
            {
                ViewBag.SalesforceId = Id;
                ViewBag.IsDuplicate = true;
                ViewBag.Title = "Add Duplicate";
                ViewBag.Msg = "Member already exists in Phoenix, Do you want to create another?";
                return View();
            }
            else
            {
                ViewBag.SalesforceId = Id;
                ViewBag.IsDuplicate = false;
                ViewBag.Title = "Add to Phoenix";
                ViewBag.Msg = "Do you want to add this Account to Phoenix?";
                return View();
            }
        }
        [RBAC(Permission = "LiveSMSAddMember")]
        public ActionResult LiveSMSAddMember(int member_key)
        {
            MemberDA mda = new MemberDA();
            MemberModel member = new MemberModel();
            member = mda.GetMemeber(member_key);
            LiveSMSMemberVM model = new LiveSMSMemberVM();
            MyWebDA myDa = new MyWebDA();
            MyWebUserProfileModel myweb = new MyWebUserProfileModel();
            myweb = myDa.GetLoginProfile(member_key);

            if (myweb.UserName == null)
            {
                ViewBag.Msg = "Member not registered for MyWeb";
                return View("Msg");
            }
            bool IsMember = mda.GetMemeberByLiveSMS(member_key);

            if (IsMember == true)
            {
                ViewBag.Memberkey = member_key;
                ViewBag.IsDuplicate = true;
                ViewBag.Title = "Add Duplicate";
                ViewBag.Msg = "Member already exists in LiveSMS, Do you want to create another?";
                return View();
            }
            else
            {
                model.name = member.FirstName + " " + member.LastName;
                model.MemberId = member.MemberId;
                model.Username = myweb.UserName;
                return View(model);
            }
        }
        [RBAC(Permission = "LiveSMSAddMember")]
        public ActionResult AddMemberToLiveSMS(LiveSMSMemberVM m, FormCollection c)
        {
            MemberModel model = new MemberModel();
            MemberDA mda = new MemberDA();
            var memberkey = mda.GetMemberKey(m.MemberId);
            model = mda.GetMemeber(memberkey);

            if (model != null)
            {
                LiveSMSMemberVM addmem = new LiveSMSMemberVM();
                addmem.name = model.FirstName + " " + model.LastName;
                addmem.email = model.Email;
                addmem.plan_id = 2;
                addmem.credits = Convert.ToInt32(c["Credit"].ToString());
                addmem.Username = m.Username;
                try
                {
                    SMSAPIHelper smshelper = new SMSAPIHelper();
                    string content = smshelper.InsertLiveSMSMember(addmem);

                    MemberLiveSMS_ExportedDA mlda = new MemberLiveSMS_ExportedDA();
                    MemberLiveSMS_ExportedModel livemodel = new MemberLiveSMS_ExportedModel();
                    livemodel.Member_key = memberkey;
                    livemodel.CreatedBy = User.Identity.Name;
                    livemodel.CreatedDate = DateTime.Now;
                    mlda.Add(livemodel);
                }
                catch
                {
                    ViewBag.Err = "An error occurred while adding member to LiveSMS API";
                    return View("CommonMsg1");
                }
            }
            ViewBag.Msg = "Member added to LiveSMS!";
            return View("Msg");
        }

        [RBAC(Permission = "MemberAddMember")]
        public ActionResult Create(MemberModel m_model)
        {
            //ModelState.Clear();
            Session["AdrList"] = null;
            Session["BAccountList"] = null;
            Session["MemberRentalModelList"] = null;
            Session["RateListToSave"] = null;
            Session["RateListToSaveConfig"] = null;
            CommissionRateDA cda = new CommissionRateDA();

            MemberModel model = new MemberModel();
            model = m_model;
            model.IsBankCustomer = true;
            DateTime now = DateTime.Now;
            var startDate1 = now.Year + "-" + now.Month.ToString("00") + "-" + now.Day.ToString("00");
            model.StartDate = startDate1;

            //string dobDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();// DateTime.Now.ToShortDateString();
            //string[] dobDateArry = dobDate.Split('/');

            //Bug Nathan
            model.DOBstring = string.Empty;//DateTime.Now.AddYears(-minimumAgeAllow).ToString();//dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            model.DOB = DateTime.Now.AddYears(-minimumAgeAllow);
            //model.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');

            if (model.RentalStartDate == null)
            {
                DateTime dt = DateTime.Today;
                model.RentalStartDate = new DateTime(dt.Year, dt.Month, 1);
            }
            //model.RentalStartDate = DateTime.Now;


            MemberModel SFmodel = new MemberModel();
            SFmodel = (MemberModel)Session["SalesforceMemberModel"];
            if (SFmodel != null)
            {
                //show original salesforce data
                model.SalesforceData = new CommissionRateVM();
                if (SFmodel.CRDebitRate != null)
                {
                    model.SalesforceData.CRDebitRate = SFmodel.CRDebitRate.Value;
                }

                if (SFmodel.CRVisaRate != null)
                {
                    model.SalesforceData.CRVisaRate = SFmodel.CRVisaRate;
                }

                if (SFmodel.CRMasterCardRate != null)
                {
                    model.SalesforceData.CRMasterCardRate = SFmodel.CRMasterCardRate;
                }

                if (SFmodel.CRDinersRate != null)
                {
                    model.SalesforceData.CRDinersRate = SFmodel.CRDinersRate;
                }

                if (SFmodel.CRAmexRate != null)
                {
                    model.SalesforceData.CRAmexRate = SFmodel.CRAmexRate;
                }

                if (SFmodel.CRUnionPayRate != null)
                {
                    model.SalesforceData.CRUnionPayRate = SFmodel.CRUnionPayRate;
                }
                if (SFmodel.CRZipPayRate != null)
                {
                    model.SalesforceData.CRZipRate = SFmodel.CRZipPayRate;
                }
                if (SFmodel.CRQantasRate != null)
                {
                    model.SalesforceData.CRQantasRate = SFmodel.CRQantasRate;
                }

                if (SFmodel.CRAlipayRate != null)
                {
                    model.SalesforceData.CRAlipayRate = SFmodel.CRAlipayRate;
                }
                if (SFmodel.CRDebitRatePercentage != null)
                {
                    model.SalesforceData.CRDebitRatePercentage = SFmodel.CRDebitRatePercentage;
                }
                if (SFmodel.CRQantasRatePercentage != null)
                {
                    model.SalesforceData.CRQantasRatePercentage = SFmodel.CRQantasRatePercentage;
                }

                //actual data
                if (SFmodel.CRDebitRate != null)
                {
                    model.CRDebitRate = SFmodel.CRDebitRate;
                }
                //else
                //{   //putting default rate
                //    model.CRDebitRate = drt.Debit;
                //}
                if (SFmodel.CRVisaRate != null)
                {
                    model.CRVisaRate = SFmodel.CRVisaRate;
                }
                //else
                //{
                //    model.CRVisaRate = drt.Visa;
                //}
                if (SFmodel.CRMasterCardRate != null)
                {
                    model.CRMasterCardRate = SFmodel.CRMasterCardRate;
                }
                //else
                //{
                //    model.CRMasterCardRate = drt.Master;
                //}
                if (SFmodel.CRDinersRate != null)
                {
                    model.CRDinersRate = SFmodel.CRDinersRate;
                }
                //else
                //{
                //    model.CRDinersRate = drt.Diners;
                //}
                if (SFmodel.CRAmexRate != null)
                {
                    model.CRAmexRate = SFmodel.CRAmexRate;
                }

                if (SFmodel.CRUnionPayRate != null)
                {
                    model.CRUnionPayRate = SFmodel.CRUnionPayRate;
                }

                if (SFmodel.CRZipPayRate != null)
                {
                    model.CRZipPayRate = SFmodel.CRZipPayRate;
                }

                if (SFmodel.CRQantasRate != null)
                {
                    model.CRQantasRate = SFmodel.CRQantasRate;
                }

                if (SFmodel.CRQantasRatePercentage != null)
                {
                    model.CRQantasRatePercentage = SFmodel.CRQantasRatePercentage;
                }

                if (SFmodel.CRAlipayRate != null)
                {
                    model.CRAlipayRate = SFmodel.CRAlipayRate;
                }
                //else
                //{
                //    model.CRAmexRate = drt.Amex;
                //}
                if (SFmodel.CRDebitRatePercentage != null)
                {
                    model.CRDebitRatePercentage = SFmodel.CRDebitRatePercentage;
                }
                //else
                //{
                //    model.CRDebitRatePercentage = drt.DebitPercentage;
                //}
                model.RentalMemberRentalTemplateKey = SFmodel.RentalMemberRentalTemplateKey;
                model.CompanyKey = 0;
                model.RentalAmountFromSalesforce = SFmodel.RentalAmountFromSalesforce;
                model.PaymentType = "Bank";
                bool IsFlatRate = SFmodel.IsFlatRate;
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();
                //this is the case for default case with entered amount in salesforce, set amt for all 5k and over
                if (model.RentalMemberRentalTemplateKey == 7 && model.RentalAmountFromSalesforce != null && IsFlatRate == false)
                {
                    if (model.RentalAmountFromSalesforce >= 0)
                    {
                        foreach (var rate in model.TerminalRentalRateList)
                        {
                            if (rate.TransactionTarget >= 5000)
                            {
                                rate.RentAmount = model.RentalAmountFromSalesforce.Value;
                            }
                        }
                    }
                }
                //this is the case for flat rate from salesforce,set amt for all 
                if (model.RentalMemberRentalTemplateKey == 7 && model.RentalAmountFromSalesforce != null && IsFlatRate == true)
                {
                    if (model.RentalAmountFromSalesforce >= 0)
                    {
                        foreach (var rate in model.TerminalRentalRateList)
                        {
                            rate.RentAmount = model.RentalAmountFromSalesforce.Value;
                        }
                    }
                }
            }
            else
            {
                //model.CRDebitRate = drt.Debit;
                //model.CRVisaRate = drt.Visa;
                //model.CRMasterCardRate = drt.Master;
                //model.CRDinersRate = drt.Diners;
                //model.CRAmexRate = drt.Amex;
                //model.CRDebitRatePercentage = drt.DebitPercentage;
                model.TerminalRentalRateList = null;
                model.CompanyKey = 0;
                Session["BusinessType"] = "LiveEftPos";
                model.PaymentType = "Bank";
                model.RentalMemberRentalTemplateKey = 7;
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();

            }
            //start change for bug 228 by Vu Nguyen. Enable Receive payment report, weekly report and monthly report by default
            model.ReceiveMonthlyActivityReportB = true;
            model.ReceiveWeeklyActivityReportB = false;
            model.ReceivePaymentReportB = true;

            //End change for bug 228 by Vu Nguyen
            Session["RateListToSave"] = model.TerminalRentalRateList;

            MemberDA mda = new MemberDA();
            var purposeList = mda.GetPurposeListForMember();
            model.PurposeTypes = JsonConvert.SerializeObject(purposeList);
            model.BankAccountDtoAsJson = "[]";
            model.CommissionRatesDtoAsJson = "[]";
            model.RebatesDtoAsJson = "[]";

            model.ZipMerchantAPIModel = new ZipMerchantAPIModel { IsActive = true };
            var requestingUser =
                RBAC_ExtendedMethods.GetRBACUser(User.Identity.Name);
            if (!requestingUser.HasPermission("DriverCardRegister"))
            {
                DrawDropDownConfig(model.CompanyKey, true, true, true);
            }
            else
            {
                DrawDropDownConfig(model.CompanyKey, true, true, false);
            }

            ViewBag.ListMCCRequiredBusinessType = Caching.GetSystemConfigurationByKey("ListMCCRequiredBusinessType");

            ViewBag.PayMethodList = PayMethodList;
            ViewBag.ListTerminal = "[]";

            return View(model);
        }
        //
        // POST: /Administrator/Member/Create



        [HttpPost]
        public ActionResult Create(MemberModel m, FormCollection c, HttpPostedFileBase file, string BtnType)
        {
            AddressListModel addressModel = new AddressListModel();
            MemberModel model = new MemberModel();
            model = m;
            int member_key = 0;
            var eftTerminalKeyList = new List<int>();

            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    MemberModel SFmodel = new MemberModel();
                    SFmodel = (MemberModel)Session["SalesforceMemberModel"];
                    DateTime ratestartdate = DateTime.Today.AddDays(DateTime.Today.Day - 1);
                    if (c["StartDate"] != null && DateTime.TryParse(c["StartDate"], out ratestartdate))
                    {
                        model.StartDate = ratestartdate.Year + "-" + ratestartdate.Month.ToString("00") + "-" + ratestartdate.Day.ToString("00");
                    }

                    if (m.CompanyKey == 0)
                    {
                        ModelState.AddModelError("error", "Please choose a business type.");
                    }

                    var mid = c["MerchantID"].ToString();
                    if (m.CompanyKey == 18 && mid == "")
                    {
                        if (c["PStreetNo"] != "" && c["PStreet"] != "" && c["PPostCode"] != "")
                        {
                            model.PStreetNo = c["PStreetNo"].ToString();
                            model.PStreet = c["PStreet"].ToString();
                            model.PPostCode = c["PPostCode"].ToString();
                        }
                        if (c["MStreetNo"] != "" && c["MStreet"] != "" && c["MPostCode"] != "")
                        {
                            model.MStreetNo = c["MStreetNo"].ToString();
                            model.MStreet = c["MStreet"].ToString();
                            model.MPostCode = c["MPostCode"].ToString();
                        }
                        ModelState.AddModelError("error", "Please enter merchant id");
                    }

                    if (c["PStreetNo"] == "" && c["PStreet"] == "" && c["PPostCode"] == "")
                    {
                        if (c["MStreetNo"] != "" && c["MStreet"] != "" && c["MPostCode"] != "")
                        {
                            model.MStreetNo = c["MStreetNo"].ToString();
                            model.MStreet = c["MStreet"].ToString();
                            model.MPostCode = c["MPostCode"].ToString();
                        }
                        ModelState.AddModelError("error", "Please add a primary address");
                    }

                    var BusinessTypeAutoEnableLiveGroupAmexSettlement = Caching
                        .GetSystemConfigurationByKey("BusinessTypeAutoEnableLiveGroupAmexSettlement").Split(new char[] { ';' });

                    if (BusinessTypeAutoEnableLiveGroupAmexSettlement.Any(k => k == m.CompanyKey.ToString()))
                    {
                        model.IndirectAmexSettlement = true;
                    }

                    var paymentType = m.PaymentType;
                    if (String.IsNullOrEmpty(model.BankAccountDtoAsJson) || model.BankAccountDtoAsJson.Length < 4)
                    {
                        ModelState.AddModelError("error", "Please add bank account details");
                    }

                    model.FirstName = c["FirstName"].ToString();
                    if (model.FirstName == "")
                    {
                        ModelState.AddModelError("error", "First Name can not be null");
                    }
                    model.LastName = c["LastName"].ToString();
                    if (model.LastName == "")
                    {
                        ModelState.AddModelError("error", "Last Name can not be null");
                    }
                    model.Telephone = c["Telephone"].ToString();
                    model.Fax = c["Fax"].ToString();
                    model.Mobile = c["Mobile"].ToString();
                    model.TradingName = c["TradingName"].ToString();
                    model.ABN = c["ABN"].ToString();
                    model.ACN = c["ACN"].ToString();
                    model.MCC = c["MCC"].ToString();
                    model.DriverLicenseNumber = c["DriverLicenseNumber"].ToString();
                    if (c["Email"] != "")
                        model.Email = c["Email"].ToString();
                    model.Active = m.Active;


                    if (paymentType == "Cash")
                    {
                        model.IsBankCustomer = false;
                        model.PaymentTypeId = 2;
                    }
                    else if (paymentType == "Bank")
                    {
                        model.IsBankCustomer = true;
                        model.PaymentTypeId = 1;
                    }
                    else if (paymentType == "Card")
                    {
                        model.IsBankCustomer = false;
                        model.PaymentTypeId = 3;
                    }

                    model.CompanyKey = m.CompanyKey;
                    model.Welcomed = m.Welcomed;
                    model.SalesforceId = m.SalesforceId;
                    model.IsOperator = true;
                    model.Modified_dttm = DateTime.Now;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedByUser = User.Identity.Name;
                    model.ReceiveMonthlyActivityReport = m.ReceiveMonthlyActivityReportB;
                    model.ReceivePaymentReport = m.ReceivePaymentReportB;
                    model.ReceiveWeeklyActivityReport = m.ReceiveWeeklyActivityReportB;
                    model.AverageTransaction = m.AverageTransaction;
                    model.AverageTurnover = m.AverageTurnover;

                    if (c["MerchantID"] != "")
                        model.MerchantID = c["MerchantID"].ToString();

                    DateTime PhotoIDExpiry = default(DateTime);
                    DateTime.TryParse(c["PhotoIDExpiry"].ToString(), out PhotoIDExpiry);
                    if (PhotoIDExpiry != null)
                        model.PhotoIDExpiry = PhotoIDExpiry;
                    model.Active = m.Active;
                    DateTime dob = default(DateTime);
                    //DateTime.TryParse(c["DOB"].ToString(), out dob);
                    dob = DateTime.ParseExact(c["DOBstring"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (dob != null)
                    {
                        model.DOB = dob;
                    }

                    byte[] photoid = new byte[1];
                    if (file != null)
                    {
                        bool validFile = Regex.IsMatch(file.FileName, "(.*?)\\.(JPG|PNG|JPEG)$");
                        if (file.ContentLength > 0 && validFile)
                        {
                            BinaryReader reader = new BinaryReader(file.InputStream);
                            photoid = reader.ReadBytes((int)file.InputStream.Length);
                        }
                    }

                    if (photoid.Count() > 1)
                    {
                        model.PhotoID = photoid;
                    }
                    model.SubBusinessTypeKey = m.SubBusinessTypeKey;
                    MemberDA mem = new MemberDA();
                    member_key = mem.AddNew(model, User.Identity.Name);
                    MemberAddressDA mdaq = new MemberAddressDA();

                    if (c["PStreetNo"] != "" && c["PStreet"] != "" && c["PPostCode"] != "")
                    {
                        var c1 = c["PPostCode"].ToString();
                        string[] pc = c1.Split(',');

                        AddressListModel m1 = new AddressListModel();
                        int id = member_key;
                        MemberDA mda1 = new MemberDA();
                        m1.Addresskey = m.PrimaryAddressKey;
                        m1.StreetNumber = c["PStreetNo"].ToString();
                        m1.StreetName = c["PStreet"].ToString();
                        if ($"{m1.StreetNumber} {m1.StreetName}".Length > 50)
                        {
                            ModelState.AddModelError("error", "Address must be equal or less than 50.");
                        }
                        m1.PostCode = pc[0].Trim();
                        if (pc.Length > 1)
                        {
                            m1.State = pc[1].Trim();
                        }

                        if (pc.Length > 2)
                        {
                            m1.Suburb = pc[2].Trim();
                        }
                        m1.Country = "Australia";
                        m1.AddressType = "Primary";

                        bool doesExists = mdaq.CheckIfAddressExists(m1.State, m1.PostCode, m1.Suburb);
                        if (doesExists == false)
                        {
                            ModelState.AddModelError("error", "Error adding primary address, please check the state/suburb");
                        }

                        if (m.PrimaryAddressKey == 0)
                        {
                            mdaq.Add(m1, id);
                        }
                        else
                        {
                            mdaq.Add(m1, id, true);
                        }

                        addressModel = m1;
                    }
                    else if (c["PStreetNo"] == "" && c["PStreet"] != "" && c["PPostCode"] != "")
                    {
                        ModelState.AddModelError("error", "Error adding primary address, please check.");
                    }
                    else
                    {

                    }

                    if (c["MStreetNo"] != "" && c["MStreet"] != "" && c["MPostCode"] != "")
                    {
                        var c1 = c["MPostCode"].ToString();
                        string[] pc = c1.Split(',');

                        AddressListModel m1 = new AddressListModel();
                        int id = member_key;
                        MemberDA mda1 = new MemberDA();
                        m1.Addresskey = m.MailingAddressKey;
                        m1.StreetNumber = c["MStreetNo"].ToString();
                        m1.StreetName = c["MStreet"].ToString();
                        m1.PostCode = pc[0].Trim();

                        if (pc.Length > 1)
                        {
                            m1.State = pc[1].Trim();
                        }

                        if (pc.Length > 2)
                        {
                            m1.Suburb = pc[2].Trim();
                        }

                        m1.Country = "Australia";
                        m1.AddressType = "Mailing";

                        bool doesExists = mdaq.CheckIfAddressExists(m1.State, m1.PostCode, m1.Suburb);
                        if (doesExists == false)
                        {
                            ModelState.AddModelError("error", "Error adding mailling address, please check.");
                        }

                        if (m.MailingAddressKey == 0)
                        {
                            mdaq.Add(m1, id);
                        }
                        else
                        {
                            mdaq.Add(m1, id, true);
                        }
                    }
                    else if (c["MStreetNo"] == "" && c["MStreet"] != "" && c["MPostCode"] != "")
                    {
                        ModelState.AddModelError("error", "Error adding mailing address, please check.");
                    }
                    else
                    {

                    }

                    if (c["DriverAuth"] != "")
                    {
                        DriverDA drda = new DriverDA();
                        DriverModel dmol = new DriverModel();
                        dmol.Driver_key = member_key;
                        dmol.DriverCertificateId = c["DriverAuth"].ToString();
                        dmol.Modified_dttm = DateTime.Now;
                        dmol.ModifiedByUser = User.Identity.Name;
                        drda.Add(dmol);
                    }
                    //adding account
                    BankAccountModelList m2 = new BankAccountModelList();
                    m2.BAccounttModelList = (List<BankAccountModel>)Session["BAccountList"];
                    if (m2.BAccounttModelList != null)
                    {
                        foreach (var b in m2.BAccounttModelList)
                        {
                            AccountDA mda = new AccountDA();
                            MemberDA mda1 = new MemberDA();
                            BankAccountModel account = new BankAccountModel();
                            account.AccountName = b.AccountName;
                            account.AccountNumber = b.AccountNumber;
                            account.Weight = b.Weight;
                            account.BSB = b.BSB;
                            account.Member_Key = member_key;
                            mda.Add(account);
                        }
                    }

                    CommissionRateDA cdass = new CommissionRateDA();
                    DefaultRateModel drtss = new DefaultRateModel();

                    string listTerminalJson = c["ListTerminal"];
                    List<TerminalAllocationModel> TAListInternal = new List<TerminalAllocationModel>();

                    if (!string.IsNullOrEmpty(listTerminalJson))
                    {
                        TAListInternal = JsonConvert.DeserializeObject<List<TerminalAllocationModel>>(listTerminalJson);
                    }
                    var companyKeysAllowOnlyOneSubBusToAllTerminals =
                        Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                            .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

                    // If in case we only allow only one sub-business type for all terminals
                    if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(model.CompanyKey.ToString()))
                    {
                        foreach (var TA in TAListInternal)
                        {
                            TA.VehicleModel.VehicleType_key = m.SubBusinessTypeKey;
                        }
                    }

                    var commissionRates = JsonConvert.DeserializeObject<List<MemberCommissionRateModel>>(m.CommissionRatesDtoAsJson);
                    var savingTerminalAllocationSubBusinessTypeIds = TAListInternal.Select(t => t.VehicleModel.VehicleType_key.GetValueOrDefault().ToString()).Distinct().ToList();
                    var savingSubBusinessTypeIds = commissionRates.Select(com => com.SubBusinessTypeID.ToString()).Distinct().ToList();
                    if (model.IsFeeFreeMember)
                    {
                        var lstSubBusinessTypeAllowNoCR =
                            Caching.GetSystemConfigurationByKey("SubBusinessTypeIdsNotRequireMSF");
                        lstSubBusinessTypeAllowNoCR.Split(';').ToList().ForEach(s => savingSubBusinessTypeIds.Add(s));
                    }
                    var result = savingTerminalAllocationSubBusinessTypeIds.Except(savingSubBusinessTypeIds).Count();
                    if (result > 0)
                    {
                        ModelState.AddModelError("error", "Please add a Commission Rate/MSF for all sub-business types of this Member.");
                    }

                    var businessTypeAllowRebate = Caching.GetSystemConfigurationByKey("BusinessKeysAllowRebate").Split(';').ToList();
                    if (businessTypeAllowRebate.Contains(model.CompanyKey.ToString())
                        && JsonConvert.DeserializeObject<List<MemberRebateModel>>(m.RebatesDtoAsJson).Count == 0)
                    {
                        ModelState.AddModelError("error", "Please input rebate amount.");
                    }

                    // Special case: Live taxi black VIC
                    var arrSubBusinessTypeForVICStateMerchantOnly = Caching.GetSystemConfigurationByKey("SubBusinessTypeForVICStateMerchantOnly").Split(new string[] { ";" }, StringSplitOptions.None);
                    if (arrSubBusinessTypeForVICStateMerchantOnly.Any(n => savingTerminalAllocationSubBusinessTypeIds.Contains(n))
                        || arrSubBusinessTypeForVICStateMerchantOnly.Any(n => savingSubBusinessTypeIds.Contains(n)))
                    {
                        var memberAddr = new MemberAddressDA().GetMemberAddress(member_key);
                        if (memberAddr.FirstOrDefault()?.State != "VIC")
                        {
                            ModelState.AddModelError("error", "Incorrect sub-business type selected. Live taxi Black VIC can only be used for Victorian members");
                        }
                    }

                    DateTime RentalStartDate = DateTime.MinValue;
                    DateTime.TryParse(c["RentalStartDate"].ToString(), out RentalStartDate);
                    if (RentalStartDate != DateTime.MinValue)
                    {
                        model.RentalStartDate = RentalStartDate;
                    }

                    DateTime RentalEndDate = DateTime.MinValue;
                    DateTime.TryParse(c["RentalEndDate"].ToString(), out RentalEndDate);
                    if (RentalEndDate != DateTime.MinValue)
                    {
                        RentalEndDate = Convert.ToDateTime(c["RentalEndDate"].ToString());
                        model.RentalEndDate = RentalEndDate;
                    }
                    //add rental
                    if (m.RentalRentFree != true)
                    {
                        List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                        if (TerminalRentalRateList == null)
                        {
                            ModelState.AddModelError("error", "Error adding rental, please try resolving it.");
                        }
                        else if (TerminalRentalRateList.Count > 0)
                        {
                            //checking if new value is added for the rental template
                            foreach (var r in TerminalRentalRateList)
                            {
                                string key = r.TerminalRentalRate_key.ToString();

                                if (c[key] != null)
                                {
                                    string amt = c[key].ToString();
                                    if (amt != string.Empty)
                                    {
                                        decimal rAmt = Convert.ToDecimal(amt);
                                        if (rAmt > 0)
                                        {
                                            r.RentAmount = rAmt;
                                        }
                                    }
                                }
                            }
                            MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
                            mtra.Add(TerminalRentalRateList, member_key, RentalStartDate, RentalEndDate, User.Identity.Name);
                        }
                    }
                    else
                    {
                        MemberRentalDA mda = new MemberRentalDA();
                        MemberRentalModel m1 = new MemberRentalModel();
                        m1.Member_key = member_key;
                        m1.RentFree = true;

                        if (RentalStartDate != DateTime.MinValue)
                        {
                            m1.StartDate = RentalStartDate;
                        }
                        if (RentalEndDate != DateTime.MinValue)
                        {
                            m1.EndDate = RentalEndDate;
                        }
                        m1.ModifiedByUser = User.Identity.Name;
                        m1.Modified_dttm = DateTime.Now;
                        m1.RentalAmount = 0;
                        //m1.RentalTurnoverRangeKey = t.RentalTurnoverRangeKey;
                        mda.AddNew(m1);

                    }
                    //add Vehicle and TerminalAllocation
                    if (TAListInternal != null && TAListInternal.Count > 0)
                    {
                        if (TAListInternal.Any(x => x.VehicleModel.VehicleType.Company_Key != m.CompanyKey))
                        {
                            ModelState.AddModelError("error", "There are some wrong between Business Type of Terminal Allocation and Business Type.");
                        }
                        else
                        {
                            foreach (var t in TAListInternal)
                            {
                                VehicleDA vda = new VehicleDA();
                                VehicleModel v1 = new VehicleModel();
                                v1.VehicleId = t.VehicleModel.VehicleId;
                                v1.RegisteredState = t.VehicleModel.RegisteredState;
                                v1.TerminalAllocatedDate = t.VehicleModel.TerminalAllocatedDate;
                                v1.Operator_key = member_key;
                                v1.VehicleType_key = t.VehicleModel.VehicleType_key;
                                v1.CreatedDate = DateTime.Now;
                                v1.Modified_dttm = DateTime.Now;
                                v1.ModifiedByUser = User.Identity.Name;
                                v1.EftTerminal_key = t.EftTerminal_key;
                                int veh_Key = vda.AddNew(v1);

                                TerminalAllocationDA mda = new TerminalAllocationDA();
                                TerminalAllocationModel m1 = new TerminalAllocationModel();
                                m1.EftTerminal_key = t.EftTerminal_key;
                                m1.Member_key = member_key;
                                m1.EffectiveDate = t.VehicleModel.TerminalAllocatedDate.Value;
                                m1.IsCurrent = true;
                                m1.ModifiedByUser = User.Identity.Name;
                                m1.DeliveryRequired = t.DeliveryRequired;
                                m1.ServiceFee = t.ServiceFee;
                                m1.AllowMOTO = t.AllowMOTO;
                                m1.AllowRefund = t.AllowRefund;
                                m1.Vehicle_key = veh_Key;
                                int termAlloc_Key = mda.AddNew(m1);
                                eftTerminalKeyList.Add(m1.EftTerminal_key ?? 0);

                                //change the status
                                TerminalDA tda = new TerminalDA();
                                tda.UpdateTerminalStatus(t.EftTerminal_key.Value, TEnum.TerminalStatus.LIVE.ToString(), "CUSTOMER");
                                string terminal = tda.GetTerminalId(t.EftTerminal_key.Value);

                                //allocate unlocated transaction
                                EFTDocketDA eda = new EFTDocketDA();
                                List<EFTDocketModel> unallocatedList = eda.GetUnallocatedTransactionFromSpecificDate(terminal, t.VehicleModel.TerminalAllocatedDate.Value);
                                if (unallocatedList != null)
                                {
                                    foreach (var u in unallocatedList)
                                    {
                                        eda.AllocateTransactionsToMember(u.EftDocket_key, t.EftTerminal_key.Value, member_key, veh_Key);
                                    }
                                }

                                if (t.Employee_key != 0)
                                {
                                    TerminalSalesDA sda = new TerminalSalesDA();
                                    TerminalSalesModel s1 = new TerminalSalesModel();
                                    s1.TerminalAllocation_key = termAlloc_Key;
                                    s1.Employee_key = t.Employee_key.Value;// member_key;
                                    s1.Active = true;
                                    s1.SaleRelationship_key = 1;
                                    s1.ModifiedByUser = User.Identity.Name;
                                    s1.Modified_dttm = DateTime.Now;

                                    sda.AddNew(s1);
                                }
                            }
                        }
                    }


                    //ZipMerchantAPI
                    if (this.HasPermission("AccessZipAPI"))
                    {
                        //update member key to ZipMerchantAPIModel incase Create new Member
                        m.ZipMerchantAPIModel.MemberKey = member_key;
                        AddOrUpdateZipMerchantAPI(m.ZipMerchantAPIModel);
                    }

                    if (!ModelState.IsValid)
                    {
                        if (model.RentalRentFree != true)
                        {
                            List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                            if (TerminalRentalRateList != null)
                            {
                                model.TerminalRentalRateList = TerminalRentalRateList;
                            }
                        }
                        DrawDropDownConfig(model.CompanyKey, true, true);
                        ViewBag.ListMCCRequiredBusinessType = Caching.GetSystemConfigurationByKey("ListMCCRequiredBusinessType");
                        ViewBag.PayMethodList = PayMethodList;

                        LogInfo($"Save rental data for MemberKey = {model.Member_key} with CompanyKey = {model.CompanyKey}, RentalMemberRentalTemplateKey = {model.RentalMemberRentalTemplateKey.Value.ToString()}");
                        Session["RateListToSaveConfig"] = new RateListToSaveConfig()
                        {
                            CompanyKey = model.CompanyKey,
                            RentalMemberRentalTemplateKey = model.RentalMemberRentalTemplateKey.HasValue ? model.RentalMemberRentalTemplateKey.Value : 0,
                            TerminalRentalRateList = model.TerminalRentalRateList,
                        };

                        ViewBag.ListTerminal = c["ListTerminal"];

                        return View(model);
                    }

                    tran.Complete();
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    if (model.RentalRentFree != true)
                    {
                        List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                        if (TerminalRentalRateList != null)
                        {
                            model.TerminalRentalRateList = TerminalRentalRateList;
                        }
                    }
                    ModelState.AddModelError("error", ex.Message);
                    DrawDropDownConfig(model.CompanyKey, true, true);
                    ViewBag.ListMCCRequiredBusinessType = Caching.GetSystemConfigurationByKey("ListMCCRequiredBusinessType");
                    ViewBag.PayMethodList = PayMethodList;
                    ViewBag.ListTerminal = c["ListTerminal"];
                    return View(model);
                }
            }

            var tdda = new TerminalAllocationDA();
            foreach (var terminalKey in eftTerminalKeyList)
            {
                //auto disable glidbox when allocate terminal to Live eftpos group
                tdda.DisableGlideboxFollowBusinessType(terminalKey, User.Identity.Name, SourceContants.AllocateTerminal);
            }

            if (!m.ZipMerchantAPIModel.IsActive)
            {
                DisableZipPaymentOfAllTerminals(m.Member_key);
            }

            // Sending welcome email to taxi driver
            SendWelcomeEmail(model, addressModel);

            return RedirectToAction("Edit", new { member_key = member_key });
        }

        private string GetEmailTemplate(MemberModel member, AddressListModel primaryAddress)
        {
            var emailTemplate = string.Empty;
            var companyKey = member.CompanyKey;
            var paymentType = member.PaymentType.ToUpper().Trim();
            var state = primaryAddress.State.ToUpper().Trim();

            LogInfo($"Get welcome email template for MemberKey = {member.Member_key} with CompanyKey = {companyKey}, PaymentType = {paymentType}, State={state}");
            switch (companyKey)
            {
                case 1: // Live taxi (TaxiEPay)
                    switch (paymentType)
                    {
                        case "CASH":
                            switch (state)
                            {
                                case "VIC":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiCashVIC.ToString();
                                    break;
                                case "NSW":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiCashNSW.ToString();
                                    break;
                                case "WA":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiCashWA.ToString();
                                    break;
                                case "SA":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiCashSA.ToString();
                                    break;
                                default:
                                    break;
                            }

                            break;
                        case "BANK":
                            switch (state)
                            {
                                case "VIC":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBankVIC.ToString();
                                    break;
                                default:
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBank.ToString();
                                    break;
                            }

                            break;
                        case "CARD":
                            switch (state)
                            {
                                case "VIC":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiDriverCardVIC.ToString();
                                    break;
                                default:
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiDriverCard.ToString();
                                    break;
                            }

                            break;
                        default:
                            break;
                    }

                    break;
                case 23: // Live taxi black (Live eftpos black for taxi)
                    switch (paymentType)
                    {
                        case "BANK":
                            switch (state)
                            {
                                case "VIC":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBlackBankVIC.ToString();
                                    break;
                                default:
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBlackBank.ToString();
                                    break;
                            }

                            break;
                        case "CARD":
                            switch (state)
                            {
                                case "VIC":
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBlackDriverCardVIC.ToString();
                                    break;
                                default:
                                    emailTemplate = SystemConfigurationKeyEnum.WelcomeEmailLiveTaxiBlackDriverCard.ToString();
                                    break;
                            }

                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }

            LogInfo($"Welcome email template for MemberKey = {member.Member_key} = {emailTemplate}");
            return emailTemplate;
        }

        private void SendWelcomeEmail(MemberModel member, AddressListModel primaryAddress)
        {
            var emailMember = member.Email.Trim();
            var companyKey = member.CompanyKey;
            var emailTemplate = GetEmailTemplate(member, primaryAddress);

            if (string.IsNullOrEmpty(emailTemplate))
            {
                return;
            }

            if (string.IsNullOrEmpty(emailMember))
            {
                LogError($"Can not sending to MemberKey = {member.Member_key} as email doesn't setup on the PhoenixWeb.");
                return;
            }

            try
            {
                LogInfo($"Get MemberId for MemberKey = {member.Member_key}");
                MemberDA mda = new MemberDA();
                member.MemberId = mda.GetMemberId(member.Member_key);

                if (string.IsNullOrEmpty(member.MemberId))
                {
                    LogError($"Can not sending to MemberKey = {member.Member_key} as memberId isn't exist.");
                    return;
                }

                LogInfo($"Create meta data for MemberKey = {member.Member_key}");
                Dictionary<string, object> customMetaData = new Dictionary<string, object>();
                customMetaData["Firstname"] = member.FirstName;
                customMetaData["MemberID"] = member.MemberId;

                LogInfo($"Prepare sending email to MemberKey = {member.Member_key}");
                dynamic sendEmailConfig = new ExpandoObject();
                sendEmailConfig.SparkPostAPIKey = Caching.
                    GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
                sendEmailConfig.TemplateID = Caching.GetSystemConfigurationByKey(emailTemplate);
                sendEmailConfig.CustomMetaData = customMetaData;
                sendEmailConfig.Recipient = emailMember;

                // Send email
                LogInfo($"Start sending welcome email to MemberKey = {member.Member_key} with Email = {sendEmailConfig.Recipient}, TemplateId = {sendEmailConfig.TemplateID}");
                IEmail emailProvider = LGEmail.GetEmailProvider(EmailType.SparkPost);
                emailProvider.SendEmail(sendEmailConfig);
                LogInfo($"End sending welcome email to MemberKey = {member.Member_key}");

                LogInfo($"Mark Memerber = {member.Member_key} is welcomed");
                mda.UpdateMemberWelcome(member.Member_key, User.Identity.Name);
            }
            catch (Exception ex)
            {
                LogError($"Fail sending email to MemberKey = {member.Member_key}, Exception:" + ex.Message);
                LogError(ex);
            }
        }

        [RBAC(Permission = "MemberAddMemberAddress")]
        public ActionResult CreateMemberAddress()
        {
            CAddressList model = new CAddressList();
            model.CAddresses = (List<CAddress>)Session["AdrList"];
            AddressListModel SFmodel = new AddressListModel();
            SFmodel = (AddressListModel)Session["SalesforceMemberAddress"];
            if (SFmodel != null)
            {
                model.StreetNumber = SFmodel.StreetNumber;
                model.StreetName = SFmodel.StreetName;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateMemberAddress(CAddressList m, FormCollection c)
        {
            var c1 = c["PostCode"].ToString();
            string[] pc = c1.Split(',');

            CAddress adr = new CAddress();
            Random rnd = new Random();
            adr.Id = rnd.Next(1, 50);
            adr.StreetNumber = m.StreetNumber;
            adr.StreetName = m.StreetName;
            adr.PostCode = pc[0].Trim();
            adr.State = pc[1].Trim();
            adr.Suburb = pc[2].Trim();
            adr.Country = c["Country"].ToString().Trim();
            adr.AddressType = c["AddressType"].ToString();
            m.StreetNumber = c["StreetNumber"].ToString();
            m.StreetName = c["StreetName"].ToString();

            List<CAddress> madd = new List<CAddress>();
            madd = (List<CAddress>)Session["AdrList"];
            if (madd == null)
            {
                madd = new List<CAddress>();
            }
            madd.Add(adr);

            m.CAddresses = madd.Distinct().ToList<CAddress>();
            Session["AdrList"] = m.CAddresses;

            CAddressList model = new CAddressList();
            model = m;
            return View(model);
        }

        public ActionResult DeleteCreatedAddress(int Id)
        {
            List<CAddress> madd = new List<CAddress>();
            madd = (List<CAddress>)Session["AdrList"];
            if (madd != null)
            {
                var item = (from n in madd
                            where n.Id == Id
                            select n).FirstOrDefault();
                madd.Remove(item);
            }
            CAddressList model = new CAddressList();
            model.CAddresses = madd.Distinct().ToList<CAddress>();

            if (madd.Count() == 0)
                model.CAddresses = null;

            Session["AdrList"] = model.CAddresses;

            // return View(model);
            return RedirectToAction("CreateMemberAddress");
        }


        [RBAC(Permission = "MemberEditMemberAddress")]
        public ActionResult AddMemberAddress(int id)
        {

            MemberDA mda = new MemberDA();
            MemberModel member = mda.GetMemeber(id);
            Session["Memberkey"] = id;

            AddressListModel model = new AddressListModel();
            AddressListModel SFmodel = new AddressListModel();
            SFmodel = (AddressListModel)Session["SalesforceMemberAddress"];
            if (SFmodel != null)
            {
                model.Member = member;
                model.StreetNumber = SFmodel.StreetNumber;
                model.StreetName = SFmodel.StreetName;
            }
            else
            {
                model.Member = member;
                model.StreetNumber = string.Empty;
                model.StreetName = string.Empty;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddMemberAddress(AddressListModel m, FormCollection c)
        {
            MemberAddressDA mda = new MemberAddressDA();
            int id = (int)Session["Memberkey"];
            MemberDA mda1 = new MemberDA();
            // MemberModel member = mda1.GetMemeber(id);
            var c1 = c["PostCode"].ToString();
            string[] pc = c1.Split(',');
            m.PostCode = pc[0].Trim();
            m.State = pc[1].Trim();
            m.Suburb = pc[2].Trim();
            m.Country = c["Country"].ToString().Trim();
            m.StreetNumber = c["StreetNumber"].ToString();
            m.StreetName = c["StreetName"].ToString();
            m.AddressType = c["AddressType"].ToString();
            // m.Member = member;
            mda.Add(m, id);

            AddressListModel model = new AddressListModel();
            model = mda.GetAddressListModel(m.Member.Member_key);
            return View(model);
        }
        [RBAC(Permission = "MemberAddMemberAccount")]
        public ActionResult CreateMemberAccount()
        {
            BankAccountModelList model = new BankAccountModelList();
            model.BAccounttModelList = (List<BankAccountModel>)Session["BAccountList"];
            DrawDropDownBsb();
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberAccount")]
        public ActionResult CreateMemberAccount(BankAccountModelList m, FormCollection c)
        {
            BankAccountModel account = new BankAccountModel();
            BankAccountModelList model = new BankAccountModelList();
            var allbs = c["BsbNumber"].ToString();
            List<BankAccountModel> BAList = new List<BankAccountModel>();
            BAList = (List<BankAccountModel>)Session["BAccountList"];
            string BSB = string.Empty;
            try
            {
                var bs = allbs.Substring(0, 7);
                BsbListDA dba = new BsbListDA();
                var IsPresent = dba.IsBSBValid(bs);
                BSB = bs;
                if (IsPresent != true)
                {
                    throw new Exception();
                }
            }
            catch
            {
                ViewBag.Error = "Error with BSB, please check";
                Session["BAccountList"] = BAList;
                model.BAccounttModelList = BAList;
                return View(model);
            }



            decimal totalWeight = 0;
            Random rnd = new Random();
            account.Id = rnd.Next(1, 50);
            account.AccountName = c["AccountName"].ToString();
            account.AccountNumber = c["AccountNumber"].ToString();
            account.Weight = Convert.ToInt32(c["Weight"].ToString());
            account.BSB = BSB;

            if (BAList == null)
            {
                BAList = new List<BankAccountModel>();
                totalWeight = account.Weight;
            }
            else
            {
                foreach (var BA in BAList)
                {
                    totalWeight = totalWeight + BA.Weight;
                }
                totalWeight = totalWeight + account.Weight;
            }

            if (totalWeight > 100)
            {
                ViewBag.Error = "Weight exceed more than 100%";
                Session["BAccountList"] = BAList;
                model.BAccounttModelList = BAList;
                return View(model);

            }
            BAList.Add(account);
            Session["BAccountList"] = BAList;
            model.BAccounttModelList = BAList;
            return View(model);

        }


        public ActionResult DeleteCreateMemberAccount(int Id)
        {
            BankAccountModel account = new BankAccountModel();
            BankAccountModelList model = new BankAccountModelList();

            List<BankAccountModel> BAList = new List<BankAccountModel>();
            BAList = (List<BankAccountModel>)Session["BAccountList"];
            if (BAList != null)
            {
                var item = (from n in BAList
                            where n.Id == Id
                            select n).FirstOrDefault();
                BAList.Remove(item);
            }
            if (BAList.Count() == 0)
            {
                BAList = null;
            }
            Session["BAccountList"] = BAList;
            model.BAccounttModelList = BAList;

            return RedirectToAction("CreateMemberAccount");
            //  return View(model);

        }
        [RBAC(Permission = "MemberEditMemberAccount")]
        public ActionResult EditMemberAccount(int Accountkey)
        {
            AccountDA ada = new AccountDA();
            BankAccountModelVM model = new BankAccountModelVM();
            BankAccountModel model1 = new BankAccountModel();

            model1 = ada.GetAccountBykey(Accountkey);
            model.AccountName = model1.AccountName;
            model.AccountNumber = model1.AccountNumber;
            model.BSB = model1.BSB;
            model.Weight = model1.Weight;
            model.Account_Key = model1.AccountKey;
            MemberDA mda = new MemberDA();
            model.MemberID = mda.GetMemberId(model1.Member_Key);

            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberEditMemberAccount")]
        public ActionResult EditMemberAccount(BankAccountModelVM Account, FormCollection c)
        {
            AccountDA ada = new AccountDA();
            MemberDA mda = new MemberDA();
            int memberKey = mda.GetMemberKey(Account.MemberID);


            decimal totalWeight = ada.GetMemberAccountTotalWeightNOTforCurrentAcc(memberKey, Account.Account_Key);
            if ((totalWeight + Account.Weight) > 100)
            {
                ViewBag.Error = "Weight exceed more than 100%";
                return View(Account);
            }
            BankAccountModel addModel = new BankAccountModel();
            addModel.AccountName = Account.AccountName;
            addModel.AccountNumber = Account.AccountNumber;
            addModel.BSB = Account.BSB;
            addModel.Weight = Account.Weight;
            addModel.AccountKey = Account.Account_Key;
            addModel.UserName = User.Identity.Name;
            ada.Edit(addModel);

            return RedirectToAction("AddMemberAccount", new { id = memberKey });
        }

        [RBAC(Permission = "MemberEditMemberAccount")]
        public ActionResult AddMemberAccount(int id)
        {
            AccountDA mda = new AccountDA();
            BankAccountModel model = new BankAccountModel();
            DrawDropDownBsb();
            model = mda.GetAccountListModel(id);
            model.Member_Key = id;
            return View(model);
        }

        [HttpGet]
        public JsonResult CheckValidBankAccountInfo(string bsb, string accountNumber, int memberKey)
        {

            var error = string.Empty;
            var bsbNumber = bsb?.Trim();
            if (!string.IsNullOrEmpty(bsbNumber))
            {
                try
                {
                    BsbListDA dba = new BsbListDA();
                    if (!dba.IsBSBValid(bsbNumber))
                    {
                        error = "This Bsb number is not valid.";
                    }

                    AccountDA mda = new AccountDA();
                    if (mda.IsExistingBankAccountInfo(bsb, accountNumber, memberKey) && memberKey > 0)
                    {
                        error = "This account has already exist in your account list.";
                    }
                }
                catch (Exception e)
                {
                    error = e.Message;
                }
            }
            else
            {
                error = "BSB number is empty";
            }

            var obj = new
            {
                IsError = !string.IsNullOrEmpty(error),
                ErrorMessage = error
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[RBAC(Permission = "MemberEditMemberAccount")]
        //public ActionResult AddMemberAccount(BankAccountModel m, FormCollection c)
        //{
        //    AccountDA mda = new AccountDA();
        //    int idi = m.Member_Key;
        //    BankAccountModel model = new BankAccountModel();
        //    var allbs = c["BsbNumber"].ToString();

        //    string BSB = string.Empty;
        //    try
        //    {
        //        var bs = allbs.Substring(0, 7);
        //        BsbListDA dba = new BsbListDA();
        //        var IsPresent = dba.IsBSBValid(bs);
        //        BSB = bs;
        //        if (IsPresent != true)
        //        {
        //            throw new Exception();
        //        }
        //    }
        //    catch
        //    {
        //        ViewBag.Error = "Error adding the Bsb, please check";
        //        model = mda.GetAccountListModel(idi);
        //        return View(model);
        //    }

        //    MemberDA mda1 = new MemberDA();
        //    MemberModel member = mda1.GetMemeber(idi);
        //    DrawDropDownBsb();
        //    BankAccountModel account = new BankAccountModel();
        //    account.AccountName = c["AccountName"].ToString();
        //    account.AccountNumber = c["AccountNumber"].ToString();
        //    account.Weight = Convert.ToInt32(c["Weight"].ToString());
        //    account.BSB = BSB;
        //    account.Member = member;
        //    account.Member_Key = idi;
        //    decimal totalWeight = 0;
        //    foreach (var BA in member.BankAccounts)
        //    {
        //        totalWeight = totalWeight + BA.Weight;

        //    }
        //    totalWeight = totalWeight + account.Weight;

        //    model.AccountName = account.AccountName;
        //    model.AccountNumber = account.AccountNumber;
        //    model.BSB = account.BSB;
        //    if (totalWeight > 100)
        //    {
        //        ViewBag.Error = "Total weight exceed more than 100%";
        //        model = mda.GetAccountListModel(idi);
        //        return View(model);
        //    }
        //    else
        //    {
        //        mda.Add(account);
        //        model = mda.GetAccountListModel(idi);
        //        return View(model);
        //    }
        //}

        public ActionResult CloneMember(int id)
        {

            MemberDA mda = new MemberDA();
            MemberModel member = mda.GetMemeber(id);
            member.IsOperator = true;
            member.Modified_dttm = DateTime.Now;
            member.ModifiedByUser = User.Identity.Name;
            int member_key = 0;
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    member_key = mda.CloneMember(member);
                    tran.Complete();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("error", ex.Message);
                    return View("Search");
                }
            }

            return RedirectToAction("Edit", new { member_key });
        }
        [RBAC(Permission = "MemberDetailMember")]
        public ActionResult Details(int? member_key, FormCollection c)
        {
            MemberDA mda = new MemberDA();
            MemberModel model = new MemberModel();
            if (member_key == null)
            {
                string memberid = c["MemberId"].ToString();
                if (memberid != null)
                {
                    member_key = mda.GetMemberKey(memberid);
                }
            }
            model = mda.GetMemeber(member_key.Value);
            var companyKeysAllowOnlyOneSubBusToAllTerminals =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                    .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

            if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(model.CompanyKey.ToString()))
            {
                if (model.AllVehicleTerminalAllocation.Any(n => n.IsAllocated))
                {
                    model.SubBusinessTypeKey =
                        model.AllVehicleTerminalAllocation.First(n => n.IsAllocated).VehicleTypeKey;
                }
                try
                {
                    model.SubBusinesstypeName = new VehicleTypeDA().GetNameByKey(model.SubBusinessTypeKey);
                }
                catch (Exception e)
                {
                    model.SubBusinesstypeName = string.Empty;
                }
                model.NotAllowSelectSubBusinessType = true;
            }
            model.EnableTransactionListOnEOMReport = mda.GetEnableTransactionListOnEOMStatement(null, member_key.Value);
            ViewBag.IsMemberDetailPage = true;
            if (model.DOB != null)
            {
                string dobDate = model.DOB.Value.ToShortDateString();
                string[] dobDateArry = dobDate.Split('/');
                //model.DOBstring = model.DOB.HasValue ? model.DOB.Value.ToString("yyyy-MM-dd") : String.Empty; //  dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
                model.DOBstring = model.DOB.HasValue ? model.DOB.Value.ToString("dd/MM/yyyy") : string.Empty;
            }
            if (model.PhotoIDExpiry != null)
            {
                string ExpiryDate = model.PhotoIDExpiry.Value.ToShortDateString();
                string[] ExpiryDateArry = ExpiryDate.Split('/');
                model.PhotoIDExpiryString = ExpiryDateArry[2].PadLeft(4, '0') + "-" + ExpiryDateArry[1].PadLeft(2, '0') + "-" + ExpiryDateArry[0].PadLeft(2, '0');
            }
            Session["Member_key"] = member_key;
            Session["Company_key"] = model.CompanyKey;
            model.RentalMemberRentalTemplateKey = 7;
            TerminalRentalRateDA trda = new TerminalRentalRateDA();

            List<TerminalRentalRateModel> rmodel = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value);

            model.TerminalRentalRateList = rmodel.OrderBy(n => n.TerminalRentalRate_key).ToList();

            DrawDropDownConfig(model.CompanyKey);

            TransactionDA tda = new TransactionDA();
            if (model.CompanyKey == 29)
            {
                model.MonthlyReportHistory = tda.GetTransactionPeriodHistory(model.Member_key, 24);
            }
            else
            {
                model.MonthlyReportHistory = tda.GetPaymentPeriodHistory(model.Member_key, 24);
            }
            model.FinancialYearReportHistory = tda.GetFinancialYearReportHistory(model.Member_key);
            model.BankAccountDtoAsJson = mda.GetBankAccountAndPurposeTypeOfMember(model.Member_key);
            model.CommissionRatesDtoAsJson = JsonConvert.SerializeObject(model.CommissionRates);
            //get all purpose list and storage in hidden field
            var purposeList = mda.GetPurposeListForMember();
            model.PurposeTypes = JsonConvert.SerializeObject(purposeList);

            //member charge fee
            model.FeeDtoAsJson = JsonConvert.SerializeObject(model.Fees);
            model.ChargeFeeTypeDtoAsJson = JsonConvert.SerializeObject(model.ChargeFeeType);
            StatusDA sda = new StatusDA();
            model.StatusTypes.Add(new SelectListItem
            {
                Value = "",
                Text = "Select All"
            });
            model.StatusTypes.AddRange(sda.GetAll()
                .Select(m => new SelectListItem
                {
                    Value = m.Name,
                    Text = m.Description
                }).ToList());
            return View(model);
        }
        [RBAC(Permission = "MemberEditMemberRentalAmount")]
        public ActionResult EditMemberRentalAmount(int MemberTerminalRentalKey)
        {
            MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
            MemberTerminalRentalModel model = new MemberTerminalRentalModel();
            model = mtra.GetMemberTerminalRental(MemberTerminalRentalKey);
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberEditMemberRentalAmount")]
        public ActionResult EditMemberRentalAmount(int MemberTerminalRentalKey, FormCollection c)
        {
            int Member_key = 0;
            int Company_key = 0;
            decimal Amt = 0.0M;

            if (c["RentalAmountNew"] != "")
            {
                Amt = Convert.ToDecimal(c["RentalAmountNew"].ToString());
            }
            else if (c["RentalAmountNew"] == "")
            {
                Amt = 0.0M;
            }
            string IsRentFree = "off";
            if (c["RentFreeChk"] != null)
            {
                IsRentFree = c["RentFreeChk"].ToString();
            }
            DateTime EndTime1 = DateTime.MinValue;
            DateTime RFEndDate1 = DateTime.MinValue;

            DateTime EndTime = default(DateTime);
            DateTime.TryParse(c["EndDateNew"].ToString(), out EndTime);
            if (EndTime != null)
                EndTime1 = EndTime;

            if (Session["Member_key"] != null)
            {
                Member_key = Convert.ToInt32(Session["Member_key"].ToString());
            }

            if (Session["Company_key"] != null)
            {
                Company_key = Convert.ToInt32(Session["Company_key"].ToString());
            }
            MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
            mtra.AddNew(MemberTerminalRentalKey, EndTime, User.Identity.Name, Amt);
            return RedirectToAction("Edit", new { member_key = Member_key });
        }

        [RBAC(Permission = "MemberViewMonthlyReport")]
        public ActionResult ViewMonthlyReport(int Member_key, DateTime Date, FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            string memberid = mda.GetMemberId(Member_key);
            string ReportName = "/LTEPAdminReports/Activity Report";
            IList<ParameterValue> parameters = new List<ParameterValue>();

            parameters.Add(new ParameterValue { Name = "ReportDate", Value = Date.ToShortDateString() });
            parameters.Add(new ParameterValue { Name = "MemberId", Value = memberid });
            parameters.Add(new ParameterValue { Name = "TimeFrame", Value = "Monthly" });
            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            return new ReportsResult(output, mimeType, $"Monthly Payments_{memberid}.pdf");
        }

        [RBAC(Permission = "MemberViewWeeklyReport")]
        public ActionResult ViewWeeklyReport(int Member_key, DateTime Date, FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            string memberid = mda.GetMemberId(Member_key);
            string ReportName = "/LTEPAdminReports/Activity Report";


            IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

            parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "ReportDate", Value = Date.ToShortDateString() });
            parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "MemberId", Value = memberid });
            parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "TimeFrame", Value = "Weekly" });
            // parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "ReportDateString", Value = state });
            //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
            //Date.ToString("yyyy-MM-dd hh:mm:ss tt")
            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            //return new ReportsResult(output, mimeType, format); this downloads report
            return new FileContentResult(output, mimeType);

        }

        //[RBAC(Permission = "MemberViewPayReport")]
        public ActionResult ViewPaymentsReport(int Member_key, string LodgementReferenceNo, FormCollection c, string PayrunID = "")
        {
            byte[] output;
            string extension, mimeType, encoding;
            Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            MemberModel mem = new MemberModel();
            mem = mda.GetMemeber(Member_key);
            string memberid = mda.GetMemberId(Member_key);
            string ReportName = "";
            IList<ParameterValue> parameters = new List<ParameterValue>();
            var tda = new TransactionDA();
            var PaymentType = tda.GetPaymentTypeViaLodgementRefNo(LodgementReferenceNo);


            var isOperator = mda.IsOperatorMember(Member_key, PayrunID);
            if (mem.CompanyKey == 1 || mem.CompanyKey == 20 || mem.CompanyKey == 23 || mem.CompanyKey == 24 || mem.CompanyKey == 25)
            {
                if (PaymentType == "BANK" && !String.IsNullOrEmpty(PayrunID))
                {
                    if (isOperator)
                    {

                        ReportName = "/LTEPAdminReports/Operator Pay Run Report (Detail)";
                        parameters = new List<ParameterValue>();
                        parameters.Add(new ParameterValue { Name = "MemberId", Value = memberid.ToString() });
                        parameters.Add(new ParameterValue { Name = "PayRunId", Value = PayrunID });
                        parameters.Add(new ParameterValue { Name = "ReportFormat", Value = "O" });
                        parameters.Add(new ParameterValue { Name = "ShowMessage", Value = "false" });
                    }
                    else
                    {
                        ReportName = "/LTEPAdminReports/Dealer Pay Report Detail";

                        parameters = new List<ParameterValue>();
                        parameters.Add(new ParameterValue { Name = "MemberId", Value = memberid.ToString() });
                        parameters.Add(new ParameterValue { Name = "PayRunId", Value = PayrunID });
                    }
                }
                else if (PaymentType == "CARD" && !String.IsNullOrEmpty(PayrunID))
                {
                    if (isOperator)
                    {
                        ReportName = "/LTEPAdminReports/DriverCard Operator Pay Run Report (Detail)";

                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = memberid.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ShowMessage", Value = "false" });
                    }
                    else
                    {
                        ReportName = "/LTEPAdminReports/Dealer Pay Report Detail";

                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = memberid.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                    }
                }
                else if (PaymentType == "CASH" && isOperator)
                {
                    ReportName = "/LTEPAdminReports/Operator Cashing Report";
                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "RefNumber", Value = LodgementReferenceNo });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });
                }
            }
            else if (mem.CompanyKey == 18 || mem.CompanyKey == 26)
            {
                if (PaymentType == "BANK" && !String.IsNullOrEmpty(PayrunID))
                {
                    //Validate the payrun belong to eCommerce or Live local or not.
                    var payrunDA = new PayrunDA();
                    if (payrunDA.BelongToPayrunGroup(PayrunID, "eCommerce"))
                    {
                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                        if (PayrunID.StartsWith("P")) // Payment report
                        {
                            ReportName = "/LTEPAdminReports/LEEcommercePaymentReport";
                        }
                        else
                        {
                            ReportName = "/LTEPAdminReports/Live Eftpos Payment Report";
                            parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                        }
                    }
                    else if (payrunDA.BelongToPayrunGroup(PayrunID, "Live local"))
                    {
                        ReportName = "/LTEPAdminReports/Live Eftpos Payment Report";

                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                    }
                    else if (payrunDA.BelongToPayrunGroup(PayrunID, "Integrated"))
                    {
                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                        if (PayrunID.StartsWith("P")) // Payment report
                        {
                            ReportName = "/LTEPAdminReports/LEIntegratedDevicePaymentReport";
                        }
                        else
                        {
                            ReportName = "/LTEPAdminReports/Live Eftpos Payment Report";
                            parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                        }
                    }
                    else
                    {
                        ReportName = "/LTEPAdminReports/Live Eftpos Payment Report";

                        parameters = new List<ParameterValue>();
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                    }

                }
                else if (PaymentType == "CASH" && isOperator)
                {
                    ReportName = "/LTEPAdminReports/Live Eftpos Cashing Report";
                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "RefNumber", Value = LodgementReferenceNo });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "TaxInvoice", Value = "true" });
                }
            }
            else if (mem.CompanyKey == 22 || mem.CompanyKey == 27)
            {

                //Validate the payrun belong to eCommerce or Live local or not.
                var payrunDA = new PayrunDA();
                if (payrunDA.BelongToPayrunGroup(PayrunID, "eCommerce") && PayrunID.StartsWith("P")) //Payment report
                {
                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });

                    if (PayrunID.StartsWith("P")) // Payment report
                    {
                        ReportName = "/LTEPAdminReports/LEEcommercePaymentReport";
                    }
                    else
                    {
                        ReportName = "/LTEPAdminReports/Live Eftpos Black Payment Report";
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                    }
                }
                else if (payrunDA.BelongToPayrunGroup(PayrunID, "Live local"))
                {

                    ReportName = "/LTEPAdminReports/Live Eftpos Black Payment Report";

                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                }
                //PJF - 1084
                else if (payrunDA.BelongToPayrunGroup(PayrunID, "Integrated"))
                {
                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                    if (PayrunID.StartsWith("P")) // Payment report
                    {
                        ReportName = "/LTEPAdminReports/LEIntegratedDevicePaymentReport";
                    }
                    else
                    {
                        ReportName = "/LTEPAdminReports/Live Eftpos Black Payment Report";
                        parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                    }
                }
                else
                {

                    ReportName = "/LTEPAdminReports/Live Eftpos Black Payment Report";

                    parameters = new List<ParameterValue>();
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                    parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
                }

            }
            else if (mem.CompanyKey == 29)
            {
                ReportName = "/LTEPAdminReports/CSAPaymentReport";
                //ReportName = "/LTEPAdminReports/Live Eftpos Payment Report";

                parameters = new List<ParameterValue>();
                parameters.Add(new ReportingWebService.ParameterValue { Name = "MemberKey", Value = Member_key.ToString() });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "PayRunId", Value = PayrunID });
                parameters.Add(new ReportingWebService.ParameterValue { Name = "ReportFormat", Value = "O" });
            }

            string format = "PDF";

            Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
            var cd = new ContentDisposition
            {
                FileName = String.Format("{0}_{1}.pdf", "PaymentReport", memberid),
                Inline = true
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(output, mimeType);
        }


        //[RBAC(Permission = "MemberViewMonthlyReport")]
        public ActionResult ViewLEEndOfMonthReport(int Member_key, int Month, int Year, FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            string memberid = mda.GetMemberId(Member_key);
            int companyKey = mda.GetMemberCompanyKey(Member_key);
            List<int> liveEftposCompanyKeys = Caching.GetSystemConfigurationByKey("LiveEftpos")
                                                        .Split(',')
                                                        .Select(s => Convert.ToInt32(s))
                                                        .ToList();

            string ReportName = String.Empty;
            if (companyKey == 29)
            {
                ReportName = "/LTEPAdminReports/CSA Monthly Payments";
            }
            else
            {
                ReportName = liveEftposCompanyKeys.Contains(companyKey) ?
                    "/EftposPortalReports/LiveEftpos Monthly Payments" :
                    "/EftposPortalReports/Monthly Report";
            }

            IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

            parameters.Add(new ReportingWebService.ParameterValue { Name = "memberkey", Value = Member_key.ToString() });
            parameters.Add(new ReportingWebService.ParameterValue { Name = "FinancialYear", Value = Year.ToString() });
            parameters.Add(new ReportingWebService.ParameterValue { Name = "Month", Value = Month.ToString() });
            // parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "ReportDateString", Value = state });
            //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            //return new ReportsResult(output, mimeType, format); this downloads report
            var cd = new ContentDisposition
            {
                FileName = String.Format("Monthly_Report_{0}.pdf", memberid),
                Inline = true
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(output, mimeType);

        }
        //[RBAC(Permission = "MemberViewMonthlyReport")]
        public ActionResult ViewLEEndOfFinanicialYearReport(int Member_key, int Year, FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            string memberid = mda.GetMemberId(Member_key);
            string ReportName = "/EftposPortalReports/LiveEftpos Yearly Payments";


            IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

            parameters.Add(new ReportingWebService.ParameterValue { Name = "memberkey", Value = Member_key.ToString() });
            parameters.Add(new ReportingWebService.ParameterValue { Name = "FinancialYear", Value = Year.ToString() });
            // parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "ReportDateString", Value = state });
            //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            //return new ReportsResult(output, mimeType, format); this downloads report
            return new FileContentResult(output, mimeType);

        }
        public ActionResult ViewLTEndOfFinanicialYearReport(int Member_key, int Year, FormCollection c)
        {
            byte[] output;
            string extension, mimeType, encoding;
            PhoenixWeb.ReportingWebService.Warning[] warnings;
            string[] streamIds;

            MemberDA mda = new MemberDA();
            string memberid = mda.GetMemberId(Member_key);
            string ReportName = "/EftposPortalReports/Live taxi End Of Financial Year Report";


            IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters = new List<PhoenixWeb.ReportingWebService.ParameterValue>();

            parameters.Add(new ReportingWebService.ParameterValue { Name = "memberkey", Value = Member_key.ToString() });
            parameters.Add(new ReportingWebService.ParameterValue { Name = "FinancialYear", Value = Year.ToString() });
            // parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "ReportDateString", Value = state });
            //  parameters.Add(new PhoenixWeb.ReportingWebService.ParameterValue { Name = "User", Value = "All" });
            string format = "PDF";
            PhoenixWeb.Models.Common.RenderReport(ReportName, format, parameters, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

            //return new ReportsResult(output, mimeType, format); this downloads report
            return new FileContentResult(output, mimeType);

        }
        public ActionResult SetCompany(string Company)
        {
            string BusinessType = string.Empty;
            int businesstypekey = 18;

            if (int.TryParse(Company, out businesstypekey))
            {

                CompanyDA cda = new CompanyDA();
                CompanyModel com = new CompanyModel();
                com = cda.Get(businesstypekey);
                BusinessType = com.Name;


                //if (Company == "1")
                //{
                //    BusinessType = "TaxiEPay";
                //}
                //else if (Company == "18")
                //{
                //    BusinessType = "LiveEftPos";
                //}
                //else if (Company == "19")
                //{
                //    BusinessType = "DirectLiveEftpos";
                //}
                //else if (Company == "20")
                //{
                //    BusinessType = "TaxiPro";
                //}
                Session["BusinessType"] = BusinessType;

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("fail", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditMemberCommissionRate(string MemberId)
        {
            ViewBag.MemberId = MemberId;
            MemberDA mda = new MemberDA();
            //var mkey = mda.GetMemberKey(MemberId);
            MemberModel modelm = new MemberModel();
            modelm = mda.GetComissionRateBaseOnMemberId(MemberId);

            CommissionRateModelVM model = new CommissionRateModelVM();
            model.StartDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"); ;
            model.CommissionRateModel = new CommissionRateVM();
            model.CurrentCommissionRateModel = new CommissionRateVM();
            model.CurrentCommissionRateModel.CRAmexRate = modelm.CRAmexRate;
            model.CurrentCommissionRateModel.CRDebitRate = modelm.CRDebitRate;
            model.CurrentCommissionRateModel.CRDinersRate = modelm.CRDinersRate;
            model.CurrentCommissionRateModel.CRMasterCardRate = modelm.CRMasterCardRate;
            model.CurrentCommissionRateModel.CRVisaRate = modelm.CRVisaRate;
            model.CurrentCommissionRateModel.CRUnionPayRate = modelm.CRUnionPayRate;
            model.CurrentCommissionRateModel.CRZipRate = modelm.CRZipPayRate;
            model.CurrentCommissionRateModel.CRQantasRate = modelm.CRQantasRate;
            model.CurrentCommissionRateModel.CRAlipayRate = modelm.CRAlipayRate;

            //model.CurrentCommissionRateModel.DebitRateType = modelm.DebitRateType;

            if (Session["MemberCommssionRate"] != null)
            {
                model = (CommissionRateModelVM)Session["MemberCommssionRate"];
            }

            if (Session["BusinessType"] != null)
                model.CommissionRateModel.BusinessType = Session["BusinessType"].ToString();
            else
                model.CommissionRateModel.BusinessType = modelm.BusinesstypeName;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCheckCommission(CommissionRateModelVM m, FormCollection c)
        {
            CommissionRateModelVM model = new CommissionRateModelVM();
            model.CommissionRateModel = new CommissionRateVM();
            model.CommissionRateModelAdded = new CommissionRateVM();
            var radio = c["rado"].ToString();
            var qantasradio = c["qantasrado"].ToString();
            var MemberId = c["MemberId"].ToString();

            var startdate = c["StartDate"].ToString();
            string[] dd = startdate.Split('-');
            DateTime sd = new DateTime(Convert.ToInt16(dd[0]), Convert.ToInt16(dd[1]), Convert.ToInt16(dd[2]));

            MemberDA mda = new MemberDA();
            var mkey = mda.GetMemberKey(MemberId);
            MemberModel modelm = new MemberModel();
            modelm = mda.GetMemeber(mkey);
            modelm.CRDebitRatePercentage = null;
            modelm.CRDebitRate = null;
            modelm.CRMasterCardRate = null;
            modelm.CRVisaRate = null;
            modelm.CRAmexRate = null;
            modelm.CRDinersRate = null;
            modelm.CRUnionPayRate = null;
            modelm.CRZipPayRate = null;
            modelm.CRQantasRate = null;
            modelm.CRQantasRatePercentage = null;
            modelm.CRAlipayRate = null;
            CommissionRateDA cdass = new CommissionRateDA();
            //vu said if zero is entered then zero is saved not default rate 27/06/2016
            DefaultRateModel drtss = new DefaultRateModel();
            int VehicleTypekey = 0;
            if (modelm.TerminalAllocationList != null && modelm.TerminalAllocationList.Count > 0)
            {
                VehicleTypekey = modelm.TerminalAllocationList.FirstOrDefault().VehicleModel.VehicleType_key.GetValueOrDefault();
            }

            if (radio == "Percentage")
            {
                // var re = c["CRDebitRate1"];

                decimal? CRDebitRate = null;
                //  if (c["CRDebitRate1"] != "")
                if (!String.IsNullOrEmpty(c["CRDebitRate1"]))
                {
                    CRDebitRate = Convert.ToDecimal(c["CRDebitRate1"].ToString());
                    if (CRDebitRate != 0)
                        modelm.CRDebitRatePercentage = CRDebitRate / 100;
                    else if (CRDebitRate == 0)
                        modelm.CRDebitRatePercentage = CRDebitRate;
                }
            }
            else
            {
                decimal? CRDebitRate = null;
                //if (c["CRDebitRate1"] != "")
                if (!String.IsNullOrEmpty(c["CRDebitRate1"]))
                {
                    CRDebitRate = Convert.ToDecimal(c["CRDebitRate1"].ToString());
                    if (CRDebitRate != 0)
                        modelm.CRDebitRate = CRDebitRate;
                }
            }

            decimal? CRMasterCardRate = null;
            //  if (c["CRMasterCardRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRMasterCardRate1"]))
            {
                CRMasterCardRate = Convert.ToDecimal(c["CRMasterCardRate1"].ToString());
                if (CRMasterCardRate != 0)
                    modelm.CRMasterCardRate = CRMasterCardRate / 100;
                else if (CRMasterCardRate == 0)
                    modelm.CRMasterCardRate = CRMasterCardRate;
            }

            decimal? CRVisaRate = null;
            //if (c["CRVisaRate1"] != "")

            if (!String.IsNullOrEmpty(c["CRVisaRate1"]))
            {
                CRVisaRate = Convert.ToDecimal(c["CRVisaRate1"].ToString());
                if (CRVisaRate != 0)
                    modelm.CRVisaRate = CRVisaRate / 100;
                else if (CRVisaRate == 0)
                    modelm.CRVisaRate = CRVisaRate;
            }

            decimal? CRAmexRate = null;
            //if (c["CRAmexRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRAmexRate1"]))
            {
                CRAmexRate = Convert.ToDecimal(c["CRAmexRate1"].ToString());
                if (CRAmexRate != 0)
                    modelm.CRAmexRate = CRAmexRate / 100;
                else if (CRAmexRate == 0)
                    modelm.CRAmexRate = CRAmexRate;
            }

            decimal? CRDinersRate = null;
            //if (c["CRDinersRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRDinersRate1"]))
            {
                CRDinersRate = Convert.ToDecimal(c["CRDinersRate1"].ToString());
                if (CRDinersRate != 0)
                    modelm.CRDinersRate = CRDinersRate / 100;
                else if (CRDinersRate == 0)
                    modelm.CRDinersRate = CRDinersRate;
            }

            decimal? CRUnionPayRate = null;
            //if (c["CRUnionPayRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRUnionPayRate1"]))
            {
                CRUnionPayRate = Convert.ToDecimal(c["CRUnionPayRate1"].ToString());
                if (CRUnionPayRate != 0)
                    modelm.CRUnionPayRate = CRUnionPayRate / 100;
                else if (CRUnionPayRate == 0)
                    modelm.CRUnionPayRate = CRUnionPayRate;
            }

            decimal? CRZipPayRate = null;
            //if (c["CRZipPayRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRZipPayRate1"]))
            {
                CRZipPayRate = Convert.ToDecimal(c["CRZipPayRate1"].ToString());
                if (CRZipPayRate != 0)
                    modelm.CRZipPayRate = CRZipPayRate / 100;
                else if (CRZipPayRate == 0)
                    modelm.CRZipPayRate = CRZipPayRate;
            }

            if (qantasradio == "Percentage")
            {
                //var re = c["CRQantasRate1"];

                decimal? CRQantasRate = null;
                // if (c["CRQantasRate1"] != "")
                if (!String.IsNullOrEmpty(c["CRQantasRate1"]))
                {
                    CRQantasRate = Convert.ToDecimal(c["CRQantasRate1"].ToString());
                    if (CRQantasRate != 0)
                        modelm.CRQantasRatePercentage = CRQantasRate / 100;
                    else if (CRQantasRate == 0)
                        modelm.CRQantasRatePercentage = CRQantasRate;
                }
            }
            else
            {
                decimal? CRQantasRate = null;
                //if (c["CRDebitRate1"] != "")
                if (!String.IsNullOrEmpty(c["CRQantasRate1"]))
                {
                    CRQantasRate = Convert.ToDecimal(c["CRQantasRate1"].ToString());
                    if (CRQantasRate != 0)
                        modelm.CRQantasRate = CRQantasRate;
                }
            }





            decimal? CRAlipayRate = null;
            // if (c["CRAlipayRate1"] != "")
            if (!String.IsNullOrEmpty(c["CRAlipayRate1"]))
            {
                CRAlipayRate = Convert.ToDecimal(c["CRAlipayRate1"].ToString());
                if (CRAlipayRate != 0)
                    modelm.CRAlipayRate = CRAlipayRate / 100;
                else if (CRAlipayRate == 0)
                    modelm.CRAlipayRate = CRAlipayRate;
            }
            Session["modelm"] = modelm;
            Session["mkey"] = mkey;
            Session["radio"] = radio;
            Session["qantasradio"] = qantasradio;
            Session["sd"] = sd;
            ViewBag.Msg = "Are you sure you want to proceed with the changes?";
            return View();
        }
        [HttpPost]
        public ActionResult EditMemberCommissionRate()
        {
            MemberModel modelm = (MemberModel)Session["modelm"];
            int mkey = (int)Session["mkey"];
            string radio = Session["radio"].ToString();
            string qantasradio = Session["qantasradio"].ToString();
            DateTime sd = (DateTime)Session["sd"];
            CommissionRateDA cdass = new CommissionRateDA();
            //add all commissions************************************************
            bool IsAdded = cdass.ProcessInsertAllNewCommssionRate(modelm, mkey, User.Identity.Name, radio, sd, qantasradio);
            if (IsAdded == false)
            {
                throw new Exception("Error adding commssion rates");
            }


            return RedirectToAction("Edit", new { member_key = mkey });
        }

        public ActionResult CreateMemberCommissionRate()
        {
            CommissionRateModelVM model = new CommissionRateModelVM();
            model.CommissionRateModel = new CommissionRateVM();

            if (Session["MemberCommssionRate"] != null)
            {
                model = (CommissionRateModelVM)Session["MemberCommssionRate"];
            }

            var SFmodel = (MemberModel)Session["SalesforceMemberModel"];

            if (SFmodel != null)
            {
                model.SalesforceData = new CommissionRateVM();
                if (SFmodel.CRDebitRate != null)
                {
                    model.SalesforceData.CRDebitRate = SFmodel.CRDebitRate.Value;
                }

                if (SFmodel.CRVisaRate != null)
                {
                    model.SalesforceData.CRVisaRate = SFmodel.CRVisaRate;
                }

                if (SFmodel.CRMasterCardRate != null)
                {
                    model.SalesforceData.CRMasterCardRate = SFmodel.CRMasterCardRate;
                }

                if (SFmodel.CRDinersRate != null)
                {
                    model.SalesforceData.CRDinersRate = SFmodel.CRDinersRate;
                }

                if (SFmodel.CRAmexRate != null)
                {
                    model.SalesforceData.CRAmexRate = SFmodel.CRAmexRate;
                }

                if (SFmodel.CRDebitRatePercentage != null)
                {
                    model.SalesforceData.CRDebitRatePercentage = SFmodel.CRDebitRatePercentage;
                }
            }
            if (Session["BusinessType"] != null)
                model.CommissionRateModel.BusinessType = Session["BusinessType"].ToString();
            return View(model);
        }
        [HttpPost]
        public ActionResult CreateMemberCommissionRate(CommissionRateModelVM m, FormCollection c)
        {
            CommissionRateModelVM model = new CommissionRateModelVM();
            model.CommissionRateModel = new CommissionRateVM();
            model.CommissionRateModelAdded = new CommissionRateVM();
            var radio = c["rado"].ToString();
            var qantasradio = c["qantasrado"].ToString();

            decimal DebitRate = 0;
            Decimal.TryParse(c["CRDebitRate"].ToString(), out DebitRate);

            decimal MasterCardRate = 0;
            Decimal.TryParse(c["CRMasterCardRate"].ToString(), out MasterCardRate);
            //if (MasterCardRate != 0)
            //    model.CRMasterCardRate = MasterCardRate / 100;
            //else
            //    model.CRMasterCardRate = drtss.Master / 100;

            decimal VisaCardRate = 0;
            Decimal.TryParse(c["CRVisaRate"].ToString(), out VisaCardRate);
            //if (VisaCardRate != 0)                
            //    model.CRVisaRate = VisaCardRate / 100;
            //else
            //    model.CRVisaRate = drtss.Visa / 100;

            decimal AmexCardRate = 0;
            Decimal.TryParse(c["CRAmexRate"].ToString(), out AmexCardRate);
            //if (AmexCardRate != 0)              
            //    model.CRAmexRate = AmexCardRate / 100;
            //else
            //    model.CRAmexRate = drtss.Amex / 100;

            decimal DinersCardRate = 0;
            Decimal.TryParse(c["CRDinersRate"].ToString(), out DinersCardRate);

            decimal UnionPayRate = 0;
            Decimal.TryParse(c["CRUnionPayRate"].ToString(), out UnionPayRate);

            decimal ZipPayRate = 0;
            Decimal.TryParse(c["CRZipPayRate"].ToString(), out ZipPayRate);

            decimal QantasRate = 0;
            Decimal.TryParse(c["CRQantasRate"].ToString(), out QantasRate);

            decimal AlipayRate = 0;
            Decimal.TryParse(c["CRAlipayRate"].ToString(), out AlipayRate);
            //if (DinersCardRate != 0)                  
            //    model.CRDinersRate = DinersCardRate / 100;
            //else
            //    model.CRDinersRate = drtss.Diners / 100;

            //m.CommissionRateModelAdded = new CommissionRateVM();
            model.CommissionRateModelAdded.CRDebitRate = DebitRate;
            model.CommissionRateModelAdded.CRMasterCardRate = MasterCardRate;
            model.CommissionRateModelAdded.CRVisaRate = VisaCardRate;
            model.CommissionRateModelAdded.CRAmexRate = AmexCardRate;
            model.CommissionRateModelAdded.CRDinersRate = DinersCardRate;

            model.CommissionRateModelAdded.DebitRateType = radio;

            model.CommissionRateModelAdded.CRUnionPayRate = UnionPayRate;
            model.CommissionRateModelAdded.CRZipRate = ZipPayRate;
            model.CommissionRateModelAdded.CRQantasRate = QantasRate;
            model.CommissionRateModelAdded.CRAlipayRate = AlipayRate;

            model.CommissionRateModelAdded.QantasRateType = qantasradio;

            if (Session["BusinessType"] != null)
                model.CommissionRateModelAdded.BusinessType = Session["BusinessType"].ToString();

            Session["MemberCommssionRate"] = model;
            return View(model);
        }

        [RBAC(Permission = "MemberEditMember")]
        public ActionResult Edit(int member_key)
        {
            var error = TempData["Error"] as string;
            Session.Clear();
            Session.RemoveAll();
            TempData["Error"] = string.Empty;
            MemberDA mda = new MemberDA();
            MemberModel model = new MemberModel();
            model = mda.GetMemeber(member_key);
            model.AllowResetEmailNotification =
                model.AllVehicleTerminalAllocation.Any(n => n.IsAllocated && n.AutoCreatePortalAccount);
            model.EnableTransactionListOnEOMReport = mda.GetEnableTransactionListOnEOMStatement(null, member_key);
            if (model.AllVehicleTerminalAllocation.Any(n => n.IsAllocated))
            {
                model.SubBusinessTypeKey =
                    model.AllVehicleTerminalAllocation.First(n => n.IsAllocated).VehicleTypeKey;
            }

            var purposeList = mda.GetPurposeListForMember();
            model.PurposeTypes = JsonConvert.SerializeObject(purposeList);

            //PHW-378
            model.DOBstring = model.DOB.HasValue ? model.DOB.Value.ToString("dd/MM/yyyy") : String.Empty;
            //PHW-378
            model.PhotoIDExpiryString = model.PhotoIDExpiry.HasValue ? model.PhotoIDExpiry.Value.ToString("dd/MM/yyyy") : String.Empty;

            Session["Member_key"] = member_key;
            Session["Company_key"] = model.CompanyKey;
            if (model.MyWebUserProfile != null)
            {
                Session["UserId"] = model.MyWebUserProfile.UserId;
            }
            Session["CurrentMemberId"] = model.MemberId;
            Session["CurrentMyWebTradingName"] = model.TradingName;

            Session["BusinessType"] = model.BusinesstypeName;

            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(model.CompanyKey).ToList();
            if (MemberRentalTemplateList.Count != 0)
            {
                if (model.CompanyKey == 18)
                {
                    model.RentalMemberRentalTemplateKey = 7;
                }
                model.RentalMemberRentalTemplateKey = MemberRentalTemplateList.FirstOrDefault().MemberRentalTemplateKey;
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                List<TerminalRentalRateModel> rmodel = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value);
                model.TerminalRentalRateList = rmodel.OrderBy(n => n.TerminalRentalRate_key).ToList();
            }

            if (!string.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("error", error);
            }

            if (model.TerminalAllocationList.Count == 0)
            {
                ModelState.AddModelError("error", "No Terminals Allocated");
            }
            if (string.IsNullOrEmpty(model.DriverCardDtoAsJson) && model.PaymentType == "Card")
            {
                ModelState.AddModelError("error", "Please add driver card");
            }

            model.BankAccountDtoAsJson = mda.GetBankAccountAndPurposeTypeOfMember(member_key);
            model.FeeDtoAsJson = JsonConvert.SerializeObject(model.Fees);
            model.ChargeFeeTypeDtoAsJson = JsonConvert.SerializeObject(model.ChargeFeeType);
            model.CommissionRatesDtoAsJson = JsonConvert.SerializeObject(model.CommissionRates);
            var isOperator = mda.IsOperatorMember(member_key, string.Empty);
            if (!isOperator)
            {
                DrawDropDownConfig(model.CompanyKey, false, false, true);
                if (model.PaymentType == "Card")
                {
                    ModelState.AddModelError("error", "This Member is a Dealer. Cannot use Card as a payment type. The payment type has been updated to Bank");
                    model.PaymentType = "Bank";
                }
            }
            else
            {
                DrawDropDownConfig(model.CompanyKey);
            }

            DriverCardStatusDA dcsda = new DriverCardStatusDA();
            ViewBag.DriverCardStatus = new SelectList(
                dcsda.GetAllCardStatus().Select(x => new SelectListItem()
                {
                    Text = x.Name + " - " + x.Description,
                    Value = x.ID.ToString(),
                }),
                "Value",
                "Text");

            StatusDA sda = new StatusDA();
            model.StatusTypes.Add(new SelectListItem
            {
                Value = "",
                Text = "Select All"
            });
            model.StatusTypes.AddRange(sda.GetAll()
                                        .Select(m => new SelectListItem
                                        {
                                            Value = m.Name,
                                            Text = m.Description
                                        }).ToList());

            ViewBag.MemberKey = member_key;
            ViewBag.ListMCCRequiredBusinessType = Caching.GetSystemConfigurationByKey("ListMCCRequiredBusinessType");

            ViewBag.PayMethodList = PayMethodList;

            return View(model);
        }
        public JsonResult ResendCreateNewAccountEmail(int id)
        {
            MyWebDA mwda = new MyWebDA();
            var myWebUserProfile = mwda.GetLoginProfile(id);
            if (myWebUserProfile.UserId != 0)
            {
                //Already existed Myweb account.
                return new JsonResult()
                {
                    Data = new
                    {
                        IsError = true,
                        Message = "This member already have MyWeb account. Please reload the page to see MyWeb details."
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            MemberDA mda = new MemberDA();
            var member = mda.GetBasicMemberInfo(id);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            Auth0ManagementAPI auth0 = new Auth0ManagementAPI(
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientID.ToString()),
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientSecretKey.ToString()),
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationAudience.ToString()),
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationBaseURL.ToString()),
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseCreatePortalAccountURL.ToString()));
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var jsonboject = new
            {
                memberKey = member.Member_key,
                userName = member.Email
            };
            request.AddParameter("application/json", JsonConvert.SerializeObject(jsonboject), ParameterType.RequestBody);
            var msg = string.Empty;
            var isError = false;
            try
            {
                var data = auth0.Execute<FuseCreateAccountModel>(request);
                if (data.error)
                {
                    isError = true;
                    msg = ErrorMessageConstant.CommonSystemError;
                }
                else
                {
                    isError = false;
                    msg = "Email has been sent successfully.";
                }
            }
            catch (Exception e)
            {
                isError = true;
                LogError($"Can not access to {Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseCreatePortalAccountURL.ToString())}");
                LogError(e);
                msg = ErrorMessageConstant.CommonSystemError;
            }
            return new JsonResult()
            {
                Data = new
                {
                    IsError = isError,
                    Message = msg
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        [RBAC(Permission = "MemberEditMember")]
        public ActionResult Edit(MemberModel m, FormCollection c, HttpPostedFileBase file)
        {
            MemberModel model = new MemberModel();
            int memkey = 0;
            Int32.TryParse(c["Member_key"], out memkey);
            try
            {
                model.Member_key = memkey;
                DrawDropDownConfig(m.CompanyKey);
                DriverCardStatusDA dcsda = new DriverCardStatusDA();
                ViewBag.DriverCardStatus = new SelectList(
                    dcsda.GetAllCardStatus().Select(x => new SelectListItem()
                    {
                        Text = x.Name + " - " + x.Description,
                        Value = x.ID.ToString(),
                    }),
                    "Value",
                    "Text");
                ViewBag.ListMCCRequiredBusinessType = Caching.GetSystemConfigurationByKey("ListMCCRequiredBusinessType");
                MemberDA mda = new MemberDA();
                model = mda.GetMemeber(memkey);
                bool isActiveZipPayment = model.ZipMerchantAPIModel.IsActive;
                if (model.AllVehicleTerminalAllocation.Any(n => n.IsAllocated))
                {
                    model.SubBusinessTypeKey =
                        model.AllVehicleTerminalAllocation.First(n => n.IsAllocated).VehicleTypeKey;
                }

                //Validate commission rate
                var commissionRates = JsonConvert.DeserializeObject<List<MemberCommissionRateModel>>(m.CommissionRatesDtoAsJson);
                var savingTerminalAllocationSubBusinessTypeIds = model.AllVehicleTerminalAllocation
                                                                    .Where(t => t.TerminalId != null)
                                                                    .Select(t => t.VehicleTypeKey)
                                                                    .Distinct()
                                                                    .ToList();
                var savingSubBusinessTypeIds = commissionRates.Select(com => com.SubBusinessTypeID).Distinct().ToList();
                if (m.IsFeeFreeMember)
                {
                    var lstSubBusinessTypeAllowNoCR =
                        Caching.GetSystemConfigurationByKey("SubBusinessTypeIdsNotRequireMSF");
                    lstSubBusinessTypeAllowNoCR.Split(';').ToList().ForEach(s => savingSubBusinessTypeIds.Add(Convert.ToInt32(s)));
                }
                var result = savingTerminalAllocationSubBusinessTypeIds.Except(savingSubBusinessTypeIds).Count();

                model.CompanyKey = m.CompanyKey;
                model.DealerKey = m.DealerKey;

                model.MerchantWebsite = m.MerchantWebsite;
                model.BusinesstypeName = m.BusinesstypeName;
                model.MemberId = c["MemberId"].ToString();
                model.FirstName = c["FirstName"].ToString();
                model.LastName = c["LastName"].ToString();
                model.Telephone = c["Telephone"].ToString();
                model.Fax = c["Fax"].ToString();
                model.Mobile = c["Mobile"].ToString();
                model.TradingName = c["TradingName"].ToString();
                model.ABN = c["ABN"].ToString();
                model.ACN = c["ACN"].ToString();
                model.DriverLicenseNumber = c["DriverLicenseNumber"].ToString();
                model.Email = c["Email"].ToString();
                model.MerchantID = c["MerchantID"].ToString();
                model.ReceiveMonthlyActivityReport = m.ReceiveMonthlyActivityReportB;
                model.ReceivePaymentReport = m.ReceivePaymentReportB;
                model.ReceiveWeeklyActivityReport = m.ReceiveWeeklyActivityReportB;
                model.MCC = m.MCC;
                model.AverageTurnover = m.AverageTurnover;
                model.AverageTransaction = m.AverageTransaction;
                model.QBRNumber = m.QBRNumber;
                model.IsDisableQBR = m.IsDisableQBR;
                model.EOMMSFPayment = m.EOMMSFPayment;
                model.IndirectAmexSettlement = m.IndirectAmexSettlement;
                model.IsFeeFreeMember = m.IsFeeFreeMember;

                if (c["ExtendedSettlement"] != null)
                {
                    model.ExtendedSettlement = m.ExtendedSettlement;
                }

                model.AmexMerchantID = m.AmexMerchantID;

                DateTime PhotoIDExpiry;
                DateTime.TryParse(c["PhotoIDExpiry"].ToString(), out PhotoIDExpiry);
                if (PhotoIDExpiry != null)
                    model.PhotoIDExpiry = PhotoIDExpiry;

                model.Active = m.Active;

                var paymentType = m.PaymentType;
                if (paymentType == "Bank")
                {
                    model.IsBankCustomer = true;
                    model.PaymentTypeId = 1;
                }
                else if (paymentType == "Cash")
                {
                    model.IsBankCustomer = false;
                    model.PaymentTypeId = 2;
                }
                else if (paymentType == "Card")
                {
                    model.IsBankCustomer = false;
                    model.PaymentTypeId = 3;
                }

                model.PaymentType = paymentType;
                var IsOperator = mda.IsOperatorMember(memkey, string.Empty);
                if (model.PaymentType == "Card" && !IsOperator)
                {
                    throw new Exception("This Member is a Dealer. Cannot use Card as a payment type. The payment type has been updated to Bank");
                }

                DateTime dob;
                //DateTime.TryParse(c["DOB"].ToString(), out dob);
                dob = DateTime.ParseExact(c["DOBstring"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (dob != null)
                {
                    model.DOB = dob;
                }
                model.IsOperator = true;
                model.Modified_dttm = DateTime.Now;
                model.ModifiedByUser = User.Identity.Name;
                model.BankAccountDtoAsJson = m.BankAccountDtoAsJson;
                model.FeeDtoAsJson = m.FeeDtoAsJson;
                model.CommissionRatesDtoAsJson = m.CommissionRatesDtoAsJson;
                model.RebatesDtoAsJson = m.RebatesDtoAsJson;
                model.DriverCardDtoAsJson = m.DriverCardDtoAsJson;
                model.EnableSplitPaymentReport = m.EnableSplitPaymentReport;
                var isUpdateSubBusinessType = false;
                var companyKeysAllowOnlyOneSubBusToAllTerminals =
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                        .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

                if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(m.CompanyKey.ToString()))
                {
                    isUpdateSubBusinessType = model.SubBusinessTypeKey != m.SubBusinessTypeKey;
                }

                model.SubBusinessTypeKey = m.SubBusinessTypeKey;

                byte[] photoid = new byte[1];
                if (file != null)
                {
                    bool validFile = Regex.IsMatch(file.FileName, "(.*?)\\.(JPG|jpg|PNG|png|JPEG|jpeg)$");
                    if (file.ContentLength > 0 && validFile)
                    {
                        BinaryReader reader = new BinaryReader(file.InputStream);
                        photoid = reader.ReadBytes((int)file.InputStream.Length);
                        model.PhotoID = photoid;
                    }
                }

                if (result > 0)
                {
                    throw new Exception("Please add a Commission Rate/MSF for all sub-business types of this Member.");
                }

                //var businessTypeAllowRebate = Caching.GetSystemConfigurationByKey("BusinessKeysAllowRebate").Split(';').ToList();
                //if (businessTypeAllowRebate.Contains(model.CompanyKey.ToString()) && model.Rebates.Count == 0)
                //{
                //    throw new Exception("Please input rebate amount.");
                //}
                MemberDA mem = new MemberDA();
                List<CardSurchargeInfo> cardSurchargeInfos = new List<CardSurchargeInfo>();
                model.EnableTransactionListOnEOMReport = m.EnableTransactionListOnEOMReport;
                mem.Update(model, this.ControllerContext.HttpContext.User.Identity.Name, ref cardSurchargeInfos, isUpdateSubBusinessType);
                if (cardSurchargeInfos.Count > 0)
                {
                    SendEmailForChangingSurcharge(cardSurchargeInfos, model.FirstName, model.Email);
                }

                MemberAddressDA mdaq = new MemberAddressDA();

                if (c["StreetNo"] != "" && c["Street"] != "" && c["Postcode1"] != "")
                {
                    var c1 = c["Postcode1"].ToString();
                    string[] pc = c1.Split(',');

                    AddressListModel m1 = new AddressListModel();
                    int id = memkey;
                    MemberDA mda1 = new MemberDA();
                    m1.Addresskey = m.PrimaryAddressKey;
                    m1.StreetNumber = c["StreetNo"].ToString();
                    m1.StreetName = c["Street"].ToString();
                    m1.PostCode = pc[0].Trim();
                    m1.State = pc[1].Trim();
                    m1.Suburb = pc[2].Trim();
                    m1.Country = "Australia";
                    m1.AddressType = "Primary";


                    bool doesExists = mdaq.CheckIfAddressExists(m1.State, m1.PostCode, m1.Suburb);
                    if (doesExists == false)
                    {
                        throw new Exception("Error adding primary address, please check the state/suburb");
                    }

                    var lstSubBusinessTypeForVICStateMerchantOnly = Caching.GetSystemConfigurationByKey("SubBusinessTypeForVICStateMerchantOnly").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                    bool isAnyTerminalAllocatedWithSubBusinessVIC = new TerminalAllocationDA()
                        .GetTerminalAllocationList(m.Member_key).Any(t => lstSubBusinessTypeForVICStateMerchantOnly.Contains(t.VehicleModel.VehicleType_key.ToString()));
                    if (isAnyTerminalAllocatedWithSubBusinessVIC)
                    {
                        if (m1.State != "VIC")
                            throw new Exception(
                                "Incorrect sub-business type selected. Live taxi Black VIC can only be used for Victorian members");
                    }

                    if (m.PrimaryAddressKey == 0)
                    {
                        mdaq.Add(m1, id);
                    }
                    else
                    {
                        mdaq.Add(m1, id, true);
                    }
                }

                if (c["MStreetNo"] != "" && c["MStreet"] != "" && c["MPostcode1"] != "")
                {
                    var c1 = c["MPostcode1"].ToString();
                    string[] pc = c1.Split(',');

                    AddressListModel m1 = new AddressListModel();
                    int id = memkey;
                    MemberDA mda1 = new MemberDA();
                    m1.Addresskey = m.MailingAddressKey;
                    m1.StreetNumber = c["MStreetNo"].ToString();
                    m1.StreetName = c["MStreet"].ToString();
                    m1.PostCode = pc[0].Trim();
                    m1.State = pc[1].Trim();
                    m1.Suburb = pc[2].Trim();
                    m1.Country = "Australia";
                    m1.AddressType = "Mailing";

                    bool doesExists = mdaq.CheckIfAddressExists(m1.State, m1.PostCode, m1.Suburb);
                    if (doesExists == false)
                    {
                        throw new Exception("Error adding primary address, please check the state/suburb");
                    }

                    if (m.MailingAddressKey == 0)
                    {
                        mdaq.Add(m1, id);
                    }
                    else
                    {
                        mdaq.Add(m1, id, true);
                    }
                }
                else if (c["MStreetNo"] == "" && c["MStreet"] != "" && c["MPostcode1"] != "")
                {
                    throw new Exception("Error adding mailing address, please try resolving it");
                }

                SaveDriverAuth(c["DriverAuth"].ToString(), memkey);

                //ZipMerchantAPI
                if (this.HasPermission("AccessZipAPI"))
                {
                    AddOrUpdateZipMerchantAPI(m.ZipMerchantAPIModel);
                }

                if (isActiveZipPayment != m.ZipMerchantAPIModel.IsActive && !m.ZipMerchantAPIModel.IsActive)
                {
                    DisableZipPaymentOfAllTerminals(m.Member_key);
                }

                ModelState.AddModelError("error", "Updated!");
                return RedirectToAction("Edit", new { member_key = memkey });
            }
            catch (Exception ex)
            {
                LogError(ex);
                TempData["Error"] = ex.Message;
                return RedirectToAction("Edit", new { member_key = memkey });
            }
        }

        private void SaveDriverAuth(string driverAuth, int memkey)
        {
            if (!string.IsNullOrEmpty(driverAuth))
            {
                DriverDA drda = new DriverDA();

                DriverModel driverModel = new DriverModel();
                driverModel.Driver_key = memkey;
                driverModel.DriverCertificateId = driverAuth;
                driverModel.Modified_dttm = DateTime.Now;
                driverModel.ModifiedByUser = User.Identity.Name;

                var driverCertificate = drda.GetDriverByMemberKey(memkey);
                if (driverCertificate != null)
                {
                    drda.Update(driverModel);
                }
                else
                {
                    drda.Add(driverModel);
                }

            }
        }

        private void AddOrUpdateZipMerchantAPI(ZipMerchantAPIModel zipMerchantAPIModel)
        {
            //verify ZIP API and LocationID
            //Write here

            //Insert or Update
            ZipMerchantAPIDA zipMerchantAPIDA = new ZipMerchantAPIDA();
            //check exist and delete row from ZipMerchantAPI
            var checkExistZipMerchantAPI = zipMerchantAPIDA.GetByMemberKey((int)zipMerchantAPIModel.MemberKey);
            if (string.IsNullOrEmpty(zipMerchantAPIModel.ZipAPIKey) && string.IsNullOrEmpty(zipMerchantAPIModel.LocationID))
            {
                if (checkExistZipMerchantAPI != null)
                {
                    zipMerchantAPIDA.Delete(zipMerchantAPIModel.MemberKey, User.Identity.Name);
                }
            }
            else
            {
                //check exist then add new row or update
                if (checkExistZipMerchantAPI != null)
                {
                    if (!(zipMerchantAPIModel.ZipAPIKey == checkExistZipMerchantAPI.ZipAPIKey &&
                        zipMerchantAPIModel.LocationID == checkExistZipMerchantAPI.ZipLocationID &&
                        !zipMerchantAPIModel.IsActive == checkExistZipMerchantAPI.InActive))
                    {
                        var orgZipMerchant = new ZipMerchantAPI()
                        {
                            ZipLocationID = checkExistZipMerchantAPI.ZipLocationID,
                            ZipAPIKey = checkExistZipMerchantAPI.ZipAPIKey,
                            InActive = checkExistZipMerchantAPI.InActive
                        };
                        var updateZipMerchantAPI = zipMerchantAPIDA.FromZipMerchantAPIModel(zipMerchantAPIModel, checkExistZipMerchantAPI);
                        zipMerchantAPIDA.Update(updateZipMerchantAPI, User.Identity.Name, orgZipMerchant);
                    }
                }
                else
                {
                    var zipMerchantAPI = zipMerchantAPIDA.FromZipMerchantAPIModel(zipMerchantAPIModel, new ZipMerchantAPI());
                    zipMerchantAPIDA.AddNew(zipMerchantAPI, User.Identity.Name);
                }
            }
        }

        public ActionResult AddSearchList(MemberSearchModel m, FormCollection c)
        {
            string key1 = c["SearchKey1label"].ToString();
            string value = c["SearchKey1"].ToString();
            SearchListModel sm = new SearchListModel();
            sm.SearchKey = key1;
            sm.SearchField = value;
            m.SearchList.Add(sm);
            return View("Search", m);
        }

        [RBAC(Permission = "MemberSearchMember")]
        public ActionResult Search()
        {
            MemberSearchVM model = new MemberSearchVM();
            PaymentDA pda = new PaymentDA();
            BusinessTypeDA bda = new BusinessTypeDA();

            model.PaymentTypes = pda.GetAllPaymentTypes().ToList().AsSelectListItem().ToList();
            model.BusinessTypes = bda.GetBusinessTypes().AsSelectListItem().ToList();
            model.QBRStatuses = PhoenixWeb.Models.Common.GenerateDisableEnableListItem().ToList();
            model.Dealers = new List<SelectListItem>() { new SelectListItem { Text = "All", Value = "" } };

            return View(model);
        }

        [RBAC(Permission = "MemberAddMemberPortalMembership")]
        public ActionResult AddMemberPortalMembership(int id)
        {
            ViewBag.Msg = "Enter a UserName, If left blank email address will be used as username!";
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberPortalMembership")]
        public ActionResult AddMemberPortalMembership(FormCollection c)
        {
            try
            {
                int memberkey = Convert.ToInt32(c["Memberkey"].ToString());

                MemberDA mda = new MemberDA();
                MemberModel mem = new MemberModel();
                mem = mda.GetMemeber(memberkey);
                if (string.IsNullOrEmpty(mem.Email))
                {
                    ViewBag.Error = "Member is missing email address, please update!";
                    return View("Msg");
                }

                string username = c["PortalUserName"].ToString();
                if (username == string.Empty)
                {
                    username = mem.Email.ToLower();
                }
                MyWebDA myda = new MyWebDA();
                bool usernameexists = myda.DoesUserNameExists(username);
                if (usernameexists == true)
                {
                    ViewBag.Error = "This username exists for another member, please try another username";
                    return View("Msg");
                }

                //atleast they are member add them no need for terminal*****************************

                //TerminalDA tda = new TerminalDA();
                //string tid = tda.AnyOneTerminalAllocatedToThisMember(memberkey);
                //if (tid == string.Empty)
                //{
                //    ViewBag.Error = "Could not locate any terminal for this member";
                //    return View("Msg");
                //}
                //************************************************************************************


                //string password = mem.LastName + DateTime.Now.Year + DateTime.Now.Month;
                string password = PhoenixWeb.Helper.Common.GenerateRandomPassword(6);
                DateTime CreatedDate = DateTime.Now;

                CustomerPortalDA customerPortalDA = new CustomerPortalDA("LiveGroupPortalDefaultConnection");
                customerPortalDA.CreateUserAndAccount(username, password, new
                {
                    EmailAddress = mem.Email,
                    MemberId = mem.MemberId,
                    TerminalId = string.Empty,
                    Inactive = 0,
                    IsAdmin = 0,
                    CreatedDate = CreatedDate,
                    UpdatedDate = DateTime.Now
                });

                LiveGroupPortal_dbDA lgda = new LiveGroupPortal_dbDA();

                lgda.AddMemberShipNewDatabase(username);

                //update old password database too- LiveEftpos
                AddNewUserToOldDatabase(username, password, mem.Email);

                if (mem.CompanyKey == 1 || mem.CompanyKey == 20)
                {
                    string subject = "LivetaxiEpay MyWeb Registration";
                    string body = "<b>Hi " + mem.FirstName + " " + mem.LastName + ",</b><br/><br/>";
                    body += "<b>You have successfully registered an account on LivetaxiEpay MyWeb.</b><br/>";
                    body += "<b>Details of your registration are below.</b><br/><br/>";
                    body += "<b>UserName: " + username + "<b><br/><br/>";
                    body += "<b>Temporary Password: " + password + "<b><br/><br/>";
                    body += "<b>Email Address : " + (mem.Email ?? "") + "<b><br/><br/>";
                    body += "<b>Member ID: " + (mem.MemberId ?? "") + "<b><br/><br/>";
                    //body += "<b>Terminal ID: " + (tid ?? "") + "<b><br/><br/>";
                    body += "<b>Date: " + CreatedDate + "<b><br/><br/>";
                    body += $"Login at {Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MyWebLiveTaxiURL.ToString())} <br/><br/>";

                    body += "<b>LiveEftpos<b>";

                    List<String> _to = new List<string>();
                    _to.Add(mem.Email);
                    Helper.Common.SendMail(_to, null, null, subject, body, true, "it@livegroup.com.au");

                }
                else
                {
                    string subject = "LiveEftpos MyWeb Registration";
                    string body = "<b>Hi " + mem.FirstName + " " + mem.LastName + ",</b><br/><br/>";
                    body += "<b>You have successfully registered an account on LiveEftpos MyWeb.</b><br/>";
                    body += "<b>Details of your registration are below.</b><br/><br/>";
                    body += "<b>UserName: " + username + "<b><br/><br/>";
                    body += "<b>Temporary Password: " + password + "<b><br/><br/>";
                    body += "<b>Email Address : " + (mem.Email ?? "") + "<b><br/><br/>";
                    body += "<b>Member ID: " + (mem.MemberId ?? "") + "<b><br/><br/>";
                    //body += "<b>Terminal ID: " + (tid ?? "") + "<b><br/><br/>";
                    body += "<b>Date: " + CreatedDate + "<b><br/><br/>";
                    body += $"Login at {Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MyWebLiveEftposURL.ToString())} <br/><br/>";

                    body += "<b>LiveEftpos<b>";

                    List<String> _to = new List<string>();
                    _to.Add(mem.Email);
                    Helper.Common.SendMail(_to, null, null, subject, body, true, "it@livegroup.com.au");
                }

                ViewBag.Error = "Done! An email as been send to the member regarding username and password";
                ViewBag.BtnName = "Ok";
                ViewBag.ContAction = "Edit";
                ViewBag.PraValus = mem.Member_key;
                ViewBag.Title = "Email";
                return View("CommonMsg1");

            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return View();
        }

        [HttpGet]
        public JsonResult ValidateQBRNumber(string qbrNumber)
        {

            var client = new RestClient($"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.qbrEndPoint.ToString())}?abnNumber={qbrNumber}");
            var request = new RestRequest(Method.GET);
            var response = client.Execute<CommonModel>(request);

            return new JsonResult()
            {
                Data = response.Data,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private void AddNewUserToOldDatabase(string Username, string Password, string EmailAddress)
        {
            var portalmembershipProvider = GetMembershipProviderByName("AspNetSqlMembershipProvider1");
            MembershipCreateStatus createStatus;
            portalmembershipProvider.CreateUser(Username, Password, EmailAddress, "LiveGroup", "LiveGroup", true, null, out createStatus);
        }

        [HttpPost]
        [RBAC(Permission = "MemberResetPasswordPortalMembership")]
        public ActionResult ResetPasswordPortalMembership(int id, string UserName)
        {
            try
            {
                int memberkey = id;
                MemberDA mda = new MemberDA();
                MemberModel mem = new MemberModel();
                mem = mda.GetMemeber(memberkey);

                string password = mem.LastName + DateTime.Now.Year + DateTime.Now.Month;
                DateTime CreatedDate = DateTime.Now;

                string newpassword = Helper.Common.GenerateRandomPassword(6);

                CustomerPortalDA customerPortalDA = new CustomerPortalDA("LiveGroupPortalDefaultConnection");
                bool response = customerPortalDA.ResetPasswordPortalMembership(string.Empty, newpassword, UserName);

                if (response)
                {
                    string subject = "New Password";
                    string body = "<b>Hi " + UserName + ",</b><br/><br/>";
                    body += "You have requested a password reset.<br/>";

                    body += "Please find your new password below. Once Logged in you can change your password by going to the my profile section on your portal<br/><br/>";
                    body += "<b>UserName: " + UserName + "<b><br/><br/>";
                    body += "<b>Password: " + newpassword + "<b><br/><br/>";
                    if (mem.CompanyKey == 1)
                    {
                        body += $"Login at {Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MyWebLiveTaxiURL.ToString())} MyWeb <br/><br/>";
                        body += "<b>LiveTaxiEpay<b>";
                    }
                    else
                    {
                        body += $"Login at {Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MyWebLiveEftposURL.ToString())} <br/><br/>";
                        body += "<b>LiveEftpos<b>";
                    }

                    try
                    {
                        List<String> _to = new List<string>();
                        _to.Add(mem.Email);

                        if (Helper.Common.SendMail(_to, null, null, subject, body, false, "it@livegroup.com.au"))
                        {
                            ModelState.AddModelError("", "Reset successful, mail Sent.");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Error occured while sending email.");
                        }
                        ViewBag.Error = "Done! An email as been send to the member regarding temporary password";
                        ViewBag.BtnName = "Ok";
                        ViewBag.ContAction = "Edit";
                        ViewBag.PraValus = memberkey;
                        ViewBag.Title = "Email";
                        return View("CommonMsg1");
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                LogError(ex.StackTrace);
                ViewBag.Msg = "Technical Error occured, Please contact admin";
                return View("Msg");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Search(MemberSearchCriteriaVM model)
        {
            MemberDA mda = new MemberDA();
            return new CustomJsonResult()
            {
                Data = mda.SearchMember(model)
            };
        }

        [RBAC(Permission = "MemberDisableMemberRentFree")]
        public ActionResult DisableMemberRentFree(int MemberRental_key)
        {
            MemberRentalDA mda = new MemberRentalDA();
            MemberRentalModel model = new MemberRentalModel();
            DateTime today = DateTime.Today;
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            ViewBag.EndDate = endOfMonth.ToShortDateString();
            model = mda.GetMemberRentalByPK(MemberRental_key);
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberDisableMemberRentFree")]
        public ActionResult DisableMemberRentFree(MemberRentalModel m, FormCollection c)
        {
            DateTime today = DateTime.Today;
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            string memberid = m.Member.MemberId;
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(memberid);
            DateTime endDate = endOfMonth;

            MemberRentalDA mrda = new MemberRentalDA();
            MemberRentalModel m1 = new MemberRentalModel();
            m1.EndDate = endDate;
            m1.MemberRental_key = m.MemberRental_key;
            m1.ModifiedByUser = User.Identity.Name;
            m1.Modified_dttm = DateTime.Now;
            mrda.Edit(m1);

            return RedirectToAction("Edit", new { member_key = member_key });
        }

        [RBAC(Permission = "MemberEditMemberRentFree")]
        public ActionResult EditMemberRentFree(int MemberRental_key)
        {
            MemberRentalDA mda = new MemberRentalDA();
            MemberRentalModel model = new MemberRentalModel();
            model = mda.GetMemberRentalByPK(MemberRental_key);
            return View(model);
        }
        [HttpPost]
        [RBAC(Permission = "MemberEditMemberRentFree")]
        public ActionResult EditMemberRentFree(MemberRentalModel m, FormCollection c)
        {
            string memberid = m.Member.MemberId;
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(memberid);
            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(c["EndDate"].ToString(), out endDate);
            if (endDate != DateTime.MinValue)
            {
                endDate = Convert.ToDateTime(c["EndDate"].ToString());

                MemberRentalDA mrda = new MemberRentalDA();
                MemberRentalModel m1 = new MemberRentalModel();
                m1.EndDate = endDate;
                m1.MemberRental_key = m.MemberRental_key;
                m1.ModifiedByUser = User.Identity.Name;
                m1.Modified_dttm = DateTime.Now;
                mrda.Edit(m1);

                return RedirectToAction("Edit", new { member_key = member_key });
            }
            else
            {
                return View();
            }
        }

        [RBAC(Permission = "MemberAddMemberRentFree")]
        public ActionResult AddMemberRentFree(int id, int Companykey, string Msg)
        {
            MemberRentalDA mda = new MemberRentalDA();
            MemberRentalModel model = new MemberRentalModel();
            Session["Memberkey"] = id;
            Session["Companykey"] = Companykey;
            model = mda.GetMemberRental(id);

            MemberTerminalRentalDA mtrda = new MemberTerminalRentalDA();
            model.MemberCurrentTerminalRentalList = mtrda.GetKeyByMemberKey(id);

            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');

            model.RentalMemberRentalTemplateKey = 7;
            TerminalRentalRateDA trda = new TerminalRentalRateDA();
            model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).ToList();
            Session["RateListToSave"] = model.TerminalRentalRateList;
            DrawDropDownConfig(Companykey);
            ViewBag.Msg = Msg;
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberRentFree")]
        public ActionResult AddMemberRentFree(MemberRentalModel m, FormCollection c)
        {

            int member_key = Convert.ToInt32(Session["Memberkey"]);// mda.GetMemberKey(m.Member.MemberId);

            int Companykey = Convert.ToInt32(Session["Companykey"]);
            DateTime StartDate = DateTime.MinValue;
            DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
            if (StartDate != DateTime.MinValue)
            {
                StartDate = Convert.ToDateTime(c["StartDate"].ToString());
            }

            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(c["EndDate"].ToString(), out endDate);
            if (endDate != DateTime.MinValue)
            {
                endDate = Convert.ToDateTime(c["EndDate"].ToString());
            }

            MemberRentalDA mrda = new MemberRentalDA();
            MemberRentalModel m1 = new MemberRentalModel();
            m1.Member_key = member_key;
            m1.RentFree = true;
            if (StartDate != DateTime.MinValue)
            {
                m1.StartDate = StartDate;
            }
            if (endDate != DateTime.MinValue)
            {
                m1.EndDate = endDate;
            }
            m1.ModifiedByUser = User.Identity.Name;
            m1.Modified_dttm = DateTime.Now;
            m1.RentalAmount = 0;
            mrda.AddNew(m1);

            // string Msg = "Rent Free Saved for the specified dates";
            // return RedirectToAction("AddMemberRentFree", new { member_key, Companykey, Msg });
            return RedirectToAction("Edit", new { member_key });
        }

        [RBAC(Permission = "MemberAddMemberRent")]
        public ActionResult AddMemberRental(int id, int Companykey, bool IsFeeFree)
        {
            MemberRentalDA mda = new MemberRentalDA();
            MemberRentalModel model = new MemberRentalModel();
            Session["Memberkey"] = id;
            Session["Companykey"] = Companykey;
            model = mda.GetMemberRental(id);

            MemberTerminalRentalDA mtrda = new MemberTerminalRentalDA();
            model.MemberCurrentTerminalRentalList = mtrda.GetKeyByMemberKey(id);

            //string dobDate = DateTime.Now.ToShortDateString();
            string dobDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            //model.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');

            DrawDropDownConfig(Companykey, isFeeFree: IsFeeFree);
            ViewBag.IsFeeFreeMember = IsFeeFree;

            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            var MemberRentalTemplateList = cda.GetByCompanyKey(Companykey).ToList();
            if (MemberRentalTemplateList.Count != 0)
            {
                model.RentalMemberRentalTemplateKey = MemberRentalTemplateList.FirstOrDefault().MemberRentalTemplateKey;

                if (Companykey == 18)
                {
                    model.RentalMemberRentalTemplateKey = 7;
                    
                    if (IsFeeFree)
                    {
                        int.TryParse(Caching.GetSystemConfigurationByKey("FeefreeRentalTemplateKey"), out int FeeFreeRenTalTemplateKey);

                        model.RentalMemberRentalTemplateKey = FeeFreeRenTalTemplateKey;
                        model.RentalStartDate = DateTime.Now;
                    }
                }
                
                TerminalRentalRateDA trda = new TerminalRentalRateDA();
                model.TerminalRentalRateList = trda.GetByMemberRentalTemplateKey(model.RentalMemberRentalTemplateKey.Value).OrderBy(r => r.RentAmount).ToList();
                Session["RateListToSave"] = model.TerminalRentalRateList;
            }
            else
            {
                ViewBag.Error = "No template found";
            }
            
            

            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberRent")]
        public ActionResult AddMemberRental(MemberRentalModel m, FormCollection c)
        {
           var isFeeFree = c["IsFeeFreeMember"] == null ? false : Convert.ToBoolean(c["IsFeeFreeMember"]);
           ViewBag.IsFeeFreeMember = isFeeFree;
            
            if (m.RentalMemberRentalTemplateKey != null)
            {
                int member_key = Convert.ToInt32(Session["Memberkey"]);// mda.GetMemberKey(m.Member.MemberId);
                int Companykey = Convert.ToInt32(Session["Companykey"]);
                DrawDropDownConfig(Companykey, isFeeFree: isFeeFree);
                m.TerminalRentalRateList = new TerminalRentalRateDA().GetByMemberRentalTemplateKey(m.RentalMemberRentalTemplateKey.Value)
                                                                        .OrderBy(r => r.RentAmount).ToList();
                
                var feeFreeRentalTemplateKey = Caching.GetSystemConfigurationByKey("FeefreeRentalTemplateKey");
                if (feeFreeRentalTemplateKey == m.RentalMemberRentalTemplateKey.ToString())
                {
                    bool isFeeFreeMember = new MemberDA().GetMemeber(member_key).IsFeeFreeMember;
                    if (!isFeeFreeMember)
                    {
                        ViewBag.Error = "Please save the member before updating the rental template.";
                        return View(m);
                    }
                }
                
                DateTime StartDate = DateTime.MinValue;
                DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
                if (StartDate != DateTime.MinValue)
                {
                    StartDate = Convert.ToDateTime(c["StartDate"].ToString());
                    string dobDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day).ToShortDateString();
                    m.RentalStartDate = StartDate;
                    string[] dobDateArry = dobDate.Split('/');

                    //m.RentalStartDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
                }

                DateTime endDate = DateTime.MinValue;
                DateTime.TryParse(c["EndDate"].ToString(), out endDate);
                if (endDate != DateTime.MinValue)
                {
                    endDate = Convert.ToDateTime(c["EndDate"].ToString());
                    m.RentalEndDate = endDate;

                    DateTime StartCom = StartDate.AddMonths(1);

                    if (StartCom > endDate)
                    {
                        ViewBag.Error = "End date should be more than a month from start date";
                        return View(m);
                    }
                }
                if (m.RentalRentFree != true)
                {
                    int MemberRentalTemplateKey = m.RentalMemberRentalTemplateKey.Value;
                    List<TerminalRentalRateModel> TerminalRentalRateList = (List<TerminalRentalRateModel>)Session["RateListToSave"];
                    if (TerminalRentalRateList == null)
                    {
                        ViewBag.Error = "Error adding new TerminalRentalRateList, please contact to Administrator.";
                        return View(m);
                    }
                    if (TerminalRentalRateList != null)
                    {
                        //checking if new value is added for the rental template
                        foreach (var r in TerminalRentalRateList)
                        {
                            string key = r.TerminalRentalRate_key.ToString();

                            if (c[key] != null)
                            {
                                string amt = c[key].ToString();
                                if (amt != string.Empty)
                                {
                                    decimal rAmt = Convert.ToDecimal(amt);
                                    if (rAmt > 0)
                                    {
                                        r.RentAmount = rAmt;
                                    }
                                }
                            }
                        }
                    }
                    MemberTerminalRentalDA mtra = new MemberTerminalRentalDA();
                    mtra.Add(TerminalRentalRateList, member_key, StartDate, endDate, User.Identity.Name);
                    m.RentalMemberRentalTemplateKey = MemberRentalTemplateKey;

                }
                else
                {
                    MemberRentalDA mrda = new MemberRentalDA();
                    MemberRentalModel m1 = new MemberRentalModel();
                    m1.Member_key = member_key;
                    m1.RentFree = true;
                    if (StartDate != DateTime.MinValue)
                    {
                        m1.StartDate = StartDate;
                    }
                    if (endDate != DateTime.MinValue)
                    {
                        m1.EndDate = endDate;
                    }
                    m1.ModifiedByUser = User.Identity.Name;
                    m1.Modified_dttm = DateTime.Now;
                    m1.RentalAmount = 0;
                    m1.RentFree = true;
                    //m1.RentalTurnoverRangeKey = t.RentalTurnoverRangeKey;
                    mrda.AddNew(m1);
                }
                //return RedirectToAction("Edit", new { member_key });
                //ViewBag.Msg = "Template saved for the member";
                //return View(m);
                ViewBag.BtnName = "Ok";
                ViewBag.ContAction = "Edit";
                ViewBag.PraValus = member_key;
                ViewBag.Msg = "Template saved for this member";
                return View("CommonMsg1");

            }
            else
            {
                ViewBag.Error = "No template found, cannot save";
                return View(m);
            }
        }

        public ActionResult AddAllocateMemberVirtualTerminal(int id, int Company, string State)
        {
            TerminalAllocationModel model = new TerminalAllocationModel();

            MemberDA memberDA = new MemberDA();
            model.Member = memberDA.GetBasicMemberInfo(id);
            model.Member_key = id;
            StateDA sda = new StateDA();
            var states = sda.GetStateAustralia().ToList();
            ViewBag.StateList = states.Select(item => new SelectListItem
            {
                Text = item.StateName,
                Value = item.StateKey.ToString()
            }).ToList();
            model.State_key = states.First().StateKey;

            TerminalAllocationDA mda = new TerminalAllocationDA();

            Session["Memberkey"] = id;
            //model = mda.GetTerminalAllocation(id);
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.EffectiveDateString = DateTime.Now.ToString("s");
            model.VehicleModel = new VehicleModel();

            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get(Company).ToList();
            ViewBag.VehicleTypeList = VehicleType.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.VehicleType_key.ToString()
            }).ToList();

            model.VehicleModel.VehicleType_key =
                VehicleType.FirstOrDefault(n => n.Name == "Live local")?.VehicleType_key;
            model.VehicleModel.VehicleType.Company_Key = Company;
            var terminalTypeDA = new TerminalTypeDA();
            var lst = terminalTypeDA.GetVirtualTerminalTypeList();
            model.VirtualTerminalTypes = lst.Select(n => new SelectListItem()
            {
                Text = n.Name,
                Value = n.TerminalType_key.ToString()
            }).ToList();
            model.TerminalType_Key = lst.First().TerminalType_key;
            return View(model);
        }

        private void ResetModel(VehicleTypeDA vtda, TerminalAllocationModel m)
        {
            var VehicleType = vtda.Get(m.VehicleModel.VehicleType.Company_Key).ToList();
            ViewBag.VehicleTypeList = VehicleType.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.VehicleType_key.ToString()
            }).ToList();

            StateDA sda = new StateDA();
            var states = sda.GetStateAustralia().ToList();
            ViewBag.StateList = states.Select(item => new SelectListItem
            {
                Text = item.StateName,
                Value = item.StateKey.ToString()
            }).ToList();

            var terminalTypeDA = new TerminalTypeDA();
            var lst = terminalTypeDA.GetVirtualTerminalTypeList();
            m.VirtualTerminalTypes = lst.Select(n => new SelectListItem()
            {
                Text = n.Name,
                Value = n.TerminalType_key.ToString()
            }).ToList();
        }

        [HttpPost]
        public ActionResult AddAllocateMemberVirtualTerminal(TerminalAllocationModel m, FormCollection c)
        {
            MemberDA mda = new MemberDA();
            ViewBag.Error = "";
            var member_key = m.Member_key.Value;
            m.Member = mda.GetBasicMemberInfo(member_key);
            try
            {
                EftTerminalLocationDA efda = new EftTerminalLocationDA();
                VehicleTypeDA vtda = new VehicleTypeDA();

                int maximum = vtda.GetMaximumAllocationByKey(m.VehicleModel.VehicleType_key.Value);
                string name = vtda.GetNameByKey(m.VehicleModel.VehicleType_key.Value);
                var vehicleTypeKey = m.VehicleModel.VehicleType_key.Value;
                var totalAllocation = efda.GetTotalTerminalAllocationByVehicleTypeKey(m.VehicleModel.VehicleType_key.Value, member_key);
                if (maximum != -1 && totalAllocation >= maximum)
                {
                    ResetModel(vtda, m);
                    throw new Exception($"Maximum number of terminals for {name} has been exceeded.");
                }

                var autoCreatePortalAccount =
                    vtda.GetAllowAutoCreatePortalAccountByKey(vehicleTypeKey);
                var username = m.Member.Email.ToLower();
                MyWebDA mwda = new MyWebDA();
                var myWebUserProfile = mwda.GetLoginProfile(member_key);
                if (autoCreatePortalAccount)
                {
                    if (myWebUserProfile.UserId != 0)
                    {
                        username = myWebUserProfile.UserName;

                    }
                    else
                    {
                        bool usernameexists = mwda.DoesUserNameExists(username);
                        if (usernameexists)
                        {
                            ResetModel(vtda, m);
                            throw new Exception("This Member’s Contact Email is already used as another Member’s Username.");
                        }
                    }
                }

                if (autoCreatePortalAccount)
                {
                    //Send email to merchant in case they already have the myweb account (Fuse)
                    if (myWebUserProfile.UserId != 0)
                    {
                        IEmail emailProvider = LGEmail.GetEmailProvider(EmailType.SparkPost);
                        Dictionary<string, object> customMetaData = new Dictionary<string, object>();
                        customMetaData["FirstName"] = m.Member.FirstName;
                        customMetaData["Username"] = username;
                        customMetaData["EmailAddress"] = m.Member.Email;
                        dynamic sendEmailConfig = new ExpandoObject();
                        sendEmailConfig.CustomMetaData = customMetaData;
                        sendEmailConfig.SparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
                        sendEmailConfig.TemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAccountDetailTemplate.ToString());
                        sendEmailConfig.Recipient = m.Member.Email;
                        emailProvider.SendEmail(sendEmailConfig);
                    }
                    else
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        Auth0ManagementAPI auth0 = new Auth0ManagementAPI(
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientID.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientSecretKey.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationAudience.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationBaseURL.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseCreatePortalAccountURL.ToString()));

                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Content-Type", "application/json");
                        var jsonboject = new
                        {
                            memberKey = m.Member_key,
                            userName = username
                        };
                        request.AddParameter("application/json", JsonConvert.SerializeObject(jsonboject), ParameterType.RequestBody);
                        try
                        {
                            var data = auth0.Execute<FuseCreateAccountModel>(request);
                            if (data.error)
                            {
                                ResetModel(vtda, m);
                                throw new Exception(data.message);
                            }
                        }
                        catch (Exception e)
                        {
                            ResetModel(vtda, m);
                            throw new Exception(ErrorMessageConstant.SystemError);
                        }
                    }
                }

                var terminalID = string.Empty;
                var tak = 0;
                using (var taxiEpay = new TaxiEpayEntities())
                {
                    terminalID = GetNextVirtualTerminalID(taxiEpay, m.TerminalType_Key);
                    //Create new virtual terminal
                    EftTerminal terminal = new EftTerminal();
                    terminal.TerminalId = terminalID;
                    terminal.TerminalType = m.TerminalType_Key;
                    terminal.Company_key = m.VehicleModel.VehicleType.Company_Key;
                    terminal.Status = "LIVE";
                    terminal.Active = true;
                    terminal.SoftwareVersion = "ONLINE";
                    taxiEpay.EftTerminals.Add(terminal);

                    //create new Vehile/Business
                    StateDA sda = new StateDA();
                    Vehicle newVeh = new Vehicle();
                    newVeh.VehicleId = "Online Virtual TID";
                    newVeh.RegisteredState = sda.Get(m.State_key.Value).StateName;
                    newVeh.VehicleType_key = m.VehicleModel.VehicleType_key;
                    newVeh.CreatedDate = DateTime.Now;
                    taxiEpay.Vehicles.Add(newVeh);

                    //Allocate terminal
                    TerminalAllocation termallocation = new TerminalAllocation();
                    termallocation.EftTerminal = terminal;
                    termallocation.Vehicle = newVeh;
                    termallocation.EffectiveDate = DateTime.Now;
                    termallocation.IsCurrent = true;
                    termallocation.AllowRefund = m.AllowRefund;
                    termallocation.Member_key = member_key;

                    //update Vehicle
                    newVeh.EftTerminal = terminal;
                    newVeh.Operator_key = member_key;
                    newVeh.TerminalAllocatedDate = DateTime.Now;
                    taxiEpay.TerminalAllocations.Add(termallocation);

                    taxiEpay.SaveChanges();
                    tak = termallocation.TerminalAllocation_key;
                }

                using (var terallocation = new TaxiEpayEntities())
                {
                    var ter = terallocation.TerminalAllocations.FirstOrDefault(n => n.TerminalAllocation_key == tak);
                    ter.OriginalTerminalAllocation_key = tak;
                    terallocation.SaveChanges();
                }

                mda.UpdateModifiedDate(member_key, User.Identity.Name);

                ViewBag.BtnName = "Ok";
                ViewBag.ContAction = "Edit";
                ViewBag.PraValus = member_key;
                ViewBag.Msg = $"Terminal ID {terminalID} has been allocated to the merchant";
                return View("CommonMsg1");

            }
            catch (Exception ex)
            {
                LogError(ex);
                VehicleTypeDA vtda = new VehicleTypeDA();
                ResetModel(vtda, m);
                ViewBag.Error = ex.Message;
                return View(m);
            }
        }

        public string GetNextVirtualTerminalID(TaxiEpayEntities entity, int terminalTypeKey)
        {
            string TID = "";
            var terminalType = entity.TerminalTypes.FirstOrDefault(n => n.TerminalType_key == terminalTypeKey);
            var nextnumber = terminalType.NextSequence;

            try
            {
                TID = terminalType.prefix.Trim() + nextnumber.ToString().PadLeft(8 - terminalType.prefix.Trim().Length, '0');
                while (entity.EftTerminals.Any(r => r.TerminalId == TID))
                {
                    nextnumber++;
                    TID = terminalType.prefix.Trim() + nextnumber.ToString().PadLeft(8 - terminalType.prefix.Trim().Length, '0');

                }

                terminalType.NextSequence = nextnumber + 1;
            }
            catch (Exception ex)
            {
                LogError(ex);
                return "";
            }
            return TID;
        }


        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult AddAllocateMemberTerminal(int id, int Company, string State, int? subBusinessTypeKey)
        {
            var subBusinessTypeKeyVal = subBusinessTypeKey.GetValueOrDefault(0);
            DrawDropDownConfigAllocateTerminal(Company, subBusinessTypeKeyVal);
            var companyKeysAllowOnlyOneSubBusToAllTerminals =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                    .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

            TerminalAllocationModel model = new TerminalAllocationModel();
            TerminalAllocationDA mda = new TerminalAllocationDA();
            model = mda.GetTerminalAllocation(id);
            if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(Company.ToString()))
            {
                if (subBusinessTypeKeyVal == 0)
                {
                    ViewBag.Err = "Please select Sub-business Type";
                    return View("CommonError");
                }
                model.NotAllowSelectSubBusinessType = true;
            }

            Session["Memberkey"] = id;
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.Employee_key = 0;
            model.EffectiveDateString = DateTime.Now.ToString("s");
            model.VehicleModel = new VehicleModel();
            StateDA sda = new StateDA();
            model.State_key = sda.GetStateKey(State);

            if (Company == 1)
            {
                model.VehicleModel.VehicleType_key = 1;
            }
            if (Company == 18)
            {
                model.VehicleModel.VehicleType_key = 19;
            }
            model.VehicleModel.VehicleType.Company_Key = Company;
            model.VehicleModel.VehicleType_key = subBusinessTypeKeyVal == 0 ? model.VehicleModel.VehicleType_key : subBusinessTypeKeyVal;

            return View(model);
        }
        [HttpPost]
        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult AddAllocateMemberTerminal(TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            MemberDA mda = new MemberDA();
            ViewBag.Error = "";

            DrawDropDownConfigAllocateTerminal(m.VehicleModel.VehicleType.Company_Key, m.VehicleModel.VehicleType_key.Value);
            int member_key = mda.GetMemberKey(m.Member.MemberId);
            int VehicleTypekey = m.VehicleModel.VehicleType_key.Value;
            int Employee_key = m.Employee_key.Value;

            m.Member = mda.GetMemeber(member_key);
            string Term_key = c["EftTerminal_key"].ToString();

            EftTerminalLocationDA efda = new EftTerminalLocationDA();
            VehicleTypeDA vtda = new VehicleTypeDA();

            int maximum = vtda.GetMaximumAllocationByKey(VehicleTypekey);
            string name = vtda.GetNameByKey(VehicleTypekey);
            var dtm11 = c["TerminalAllocationDate"];
            var dtm21 = c["TerminalAllocationTime"];
            var totalAllocation = efda.GetTotalTerminalAllocationByVehicleTypeKey(m.VehicleModel.VehicleType_key.Value, member_key);
            if (totalAllocation >= maximum && maximum != -1)
            {
                ViewBag.Error = $"Maximum number of terminals for {name} has been exceeded.";
                return View(m);
            }

            TerminalModel term = tda.CheckTerminalExists(Term_key);
            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                return View(m);
            }

            bool IsAllocated = tda.IsTerminalAllocated(Term_key);

            if (IsAllocated)
            {
                ViewBag.Error = "looks like terminal is allocated to another member";
                return View(m);
            }

            if (Convert.ToDateTime(dtm11) > DateTime.Now)
            {
                ViewBag.Error = "Allocation date cannot be future date.";
                return View(m);
            }

            var lstSubBusinessTypeForVICStateMerchantOnly = Caching.GetSystemConfigurationByKey("SubBusinessTypeForVICStateMerchantOnly").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
            var memberAddress = new MemberAddressDA().GetMemberAddress(member_key).FirstOrDefault();
            if (lstSubBusinessTypeForVICStateMerchantOnly.Contains(VehicleTypekey.ToString()) //Live taxi black VIC
                && memberAddress?.State != "VIC") //VIC state
            {
                ViewBag.Error = "Incorrect sub-business type selected. Live taxi Black VIC can only be used for Victorian members";
                return View(m);
            }

            try
            {
                string[] aa = dtm11.Split('-');
                string[] ab = dtm21.Split(':');

                int y1 = Convert.ToInt32(aa[0]);
                int m12 = Convert.ToInt32(aa[1]);
                int d1 = Convert.ToInt32(aa[2]);

                int h1 = Convert.ToInt32(ab[0]);
                int min1 = Convert.ToInt32(ab[1]);

                DateTime dtm3 = new DateTime(y1, m12, d1, h1, min1, 00);

                TerminalAllocationModel account = new TerminalAllocationModel();
                account.VehicleModel = new VehicleModel();
                account.VehicleModel.VehicleId = c["VehicleId"].ToString();// m.VehicleModel.VehicleId;
                account.EftTerminal_key = term.EftTerminal_key;
                account.EffectiveDate = dtm3;// Convert.ToDateTime(c["TerminalAllocationDate"].ToString());
                account.IsCurrent = true;
                account.ModifiedByUser = User.Identity.Name;
                account.DeliveryRequired = m.DeliveryRequiredB;
                account.ServiceFee = m.ServiceFee;
                account.AllowMOTO = m.AllowMOTOB;
                account.AllowRefund = m.AllowRefundB;
                account.Employee_key = Employee_key;
                StateDA sda = new StateDA();
                StateModel smodel = new StateModel();
                smodel = sda.Get(m.State_key.Value);

                account.VehicleModel.RegisteredState = smodel.StateName;
                account.EftTerminalId = term.TerminalId;

                account.VehicleModel.TerminalAllocatedDate = dtm3;


                account.VehicleModel.VehicleType_key = m.VehicleModel.VehicleType_key;
                account.VehicleModel.CreatedDate = DateTime.Now;
                account.VehicleModel.VehicleType.Company_Key = m.VehicleModel.VehicleType.Company_Key;

                List<TerminalAllocationModel> TAListInternal = new List<TerminalAllocationModel>();
                TAListInternal.Add(account);

                if (TAListInternal != null)
                {
                    mda.UpdateModifiedDate(member_key, User.Identity.Name);
                    foreach (var t in TAListInternal)
                    {
                        VehicleDA vda = new VehicleDA();
                        VehicleModel v1 = new VehicleModel();
                        v1.VehicleId = t.VehicleModel.VehicleId;
                        v1.RegisteredState = t.VehicleModel.RegisteredState;
                        v1.TerminalAllocatedDate = t.VehicleModel.TerminalAllocatedDate;
                        v1.Operator_key = member_key;
                        v1.VehicleType_key = t.VehicleModel.VehicleType_key;
                        v1.CreatedDate = DateTime.Now;
                        v1.Modified_dttm = DateTime.Now;
                        v1.ModifiedByUser = User.Identity.Name;
                        v1.EftTerminal_key = t.EftTerminal_key;
                        int veh_Key = vda.AddNew(v1);

                        TerminalAllocationDA mda1 = new TerminalAllocationDA();
                        TerminalAllocationModel m1 = new TerminalAllocationModel();
                        m1.EftTerminal_key = t.EftTerminal_key;
                        m1.Member_key = member_key;
                        m1.EffectiveDate = t.VehicleModel.TerminalAllocatedDate.Value;
                        m1.IsCurrent = true;
                        m1.ModifiedByUser = User.Identity.Name;
                        m1.DeliveryRequired = t.DeliveryRequired;
                        m1.ServiceFee = t.ServiceFee;
                        m1.AllowMOTO = t.AllowMOTO;
                        m1.AllowRefund = t.AllowRefund;
                        m1.Vehicle_key = veh_Key;
                        int termAlloc_Key = mda1.AddNew(m1);

                        //auto disable glidbox when allocate terminal to Live eftpos group
                        mda1.DisableGlideboxFollowBusinessType(m1.EftTerminal_key ?? 0, User.Identity.Name, SourceContants.AllocateTerminal);

                        //change the status
                        TerminalDA tda1 = new TerminalDA();
                        tda1.UpdateTerminalStatus(t.EftTerminal_key.Value, TEnum.TerminalStatus.LIVE.ToString(), "CUSTOMER");
                        string terminal = tda1.GetTerminalId(t.EftTerminal_key.Value);

                        //Update Terminal information on TransactionHost
                        TerminalHostDA thda = new TerminalHostDA();
                        thda.UpdateTerminal(t.EftTerminalId?.ToString(), t.AllowMOTO, t.AllowRefund);

                        //allocate unlocated transaction
                        EFTDocketDA eda = new EFTDocketDA();
                        List<EFTDocketModel> unallocatedList = eda.GetUnallocatedTransactionFromSpecificDate(terminal, t.VehicleModel.TerminalAllocatedDate.Value);
                        if (unallocatedList != null)
                        {
                            foreach (var u in unallocatedList)
                            {
                                eda.AllocateTransactionsToMember(u.EftDocket_key, t.EftTerminal_key.Value, member_key, veh_Key);
                            }
                        }

                        if (t.Employee_key != 0)
                        {
                            TerminalSalesDA sda1 = new TerminalSalesDA();
                            TerminalSalesModel s1 = new TerminalSalesModel();
                            s1.TerminalAllocation_key = termAlloc_Key;
                            s1.Employee_key = t.Employee_key.Value;// member_key;
                            s1.Active = true;
                            s1.SaleRelationship_key = 1;
                            s1.ModifiedByUser = User.Identity.Name;
                            s1.Modified_dttm = DateTime.Now;

                            sda1.AddNew(s1);
                        }
                    }
                }

                TerminalAllocationDA mda3 = new TerminalAllocationDA();
                TerminalAllocationModel model = new TerminalAllocationModel();
                model = mda3.GetTerminalAllocation(member_key);
                model.TerminalAllocationList = TAListInternal;
                model.AllowMOTOB = false;
                model.AllowRefundB = false;
                model.DeliveryRequiredB = false;
                model.Employee_key = 0;
                model.EffectiveDateString = DateTime.Now.ToString("s");
                model.VehicleModel = new VehicleModel();
                model.VehicleModel.VehicleType.Company_Key = m.VehicleModel.VehicleType.Company_Key;
                model.VehicleModel.VehicleType_key = m.VehicleModel.VehicleType_key;
                var companyKeysAllowOnlyOneSubBusToAllTerminals =
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                        .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());
                if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(m.VehicleModel.VehicleType.Company_Key.ToString()))
                {
                    model.NotAllowSelectSubBusinessType = true;
                }

                DisableZipPaymentOfTerminals(member_key, term.TerminalId);

                ModelState.Clear();

                return View(model);

            }
            catch (Exception ex)
            {
                LogError(ex);
                ViewBag.Error = ex.Message;
                return View(m);
            }
        }

        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult CreateAllocateMemberTerminal(int? Id, int? subBusinessTypeKey)
        {
            var subBusinessTypeKeyVal = subBusinessTypeKey.GetValueOrDefault(0);
            var companyKeyVal = Id.GetValueOrDefault(0);
            TerminalAllocationModel model = new TerminalAllocationModel();
            var companyKeysAllowOnlyOneSubBusToAllTerminals =
                Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                    .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());


            if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(companyKeyVal.ToString()))
            {
                if (subBusinessTypeKeyVal == 0)
                {
                    ViewBag.Err = "Please select Sub-business Type";
                    return View("CommonError");
                }
                model.NotAllowSelectSubBusinessType = true;
            }

            DrawDropDownConfigAllocateTerminal(companyKeyVal, subBusinessTypeKeyVal);
            model.VehicleModel.VehicleType_key = subBusinessTypeKeyVal == 0 ? 19 : subBusinessTypeKeyVal;
            model.VehicleModel.VehicleType.Company_Key = companyKeyVal;

            string dobDate = DateTime.Now.ToShortDateString();
            string[] dobDateArry = dobDate.Split('/');
            model.EffectiveDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');


            if (Session["AdrList"] != null)
            {
                List<CAddress> CAddresses = (List<CAddress>)Session["AdrList"];

                var state = (from s in CAddresses
                             where s.AddressType == "Primary"
                             select s.State).FirstOrDefault();

                StateDA sda = new StateDA();
                var stateId = sda.GetStateKey(state);

                model.State_key = stateId;
            }
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.Employee_key = 0;
            model.EffectiveDateString = DateTime.Now.ToString("s");

            MemberModel SFmodel = new MemberModel();
            SFmodel = (MemberModel)Session["SalesforceMemberModel"];

            if (SFmodel != null)
            {
                if (SFmodel.Employee_key != null)
                {
                    model.Employee_key = SFmodel.Employee_key;
                }
            }

            return View(model);
        }


        public ActionResult TransferHistory(int? Transaction_key, int? Member_key)
        {
            var transferHistoryModel = new TransferHistoryVM();
            var selectedPayment = new DriverCardDA().LoadTransactionHistory(Transaction_key.GetValueOrDefault(0)).FirstOrDefault();
            transferHistoryModel.MemberID = selectedPayment.MemberId;
            transferHistoryModel.LodgementRefNo = selectedPayment.LodgementReferenceNo;
            transferHistoryModel.TotalAmount = selectedPayment.TotalAmount;
            var data = new DriverCardDA().LoadPendingPayment(Member_key.GetValueOrDefault(0));

            if (data == null || !data.Any())
            {
                View("DriverCard/TransferHistory", transferHistoryModel);
            }
            var pendingData = data.Where(n => n.Transaction_key == Transaction_key.GetValueOrDefault(0)).FirstOrDefault();
            if (pendingData != null)
            {
                transferHistoryModel.PendingAmountForSelectedPayment = pendingData.RemainingAmount.GetValueOrDefault(0);
            }
            else
            {
                transferHistoryModel.PendingAmountForSelectedPayment = 0;
            }
            transferHistoryModel.PendingBalanceForMID = data.Sum(n => n.RemainingAmount).GetValueOrDefault(0);

            var history = new DriverCardDA().LoadTransactionHistory(Transaction_key.GetValueOrDefault(0)).Where(n => n.OxygenTransactionID != null).Select(n => new TransferHistoryDataVM
            {
                PaymentStatus = n.PaymentStatus,
                SentAmount = n.SentAmount,
                TransactionDate = n.TransactionDate.ToString("dd/MM/yyyy")
            }).ToList();

            transferHistoryModel.TransferHistories = history;

            return View("DriverCard/TransferHistory", transferHistoryModel);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult CreateAllocateMemberTerminal(TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            string listAddedTerminalJson = c["ListAddedTerminalJson"];
            string reloadThis = c["ReloadThis"];
            List<TerminalAllocationModel> listAddedTerminalAllocation = new List<TerminalAllocationModel>();

            if (listAddedTerminalJson != null && listAddedTerminalJson != "")
            {
                listAddedTerminalAllocation = JsonConvert.DeserializeObject<List<TerminalAllocationModel>>(listAddedTerminalJson);
            }

            ViewBag.ListAddedTerminalJson = JsonConvert.SerializeObject(listAddedTerminalAllocation);
            m.TerminalAllocationList = listAddedTerminalAllocation;
            DrawDropDownConfigAllocateTerminal(m.VehicleModel.VehicleType.Company_Key);
            int VehicleTypekey = m.VehicleModel.VehicleType_key.Value;// Convert.ToInt32(c["VehicleType"].ToString());
            if (reloadThis == "true")
            {
                ModelState.Clear();

                ViewBag.ListAddedTerminalJson = JsonConvert.SerializeObject(listAddedTerminalAllocation);
                var companyKeysAllowOnlyOneSubBusToAllTerminals =
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                        .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());
                DrawDropDownConfigAllocateTerminal(m.VehicleModel.VehicleType.Company_Key, VehicleTypekey);
                TerminalAllocationModel model = new TerminalAllocationModel();
                if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(m.VehicleModel.VehicleType.Company_Key.ToString()))
                {
                    if (VehicleTypekey == 0)
                    {
                        ViewBag.Err = "Please select Sub-business Type";
                        return View("CommonError");
                    }
                    model.NotAllowSelectSubBusinessType = true;
                }
                model.VehicleModel.VehicleType_key = VehicleTypekey == 0 ? 19 : VehicleTypekey;
                model.VehicleModel.VehicleType.Company_Key = m.VehicleModel.VehicleType.Company_Key;
                model.TerminalAllocationList = listAddedTerminalAllocation;
                model.AllowMOTOB = false;
                model.AllowRefundB = false;
                model.DeliveryRequiredB = false;
                model.Employee_key = 0;
                model.EffectiveDateString = DateTime.Now.ToString("s");

                return View(model);
            }

            ViewBag.Error = string.Empty;
            int empkey = 0;
            string Term_key = c["EftTerminal_key"].ToString();
            int Employee_key = m.Employee_key.Value;
            TerminalModel term = tda.CheckTerminalExists(Term_key);

            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                return View(m);
            }

            bool IsTerminalAllocated = false;

            if (listAddedTerminalAllocation.Where(t => t.EftTerminalId.ToString() == Term_key).FirstOrDefault() != null)
            {
                IsTerminalAllocated = true;
            }
            else
            {
                IsTerminalAllocated = tda.IsTerminalAllocated(Term_key);
            }

            if (IsTerminalAllocated)
            {
                ViewBag.Error = "Terminal already allocated";
                return View(m);
            }

            int state = m.State_key.Value;// c["State"].ToString();

            if (state == 0)
            {
                ViewBag.Error = "Select State";
                return View(m);
            }


            if (VehicleTypekey == 0)
            {
                ViewBag.Error = "Select Vehicle Type";
                return View(m);
            }

            if (Convert.ToDateTime(c["TerminalAllocationDate"]) > DateTime.Now)
            {
                ViewBag.Error = "Allocation date issue.";
                return View(m);
            }

            try
            {
                var dtm11 = c["TerminalAllocationDate"];
                var dtm21 = c["TerminalAllocationTime"];

                string[] aa = dtm11.Split('-');
                string[] ab = dtm21.Split(':');

                int y1 = Convert.ToInt32(aa[0]);
                int m1 = Convert.ToInt32(aa[1]);
                int d1 = Convert.ToInt32(aa[2]);
                int h1 = Convert.ToInt32(ab[0]);
                int min1 = Convert.ToInt32(ab[1]);

                DateTime dtm3 = new DateTime(y1, m1, d1, h1, min1, 00);

                TerminalAllocationModel account = new TerminalAllocationModel();
                account.VehicleModel = new VehicleModel();
                account.VehicleModel.VehicleId = c["VehicleId"].ToString();// m.VehicleModel.VehicleId;
                account.EftTerminal_key = term.EftTerminal_key;
                account.Member_key = empkey;
                account.EffectiveDate = dtm3;
                account.IsCurrent = true;
                account.ModifiedByUser = User.Identity.Name;
                account.DeliveryRequired = m.DeliveryRequiredB;
                account.ServiceFee = m.ServiceFee;
                account.AllowMOTO = m.AllowMOTOB;
                account.AllowRefund = m.AllowRefundB;
                account.Employee_key = m.Employee_key;

                StateDA sda = new StateDA();
                StateModel smodel = new StateModel();
                smodel = sda.Get(m.State_key.Value);

                account.VehicleModel.RegisteredState = smodel.StateName;
                account.EftTerminalId = term.TerminalId;

                account.VehicleModel.TerminalAllocatedDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());

                account.VehicleModel.VehicleType_key = m.VehicleModel.VehicleType_key;// Convert.ToInt32(c["VehicleType"].ToString());
                account.VehicleModel.CreatedDate = DateTime.Now;

                account.VehicleModel.VehicleType.Company_Key = m.VehicleModel.VehicleType.Company_Key;
                listAddedTerminalAllocation.Add(account);

                ModelState.Clear();

                ViewBag.ListAddedTerminalJson = JsonConvert.SerializeObject(listAddedTerminalAllocation);

                TerminalAllocationModel model = new TerminalAllocationModel();
                var companyKeysAllowOnlyOneSubBusToAllTerminals =
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum
                        .CompanyKeysAllowOnlyOneSubBusToAllTerminals.ToString());

                if (companyKeysAllowOnlyOneSubBusToAllTerminals.Split(';').Contains(m.VehicleModel.VehicleType.Company_Key.ToString()))
                {
                    if (VehicleTypekey == 0)
                    {
                        ViewBag.Err = "Please select Sub-business Type";
                        return View("CommonError");
                    }
                    model.NotAllowSelectSubBusinessType = true;
                }

                DrawDropDownConfigAllocateTerminal(m.VehicleModel.VehicleType.Company_Key, VehicleTypekey);
                model.VehicleModel.VehicleType_key = VehicleTypekey == 0 ? 19 : VehicleTypekey;
                model.TerminalAllocationList = listAddedTerminalAllocation;
                model.AllowMOTOB = false;
                model.AllowRefundB = false;
                model.DeliveryRequiredB = false;
                model.Employee_key = 0;
                model.EffectiveDateString = DateTime.Now.ToString("s");

                return View(model);
            }
            catch (Exception ex)
            {
                LogError(ex);
                ViewBag.Error = ex.Message;
                m.TerminalAllocationList = listAddedTerminalAllocation;
                return View(m);
            }
        }


        [RBAC(Permission = "MemberAddMemberRent")]
        public ActionResult CreateMemberRental()
        {
            MemberRentalModel model = new MemberRentalModel();
            model.MemberRentalModelList = (List<MemberRentalModel>)Session["MemberRentalModelList"];
            MemberRentalModel SFmodel = new MemberRentalModel();
            SFmodel = (MemberRentalModel)Session["SalesforceRentalModel"];
            if (SFmodel != null)
            {
                model.RentalAmount = SFmodel.RentalAmount;
                model.RentalTurnoverRangeKey = SFmodel.RentalTurnoverRangeKey;
            }

            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberRent")]
        public ActionResult CreateMemberRental(MemberRentalModel m, FormCollection c)
        {

            MemberRentalModel account = new MemberRentalModel();

            account.RentFree = m.RentFree;
            DateTime StartDate = DateTime.MinValue;
            DateTime.TryParse(c["StartDate"].ToString(), out StartDate);
            if (StartDate != DateTime.MinValue)
            {
                account.StartDate = Convert.ToDateTime(c["StartDate"].ToString());
            }

            DateTime endDate = DateTime.MinValue;
            DateTime.TryParse(c["EndDate"].ToString(), out endDate);
            if (endDate != DateTime.MinValue)
            {
                account.EndDate = Convert.ToDateTime(c["EndDate"].ToString());
            }
            account.ModifiedByUser = User.Identity.Name;
            account.Modified_dttm = DateTime.Now;
            account.RentalAmount = Convert.ToDecimal(c["RentalAmount"].ToString());
            account.RentalTurnoverRangeKey = Convert.ToInt32(c["RentalTurnoverRange"].ToString());
            account.RentalTurnoverRangeName = c["RentalTurnoverRange"].ToString();

            List<MemberRentalModel> TAListInternal = new List<MemberRentalModel>();
            TAListInternal = (List<MemberRentalModel>)Session["MemberRentalModelList"];

            if (TAListInternal == null)
            {
                TAListInternal = new List<MemberRentalModel>();
            }
            TAListInternal.Add(account);

            Session["MemberRentalModelList"] = TAListInternal;
            MemberRentalModel model = new MemberRentalModel();
            model.MemberRentalModelList = TAListInternal;
            return View(model);
        }

        public ActionResult GetPhoto(int memberkey)
        {
            MemberDA mda = new MemberDA();
            byte[] photo = mda.GetMemberPhoto(memberkey);
            if (photo == null)
            {
                try
                {
                    photo = System.IO.File.ReadAllBytes("PhoenixWeb/Images/LIV002_LiveGroupLogo_CMYK.png");
                }
                catch
                { }
            }
            return File(photo, "image/jpeg");
        }

        public ActionResult EnteredBankDetails()
        {
            List<BankAccountModel> BAList = new List<BankAccountModel>();
            BAList = (List<BankAccountModel>)Session["BAccountList"];
            if (BAList != null)
            {
                if (BAList.Count > 0)
                {
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("error", JsonRequestBehavior.AllowGet);

        }

        [RBAC(Permission = "MemberDeallocateTerminal")]
        public ActionResult DeallocateTerminal(int Id, string TerminalId, int Member)
        {

            ViewBag.Id = Id;
            DeallocateTerminalVM model = new DeallocateTerminalVM();
            model.EffectiveDateString = DateTime.Now.ToString("s");
            model.TerminalId = TerminalId;
            model.TerminalAllocationKey = Id;
            model.MemberKey = Member;
            DrawDropDownConfigDeallocate();
            model.Status = "RETURNED";
            return View(model);
        }
        [RBAC(Permission = "MemberDeallocateTerminal")]
        public ActionResult DeallocateMemberTerminal(FormCollection c, DeallocateTerminalVM m)
        {
            TerminalAllocationDA tda = new TerminalAllocationDA();
            //  DeallocateTerminalVM model = new DeallocateTerminalVM();
            try
            {
                ViewBag.Err = "";
                DrawDropDownConfigDeallocate();
                string stat = m.Status;
                DateTime EffectiveDate = Convert.ToDateTime(m.EffectiveDateString);
                DateTime dateOneMonth = DateTime.Now.AddMonths(-1);
                DateTime dateOneWeek = DateTime.Now.AddDays(7);

                if (EffectiveDate < dateOneMonth || EffectiveDate > dateOneWeek)
                {
                    m.EffectiveDateString = DateTime.Now.ToString("s");
                    ViewBag.Err = "Effective date cannot be one month before or one week after, please select appropriate date";
                    return View("DeallocateTerminal", m);
                }
                else
                {
                    tda.DeallocateTerminal(m.TerminalAllocationKey, User.Identity.Name, stat, EffectiveDate, m.Location);
                }
            }
            catch (Exception e)
            {
                ViewBag.Err = e.Message;

                return View("DeallocateTerminal", m);
            }
            ViewBag.BtnName = "Ok";
            ViewBag.ContAction = "Edit";
            ViewBag.PraValus = m.MemberKey;
            ViewBag.Msg = "Terminal Deallocated";
            return View("CommonMsg1");
        }

        [RBAC(Permission = "MemberAllocateTerminal")]
        public ActionResult AllocateTerminal(string Id, int Member, int Vehicle)
        {
            ViewBag.VehicleId = Id;
            ViewBag.VehicleKey = Vehicle;
            DrawDropDownConfigAllocateTerminal();
            TerminalAllocationDA mda = new TerminalAllocationDA();
            TerminalAllocationModel model = new TerminalAllocationModel();
            Session["Memberkey"] = Member;
            model = mda.GetTerminalAllocation(Member);
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.Employee_key = 0;
            model.Member_key = Member;
            model.EffectiveDateString = DateTime.Now.ToString("s");
            return View(model);
        }

        [RBAC(Permission = "MemberAllocateTerminal")]
        public ActionResult AllocateMemberTerminal(int Vehicle, TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            VehicleDA vda = new VehicleDA();
            ViewBag.Error = "";
            DrawDropDownConfigAllocateTerminal();
            string Term_key = c["EftTerminal_key"].ToString();
            TerminalModel term = tda.CheckTerminalExists(Term_key);
            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                return View("AllocateTerminal", m);
            }
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(m.Member.MemberId);

            EftTerminalLocationDA efda = new EftTerminalLocationDA();
            VehicleTypeDA vtda = new VehicleTypeDA();
            var vecicle = vda.Get(Vehicle);
            int maximum = vtda.GetMaximumAllocationByKey(vecicle.VehicleType_key.Value);
            string name = vtda.GetNameByKey(vecicle.VehicleType_key.Value);
            var totalAllocation = efda.GetTotalTerminalAllocationByVehicleTypeKey(vecicle.VehicleType_key.Value, member_key);
            if (totalAllocation >= maximum && maximum != -1)
            {
                ViewBag.Error = $"Maximum number of terminals for {name} has been exceeded.";
                return View("AllocateTerminal", m);
            }

            var mid = tda.TerminalBelongToMID(Term_key);
            if (!string.IsNullOrEmpty(mid))
            {
                ViewBag.Error = mid == m.Member.MemberId ? "The terminal has been allocated to this member" : "looks like terminal is allocated to another member";
                return View("AllocateTerminal", m);
            }

            var lstSubBusinessTypeForVICStateMerchantOnly = Caching.GetSystemConfigurationByKey("SubBusinessTypeForVICStateMerchantOnly").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
            var memberAddress = new MemberAddressDA().GetMemberAddress(member_key).FirstOrDefault();
            if (lstSubBusinessTypeForVICStateMerchantOnly.Contains(vecicle.VehicleType_key.Value.ToString()) //Live taxi black VIC
                && memberAddress?.State != "VIC") //VIC state
            {
                ViewBag.Error = "Incorrect sub-business type selected. Live taxi Black VIC can only be used for Victorian members";
                return View("AllocateTerminal", m);
            }

            int Employee_key = m.Employee_key.Value;
            try
            {
                TerminalAllocationModel account = new TerminalAllocationModel();
                account.VehicleModel = new VehicleModel();
                account.VehicleModel = vda.Get(Vehicle);
                account.EftTerminal_key = term.EftTerminal_key;
                account.EffectiveDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());
                account.IsCurrent = true;
                account.ModifiedByUser = User.Identity.Name;
                account.DeliveryRequired = m.DeliveryRequiredB;
                account.ServiceFee = m.ServiceFee;
                account.AllowMOTO = m.AllowMOTOB;
                account.AllowRefund = m.AllowRefundB;
                account.Employee_key = Employee_key;

                account.EftTerminalId = term.TerminalId;
                List<TerminalAllocationModel> TAListInternal = new List<TerminalAllocationModel>();

                TAListInternal.Add(account);

                if (TAListInternal != null)
                {
                    mda.UpdateModifiedDate(member_key, User.Identity.Name);
                    foreach (var t in TAListInternal)
                    {
                        VehicleModel v1 = new VehicleModel();
                        v1 = vda.Get(Vehicle);
                        v1.TerminalAllocatedDate = t.EffectiveDate;
                        v1.Modified_dttm = DateTime.Now;
                        v1.ModifiedByUser = User.Identity.Name;
                        v1.EftTerminal_key = t.EftTerminal_key;
                        vda.Update(v1);

                        TerminalAllocationDA mda1 = new TerminalAllocationDA();
                        TerminalAllocationModel m1 = new TerminalAllocationModel();
                        m1.EftTerminal_key = t.EftTerminal_key;
                        m1.Member_key = member_key;
                        m1.EffectiveDate = t.EffectiveDate;
                        m1.IsCurrent = true;
                        m1.ModifiedByUser = User.Identity.Name;
                        m1.DeliveryRequired = t.DeliveryRequired;
                        m1.ServiceFee = t.ServiceFee;
                        m1.AllowMOTO = t.AllowMOTO;
                        m1.AllowRefund = t.AllowRefund;
                        m1.Vehicle_key = v1.Vehicle_key;
                        int termAlloc_Key = mda1.AddNew(m1);

                        //change the status
                        TerminalDA tda1 = new TerminalDA();
                        tda1.UpdateTerminalStatus(t.EftTerminal_key.Value, TEnum.TerminalStatus.LIVE.ToString(), "CUSTOMER");
                        string terminal = tda1.GetTerminalId(t.EftTerminal_key.Value);

                        //allocate unlocated transaction
                        EFTDocketDA eda = new EFTDocketDA();
                        List<EFTDocketModel> unallocatedList = eda.GetUnallocatedTransactionFromSpecificDate(terminal, t.VehicleModel.TerminalAllocatedDate.Value);
                        if (unallocatedList != null)
                        {
                            foreach (var u in unallocatedList)
                            {
                                eda.AllocateTransactionsToMember(u.EftDocket_key, t.EftTerminal_key.Value, member_key, v1.Vehicle_key);
                            }
                        }

                        if (t.Employee_key != 0)
                        {
                            TerminalSalesDA sda1 = new TerminalSalesDA();
                            TerminalSalesModel s1 = new TerminalSalesModel();
                            s1.TerminalAllocation_key = termAlloc_Key;
                            s1.Employee_key = t.Employee_key.Value;// member_key;
                            s1.Active = true;
                            s1.SaleRelationship_key = 1;
                            s1.ModifiedByUser = User.Identity.Name;
                            s1.Modified_dttm = DateTime.Now;

                            sda1.AddNew(s1);
                        }



                        //auto disable glidbox when allocate terminal to Live eftpos group
                        new TerminalAllocationDA().DisableGlideboxFollowBusinessType(t.EftTerminal_key ?? 0, User.Identity.Name, SourceContants.AllocateTerminal);
                    }
                }

                TerminalAllocationDA mda3 = new TerminalAllocationDA();
                TerminalAllocationModel model = new TerminalAllocationModel();
                model = mda3.GetTerminalAllocation(member_key);
                model.TerminalAllocationList = TAListInternal;
                model.AllowMOTOB = false;
                model.AllowRefundB = false;
                model.DeliveryRequiredB = false;
                model.Employee_key = 0;
                model.EffectiveDateString = DateTime.Now.ToString("s");

                DisableZipPaymentOfTerminals(member_key, term.TerminalId);

                ViewBag.BtnName = "Ok";
                ViewBag.ContAction = "Edit";
                ViewBag.PraValus = m.Member_key;
                ViewBag.Msg = "Terminal Allocated";
                return View("CommonMsg1");

            }
            catch (Exception ex)
            {
                LogError(ex);
                ViewBag.Error = ex.Message;
                return View("AllocateTerminal", m);
            }
        }

        [RBAC(Permission = "MemberSwapTerminal")]
        public ActionResult SwapTerminal(string VehicleId, int Member, int Vehicle, string Terminal, int TerminalAllocation)
        {
            ViewBag.VehicleId = VehicleId;
            ViewBag.VehicleKey = Vehicle;
            DrawDropDownConfigAllocateTerminal();
            DrawDropDownConfigDeallocate();
            TerminalAllocationDA mda = new TerminalAllocationDA();
            TerminalAllocationModel model = new TerminalAllocationModel();
            //Session["Memberkey"] = Member;
            model = mda.GetTerminalAllocation(Member);
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.Employee_key = 0;
            model.Member_key = Member;
            model.OriginalTerminalID = Terminal;
            model.TerminalAllocation_key = TerminalAllocation;
            model.EffectiveDateString = DateTime.Now.ToString("s");
            model.OldTerminalStatus = "RETURNED";
            return View(model);
        }
        [RBAC(Permission = "MemberSwapTerminal")]
        public ActionResult SwapMemberTerminal(int Vehicle, TerminalAllocationModel m, FormCollection c)
        {
            TerminalDA tda = new TerminalDA();
            VehicleDA vda = new VehicleDA();
            TerminalAllocationDA tda1 = new TerminalAllocationDA();
            ViewBag.Error = "";
            DrawDropDownConfigAllocateTerminal();
            DrawDropDownConfigDeallocate();
            string Term_key = c["EftTerminal_key"].ToString();
            TerminalModel term = tda.CheckTerminalExists(Term_key);
            if (term == null)
            {
                ViewBag.Error = "Terminal Id does not exists";
                return View(nameof(SwapTerminal), m);
            }
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(m.Member.MemberId);

            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    ViewBag.Err = "";
                    DrawDropDownConfigDeallocate();
                    DateTime EffectiveDate = Convert.ToDateTime(c["TerminalAllocationDate"].ToString());
                    DateTime dateOneMonth = DateTime.Now.AddMonths(-1);
                    DateTime dateOneWeek = DateTime.Now.AddDays(7);

                    if (EffectiveDate < dateOneMonth || EffectiveDate > dateOneWeek)
                    {
                        m.EffectiveDateString = DateTime.Now.ToString("s");
                        ViewBag.Err = "Effective date cannot be one month before or one week after, please select appropriate date";
                        return View("DeallocateTerminal", m);
                    }
                    else
                    {
                        tda1.SwapTerminal(m.TerminalAllocation_key, User.Identity.Name, m.OldTerminalStatus, term.EftTerminal_key, EffectiveDate, m.EFTTerminalLocation);
                        //allocate unlocated transaction
                        EFTDocketDA eda = new EFTDocketDA();
                        List<EFTDocketModel> unallocatedList = eda.GetUnallocatedTransactionFromSpecificDate(Term_key, EffectiveDate);
                        if (unallocatedList != null)
                        {
                            foreach (var u in unallocatedList)
                            {
                                eda.AllocateTransactionsToMember(u.EftDocket_key, term.EftTerminal_key, member_key, Vehicle);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.Err = e.Message;
                    return View("DeallocateTerminal", m);
                }

                tran.Complete();
            }
            TerminalAllocationDA mda3 = new TerminalAllocationDA();

            var isAllowGlideBox = new TerminalHostDA().IsAllowAccessToGlidebox(member_key);
            if (!isAllowGlideBox)
            {
                //auto disable glidbox when allocate terminal to Live eftpos group
                mda3.DisableGlideboxFollowBusinessType(term.EftTerminal_key, User.Identity.Name, SourceContants.SwapTerminal);
            }

            TerminalAllocationModel model = new TerminalAllocationModel();
            model = mda3.GetTerminalAllocation(member_key);
            model.AllowMOTOB = false;
            model.AllowRefundB = false;
            model.DeliveryRequiredB = false;
            model.Employee_key = 0;
            model.EffectiveDateString = DateTime.Now.ToString("s");

            ViewBag.BtnName = "Ok";
            ViewBag.ContAction = "Edit";
            ViewBag.PraValus = m.Member_key;
            ViewBag.Msg = "Terminal Allocated";
            return View("CommonMsg1");
        }

        public ActionResult AuthorisedMembers(int Id)
        {
            AuthorisedMemberDA mda = new AuthorisedMemberDA();
            List<AuthorisedMemberModel> model = mda.GetAllByMemberKey(Id);
            return View(model);
        }

        public ActionResult AuthorisedMembersPhoto(int Id)
        {
            ViewBag.MemberKey = Id;
            return View();
        }

        [RBAC(Permission = "MemberAddAuthorisedMember")]
        public ActionResult SearchAuthorisedMember(int Id)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            ViewBag.Memberkey = Id;
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddAuthorisedMember")]
        public ActionResult SearchAuthorisedMember(FormCollection c, int MemberKey)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            try
            {
                MemberModel modl = new MemberModel();
                MemberDA mda = new MemberDA();

                if (c["MemberSearchField"] != "")
                {
                    ViewBag.Memberkey = MemberKey;
                    string term = c["MemberSearchField"].ToString();

                    if (term.IndexOf(",") > 0)
                    {
                        string memberid = string.Empty;
                        string[] memberlist = term.Split(',');
                        memberid = memberlist[0];
                        if (memberid.Length > 1)
                        {
                            var mkey = mda.GetMemberKey(memberid);
                            model.MemberList.Add(mda.GetMemeber(mkey));
                        }
                    }
                    else if (term.Length > 6 && term.Length < 9 && term.Substring(0, 1) == "7")
                    {
                        var mkey = mda.GetMemberKeyByTerminalId(term);
                        model.MemberList.Add(mda.GetMemeber(mkey));
                    }
                    else
                    {
                        List<MemberModelSimple> memlist = new List<MemberModelSimple>();
                        memlist = mda.GetMemberGeneralSearch(term);
                        model.MemberList = memlist;
                    }
                }
                else
                {
                    model = null;
                }

                Session["searchList"] = null;
                return View(model);
            }
            catch (Exception ex)
            {
                LogError(ex);
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }

        }

        [RBAC(Permission = "MemberAddAuthorisedMember")]
        public ActionResult AddAuthMember(int Id, int AMember)
        {
            AuthorisedMemberDA mda = new AuthorisedMemberDA();
            mda.AddAuthorizedMember(Id, AMember);

            return RedirectToAction("Edit", new { member_key = Id });
        }

        [RBAC(Permission = "MemberRemoveAuthorisedMember")]
        public ActionResult RemoveAuthorisedMember(int Id, int AMember)
        {
            int memberkey = Id;
            ViewBag.BtnName = "Remove Member";
            ViewBag.ContAction = "RemoveAuthMember";
            ViewBag.PraValus1 = Id;
            ViewBag.PraValus2 = AMember;
            ViewBag.Title = "Remove Member";
            ViewBag.Msg = "Remove authorised member?";
            return View("CommonMsg2");
        }

        [RBAC(Permission = "MemberRemoveAuthorisedMember")]
        public ActionResult RemoveAuthMember(int Id, int AMember)
        {
            AuthorisedMemberDA mda = new AuthorisedMemberDA();
            mda.RemoveAuthorizedMember(Id, AMember);
            int memberkey = Id;
            return RedirectToAction("Edit", new { member_key = Id });
        }

        [RBAC(Permission = "MemberAddMemberNotes")]
        public ActionResult MemberNotes(string Id)
        {
            MemberDA mda = new MemberDA();
            int member_key = mda.GetMemberKey(Id);

            MemberModel mobj = new MemberModel();
            mobj = mda.GetMemeberLowVersion(member_key);

            MemberNotesDA mdna = new MemberNotesDA();
            IEnumerable<MemberNotesModel> mnm;
            mnm = mdna.GetMemberNotes(member_key);
            Session["MemeberKey"] = member_key;
            Session["MemeberId"] = Id;
            MemberNoteVM model = new MemberNoteVM();
            model.MemberId = mobj.MemberId;
            model.FirstName = mobj.FirstName;
            model.LastName = mobj.LastName;
            model.TradingName = mobj.TradingName;
            model.MemberNotes = mnm.ToList();

            return View(model);
        }

        public ActionResult SearchMemberTerminalAllocationDialog(string Id)
        {
            SearchMemberTerminalAllocationVM model = new SearchMemberTerminalAllocationVM();
            try
            {
                MemberDA mda = new MemberDA();
                int member_key = mda.GetMemberKey(Id);

                TerminalAllocationDA tda = new TerminalAllocationDA();
                model.AllVehicleTerminalAllocation = tda.GetAllVehicleTerminalAllocationList(member_key);
                model.IsError = false;
                return View(model);
            }
            catch (Exception e)
            {
                model.IsError = true;
                return View(model);
            }
        }

        private void DrawDropDownConfigDeallocate()
        {
            EftTerminalStatusDA eda = new EftTerminalStatusDA();
            var stats = eda.GetAll();
            ViewBag.StatusList = stats.Select(item => new SelectListItem
            {
                Text = item.Status,
                Value = item.Status
            }).ToList();

            EftTerminalLocationDA edl = new EftTerminalLocationDA();
            var location = edl.GetAll();
            ViewBag.LocationList = location.Select(item => new SelectListItem
            {
                Text = item.Location,
                Value = item.Location
            }).ToList();
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddMemberNotes")]
        public ActionResult CreateMemberNotes(FormCollection c, MemberNoteVM m)
        {
            MemberNotesDA mda = new MemberNotesDA();
            MemberNotesModel model = new MemberNotesModel();
            model.NoteDate = DateTime.Now;
            var t = c["IsWarning"];
            if (t == "on")
                model.Warning = true;
            else
                model.Warning = false;

            model.Note = c["Note"].ToString();

            int member_key = (int)Session["MemeberKey"];
            string Id = "0";
            if (member_key != 0)
            {
                Id = Session["MemeberId"].ToString();

                model.Member_key = member_key;
                model.Modified_dttm = DateTime.Now;
                model.ModifiedByUser = User.Identity.Name;

                mda.Add(model);
            }

            ViewBag.Msg = "Member note added";
            return View("Msg");
        }

        public JsonResult GetQbrPointHistory(string sid, string sord, int page, int rows, int memberKey, string sortorder)  //Gets the QBR history
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            using (RewardsDA taxi = new RewardsDA())
            {
                int count;
                var qbrPointHistoryList = taxi.GetQbrPointHistory(memberKey, out count, pageIndex * pageSize, pageSize);

                int totalRecords = count; // todoListsResults.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

                if (sortorder.ToUpper() == "DESC")
                {
                    qbrPointHistoryList = qbrPointHistoryList.ToList();
                }
                else
                {
                    qbrPointHistoryList = qbrPointHistoryList.ToList();
                }
                var jsonData = new
                {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = qbrPointHistoryList
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetQbrPointHistoryRejected(string sid, string sord, int page, int rows, int memberKey, string sortorder)  //Gets the QBR history
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            using (RewardsDA taxi = new RewardsDA())
            {
                var todoListsResults = taxi.GetExportFileEntries(memberKey, 7);
                int totalRecords = todoListsResults.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                if (sortorder.ToUpper() == "DESC")
                {
                    todoListsResults = todoListsResults
                            .OrderByDescending(order => order.MemberReferenceEntry.YearPeriod)
                            .ThenByDescending(order => order.MemberReferenceEntry.MonthPeriod)
                            .ThenByDescending(order => order.MemberReferenceEntry.EntryDateTime).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults = todoListsResults
                            .OrderBy(order => order.MemberReferenceEntry.YearPeriod)
                            .ThenBy(order => order.MemberReferenceEntry.MonthPeriod)
                            .ThenBy(order => order.MemberReferenceEntry.EntryDateTime).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }

                var jsonData = new
                {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = todoListsResults
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFreshDeskTickets(string status, string memberid)  //Gets the QBR Point history
        {
            using (UserDA userDA = new UserDA())
            {
                UserFreshDeskAuthModel userFreshDeskAuth = userDA.Get((Guid)Membership.GetUser().ProviderUserKey);
                string apiKey = String.Empty;
                if (userFreshDeskAuth != null)
                {
                    apiKey = userFreshDeskAuth.FreshDeskAPIKey;
                }
                string jsonData = FreshdeskAPIs.Get(apiKey, "search/tickets", "?query=\"member_id:'" + memberid + "' AND status:" + status + "\"  ");

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }



        private void DrawDropDownRewardsManualEntry(int memberKey)
        {
            ViewBag.Months = new SelectList(Enumerable.Range(1, 12).Select(x => new SelectListItem()
            {
                Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[x - 1] + " (" + x + ")",
                Value = x.ToString()
            }), "Value", "Text");

            ViewBag.Years = new SelectList(Enumerable.Range(DateTime.Today.Year - 1, 2).Select(x =>
            new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString()
            }), "Value", "Text");

            using (MemberReferenceEntryDA memberReferenceEntryDA = new MemberReferenceEntryDA())
            {
                var memberReferenceEntry = memberReferenceEntryDA.GetEntryTypeList().ToList();
                ViewBag.MemberReferenceEntryList = memberReferenceEntry.Select(item => new SelectListItem
                {
                    Text = item.Description,
                    Value = item.MemberReferenceEntryTypeKey.ToString()
                }).ToList();

                //get loyalty promotion list
                var loyaltyPromotions = memberReferenceEntryDA.GetLoyaltyPromotionList(memberKey).ToList();
                ViewBag.PromotionIds = loyaltyPromotions.Select(item => new SelectListItem
                {
                    Text = item.PromotionName,
                    Value = item.ID.ToString(),
                    Selected = item.IsDefault
                }).ToList();
            }
        }

        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult AddRewardsManualEntry(int memberid)
        {
            DrawDropDownRewardsManualEntry(memberid);
            Session["Memberkey"] = memberid;
            MemberDA mda = new MemberDA();
            return View(new MemberReferenceEntryViewModel()
            {
                Member = mda.GetMemeber(memberid),
                MemberReferenceEntries = new MemberReferenceEntryDA().GetList(memberid)
            });
        }

        [HttpPost]
        [RBAC(Permission = "MemberAddAllocateMemberTerminal")]
        public ActionResult AddRewardsManualEntry(MemberReferenceEntryViewModel model, FormCollection c)
        {
            ViewBag.MessageType = "Error";
            ViewBag.Message = "";

            DateTime dummy = new DateTime(model.SelectedYear, model.SelectedMonth, 01);
            var memberKey = (int)Session["Memberkey"];
            DrawDropDownRewardsManualEntry(memberKey);

            if (String.IsNullOrEmpty(model.Description) || (int.TryParse(model.Description, out int o) && o > 0))
            {
                ViewBag.Message = "Please add description";
            }
            else if (Session["Memberkey"] == null)
            {
                ViewBag.Message = "Invalid member details, please either refresh current page or re-search member.";
            }
            else if (dummy > DateTime.Now || dummy < DateTime.Now.AddMonths(-4))
            {
                ViewBag.Message = "Invalid month and year selected.";
            }
            else if (model.Points == null ||
                    (model.Points.HasValue &&
                        (String.IsNullOrEmpty(model.Points.Value.ToString()) ||
                        (int.TryParse(model.Points.Value.ToString(), out int n) && n == 0))))
            {
                ViewBag.Message = "Please add Points and points should be number.";
            }
            else if (model.Points > 500000 || model.Points < 0)
            {
                ViewBag.Message = "Allocated points should not be greater than 500000 and must be positive number.";
            }
            else if (model.MemberReferenceEntryTypeKey != 1)
            {
                ViewBag.Message = "Entry type should be manual.";
            }
            else if (model.SelectedPromotionId <= 0 || !new MemberReferenceEntryDA().IsExistPromotion(model.SelectedPromotionId))
            {
                ViewBag.Message = "Please select Promotion ID.";
            }

            if (ViewBag.Message != "")
            {
                model.MemberReferenceEntries = new MemberReferenceEntryDA().GetList(memberKey);

                return View(model);
            }

            try
            {
                new MemberReferenceEntryDA().Add(new MemberReferenceEntryModel()
                {
                    Description = model.Description,
                    MemberReferenceEntryTypeKey = model.MemberReferenceEntryTypeKey,
                    YearPeriod = model.SelectedYear,
                    MonthPeriod = model.SelectedMonth,
                    Points = model.Points,
                    CreatedByUser = RolePrincipal.Current.Identity.Name,
                    Member_Key = memberKey,
                    PromotionId = model.SelectedPromotionId,
                    QBRPointTypeID = 2
                });

                ModelState.Clear();

                ViewBag.MessageType = "Success";
                ViewBag.Message = "Saved entry.";

                return View(new MemberReferenceEntryViewModel()
                {
                    Member = new MemberDA().GetMemeber(memberKey),
                    MemberReferenceEntries = new MemberReferenceEntryDA().GetList(memberKey)
                });

            }
            catch (Exception ex)
            {
                LogError(ex);
                ViewBag.Message = ex.Message;
                return View(model);
            }
        }

        [HttpGet]
        public JsonResult GetSessionTerminalAllocationList()
        {
            var data = Session["TerminalAllocationList"] as List<TerminalAllocationModel>;
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTerminalAllocationList(int member_key)
        {
            var data = new TerminalAllocationDA().GetVehicleTypes(member_key);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateTerminal(AllVehicleTerminalAllocationModel model)
        {
            try
            {

                //Update Terminal Vehicle information
                EftTerminalLocationDA eftTerminalLocationDa = new EftTerminalLocationDA();
                eftTerminalLocationDa.UpdateTerminal(model);

                // Reload Terminal allocation grid.
                var memberModel = new MemberModel();
                memberModel.AllVehicleTerminalAllocation =
                    new MemberDA().GetAllVehicleTerminalAllocationModelBaseOnMemberKey(model.MemberKey);
                return PartialView("~/Views/Member/_TerminalAllocationGrid.cshtml", memberModel);

            }
            catch (Exception e)
            {
                LogError(e.Message);
                var data = new
                {
                    ErrorMessage = e.Message
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetItemDataForVehiclePopup(int businessType, int vehicleKey)
        {
            try
            {
                StateDA sda = new StateDA();
                var stateListItem = sda.GetStateAustralia().AsSelectListItem();

                VehicleTypeDA cda = new VehicleTypeDA();
                var VehicleTypeListItem = cda.Get(businessType).AsSelectListItem();

                EftTerminalLocationDA eftTerminalLocationDa = new EftTerminalLocationDA();
                var effectiveDate = eftTerminalLocationDa.GetCurrenTerminalAllocationByVehicleKey(vehicleKey)?.EffectiveDate.ToString("dd/MM/yyyy HH:mm:ss");

                var data = new
                {
                    IsError = false,
                    VehicleTypeList = VehicleTypeListItem,
                    VehicleStateList = stateListItem,
                    EffectiveDate = effectiveDate
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var data = new
                {
                    ErrorMessage = e.Message,
                    IsError = true
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [RBAC(Permission = "ManageMerchant")]
        public ActionResult RemoveMerchantMessage(int memberKey, int userId, string memberId)
        {
            ViewBag.memberKey = memberKey;
            ViewBag.userId = userId;
            ViewBag.memberId = memberId;
            ViewBag.Title = "Remove Merchant";
            ViewBag.Msg = "Remove this merchant?";
            return View("_RemoveMerchantDialog");
        }

        [RBAC(Permission = "ManageMerchant")]
        public ActionResult RemoveMerchant(int memberKey, int userId, string memberId)
        {
            MyWebDA mda = new MyWebDA();
            MemberHistoryDA historyDA = new MemberHistoryDA();

            mda.RemoveMerchant(memberKey, userId);

            string addedMerchantHistory = $"[LinkMember] [Member {memberId}] has been unlinked to [Member {Session["CurrentMemberId"].ToString()}] by {User.Identity.Name}";
            historyDA.Add(memberKey: memberKey, history: addedMerchantHistory, username: User.Identity.Name);

            return RedirectToAction("Edit", new { member_key = (int)Session["Member_key"] });
        }

        [RBAC(Permission = "ManageMerchant")]
        public ActionResult SearchMerchant(int memberKey, int companyKey, int currentUserId)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            ViewBag.Memberkey = memberKey;
            ViewBag.CompanyKey = companyKey;
            ViewBag.CurrentUserId = currentUserId;
            return View(model);
        }

        [HttpPost]
        [RBAC(Permission = "ManageMerchant")]
        public ActionResult SearchMerchant(string MemberSearchField, int CompanyKeyHiddenField, int MemberKey, int CurrentUserId)
        {
            MultiMemberSearchModel model = new MultiMemberSearchModel();
            try
            {
                MemberModel modl = new MemberModel();
                MemberDA mda = new MemberDA();
                MyWebDA webDa = new MyWebDA();

                ViewBag.Memberkey = MemberKey;
                ViewBag.CurrentUserId = CurrentUserId;
                List<string> liveTaxiGroup = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveTaxi.ToString()).Split(',').ToList();
                List<string> liveEftposGroup = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveEftpos.ToString()).Split(',').ToList();
                List<int> availableBusinessType = new List<int>();

                if (liveTaxiGroup.Contains(CompanyKeyHiddenField.ToString()))
                {
                    ViewBag.BusinessTypeName = "Live taxi";
                    availableBusinessType = liveTaxiGroup.Select(int.Parse).ToList();
                }
                else if (liveEftposGroup.Contains(CompanyKeyHiddenField.ToString()))
                {
                    ViewBag.BusinessTypeName = "Live eftpos";
                    availableBusinessType = liveEftposGroup.Select(int.Parse).ToList();
                }

                if (MemberSearchField != "")
                {
                    List<MemberModelSimple> memlist = new List<MemberModelSimple>();
                    memlist = mda.GetSimpleMembersByMemberId(MemberSearchField, availableBusinessType, MemberKey);
                    model.MemberList = webDa.GetAvailableSubAccount(memlist, CurrentUserId);
                }
                ViewBag.CompanyKey = CompanyKeyHiddenField;
                return View(model);
            }
            catch (Exception ex)
            {
                LogError(ex);
                ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }

        }

        [RBAC(Permission = "ManageMerchant")]
        public JsonResult AddMerchant(int memberKey,
                                        string memberId,
                                        string firstName,
                                        string tradingName,
                                        string email,
                                        string businessType)
        {
            MyWebDA mda = new MyWebDA();
            MemberHistoryDA historyDA = new MemberHistoryDA();

            usermembership merchant = mda.AddMerchant(MemberKey: memberKey, UserId: (int)Session["UserId"], username: User.Identity.Name);
            string addedMerchantHistory = $"[LinkMember] [Member {memberId}] has been linked to [Member {Session["CurrentMemberId"].ToString()}] by {User.Identity.Name}";
            historyDA.Add(memberKey: memberKey, history: addedMerchantHistory, username: User.Identity.Name);

            IEmail emailProvider = LGEmail.GetEmailProvider(EmailType.SparkPost);

            Dictionary<string, object> customMetaData = new Dictionary<string, object>();
            customMetaData["FNAME"] = firstName;
            customMetaData["subAccountMerchantId"] = memberId;
            customMetaData["subAccountTradingName"] = tradingName;
            customMetaData["merchantID"] = Session["CurrentMemberId"].ToString();
            customMetaData["TradingName"] = Session["CurrentMyWebTradingName"].ToString();
            customMetaData["businessTypeSubAccount"] = businessType;

            dynamic sendEmailConfig = new ExpandoObject();
            sendEmailConfig.CustomMetaData = customMetaData;
            sendEmailConfig.SparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
            sendEmailConfig.TemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LinkMerchantTemplateID.ToString());
            sendEmailConfig.Recipient = email;

            emailProvider.SendEmail(sendEmailConfig);

            return new JsonResult()
            {
                Data = new
                {
                    MemberKey = (int)Session["Member_key"],
                    IsCompleted = true
                }
            };
        }

        public ActionResult ConfirmAddingMerchant(int memberKey,
                                                  string memberId,
                                                  string firstName,
                                                  string tradingName,
                                                  string email,
                                                  string mobile,
                                                  string businessType)
        {
            ViewBag.memberKey = memberKey;
            ViewBag.memberId = memberId;
            ViewBag.firstName = firstName;
            ViewBag.tradingName = tradingName;
            ViewBag.email = email;
            ViewBag.mobile = mobile;
            ViewBag.businessType = businessType;
            ViewBag.ParentMemberKey = (int)Session["Member_key"];
            ViewBag.ParentMemberId = Session["CurrentMemberId"].ToString();
            ViewBag.ParentTradingName = Session["CurrentMyWebTradingName"].ToString();
            ViewBag.Title = "Confirmation";

            return View("_ConfirmationForAddingSubMerchant");
        }

        public ActionResult SendPhoneVerification(int memberKey,
                                                  string memberId,
                                                  string firstName,
                                                  string tradingName,
                                                  string email,
                                                  string mobile,
                                                  string businessType)
        {
            try
            {
                ViewBag.memberKey = memberKey;
                ViewBag.memberId = memberId;
                ViewBag.firstName = firstName;
                ViewBag.tradingName = tradingName;
                ViewBag.email = email;
                ViewBag.mobile = mobile;
                ViewBag.businessType = businessType;
                ViewBag.ParentMemberId = Session["CurrentMemberId"].ToString();
                ViewBag.ParentTradingName = Session["CurrentMyWebTradingName"].ToString();
                ViewBag.Title = "Verification";

                return View("_VerificationPhoneDialog");
            }
            catch (Exception e)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Message = "Error. Please contact Livegroup support.",
                        IsError = true
                    }
                };
            }

        }

        [HttpPost]
        [RBAC(Permission = "ManageMerchant")]
        public JsonResult SendPhoneVerification(string mobile)
        {
            try
            {
                ViewBag.ParentMemberId = Session["CurrentMemberId"].ToString();
                ViewBag.ParentTradingName = Session["CurrentMyWebTradingName"].ToString();
                ViewBag.Title = "Verification";

                var phoneNumber = mobile?.Trim();

                SMSModel smsConfig = new SMSModel
                {
                    AuthyKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioAuthKey.ToString()),
                    CodeLength = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioCodeLength.ToString()),
                    CountryCode = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioCountryCode.ToString()),
                    Via = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioSendSMSVia.ToString()),
                    BaseURL = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioSendCodeUri.ToString())
                };
                var enableTwilioVerificationConfig = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioEnableSendSMS.ToString());

                var isEnableTwilio = false;
                bool.TryParse(enableTwilioVerificationConfig, out isEnableTwilio);

                var twilioAPI = new TwilioAPI(smsConfig);
                twilioAPI.SendSMSVerificationCode(phoneNumber, isEnableTwilio);

                return new JsonResult()
                {
                    Data = new
                    {
                        Message = string.Empty,
                        IsError = false
                    }
                };
            }
            catch (Exception e)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Message = e.Message,
                        IsError = true
                    }
                };
            }
        }

        [HttpPost]
        public JsonResult ValidateVerificationCode(string verificationCode, string mobile)
        {
            try
            {
                ViewBag.ParentMemberId = Session["CurrentMemberId"].ToString();
                ViewBag.ParentTradingName = Session["CurrentMyWebTradingName"].ToString();
                ViewBag.Title = "Verification";

                var phoneNumber = mobile?.Trim();

                SMSModel smsConfig = new SMSModel
                {
                    AuthyKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioAuthKey.ToString()),
                    CodeLength = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioCodeLength.ToString()),
                    CountryCode = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioCountryCode.ToString()),
                    Via = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioSendSMSVia.ToString()),
                    BaseURL = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioCheckCodeUri.ToString())
                };
                var enableTwilioVerificationConfig = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixTwilioEnableSendSMS.ToString());

                var isEnableTwilio = false;
                bool.TryParse(enableTwilioVerificationConfig, out isEnableTwilio);

                var twilioAPI = new TwilioAPI(smsConfig);
                twilioAPI.ValidateSMSVerificationCode(phoneNumber, verificationCode, isEnableTwilio);

                return new JsonResult()
                {
                    Data = new
                    {
                        Message = string.Empty,
                        IsError = false
                    }
                };
            }
            catch (Exception e)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Message = e.Message,
                        IsError = true
                    }
                };
            }
        }



        [HttpGet]
        public JsonResult GetMemberNotes(int memberKey, int currentPage, int pageSize)
        {
            MemberNotesDA mnDA = new MemberNotesDA();
            var notes = mnDA.GetAllMemberNotes(memberKey);

            int totalRecords = notes.Count();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var skip = (currentPage - 1) * pageSize;
            var res = notes.Skip(skip).Take(pageSize);

            var jsonData = new
            {
                total = totalPages,
                page = currentPage,
                records = totalRecords,
                rows = res.ToList()
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [RBAC(Permission = "GhostMyWebUser")]
        public JsonResult GhostMyWebAccount(int memberKey)
        {
            try
            {
                MyWebDA mwda = new MyWebDA();
                var myWebUserProfile = mwda.GetLoginProfile(memberKey);

                if (myWebUserProfile.UserId > 0) // the myweb account is valid
                {
                    var phoenixUserId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                    var myWebUserId = myWebUserProfile.UserId;

                    //check in case another myweb account was ghost before but doesn't log out
                    var otherAccountGhosted = mwda.GetMyWebGhostDiffMyWebUserId(phoenixUserId, myWebUserId);
                    if (otherAccountGhosted != null)
                    {
                        var myWebUserName = mwda.GetUserProfileById(otherAccountGhosted.MyWebUserIDGhost)?.UserName;
                        var memberId = mwda.GetMyWebGhostHistory(otherAccountGhosted.PhoenixUserIDGhost,
                            otherAccountGhosted.MyWebUserIDGhost)?.Member.MemberId;
                        throw new Exception(
                            $"The {User.Identity.Name} is logging into {myWebUserName} - MID {memberId}. Please log out that account before ghosting into another account.");
                    }

                    //check business group type of myweb account
                    var mywebUrl = string.Empty;
                    var mda = new MemberDA();
                    var companyKey = mda.GetMemberCompanyKey(memberKey);

                    List<string> liveTaxiGroup = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveTaxi.ToString()).Split(',').ToList();
                    List<string> liveEftposGroup = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveEftpos.ToString()).Split(',').ToList();

                    //the myweb account is live taxi
                    if (liveTaxiGroup.Contains(companyKey.ToString()))
                    {

                        var uri = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveTaxiGhostUri.ToString());
                        if (string.IsNullOrEmpty(uri))
                        {
                            throw new Exception("LiveTaxiGhostUri configuration not available");
                        }
                        mywebUrl = uri;
                    }

                    //the myweb account is live eftpos
                    if (liveEftposGroup.Contains(companyKey.ToString()))
                    {
                        var uri = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveEftposGhostUri.ToString());
                        if (string.IsNullOrEmpty(uri))
                        {
                            throw new Exception("LiveEftposGhostUri configuration not available");
                        }
                        mywebUrl = uri;
                    }

                    if (string.IsNullOrEmpty(mywebUrl))
                    {
                        throw new Exception("The myweb url invalid.");
                    }

                    //encrypt data
                    var encryptKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.MyWebGhostKey.ToString());//encrypt key
                    var encyptData = LiveEncryptionHelper.TripleDESEncypt($"{phoenixUserId}|{myWebUserId}", encryptKey);

                    //saving ghost data and log
                    mwda.SaveGhostMyWebData(memberKey, phoenixUserId, myWebUserId);

                    return new JsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Url = mywebUrl + HttpUtility.UrlEncode(encyptData)
                        }
                    };

                }
                else
                {
                    throw new Exception("The myweb account invalid.");
                }
            }
            catch (Exception e)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Message = e.Message,
                        IsError = true
                    }
                };
            }
        }

        [RBAC(Permission = "EditFees")]
        public ActionResult MemberFees()
        {
            FeeDA fda = new FeeDA();
            var feeTypes = new List<FeeTypeModel>();
            feeTypes.Add(new FeeTypeModel
            {
                ID = 0,
                Name = "Please Select",
                MinAmount = 0,
                MaxAmount = 0
            });
            feeTypes.AddRange(fda.GetFees().Select(n => new FeeTypeModel
            {
                ID = n.ID,
                Name = n.Name,
                MinAmount = n.MinAmount,
                MaxAmount = n.MaxAmount
            }));
            string feeTypeAsJson = JsonConvert.SerializeObject(feeTypes);
            ViewBag.FeeTypes = feeTypeAsJson;
            return View();
        }

        [RBAC(Permission = "EditFees")]
        public ActionResult AddMemberForFee(string memberId)
        {
            if (string.IsNullOrEmpty(memberId))
            {
                return Json(new { Message = "Please enter Member Id." }, JsonRequestBehavior.AllowGet);
            }

            List<string> memberIds = memberId.Split(';').Select(x => x.Trim().ToLower()).Distinct().ToList();
            List<MemberModelSimple> models = new List<MemberModelSimple>();
            List<string> addedError = new List<string>();
            List<string> addedErrorBecauseNotHasTerminalAllocation = new List<string>();
            List<string> invalidIds = new List<string>();

            using (var entities = new TaxiEpayEntities())
            {
                var memberList = entities.Members;
                List<string> memberIdsAlreadyHaveFee = (from m in entities.Members
                                                        join fcd in entities.FeeChargeDetails on m.Member_key equals fcd.MemberKey
                                                        select m.MemberId.ToLower()).ToList();
                var midAllocatedTerminal =
                    entities.vTerminalAllocations.Where(n => memberIds.Contains(n.MemberID) && n.IsCurrent && n.EftTerminal_key != null && !n.VirtualTerminal)
                        .Select(n => n.MemberID.ToLower()).ToList();
                foreach (var id in memberIds)
                {
                    if (string.IsNullOrEmpty(id))
                    {
                        continue;
                    }
                    if (memberIdsAlreadyHaveFee.Contains(id))
                    {
                        addedError.Add(id);
                    }
                    else
                    {
                        var member = memberList.FirstOrDefault(x => x.MemberId == id);
                        if (member != null)
                        {
                            // If merchant has any terminal allocation, allowed to add Account Fee
                            if (midAllocatedTerminal.Any(n => n == id))
                            {
                                models.Add(new MemberModelSimple
                                {
                                    MemberId = member.MemberId,
                                    FirstName = member.FirstName,
                                    LastName = member.LastName,
                                    TradingName = member.TradingName,
                                    Email = member.Email
                                });
                            }
                            else
                            {
                                //otherwise will add to error list
                                addedErrorBecauseNotHasTerminalAllocation.Add(id);
                            }
                        }
                        else
                        {
                            invalidIds.Add(id);
                        }
                    }
                }
            }
            string message = "";
            if (invalidIds.Any())
            {
                string ids = invalidIds.Count > 1 ? string.Join(", ", invalidIds) : invalidIds[0].ToString();
                message = "Cannot add invalid MID(s): " + ids + "\n";
            }

            if (addedErrorBecauseNotHasTerminalAllocation.Any())
            {
                string ids = addedErrorBecauseNotHasTerminalAllocation.Count > 1 ? string.Join(", ", addedErrorBecauseNotHasTerminalAllocation) : addedErrorBecauseNotHasTerminalAllocation[0].ToString();
                addedErrorBecauseNotHasTerminalAllocation.Remove("");
                message += "Members with MIDs cannot be added as " + ids + " do not have any physical terminal allocation.";
            }

            if (addedError.Any())
            {
                if (addedError.Count == memberIds.Count)
                {
                    string ids = addedError.Count > 1 ? string.Join(", ", addedError) : addedError[0].ToString();
                    addedError.Remove("");
                    message += "Members with MIDs cannot be added as " + ids + " have already existing Account Fee.";
                }
                else
                {
                    string ids = addedError.Count > 1 ? string.Join(", ", addedError) : addedError[0].ToString();
                    addedError.Remove("");
                    message += "Can not add the Account Fee to " + ids + " as the Account fee already exists for the MID.";
                }
            }
            var jsonData = new
            {
                addedError = message,
                records = models,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [RBAC(Permission = "EditFees")]
        public ActionResult AddFeeDetailMultipleMembers(int feeTypeId, decimal amount, string memberIds)
        {
            FeeDA fda = new FeeDA();
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                List<string> ids = memberIds.Split(',').ToList();
                foreach (string id in ids)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        var mem = context.Members.Where(m => m.MemberId == id).FirstOrDefault();
                        if (mem != null)
                        {
                            fda.UpdateFeeDetailForMultilpleMembers(context, feeTypeId, amount, mem.Member_key, this.ControllerContext.HttpContext.User.Identity.Name);
                        }
                    }
                }
            }
            var feeTypes = fda.GetFees().Select(n => new FeeTypeModel
            {
                ID = n.ID,
                Name = n.Name,
                MinAmount = n.MinAmount,
                MaxAmount = n.MaxAmount
            }).ToList();
            string feeTypeAsJson = JsonConvert.SerializeObject(feeTypes);
            ViewBag.FeeTypes = feeTypeAsJson;
            return View("MemberFees");
        }

        [RBAC(Permission = "EditFees")]
        public ActionResult AddMemberFee(int memberKey)
        {
            FeeDA fda = new FeeDA();
            var feeAvailabled = fda.Get(memberKey).Select(n => n.FeeID).Distinct();
            var chargeFeeTypes = fda.GetFees().Where(n => !feeAvailabled.Contains(n.ID)).Select(n => new FeeTypeModel
            {
                ID = n.ID,
                Name = n.Name,
                MinAmount = n.MinAmount,
                MaxAmount = n.MaxAmount
            }).ToList();

            var selectListItem = new List<SelectListItem>();
            selectListItem.Add(new SelectListItem()
            {
                Selected = true,
                Text = "Please Select",
                Value = "-1"
            });

            selectListItem.AddRange(chargeFeeTypes.Select(item => new SelectListItem
            {
                Text = item.Name,
                Value = item.ID.ToString()
            }).ToList());
            ViewBag.ChargeFeeTypes = selectListItem;
            ViewBag.MemberKey = memberKey;
            ViewBag.FeeAsJson = JsonConvert.SerializeObject(chargeFeeTypes);
            return View();
        }

        [HttpPost]
        [RBAC(Permission = "EditFees")]
        public ActionResult AddMemberFee(MemberFeeModel model)
        {
            model.FrequencyID = 1;
            FeeDA fda = new FeeDA();
            using (var taxiEpaydbContext = new TaxiEpayEntities())
            {
                fda.CreateFeeForMember(taxiEpaydbContext, this.ControllerContext.HttpContext.User.Identity.Name, model);
            }

            var memberModel = new MemberModel();
            memberModel.Fees = fda.Get(model.MemberKey);

            return PartialView("~/Views/Member/_MemberFeeGrid.cshtml", memberModel);
        }

        [HttpGet]
        [RBAC(Permission = "EditFees")]
        public ActionResult EditMemberFee(int memberKey, int feeChargeDetailID)
        {

            ViewBag.MemberKey = memberKey;
            using (var taxiEpaydbContext = new TaxiEpayEntities())
            {
                FeeDA fda = new FeeDA();
                var feechargedetail = fda.GetFeeChargeDetailByIDAndMemberID(taxiEpaydbContext, feeChargeDetailID, memberKey);
                if (feechargedetail == null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        Message = "Data not availabled."

                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = true,
                        Message = "",
                        Data = feechargedetail.ToFeeChargeDetailModel()

                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        [RBAC(Permission = "EditFees")]
        public ActionResult EditMemberFee(FeeChargeDetailModel editMemberFee)
        {
            if (ModelState.IsValid)
            {
                var feeDA = new FeeDA();
                //Get current feechargedetail
                var feeChargeDetail = feeDA.GetFeeChargeDetailByIDAndMemberID(new TaxiEpayEntities(),
                    editMemberFee.FeeChargeDetailID, editMemberFee.MemberKey);

                if (feeChargeDetail.IsActive != editMemberFee.IsActive)
                {
                    if (editMemberFee.IsActive)
                    {
                        //Records both of Active status and Amount
                        feeDA.UpdateFeeChargeDetail(editMemberFee.FeeChargeDetailID, ControllerContext.HttpContext.User.Identity.Name, editMemberFee.Reason, editMemberFee.TicketNumber, editMemberFee.Amount, editMemberFee.IsActive);
                    }
                    else
                    {
                        //Only perform the status changed
                        feeDA.DeactivateFeeChargeDetail(editMemberFee.FeeChargeDetailID, ControllerContext.HttpContext.User.Identity.Name, editMemberFee.Reason, editMemberFee.TicketNumber, feeChargeDetail.Amount);
                    }
                }
                else
                {
                    //Change Amount only
                    new FeeDA().UpdateFeeChargeDetail(editMemberFee.FeeChargeDetailID, ControllerContext.HttpContext.User.Identity.Name, editMemberFee.Reason, editMemberFee.TicketNumber, editMemberFee.Amount, editMemberFee.IsActive, false);
                }
            }
            else
            {
                ModelState.AddModelError("error", "The Reason or Ticket# not correct. Please try again.");
            }

            FeeDA fda = new FeeDA();
            MemberModel memberModel = new MemberModel();
            memberModel.Fees = fda.Get(editMemberFee.MemberKey);

            return PartialView("~/Views/Member/_MemberFeeGrid.cshtml", memberModel);
        }

        [RBAC(Permission = "ViewFees")]
        public JsonResult GetChargeActivities(ChargeActivitySearchModel searchModel)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                string fromDate = searchModel.StartDate == null ? "" : searchModel.StartDate.Value.ToString("yyyy-MM-dd") + " 00:00:00.000";
                string endDate = searchModel.EndDate == null ? "" : searchModel.EndDate.Value.ToString("yyyy-MM-dd") + " 23:59:59.000";
                string orderDirection = searchModel.SortDirection == null ? "" : searchModel.SortDirection;
                string orderColumn = searchModel.SortColumn == null ? "" : searchModel.SortColumn == "Status" ? "ListOrder " + orderDirection : searchModel.SortColumn + " " + orderDirection;
                string status = searchModel.Status == null ? "" : searchModel.Status;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("@memberKey", searchModel.MemberKey));
                sqlParameters.Add(new SqlParameter("@page", searchModel.CurrentPage));
                sqlParameters.Add(new SqlParameter("@offset", searchModel.PageSize));
                sqlParameters.Add(new SqlParameter("@orderColumn", orderColumn));

                sqlParameters.Add(new SqlParameter("@status", status));
                sqlParameters.Add(new SqlParameter("@startdate", fromDate));
                sqlParameters.Add(new SqlParameter("@enddate", endDate));
                var totalItemsParam = new SqlParameter
                {
                    ParameterName = "@totalNumber",
                    Value = 0,
                    Direction = ParameterDirection.Output
                };
                sqlParameters.Add(totalItemsParam);

                var response = context.Database.SqlQuery<MemberFeeChargeHistory>("EXEC usp_GetFeeChargeHistoryData " +
                     "@memberKey, " +
                     "@page, " +
                     "@offset, " +
                     "@orderColumn, " +
                     "@status, " +
                     "@startdate, " +
                     "@enddate, " +
                     "@totalNumber OUTPUT", sqlParameters.ToArray()).ToList();

                int total = 0;
                int.TryParse(totalItemsParam.Value.ToString(), out total);
                int totalPages = (int)Math.Ceiling((float)total / (float)searchModel.PageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = searchModel.CurrentPage,
                    records = total,
                    rows = response
                };

                return new CustomJsonResult
                {
                    Data = jsonData
                };
            }
        }

        [RBAC(Permission = "ViewFees")]
        public JsonResult GetChangeActivities(ChangeActivitySearchModel searchModel)
        {
            using (TaxiEpayEntities context = new TaxiEpayEntities())
            {
                var histories = context.AccountFeeChangeHistories
                            .Where(m => m.FeeChargeDetail.MemberKey == searchModel.MemberKey &&
                                        (searchModel.ProcessedDate == null ||
                                            (
                                                m.CreatedDate.Value.Year == searchModel.ProcessedDate.Value.Year &&
                                                m.CreatedDate.Value.Month == searchModel.ProcessedDate.Value.Month &&
                                                m.CreatedDate.Value.Day == searchModel.ProcessedDate.Value.Day
                                            )
                                        ) &&
                                        (string.IsNullOrEmpty(searchModel.Username) || m.CreatedBy.Contains(searchModel.Username))
                                  );

                if (!string.IsNullOrEmpty(searchModel.SortColumn))
                {
                    switch (searchModel.SortColumn)
                    {
                        case "ProcessedDate":
                            if (searchModel.SortDirection.ToUpper() == "ASC")
                            {
                                histories = histories.OrderBy(h => h.CreatedDate);
                            }
                            else
                            {
                                histories = histories.OrderByDescending(h => h.CreatedDate);
                            }
                            break;
                        case "Username":
                            if (searchModel.SortDirection.ToUpper() == "ASC")
                            {
                                histories = histories.OrderBy(h => h.CreatedBy);
                            }
                            else
                            {
                                histories = histories.OrderByDescending(h => h.CreatedBy);
                            }
                            break;
                    }
                }
                var result = histories.Select(m => new MemberFeeChangeHistory
                {
                    ID = m.ID,
                    ProcessedDate = m.CreatedDate.Value,
                    Reason = m.Reason,
                    Username = m.CreatedBy,
                    TicketNumber = m.TicketNumber,
                    OldAmount = m.OldAmount,
                    NewAmount = m.NewAmount,
                    IsActive = m.IsActive,
                    Description = ""
                })
                           .ToList();
                int totalRecords = result.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)searchModel.PageSize);
                var skip = (searchModel.CurrentPage - 1) * searchModel.PageSize;
                var res = result.Skip(skip).Take(searchModel.PageSize);

                var jsonData = new
                {
                    total = totalPages,
                    page = searchModel.CurrentPage,
                    records = totalRecords,
                    rows = res.ToList()
                };
                return new CustomJsonResult() { Data = jsonData };
            }
        }

        [HttpPost]
        //[RBAC(Permission = "EditFees")]
        [RBAC(Permission = "RefundFees")]
        public ActionResult RefundMemberFee(MemberRefundFeeModel model)
        {
            FeeDA fda = new FeeDA();
            using (var taxiEpaydbContext = new TaxiEpayEntities())
            {
                fda.RefundFeeForMember(taxiEpaydbContext, this.ControllerContext.HttpContext.User.Identity.Name, model);
            }

            var memberModel = new MemberModel();
            memberModel.Fees = fda.Get(model.MemberKey);
            return PartialView("~/Views/Member/_MemberFeeHistoryGrid.cshtml", memberModel);
        }

        public ActionResult GetDefaultCommissionRateByBusiness(int model)
        {
            return RedirectToAction("EditMemberCommissionRate", new { MemberId = "", BusinessType = model });
        }

        public JsonResult LoadDataForCommissionRatePopup(int? BusinessType, string state)
        {
            try
            {
                CompanyDA cda = new CompanyDA();
                CommissionRateDA rateDA = new CommissionRateDA();
                VehicleTypeDA vda = new VehicleTypeDA();

                var company = cda.Get(BusinessType.GetValueOrDefault());

                List<VehicleTypeModel> vehicles = vda.Get(BusinessType.GetValueOrDefault());
                var vehicleKeys = vehicles.Select(v => v.VehicleType_key).ToList();
                //var defaultRates = rateDA.GetAllDefaultCommissionRates(state, BusinessType.GetValueOrDefault(), vehicleKeys);

                var vehicleListItem = vehicles
                    .OrderBy(v => v.Name)
                    .Select(item => new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.VehicleType_key.ToString()
                    })
                    .ToList();
                var data = new
                {
                    IsError = false,
                    VehicleTypeListItem = vehicleListItem,
                    StartDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"),
                    BusinessName = company.Name
                    //DefaultCommissionRates = JsonConvert.SerializeObject(defaultRates)
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var data = new
                {
                    ErrorMessage = e.Message,
                    IsError = true
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        private void SendEmailForChangingSurcharge(List<CardSurchargeInfo> infos, string firstName, string email)
        {
            var emailConfig = LGEmail.GetEmailProvider(EmailType.SparkPost);
            dynamic emailDto = new ExpandoObject();

            Dictionary<string, object> customMetaData = new Dictionary<string, object>();
            customMetaData["customer_first"] = firstName;

            foreach (CardSurchargeInfo info in infos)
            {
                string cardType = info.CardName.ToUpper();
                customMetaData[cardType + "_OLDMSF"] = info.OldMSF;
                customMetaData[cardType + "_NEWMSF"] = info.NewMSF;
                customMetaData[cardType + "_NEWRATE"] = info.NewSurchargeRate;
            }
            emailDto.CustomMetaData = customMetaData;
            emailDto.TemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.SurchargeSettingUpdatedEmailTemplate.ToString());
            emailDto.SparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
            emailDto.Recipient = email;
            emailConfig.SendEmail(emailDto);
        }

        [HttpPatch]
        public JsonResult SendAPIKey(string TerminalID)
        {
            if (string.IsNullOrEmpty(TerminalID))
            {
                return Json(new
                {
                    IsSuccess = false
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                Member member = null;
                using (var entites = new TaxiEpayEntities())
                {
                    var terminal = entites.vTerminalAllocations.FirstOrDefault(n =>
                        n.IsCurrent && n.VirtualTerminal && n.TerminalId == TerminalID);
                    if (terminal == null)
                    {
                        LogError($"The Terminal ID {TerminalID} does not belong to any merchant.");
                        return Json(new
                        {
                            IsSuccess = false
                        }, JsonRequestBehavior.AllowGet);
                    }

                    member = entites.Members.FirstOrDefault(n => n.Member_key == terminal.Member_key);
                    if (member == null)
                    {
                        LogError($"The Member key {terminal.Member_key} does not exist.");
                        return Json(new
                        {
                            IsSuccess = false
                        }, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(member.Email))
                    {
                        LogError($"The Member ID = {member.MemberId} does not has email");
                        return Json(new
                        {
                            IsSuccess = false
                        }, JsonRequestBehavior.AllowGet);
                    }

                }

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                Auth0ManagementAPI auth0 = new Auth0ManagementAPI(
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientID.ToString()),
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationClientSecretKey.ToString()),
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationAudience.ToString()),
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.FuseAuthenticationBaseURL.ToString()),
                    Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.GetPluginAPIKeyURL.ToString()));
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var jsonboject = new
                {
                    TID = TerminalID
                };
                request.AddParameter("application/json", JsonConvert.SerializeObject(jsonboject), ParameterType.RequestBody);

                try
                {
                    var data = auth0.Execute<APIKeyResponse>(request);
                    if (data.error)
                    {
                        LogError($"{data.message}");
                        return Json(new
                        {
                            IsSuccess = false
                        }, JsonRequestBehavior.AllowGet);
                    }

                    var emailProvider = LGEmail.GetEmailProvider(EmailType.SparkPost);

                    Dictionary<string, object> customMetaData = new Dictionary<string, object>();
                    customMetaData["FirstName"] = member.FirstName;
                    customMetaData["VirtualTerminalID"] = TerminalID;
                    customMetaData["GeneratedAPIKey"] = data.data.apiKey;
                    customMetaData["APIKeyExpiryDate"] = data.data.expiredTime;

                    dynamic sendEmailConfig = new ExpandoObject();
                    sendEmailConfig.CustomMetaData = customMetaData;
                    sendEmailConfig.SparkPostAPIKey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebSparkPostAPIKey.ToString());
                    sendEmailConfig.TemplateID = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.eCommerceAPIKeyEmailTemplate.ToString());
                    sendEmailConfig.Recipient = member.Email;

                    emailProvider.SendEmail(sendEmailConfig);

                    return Json(new
                    {
                        IsSuccess = true
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    LogError($"{e.Message}");
                    return Json(new
                    {
                        IsSuccess = false
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                LogError(GetAllException(e));
                return Json(new
                {
                    IsSuccess = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [RBAC(Permission = "ExportMember")]
        public ActionResult PHWMemberList(
            string SearchKeyword,
            bool IsSearchByKeyword,
            string CompanyKey,
            string PaymentKey,
            string DealerKey,
            string QBRStatusKey,
            string CreatedDateFrom,
            string CreatedDateTo,
            string IsActive,
            bool IsUseAutoComplete,
            string SortField,
            string SortOrder
            )
        {
            try
            {
                SearchKeyword = IsSearchByKeyword ? SearchKeyword : "";
                IsUseAutoComplete = !IsSearchByKeyword ? false : IsUseAutoComplete;
                if (SearchKeyword.IndexOf(",") > 0)
                {
                    string[] memberInformations = SearchKeyword.Split(',');
                    SearchKeyword = memberInformations[0];
                }

                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}PHWMemberList";

                IList<ReportingWebService.ParameterValue> reportparam = new List<ReportingWebService.ParameterValue>();

                reportparam.Add(new ReportingWebService.ParameterValue { Name = "Keyword", Value = SearchKeyword ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CompanyTypeId", Value = CompanyKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "PaymentTypeId", Value = PaymentKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "DealerId", Value = DealerKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "QBRStatus", Value = QBRStatusKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CreatedFrom", Value = string.IsNullOrEmpty(CreatedDateFrom) ? null : CreatedDateFrom });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CreatedTo", Value = string.IsNullOrEmpty(CreatedDateTo) ? null : CreatedDateTo });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "IsActive", Value = IsActive ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "IsFromAutoSuggest", Value = IsUseAutoComplete ? "1" : "0" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SortField", Value = SortField ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SortOrder", Value = SortOrder ?? "" });

                string format = "EXCELOPENXML";
                Models.Common.RenderReport(ReportName, format, reportparam, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = $"PHW_Member_List_{DateTime.Now.ToString("ddMMyyyy")}.xlsx",
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(output, mimeType);
            }
            catch (Exception e)
            {
                return File(new byte[] { }, "application/xlsx");
            }
        }

        public JsonResult GetPricingPlan(int subBusinessId)
        {
            try
            {
                var result = new PricingPlanDA().GetPricingPlan(subBusinessId);

                var pricingPlanVMs = result.GroupBy(p => new { p.MSFPLanID, p.MSFPLanName }).Select(g => new PricingPlanVM()
                {
                    PlanId = g.Key.MSFPLanID,
                    PlanName = g.Key.MSFPLanName,
                    BusinessTypes = new List<Models.APIModels.BusinessType>(),
                    Rates = g.Select(r => new PricingRate()
                    {
                        PayMethodKey = r.PayMethodKey,
                        PayMethodName = r.PayMethodName,
                        Rate = r.Rate,
                        TransactionFee = r.TransactionFee,
                        IsRateFixed = r.IsRateFixed
                    })
                        .Distinct()
                        .ToList(),
                }).ToList();

                var data = new
                {
                    IsError = false,
                    PricingPlanList = pricingPlanVMs
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var data = new
                {
                    ErrorMessage = e.Message,
                    IsError = true
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPaymentHistory(PaymentSearchCriteriaVM searchModel)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = searchModel.SortField;
            query["sortOrder"] = searchModel.SortOrder;
            query["pageSize"] = searchModel.PageSize.ToString();
            query["pageIndex"] = searchModel.PageIndex.ToString();
            query["memberkey"] = searchModel.MemberKey;
            query["fromdate"] = searchModel.FromDate;
            query["todate"] = searchModel.ToDate;
            query["paymentReferenceNumber"] = searchModel.ReferenceNo;
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<DataSearchResponse<PaymentHistoryModel>>(PhoenixWebAPIEndpoint.PAYMENTHISTORY_GET_LIST + uriBuilder.Query.ToString());

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPaymentHistoryDetail(string transactionKey)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<PaymentHistoryDetailModel>(PhoenixWebAPIEndpoint.PAYMENTHISTORY_GET_LIST + "/" + transactionKey + uriBuilder.Query.ToString());

            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        private void DisableZipPaymentOfAllTerminals(int memberKey)
        {
            try
            {
                LogInfo($"DisableZipPayment - Get all terminals form member key {memberKey}");

                var terminalAllocationDA = new TerminalAllocationDA();
                var listAllocation = terminalAllocationDA.GetAllVehicleTerminalAllocationList(memberKey);
                var listAvaiAllocationTerminals = listAllocation.Where(x => x.IsAllocated).Select(x => x.TerminalId).ToList();

                if (listAvaiAllocationTerminals != null && listAvaiAllocationTerminals.Any())
                {
                    LogInfo($"DisableZipPayment - Begin increase Version number and set EnableZip to false.");

                    using (TransactionHostEntities context = new TransactionHostEntities())
                    {
                        foreach (var terminalId in listAvaiAllocationTerminals)
                        {
                            var terminal = (from t in context.Terminals
                                            where t.TerminalID.Trim() == terminalId.Trim() && t.EnableZip == true
                                            select t).FirstOrDefault();

                            if (terminal != null)
                            {
                                LogInfo($"DisableZipPayment - Update termialID: {terminalId}, version: {terminal.VersionNo}.");

                                terminal.VersionNo = terminal.VersionNo + 1;
                                terminal.EnableZip = false;

                                terminal.UpdatedBy = new Guid();
                                terminal.UpdatedDate = DateTime.Now;
                                terminal.UpdatedByUser = User.Identity.Name;
                            }
                        }

                        LogInfo($"DisableZipPayment - Save to dabase.");
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogError($"DisableZipPayment - Error: {ex.Message}.");
                LogError(ex);
            }
        }

        private void DisableZipPaymentOfTerminals(int memberKey, string terminalId)
        {
            try
            {
                LogInfo($"DisableZipPayment - Get zip API config form member key {memberKey}");

                var zipMerchantAPIDA = new ZipMerchantAPIDA();
                var zipMerchantAPI = zipMerchantAPIDA.GetByMemberKey(memberKey);
                var inActive = zipMerchantAPI?.InActive ?? true; // inactive = true mean is not active

                LogInfo($"DisableZipPayment - inActive: {inActive}");

                if (inActive)
                {
                    using (TransactionHostEntities context = new TransactionHostEntities())
                    {
                        var terminal = (from t in context.Terminals
                                        where t.TerminalID.Trim() == terminalId.Trim() && t.EnableZip == true
                                        select t).FirstOrDefault();

                        if (terminal != null)
                        {
                            LogInfo($"DisableZipPayment - Update termialID : {terminalId}, version: {terminal.VersionNo}");

                            terminal.VersionNo = terminal.VersionNo + 1;
                            terminal.EnableZip = false;

                            terminal.UpdatedBy = new Guid();
                            terminal.UpdatedDate = DateTime.Now;
                            terminal.UpdatedByUser = User.Identity.Name;

                            LogInfo($"DisableZipPayment - Update complete termialID : {terminalId}.");
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError($"DisableZipPayment - Error: {ex.Message}.");
                LogError(ex);
            }
        }
    }

    public class APIKeyResponse
    {
        public string message { get; set; }
        public bool error { get; set; }
        public ApiKeyData data { get; set; }
    }

    public class ApiKeyData
    {
        public string apiKey { get; set; }
        public string expiredTime { get; set; }
    }

    public class RateListToSaveConfig
    {
        public int CompanyKey { get; set; }

        public int RentalMemberRentalTemplateKey { get; set; }

        public List<TerminalRentalRateModel> TerminalRentalRateList { get; set; }
    }
}
