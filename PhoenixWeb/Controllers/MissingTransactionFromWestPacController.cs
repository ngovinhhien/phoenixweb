﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using PhoenixWeb.Helper;
using PhoenixWeb.Models.ViewModel;
using Phoenix_Service;

namespace PhoenixWeb.Controllers
{
    public class MissingTransactionFromWestPacController : PhoenixWebController
    {
        public ActionResult Index()
        {
            MissingTransactionFromWestPacVM model = new MissingTransactionFromWestPacVM();
#if DEBUG
            model.TerminalID = "72946425";
#endif
            model.TransactionStartDate = DateTime.Now.ToString("dd-MM-yyyy");
            model.TransactionEndDate = DateTime.Now.ToString("dd-MM-yyyy");
            model.FileDate = DateTime.Now.ToString("dd-MM-yyyy");
            model.BatchID = DateTime.Now.ToString("ddMM");
            return View("~/Views/MissingTransaction/MissingTransactionFromWestPac.cshtml", model);
        }

        /// <summary>
        /// Get missing transaction from westpac and display to UI for reviewing
        /// </summary>
        /// <param name="searchObject">MissingTransactionFromWestPacVM</param>
        /// <returns>the list of missing transactions</returns>
        public JsonResult GetMissingTransactionFromWestPac([FromBody] MissingTransactionFromWestPacVM searchObject)  //Gets the QBR history
        {
            using (var entities = new TaxiEpayEntities())
            {
                entities.SetCommandTimeOut(1800);

                var firstDateStr = searchObject.TransactionStartDate;
                var dateformat = "dd-MM-yyyy";
                var firstDate = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");
                var endDateStr = searchObject.TransactionEndDate;
                var endDate = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");

                var totalRecords = entities.Database.SqlQuery<TransactionFromBankVM>(
                    $"EXEC [CheckTransactionsFromBanktoCadmus_WBC] '{searchObject.TerminalID}','0','0','{searchObject.BatchSequence}','{firstDate} 00:00:00.000','{endDate} 23:59:59.000',0");
                int totalRecordCount = totalRecords.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecordCount / (float)searchObject.PageSize);
                var skip = (searchObject.CurrentPage - 1) * searchObject.PageSize;
                var lsttotalRecords = totalRecords.Skip(skip).Take(searchObject.PageSize).ToList();
                var res = lsttotalRecords.Select(n => new
                {
                    id = n.id,
                    Startshift = n.Startshift.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    endshift = n.endshift.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    n.terminalnumber,
                    n.transpan,
                    transstart = n.transstart.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    n.TransFare,
                    n.TransExtras,
                    n.TransTotalAmount
                });
                var jsonData = new
                {
                    total = totalPages,
                    page = searchObject.CurrentPage,
                    records = totalRecordCount,
                    rows = res
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Approved missing transaction from westpac.
        /// </summary>
        /// <param name="searchObject">MissingTransactionFromWestPacVM</param>
        /// <returns>Result after perform approved transaction</returns>
        public JsonResult ApprovedMissingTransactionFromWestPac([FromBody] MissingTransactionFromWestPacVM searchObject)  //Gets the QBR history
        {
            try
            {
                using (var entities = new TaxiEpayEntities())
                {
                    try
                    {
                        entities.SetCommandTimeOut(1800);
                        var firstDateStr = searchObject.TransactionStartDate;
                        var dateformat = "dd-MM-yyyy";
                        var firstDate = DateTime.ParseExact(firstDateStr, dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");
                        var endDateStr = searchObject.TransactionEndDate;
                        var endDate = DateTime.ParseExact(endDateStr, dateformat, new CultureInfo("en-GB")).ToString("yyyy-MM-dd");

                        var fileDateStr = searchObject.FileDate;
                        var fileDate = DateTime.ParseExact(fileDateStr, dateformat, new CultureInfo("en-GB"));

                        var westpacFileName = $"TaxiPay000000{fileDate.ToString("yyyyMMdd")}E.csv";

                        var totalRecords = entities.Database.SqlQuery<TransactionFromBankVM>(
                            $"EXEC [CheckTransactionsFromBanktoCadmus_WBC] '{searchObject.TerminalID}','0','0','{searchObject.BatchSequence}','{firstDate} 00:00:00.000','{endDate} 23:59:59.000',0,'{searchObject.BatchID}','{fileDate.ToString("yyyy-MM-dd")}','{westpacFileName}'");
                        var selectedId = searchObject.SelectedTransactionIds.Split(',');
                        var res = totalRecords.Where(n => selectedId.Contains(n.id.ToString()));
                        if (!res.Any())
                        {
                            var jsonData = new
                            {
                                IsApproved = false,
                                Message = "The Batch Sequence already used for Current Batch Number, please input another one."
                            };
                            return Json(jsonData, JsonRequestBehavior.AllowGet);
                        }
                        var dic = new Dictionary<long, CadmusIN>();
                        foreach (var re in res)
                        {
                            dic.Add(re.id, re.ToCadmusIN());
                        }

                        entities.CadmusINs.AddRange(dic.Select(n => n.Value));
                        entities.SaveChanges();

                        foreach (var cadMusIn in dic)
                        {
                            var cadmusInId = cadMusIn.Value.id;
                            var westpacId = cadMusIn.Key;
                            var docketKey = entities.Database.SqlQuery<int>(
                                $"EXEC [_Docket_ImportDocketFromTranHost] @CadmusinID={cadmusInId}").FirstOrDefault();

                            entities.Database.ExecuteSqlCommand(
                                $"UPDATE WestPacIN SET docket_key={docketKey} WHERE id={westpacId}");
                        }
                        entities.SaveChanges();
                        var jsonResponse = new
                        {
                            IsApproved = true,
                            Message = string.Empty
                        };
                        return Json(jsonResponse, JsonRequestBehavior.AllowGet);

                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message);
                var jsonData = new
                {
                    IsApproved = false,
                    Message = "Please select missing transaction"
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
