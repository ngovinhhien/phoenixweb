﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhoenixObjects.Common;
using Phoenix_BusLog.Data;
using PhoenixObjects.Member;
using PhoenixObjects.Administrator;
using PhoenixObjects.UserAuthentication;
using PhoenixObjects.Terminal;
using PhoenixObjects.Financial;
using PhoenixWeb.Models.ViewModel;
using System.Web.Security;
using Phoenix_BusLog;
using PhoenixObjects.QBR;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class AjaxAdminController : PhoenixWebController
    {
        public JsonResult GetCountries()
        {
            CountryDA cda = new CountryDA();

            var countries = cda.Get().ToList();
            countries.Add(new CountryModel
            {
                CountryKey = 0,
                CountryName = "Select Country"
            });
            return Json(countries.OrderBy(c => c.CountryKey), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSearchFields()
        {
            MemberDA mda = new MemberDA();
            var SearchFieldList = mda.SearchField(string.Empty, string.Empty).ToList();
            return Json(SearchFieldList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteSPMemberSearch(string term, bool isActive)
        {
            List<string> m = new List<string>();
            MemberDA mda = new MemberDA();
            var model = mda.SPMemberSearch(term, isActive).ToList();
            foreach (var p in model)
            {
                m.Add(p);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetRolePermission(Guid RoleId)
        {
            Session["PerRoleId"] = RoleId;
            PermissionsDA mda = new PermissionsDA();
            List<PermissionModel> model = mda.GetAllPermissionListByRole(RoleId).ToList();
            return Json(model.OrderBy(p=>p.PermissionName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoleUsers(Guid RoleId)
        {
            UserDA mda = new UserDA();
            var us = mda.GetUsersByRole(RoleId).ToList();
            List<UserModelVM> model = new List<UserModelVM>();
            foreach(var u in us)
            {
                UserModelVM m = new UserModelVM();
                m.UserId = u.UserId;
                m.UserName = u.UserName;
                    model.Add(m);
            }
            return Json(model.OrderBy(u=>u.UserName), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult AutoCompleteGeneralMemberSearch(string term)
       {
            List<string> m = new List<string>();
            MemberDA mda = new MemberDA();
            TerminalDA tda = new TerminalDA();
            string firstchar = string.Empty;
            if (term.Length > 1)
            {
                firstchar = term.Substring(0, 1);
            }
            else
            {
                firstchar = term;
            }

            if (firstchar == "1" || firstchar == "3" || firstchar == "7" || firstchar == "8")
            {
                var model = tda.GetAssignedEftTerminals(term).ToList();
                foreach (var p in model)
                {
                    m.Add(p);
                }
            }
            else
            {
                var model = mda.GetMemberByTradingName(term).ToList();
                foreach (var p in model)
                {
                    m.Add(p);
                }
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDefaultRates(int CompanyKey)
        {
            CommissionRateDA cda = new CommissionRateDA();
            DefaultRateModel drt = new DefaultRateModel();
            List<DefaultRateVM> drvmList = new List<DefaultRateVM>();

            //remove hardcode vehiclekey when getting default commissionrate
            VehicleTypeDA vht = new VehicleTypeDA();
            int defaultvehicletypekey = vht.GetVehicleTypeKey(CompanyKey);
            if (defaultvehicletypekey > 0)
                drt = cda.GetDefaultCommissionRate(CompanyKey, defaultvehicletypekey); //

            
            //if (CompanyKey == 18)
            //{
            //    drt = cda.GetDefaultCommissionRate(CompanyKey, 19);
            //}
            //else if (CompanyKey == 1)
            //{
            //    drt = cda.GetDefaultCommissionRate(CompanyKey, 1);
            //}
            //else if (CompanyKey == 19)
            //{
            //    drt = cda.GetDefaultCommissionRate(CompanyKey, 20);
            //}
            //else if (CompanyKey == 20)
            //{
            //    drt = cda.GetDefaultCommissionRate(CompanyKey, 21);
            //}                  

            return Json(drt, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDealersByCompany(int CompanyKey)
        {
            DealerDA dda = new DealerDA();          
            List<DealerModel> drvmList = new List<DealerModel>();
            drvmList = dda.GetDealersByCompanykey(CompanyKey);

            return Json(drvmList.OrderBy(c => c.DealerName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPromotionsByCompany(int CompanyKey)
        {
            LoyaltyPromotionDA pda = new LoyaltyPromotionDA();
            List<LoyaltyPromotionModel> promotionList = new List<LoyaltyPromotionModel>();
            promotionList = pda.GetPromotionsByCompanykey(CompanyKey);

            return Json(promotionList.OrderBy(c => c.PromotionName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDefaultAllocationDate(string TerminalId)
        {
            EFTDocketDA cda = new EFTDocketDA();
            TerminalAllocationDateVM mod = new TerminalAllocationDateVM();

            string FirstUnallocatedDate = cda.GetFirstUnallocatedDate(TerminalId);
            DateTime d = Convert.ToDateTime(FirstUnallocatedDate);
            DateTime dd = d.Date;
            TimeSpan tt = d.TimeOfDay;
            //mod.dDate = dd.Day.ToString() + "-" + dd.Month.ToString() + "-" + dd.Year.ToString();
            mod.dDate = dd.Date.ToString("yyyy-MM-dd");
            var h = tt.Hours;
            var m = tt.Minutes;
            var s = tt.Seconds;
            //var u = h.ToString() + ":" + m.ToString() + ":" + s.ToString();
            //mod.tTime = u;
            mod.tActual = "Actual Assign date : "+ d.ToShortDateString() + ":" + d.ToShortTimeString();
            mod.EffectiveDateString = d.ToString("s");
            //string FirstUnallocatedDate = FirstUnallocatedDate.ToString("s");
            //string dobDate = FirstUnallocatedDate
            //string[] dobDateArry = dobDate.Split('/');
            //model.EffectiveDateString = dobDateArry[2].PadLeft(4, '0') + "-" + dobDateArry[1].PadLeft(2, '0') + "-" + dobDateArry[0].PadLeft(2, '0');
            //model.EffectiveDateString = DateTime.Now.ToString("s");

            return Json(mod, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult ShouldUserMassScan()
        //{
        //    var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
        //    Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
        //    UserDA rda = new UserDA();
        //    UserProfileModel user = rda.GetUsers(uId);
        //    if (user.OfficeType_Key == 4 || user.OfficeType_Key == 3)
        //    {
        //        return Json("success", JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        if(User.IsInRole("Administrator"))
        //        {
        //            return Json("success", JsonRequestBehavior.AllowGet);
        //        }
        //        return Json("Error", JsonRequestBehavior.AllowGet);
        //    }

        //}

        public JsonResult GetTerminalStatus()
        {
            List<TerminalStatus> stats = new List<TerminalStatus>();          
            stats.Add(new TerminalStatus
            {
                Id  = "STOCK",
                Name = "STOCK"
            });
            stats.Add(new TerminalStatus
            {
                Id= "RETURNED",
                Name = "RETURNED"
            });
            stats.Add(new TerminalStatus
            {
                Id = "LIVE",
                Name = "LIVE"
            });
            stats.Add(new TerminalStatus
            {
                Id = "DAMAGED",
                Name = "DAMAGED"
            });
            stats.Add(new TerminalStatus
            {
                Id = "TERMINATED",
                Name = "TERMINATED"
            });
            stats.Add(new TerminalStatus
            {
                Id = "UNKNOWN",
                Name = "UNKNOWN"
            });
           
            return Json(stats, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberRentalTemplate(int company_key)
        {
            MemberRentalTemplateDA cda = new MemberRentalTemplateDA();
            Session["RateListToSave"] = null;
            var MemberRentalTemplateList = cda.GetByCompanyKey(company_key).ToList();
            MemberRentalTemplateList.Add(new MemberRentalTemplateModel
            {
                MemberRentalTemplateKey = 0,
                Description = "Select Member Rental Template"
            });
            return Json(MemberRentalTemplateList.OrderBy(c => c.MemberRentalTemplateKey), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTerminalRentalRateList(int? MemberRentalTemplateKey)
        {
            var session = Session["RateListToSaveConfig"];
            var config = new RateListToSaveConfig();
            var cda = new TerminalRentalRateDA();
            var terminalRentalTemplateList = new List<TerminalRentalRateModel>();

            if (session != null)
            {
                config = (RateListToSaveConfig)session;
                terminalRentalTemplateList = cda.GetByMemberRentalTemplateKey(config.RentalMemberRentalTemplateKey).ToList();

                if (terminalRentalTemplateList != null && terminalRentalTemplateList.Any())
                {
                    LogInfo($"Load rental data by session data with RentalMemberRentalTemplateKey = {config.RentalMemberRentalTemplateKey}");

                    foreach (var item in terminalRentalTemplateList)
                    {
                        var newRentAmount = config.TerminalRentalRateList?
                            .FirstOrDefault(x => x.MemberRentalTemplateKey == item.MemberRentalTemplateKey &&
                                                 x.TerminalRentalRate_key == item.TerminalRentalRate_key)?
                            .RentAmount;

                        if (item.RentAmount != newRentAmount)
                        {
                            item.NewRentAmount = newRentAmount;
                        }
                    }
                }

                LogInfo($"Clear RateListToSaveConfig Session.");
                Session["RateListToSaveConfig"] = null;
            }
            else
            {
                LogInfo($"Load rental data with RentalMemberRentalTemplateKey = {MemberRentalTemplateKey}");
                terminalRentalTemplateList = cda.GetByMemberRentalTemplateKey(MemberRentalTemplateKey.GetValueOrDefault(0)).ToList();
            }

            Session["RateListToSave"] = null;
            Session["RateListToSave"] = terminalRentalTemplateList;

            LogInfo($"Return the terminalRentalTemplateList.");
            return Json(terminalRentalTemplateList.OrderBy(c => c.RentAmount), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBusinessType()
        {
            BusinessTypeDA cda = new BusinessTypeDA();

            var businessType = cda.Get().ToList();
            businessType.Add(new BusinessTypeModel
            {
                BusinessTypeKey = 0,
                BusinessTypeName = "Select Business Type"
            });
            return Json(businessType.OrderBy(c => c.BusinessTypeKey), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleType()
        {
            VehicleTypeDA cda = new VehicleTypeDA();
            var VehicleType = cda.Get().ToList();
           
            return Json(VehicleType.OrderBy(c => c.VehicleType_key), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOffice()
        {
            OfficeDA oda = new OfficeDA();
            var office = oda.GetOffices().ToList();
            office.Add(new OfficeModel
            {
                Name = "Select Office",
                Office_key = 0
            });
            return Json(office.OrderBy(c => c.Office_key), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUsers()
        {
            UserDA oda = new UserDA();
            var users = oda.GetUsers().ToList();
            users.Add(new UserProfileModel
            {
                UserName = "Select Users",
                UserId = Guid.Empty
            });
            return Json(users.OrderBy(c => c.UserName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserRoles(string Userid)
        {
            UserDA rda = new UserDA();
            Guid Id = Guid.Parse(Userid);
            List<string> roles = rda.GetUsers(Id).UsersRoleList;
            List<RoleModel> rols = new List<RoleModel>();
            foreach(string s in roles)
            {
                RoleModel rw = new RoleModel();
                rw.RoleId = Guid.NewGuid();
                rw.RoleName = s;
                rols.Add(rw);
            }
            return Json(rols, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUserFreshDeskCred(string Userid)
        {
            FreshDeskCredDA rda = new FreshDeskCredDA();
            FreshDeskCredModel model = new FreshDeskCredModel();
            Guid Id = Guid.Parse(Userid);
             model = rda.GetUserFreshDeskCred(Id);

             return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRoles()
        {
            RolesDA rda = new RolesDA();
            var roles = rda.GetRoles().ToList();
            roles.Add(new RoleModel
            {
                RoleName = "Select Role",
                RoleId = Guid.Empty
            });
            return Json(roles.OrderBy(c => c.RoleId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDealers()
        {
            DealerDA cda = new DealerDA();
            var dealer = cda.Get().ToList();
            dealer.Add(new MemberModel
            {
                Member_key = 0,
                TradingName = "Select Dealer"
            });
            return Json(dealer.OrderBy(c => c.Member_key), JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult GetRentalTurnoverRange()
        {
            RentalTurnOverDA cda = new RentalTurnOverDA();
            var RentalTurnoverRange = cda.Get().ToList();
            RentalTurnoverRange.Add(new RentalTurnOverModel
            {
                RentalTurnoverRangeKey = 0,
               RangeMsg = "Please select turnover range"
            });
            return Json(RentalTurnoverRange.OrderBy(c => c.RentalTurnoverRangeKey), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStates()
        {

            StateDA cda = new StateDA();
            var states = cda.GetStateAustralia().ToList();
            //states.Add(new StateModel
            //{
            //    StateKey = 0,
            //    StateName = "Select State"
            //});
            return Json(states.OrderBy(c => c.StateKey), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEFTTerminalId(int termId)
        {
            TerminalDA cda = new TerminalDA();
            string teId = termId.ToString();
            var states = cda.CheckTerminalExists(teId);
            return Json(states.TerminalId, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubBusinessType(int companyKey)
        {
            VehicleTypeDA cda = new VehicleTypeDA();
            var subbusinesstypes = cda.Get(companyKey);
            return Json(subbusinesstypes, JsonRequestBehavior.AllowGet);
        }



    }
}
