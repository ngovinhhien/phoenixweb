using log4net;
using System;
using System.Text;

namespace PhoenixWeb.Controllers
{
    public abstract class PhoenixWebController : PhoenixController
    {
        private ILog logger = LogManager.GetLogger("PhoenixWebController");
        protected string CurrentAction { get { return this.ControllerContext.RouteData.Values["action"].ToString(); } }
        protected string CurrentController { get { return this.ControllerContext.RouteData.Values["controller"].ToString(); } }

        public PhoenixWebController()
        {
            
        }


        protected void LogInfo(string info)
        {
            logger.Info(String.Format("Log in\fo for Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, info));
        }
        protected void LogError(string errormessage)
        {
            logger.Error(String.Format("Error processing Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, errormessage));
        }

        protected void LogError(Exception ex)
        {
            logger.Error(GetAllException(ex));
        }

        protected void LogWarning(string warning)
        {
            logger.Warn(String.Format("Warning processing Controller {0}, Action {1}. Info message: {2}", CurrentController, CurrentAction, warning));
        }

        protected string GetAllException(Exception ex)
        {
            var error = new StringBuilder();
            while (ex != null)
            {
                error.AppendLine(ex.Message);
                error.AppendLine(ex.StackTrace);
                ex = ex.InnerException;
            }

            return error.ToString();
        }
    }
}
