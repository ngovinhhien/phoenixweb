﻿using System;
using System.Linq;
using System.Web.Mvc;
using PhoenixWeb.Models.ViewModel;
using Phoenix_BusLog.Data;
using log4net;
using Newtonsoft.Json;
using System.IO;
using ExcelDataReader;
using System.Data;
using System.Collections.Generic;
using System.Web;
using PhoenixWeb.Helper;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class GroupController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetMerchantGroup(VMBaseWithPaging basicSearch)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.GetMerchantGroupAndCountTheNumberOfMerchant(basicSearch.PageSize, basicSearch.CurrentPage, basicSearch.SearchText);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMerchantGroupDetail(GroupMerchantDetailVM groupMerchantDetail)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.GetMerchantGroupDetail(groupMerchantDetail.GroupID, groupMerchantDetail.PageSize, groupMerchantDetail.CurrentPage, groupMerchantDetail.SearchText);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchMerchantAutoComplete(string term)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.GetMerchantAutoComplete(term);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddMerchantToGroup(int groupID, string merchantKey)
        {
            if (string.IsNullOrEmpty(merchantKey))
            {
                return Json(new { Message = "Please enter merchant id." }, JsonRequestBehavior.AllowGet);
            }

            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.AddMerchantToGroup(groupID, merchantKey.Split(';').Select(x => x.Trim()).Distinct().ToList(), User.Identity.Name);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportMerchant()
        {
            try
            {
                if (Request.Files == null || Request.Files.Count == 0)
                {
                    return Json(new { Message = "Please select a merchant file to import." }, JsonRequestBehavior.AllowGet);
                }

                if (!int.TryParse(Request.Params["GroupId"], out int groupId))
                {
                    return Json(new { Message = $"Could not find group with ID = {groupId}." }, JsonRequestBehavior.AllowGet);
                }

                //get file data
                var merchantFile = Request.Files[0];
                var extension = Path.GetExtension(merchantFile.FileName).ToLower();

                var reader = ExcelDataReaderHelper.CreateDataReaderByExtensionFile(extension, merchantFile.InputStream);

                if (reader == null)
                {
                    return Json(new { Message = "This file format is not supported." }, JsonRequestBehavior.AllowGet);
                }

                DataSet ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });

                reader.Close();

                string[] defaultColumns = { "Merchant ID" };
                var currentColumnsInFile = ds.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower());

                var columns = defaultColumns.Select(x => x.ToLower()).Except(currentColumnsInFile).ToList();

                if (columns.Count > 0)
                {
                    return Json(new { Message = $"Could not find default colums in file: {string.Join("; ", defaultColumns)}." }, JsonRequestBehavior.AllowGet);
                }

                List<string> merchantIds = new List<string>();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var merchantId = row[defaultColumns[0]].ToString();

                    if (!string.IsNullOrEmpty(merchantId))
                    {
                        merchantIds.Add(merchantId);
                    }
                };

                if (merchantIds.Count == 0)
                {
                    return Json(new { Message = "Please enter merchant id into file." }, JsonRequestBehavior.AllowGet);
                }
                else if (merchantIds.Count > 1000)
                {
                    return Json(new { Message = "You only can import merchants not exceed 1000 records." }, JsonRequestBehavior.AllowGet);
                }

                MerchantGroupDA mgDA = new MerchantGroupDA();
                var jsonData = mgDA.AddMerchantToGroup(groupId, merchantIds.Distinct().ToList(), User.Identity.Name);

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Message = "System error." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveMerchantOutOfGroup(int merchantID, int groupID)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.RemoveMerchantOutOfGroup(groupID, merchantID, User.Identity.Name);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveGroup(int groupID)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.DeleteGroup(groupID);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddGroupAndCampaign(QantasPointEarnedRateVM qantasPointEarnedRateVM)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var group = qantasPointEarnedRateVM.ToGroup();
            group.CreatedBy = group.UpdatedBy = User.Identity.Name;
            var qbrExchangeRate = qantasPointEarnedRateVM.ToQbrExchangeRateFromGroup();
            qbrExchangeRate.CreatedBy = qbrExchangeRate.UpdatedBy = User.Identity.Name;

            var jsonData = mgDA.AddGroupAndCampaign(group, qbrExchangeRate);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditGroupAndCampaign(QantasPointEarnedRateVM qantasPointEarnedRateVM)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var group = qantasPointEarnedRateVM.ToGroup();
            group.UpdatedBy = User.Identity.Name;
            var qbrExchangeRate = qantasPointEarnedRateVM.ToQbrExchangeRateFromGroup();
            qbrExchangeRate.UpdatedBy = User.Identity.Name;

            var jsonData = mgDA.EditGroupAndCampaign(group, qbrExchangeRate);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupAndExchangeRateInformation(int groupID)
        {
            MerchantGroupDA mgDA = new MerchantGroupDA();
            var jsonData = mgDA.GetGroupAndExchangeRateInformation(groupID);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}