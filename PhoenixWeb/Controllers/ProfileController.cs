﻿using Phoenix_BusLog.Data;
using PhoenixObjects.User;
using PhoenixObjects.UserAuthentication;
using PhoenixWeb.ActionFilters;
using PhoenixWeb.Models.Contants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PhoenixWeb.Controllers
{
     [Authorize]
    public class ProfileController : PhoenixWebController
    {
        //
        // GET: /Profile/

        public ActionResult Index()
        {
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            UserDA uda = new UserDA();
            UserProfileModel model = uda.GetUsers(uId);
            
            //UserFreshDeskAuthModel userFreshDeskAuth = uda.Get((Guid)Membership.GetUser().ProviderUserKey);
            UserFreshDeskAuthModel userFreshDeskAuth = uda.Get(uId);
            if (userFreshDeskAuth != null)
            {
                model.FreshDeskAPI = userFreshDeskAuth.FreshDeskAPIKey;
            }
            return View(model);
        }

        //
        // GET: /Profile/Details/5

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection c)
        {
            try
            {
                string OldPassword = c["OldPassword"].ToString();
                string NewPassword = c["NewPassword"].ToString();

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                membershipProvider.UnlockUser(User.Identity.Name);
                var IsChanged = membershipProvider.ChangePassword(User.Identity.Name, OldPassword, NewPassword);
                membershipProvider.UnlockUser(User.Identity.Name);
                if (IsChanged)
                {
                    //log reset password history
                    UserDA da = new UserDA();
                    da.SaveResetPasswordHistory(User.Identity.Name, ResetPasswordSourceConstants.SourceNameOfChangePasswordInProfileScreen, User.Identity.Name);
                }

                ViewBag.Msg = "Password Changed!";
                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }

        }

     
        [HttpPost]
        public ActionResult SaveFreshDeskAPI(UserProfileModel model)  //Gets the QBR Point history
        {
            using (UserDA userDA = new UserDA())
            {
                userDA.Save(new UserFreshDeskAuthModel() { FreshDeskAPIKey = model.FreshDeskAPI, UserId = model.UserId });               
            }
            return RedirectToAction("Index");
        }
    }
}
