﻿using Phoenix_BusLog.Data;
using Phoenix_Service;
using PhoenixObjects.Docket;
using PhoenixWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    public class DocketController : PhoenixWebController
    {
       [RBAC(Permission = "UnallocateDockets")]
        public ActionResult UnallocatedDockets()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetUnallocatedDockets(PhoenixObjects.Common.GridModel<SearchUnallocatedDocketModel, List<UnallocatedDocketModel>> model)
        {
            using (var unallocateDocketDA = new UnallocateDocketDA())
            {
                unallocateDocketDA.GetUnllocateDockets(model);
            }
            return new CustomJsonResult
            {
                Data = model
            };
        }
        [HttpPost]
        public JsonResult GetTerminalAllocations(PhoenixObjects.Common.GridModel<SearchUnallocatedDocketModel, List<TerminalAllcationModel>> model)
        {
            using (var unallocateDocketDA = new UnallocateDocketDA())
            {
                unallocateDocketDA.GetTerminalAllocations(model);
            }
            return new CustomJsonResult
            {
                Data = model
            };
        }
        [HttpPost]
        public JsonResult AllocateDocket(AllocateDocketModel model)
        {
            var result = string.Empty;
            using (var unallocateDocketDA = new UnallocateDocketDA())
            {
                result = unallocateDocketDA.AllocateDocket(model);
            }
            return Json(result);
        }
    }
}
