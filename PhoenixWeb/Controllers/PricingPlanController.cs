﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelperSharp;
using LGHelper.PhoenixWebAPI;
using Newtonsoft.Json;
using PhoenixWeb.Models;
using PhoenixWeb.Models.APIModels;
using PhoenixWeb.Models.ViewModel;
using PhoenixWeb.Models.ViewModel.FraudMonitoring;
using PhoenixWeb.Models.ViewModel.PricingPlan;

namespace PhoenixWeb.Controllers
{
    [RBAC(Permission = "ConfigurePricingPlan")]
    public class PricingPlanController : Controller
    {
        private List<SelectListItem> RateOption = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Dollar value ($)", Value = "true" },
            new SelectListItem() { Text = "Percentage (%)", Value = "false" },
        };

        public ActionResult ManagePricingPlan()
        {
            return View("ManagePricingPlan");
        }

        /// <summary>
        /// This is function loading pricing plan list from PhoenixAPI. It's called from PH-ManagePricingPlanService.js
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult LoadPricingPlans(FilterModel filter)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["sortField"] = filter.SortField;
            query["sortOrder"] = filter.SortOrder;
            query["pageSize"] = filter.PageSize.ToString();
            query["pageIndex"] = filter.PageIndex.ToString();
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var resultModel = client.Get<DataSearchResponse<PricingPlan>>(PhoenixWebAPIEndpoint.PRICINGPLAN_GET_LIST + uriBuilder.Query.ToString());
            return Json(resultModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreatePlan()
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var model = client
                .Get<PricingPlanModel>(
                    PhoenixWebAPIEndpoint.PRICINGPLAN_LOAD_PRICING_PLAN + uriBuilder.Query.ToString()).Data;
            var pricingPlanVM = new PricingPlanVM();
            pricingPlanVM.Rates = model.PayMethods;
            pricingPlanVM.BusinessTypes = model.BusinessTypes;
            pricingPlanVM.SelectedBusinessTypeKey = model.BusinessTypes.FirstOrDefault(n => n.IsSelected) == null ? 0 : model.BusinessTypes.FirstOrDefault(n => n.IsSelected).BusinessTypeKey;
            pricingPlanVM.PlanId = -1;

            return View(pricingPlanVM);
        }

        [HttpPost]
        public ActionResult SavePlan(PricingPlanVM formCollection)
        {
            var subbusinesses = formCollection.SubBusinessTypeIDs.Select(n => new
            {
                SubBusinessTypeKey = n
            });
            var parameters = new 
            {
                PlanId = formCollection.PlanId,
                PlanName = formCollection.PlanName,
                BusinessTypeKey = formCollection.SelectedBusinessTypeKey,
                CreatedBy = HttpContext.User.Identity.Name,
                SubBusinessTypes = subbusinesses.ToList(),
                Rates = formCollection.Rates,
            };

            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            if (formCollection.PlanId > 0)
            {
                var resultModel = client.Put<object>(PhoenixWebAPIEndpoint.PRICINGPLAN_ADD_EDIT_PRICING_PLAN, parameters);
                return Json(resultModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var resultModel = client.Post<object>(PhoenixWebAPIEndpoint.PRICINGPLAN_ADD_EDIT_PRICING_PLAN, parameters);
                return Json(resultModel, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditPlan(int PlanID)
        {
            var uriBuilder = new UriBuilder();
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["phwUserName"] = User.Identity.Name;
            uriBuilder.Query = query.ToString();
            PhoenixWebRestAPI client = new PhoenixWebRestAPI();
            var model = client
                .Get<PricingPlanModel>(
                    $"{PhoenixWebAPIEndpoint.PRICINGPLAN_LOAD_PRICING_PLAN}/{PlanID}" + uriBuilder.Query.ToString()).Data;
            var pricingPlanVM = new PricingPlanVM();
            pricingPlanVM.Rates = model.PayMethods;
            pricingPlanVM.BusinessTypes = model.BusinessTypes;
            var selectedBusinessType = model.BusinessTypes.FirstOrDefault(n => n.IsSelected);
            pricingPlanVM.SelectedBusinessTypeKey = selectedBusinessType?.BusinessTypeKey ?? 0;
            pricingPlanVM.PlanId = PlanID;
            pricingPlanVM.PlanName = model.PlanName;

            return View(pricingPlanVM);
        }
    }
}