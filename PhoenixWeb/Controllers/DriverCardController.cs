﻿using System;
using System.Linq;
using System.Web.Mvc;
using Phoenix_BusLog.Data;
using Newtonsoft.Json;
using PhoenixObjects.Common;
using PhoenixWeb.Common;
using PhoenixObjects.Member.DriverCard;
using LGHelper;
using RestSharp;
using PhoenixWeb.Models.ViewModel.DriverCard;
using Phoenix_BusLog;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Collections.Generic;
using Phoenix_Service;
using System.Data.SqlClient;
using System.Data;
using PhoenixObjects.DriverCard;
using log4net;
using PhoenixObjects.Enums;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class DriverCardController : Controller
    {
        private void CreateHistory(string actionDescription, string note, int cardId, int memberKey)
        {
            DriverCardHistoryDA.SaveHistory(new DriverCardHistoryModel
            {
                ActionBy = User.Identity.Name,
                ActionDescription = actionDescription,
                Date = DateTime.Now,
                Note = note,
                DriverCardId = cardId,
                MemberKey = memberKey,
            });
        }

        private string ReloadDriverCards(int memberKey)
        {
            DriverCardDA dcda = new DriverCardDA();
            var cards = dcda.GetByMemberKey(memberKey);
            string driverCardJson = string.Empty;
            if (cards.Count > 0)
            {
                driverCardJson = JsonConvert.SerializeObject(cards);
            }
            return driverCardJson;
        }

        private JsonResult CreateResponseResult(bool isError, string message, string data = null)
        {
            return new CustomJsonResult()
            {
                Data = new
                {
                    IsError = isError,
                    Message = message,
                    DriverCardJson = data,
                },
            };
        }

        private JsonResult VerifyCardCloseStatus(string accountIdentifier, string action)
        {
            OxygenAPI oxygenAPI = new OxygenAPI();
            var statusResponse = oxygenAPI.GetCardStatus(accountIdentifier);
            if (statusResponse != null)
            {
                if (statusResponse.statusCode == "SD")
                {
                    return CreateResponseResult(true, $"This card is in CLOSED status and cannot be used to {action}.");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return CreateResponseResult(true, "Cannot get card status.");
            }
        }

        [HttpPost]
        public JsonResult ReissueCard(int memberKey, string oldAccountIdentifier, string newAccountIdentifier)
        {
            try
            {
                var closeResponse = VerifyCardCloseStatus(newAccountIdentifier, "Reissue");
                if (closeResponse != null)
                {
                    return closeResponse;
                }
                DriverCardDA dcda = new DriverCardDA();
                OxygenAPI oxygenAPI = new OxygenAPI();

                var cardStatus = dcda.GetCardStatusByIdentifier(newAccountIdentifier);
                if (!string.IsNullOrEmpty(cardStatus))
                {
                    return CreateResponseResult(true, $"This card is in {cardStatus} status and cannot be used to reissue.");
                }

                var response = oxygenAPI.ReissueCard(oldAccountIdentifier, newAccountIdentifier);
                if (response.statusCode == null)
                {
                    DriverCardStatusDA dsda = new DriverCardStatusDA();
                    var closeStatus = dsda.GetStatus("CLOSED");

                    dcda.UpdateCardStatus(oldAccountIdentifier, closeStatus.StatusCode);
                    var oldCard = dcda.GetByAccessIdentifier(oldAccountIdentifier);
                    CreateHistory("Close Card", "Close card due to reissue.", oldCard.ID, memberKey);

                    var issuedStatus = dsda.GetIssuedStatus();
                    var newCardId = dcda.Register(newAccountIdentifier, issuedStatus.ID, memberKey);
                    CreateHistory("Register Card", "Register Card due to reissue", newCardId, memberKey);
                    return CreateResponseResult(false, "Card has been reissued successfully.", ReloadDriverCards(memberKey));
                }
                else
                {
                    string errorMessage = response.description;
                    if (response.statusCode == "500")
                    {
                        errorMessage = "Error happened when trying to reissue card.";
                    }
                    return CreateResponseResult(true, errorMessage);
                }
            }
            catch (Exception e)
            {
                return CreateResponseResult(true, e.Message);
            }
        }

        [HttpPost]
        public JsonResult RegisterCard(DriverCardInfoVM model)
        {
            try
            {
                var closeResponse = VerifyCardCloseStatus(model.accountIdentifier, "Register");
                if (closeResponse != null)
                {
                    return closeResponse;
                }
                DriverCardDA dcda = new DriverCardDA();
                var cardStatus = dcda.GetCardStatusByIdentifier(model.accountIdentifier);
                if (!string.IsNullOrEmpty(cardStatus))
                {
                    return CreateResponseResult(true, $"This card is in {cardStatus} status and cannot be used to register.");
                }
                if (model.postCode == null)
                {
                    return CreateResponseResult(true, "Please input Post Code.");
                }
                string[] detailAddress = model.postCode.Split(',');
                if (detailAddress.Length < 3)
                {
                    return CreateResponseResult(true, "Please input postal code, state, city.");
                }

                if (model.DOB == DateTime.MinValue)
                {
                    return CreateResponseResult(true, "Invalid date of birth.");
                }

                string postCode = detailAddress[0].Trim();
                string state = detailAddress[1].Trim();
                string suburb = detailAddress[2].Trim();

                List<string> missingFields = new List<string>();
                if (string.IsNullOrEmpty(model.firstName))
                {
                    missingFields.Add(" First Name");
                }
                if (string.IsNullOrEmpty(model.lastName))
                {
                    missingFields.Add(" Last Name");
                }
                if (string.IsNullOrEmpty(model.mobile))
                {
                    missingFields.Add(" Mobile");
                }
                if (string.IsNullOrWhiteSpace(model.address))
                {
                    missingFields.Add(" Address Line");
                }
                string error = string.Join(",", missingFields);
                if (!string.IsNullOrWhiteSpace(error))
                {
                    if (missingFields.Count > 1)
                    {
                        error += " are required.";
                    }
                    else
                    {
                        error += " is required.";
                    }
                    return CreateResponseResult(true, error);
                }

                OxygenAPI oxygenAPI = new OxygenAPI();
                var response = oxygenAPI.RegisterCard(model, postCode, state, suburb);
                var oxygenErrorCodes = new string[] { "500", "12", "ID" };
                if (response != null)
                {
                    if (response.statusCode == null)
                    {
                        DriverCardStatusDA dsda = new DriverCardStatusDA();
                        var issuedStatus = dsda.GetIssuedStatus();
                        int cardId = dcda.Register(model.accountIdentifier, issuedStatus.ID, model.memberKey);

                        CreateHistory("Register Card", "Register Card", cardId, model.memberKey);
                        return CreateResponseResult(false, "Card has been registered successfully.", ReloadDriverCards(model.memberKey));
                    }
                    else
                    {
                        string errorMessage = response.description;
                        if (response.statusCode == "ID")
                        {
                            errorMessage = "Invalid account identifier.";
                        }
                        return CreateResponseResult(true, errorMessage);
                    }
                }
                else
                {
                    return CreateResponseResult(true, "Cannot register driver card.");
                }
            }
            catch (Exception e)
            {
                return CreateResponseResult(true, e.Message);
            }
        }

        [HttpPost]
        public JsonResult ChangeCardStatus(string cardRefNumber, int memberKey, int statusID)
        {
            try
            {
                DriverCardDA dcda = new DriverCardDA();
                DriverCardStatusDA sDA = new DriverCardStatusDA();
                var status = sDA.GetStatus(statusID);
                string statusCode = status.StatusCode;
                string statusName = status.Name;

                var card = dcda.GetInUseCard(cardRefNumber, memberKey);
                if (card != null)
                {
                    string currentStatus = sDA.GetStatus(card.StatusID.GetValueOrDefault()).Name;
                    OxygenAPI oxygenAPI = new OxygenAPI();
                    var response = oxygenAPI.ChangeCardStatus(cardRefNumber, statusCode);
                    if (response == null)
                    {
                        return CreateResponseResult(true, "API call return null response.");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(response.statusCode))
                        {
                            return CreateResponseResult(true, response.description);
                        }
                    }
                    dcda.UpdateCardStatus(cardRefNumber, statusID);
                    CreateHistory($"Update Status", $"Update Status from {currentStatus} to {statusName}", card.ID, memberKey);
                    return new CustomJsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Data = response,
                            DriverCardJson = ReloadDriverCards(memberKey),
                        },
                    };
                }

                return CreateResponseResult(true, "Card is closed");
            }
            catch (Exception e)
            {
                return CreateResponseResult(true, e.Message);
            }
        }

        public JsonResult GetCardInfo(string cardRefNumber, int memberKey)
        {
            try
            {
                DriverCardDA dcda = new DriverCardDA();
                var card = dcda.GetInUseCard(cardRefNumber, memberKey);
                if (card != null)
                {
                    OxygenAPI oxygenAPI = new OxygenAPI();
                    var response = oxygenAPI.GetCardInfo(cardRefNumber);
                    if (response == null)
                    {
                        return CreateResponseResult(true, "API call return null response");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(response.statusCode))
                        {
                            return CreateResponseResult(true, response.description);
                        }
                    }

                    var data = response.ToMemberDriverCardModel();
                    DriverCardStatusDA dcsDA = new DriverCardStatusDA();
                    data.Status = dcsDA.GetStatus(data.StatusCode).Name;
                    StateDA sda = new StateDA();
                    var states = sda.GetStateAustralia();
                    return new CustomJsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Data = data,
                            StateData = states.ToList(),
                        },
                    };
                }

                return CreateResponseResult(true, "Card is closed");
            }
            catch (Exception e)
            {
                return CreateResponseResult(true, e.Message);
            }
        }

        public JsonResult GetCardChangeHistory(int driverCardId, int currentPage, int pageSize)
        {
            DriverCardDA dcda = new DriverCardDA();
            var histories = dcda.GetHistoriesByIdPaging(driverCardId, currentPage, pageSize);
            return new CustomJsonResult()
            {
                Data = new
                {
                    IsError = false,
                    Data = histories,
                },
            };
        }

        [HttpPost]
        public JsonResult UpdateCardProfile(DriverCardInfoVM model)
        {
            try
            {
                List<string> missingFields = new List<string>();
                if (string.IsNullOrEmpty(model.firstName))
                {
                    missingFields.Add(" First Name");
                }
                if (string.IsNullOrEmpty(model.lastName))
                {
                    missingFields.Add(" Last Name");
                }
                if (string.IsNullOrEmpty(model.mobile))
                {
                    missingFields.Add(" Mobile");
                }
                if (string.IsNullOrWhiteSpace(model.address))
                {
                    missingFields.Add(" Address Line");
                }
                if (string.IsNullOrWhiteSpace(model.postCode))
                {
                    missingFields.Add(" Postal Code");
                }
                if (string.IsNullOrWhiteSpace(model.state))
                {
                    missingFields.Add(" State");
                }
                if (string.IsNullOrWhiteSpace(model.city))
                {
                    missingFields.Add(" City");
                }
                if (string.IsNullOrWhiteSpace(model.email))
                {
                    missingFields.Add(" Email");
                }
                string error = string.Join(",", missingFields);
                if (!string.IsNullOrWhiteSpace(error))
                {
                    if (missingFields.Count > 1)
                    {
                        error += " are required.";
                    }
                    else
                    {
                        error += " is required.";
                    }
                    return CreateResponseResult(true, error);
                }

                if (model.DOB.CompareTo(DateTime.Now) > 0)
                {
                    return CreateResponseResult(true, "Date of birth cannot greater than current date.");
                }
                var emailAttribute = new EmailAddressAttribute();
                if (!emailAttribute.IsValid(model.email))
                {
                    return CreateResponseResult(true, "Invalid email format.");
                }
                if (model.DOB == DateTime.MinValue)
                {
                    return CreateResponseResult(true, "Invalid date of birth.");
                }

                OxygenAPI oxygenAPI = new OxygenAPI();
                var response = oxygenAPI.UpdateProfile(model);
                if (response != null)
                {
                    if (response.statusCode == null)
                    {
                        DriverCardDA dcda = new DriverCardDA();
                        var card = dcda.GetInUseCard(model.accountIdentifier, model.memberKey);
                        var changes = model.changedContent.Split(';');
                        StringBuilder content = new StringBuilder();
                        foreach (string c in changes)
                        {
                            content.AppendLine(c);
                        }
                        CreateHistory($"Update Profile", content.ToString(), card.ID, model.memberKey);
                        return CreateResponseResult(false, "Update profile successfully.");
                    }
                    else
                    {
                        string errorMessage = response.description;
                        if (response.statusCode == "ID")
                        {
                            errorMessage = "Invalid account identifier.";
                        }
                        return CreateResponseResult(true, errorMessage);
                    }
                }
                else
                {
                    return CreateResponseResult(true, "Cannot update profile.");
                }
            }
            catch (Exception e)
            {
                return CreateResponseResult(true, e.Message);
            }
        }


        [HttpPost]
        public JsonResult GetCardStatus(string accountIdentifier, int memberKey)
        {
            OxygenAPI oxygenAPI = new OxygenAPI();
            var response = oxygenAPI.GetCardStatus(accountIdentifier);
            if (response != null)
            {
                DriverCardDA dcda = new DriverCardDA();
                DriverCardStatusDA sDA = new DriverCardStatusDA();
                if (response.statusCode == null)
                {
                    var card = dcda.GetInUseCard(accountIdentifier, memberKey);
                    if (card != null)
                    {
                        string currentStatus = sDA.GetStatus(card.StatusID.GetValueOrDefault()).StatusCode;
                        if (currentStatus != response.cardStatus)
                        {
                            int newStatusId = sDA.GetStatus(response.cardStatus).ID;
                            dcda.UpdateCardStatus(accountIdentifier, newStatusId);
                            return new CustomJsonResult()
                            {
                                Data = new
                                {
                                    IsError = false,
                                    Data = response
                                },
                            };
                        }
                    }

                    return new CustomJsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Data = response,
                        },
                    };
                }
                else if (response.statusCode == "SD")
                {
                    int newStatusId = sDA.GetStatus("F").ID;
                    dcda.UpdateCardStatus(accountIdentifier, newStatusId);
                    return new CustomJsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Data = response,
                            DriverCardJson = ReloadDriverCards(memberKey),
                        },
                    };
                }
                else if (response.statusCode == "54")
                {
                    var cardStatus = sDA.GetStatus("E");
                    int newStatusId = cardStatus.ID;
                    dcda.UpdateCardStatus(accountIdentifier, newStatusId);
                    return new CustomJsonResult()
                    {
                        Data = new
                        {
                            IsError = false,
                            Data = new
                            {
                                cardStatus = cardStatus.StatusCode,
                                cardStatusDescription = cardStatus.Description
                            }
                        },
                    };
                }
                else
                {
                    string errorMessage = response.description;
                    return CreateResponseResult(true, errorMessage);
                }
            }
            else
            {
                return CreateResponseResult(true, "Cannot get card status.");
            }
        }

        public ActionResult OxygenTransaction()
        {
            ViewBag.TransactionTypeList = new List<SelectListItem> {
                new SelectListItem {Value = "1", Text = "Load Fund"},
                new SelectListItem {Value = "0", Text = "Withdraw Fund"}
            };
            DriverCardDA dcDA = new DriverCardDA();
            var statusList = new List<SelectListItem>();
            dcDA.GetAllOxygenTransactionStatus().ForEach(s => statusList.Add(new SelectListItem
            {
                Text = s.Name,
                Value = s.ID.ToString(),
            }));
            ViewBag.StatusList = statusList;
            OxygenTransactionSearchVM model = new OxygenTransactionSearchVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult GetOxygenTransaction(GridModel<OxygenTransactionSearchVM, List<OxygenTransactionVM>> model)
        {
            DriverCardDA dcDA = new DriverCardDA();
            dcDA.GetOxygenTransaction(ref model);
            return new CustomJsonResult()
            {
                Data = model
            };
        }


        [HttpPost]
        public JsonResult ApproveTimeoutTransaction(OxygenTransactionVM data)
        {
            DriverCardDA dcda = new DriverCardDA();
            //var trans = dcda.GetOxygenTransById(data.ID);
            string error = string.Empty;
            OxygenAPI api = new OxygenAPI();
            var transDetail = api.GetTransactionDetail(data.OxygenTransRefNo);

            if (transDetail.responseCode == "00")
            {
                if (string.IsNullOrEmpty(error))
                {
                    error = dcda.ApproveTimeoutTransaction(transDetail, data.ID, User.Identity.Name);
                    if (string.IsNullOrEmpty(error))
                    {
                        return Json(new
                        {
                            success = true,
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(transDetail.description))
                {
                    error += "Error getting response from Oxygen;";
                }
                else
                {
                    error += transDetail.description;
                }
            }

            return Json(new
            {
                success = false,
                errors = error.Trim(';')
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ExportOxygenTransaction(string TransactionDateFrom
           , string TransactionDateTo
           , string CreatedDateFrom
           , string CreatedDateTo
           , string OxygenTransRefNo
           , string MemberID
           , string LodgementReferenceNo
           , string AccountIdentifier
           , string TransactionType
           , string AmountFrom
           , string AmountTo
           , string StatusID)
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}OxygenTransaction";

                IList<ReportingWebService.ParameterValue> reportparam = new List<ReportingWebService.ParameterValue>();

                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionDateFrom", Value = TransactionDateFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionDateTo", Value = TransactionDateTo ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CreatedDateFrom", Value = CreatedDateFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CreatedDateTo", Value = CreatedDateTo ?? "" });
                if (!string.IsNullOrEmpty(OxygenTransRefNo))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "OxygenTransRefNo", Value = OxygenTransRefNo});
                }

                if (!string.IsNullOrEmpty(MemberID))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "MemberID", Value = MemberID });
                }

                if (!string.IsNullOrEmpty(LodgementReferenceNo))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "LodgementReferenceNo", Value = LodgementReferenceNo });
                }

                if (!string.IsNullOrEmpty(AccountIdentifier))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "AccountIdentifier", Value = AccountIdentifier });
                }

                if (!string.IsNullOrEmpty(TransactionType))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "TransactionType", Value = TransactionType });
                }

                if (!string.IsNullOrEmpty(AmountFrom))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "AmountFrom", Value = AmountFrom});
                }

                if (!string.IsNullOrEmpty(AmountTo))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "AmountTo", Value = AmountTo });
                }

                if (!string.IsNullOrEmpty(StatusID))
                {
                    reportparam.Add(new ReportingWebService.ParameterValue { Name = "StatusID", Value = StatusID });
                }

                string format = "EXCELOPENXML";
                Models.Common.RenderReport(ReportName, format, reportparam, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = $"ExportOxygenTransfer.xlsx",
                    Inline = false,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                return File(output, mimeType);
            }
            catch (Exception e)
            {
                return File(new byte[] { }, "application/xlsx");
            }
        }

        public ActionResult UpdateCardStatus()
        {
            return View();
        }

        public ActionResult UpdateMultipleAccountIdentifiersStatus(string accountIdentifiers)
        {
            if (string.IsNullOrEmpty(accountIdentifiers))
            {
                return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = "Please enter Oxygen Account Identifier(s).",
                    }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                OxygenAPI oxygenAPI = new OxygenAPI();
                List<string> listAccountIdentifier = accountIdentifiers.Split(';').Select(x => x.Trim().ToLower()).Distinct().ToList();
                using (TaxiEpayEntities taxiEpay = new TaxiEpayEntities())
                {
                    var existedCards = (from dc in taxiEpay.DriverCards
                                        join st in taxiEpay.DriverCardStatus on dc.StatusID equals st.ID
                                        where listAccountIdentifier.Contains(dc.AccountIdentifier)
                                        select dc).ToList();
                    var allStatuses = taxiEpay.DriverCardStatus.Select(s => new {
                        id = s.ID,
                        code = s.StatusCode,
                        name = s.Name,
                    }).ToList();
                    int numberOfSuccessfullyChanges = 0;
                    foreach (var card in existedCards)
                    {
                        var response = oxygenAPI.GetCardStatus(card.AccountIdentifier);
                        if (response.responseCode != "00")
                        {
                            continue;
                        }

                        var oldStatus = allStatuses.Where(x => x.id == card.StatusID).Select(x => new {
                            code = x.code,
                            name = x.name,
                        }).FirstOrDefault();
                        if (!string.IsNullOrEmpty(response.cardStatus) &&
                            oldStatus.code != response.cardStatus)
                        {
                            var newStatus = allStatuses.Where(x => x.code == response.cardStatus).Select(x => x.name).FirstOrDefault();
                            card.StatusID = allStatuses.Where(x => x.code == response.cardStatus).Select(x => x.id).SingleOrDefault();

                            taxiEpay.DriverCardHistories.Add(new DriverCardHistory {
                                ActionBy = User.Identity.Name,
                                ActionDescription = "Update Status",
                                Date = DateTime.Now,
                                Note = $"Update Status from {oldStatus.name} to {newStatus}",
                                DriverCardId = card.ID,
                                MemberKey = card.MemberKey,
                            });
                            numberOfSuccessfullyChanges++;
                        }
                    }

                    if (numberOfSuccessfullyChanges > 0)
                    {
                        taxiEpay.SaveChanges();
                    }
                }

                return Json(
                    new
                    {
                        IsSuccess = true,
                        Message = "Update Successfully",
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = e.Message,
                    }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}