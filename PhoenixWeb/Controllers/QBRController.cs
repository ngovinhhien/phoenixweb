﻿using Phoenix_BusLog;
using Phoenix_BusLog.Data;
using Phoenix_Service;
using PhoenixObjects.Enums;
using PhoenixObjects.QBR;
using PhoenixWeb.Common;
using PhoenixWeb.Helper;
using PhoenixWeb.Models.ViewModel.QBR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class QBRController : PhoenixWebController
    {
        public ActionResult SearchPointHistory()
        {
            QBRSearchPointHistoryVM model = new QBRSearchPointHistoryVM();
            BusinessTypeDA bda = new BusinessTypeDA();
            QantasFileEntryStatusDA qda = new QantasFileEntryStatusDA();

            model.BusinessTypes = bda.GetBusinessTypesAvailableForPromotion().AsSelectListItem().ToList();
            model.QBRFileStatuses = qda.GetAllActive().AsSelectListItem().ToList();
            model.Promotions = new List<SelectListItem>() { new SelectListItem { Text = "All", Value = "" } };

            return View(model);
        }

        [HttpPost]
        public ActionResult SearchPointHistory(QBRSearchPointHistoryCriteriaVM model)
        {
            QantasProcessingDA qda = new QantasProcessingDA();
            return new CustomJsonResult()
            {
                Data = qda.SearchQBRPointHistory(model),
            };
        }

        

        [HttpPost]
        public ActionResult FileDetail(QBRSearchFileDetailVM model)
        {
            using (TaxiEpayEntities entity = new TaxiEpayEntities())
            {
                QantasProcessingDA qda = new QantasProcessingDA();
                QBRFileHistoryDetailVM result = new QBRFileHistoryDetailVM();
                try
                {
                    result.qbrFileHistories = qda.GetHistoryByExportFile(model.MemberReferenceSummaryKey);
                    result.qbrFileDetails = qda.GetDetailEntryByExportFile(model.MemberReferenceSummaryKey);
                    result.MemberId = model.MemberId;
                    result.Points = $"{string.Format("{0:##,###}", model.Points)} pts";
                    result.TradingName = model.TradingName;
                    result.PromotionName = model.PromotionName;
                    result.QBRNumber = model.QBRNumber;
                }
                catch (Exception e)
                {
                    result.IsError = true;
                }
                return View(result);
            }
        }

        [RBAC(Permission = "ExportQBRSearchPointHistory")]
        public ActionResult QBR_Point_History(
            string memberId,
            string qbrNumber,
            string companyKey,
            string promotionKey,
            string fileName,
            string statusKey,
            string submittedDateFrom,
            string submittedDateTo,
            string periodFrom,
            string periodTo,
            string SortField,
            string SortOrder
            )
        {
            try
            {
                byte[] output;
                string extension, mimeType, encoding;
                ReportingWebService.Warning[] warnings;
                string[] streamIds;
                string ReportName = $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportFolder.ToString())}QBR_Point_History";

                IList<ReportingWebService.ParameterValue> reportparam = new List<ReportingWebService.ParameterValue>();
                if (!string.IsNullOrEmpty(submittedDateFrom))
                {
                    submittedDateFrom = submittedDateFrom.Substring(6) + submittedDateFrom.Substring(3, 2) + submittedDateFrom.Substring(0, 2);
                }
                if (!string.IsNullOrEmpty(submittedDateTo))
                {
                    submittedDateTo = submittedDateTo.Substring(6) + submittedDateTo.Substring(3, 2) + submittedDateTo.Substring(0, 2);
                }
                if (!string.IsNullOrEmpty(periodFrom))
                {
                    periodFrom = periodFrom.Substring(3, 4) + periodFrom.Substring(0, 2);
                }
                if (!string.IsNullOrEmpty(periodTo))
                {
                    periodTo = periodTo.Substring(3, 4) + periodTo.Substring(0, 2);
                }

                if (SortField == "Pointstr")
                {
                    SortField = "Points";
                }
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "MemberId", Value = memberId ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "QBRNumber", Value = qbrNumber ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "CompanyTypeId", Value = companyKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "PromotionId", Value = promotionKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "FileName", Value = fileName ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "PeriodFrom", Value = periodFrom ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "PeriodTo", Value = periodTo ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SubmittedFrom", Value = string.IsNullOrEmpty(submittedDateFrom) ? null : submittedDateFrom });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SubmittedTo", Value = string.IsNullOrEmpty(submittedDateTo) ? null : submittedDateTo });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "StatusId", Value = statusKey ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SortField", Value = SortField ?? "" });
                reportparam.Add(new ReportingWebService.ParameterValue { Name = "SortOrder", Value = SortOrder ?? "" });

                string format = "EXCEL";
                Models.Common.RenderReport(ReportName, format, reportparam, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);

                return File(output, mimeType);
            }
            catch (Exception e)
            {
                return File(new byte[] { }, "application/xlsx");
            }
        }

        [HttpPost]
        public ActionResult GetMemberQBRNumberPointHistory(MemberQBRPointHistoryCriteriaVM model)
        {
            MemberReferenceAuditDA auditDA = new MemberReferenceAuditDA();
            return new CustomJsonResult()
            {
                Data = auditDA.MemberQBRPointHistory(model),
            };
        }

        public ActionResult MemberPointHistoryDialog()
        {
            return View();
        }

    }
}