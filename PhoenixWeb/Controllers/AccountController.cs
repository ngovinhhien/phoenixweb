﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PhoenixObjects.Member;
using PhoenixObjects.UserAuthentication;
//using Live.Util.TraceListeners;
using System.Diagnostics;
using System.IO;
using PhoenixWeb.Common;
using System.Web.Script.Serialization;
using Phoenix_BusLog.Data;
using System.Security.Principal;
using Google.Authenticator;
using System.Configuration;
using Microsoft.ReportingServices.Interfaces;
using PhoenixWeb.Models.ViewModel;
using PhoenixWeb.Models.Contants;
using Phoenix_BusLog;
using PhoenixObjects.Enums;
//using log4net.Config;


namespace PhoenixWeb.Controllers
{
    [Authorize]
    public class AccountController : PhoenixWebController
    {
        //private MembershipProvider provider;

        //private MembershipProvider membershipProvider
        //{
        //    get
        //    {
        //        if (provider == null)
        //            provider = Membership.Providers["AspNetSqlMembershipProvider"];
        //        return provider;
        //    }
        //}
        
        public AccountController(): base()
        {
          
        }
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public ActionResult CheckPasswordExpiry()
        {
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];

            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            DateTime LastPWChangedDate = membershipProvider.GetUser(User.Identity.Name, false).LastPasswordChangedDate;
            double t = DateTime.Now.Subtract(LastPWChangedDate).TotalDays;
            LoginModel model = new LoginModel();
            model.UserName = User.Identity.Name;
            ViewBag.Expiring = 0;
            if (t < 86 && t > 76)
            {
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public ActionResult CheckPasswordExpiry3dayLeft()
        {
            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];

            Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
            DateTime LastPWChangedDate = membershipProvider.GetUser(User.Identity.Name, false).LastPasswordChangedDate;
            double t = DateTime.Now.Subtract(LastPWChangedDate).TotalDays;
            LoginModel model = new LoginModel();
            model.UserName = User.Identity.Name;
            ViewBag.Expiring = 0;
            if (t < 90 && t > 86)
            {
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            return Json("error", JsonRequestBehavior.AllowGet);
        }


        
        
        

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl, FormCollection c)
        {       
            try
            {
                if (ModelState.IsValid)
                {
                   // RBAC_ExtendedMethods.Roles = new List<UserRole>();
                    string userName = c["UserName"].ToString();// model.UserName.Trim();
                    string password = c["Password"].ToString();// model.Password.Trim();
                    bool RememberMe = false;
                    Trace.TraceInformation(String.Format("Login request from user: {0}, password: {1}", userName, password.Substring(0, 3)));
                    if(c["remember"] != null)
                    {
                        var remember = c["remember"].ToString();
                        if(remember == "on")
                        {
                            RememberMe = true;
                        }
                    }
                    //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                    UserLoginHistoryDA uda = new UserLoginHistoryDA();
                    UserLoginHistoryModel uMod = new UserLoginHistoryModel();
                    //  Guid uId = (Guid)membershipProvider.GetUser(User.Identity.Name, false).ProviderUserKey;
                    Trace.TraceInformation(String.Format("Login start validating for user: {0}, password: {1}", userName, password.Substring(0, 3)));
                    if (membershipProvider.ValidateUser(userName, password))
                    {
                        Trace.TraceInformation(String.Format("Login validated from user: {0}, password: {1}", userName, password.Substring(0, 3)));

                        Guid uId = (Guid)membershipProvider.GetUser(userName, false).ProviderUserKey;

                        string mfaApplicationName = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ApplicationNameForMultiFactor.ToString());
                        string enableMultiFactor = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.EnableMultiFactor.ToString());
                        if (Convert.ToBoolean(enableMultiFactor))
                        {
                            Trace.TraceInformation(String.Format("Login successful from user: {0}, password: {1}", userName, password.Substring(0, 3)));
                            string mfaSecretKey = string.Empty, mfaType = string.Empty, mfaName = string.Empty;
                            using (MultiFactorAuthenticatorDA MultiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
                            {
                                UserMFAConfigurationModel userMFAConfigurationModel = MultiFactorAuthenticatorDA.GetUser(uId);
                                if (userMFAConfigurationModel != null && userMFAConfigurationModel.IsEnabled && userMFAConfigurationModel.IsActive && !userMFAConfigurationModel.IsDeleted && !userMFAConfigurationModel.HasCompleted)
                                {

                                    MFATypeModel mfaTypeModel = MultiFactorAuthenticatorDA.GetActiveMultiFactor();
                                    mfaSecretKey = mfaTypeModel.SecretKey;
                                    mfaType = mfaTypeModel.Code;
                                    mfaName = mfaTypeModel.Name;

                                    //Google Authenticator
                                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                                    var setupInfo = tfa.GenerateSetupCode(mfaApplicationName, userName, mfaSecretKey, 300, 300);

                                    string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                                    string manualEntrySetupCode = setupInfo.ManualEntryKey;

                                    MultiFactorAuthenticatorDA.UpsertUserMFASetup(uId, mfaTypeId: mfaTypeModel.MFATypeId, setupField1: qrCodeImageUrl, setupField2: manualEntrySetupCode,
                                        setupField3: String.Empty);

                                    return MultiFactorAuthenication(uId);
                                }

                                if (userMFAConfigurationModel != null && userMFAConfigurationModel.HasCompleted)
                                {
                                    return MultiFactorAuthenication(uId, true, "login");
                                }

                            };
                        }

                        // CreateAuthenticationTicket(model.UserName, RememberMe); 
                        //List<string> Permission = (List<string>)Session["Permission"];

                        // return RedirectToAction("Index", "AdminHome");

                        uMod.UserName = userName;
                        //uMod.UserId = uId;
                        uMod.Status = "Pass";
                        uMod.IPAddress =  GetUser_IP();
                        uMod.AttemptDate = DateTime.Now;
                        uMod.ApplicationName = "Phoenix";
                        uMod.AttempDateTime = DateTime.Now;
                        uda.Add(uMod);
                        FormsAuthentication.SetAuthCookie(userName, RememberMe);

                        Trace.TraceInformation(String.Format("Login successful for user: {0}, password: {1}", userName, password.Substring(0, 3)));

                        UserDA userda = new UserDA();
                        var u = userda.GetUsers(uId);

                        if (u.Dealerkey != null)
                           {
                                return RedirectToAction("Index", "Dealer");
                           }

                        if (Url.IsLocalUrl(returnUrl))
                        {
                            Session["Username"] = userName;
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "AdminHome");
                        }
                    }
                    else
                    {
                        Trace.TraceInformation(String.Format("Login failed for user: {0}, password: {1}", userName, password.Substring(0, 3)));
                        uMod.UserName = userName;
                      //  uMod.UserId = uId;
                        uMod.Status = "Fail";
                        uMod.IPAddress = GetUser_IP();
                        uMod.AttemptDate = DateTime.Now;
                        uMod.AttempDateTime = DateTime.Now;
                        uMod.ApplicationName = "Phoenix";
                        uda.Add(uMod);
                        ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    }
                }
            }
            catch (Exception e)
            {
                // Trace.TraceError(e.Message + ". " + e.InnerException, e.InnerException, e.StackTrace);
                Trace.TraceInformation(String.Format("Login fail with error: {0}", e.Message ));
                return View("ConnectError");// Content(e.Message.ToString());      
                
            }           
            return View(model);          
        }

        protected string GetUser_IP()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Request.UserHostAddress;
            }
            return VisitorsIPAddr;
        }

        public ActionResult ChangePassword()
        {
            return View("_ChangePasswordPartial");
        }

        public ActionResult ChangePasswordMain(LoginModel model)
        {
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePasswordMain(LoginModel model, FormCollection c)
        {
            try
            {
                string OldPassword = c["OldPassword"].ToString();
                string NewPassword = c["NewPassword"].ToString();

                if(OldPassword == NewPassword )
                {
                    ViewBag.Error = "New password cannot be same as old password";
                    return View();
                }

                //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];
                membershipProvider.UnlockUser(User.Identity.Name);
               bool IsChanged = membershipProvider.ChangePassword(User.Identity.Name, OldPassword, NewPassword);
               if (IsChanged)
               {
                   //log reset password
                   UserDA userDA = new UserDA();
                   userDA.SaveResetPasswordHistory(model.UserName, ResetPasswordSourceConstants.SourceNameOfChangePasswordInPasswordExpiration, model.UserName);

                }
               else
               {
                   return RedirectToAction("ChangePasswordMain");

                }

               return RedirectToAction("Index", "AdminHome");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }
    
        public ActionResult LogOff()
        {
           // RBAC_ExtendedMethods.Roles = new List<UserRole>();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        //[AllowAnonymous]
        //public ActionResult ForgotPassword()
        //{
        //    return View();
        //}
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ForgotPassword(string UserName, string EmailAddress)
        //{
        //    string SystemEmail = string.Empty;
        //    var token = WebSecurity.GeneratePasswordResetToken(UserName);

        //    string newpassword = PhoenixWeb.Helper.Common.GenerateRandomPassword(6);
        //    bool response = WebSecurity.ResetPassword(token, newpassword);

        //    var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];

        //    membershipProvider.UnlockUser(UserName);
        //    string newpasswordT = membershipProvider.ResetPassword(UserName, null);
        //    membershipProvider.ChangePassword(UserName, newpasswordT, newpassword);

        //    if (response == true)
        //    {
        //        string subject = "New Password";
        //        string body = "<b>Hi " + UserName + ",</b><br/><br/>";
        //        body += "<b>You have requested a password reset.</b><br/>";

        //        body += "<b>Please find your new password below. Once Logged in you can change your password by going to the my profile section on your portal</b><br/><br/>";
        //        body += "<b>UserName: " + UserName + "<b><br/><br/>";
        //        body += "<b>Password: " + newpassword + "<b><br/><br/>";
        //        body += "Login at https://www.livetaxiepay.com.au/Phoenixweb/Account/Login?ReturnUrl=%2fphoenixweb <br/><br/>";

        //        body += "<b>PhoenixWeb<b>";

        //        try
        //        {
        //            List<String> _to = new List<string>();
        //            _to.Add(EmailAddress);

        //            if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true,string.Empty))
        //            {
        //                ModelState.AddModelError("", "Reset successful, mail Sent.");
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("", "Error occured while sending email.");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", "Error!");
        //    }
        //    return View("ResetPassword");
        //}
                   
        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        public ActionResult MultiFactorAuthenication(Guid userId)
        {
            using (MultiFactorAuthenticatorDA mda = new MultiFactorAuthenticatorDA())
            {
                UserMFASetupModel userMFASetupModel = mda.GetUserMFASetup(userId);
                UserMFAConfigurationModel userMFAConfigurationModel = mda.GetUser(userId);
                ViewBag.HasCompleted = userMFAConfigurationModel.HasCompleted;
                return View("_MultiFactorAuthenticator", userMFASetupModel);
            };            
        }

        public ActionResult MultiFactorAuthenication(Guid userId, bool hascompleted, string currentModule, bool isAdminChangePassWord = false)
        {
            using (MultiFactorAuthenticatorDA mda = new MultiFactorAuthenticatorDA())
            {
                if (!hascompleted)
                {
                    ViewBag.HasCompleted = false;
                    UserMFASetupModel userMFASetupModel = mda.GetUserMFASetup(userId);
                    userMFASetupModel.CurrentModule = currentModule;
                    if (currentModule == "reset-pass")
                    {
                        userMFASetupModel.IsAdminChangePassword = isAdminChangePassWord; //only for reset password case
                    }
                    return View("_MultiFactorAuthenticator", userMFASetupModel);
                }
                else
                {
                    ViewBag.HasCompleted = true;
                    UserMFASetupModel userMFASetupModel = new UserMFASetupModel() { UserId = userId };
                    userMFASetupModel.CurrentModule = currentModule;
                    if (currentModule == "reset-pass")
                    {
                        userMFASetupModel.IsAdminChangePassword = isAdminChangePassWord; //only for reset password case
                    }
                    return View("_MultiFactorAuthenticator", userMFASetupModel);

                }
            };
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult MultiFactorAuthenication(UserMFASetupModel m)
        {
            using (MultiFactorAuthenticatorDA multiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
            {
                string mfaSecretKey = string.Empty;
                MFATypeModel mfaTypeModel = multiFactorAuthenticatorDA.GetActiveMultiFactor();
                mfaSecretKey = mfaTypeModel.SecretKey;
                var currentModule = m.CurrentModule;

                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                bool isCorrectPIN = tfa.ValidateTwoFactorPIN(mfaSecretKey, m.SetupField3);

                UserMFAConfigurationModel userMFAConfigurationModel = multiFactorAuthenticatorDA.GetUser(m.UserId);
                ViewBag.HasCompleted = userMFAConfigurationModel.HasCompleted;
                if (!isCorrectPIN)
                {
                    m.ErrorMessage = "Entered PIN is incorrect. Please enter it again";
                    m.IsError = true;
                    return View("_MultiFactorAuthenticator", m);
                }

                
                if (!userMFAConfigurationModel.HasCompleted)
                {
                    bool hasCompleted = multiFactorAuthenticatorDA.SetUserMFASetupComplete(m.UserId);
                    if (hasCompleted)
                    {
                        TempData["MFAMessage"] = "Congratulations! You account is setup with multi factor authentication. You need to use it everytime you login.";
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        m.ErrorMessage = "There was some error while setting up your account, Please try again. If you see this message quite often, please reach out to Jag (jagdish.chichria@livegroup.com.au).";
                        m.IsError = true;
                    }
                }
                else
                {
                    if (currentModule == "login")
                    {
                        UserLoginHistoryDA uda = new UserLoginHistoryDA();
                        UserLoginHistoryModel uMod = new UserLoginHistoryModel();
                        UserDA userda = new UserDA();
                        var u = userda.GetUsers(m.UserId);

                        uMod.UserName = u.UserName;
                        //uMod.UserId = uId;
                        uMod.Status = "Pass";
                        uMod.IPAddress = GetUser_IP();
                        uMod.AttemptDate = DateTime.Now;
                        uMod.ApplicationName = "Phoenix";
                        uMod.AttempDateTime = DateTime.Now;
                        uda.Add(uMod);
                        FormsAuthentication.SetAuthCookie(u.UserName, false);

                        if (u.Dealerkey != null)
                        {
                            return RedirectToAction("Index", "Dealer");
                        }
                        return RedirectToAction("Index", "AdminHome");
                    }
                    else if (currentModule == "reset")
                    {
                        using (UserDA userDA = new UserDA())
                        {
                            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];                            
                            string email = membershipProvider.GetUser(m.UserId, false).Email;
                            UserResetPasswordRequestModel userResetPasswordRequestModel = userDA.SaveResetPasswordRequest(m.UserId, email, User.Identity.Name, GetUser_IP());
                            SendVerificationEmail(m.UserId, userResetPasswordRequestModel, email);
                            ViewBag.Message = "Successfully sent verification email to your registered email.";
                            ViewBag.Title = "Reset Password Request";
                            return View("ResetPasswordStepView");
                        }
                    }
                    else if (currentModule == "reset-pass")
                    {
                        using (UserDA userDA = new UserDA())
                        {
                            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];                            
                            if (membershipProvider.GetUser(m.UserId, false) == null)
                            {

                            }
                            var user = membershipProvider.GetUser(m.UserId, false);
                            membershipProvider.UnlockUser(user.UserName);
                            string newpasswordT = membershipProvider.ResetPassword(user.UserName, null);
                            var IsChanged = membershipProvider.ChangePassword(user.UserName, newpasswordT, Session["NewPassword"].ToString());

                            if (IsChanged)
                            {
                                //log after reset password
                                var createdBy = m.IsAdminChangePassword ? User.Identity.Name : user.UserName;
                                var sourceName = m.IsAdminChangePassword
                                    ? ResetPasswordSourceConstants.SourceNameOfChangePasswordInResetPasswordRequestsScreen
                                    : ResetPasswordSourceConstants.SourceNameOfChangePasswordInResetPasswordLink;
                                userDA.SaveResetPasswordHistory(user.UserName, sourceName, createdBy);
                            }

                            userDA.MarkComplete(m.UserId, new Guid(Session["ResetPasswordToken"].ToString()));
                            Session["NewPassword"] = String.Empty;
                            ViewBag.Message = "Successfully changed your password. Please re-login again.";
                            ViewBag.Title = "Reset Password Request";
                            return View("ResetPasswordStepView");
                        }
                    }                    
                }

                return View("_MultiFactorAuthenticator", m);
            };
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            using (UserDA userDA = new UserDA())
            {
                return View(new UserForgotPasswordRequestViewModel()
                {
                    
                });
            }
        }
        
        [AllowAnonymous]
        public ActionResult ResetPassword(string id, bool isAdminChangePassword = false)
        {
            UserDA userDA = new UserDA();
            //check token valid for reset
            bool validRequest = userDA.IsValidResetPasswordRequest(new Guid(id));

            return View(new UserResetPasswordRequestViewModel()
            {
                ResetPasswordToken = new Guid(id),
                IsAdminChangePassword = isAdminChangePassword,
                IsValidRequest = validRequest
            });
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(UserResetPasswordRequestViewModel model, FormCollection c)
        {
            ViewBag.Error = String.Empty;
            ViewBag.Success = String.Empty;

            if (String.IsNullOrEmpty(model.Username))
            {
                ViewBag.Error = "Please put your username";
                return View(model);
            }

            if (String.IsNullOrEmpty(model.NewPassword) || String.IsNullOrEmpty(model.NewPasswordConfirm))
            {
                ViewBag.Error = "Please put new password";
                return View(model);
            }


            if (model.NewPassword != model.NewPasswordConfirm)
            {
                ViewBag.Error = "Please reenter your password, they must match";
                return View(model);
            }

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];

            if (membershipProvider.GetUser(model.Username, false) != null)
            {
                Guid uId = (Guid)membershipProvider.GetUser(model.Username, false).ProviderUserKey;
                using (UserDA userDA = new UserDA())
                {
                    UserResetPasswordRequestModel userResetPasswordRequestModel = userDA.GetResetPasswordRequest(uId);
                    if (userResetPasswordRequestModel!=null )
                    {
                        if (userResetPasswordRequestModel.ResetPasswordToken != model.ResetPasswordToken)
                        {
                            ViewBag.Error = "Incorrect token or token is expired. Please regenerate it using reset your password. Click <a href=\"Account/ForgotPassword\">here</a> to reset your password.";
                            return View(model);
                        }
                        if (userResetPasswordRequestModel.ExpiresBy < DateTime.Now)
                        {
                            ViewBag.Error = "Incorrect token or token is expired. Please regenerate it using reset your password. Click <a href=\"Account/ForgotPassword\">here</a> to reset your password.";
                            return View(model);
                        }                        
                    }
                }
                if (uId != null)
                {
                    string mfaApplicationName = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ApplicationNameForMultiFactor.ToString());
                    string enableMultiFactor = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.EnableMultiFactor.ToString());
                    if (Convert.ToBoolean(enableMultiFactor))
                    {

                        string mfaSecretKey = string.Empty, mfaType = string.Empty, mfaName = string.Empty;
                        using (MultiFactorAuthenticatorDA MultiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
                        {
                            UserMFAConfigurationModel userMFAConfigurationModel = MultiFactorAuthenticatorDA.GetUser(uId);
                            if (userMFAConfigurationModel != null && userMFAConfigurationModel.HasCompleted)
                            {
                                Session["NewPassword"] = model.NewPassword;
                                Session["ResetPasswordToken"] = model.ResetPasswordToken;
                                return MultiFactorAuthenication(uId, true, "reset-pass", model.IsAdminChangePassword);
                            }
                        };
                    }

                    membershipProvider.UnlockUser(model.Username);
                    string newpasswordT = membershipProvider.ResetPassword(model.Username, null);
                    var IsChanged = membershipProvider.ChangePassword(model.Username, newpasswordT, model.NewPassword);
                    using (UserDA userDA = new UserDA())
                    {
                        if (IsChanged)
                        {
                            //log after reset password
                            var createdBy = model.IsAdminChangePassword ? User.Identity.Name : model.Username;
                            var sourceName = model.IsAdminChangePassword
                                ? ResetPasswordSourceConstants.SourceNameOfChangePasswordInResetPasswordRequestsScreen
                                : ResetPasswordSourceConstants.SourceNameOfChangePasswordInResetPasswordLink;
                            userDA.SaveResetPasswordHistory(model.Username, sourceName, createdBy);
                        }
                        userDA.MarkComplete(uId, model.ResetPasswordToken);
                    }
                    ViewBag.Message = "Successfully changed your password. Please re-login again.";
                    ViewBag.Title = "Reset Password Request";
                    return View("ResetPasswordStepView");
                }

            }
            else
            {
                ViewBag.Error = "Incorrect username or you are not authorised to use reset password.";
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(UserForgotPasswordRequestViewModel model)
        {
            ViewBag.Error = String.Empty;
            ViewBag.Success = String.Empty;
        
            if (String.IsNullOrEmpty(model.Username))
            {
                ViewBag.Error = "Please put your username";
                return View(model);
            }

            //var membershipProvider = Membership.Providers["AspNetSqlMembershipProvider"];

            if (membershipProvider.GetUser(model.Username, false) != null)
            {
                Guid uId = (Guid)membershipProvider.GetUser(model.Username, false).ProviderUserKey;
                if (uId != null)
                {
                    string mfaApplicationName = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ApplicationNameForMultiFactor.ToString());
                    string enableMultiFactor = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.EnableMultiFactor.ToString());
                    if (Convert.ToBoolean(enableMultiFactor))
                    {

                        string mfaSecretKey = string.Empty, mfaType = string.Empty, mfaName = string.Empty;
                        using (MultiFactorAuthenticatorDA MultiFactorAuthenticatorDA = new MultiFactorAuthenticatorDA())
                        {
                            UserMFAConfigurationModel userMFAConfigurationModel = MultiFactorAuthenticatorDA.GetUser(uId);
                            if (userMFAConfigurationModel != null && userMFAConfigurationModel.HasCompleted)
                            {
                                return MultiFactorAuthenication(uId, true, "reset");
                            }
                        };
                    }

                    using (UserDA userDA = new UserDA())
                    {                        
                        string email = membershipProvider.GetUser(model.Username, false).Email;
                        UserResetPasswordRequestModel userResetPasswordRequestModel = userDA.SaveResetPasswordRequest(uId, email, User.Identity.Name, GetUser_IP());
                        SendVerificationEmail(uId, userResetPasswordRequestModel, email);
                        ViewBag.Message = "Successfully sent verification email to your registered email.";
                        ViewBag.Title = "Reset Password Request";
                        return View("ResetPasswordStepView");
                    }
                }

            }
            else
            {
                ViewBag.Error = "Incorrect username or you are not authorised to use reset password.";
            }
            return View("ForgotPassword",model);        
        }

        private void SendVerificationEmail(Guid userid, UserResetPasswordRequestModel model, string email)
        {
            UserDA userda = new UserDA();
            var u = userda.GetUsers(userid);

            string subject = "Reset Password";
            string body = "<b>Hi " + u.UserName + ",</b><br/><br/>";
            body += "<b>You have requested a password reset.</b><br/>";

            body += "<b>Please find your verification link below. Once Logged in you can change your password by going to the my profile section on your portal</b><br/><br/>";
            body += "<b>UserName: " + u.UserName + "<b><br/><br/>";
            body += $"{Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebDNS.ToString())}/Account/ResetPassword?id=" + model.ResetPasswordToken + " <br/><br/>";

            body += "<b>Regards, <br/>PhoenixWeb Support Team<b>";

            try
            {
                List<String> _to = new List<string>();
                _to.Add(email);

                if (PhoenixWeb.Helper.Common.SendMail(_to, null, null, subject, body, true, "it@livegroup.com.au"))
                {
                    ModelState.AddModelError("", "Reset successful, mail Sent.");
                }
                else
                {
                    ModelState.AddModelError("", "Error occured while sending email.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error occured while sending email. " + ex.Message);
            }
        }

    }
}
