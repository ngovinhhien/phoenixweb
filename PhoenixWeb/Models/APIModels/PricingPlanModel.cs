﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace PhoenixWeb.Models.APIModels
{
    public class PricingPlanModel
    {
        public PricingPlanModel()
        {
            BusinessTypes = new List<BusinessType>();
            PayMethods = new List<PricingRate>();
        }

        public string PlanName { get; set; }
        public List<BusinessType> BusinessTypes { get; set; }

        public List<PricingRate> PayMethods { get; set; }

    }

    public class BusinessType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessType"/> class.
        /// </summary>
        public BusinessType()
        {
            SubBusinessTypes = new List<VehicleType>();
        }

        public int BusinessTypeKey { get; set; }

        public string BusinessTypeName { get; set; }

        public bool IsSelected { get; set; }

        public List<VehicleType> SubBusinessTypes { get; set; }
    }

    public class VehicleType
    {
        public int SubBusinessTypeKey { get; set; }

        public string SubBusinessTypeName { get; set; }

        public bool IsChecked { get; set; }
    }

    public class PricingRate
    {
        public int PayMethodKey { get; set; }

        public string PayMethodName { get; set; }

        public decimal Rate { get; set; }

        public bool AllowSwitchDollarAndPercentage { get; set; }

        public bool IsRateFixed { get; set; }

        public decimal TransactionFee { get; set; }
    }

    public class PricingPlan
    {
        public string MSFPlanName { get; set; }

        public string CreatedDate { get; set; }

        public string BusinessTypeName { get; set; }

        public string CreatedBy { get; set; }

        public int MSFPlanID { get; set; }
    }
}