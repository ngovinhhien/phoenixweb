﻿using System;
using System.Collections.Generic;

namespace PhoenixWeb.Models.APIModels
{
    public class PaymentHistoryDetailModel
    {
        public PaymentDetailModel PaymentDetail { get; set; }
        public List<TransferHistoryModel> TransferHistories { get; set; }
    }

    public class PaymentDetailModel
    {
        public string PaymentDate { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string TotalAmount { get; set; }
        public string TransferredAmount { get; set; }
        public string PendingAmount { get; set; }
        public string OriginalPaymentType { get; set; }
        public string PaymentType { get; set; }
        public string PaymentStatus { get; set; }
        public string LatestTransferDate { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string Bsb { get; set; }
        public string FailedDate { get; set; }
        public bool IsSplitPayment { get; set; }
        public bool IsShowTransferHistory { get; set; }

        public string TotalAmountFormatted => Convert.ToDecimal(TotalAmount).ToString("$#,##0.00");
        public string TransferredAmountFormatted => Convert.ToDecimal(TransferredAmount).ToString("$#,##0.00");
        public string PendingAmountFormatted => Convert.ToDecimal(PendingAmount).ToString("$#,##0.00");
        public bool IsShowLatestTransferDate => PaymentType == "Card" && !IsShowTransferHistory;
    }

    public class TransferHistoryModel
    {
        public string TransferredDate { get; set; }
        public string Amount { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string PaymentNote { get; set; }
        public string AmountFormatted => Convert.ToDecimal(Amount).ToString("$#,##0.00");
    }
}