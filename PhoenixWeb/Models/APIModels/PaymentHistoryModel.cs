﻿using System;
using WebGrease.Css;

namespace PhoenixWeb.Models.APIModels
{
    public class PaymentHistoryModel
    {
        public  int MemberKey { get; set; }
        public int TransactionKey { get; set; }
        public string PaymentDate { get; set; }
        public string TotalAmount { get; set; }
        public string TotalAmountFormatted => Convert.ToDecimal(TotalAmount).ToString("$#,##0.00");
        public string LodgementReferenceNo { get; set; }
        public string PaymentType { get; set; }
        public string PayRunId { get; set; }
    }
}