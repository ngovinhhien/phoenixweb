﻿namespace PhoenixWeb.Models
{
    public class InterchangeSchemeFeeMIDValidateResultModel
    {
        public string TradingName { get; set; }
        public int Company_key { get; set; }
        public string MemberId { get; set; }
        public int MemberKey { get; set; }
    }
}