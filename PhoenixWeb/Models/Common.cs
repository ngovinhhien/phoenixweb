﻿using Phoenix_Service;
using PhoenixWeb.ReportingWebService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PhoenixWeb.Models.ViewModel;
using PhoenixObjects.Enums;
using Phoenix_BusLog;

namespace PhoenixWeb.Models
{
    public static class Common
    {
        public static void RenderReport(string reportName,string format, IList<ParameterValue> parameters, out byte[] output, out string extension, out string mimeType, out string encoding, out Warning[] warnings, out string[] streamIds)
        {
            using (var webServiceProxy = new ReportExecutionServiceSoapClient("ReportExecutionServiceSoap"))
            {
                try
                {
                    // IList<ParameterValue> parameters = new List<ParameterValue>();
                    //string reportName = ConfigurationManager.AppSettings.Get("ReportServerPath") + report;
                    NetworkCredential clientCredentials = new NetworkCredential
                        (
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomainAdmin.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomainPassword.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomain.ToString())
                        );

                    webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                    webServiceProxy.ClientCredentials.Windows.ClientCredential = clientCredentials;
                    
                    // Init Report to execute
                    ServerInfoHeader serverInfoHeader;
                    ExecutionInfo executionInfo;
                    ExecutionHeader executionHeader = webServiceProxy.LoadReport(null, reportName, null, out serverInfoHeader, out executionInfo);
                    // Attach Report Parameters
                    webServiceProxy.SetExecutionParameters(executionHeader, null, parameters.ToArray(), "en-au", out executionInfo);
                    // Render
                  //  webServiceProxy.Render(executionHeader, null, "HTML4.0", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                    //webServiceProxy.Render(executionHeader, null, "PDF", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                    webServiceProxy.Render(executionHeader, null, format, null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
                   
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

        }

        public static IEnumerable<SelectListItem> AsSelectListItems(this List<int> numberList)
        {
            return numberList.Select(n => new SelectListItem()
            {
                Text = n.ToString(),
                Value = n.ToString()
            });
        }

        public static IEnumerable<SelectListItem> AsSelectListItems(this List<Company> companyList)
        {
            return companyList.Select(n => new SelectListItem()
            {
                Text = n.Name,
                Value = n.Company_key.ToString()
            });
        }

        public static IEnumerable<SelectListItem> AsSelectListItems(this List<LoyaltyPromotion> loyaltyPromotion)
        {
            return loyaltyPromotion.Select(n => new SelectListItem()
            {
                Text = n.PromotionName,
                Value = n.ID.ToString()
            });
        }

        public static IEnumerable<SelectListItem> AsSelectListItems(this List<ExchangeRateType> exchangeRateType, string defaultTypeCode = "DEFAULT")
        {
            return exchangeRateType.Select(n => new SelectListItem()
            {
                Text = n.ExchangeRateTypeName,
                Value = n.ExchangeRateTypeCode,
                Selected = n.ExchangeRateTypeCode.Equals(defaultTypeCode)
            });
        }

        public static IEnumerable<SelectListItem> GenerateDisableEnableListItem()
        {
            Dictionary<string, string> QBRStatuses = new Dictionary<string, string>();
            QBRStatuses.Add("", "All");
            QBRStatuses.Add("0", "Disabled");
            QBRStatuses.Add("1", "Enabled");
            return QBRStatuses.Select(item => new SelectListItem
            {
                Text = item.Value,
                Value = item.Key,
                Selected = item.Key == string.Empty,
            }).ToList();
        }

        public static EftTerminal ToEftTerminal(this ImportTerminalVM importTerminal)
        {
            return new EftTerminal()
            {
                Active = true,
                TerminalId = importTerminal.TerminalID.Trim(),
                SoftwareVersion = importTerminal.Location.Trim()
            };
        }
    }
}
