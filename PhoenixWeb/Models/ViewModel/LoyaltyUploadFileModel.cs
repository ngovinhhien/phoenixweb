﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    
    public class LoyaltyUploadFileModel
    {
        [Required]
        public string MemberId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Points { get; set; }
        [Required]
        public string LoyaltyNumber { get; set; }
        public string Status { get; set; }
        [Required]
        public string RecordDate { get; set; }
        public string Load { get; set; }

        public void Verify()
        {
            doBusinessLogic();
        }

        public string[] Error { get; private set; }

        /// <summary>
        /// Never use without calling Verify()
        /// </summary>
        /// <seealso cref="Verify"/>
        public bool HasError { get { return Error.Length > 1; } }
        private void doBusinessLogic()
        {
            StringBuilder error = new StringBuilder();
            Int64.TryParse(LoyaltyNumber, out Int64 output);
            if (LoyaltyNumber.Trim().Length != 11 || output == 0 || output<0 || LoyaltyNumber.Trim().Contains("-") || LoyaltyNumber.Trim().Contains("."))
            {
                error.Append("|Incorrect QBR Number");
            }
            else
            {
                LiveGroup.ServiceLayer.Services.QantasBusinessRewards.MembershipService rewards =
                    new LiveGroup.ServiceLayer.Services.QantasBusinessRewards.MembershipService();
                try
                {
                    LiveGroup.ServiceLayer.IServiceLayerResponseMessage responseMessage =
                                            rewards.Validate(new LiveGroup.ServiceLayer.Services.QantasBusinessRewards.MembershipRequestMessage()
                                            {
                                                Number = LoyaltyNumber.Trim(),
                                            });
                    if (!responseMessage.IsValid)
                    {
                        error.Append("|Invalid QBR Number");
                    }
                }
                catch (Exception ex)
                {
                    error.Append(ex.Message + Environment.NewLine + ex.InnerException ?? ex.InnerException.Message);
                }                              
            }

            int.TryParse(Points, out int parsedPoints);
            if (parsedPoints < 0) { error.Append("|Negative points are not allowed."); }
            if (parsedPoints > 125000) { error.Append("|Maximum of 125,000 loyalty points is rewarded to a member."); }
            if (parsedPoints == 0) { error.Append("|Invalid points or it canot be Zero."); }

            DateTime.TryParse(RecordDate, out DateTime parsedRecordDate);
            if (parsedRecordDate == DateTime.MinValue) { error.Append("|Incorrect date."); }


            bool isValid = int.TryParse(Name, out int name);
            if (name > 0 || isValid) { error.Append("|Incorrect name or has invalid characters."); }
            if (Name.Length < 2) { error.Append("|Name is too small"); }
            if (Name.Length > 255) { error.Append("|Name is too long"); }

            Error = error.ToString().Split('|') ;
        }
    }

    public static class ExtensionClass
    {
        public static string IsValid(this IEnumerable<LoyaltyUploadFileModel> loyaltyUploadFileModels)
        {

            StringBuilder error = new StringBuilder();
            foreach (LoyaltyUploadFileModel loyaltyUploadFileModel in loyaltyUploadFileModels)
            {
                loyaltyUploadFileModel.Verify();
                if (loyaltyUploadFileModel.HasError) { 
                    //error.Append(loyaltyUploadFileModel.Error); 
                }
            }
            var duplicateLoyaltyNumber = loyaltyUploadFileModels.GroupBy(i => i.LoyaltyNumber)
                                     .Where(x => x.Count() > 1)
                                     .Select(val => val.Key);

            if (duplicateLoyaltyNumber.Count() > 0) { error.Append("|Duplicate QBR Numbers"); }

            var duplicateMemberId = loyaltyUploadFileModels.GroupBy(i => i.MemberId)
                             .Where(x => x.Count() > 1)
                             .Select(val => val.Key);
            if (duplicateMemberId.Count() > 0) { error.Append("|Duplicate Member Id"); }
            return error.ToString();
        }
    }

}