﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class RewardsPointVM
    {
        public string Total { get; set; }
        public string TotalUsed { get; set; }
        public string TotalAvail { get; set; }
    }
}