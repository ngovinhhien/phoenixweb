﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using PhoenixWeb.Helper;
using PhoenixWeb.Models.APIModels;

namespace PhoenixWeb.Models.ViewModel.PricingPlan
{
    public class PricingPlanVM
    {
        public int PlanId { get; set; }

        public string PlanName { get; set; }

        public int SelectedBusinessTypeKey { get; set; }

        public string BusinessTypeName { get; set; }

        public List<BusinessType> BusinessTypes { get; set; }

        public List<int> SubBusinessTypeIDs { get; set; }
        public List<VehicleType> SelectedSubBusinessTypes
        {
            get { return BusinessTypes.FirstOrDefault(n => n.IsSelected)?.SubBusinessTypes; }
        }

        public List<CustomHelpers.CustomSelectItem> BusinessTypeSelectItem
        {
            get
            {
                //Using for the future if we need to update the render the vehicle type list base on the selected business type.
                // Not for now
                return BusinessTypes.Select(n => ToSelectListItem(n)).ToList();
            }
        }

        public List<PricingRate> Rates { get; set; }

        public string CreatedBy { get; set; }

        public PricingPlanVM()
        {
            Rates = new List<PricingRate>();
        }

        private CustomHelpers.CustomSelectItem ToSelectListItem(BusinessType bus)
        {
            return new CustomHelpers.CustomSelectItem()
            {
                Text = bus.BusinessTypeName,
                Value = bus.BusinessTypeKey.ToString(),
                Selected = bus.BusinessTypeKey == SelectedBusinessTypeKey,
                CustomValue = JsonConvert.SerializeObject(bus.SubBusinessTypes)
            };
        }
    }
}