﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class UserModelVM
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }
}