﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasPointFileReviewingVM : VMBaseWithPaging
    {
        [Required]
        [Display(Name = "Year")]
        public IEnumerable<SelectListItem> Years { get; set; }
        public int Year { get; set; }

        [Required]
        [Display(Name = "Month")]
        public IEnumerable<SelectListItem> Months { get; set; }
        public int Month { get; set; }
        [Display(Name = "Qantas File")]
        public int QantasFileID { get; set; }
        public IEnumerable<SelectListItem> QantasFileNameList { get; set; }

    }
}