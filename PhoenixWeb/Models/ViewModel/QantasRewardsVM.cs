﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasRewardsVM
    {
        public ICollection<QantasExportFileVM> QantasExportFiles { set; get; }
        public QantasExportFileVM  QantasExportFile {get;set;}
    }
}