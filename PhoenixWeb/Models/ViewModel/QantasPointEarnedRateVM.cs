﻿using Phoenix_Service;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasPointEarnedRateVM: VMBaseWithPaging
    {

        [Required]
        [Display(Name = "Type")]
        public string ExchangeRateTypeCode { get; set; }
        [Required]
        [Display(Name = "Earned Rate")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only allow input number or decimal with max 2 digit (0.00)")]
        [Range(1,99999)]
        public float QantasExchangeRate { get; set; }
        [Required]
        [Display(Name = "Name")]
        [StringLength(100, ErrorMessage = "Name cannot be longer than 100 characters.")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Campaign Start Date")]
        public string StartDate { get; set; }
        [Required]
        [Display(Name = "Campaign End Date")]
        public string EndDate { get; set; }
        [Required]
        [Display(Name = "Effected Days")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Please add a numbers")]
        [Range(1, 99999)]
        public int EffectedDay { get; set; }

        [Display(Name = "Group Name")]
        [Required]
        [StringLength(100, ErrorMessage = "Name cannot be longer than 100 characters.")]
        public string GroupName { get; set; }
        public int GroupID { get; set; }

        public int ID { get; set; }

        public Group ToGroup()
        {
            return new Group()
            {
                GroupID = GroupID,
                GroupName = GroupName,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                IsActive = true
            };
        }

        public QBRExchangeRate ToQbrExchangeRateFromGroup()
        {
            CultureInfo culture = new CultureInfo("en-US");
            DateTime? startDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", culture);
            DateTime? endtDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", culture);
            return new QBRExchangeRate()
            {
                ID = ID,
                GroupID = GroupID,
                QantasExchangeRate = QantasExchangeRate,
                CampaignStartDate = startDate,
                CampaignEndDate = endtDate,
                IsActive = true,
                CreatedDate = DateTime.Now,
                ExchangeRateTypeCode = "GROUP",
                UpdatedDate = DateTime.Now,
                Name = Name
            };
        }

    }
}