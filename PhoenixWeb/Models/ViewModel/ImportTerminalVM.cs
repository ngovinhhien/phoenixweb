﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class ImportTerminalVM
    {
        public string TerminalID { get; set; }
        public string TerminalType { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
    }
}