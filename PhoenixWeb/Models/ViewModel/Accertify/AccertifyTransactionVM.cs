﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.Accertify
{
    public class AccertifyTransactionVM
    {
        public int MemberKey { get; set; }
        public string MemberID { get; set; }
        public string TradingName { get; set; }
        public string TerminalID { get; set; }
        public string TransactionType { get; set; }
        public string OrderNumber { get; set; }
        public string ReceiptNumber { get; set; }
        public decimal TotalAmount { get; set; }
        public string TransactionDateTime { get; set; }
        public string TransactionStatus { get; set; }
        public string AccertifyStatus { get; set; }
        public string MCCCategory { get; set; }
        public string CardNumber { get; set; }
        public string CardName { get; set; }
        public int Total { get; set; }

    }
}