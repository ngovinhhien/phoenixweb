﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel.Accertify
{
    public class AccertifyTransactionFilter : FilterModel
    {
        [Display(Name = "Search")]
        public string SearchText { get; set; } = string.Empty;

        [Display(Name = "Transaction Dates")]
        public string FromDate { get; set; }

        [Display(Name = "To")]
        public string ToDate { get; set; }

        [Display(Name = "Accertify Status")]
        public string AccertifyStatus { get; set; }

        [Display(Name = "Sub-Business Type")]
        public string SubBusinessType { get; set; }

       

        public List<SelectListItem> ListSubBusinessType { get; set; }
        public List<SelectListItem> ListAccertifyStatus { get; set; }
    }
}