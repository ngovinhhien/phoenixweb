﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.QBR
{
    public class QBRSearchFileDetailVM
    {
        public int MemberReferenceSummaryKey { get; set; }
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public int Points { get; set; }
        public string SummaryEntryDescription { get; set; }
        public string PromotionName { get; set; }
        public string QBRNumber { get; set; }
    }
}