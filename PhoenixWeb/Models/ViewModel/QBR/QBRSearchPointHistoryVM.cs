﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel.QBR
{
    public class QBRSearchPointHistoryVM
    {
        public List<SelectListItem> BusinessTypes { get; set; }
        public List<SelectListItem> QBRFileStatuses { get; set; }
        public List<SelectListItem> Promotions { get; set; }

        public int CompanyKey { get; set; }
        public int PromotionID { get; set; }
        public string MemberID { get; set; }
        public string FileName { get; set; }
        public string QBRNumber { get; set; }
        public string StatusID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodFrom { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodTo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SubmittedFrom { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SubmittedTo { get; set; }
    }
}