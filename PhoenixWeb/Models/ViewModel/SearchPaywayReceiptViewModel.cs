﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class SearchPaywayReceiptViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string SortName { get; set; }
        public string SortOrder { get; set; }
        public string CustomerReferenceNumber { get; set; }
        public string OrderNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}