﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasExportFileVM
    {     
        public int QantasExportFileKey { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> FileDate { get; set; }
        public Nullable<System.DateTime> ExportDateTime { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> QantasExportFileStatusId { get; set; }

        public QantasExportFileStatusVM QantasExportFileStatus { get; set; }
        public ICollection<QantasExportFileApproverVM> QantasExportFileApprovers { get; set; }
        public ICollection<QantasFileEntryVM> QantasFileEntries { get; set; }
    }


    public class LoyaltyExportFileVM
    {
        public int LoyaltyExportFileId { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> FileDate { get; set; }
        public Nullable<System.DateTime> ExportDateTime { get; set; }
        public Nullable<int> QantasExportFileStatusId { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int LoyaltyUploadFileHeaderId { get; set; }
    }
}
