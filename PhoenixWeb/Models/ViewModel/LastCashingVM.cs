﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class LastCashingVM
    {
        public string LastCashedAmt { get; set; }
        public string LastCashedDateTime { get; set; }
    }
}