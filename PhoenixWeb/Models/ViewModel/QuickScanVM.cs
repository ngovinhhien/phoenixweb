﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class QuickScanVM
    {
        public int Id { get; set; }
        public string TopBarCode { get; set; }
        public string BottomBarCode { get; set; }
        public string Amount { get; set; }
    }
}