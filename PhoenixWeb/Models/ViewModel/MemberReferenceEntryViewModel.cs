﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class MemberReferenceEntryViewModel : MemberReferenceEntryModel
    {
        public int SelectedMonth { get; set; }
        public int SelectedYear { get; set; }
        public int SelectedPromotionId { get; set;}

        public IEnumerable<MemberReferenceEntryModel> MemberReferenceEntries { get; set; }
    }
}