﻿using Phoenix_BusLog.TransactionHost.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class TerminalTMSConnectVM
    {
        public TerminalTMSConnectVM()
        {
            InvalidOrNotUpdated = new string[0];
            Updated = new string[0];

        }
        public string TerminalId { get; set; }
        public bool TMSConnect { get; set; }
        public string[] InvalidOrNotUpdated { get; set; }
        public string[] Updated { get; set; }

    }


    public class BulkAddTerminalVM
    {
        public BulkAddTerminalVM()
        {
            InvalidOrNotUpdated = new string[0];
            Updated = new string[0];

        }

        public TerminalModel Terminal { get; set; }
        public string TerminalId { get; set; }      
        public string[] InvalidOrNotUpdated { get; set; }
        public string[] Updated { get; set; }

    }
}