﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class TransactionFromBankVM
    {
        public long id { get; set; }
        public decimal batchid { get; set; }
        public string batchstatus { get; set; }
        public string transid1 { get; set; }
        public string Merchantid { get; set; }
        public string merchantname { get; set; }
        public string driverid { get; set; }
        public DateTime? Loaded { get; set; }
        public DateTime? Startshift { get; set; }
        public DateTime? endshift { get; set; }
        public string BatchNumber { get; set; }
        public string taxiid { get; set; }
        public string terminalnumber { get; set; }
        public string TransType { get; set; }
        public string Trans_Status { get; set; }
        public string CardName { get; set; }
        public string transpan { get; set; }
        public DateTime? transstart { get; set; }
        public DateTime? transend { get; set; }
        public decimal? TransFare { get; set; }
        public decimal? TransExtras { get; set; }
        public decimal? TransTip { get; set; }
        public decimal? TransTotalAmount { get; set; }
        public int TransAuthorisationNumber { get; set; }
        public int TransPassengers { get; set; }
        public int TransTarrif { get; set; }
        public int TransKilometers { get; set; }
        public string TransPickupZone { get; set; }
        public string TransDropoffZone { get; set; }
        public string sourcefile { get; set; }
        public DateTime recordcreated { get; set; }
        public DateTime? filedate { get; set; }
        public string orig_CardName { get; set; }
    }
}