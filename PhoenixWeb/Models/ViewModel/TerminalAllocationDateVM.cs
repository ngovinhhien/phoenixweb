﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class TerminalAllocationDateVM
    {
        public string dDate { get; set; }
        public string tTime { get; set; }
        public string tActual { get; set; }
        public string EffectiveDateString { get; set; }
    }
}