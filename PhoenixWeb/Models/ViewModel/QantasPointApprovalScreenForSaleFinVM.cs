﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phoenix_Service;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasPointApprovalScreenVM : VMBaseWithPaging
    {
        public QantasPointApprovalScreenVM()
        {
            BusinessTypes = new List<SelectListItem>();
            Years = new List<SelectListItem>();
            Months = new List<SelectListItem>();
        }

        [Required]
        [Display(Name = "Year")]
        public IEnumerable<SelectListItem> Years { get; set; }
        public int Year { get; set; }

        [Required]
        [Display(Name = "Month")]
        public IEnumerable<SelectListItem> Months { get; set; }
        public int Month { get; set; }

        [Required]
        [Display(Name = "Business Type")]
        public IEnumerable<SelectListItem> BusinessTypes { get; set; }

        public int BusinessTypeID { get; set; }

        public List<GetQBRPointForSaleFinByYearMonthAndBusinessType_Result> RejectedInforObject { get; set; }
        public string ManagerRejectedComment { get; set; }

    }
}