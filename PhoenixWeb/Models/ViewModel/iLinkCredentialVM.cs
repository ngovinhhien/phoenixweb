﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class iLinkCredentialVM
    {
        public int ID { get; set; }
        public string iLinkPassword { get; set; }
        public string iLinkFilename { get; set; }
        public Nullable<System.DateTime> iLinkExpiryDate { get; set; }
        public string iLinkFile { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public Phoenix_Service.iLinkCredential toiLinkCredential()
        {
            Phoenix_Service.iLinkCredential model = new Phoenix_Service.iLinkCredential();
            model.ID = ID;
            model.iLinkPassword = iLinkPassword;
            model.iLinkFilename = iLinkFilename;
            model.iLinkExpiryDate = iLinkExpiryDate;
            model.iLinkFile = iLinkFile;
            model.IsActive = IsActive;
            model.CreatedBy = CreatedBy;
            model.CreatedDate = DateTime.Now;
            model.UpdatedBy = UpdatedBy;
            model.UpdateDate = DateTime.Now;
            return model;
        }
    }
}