﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class GroupMemberVM : VMBaseWithPaging
    {
        public int GroupID { get; set; }
        [DisplayName("Group Name")]
        public string GroupName { get; set; }

        [DisplayName("Number of merchants")]
        public int TotalMerchant { get; set; }
    }
}