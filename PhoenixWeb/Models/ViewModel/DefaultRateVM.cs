﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class DefaultRateVM
    {
        public string Name { get; set; }
        public decimal Rate { get; set; }
    }
}