﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class TerminalSearchVM
    {
        public int TerminalID { get; set; }
        public int Terminalkey { get; set; }
        public int TerminalkeyHost { get; set; }
        public string SerialNo { get; set; }
        public string  VAAVersion { get; set; }
        public string Status { get; set; }
    }
}