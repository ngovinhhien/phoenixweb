﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class MemberNoteVM
    {
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<MemberNotesModel> MemberNotes { get; set; }
    }
}