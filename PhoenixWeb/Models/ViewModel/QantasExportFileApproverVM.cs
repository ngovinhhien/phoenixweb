﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasExportFileApproverVM
    {
        public int QantasExportFileApproverId { get; set; }
        public int ProcessApproverListId { get; set; }
        public int QantasExportFileId { get; set; }
        public int Sequence { get; set; }
        public int QantasExportFileApproverStatusId { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }

        public virtual QantasExportFileVM QantasExportFile { get; set; }
        public virtual QantasExportFileApproverStatusVM QantasExportFileApproverStatus { get; set; }
    }

    public class LoyaltyExportFileApproverVM
    {
        public int QantasExportFileApproverId { get; set; }
        public int ProcessApproverListId { get; set; }
        public int QantasExportFileId { get; set; }
        public int Sequence { get; set; }
        public int QantasExportFileApproverStatusId { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }

        public virtual LoyaltyExportFileVM QantasExportFile { get; set; }
        public virtual LoyaltyExportFileApproverStatusVM QantasExportFileApproverStatus { get; set; }
    }
}
