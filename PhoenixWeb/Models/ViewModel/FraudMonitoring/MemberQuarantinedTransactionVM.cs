﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberQuarantinedTransactionVM
    {
        public string TerminalId { get; set; }
        public string TransactionDate { get; set; }
        public string PaymentStatus { get; set; }
        public string Amount { get; set; }
        public string Extras { get; set; }
        public string TotalAmount { get; set; }
        public string FileDate { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string Used { get; set; }
        public string BatchId { get; set; }
        public string BatchNumber { get; set; }
        public string BusinessId { get; set; }
        public int DocketKey { get; set; }
        public bool IsRaiseReverse { get; set; }
        public bool IsRelease { get; set; }
    }
}