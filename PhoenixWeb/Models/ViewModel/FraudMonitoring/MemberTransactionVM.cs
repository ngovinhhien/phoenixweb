﻿namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberTransactionVM
    {
        public int EftDocketKey { get; set; }
        public string TerminalId { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string Amount { get; set; }
        public string Extras { get; set; }
        public string TotalAmount { get; set; }
        public string FileDate { get; set; }
        public string CardNumber { get; set; }
        public string CardHolder { get; set; }
        public string BatchId { get; set; }
        public string BatchNumber { get; set; }
        public string BusinessId { get; set; }
        public string Used { get; set; }
        public string RemainingAmount { get; set; }
        public bool IsRaiseChargeback { get; set; }
        public bool IsQuarantinable { get; set; }
    }
}