﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberPendingTransactionVM
    {
        public string ExpectPaidDate { get; set; }
        public string TerminalId { get; set; }
        public string BatchNumber { get; set; }
        public string TransactionDate { get; set; }
        public string FileDate { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string RateUsed { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
    }

    public class PendingTransactionTotal
    {
        public string TotalPendingAmount { get; set; }
    }
}