﻿namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberTransactionFilterVM : FilterModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string TerminalId { get; set; }
        public string TotalAmount { get; set; }
        public string CardNumber { get; set; }
    }
}