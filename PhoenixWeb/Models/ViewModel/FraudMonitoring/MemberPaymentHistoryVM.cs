﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberPaymentHistoryVM
    {
        public string OfficeName { get; set; }
        public string TransactionDatetime { get; set; }
        public string TotalAmount { get; set; }
        public string Status { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string Bsb { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string FailedDatetime { get; set; }
    }

    public class Latestpayment
    {
        public string LastSuccessPaymentDate { get; set; }
        public string LastSuccessPaidAmount { get; set; }
    }
}