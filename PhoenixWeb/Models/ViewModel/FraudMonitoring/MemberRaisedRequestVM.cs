﻿namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberRaisedRequestVM
    {
        public int RaiseRequestID { get; set; }
        public int RaiseRequestTypeID { get; set; }
        public string RaiseRequestTypeName { get; set; }
        public string ReasonDescription { get; set; }
        public string Amount { get; set; }
        public string Fee { get; set; }
        public string TotalTransactionAmount { get; set; }
        public string RequestStatus { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ReferenceNumber { get; set; }
        public string FreshdeskTicketNo { get; set; }
        public string FreshdeskTicketURL { get; set; }
        public string RequestNote { get; set; }
        public string TransactionDate { get; set; }
        public string CardNumber { get; set; }
        public string ProcessedDate { get; set; }
        public string FailedDate { get; set; }
        public string LodgementReferenceNo { get; set; }
        public int DocketKey { get; set; }
        public bool IsCancelRequest { get; set; }
        public bool IsReverseRequest { get; set; }
    }
}