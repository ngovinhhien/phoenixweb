﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberOverviewVM
    {
        public List<MemberBankAccount> BankAccount { get; set; }
        public List<MemberTerminalAllocation> TerminalAllocation { get; set; }
    }

    public class MemberBankAccount
    {
        public string Bsb { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string PurposeTypeName { get; set; }
    }

    public class MemberTerminalAllocation
    {
        public string TerminalId { get; set; }
        public string LatestStatus { get; set; }
        public string VehicleId { get; set; }
        public string StateName { get; set; }
        public string VehicleTypeName { get; set; }
        public string EffectiveDate { get; set; }
    }
}