﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class TransactionDetail
    {
        public int DocketKey { get; set; }
        public int MemberKey { get; set; }
        public int CompanyKey { get; set; }
        public string MemberId { get; set; }
        [Display(Name = "Business Type")]
        public string BusinessType { get; set; }
        [Display(Name = "TID")]
        public string TerminalId { get; set; }
        [Display(Name = "Transaction Date")]
        public string TransactionDate { get; set; }
        [Display(Name = "Response")]
        public string TransactionResponse { get; set; }
        [Display(Name = "Card Number")]
        public string CardNumber { get; set; }
        [Display(Name = "Mode")]
        public string CardMode { get; set; }
        [Display(Name = "Payment Status")]
        public string PaymentStatus { get; set; }
        [Display(Name = "Face Value")]
        public string FaceValue { get; set; }
        [Display(Name = "Extras")]
        public string Extras { get; set; }
        [Display(Name = "GST")]
        public string Gst { get; set; }
        [Display(Name = "Service Charge")]
        public string ServiceCharge { get; set; }
        [Display(Name = "Levy")]
        public string LevyCharge { get; set; }
        [Display(Name = "Total Amount")]
        public string TotalAmount { get; set; }
        [Display(Name = "Member")]
        public string Member { get; set; }
        public string TradingName { get; set; }
    }

    public class ChargebackTransactionDetail : TransactionDetail
    {
        public decimal OriginalAmount { get; set; }
        public decimal ChargedbackAmount { get; set; }
        [Display(Name = "Chargeback Amount")]
        [Required]
        public decimal ChargebackAmount { get; set; }
        [Display(Name = "Chargeback Fee")]
        public decimal ChargebackFee { get; set; }
        [Display(Name = "Chargeback Reason")]
        [Required]
        public int ChargebackReasonKey { get; set; }
        [Display(Name = "Other Reason")]
        public string OtherChargebackReason { get; set; }
        [Display(Name = "Dispute Reference No.")]
        public string DisputeReferenceNo { get; set; }
        [Display(Name = "Freshdesk Ticket No.")]
        public string FreshdeskTicketNo { get; set; }
        [Display(Name = "Notes")]
        public string ChargebackNote { get; set; }
        public List<SelectListItem> ChargebackReasons { get; set; }
        public string Error { get; set; }
    }

    public class ChargebackData
    {
        public List<SelectListItem> ChargebackReasons { get; set; }
        public ChargebackTransactionDetail ChargebackTransactionDetail { get; set; }
    }
}