﻿namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class FraudTransaction
    {
        public string MemberId { get; set; }
        public string TradingName { get; set; }
        public string BusinessTypeName { get; set; }
        public string TerminalId { get; set; }
        public decimal TransactionTotal { get; set; }
        public string TransactionDate { get; set; }
        public string FraudReason { get; set; }
        public string CardEntryMode { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string BankName { get; set; }
        public string BankCountryName { get; set; }
        public string mccCode { get; set; }
        public string mccCategory { get; set; }
        public string TransactionStatus { get; set; }
        public string FileDate { get; set; }
        public int EftDocketKey { get; set; }
        public int MemberKey { get; set; }
        public bool IsQuarantined { get; set; }
        public string PaymentStatus { get; set; }
        public bool IsQuarantinable { get; set; }
        public bool IsRelease { get; set; }
    }
}