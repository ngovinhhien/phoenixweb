﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberNoteVM
    {
        public string CreatedDate { get; set; }
        public string NoteType { get; set; }
        public string NoteDesciption { get; set; }
        public string CreatedBy { get; set; }
        public int MemberKey { get; set; }
    }
}
