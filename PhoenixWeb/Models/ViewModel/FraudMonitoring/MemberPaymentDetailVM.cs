﻿namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberPaymentDetailVM
    {
        public string TerminalId { get; set; }
        public string BatchNumber { get; set; }
        public string TransactionDate { get; set; }
        public string FileDate { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string RateUsed { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string LodgementRefNo { get; set; }
        public string ProcessedDate { get; set; }
        public string ProcessedBy { get; set; }
    }
}