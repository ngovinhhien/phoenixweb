﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class MemberReviewVM
    {
        public Member member { get; set; }
        public List<MstAssociatedMID> mstAssociatedMIDs { get; set; }
    }
    public class Member
    {
        [Display(Name = "MID")]
        public string MemberId { get; set; }
        public int MemberKey { get; set; }
        [Display(Name = "Merchant ID")]
        public string MerchantID { get; set; }
        [Display(Name = "Amex Merchant ID")]
        public string AmexMerchantID { get; set; }
        public string TradingName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "DOB")]
        public string dob { get; set; }
        [Display(Name = "Address")]
        public string PrimaryAddress { get; set; }
        [Display(Name = "City/Suburb")]
        public string PrimarySuburb { get; set; }
        [Display(Name = "State")]
        public string PrimaryStateName { get; set; }
        [Display(Name = "Postcode")]
        public string PrimaryPostcode { get; set; }
        [Display(Name = "Business Type")]
        public string BusinessTypeName { get; set; }
        [Display(Name = "ABN")]
        public string abn { get; set; }
        [Display(Name = "ACN")]
        public string acn { get; set; }
        [Display(Name = "Dealer")]
        public string DealerName { get; set; }
        [Display(Name = "MCC")]
        public string mcc { get; set; }
        [Display(Name = "Active User")]
        public bool IsActive { get; set; }
        [Display(Name = "Payment Type")]
        public string PaymentTypeName { get; set; }
        [Display(Name = "Payment Report")]
        public bool ReceivePaymentReport { get; set; }
        [Display(Name = "Monthly Report")]
        public bool ReceiveMonthlyReport { get; set; }
        [Display(Name = "EOM MSF Payment")]
        public bool eomMSFPayment { get; set; }
        [Display(Name = "Average Transaction")]
        public string AverageTransaction { get; set; }
        [Display(Name = "Average MCC Transaction")]
        public string AverageMCCTransaction { get; set; }
        [Display(Name = "Highest Transaction")]
        public string HighestTransaction { get; set; }
        [Display(Name = "Lowest Transaction")]
        public string LowestTransaction { get; set; }
        [Display(Name = "Average Refund Transaction")]
        public string AverageRefundTransaction { get; set; }
        [Display(Name = "Chargeback Count")]
        public int ChargeBackCount { get; set; }
        [Display(Name = "Total Chargeback Value")]
        public string ChargeBackValue { get; set; }
        [Display(Name = "Merchant Duration")]
        public string MerchantDuration { get; set; }
    }
    public class MstAssociatedMID
    {
        public string AssociateMemberId { get; set; }
        public string AssociateReason { get; set; }
        public int AssociateMemberKey { get; set; }
    }
}
