﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class FraudTransactionFilter : FilterModel
    {
        [Display(Name = "File Dates")]
        public string FromDate { get; set; }

        [Display(Name = "To")]
        public string ToDate { get; set; }

        [Display(Name = "Quarantine Status")]
        public string QuarantineStatus { get; set; }

        [Display(Name = "Transaction Status")]
        public string TransactionStatus { get; set; }

        [Display(Name = "Business Type")]
        public int BusinessTypeKey { get; set; }

        [Display(Name = "Search")]
        public string SearchText { get; set; } = string.Empty;

        [Display(Name = "Fraud Alert")]
        public int FraudAlertKey { get; set; }


        // Render data
        public List<SelectListItem> ListBusinessType { get; set; }
        public List<SelectListItem> ListFraudAlert { get; set; }
        public List<SelectListItem> ListQuarantineStaus { get; set; }
        public List<SelectListItem> ListTransactionStatus { get; set; }
    }
}
