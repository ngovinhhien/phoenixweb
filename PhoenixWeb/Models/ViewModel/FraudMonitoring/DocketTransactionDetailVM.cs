﻿using System.ComponentModel.DataAnnotations;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class DocketTransactionDetailVM
    {
        public int DocketKey { get; set; }

        [Display(Name = "Member")]
        public string Member { get; set; }

        public int MemberKey { get; set; }

        public string MemberId { get; set; }

        public string TradingName { get; set; }

        public int CompanyKey { get; set; }

        [Display(Name = "Business Type")]
        public string BusinessType { get; set; }

        public string MemberType { get; set; }

        [Display(Name = "Terminal Id")]
        public string TerminalId { get; set; }

        [Display(Name = "Transaction Date")]
        public string TransactionDate { get; set; }

        [Display(Name = "Response")]
        public string TransactionResponse { get; set; }

        [Display(Name = "Card Type")]
        public string CardType { get; set; }

        [Display(Name = "Card Number")]
        public string CardNumber { get; set; }

        [Display(Name = "Cardholder Name")]
        public string CardHolderName { get; set; }

        [Display(Name = "Mode")]
        public string CardMode { get; set; }

        [Display(Name = "Fare")]
        public string FareAmountTaxi { get; set; }

        [Display(Name = "Extras")]
        public string ExtrasAmountTaxi { get; set; }

        [Display(Name = "Tip Pro")]
        public string AutoTipAmountTaxi { get; set; }

        [Display(Name = "Total Fare")]
        public string TotalFareTaxi { get; set; }

        [Display(Name = "Levy")]
        public string LevyChargeAmountTaxi { get; set; }

        [Display(Name = "Tip Fee")]
        public string AutoTipFeeAmountTaxi { get; set; }

        [Display(Name = "Amount")]
        public string TransactionAmountLE { get; set; }

        [Display(Name = "MSF/Commission")]
        public string MSF { get; set; }

        [Display(Name = "Total Paid")]
        public string TotalPaidAmount { get; set; }

        [Display(Name = "Payment Status")]
        public string PaymentStatus { get; set; }

        [Display(Name = "Processed Date")]
        public string ProcessedDate { get; set; }

        [Display(Name = "Failed Date")]
        public string FailedDate { get; set; }

        [Display(Name = "Lodgement Ref. No.")]
        public string LodgementReferenceNo { get; set; }

        public string DisplayFields { get; set; }
    }
  
    public class TransactionNote
    {
        public int DocketKey { get; set; }

        public string NoteDescription { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedDate { get; set; }
    }

    public class RelatedTransaction
    {
        public int RaiseRequestID { get; set; }

        public int RaiseRequestTypeID { get; set; }

        [Display(Name = "Type")]
        public string RaiseRequestTypeName { get; set; }

        [Display(Name = "Amount")]
        public string RequestAmount { get; set; }

        [Display(Name = "Reason")]
        public string ReasonDescription { get; set; }

        [Display(Name = "Dispute Ref. No.")]
        public string ReferenceNumber { get; set; }

        [Display(Name = "Freshdesk Ticket No.")]
        public string FreshdeskTicketNo { get; set; }
        public string FreshdeskTicketURL { get; set; }

        [Display(Name = "Note")]
        public string RequestNote { get; set; }

        public string MSF { get; set; }

        public string AutoTipFeeAmount { get; set; }

        public string LevyChargeAmount { get; set; }

        [Display(Name = "Fee")]
        public string FeeAmount { get; set; }

        [Display(Name = "Status")]
        public string RequestStatus { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        [Display(Name = "Processed Date")]
        public string ProcessedDate { get; set; }

        [Display(Name = "Failed Date")]
        public string FailedDate { get; set; }

        [Display(Name = "Lodgement Ref. No.")]
        public string LodgementReferenceNo { get; set; }

        public int DocketKey { get; set; }

        public bool IsCancelRequest { get; set; }

        public bool IsReverseRequest { get; set; }
    }
}