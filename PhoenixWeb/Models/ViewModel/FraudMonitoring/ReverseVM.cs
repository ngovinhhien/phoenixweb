﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel.FraudMonitoring
{
    public class ReverseTransactionDetail : TransactionDetail
    {
        [Display(Name = "Amount")]
        [Required]
        public string ReverseAmount { get; set; }
        [Display(Name = "Reason")]
        [Required]
        public int ReverseReasonKey { get; set; }
        [Display(Name = "Other Reason")]
        public string OtherReverseReason { get; set; }
        [Display(Name = "Freshdesk Ticket No.")]
        public string FreshdeskTicketNo { get; set; }
        [Display(Name = "Notes")]
        public string ReverseNote { get; set; }
        public List<SelectListItem> ReverseReasons { get; set; }
        public string Error { get; set; }
    }

    public class ReverseData
    {
        public List<SelectListItem> ReverseReasons { get; set; }
        public ReverseTransactionDetail ReverseTransactionDetail { get; set; }
    }
}