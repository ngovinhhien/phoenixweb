﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class CashingReportVM
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }

    public class ManualVoucherReportVM
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}