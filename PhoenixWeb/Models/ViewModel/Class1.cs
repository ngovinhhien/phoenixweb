﻿using PhoenixObjects.Rewards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class MissingTransactionUploadModel
    {

        public MissingTransactionUploadModel()
        {
            FileRecords = new List<LoyaltyUploadFileModel>();
        }

        public string FileName { get; set; }


        public List<LoyaltyUploadFileModel> FileRecords { get; set; }

        public bool AllowImport { get; set; }

        public IEnumerable<SelectListItem> OrganisationforLoyaltySelectedItem
        {
            get
            {

                //return OrganisationforLoyaltyModel.Select(item => new SelectListItem
                //{
                //    Text = item.Name,
                //    Value = item.OrganisationforLoyaltyId.ToString()
                //}).ToList();

                return new List<SelectListItem>();
            }
        }
    }
}