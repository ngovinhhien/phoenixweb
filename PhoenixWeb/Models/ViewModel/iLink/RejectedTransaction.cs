﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.iLink
{
    public class RejectedTransaction
    {
        public string TerminalNumber { get; set; }
        public string ProcessingDate { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public int TransactionNumber { get; set; }
        public string TransactionTime { get; set; }
        public string TransactionAmount { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string AccountType { get; set; }
        public string AuthorisationNumber { get; set; }
        public string IsVoid { get; set; }
        public string CardEntryMethod { get; set; }
        public string MerchantBSBAccountNumber { get; set; }
        public string MerchantName { get; set; }
        public string MerchantLocation { get; set; }
        public string CashoutOrTipAmount { get; set; }
    }
}