﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.iLink
{
    public class iLinkFileVM
    {
        public Int64 RowID { get; set; }
        public int ID { get; set; }
        public string Filename { get; set; }
        public string StatusCode { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<double> FileSize { get; set; }
        public string FileType { get; set; }
        public string URL { get; set; }
        public Nullable<System.DateTime> DownloadedDate { get; set; }
        public Nullable<int> TotalTransaction { get; set; }
        public Nullable<int> TotalImportedTransaction { get; set; }
        public Nullable<int> TotalRejectedTransaction { get; set; }
        public Nullable<decimal> TotalAmountImportedTransaction { get; set; }
        public Nullable<decimal> TotalAmountRejectedTransaction { get; set; }
        public Nullable<System.DateTime> ImportedDate { get; set; }
    }
}