﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.iLink
{
    public class ILinkFileManagementSearchVM
    {
        [Required]
        [Display(Name = "Download Date From")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "To")]
        public DateTime EndDate { get; set; }

        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Display(Name = "File Status")]
        public string Status { get; set; }

        [Display(Name = "File Type")]
        public int? FileType { get; set; }

        public string SortOrder { get; set; }
    }
}