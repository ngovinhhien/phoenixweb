﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.iLink
{
    public class ILinkTransactionVM
    {
        public int TransactionNumber { get; set; }
        public string MerchantName { get; set; }
        public string MemberID { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public string ApproveStatus { get; set; }
        public string Amount { get; set; }
        public string CashOut { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public string AuthorisationNumber { get; set; }
        public string BatchNumber { get; set; }
        // public string Docket { get; set; } //bug
        public string CardType { get; set; }
        public string AccountType { get; set; }
        public string CardNumber { get; set; }
        public string FileName { get; set; }
        public string CardEntryMode { get; set; }
        public string IsVoid { get; set; }
        public DateTime ImportedDate { get; set; }
        public string MerchantBsbAccNumber { get; set; }
    }
}