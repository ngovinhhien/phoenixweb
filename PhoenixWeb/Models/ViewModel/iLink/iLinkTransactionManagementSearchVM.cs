﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.iLink
{
    public class ILinkTransactionManagementSearchVM
    {
        [Display(Name = "Transaction Date From")]
        public string TransactionDateFrom { get; set; }

        [Display(Name = "To")]
        public string TransactionDateTo { get; set; }

        [Display(Name = "Imported Date From")]
        public string ImportedDateFrom { get; set; }

        [Required]
        [Display(Name = "To")]
        public string ImportedDateTo { get; set; }

        [Display(Name = "Card No. Masked")]
        public string CardNumber { get; set; }

        [Display(Name = "Transaction No.")]
        public string TransactionNumber { get; set; }

        [Display(Name = "Authorisation")]
        public string AuthorisationNumber { get; set; }

        [Display(Name = "Member ID")]
        public string MemberID { get; set; }

        [Display(Name = "Terminal ID")]
        public string TerminalID { get; set; }

        [Display(Name = "Card Type")]
        public string CardType { get; set; }

        [RegularExpression("([1-9][0-9]*[.][0-9]{1,2})")]
        [Display(Name = "Amount From")]
        public string AmountFrom { get; set; }

        [RegularExpression("([1-9][0-9]*[.][0-9]{1,2})")]
        [Display(Name = "To")]
        public string AmountTo { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchNumber { get; set; }

        public bool Approved { get; set; }

        public bool Declined { get; set; }

        [Display(Name = "Source File Name")]
        public string SourceFile { get; set; }

        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }

        public string IsVoid { get; set; }

        public string SortField { get; set; }

        public string SortOrder { get; set; }
    }
}