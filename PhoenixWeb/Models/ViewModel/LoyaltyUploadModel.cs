﻿using PhoenixObjects.Rewards;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class LoyaltyUploadModel
    {

        public LoyaltyUploadModel ()
        {
            FileRecords = new List<LoyaltyUploadFileModel>();
        }

        public string FileName { get; set; }

        
        public List<LoyaltyUploadFileModel> FileRecords { get; set;  }


        public string SelectionOrganisationId { get; set; }

        public IEnumerable<OrganisationforLoyaltyModel> OrganisationforLoyaltyModel { get; set; }


        public bool AllowImport { get; set; }

        public IEnumerable<SelectListItem> OrganisationforLoyaltySelectedItem
        {
            get
            {
                return OrganisationforLoyaltyModel.Select(item => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.OrganisationforLoyaltyId.ToString()
                }).ToList();
            }
        }
    }
}