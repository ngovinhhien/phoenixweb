﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class PaymentHistory
    {
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string LodgementReferenceNo { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> Modified_dttm { get; set; }
        public string ModifiedByUser { get; set; }
        public byte[] DBTS { get; set; }
        public Nullable<int> Account_key { get; set; }
        public int Transaction_key { get; set; }
        public Nullable<int> Office_key { get; set; }
        public Nullable<int> TransactionType_key { get; set; }
        public Nullable<int> PayRun_key { get; set; }
        public bool IsBankPayment { get { return this.PayRun_key.HasValue; } }

    }
}