﻿using PhoenixObjects.Rewards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class iLinkAutomationConfigVM
    {
        public string iLinkAllowedFileExtensions { set; get; }
        public string iLinkFilenameConvention { set; get; }
        public string iLinkAutomaticErrorReceivers { set; get; }
        public string iLinkAutomaticResultReceivers { set; get; }
        public Dictionary<string, string> toDictionary()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                { "iLinkAllowedFileExtensions", iLinkAllowedFileExtensions },
                { "iLinkFilenameConvention", iLinkFilenameConvention },
                { "iLinkAutomaticErrorReceivers", iLinkAutomaticErrorReceivers },
                { "iLinkAutomaticResultReceivers", iLinkAutomaticResultReceivers },
            };
            return dict;
        }
    }
}