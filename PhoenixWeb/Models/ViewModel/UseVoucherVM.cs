﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhoenixObjects.Vouchers;
using System.ComponentModel.DataAnnotations;

namespace PhoenixWeb.Models.ViewModel
{
    public class UseVoucherVM
    {
        public int Member_key { get; set; }
        public string MemberId { get; set; }
        public string VoucherNumber { get; set; }
        public string Name { get; set; }
       
    }
}