﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhoenixObjects.Administrator;

namespace PhoenixWeb.Models.ViewModel
{
    public class RolesPermissionVM
    {
        public List<RoleModel> RoleList { get; set; }
    }
}