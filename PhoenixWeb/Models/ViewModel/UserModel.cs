﻿using PhoenixObjects.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class UserForgotPasswordRequestViewModel : UserResetPasswordRequestModel
    {

        public string Username { get; set; }
        public string GoogleAuthToken { get; set; }
    }


    public class UserResetPasswordRequestViewModel : UserResetPasswordRequestModel
    {

        public string Username { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordConfirm { get; set; }    
        public bool IsAdminChangePassword { get; set; }
        public bool IsValidRequest { get; set; }

    }

    public class UserResendTokenViewModel : UserResetPasswordRequestModel
    {

        public string Username { get; set; }

        public string Email { get; set; }
    }
}