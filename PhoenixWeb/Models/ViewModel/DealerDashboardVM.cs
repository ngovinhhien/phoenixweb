﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class DealerDashboardVM
    {
        public int OperatorCount { get; set; }
        public int TerminalCount { get; set; }
        public MemberModel Dealer { get; set; }
        public List<PerformanceOperatorModel> WeeklyTopPerformers { get; set; }
        public List<PerformanceOperatorModel> WeeklyLeastPerformers { get; set; }
        public List<PerformanceOperatorModel> MonthlyTopPerformers { get; set; }
        public List<PerformanceOperatorModel> MonthlyLeastPerformers { get; set; }
        public List<PerformanceOperatorModel> NoTransactionIn7Days { get; set; }
        public List<PerformanceOperatorModel> NoTransactionIn30Days { get; set; }
        public List<PerformanceOperatorModel> WeeklyTop5PerformersByTerminal { get; set; }
        public List<PerformanceOperatorModel> MonthlyTop5PerformersByTerminal { get; set; }
        public List<PerformanceOperatorModel> WeeklyLeast5PerformersByTerminal { get; set; }
        public List<PerformanceOperatorModel> MonthlyLeast5PerformersByTerminal { get; set; }

    }
}