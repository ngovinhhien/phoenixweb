﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class ChangeActivitySearchModel
    {
        public int MemberKey { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public string Username { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }

    }
}