﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class BankAccountModelVM
    {
        public int Account_Key { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public int Weight { get; set; }
        public string MemberID { get; set; }
    }
}