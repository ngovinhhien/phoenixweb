﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhoenixObjects.Vouchers;
using System.ComponentModel.DataAnnotations;

namespace PhoenixWeb.Models.ViewModel
{
    public class RedeemVoucherVM
    {
        public int Member_key { get; set; }
        public string MemberId { get; set; }
        public string Name { get; set; }
        [Display(Name="Points Earned")]
        public decimal TotalPointEarned { get; set; }
           [Display(Name = "Points Used")]
        public decimal TotalPointUsed { get; set; }
           [Display(Name = "Points Available")]
        public decimal TotalPointAvailable { get; set; }
        public bool Override { get; set; }
        public int VoucherTypekey { get; set; }
        public List<SPGetVoucherByMemberKeyModel> HistoryVouchers { get; set; }
    }
}