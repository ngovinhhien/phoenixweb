﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.DriverCard
{
    public class DriverCardInfoVM
    {
        public string accountIdentifier { get; set; }
        public int memberKey { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string postCode { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public DateTime DOB { get; set; }
        public string mobile { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string changedContent { get; set; }
    }
}