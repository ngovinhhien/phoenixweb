﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel.DriverCard
{
    public class TransferHistoryVM
    {
        public string LodgementRefNo { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PendingAmountForSelectedPayment { get; set; }
        public decimal PendingBalanceForMID { get; set; }
        public List<TransferHistoryDataVM> TransferHistories { get; set; }
        public string MemberID { get; set; }
    }

    public class TransferHistoryDataVM
    {
        public string TransactionDate { get; set; }
        public decimal SentAmount { get; set; }
        public string PaymentStatus { get; set; }
    }
}