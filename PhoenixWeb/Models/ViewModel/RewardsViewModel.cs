﻿using PhoenixObjects.Rewards;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class RewardsViewModel
    {
        [Required] [Display(Name = "Qantas File name")]
        public string SelectedFileName { get; set; }
        public IEnumerable<SelectListItem> FileNames { get; set; }

        public string OrganisationId  { get; set; }
        public IEnumerable<SelectListItem> Organisations { get; set; }

        public string SelectionAction { get; set; }
        public IEnumerable<SelectListItem> ApproveRejectAction
        {
            get
            {
                List<SelectListItem> selectListItem = new List<SelectListItem>()
                {
                    new SelectListItem () {  Text = "---- Select ----",  Value = "0" },
                    new SelectListItem () {  Text = "Reject",  Value = "1" },
                    new SelectListItem () {  Text = "Ready to be send to QBR",  Value = "2" },
                };

                return selectListItem;
            }
        }
    }
}