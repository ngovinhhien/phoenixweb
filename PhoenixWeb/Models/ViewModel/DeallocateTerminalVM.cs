﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class DeallocateTerminalVM
    {
        public string EffectiveDateString { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public string TerminalId { get; set; }
        public int TerminalAllocationKey { get; set; }
        public int MemberKey { get; set; }
    }
}