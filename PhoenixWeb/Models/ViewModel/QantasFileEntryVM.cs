﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasFileEntryVM
    {
        public int QantasFileEntryKey { get; set; }
        public Nullable<int> QantasExportFileKey { get; set; }
        public Nullable<int> MemberReferenceEntryKey { get; set; }
        public Nullable<int> Points { get; set; }
        public string UploadStatus { get; set; }
        public string QBRNumber { get; set; }
        public Nullable<int> QantasExportFileEntryStatusId { get; set; }

        public virtual QantasExportFileEntryStatusVM QantasExportFileEntryStatus { get; set; }
    }    
}
