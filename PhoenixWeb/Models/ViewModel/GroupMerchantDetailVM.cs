﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class GroupMerchantDetailVM : VMBaseWithPaging
    {
        public int GroupID { get; set; }
        public int MerchantID { get; set; }
    }
}