﻿using System.ComponentModel.DataAnnotations;

namespace PhoenixWeb.Models.ViewModel
{
    public class MissingTransactionFromWestPacVM
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string SortName { get; set; }
        public string SortOrder { get; set; }

        [Display(Name = "Terminal ID")]
        public string TerminalID { get; set; }

        [Display(Name = "Batch Sequence")]
        public string BatchSequence { get; set; }

        [Display(Name = "From Date")]
        public string TransactionStartDate { get; set; }

        [Display(Name = "To Date")]
        public string TransactionEndDate { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchID { get; set; }

        [Display(Name = "File Date")]
        public string FileDate { get; set; }

        public string SelectedTransactionIds { get; set; }

    }
}