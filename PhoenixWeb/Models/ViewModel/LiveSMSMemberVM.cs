﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class LiveSMSMemberVM
    {
        public string MemberId { get; set; }
        public string Username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int plan_id { get; set; }
        public int credits { get; set; }
        public string secret_key { get; set; }
    }
}