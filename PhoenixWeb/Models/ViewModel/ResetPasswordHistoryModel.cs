﻿namespace PhoenixWeb.Models.ViewModel
{
    public class ResetPasswordHistoryModel : VMBaseWithPaging
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}