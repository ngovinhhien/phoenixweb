﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class AllMemberNoteVM
    {
        public List<MemberNotesModel> MemberNotes { get; set; }
    }
}