﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Phoenix_Service;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasPointUploadVM
    {
        public QantasPointUploadVM()
        {
            BusinessTypes = new List<SelectListItem>();
            LoyaltyPromotions = new List<SelectListItem>();
        }

        [Required]
        [Display(Name = "Loyalty Promotion")]
        public IEnumerable<SelectListItem> LoyaltyPromotions { get; set; }
        public int LoyaltyPromotionId { get; set; }

        [Required]
        [Display(Name = "Business Type")]
        public IEnumerable<SelectListItem> BusinessTypes { get; set; }
        public int BusinessTypeId { get; set; }

        public string EntryList { get; set; }
    }
}