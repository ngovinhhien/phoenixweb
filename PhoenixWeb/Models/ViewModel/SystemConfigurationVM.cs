﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class SystemConfigurationVM
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}