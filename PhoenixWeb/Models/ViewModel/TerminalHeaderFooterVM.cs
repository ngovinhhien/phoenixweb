﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class TerminalHeaderFooterVM
    {
        public string HeaderLineOne { get; set; }
        public string HeaderLineTwo { get; set; }
        public string HeaderLineThree { get; set; }
        public string HeaderLineFour { get; set; }


        public string FooterLineOne { get; set; }
        public string FooterLineTwo { get; set; }
        public string FooterLineThree { get; set; }
        public string FooterLineFour { get; set; }
        public string TerminalId { get; set; }
    }
}