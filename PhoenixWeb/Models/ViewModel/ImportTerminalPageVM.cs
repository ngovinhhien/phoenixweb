﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoenixWeb.Models.ViewModel
{
    public class ImportTerminalPageVM
    {
        public IEnumerable<SelectListItem> BusinessType { get; set; }
        public int BusinessTypeId { get; set; }
        public string TerminalList { get; set; }
    }
}