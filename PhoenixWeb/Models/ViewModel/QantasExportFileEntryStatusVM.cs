﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasExportFileEntryStatusVM
    {
        public QantasExportFileEntryStatusVM()
        {
            this.QantasFileEntries = new HashSet<QantasFileEntryVM>();
        }

        public int QantasExportFileEntryStatusId { get; set; }
        public string Name { get; set; }
        public Nullable<int> NextStatus { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<QantasFileEntryVM> QantasFileEntries { get; set; }
    }
}
