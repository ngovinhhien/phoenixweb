﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.ViewModel
{
    public class DeclinedTransactionVM
    {
        [Required]
        [Display(Name = "Terminal ID")]
        public string TerminalID { get; set; }

        [Required]
        [Display(Name = "Transaction Start Date")]
        public string TransactionStartDate { get; set; }

        [Required]
        [Display(Name = "Transaction End Date")]
        public string TransactionEndDate { get; set; }
    }
}