﻿using PhoenixObjects.Member;
using System;
using System.Collections.Generic;

namespace PhoenixWeb.Models.ViewModel.Member.Search
{
    public class SearchMemberTerminalAllocationVM
    {
        public SearchMemberTerminalAllocationVM()
        {
            AllVehicleTerminalAllocation = new List<AllVehicleTerminalAllocationModel>();
        }

        public List<AllVehicleTerminalAllocationModel> AllVehicleTerminalAllocation { get; set; }
        public bool IsError { get; set; }
    }
}