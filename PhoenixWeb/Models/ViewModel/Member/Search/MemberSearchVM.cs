﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoenixObjects.Common;
using PhoenixObjects.Exceptions;
//using System.Web.Mvc;
using PhoenixObjects.Financial;
using PhoenixObjects.Terminal;
using PhoenixObjects.MyWeb;
using PhoenixObjects.Report;
using PhoenixObjects.Customer;
using PhoenixObjects.Transaction;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using PhoenixObjects.Fee;
using PhoenixObjects.Member.CommissionRate;
using PhoenixObjects.Member.DriverCard;

namespace PhoenixWeb.Models.ViewModel.Member.Search
{
    public class MemberSearchVM
    {
        public List<SelectListItem> PaymentTypes { get; set; }
        public List<SelectListItem> BusinessTypes { get; set; }
        public List<SelectListItem> QBRStatuses { get; set; }
        public List<SelectListItem> Dealers { get; set; }

        public int CompanyKey { get; set; }
        public int DealerKey { get; set; }
        public int PaymentKey { get; set; }
        public int QBRStatusKey { get; set; }
        public string SearchKeyword { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDateFrom { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDateTo { get; set; }
    }
}