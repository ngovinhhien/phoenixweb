﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoenixWeb.Models.ViewModel
{
    public class QantasExportFileApproverStatusVM
    {
        public QantasExportFileApproverStatusVM()
        {
            this.QantasExportFileApprovers = new HashSet<QantasExportFileApproverVM>();
        }

        public int QantasExportFileApproverStatusId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<QantasExportFileApproverVM> QantasExportFileApprovers { get; set; }


    }


    public class LoyaltyExportFileApproverStatusVM
    {
        public LoyaltyExportFileApproverStatusVM()
        {
            this.LoyaltyExportFileApprovers = new HashSet<LoyaltyExportFileApproverVM>();
        }

        public int QantasExportFileApproverStatusId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<LoyaltyExportFileApproverVM> LoyaltyExportFileApprovers { get; set; }


    }


}
