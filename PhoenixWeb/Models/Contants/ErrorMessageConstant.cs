﻿namespace PhoenixWeb.Models.Contants
{
    public static class ErrorMessageConstant
    {
        public const string SystemError = "System error, please try again. If this error continues to occur, please contact IT Support.";
        public const string CommonSystemError = "An error happened while processing your request. Please try again.";
    }

    public static class GlideBoxCategoryMessageConstant
    {
        public const string CategoryAssignedToTerminals = "This category cannot be de-activated, as it is enabled on some terminals. ";
        public const string PriceLargerCommissionAmount = "The Category Price must larger than the Category Commission amount.";
        public const string InvalidConfigMaxCategoryCanCreate = "The category limit has not been configured. Please contact IT Support to configure the category limit.";
        public const string CreateCategoryLargerConfig = "The category limit has been exceeded. Please contact IT Support to increase the category limit.";
        public const string CategoryCodeExists = "Category Code exists. Please choose a unique Category Code.";
        public const string CategoryNameExists = "Category name exists. Please choose a unique Category Name.";
        public const string InvalidConfigMaxCategoryCanAssign = "Maximum amount of categories has not been assigned. Please contact IT Support.";
        public const string InvalidTerminalBulkAssign = "TerminalId list invalid.";
        public const string InvalidCategoryList = "CategoryId list invalid.";
        public const string InvalidCategoryOrder = "Category order is invalid.";
        public const string DuplicateCategoryOrder = "Category order is duplicate.";
        public const string InvalidTerminalInvidiualAssign = "TerminalId invalid.";
        public const string DuplicateCategoryId = "Category Id is duplicate.";
        public const string InvalidCategory = "Category invalid.";
        public const string MessageAddedCategoriesToTerminal = "The categories you’ve selected have been added to Glide Box for the following Terminals: ";
        public const string MessageInvalidTerminalList = "The terminal ID entered is not in the list of existing terminal IDs.";
        public const string MessageTerminalExistCategories = "The terminal ID entered already has a category/categories assigned to it.";
        public const string MessageTerminalNoSupport = "The terminal ID entered is not a valid Live taxi terminal ID.";
        public const string MessagePleaseAssignAgain = "Please remove these Terminal IDs and try again.";
    }

    public static class GlideBoxProductMessageConstant
    {
        public const string InvalidImageFile = "Image file not support.";
        public const string InvalidExtensionImageFile = "Please select image file with .png or .jpg format to upload.";
        public const string ImageSizeTooLarge = "Image size too large.";
        public const string ProductNameExists = "Product name exists. Please choose a unique Product Name.";
        public const string InvalidConfigGlideboxImageMaxSize = "The image max size has not been configured. Please contact IT Support to configure the image max size.";
    }
}