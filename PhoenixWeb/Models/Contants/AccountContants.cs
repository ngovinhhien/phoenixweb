﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.Contants
{
    public static class ResetPasswordSourceConstants
    {
        public const string SourceNameOfChangePasswordInProfileScreen = "Change password in the user profile screen";
        public const string SourceNameOfChangePasswordInResetPasswordRequestsScreen = "Reset by admin in Reset Password Requests screen";
        public const string SourceNameOfChangePasswordInResetPasswordLink = "Reset password link (phoenix web login page)";
        public const string SourceNameOfChangePasswordInPasswordExpiration = "Password expiration feature within PhoenixWeb";
        public const string SourceNameOfChangePasswordInViewAllUsersScreen = "Change password in the view all users screen";
    }
}