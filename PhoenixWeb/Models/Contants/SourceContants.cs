﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.Contants
{
    public static class SourceContants
    {
        public const string TerminalConfig = "Configuration";
        public const string TerminalBulkAdd = "BulkAdd";
        public const string TerminalBulkUpdate = "BulkUpdate";
        public const string BulkAssignCategory = "BulkAssignCategory";
        public const string IndividualAssignCategory = "IndividualAssignCategory";
        public const string AllocateTerminal = "AllocateTerminal";
        public const string SwapTerminal = "SwapTerminal";
    }
}