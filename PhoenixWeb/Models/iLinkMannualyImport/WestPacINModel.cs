﻿using System;

namespace PhoenixWeb.Models.iLinkMannualyImport
{
    public class WestPacINModel
    {
        public string status { get; set; }
        public string terminalnumber { get; set; }
        public string processingdatetype { get; set; }
        public string transactiondate { get; set; }
        public string transactiontype { get; set; }
        public int transactionnumber { get; set; }
        public string transactiontime { get; set; }
        public string transactionamount { get; set; }
        public string cardnumber { get; set; }
        public string cardtype { get; set; }
        public string accounttype { get; set; }
        public string authorisationnumber { get; set; }
        public string doubtfulltransactionflag { get; set; }
        public string voidtransactionflag { get; set; }
        public string tracktwo { get; set; }
        public string merchantbsbaccnumber { get; set; }
        public string merchantname { get; set; }
        public string merchantlocation { get; set; }
        public string screenphoneinformation { get; set; }
        public string cashoutortipamount { get; set; }
        public string sourcefile { get; set; }
        public Nullable<System.DateTime> recordcreated { get; set; }
        public Nullable<int> docket_key { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> ReconciledDate { get; set; }
        public string ReconciledBy { get; set; }
        public Nullable<int> BankReconReasonKey { get; set; }
        public Nullable<int> iLinkFileID { get; set; }
        public string RejectedReason { get; set; }
    }
}