﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.iLinkMannualyImport
{
    public class iLinkFileModel
    {
        public int ID { get; set; }
        public string FileName { get; set; }
        public float FileSize { get; set; }
        public string StatusCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public int FileTypeID { get; set; }
        public DateTime DownloadedDate { get; set; }
        public int TotalTransaction { get; set; }
        public int TotalImportedTransaction { get; set; }
        public int TotalRejectedTransaction { get; set; }
        public decimal TotalAmountImportedTransaction { get; set; }
        public string FileContent { get; set; }
        public string URL { get; set; }
        public string ErrorMessage { get; set; }
    }
}