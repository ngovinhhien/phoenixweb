﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Models.iLinkMannualyImport
{
    public class ConfigurationModel
    {
        //public string iLinkAutomaticErrorReceivers { get; set; }
        //public string iLinkAutomaticResultReceivers { get; set; }
        //public string iLinkSparkPostAPIKey { get; set; }
        //public string iLinkNotificationDownloadImportSuccess { get; set; }
        public string S3BucketAccessKey { get; set; }
        public string S3BucketSecretAccessKey { get; set; }
        public string S3BucketName { get; set; }
        public string DownloadedStatusCode { get; set; }
        public string ImportedStatusCode { get; set; }
        //public string iLinkImportSuccessTemplateID { get; set; }
        public string FailedStatusCode { get; set; }
        public string iLinkFilenameConvention { get; set; }
        public string iLinkAllowedFileExtensions { get; set; }
        public DateTime iLinkFileReleaseDate { get; set; }
    }
}