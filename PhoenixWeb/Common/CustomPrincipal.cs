﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace PhoenixWeb.Common
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }

        public CustomPrincipal(string email)
        {
            this.Identity = new GenericIdentity(email);
            Permissions = new List<string>();
        }

        public string Username { get; set; }
        public List<string> Permissions { get; set; }
        public bool IsInPermission(string Permission)
        {
            if (Permissions != null)
            {
                var rr = Permissions.Find(n => n == Permission);
                if (rr != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}