﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Common
{
    public class PagedModel<T>
    {
        public int total { get; set; }
        public int page { get; set; }
        public List<T> rows { get; set; }
        public int records { get; set; }
        public bool isError { get; set; }
        public string errorMessage { get; set; }
    }
}