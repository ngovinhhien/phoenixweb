﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhoenixWeb.ReportingWebService;
using System.Net.Mail;
using System.Web.Configuration;
using SparkPost;
using System.Net.Configuration;
using System.Net;
using System.Configuration;
using log4net;
using Phoenix_BusLog;
using PhoenixObjects.Enums;
using System.Text.RegularExpressions;

namespace PhoenixWeb.Common
{
    public class Common
    {
        private static readonly ILog mailLogger = LogManager.GetLogger("SparkPostEmail");

        public static void RenderReport(string reportName, IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters, out byte[] output, out string extension, out string mimeType, out string encoding, out Warning[] warnings, out string[] streamIds)
        {
            using (var webServiceProxy = new ReportExecutionServiceSoapClient("ReportExecutionServiceSoap"))
            {
                NetworkCredential clientCredentials = new NetworkCredential
                    (
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomainAdmin.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomainPassword.ToString()),
                            Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.ReportDomain.ToString())
                    );
                webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                webServiceProxy.ClientCredentials.Windows.ClientCredential = clientCredentials;

                // Init Report to execute
                ServerInfoHeader serverInfoHeader;
                ExecutionInfo executionInfo;
                ExecutionHeader executionHeader = webServiceProxy.LoadReport(null, reportName, null, out serverInfoHeader, out executionInfo);

                // Attach Report Parameters
                webServiceProxy.SetExecutionParameters(executionHeader, null, parameters.ToArray(), "en-au", out executionInfo);
                // Render
                webServiceProxy.Render(executionHeader, null, "PDF", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
            }

        }

        public static void SendEmail(dynamic mailDto)
        {
            try
            {
                System.Net.ServicePointManager.
                        SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                var transmission = new Transmission();
                var properties = (IDictionary<string, object>)mailDto;
                if (!properties.ContainsKey("TemplateID") || string.IsNullOrEmpty(mailDto.TemplateID))
                {
                    throw new Exception("Template ID not available when sending email with SparkPost");
                }
                transmission.Content.TemplateId = mailDto.TemplateID;
                if (!properties.ContainsKey("Recipient") || string.IsNullOrEmpty(mailDto.Recipient))
                {
                    throw new Exception("Recipient not available when sending email with SparkPost");
                }

                // add recipients who will receive your email
                var lstRecipient = mailDto.Recipient.Split(';');
                foreach (var rec in lstRecipient)
                {
                    var recipient = new Recipient
                    {
                        Address = new Address { Email = rec }

                    };
                    transmission.Recipients.Add(recipient);
                }

                if (properties.ContainsKey("CustomMetaData"))
                {
                    transmission.SubstitutionData = mailDto.CustomMetaData;
                }

                if (!properties.ContainsKey("SparkPostAPIKey") || string.IsNullOrEmpty(mailDto.SparkPostAPIKey))
                {
                    throw new Exception("SparkPostAPIKey not available when sending email with SparkPost");
                }

                // create a new API client using your API key
                var client = new Client(mailDto.SparkPostAPIKey);

                // if you do not understand async/await, use the sync sending mode:
                client.CustomSettings.SendingMode = SendingModes.Sync;
                var response = client.Transmissions.Send(transmission).Result;
                if (string.IsNullOrEmpty(response.Id))
                {
                    throw new Exception($"Can not sending email to {mailDto.Recipient}. Please check the response status code from SparkPost {response.StatusCode}");
                }
            }
            catch (Exception e)
            {
                mailLogger.Fatal($"Can not sending email", e);
                throw new Exception("Can not send the notificaiton email.");
            }

        }
    }

    public static class StringExtensions
    {
        public static string RemoveMultipleSpaces(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                Regex regex = new Regex("[ ]{2,}", RegexOptions.None);
                input = input.Trim();
                input = regex.Replace(input, " ");
            }

            return input;
        }
    }
}