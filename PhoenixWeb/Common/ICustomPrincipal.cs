﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace PhoenixWeb.Common
{
    interface ICustomPrincipal : IPrincipal
    {
        string Username { get; set; }
        List<string> Permissions { get; set; }
        bool IsInPermission(string Permission);
    }
}