﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoenixWeb.Common
{
    public class CustomPrincipalSerializeModel
    {
       
        public string Username { get; set; }
        public List<string> Permissions { get; set; }
    }
}