﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
namespace PhoenixWeb.Common
{
    public class ReportsResult : ActionResult
    {
        public ReportsResult(byte[] data, string mineType, string fileName = "")
        {
            this.Data = data;
            this.MineType = mineType;
            this._filename = fileName;
        }

        public byte[] Data { get; set; }
        public string MineType { get; set; }
        string _filename = "File.pdf";
        public string Filename 
        { 
            get 
            {
                if (string.IsNullOrEmpty(_filename))
                    _filename = "File.pdf";
                return _filename; 
            } 
        }

        public override void ExecuteResult(ControllerContext context)
        {
            MemoryStream ms = new MemoryStream(Data);
            context.HttpContext.Response.ContentType = "application/pdf";
          
            context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename="+Filename);
            context.HttpContext.Response.Buffer = true;
            ms.WriteTo(context.HttpContext.Response.OutputStream);

        }
    }
}