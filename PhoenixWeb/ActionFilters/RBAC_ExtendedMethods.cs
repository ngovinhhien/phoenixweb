﻿using Phoenix_BusLog;
using PhoenixObjects.Enums;
using PhoenixWeb.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//namespace PhoenixWeb.ActionFilters
//{
public static class RBAC_ExtendedMethods
{
    //public static List<UserRole> Roles = new List<UserRole>();
    public static Dictionary<string, RBACUser> rbaUsers = new Dictionary<string, RBACUser>();

    public static void ForceReloadRBAUsers()
    {
        rbaUsers = new Dictionary<string, RBACUser>();
    }

    public static RBACUser GetRBACUser(string username)
    {
        RBACUser rBACUser = null;

        if (rbaUsers.TryGetValue(username, out rBACUser))
        {
            return rBACUser;
        }
        else
        {
            RBACUser a = new RBACUser(username);
            rbaUsers.Add(username, a);
            return a;
        }
    }

    public static bool HasRole(this ControllerBase controller, string role)
    {
        bool bFound = false;
        try
        {

            RBACUser rBACUser = null;

            if (rbaUsers.TryGetValue(controller.ControllerContext.HttpContext.User.Identity.Name, out rBACUser))
            {
                bFound = rBACUser.HasRole(role);
            }
            else
            {
                var userName = controller.ControllerContext.HttpContext.User.Identity.Name;
                RBACUser a = new RBACUser(userName);
                bFound = a.HasRole(role);
                rbaUsers.Add(userName, a);
            }
        }
        catch
        {
            bFound = false;
        }
        return bFound;
    }

    public static string Username(this ControllerBase controller)
    {
        string bFound = "";
        try
        {
            bFound = controller.ControllerContext.HttpContext.User.Identity.Name;
        }
        catch { }
        return bFound;
    }



    public static bool HasRoles(this ControllerBase controller, string roles)
    {
        bool bFound = false;
        try
        {
            //Check if the requesting user has any of the specified roles...
            //Make sure you separate the roles using ; (ie "Sales Manager;Sales Operator"
            bFound = new RBACUser(controller.ControllerContext.HttpContext.User.Identity.Name).HasRoles(roles);
        }
        catch { }
        return bFound;
    }

    public static bool HasPermission(this ControllerBase controller, string permission)
    {
        bool bFound = false;
        try
        {

            RBACUser rBACUser = null;

            if (rbaUsers.TryGetValue(controller.ControllerContext.HttpContext.User.Identity.Name, out rBACUser))
            {
                bFound = rBACUser.HasPermission(permission);
            }
            else
            {
                var userName = controller.ControllerContext.HttpContext.User.Identity.Name;
                RBACUser a = new RBACUser(userName);
                bFound = a.HasPermission(permission);
                rbaUsers.Add(userName, a);
            }
        }
        catch
        {
            bFound = false;
        }
        return bFound;
    }

    public static bool IsDisplayUIControl(this ControllerBase controller, string requiredPermission, BusinessDomainEnum businessDomain)
    {
        switch (businessDomain)
        {
            case BusinessDomainEnum.Glidebox:
                bool.TryParse(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.DisplayGlideboxUI.ToString()), out bool isDisplay);

                if (isDisplay && requiredPermission == SystemConfigurationKeyEnum.DisplayGlideboxUI.ToString())
                {
                    return isDisplay;
                }
                break;
            default:
                break;
        }
        
        return false;
    }

    public static string GetSystemVersion(this ControllerBase controller)
    {
        var webversion = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.PhoenixWebVersion.ToString());
        if (string.IsNullOrEmpty(webversion))
        {
            return "1.0.0.0";
        }

        return webversion;
    }

    public static bool IsSysAdmin(this ControllerBase controller)
    {
        bool bIsSysAdmin = false;
        try
        {
            //Check if the requesting user has the System Administrator privilege...
            bIsSysAdmin = new RBACUser(controller.ControllerContext.HttpContext.User.Identity.Name).IsSysAdmin;
        }
        catch { }
        return bIsSysAdmin;
    }
}
//}