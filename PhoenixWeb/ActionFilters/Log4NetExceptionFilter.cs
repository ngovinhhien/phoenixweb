﻿using log4net;
using System.Web.Mvc;

namespace PhoenixWeb.ActionFilters
{
    public class Log4NetExceptionFilter : IExceptionFilter
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void OnException(ExceptionContext filterContext)
        {
            var ex = filterContext.Exception;
            while (ex != null)
            {
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
                ex = ex.InnerException;
            }
        }
    }
}