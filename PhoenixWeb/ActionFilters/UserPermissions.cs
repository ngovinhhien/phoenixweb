﻿using Phoenix_BusLog.Data;
using PhoenixWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;

namespace PhoenixWeb.ActionFilters
{
    public class UserPermissions : System.Web.Mvc.AuthorizeAttribute
    {
        public string Permission { get; set; }
        public string UserName { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool isAuthorized = AuthorizeCore(filterContext.HttpContext);
          
            if (!isAuthorized)
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            
            filterContext.Result =
           new RedirectToRouteResult(
               new RouteValueDictionary{{ "controller", "AdminHome" },
                                          { "action", "Permission" } });

        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }
            string per = Permission;
            IPrincipal user = httpContext.User;
            //CustomPrincipal u = (CustomPrincipal)httpContext.User;
            // bool hasPermission  = u.IsInPermission(per);
            PermissionsDA pda = new PermissionsDA();
            bool hasPermission = pda.HasPermission(user, per);
          
            if (!hasPermission)
            {

                return false;

            }
            return true;

        }
     
    }
}