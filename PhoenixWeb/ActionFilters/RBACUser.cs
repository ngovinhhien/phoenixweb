﻿using Phoenix_Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

//namespace PhoenixWeb.ActionFilters
//{
public class RBACUser
{
    public Guid User_Id { get; set; }
    public bool IsSysAdmin { get; set; }
    public string Username { get; set; }
    private List<UserRole> Roles = new List<UserRole>();

    public RBACUser(string _username)
    {
        this.Username = _username;
        this.IsSysAdmin = false;
        GetDatabaseUserRolesPermissions();
    }
    public RBACUser()
    {

    }
    public List<UserRole> GetDatabaseUserRolesPermissionsOver(string Username)
    {
        List<UserRole> Roles1 = new List<UserRole>();
        using (aspnetdbEntities _data = new aspnetdbEntities())
        {
            var _user = _data.aspnet_Users.Where(u => u.UserName == Username).FirstOrDefault();

            if (_user != null)
            {
                var _userRoles = _data.aspnet_UsersInRoles_GetRolesForUser("Phoenix", Username).ToList();
                this.User_Id = _user.UserId;
                foreach (var _role in _userRoles)
                {
                    var rol = (from d in _data.aspnet_Roles
                               where d.RoleName == _role
                               select d.RoleId).FirstOrDefault();
                    UserRole _userRole = new UserRole { Role_Id = rol, RoleName = _role };



                    var sk = (from si in _data.RolePermissions
                              join p in _data.Permissions on si.PermissionId equals p.PermissionId
                              where si.RoleName == _role
                              select new { pId = si.PermissionId, pName = p.PermissionName }).ToList();
                    if (sk.Count > 0)
                    {
                        foreach (var s in sk)
                        {
                            _userRole.Permissions.Add(new RolePermission { Permission_Id = s.pId.Value, PermissionDescription = s.pName });

                        }
                    }
                    Roles1.Add(_userRole);
                }
            }
        }
        return Roles1;
    }

    private void GetDatabaseUserRolesPermissions()
    {
        using (aspnetdbEntities _data = new aspnetdbEntities())
        {
            var _user = _data.aspnet_Users
                .Include(n => n.aspnet_Roles)
                .FirstOrDefault(u => u.UserName == this.Username);

            if (_user != null)
            {
                //var _userRoles = _data.aspnet_UsersInRoles_GetRolesForUser("Phoenix", this.Username).ToList();
                this.User_Id = _user.UserId;

                foreach (var _role in _user.aspnet_Roles)
                {
                    UserRole _userRole = new UserRole { Role_Id = _role.RoleId, RoleName = _role.RoleName };

                    var sk = (from si in _data.RolePermissions
                              join p in _data.Permissions on si.PermissionId equals p.PermissionId
                              where si.RoleName == _role.RoleName
                              select new { pId = si.PermissionId, pName = p.PermissionName }).ToList();

                    var abc = sk.Select(n => new RolePermission()
                    {
                        Permission_Id = n.pId.Value,
                        PermissionDescription = n.pName
                    });
                    _userRole.Permissions.AddRange(abc);
                    this.Roles.Add(_userRole);
                }
            }
        }
    }

    public bool HasPermission(string requiredPermission)
    {
        bool bFound = false;
        foreach (UserRole role in this.Roles)
        {
            bFound = (role.Permissions.Any(p => p.PermissionDescription.ToLower() == requiredPermission.ToLower()));
            if (bFound)
                break;
        }
        return bFound;
    }

    public bool HasRole(string role)
    {
        return (Roles.Where(p => p.RoleName == role).ToList().Count > 0);
    }

    public bool HasRoles(string roles)
    {
        bool bFound = false;
        string[] _roles = roles.ToLower().Split(';');
        foreach (UserRole role in this.Roles)
        {
            try
            {
                bFound = _roles.Contains(role.RoleName.ToLower());
                if (bFound)
                    return bFound;
            }
            catch (Exception)
            {
            }
        }
        return bFound;
    }
}

public class UserRole
{
    public Guid Role_Id { get; set; }
    public string RoleName { get; set; }
    public List<RolePermission> Permissions = new List<RolePermission>();
}

public class RolePermission
{
    public int Permission_Id { get; set; }
    public string PermissionDescription { get; set; }
}
//}