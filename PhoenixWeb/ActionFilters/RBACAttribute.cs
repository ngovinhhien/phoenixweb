﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

//namespace PhoenixWeb.ActionFilters
//{
    public class RBACAttribute : AuthorizeAttribute
    {
        public string Permission { get; set; }
        public string UserName { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //if(Permission == null)
            //{
            //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Account" }, { "action", "LogOff" } });
            //}


            //Create permission string based on the requested controller name and action name in the format 'controllername-action'
            string requiredPermission = Permission;//String.Format("{0}-{1}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName);

            //Create an instance of our custom user authorization object passing requesting user's 'Windows Username' into constructor
            var requestingUser =
                RBAC_ExtendedMethods.GetRBACUser(filterContext.RequestContext.HttpContext.User.Identity.Name);

            //Check if the requesting user has the permission to run the controller's action
            if (!requestingUser.HasPermission(requiredPermission))
            {
                //User doesn't have the required permission and is not a SysAdmin, return our custom “401 Unauthorized” access error
                //Since we are setting filterContext.Result to contain an ActionResult page, the controller's action will not be run.
                //The custom “401 Unauthorized” access error will be returned to the browser in response to the initial request.
              //  filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Index" }, { "controller", "Unauthorised" } });

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary{{ "controller", "AdminHome" }, { "action", "Permission" } });
            }
            //If the user has the permission to run the controller's action, then filterContext.Result will be uninitialized and
            //executing the controller's action is dependant on whether filterContext.Result is uninitialized.
        }
    }
//}