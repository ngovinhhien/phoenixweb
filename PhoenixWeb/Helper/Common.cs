﻿using Phoenix_Service;
using PhoenixWeb.ReportingWebService;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Caching;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace PhoenixWeb.Helper
{
    public class Common
    {
        public static bool SendMail(List<String> _AddressTo, List<String> _CC, List<String> _BCC, String _Subject, String _Body, Boolean _CCAdmin, String _AddressFrom)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(System.Web.HttpContext.Current.Request.ApplicationPath);
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/MyWeb");

            MailSettingsSectionGroup mailSettings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            MailMessage _MailMessage = new MailMessage();
            _MailMessage.IsBodyHtml = true;

            if (_AddressTo != null)
            {
                foreach (var AddTo in _AddressTo)
                {
                    _MailMessage.To.Add(AddTo);
                }
            }
            _MailMessage.Subject = _Subject;
            if (_CCAdmin)
            {
                _MailMessage.Bcc.Add(_AddressFrom);
            }

            if (_CC != null)
            {
                foreach (var _cc in _CC)
                {
                    if (!string.IsNullOrEmpty(_cc))
                    {
                        _MailMessage.CC.Add(_cc);
                    }
                }
            }

            if (_BCC != null)
            {
                foreach (var _bcc in _BCC)
                {
                    if (!string.IsNullOrEmpty(_bcc))
                    {
                        _MailMessage.Bcc.Add(_bcc);
                    }
                }
            }
            _MailMessage.Body = _Body;
            SmtpClient postMan = new SmtpClient();
            postMan.SendCompleted += new SendCompletedEventHandler(postMan_SendCompleted);
            try
            {
                postMan.Send(_MailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static bool SendAdminMail(String _Subject, String _Body, String _emailto)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(System.Web.HttpContext.Current.Request.ApplicationPath);
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/MyWeb");

            MailSettingsSectionGroup mailSettings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            MailMessage _MailMessage = new MailMessage();
            _MailMessage.IsBodyHtml = true;

            if (_emailto != string.Empty)
            {
                _MailMessage.To.Add(_emailto);
            }
            else
            {
                _MailMessage.To.Add(mailSettings.Smtp.From);
            }

            _MailMessage.Bcc.Add("it@livegroup.com.au");
            _MailMessage.Subject = _Subject;
            _MailMessage.Body = _Body;

            SmtpClient postMan = new SmtpClient();
            postMan.SendCompleted += new SendCompletedEventHandler(postMan_SendCompleted);
            try
            {
                postMan.Send(_MailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
        static void postMan_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                //throw new NotImplementedException();
            }
        }

        public static string GenerateRandomPassword(int length)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?&#+";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }
      
        //public static void RenderReport(string reportName, IList<PhoenixWeb.ReportingWebService.ParameterValue> parameters, out byte[] output, out string extension, out string mimeType, out string encoding, out Warning[] warnings, out string[] streamIds)
        //{
        //    using (var webServiceProxy = new ReportExecutionServiceSoapClient("ReportExecutionServiceSoap"))
        //    {
        //        NetworkCredential clientCredentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("ReportDomainAdmin"), ConfigurationManager.AppSettings.Get("ReportDomainPassword"), ConfigurationManager.AppSettings.Get("ReportDomain"));
        //        webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
        //        webServiceProxy.ClientCredentials.Windows.ClientCredential = clientCredentials;

        //        // Init Report to execute
        //        ServerInfoHeader serverInfoHeader;
        //        ExecutionInfo executionInfo;
        //        ExecutionHeader executionHeader = webServiceProxy.LoadReport(null, reportName, null, out serverInfoHeader, out executionInfo);

        //        // Attach Report Parameters
        //        webServiceProxy.SetExecutionParameters(executionHeader, null, parameters.ToArray(), "en-au", out executionInfo);
        //        // Render
        //        webServiceProxy.Render(executionHeader, null, "PDF", null, out output, out extension, out mimeType, out encoding, out warnings, out streamIds);
        //    }

        //}

        public static void AddFilter(string name, string value)
        {
            NameValueCollection qs = System.Web.HttpContext.Current.Request.QueryString;
            qs = (NameValueCollection)System.Web.HttpContext.Current.Request.GetType().GetField("_queryString", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(System.Web.HttpContext.Current.Request);
            PropertyInfo readOnlyInfo = qs.GetType().GetProperty("IsReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            readOnlyInfo.SetValue(qs, false, null);
            qs[name] = value;
            readOnlyInfo.SetValue(qs, true, null);
        }

        public class ReportsResult : ActionResult
        {
            public ReportsResult(byte[] data, string mineType)
            {
                this.Data = data;
                this.MineType = mineType;
            }

            public byte[] Data { get; set; }
            public string MineType { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                if (Data == null)
                {
                    new EmptyResult().ExecuteResult(context);
                    return;
                }
                context.HttpContext.Response.ContentType = MineType;

                using (MemoryStream ms = new MemoryStream(Data))
                {
                    ms.Position = 0;
                    using (StreamReader sr = new StreamReader(ms))
                    {
                        context.HttpContext.Response.Output.Write(sr.ReadToEnd());
                    }
                }
            }
        }
    }
}