﻿using Phoenix_BusLog;
using PhoenixObjects.Enums;
using PhoenixWeb.Common;
using PhoenixWeb.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;

namespace PhoenixWeb.Helper
{
    public class SMSAPIHelper
    {
        public string InsertLiveSMSMember(LiveSMSMemberVM mem)
        {
            using (var client = new HttpClient())
            {
                string content = string.Empty;
               
                client.BaseAddress = new Uri(Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveSMSAPI.ToString()));
                
                client.DefaultRequestHeaders
                                         .Accept
                                         .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                var skey = Caching.GetSystemConfigurationByKey(SystemConfigurationKeyEnum.LiveSMSAPIKey.ToString());
                mem.secret_key = skey.ToString();
                var result = client.PostAsync("insert_liveftpos_user", mem, new JsonMediaTypeFormatter()).Result;

                if (result.IsSuccessStatusCode)
                {
                    content = result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    content = result.Content.ReadAsStringAsync().Result;
                }
                return content;
            }
        }
    }
}