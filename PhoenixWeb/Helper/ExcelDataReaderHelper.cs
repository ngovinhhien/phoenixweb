﻿using ExcelDataReader;
using System.IO;

namespace PhoenixWeb.Helper
{
    public static class ExcelDataReaderHelper
    {
        public static IExcelDataReader CreateDataReaderByExtensionFile(string extensionFile, Stream inputStream)
        {
            if (extensionFile.Equals(".xls"))
            {
                return ExcelReaderFactory.CreateBinaryReader(inputStream);
            }
            else if (extensionFile.Equals(".xlsx"))
            {
                return ExcelReaderFactory.CreateOpenXmlReader(inputStream);
            }
            else if (extensionFile.Equals(".csv"))
            {
                return ExcelReaderFactory.CreateCsvReader(inputStream);
            }

            return null;
        }
    }
}