﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using PhoenixWeb.Models.ViewModel;
using Phoenix_Service;
using System.Globalization;
using System.Web.Mvc;
using PhoenixObjects.Common;

namespace PhoenixWeb.Helper
{
    public static class Extensions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            //var sourceType = typeof(TSource);
            //var destinationType = typeof(TDestination);
            //var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));
            //foreach (var property in existingMaps.GetUnmappedPropertyNames())
            //{
            //    expression.ForMember(property, opt => opt.Ignore());
            //}
            return expression;
        }

        public static CadmusIN ToCadmusIN(this CadmusDeclined cadmusDeclined)
        {
            return new CadmusIN()
            {
                batchid = cadmusDeclined.batchid,
                batchstatus = "Loaded",
                transid1 = cadmusDeclined.transid1,
                merchantid = cadmusDeclined.merchantid,
                merchantname = cadmusDeclined.merchantname,
                driverid = cadmusDeclined.driverid,
                loaded = cadmusDeclined.loaded,
                startshift = cadmusDeclined.startshift,
                endshift = cadmusDeclined.endshift,
                batchnumber = cadmusDeclined.batchnumber,
                taxiid = cadmusDeclined.taxiid,
                terminalid = cadmusDeclined.terminalid,
                transtype = cadmusDeclined.transtype,
                trans_status = "Approved (00)",
                transpan = cadmusDeclined.transpan,
                cardname = cadmusDeclined.cardname,
                transstart = cadmusDeclined.transstart,
                transend = cadmusDeclined.transend,
                transfare = cadmusDeclined.transfare,
                transextras = cadmusDeclined.transextras,
                transtip = cadmusDeclined.transtip,
                transtotalamount = cadmusDeclined.transtotalamount,
                transauthorisationnumber = cadmusDeclined.transauthorisationnumber,
                transpassengers = cadmusDeclined.transpassengers,
                transtarrif = cadmusDeclined.transtarrif,
                transkilometers = cadmusDeclined.transkilometers,
                transpickupzone = cadmusDeclined.transpickupzone,
                transdropoffzone = cadmusDeclined.transdropoffzone,
                sourcefile = cadmusDeclined.sourcefile,
                recordcreated = cadmusDeclined.recordcreated,
                filedate = cadmusDeclined.filedate,
                HostServiceCharge = cadmusDeclined.HostServiceCharge,
                HostGST = cadmusDeclined.HostGST,
                HostTransactionHistoryKey = cadmusDeclined.HostTransactionHistoryKey,
                TransactionDescription = cadmusDeclined.TransactionDescription,
                UserID = cadmusDeclined.UserID,
                BusinessID = cadmusDeclined.BusinessID,
                HostTransactionNumber = cadmusDeclined.HostTransactionNumber,
                ResponseCode = cadmusDeclined.ResponseCode,
                ResponseText = cadmusDeclined.ResponseText,
                ABN = cadmusDeclined.ABN,
                CardEntryMode = cadmusDeclined.CardEntryMode,
                CardExpiry = cadmusDeclined.CardExpiry,
                AutoTipAmount = cadmusDeclined.AutoTipAmount,
                AutoTipFee = cadmusDeclined.AutoTipFee,
                TransactionPaymentTime = cadmusDeclined.TransactionPaymentTime
            };
        }


        public static CadmusIN ToCadmusIN(this TransactionFromBankVM transactionFromBank)
        {
            return new CadmusIN()
            {
                batchid = transactionFromBank.batchid.ToString(),
                batchstatus = "Loaded",
                transid1 = transactionFromBank.transid1,
                merchantid = transactionFromBank.Merchantid,
                merchantname = transactionFromBank.merchantname,
                driverid = long.Parse(transactionFromBank.driverid),
                loaded = transactionFromBank.Loaded.Value,
                startshift = transactionFromBank.Startshift.Value,
                endshift = transactionFromBank.endshift.Value,
                batchnumber = transactionFromBank.BatchNumber,
                taxiid = transactionFromBank.taxiid,
                terminalid = transactionFromBank.terminalnumber,
                transtype = transactionFromBank.TransType,
                trans_status = transactionFromBank.Trans_Status,
                transpan = transactionFromBank.transpan,
                cardname = transactionFromBank.CardName,
                transstart = transactionFromBank.transstart.Value,
                transend = transactionFromBank.transend.Value,
                transfare = (float)transactionFromBank.TransFare.Value,
                transextras = (float)transactionFromBank.TransExtras.Value,
                transtip = (float)transactionFromBank.TransTip.Value,
                transtotalamount = (float)transactionFromBank.TransTotalAmount.Value,
                transauthorisationnumber = transactionFromBank.TransAuthorisationNumber,
                transpassengers = transactionFromBank.TransPassengers,
                transtarrif = transactionFromBank.TransTarrif,
                transkilometers = transactionFromBank.TransKilometers,
                transpickupzone = transactionFromBank.TransPickupZone,
                transdropoffzone = transactionFromBank.TransDropoffZone,
                sourcefile = transactionFromBank.sourcefile,
                recordcreated = transactionFromBank.recordcreated,
                filedate = transactionFromBank.filedate,
                orig_cardname = transactionFromBank.orig_CardName
            };
        }

        public static MemberReferenceEntryForManagerApproval ToMemberReferenceEntryForManagerApproval(
            this GetQBRPointForSaleFinByYearMonthAndBusinessType_Result viewData)
        {
            return new MemberReferenceEntryForManagerApproval()
            {
                Point = viewData.Point,
                StatusCode = viewData.Status,
                MemberKey = viewData.MemberKey,
                OriginalYear = viewData.Year ?? 0,
                OriginalMonth = viewData.Month ?? 0,
                LoyaltyPromotionID = viewData.LoyaltyPromotionID,
                Comment = viewData.Comment,
                CreatedBy = viewData.CreatedBy,
                Description = viewData.Description,
                BusinessTypeID = viewData.BusinessTypeID,
                QBRNumber = viewData.QBRNumber,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                EntryDateTime = viewData.EntryDate,
                ModifiedBy = viewData.CreatedBy,
                IsActive = true,
                MemberReferenceEntries = new List<MemberReferenceEntry>()
            };
        }

        public static QBRExchangeRate ToQbrExchangeRate(this QantasPointEarnedRateVM viewModel)
        {
            var isNewMerchantTypeCode = viewModel.ExchangeRateTypeCode.Equals("NEW_MERCHANT");
            var isDefault = viewModel.ExchangeRateTypeCode.Equals("DEFAULT");
            CultureInfo culture = new CultureInfo("en-US");
            DateTime? startDate = null;
            DateTime? endtDate = null;
            if (!string.IsNullOrEmpty(viewModel.StartDate))
            {
                startDate = DateTime.ParseExact(viewModel.StartDate, "dd/MM/yyyy", culture);
            }

            if (!string.IsNullOrEmpty(viewModel.EndDate))
            {
                endtDate = DateTime.ParseExact(viewModel.EndDate, "dd/MM/yyyy", culture);
            }

            return new QBRExchangeRate()
            {
                ID = viewModel.ID,
                Name = viewModel.Name,
                ExchangeRateTypeCode = viewModel.ExchangeRateTypeCode,
                IsActive = true,
                CampaignStartDate = (!isNewMerchantTypeCode && !isDefault) ? startDate : null,
                CampaignEndDate = (!isNewMerchantTypeCode && !isDefault) ? endtDate : null,
                RegisterStartDate = (isNewMerchantTypeCode && !isDefault) ? startDate : null,
                RegisterEndDate = (isNewMerchantTypeCode && !isDefault) ? endtDate : null,
                QantasExchangeRate = Math.Round(viewModel.QantasExchangeRate, 2, MidpointRounding.AwayFromZero),
                RegisterEffectedDays = viewModel.EffectedDay,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(
            this IEnumerable<View_GetQantasExportFile> qantasExportFiles)
        {
            return qantasExportFiles.Select(n => new SelectListItem()
            {
                Text = n.FileName,
                Value = n.QantasExportFileKey.ToString()
            }).ToList();
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(List<BusinessTypeModel> businessTypeList, bool needSelectBusinessTypeItem = false)
        {
            if (needSelectBusinessTypeItem)
            {
                businessTypeList.Add(new BusinessTypeModel
                {
                    BusinessTypeKey = 0,
                    BusinessTypeName = "Select Business Type"
                });
            }

            return businessTypeList.Select(item => new SelectListItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString()
            }).OrderBy(c => c.Value).ToList();
        }

        public static IEnumerable<CustomHelpers.CustomSelectItem> AsSelectListItemCustom(List<BusinessTypeModel> businessTypeList)
        {
            return businessTypeList.Select(item => new CustomHelpers.CustomSelectItem
            {
                Text = item.BusinessTypeName,
                Value = item.BusinessTypeKey.ToString(),
                CustomValue = item.MerchantId
            }).OrderBy(c => c.Value).ToList();
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<CadmusinCardType> cardTypeList, string selectedValue = "")
        {
            if (cardTypeList == null)
            {
                return new SelectListItem[] { };
            }

            return cardTypeList.Select(n => new SelectListItem()
            {
                Text = n.CardTypeKey,
                Value = n.CardTypeKey,
                Selected = (selectedValue == n.CardTypeKey)
            }).ToList();
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<StateModel> stateList, string selectedValue = "")
        {
            if (stateList == null)
            {
                return new SelectListItem[] { };
            }

            return stateList.Select(n => new SelectListItem()
            {
                Text = n.StateName,
                Value = n.StateName,
                Selected = (selectedValue == n.StateName)
            }).ToList();
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<VehicleTypeModel> vehicleTypeList, int selectedValue = 0)
        {
            if (vehicleTypeList == null)
            {
                return new SelectListItem[] { };
            }

            return vehicleTypeList.Select(n => new SelectListItem()
            {
                Text = n.Name,
                Value = n.VehicleType_key.ToString(),
                Selected = (selectedValue == n.VehicleType_key)
            }).ToList();
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<PaymentType> paymentTypes)
        {
            var paymentTypeList = new List<SelectListItem> { new SelectListItem { Text = "All", Value = string.Empty } };
            paymentTypeList.AddRange(paymentTypes.Select(type => new SelectListItem()
            {
                Text = type.PaymentTypeDescription,
                Value = type.PaymentType_key.ToString(),
            }).ToList());
            return paymentTypeList;
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<Company> businessTypes)
        {
            var businessTypeList = new List<SelectListItem> { new SelectListItem { Text = "All", Value = string.Empty } };
            businessTypeList.AddRange(businessTypes.Select(type => new SelectListItem()
            {
                Text = type.Name,
                Value = type.Company_key.ToString(),
            }).ToList());
            return businessTypeList;
        }

        public static IEnumerable<SelectListItem> AsSelectListItem(this List<QantasExportFileEntryStatu> fileEntryStatuses)
        {
            var fileEntryStatusList = new List<SelectListItem> { new SelectListItem { Text = "All", Value = string.Empty } };
            fileEntryStatusList.AddRange(fileEntryStatuses.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = s.QantasExportFileEntryStatusId.ToString(),
            }).ToList());
            return fileEntryStatusList;
        }


        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
}