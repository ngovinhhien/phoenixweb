﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using PhoenixObjects.Member.DriverCard;
using System.Net;
using Newtonsoft.Json;
using Phoenix_BusLog;
using PhoenixWeb.Models.ViewModel.DriverCard;
using PhoenixObjects.DriverCard;

namespace LGHelper
{
    public class OxygenAPI : RestAPI
    {
        private string _tellerId = string.Empty;
        private string _vendorBranchId = string.Empty;
        private string _vendorBranchName = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="OxygenAPI"/> class.
        /// </summary>
        public OxygenAPI()
        {
            CreateAPI("DriverCardAPIURL", "DriverCardClientID", "DriverCardClientSecret");
            _tellerId = Caching.GetSystemConfigurationByKey("DriverCardAPITellerId");
            _vendorBranchId = Caching.GetSystemConfigurationByKey("DriverCardAPIVendorBranchId");
            _vendorBranchName = Caching.GetSystemConfigurationByKey("DriverCardAPIVendorGroupName");
        }

        public OxygenAPIResponseModel Execute(Method method, string action, object requestBody = null)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(_baseURL + "card/" + action);
            RestRequest req = new RestRequest(method);
            req.AddHeader("clientId", _clientId);
            req.AddHeader("clientSecret", _clientSecret);
            req.AddJsonBody(requestBody);
            var result = client.Execute(req);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                if (string.IsNullOrEmpty(result.Content))
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<OxygenAPIResponseModel>(result.Content);
            }
            else if (result.StatusCode == HttpStatusCode.BadGateway)
            {
                return JsonConvert.DeserializeObject<OxygenAPIResponseModel>("{'statusCode': '502', 'description': 'Bad Request'}");
            }
            else
            {
                return JsonConvert.DeserializeObject<OxygenAPIResponseModel>("{\"statusCode\": \"" + result.StatusCode.ToString() + "\", \"description\": \"" + result.StatusDescription + "\"}");
            }
        }

        public OxygenAPIResponseModel ReissueCard(string oldAccountIdentifier, string newAccountIdentifier)
        {
            var response = Execute(Method.POST, "reissueCard", new
            {
                accountIdentifier = oldAccountIdentifier,
                cardReferenceNumber = newAccountIdentifier,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }

        public OxygenAPIResponseModel RegisterCard(DriverCardInfoVM model, string postCode, string state, string suburb)
        {
            var response = Execute(Method.POST, "cardSale", new
            {
                accountIdentifier = model.accountIdentifier,
                customerDetails = new
                {
                    address1 = model.address,
                    city = suburb,
                    postalCode = postCode,
                    state = state,
                    country = "AUS",
                    dateOfBirth = model.DOB.ToString("dd/MM/yyyy"),
                    email = model.email,
                    firstName = model.firstName,
                    lastName = model.lastName,
                    mobile = model.mobile,
                    language = "EN",
                },
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }

        public OxygenAPIResponseModel ChangeCardStatus(string cardRefNumber, string statusCode)
        {
            var response = Execute(Method.POST, "accountBlock", new
            {
                accountIdentifier = cardRefNumber,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
                cardStatus = statusCode,
            });
            return response;
        }

        public OxygenAPIResponseModel GetCardInfo(string cardRefNumber)
        {
            var response = Execute(Method.POST, "getCustomerAccountInfo", new
            {
                accountIdentifier = cardRefNumber,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }

        public OxygenAPIResponseModel UpdateProfile(DriverCardInfoVM model)
        {
            var response = Execute(Method.POST, "updateProfile", new
            {
                firstName = model.firstName,
                lastName = model.lastName,
                email = model.email,
                mobileNumber = model.mobile,
                countryCode = "AUS",
                dateOfBirth = model.DOB.ToString("dd/MM/yyyy"),
                address1 = model.address,
                address2 = model.address2,
                city = model.city,
                postalCode = model.postCode,
                state = model.state,
                accountIdentifier = model.accountIdentifier,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }

        public OxygenAPIResponseModel GetCardStatus(string accountIdentifier)
        {
            var response = Execute(Method.POST, "cardStatus", new
            {
                accountIdentifier = accountIdentifier,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }

        public OxygenAPIResponseModel GetTransactionDetail(string transRefId)
        {
            var response = Execute(Method.POST, "getTransactionDetails", new
            {
                transRefId = transRefId,
                tellerId = _tellerId,
                vendorBranchId = _vendorBranchId,
                vendorGroupName = _vendorBranchName,
            });
            return response;
        }
    }
}